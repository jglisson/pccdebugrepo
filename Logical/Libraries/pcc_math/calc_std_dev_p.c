#include <math.h>
#include "pcc_math.h"

float calc_std_dev_p(samples *sample_data,float *mean,signed short num)
{
    float std_dev;
    float var;

    var = calc_variance_p(sample_data,mean,num);

    std_dev = (float)sqrt((float)var);
    return std_dev;
}


