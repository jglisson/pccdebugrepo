/************************************************************************/
/*
   File:  register.h

   Description: Remote used registers.


      $Log:   F:\Software\BR_Guardian\register.h_v  $
 *
 *    Rev 1.0   Feb 11 2008 11:17:28   YZS
 * Initial revison.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/

/* Original PCC type addressing definitions                             */
/* note: ext offset is 900, per hop is 100, nip starts at 10000, system */
/*  starts at 20000                                                     */

/*      definition           offset    ext  hop  nip  sys  udata  r/w   */

#define R_SET_PARTS             1   /*  x    x               x    r/w   */
#define R_ACT_PARTS             2   /*  x    x               x     r    */
#define R_SET_WTP               3   /*  x    x         x     x    r/w   */
#define R_ACT_WTP               4   /*  x    x         x     x     r    */
#define R_SET_LTP               5   /*            x          x    r/w   */
#define R_ACT_LTP               6   /*            x          x     r    */
/* Following point 'overloaded' to give more registers */
#define R_INSTANT_WTP           6   /*  x    x               x     r    */
#define R_SET_WPL               7   /*                 x     x    r/w   */
#define R_ACT_WPL               8   /*                 x     x     r    */
#define R_SET_SPD               9   /*  x    x    x          x    r/w   */
#define R_ACT_SPD               10  /*  x    x    x          x     r    */
#define R_INVEN_WT              11  /*  x    x         x     x    r/w   */
#define R_SHIFT_WT              12  /*  x    x         x     x    r/w   */
#define R_INVEN_LEN             13  /*            x          x    r/w   */
#define R_SHIFT_LEN             14  /*            x          x    r/w   */
#define R_HOPPER_WEIGHT         15  /*       x               x     r    */
#define R_MAN_SPD               16  /*  x    x    x               r/w   */
#define R_SET_THK               18  /*  x              x     x    r/w   */
#define R_ACT_THK               19  /*  x              x     x     r    */
#define R_SET_OD                20  /*                 x     x    r/w   */
#define R_ACT_OD                21  /*                 x     x     r    */
#define R_SET_ID                22  /*                 x     x    r/w   */
#define R_ACT_ID                23  /*                 x     x     r    */
#define R_SET_WIDTH             24  /*                 x     x    r/w   */
#define R_ACT_WIDTH             25  /*                 x     x     r    */
#define R_SET_DENSITY           26  /*  x    x         x          r/w   */
#define R_ACT_DENSITY           27  /*  x    x         x          r/w   */
#define R_SET_RATIO_SPD         28  /*  x    x    x    x     x    r/w   */
#define R_ACT_RATIO_SPD         29  /*  x    x    x          x     r    */

#define R_TEMP_SET_PARTS        30  /*  x    x               x    r/w   */
#define R_TEMP_SET_WTP          31  /*  x    x    x          x    r/w   */
#define R_TEMP_SET_LTP          32  /*            x          x    r/w   */
#define R_TEMP_SET_WPL          33  /*                 x     x    r/w   */
#define R_TEMP_MAN_SPD          34  /*  x    x    x               r/w   */
#define R_TEMP_SET_THK          36  /*  x              x     x    r/w   */
#define R_TEMP_SET_OD           37  /*                 x     x    r/w   */
#define R_TEMP_SET_ID           38  /*                 x     x    r/w   */
#define R_TEMP_SET_WIDTH        39  /*                 x     x    r/w   */
#define R_TEMP_SET_DENSITY      40  /*  x    x         x          r/w   */
#define R_TEMP_STRETCH_FACTOR   41  /*                       x    r/w   */
#define R_STRETCH_FACTOR        42  /*                       x    r/w   */
#define R_SET_WPA               43  /*  x              x     x    r     */
#define R_ACT_WPA               44  /*  x              x     x    r     */
#define R_TEMP_SET_WPA          45  /*  x              x     x    r/w   */
#define R_SHIFT_AREA            46  /*  x              x     x    r/w   */
#define R_INVEN_AREA            47  /*  x              x     x    r/w   */

#define R_ACCEL                 50  /*  x    x    x    x          r/w   */
#define R_DECEL                 51  /*  x    x    x    x          r/w   */

#define R_SPECIAL_1             55  /*                                  */
#define R_SPECIAL_2             56  /*                                  */
#define R_SPECIAL_3             57  /*                                  */
#define R_SPECIAL_4             58  /*                                  */
#define R_SPECIAL_5             59  /*                                  */


#define R_SYS_BATCH_SIZE        60  /*                 x          r/w   */
#define R_SYS_BATCH_WT          61  /*                 x          r/w   */

#define R_MIX_TIME              62
#define R_MIX_TIME_LEFT         63

#define R_MIN_WTP               64
#define R_MAX_WTP               65
#define R_MIN_LTP               66
#define R_MAX_LTP               67

#define R_TEMP_SYS_BATCH_SIZE   68
#define R_TEMP_MIX_TIME         69

#define R_ALTERNATE_SET_VALUE   75  /* Value from alternate register set with address at R_ALTERNATE_SET_ADDR */

/* inventory registers for the CCM protocol */
#define R_INVEN_WT_HI           76
#define R_SHIFT_WT_HI           77
#define R_INVEN_LEN_HI          78
#define R_SHIFT_LEN_HI          79

/* the following are integers */

#define R_INTEGER               80  /* start of integer registers       */
#define R_CONFIG                80  /*  x              x          r     */
#define R_NUM_EXT               81  /*                 x          r     */
#define R_HO_GEAR_NUM           81  /*            x               r/w   */
#define R_NUM_HOP               82  /*  x              x          r     */
#define R_LAYER_PATTERN_LO      83  /* layer pattern for gravitrol */
#define R_TEMP_LAYER_PATTERN_LO 84  /* temp layer pattern for gravitrol */

#define R_ALTERNATE_SET_ADDR    85  /* Gets address from alternate register set, value at R_ALTERNATE_SET_VALUE */
#define R_NEXT_RECIPE           86
#define R_I_ALTERNATE_SET_VALUE 87  /* Integer version of value from alternate register set with address at R_ALTERNATE_SET_ADDR */
#define R_DEVICE_CONTROL        88  /* re-used layer pattern for batch blender */
#define R_LAYER_PATTERN         88  /* layer pattern for gravitrol */
#define R_TEMP_LAYER_PATTERN    89  /* temp layer pattern for gravitrol */

#define R_MODE_BUTTON           90  /*                 x          r/w   */
#define R_AUX_BUTTON            91  /*                 x          r/w   */
#define R_STATUS_WORD_LO        92  /*  x    x    x    x          r     */
#define R_ALARM_LATCH_WORD_LO   93  /*  x    x    x    x          r/w   */
#define R_ALARM_STATUS_WORD_LO  94  /*  x    x    x    x          r/w   */

#define R_MODE_BUTTON_HI        95  /*                 x          r/w   */
#define R_AUX_BUTTON_HI         96  /*                 x          r/w   */
#define R_STATUS_WORD_HI        97  /*  x    x    x    x          r     */
#define R_ALARM_LATCH_WORD      98  /*  x    x    x    x          r/w   */
#define R_ALARM_STATUS_WORD     99  /*  x    x    x    x          r/w   */

#define R_STATUS_HIGH         150  /*  x    x    x    x          r/w   */
#define R_ALARM_CURRENT_HIGH  151  /*  x    x    x    x          r/w   */
#define R_ALARM_LATCHED_HIGH  152  /*  x    x    x    x          r/w   */

/* The following are ONLY system points - that is why the numbers can exceed 99 */
#define R_AUX_OUT_ACT          101  /*     float                  r     */
#define R_AUX_OUT_VOLTS        102  /*     float                  r     */
#define R_RESIN_DENSITY        103  /*     float                        */
#define R_RECIPE_DENSITY       104
#define R_RECIPE_INVEN         105  /*     float                        */
#define R_RESIN_INDEX          180  /*     int                    r/w   */
#define R_RECIPE_INDEX         181  /*     int                    r     */
#define R_NUM_RECIPES          182  /*     int                    r     */
#define R_RECIPE_USED          183

#define R_MSGS_PER_SEC         190  /*                 x     x          */
#define R_PERCENT_GOOD         191  /*                 x     x          */
#define R_INST_MSGS_PER_SEC    192  /*                 x     x          */
#define R_INST_PERCENT_GOOD    193  /*                 x     x          */

/* The following are Only for description of job, resin, device and recipe name*/
#define R_RESIN_NAME_1         80
#define R_RESIN_NAME_2         81
#define R_RESIN_NAME_3         82
#define R_JOB_NAME_1           83
#define R_JOB_NAME_2           84
#define R_JOB_NAME_3           85
#define R_JOB_NAME_4           86
#define R_RECIPE_NAME_1        87
#define R_RECIPE_NAME_2        88
#define R_RECIPE_NAME_3        89
#define R_RECIPE_NAME_4        90
#define R_DEVICE_NAME_1        91
#define R_DEVICE_NAME_2        92
#define R_DEVICE_NAME_3        93
#define R_DEVICE_NAME_4        94

/**********************************************************************/
/* String Registers                                                   */
/*      reserve registers 21000-21999 for string type registers       */
/**********************************************************************/
#define R_RESIN_DESC           1000 /*     string                 r     */
#define R_RECIPE_DESC          1001 /*     string                 r     */

#define USER_BLK_START_REG 30000
#define MAX_PCC_REG          100


/* *************** CONFIGURE FOR HOPPERS ************************* */

/* NETWORK ADDRESSES */
#define A_WEIGH_MOD_ADDR            0           /* unsigned char weigh_mod.addr - address of weigh module      */
#define A_WEIGH_MOD_CHAN            1           /* unsigned char weigh_mod.chan - address of weigh module      */
#define A_DRIVE_MOD_ADDR            2           /* unsigned char drive_mod.addr - address of drive module      */
#define A_DRIVE_MOD_CHAN            3           /* unsigned char drive_mod.chan - address of drive module      */
#define A_LOAD_RELAY_ADDR           4           /* unsigned char load_relay.addr - address of load relay        */
#define A_LOAD_RELAY_CHAN           5           /* unsigned char load_relay.chan - address of load relay        */
#define A_ALARM_RELAY_ADDR          6           /* unsigned char alarm_relay.addr - address of alarm relay       */
#define A_ALARM_RELAY_CHAN          7           /* unsigned char alarm_relay.chan - address of alarm relay       */
#define A_SYNC                      8           /* char */
#define A_SYNC_ANGLE                9           /* short */
#define A_DRIVE_INHIBIT_INVERT      10          /* short */

/* ALARM/LOAD WEIGHTS */
#define A_LOAD_WT_ON                11          /* float    load_wt_on - below this weight, signal load       */
#define A_LOAD_WT_OFF               12          /* float    load_wt_off - above this weight, load off          */
#define A_ALARM_WT                  13          /* float    alarm_wt - below this weight, LOW HOPPER ALARM  */
#define A_CRIT_WT                   14          /* float    crit_wt - below this weight, CRIT LOW ALARM    */
#define A_ON_OVV_LOW                15          /* float  */
#define A_ON_OFF_HIGH               16          /* float  */
#define A_DEMO_MAX_WTP              17          /* float  */
#define A_MIN_DUMP_WT               18          /* float    min_dump_wt - below this weight, LOW DUMP ALARM    */

/* CONTROL LOOP PARAMETERS */
#define A_MOVEPTS                   19          /* unsigned char movepts - number of mov points(0->MAX_MOVE)    */
#define A_CALCTIME                  20          /* unsigned short calctime - calc time in 68000 ticks             */
#define A_RS_CLIP                   21          /* unsigned char rs_clip - specifies whether max_chg active     */
#define A_RS_MAXCHG                 22          /* float    rs_maxchg - specifies max dev from ave_rs        */
#define A_DEAD_BAND                 23          /* float    dead_band - control dead band percent muliplier  */
#define A_MAX_ERROR                 24          /* float    max_error - max error before throwing away       */
#define A_MAX_BAD                   25          /* unsigned short max_bad - max bad updates before forcing update*/
#define A_COAST_ERROR               26          /* float    coast_error - percent error for coast              */
#define A_COAST_TOLERANCE           27          /* unsigned short coast_tolerance -t tolerance for coast             */
#define A_INITIAL_COAST             28          /* unsigned char initial_coast -initial coast count when coasting    */
#define A_MAX_COAST_TIME            29          /* unsigned short max_coast_time -x coast time in 68000 time ticks   */
#define A_DRIVE_FAIL_ERROR          30          /* unsigned char drive_fail_error -m bits/scan needed for no fail    */
#define A_MAX_DRIVE_FAIL            31          /* unsigned char max_drive_fail -x drive fails before alarm         */
#define A_ON_DELAY                  32          /* unsigned short on delay time for manual backup      */
#define A_MEET_RATE_TIME            33          /* unsigned short If no weight gain (or proc covered) after this secs., alarm */
#define A_RATE_CHANGE_DEAD_BAND     34          /* float    Limit on bg rate change when controlling extruder */
#define A_RATE_MATCH_TOLERANCE      35          /* float    In CONTROL, IN and OUT must have < this % err before rs computed */
#define A_OUT_OF_SPEC_LIMIT         36          /* float    out_of_spec_limit - out of spec alarm limit              */
#define A_OUT_OF_SPEC_DELAY         37          /* Int, c->out_of_spec_delay */
#define A_WM_FILTER_FREQ            73

/* ACCEL/DECEL/SPEED PARAMETERS */
#define A_WARNING_HIGH_SPD          38          /* float    warning_high_spd - above this speed, recipe warning     */
#define A_WARNING_LOW_SPD           39          /* float    warning_low_spd - below this speed, recipe warning     */
#define A_ALARM_HIGH_SPD            40          /* float    alarm_high_spd - above this speed, OVERSPEED ALARM    */
#define A_ALARM_LOW_SPD             41          /* float    alarm_low_spd - below this speed, UNDERSPEED ALARM   */
#define A_MAX_VOLTAGE               42          /* float    max_voltage - For trim-DAC - 0 to 10       */
#define A_ZERO_CROSSING             43          /* float    zero_crossing - speed at which drive starts to turn  */
#define A_ACCEL                     44          /* float   */
#define A_DECEL                     45          /* float   */


/* BATCH BLENDER - BATCH HOPPER DEFINES */
#define A_MAIN_INGR_FEED_METHOD         46          /* char */
#define A_WB_NUMBER_FEEDERS             47
#define A_WEIGH_HOPPER_SOLENOID_TYPE    48          /* char */
#define A_WEIGH_HOPPER_SOLENOID_ADDR    49
#define A_WEIGH_HOPPER_OPEN_RELAY       50
#define A_WEIGH_HOPPER_CLOSE_RELAY      51
#define A_MAIN_INGR_SOLENOID_TYPE       52
#define A_MAIN_INGR_SOLENOID_ADDR       53
#define A_MAIN_INGR_OPEN_RELAY          54
#define A_MAIN_INGR_CLOSE_RELAY         55
#define A_LOW_LVL_PROX_ADDR             56
#define A_LOW_LVL_PROX_CHAN             57
#define A_MANUAL_HOP_FULL_WT            58

/* BATCH BLENDER - FEEDER DEFINES */
#define A_INIT_BULK_MULT                59
#define A_INIT_DRIBBLE_MULT             60
#define A_MAX_BULK_OVERSPILL            61
#define A_MAX_DRIBBLE_OVERSPILL         62
#define A_MAX_DRIBBLE_TIME              63
#define A_MIN_DRIBBLE_TIME              64
#define A_PERCENT_ACCURACY_TGT          65
#define A_TIME_BEFORE_DRIVE_FAIL        66
#define A_NUM_SETTLE_READINGS           71
#define A_TICKS_PER_SETTLE              72

/* BATCH BLENDER - MIXER DEFINES */
#define A_MIX_TIME                      70

/* CALIBRATION */
#define A_ZERO_WT_BITS                  67
#define A_TEST_WT_BITS                  68
#define A_TEST_WT_LBS                   69

#define A_AUX_OUT_ADDR                  73      /* unsigned char cs->aux_out.addr */
#define A_AUX_OUT_CHAN                  74      /* unsigned char cs->aux_out.chan */
#define A_AUX_OUT_PERCENT               75      /* float cs->aux_out_percent */
#define A_AUX_OUT_BASE_RATE             76      /* float cs->aux_out_base_rate */
#define A_AUX_OUT_MAX_VOLTS             77      /* float cs->aux_out_max_volts */
#define A_SPEED_CUT_RELAY_ADDR          78      /* unsigned char cs->speed_cut_relay.addr */
#define A_SPEED_CUT_RELAY_CHAN          79      /* unsigned char cs->speed_cut_relay.chan */
#define A_SPEED_CUT                     80      /* float cs->speed_cut */

#define A_LOADING_TIME              81          /* float  */
#define A_WEIGHT_ALARM_DELAY        82          /* float  */
#define A_MAX_SPEED                 83          /* float  */
#define A_SPEED_FACTOR              84          /* float  */
#define A_WIDTH_MODE                85          /* float  */
#define A_WIDTH_ALARM_MIN           86          /* float  */
#define A_WIDTH_ALARM_MAX           87          /* float  */
#define A_RS_RESETCHG               88          /* float  */

/* More calibration stuff (for NIPs) */
#define A_NIP_CIRCUMFERENCE         89          /* float */
#define A_PULSES_PER_REV            90          /* float */
#define A_LENPERBIT                 91          /* float */

/* Possible points for getting out relay states and directions */
#define A_RLY_CRD1_STATE      92
#define A_RLY_CRD1_DIRECTION  93
#define A_RLY_CRD2_STATE      94
#define A_RLY_CRD2_DIRECTION  95
#define A_RLY_CRD3_STATE      96
#define A_RLY_CRD3_DIRECTION  97
#define A_RLY_CRD4_STATE      98
#define A_RLY_CRD4_DIRECTION  99

/* PCC2 Device Definitions */
#define REM_MAX_EXT_ID        19
#define REM_MAX_SUB_DEVICE_ID 39
#define REM_HO_ID             50
#define REM_SHO_ID            51  /* sho is subdevice 01 */
#define REM_WIDTH_ID          52
#define REM_FLUFF_ID          53
#define REM_MIXER_ID          54
#define REM_UCB_DEF_ID        58
#define REM_UCB_DATA_ID       59
#define REM_RECIPE_DATA_ID    60
#define REM_RECIPE_NAME_ID    61
#define REM_RESIN_DATA_ID     62
#define REM_RESIN_NAME_ID     63
#define REM_SYSTEM_ID         64
#define REM_MAX_DEVICE_ID     64

/* Modbus device definitions     */
#define MOD_DC_DEVICE         36
#define MOD_BWH_DEVICE        36
#define MOD_MIXER_DEVICE      37
#define MOD_REFEED_DEVICE     38
#define MOD_SHO_DEVICE        51

/* PCC2 SubDevice Definitions */
#define TOTALS_SUBDEVICE      0                          /* Extruder totals */
#define DC_SUBDEVICE          TOTALS_SUBDEVICE           /* Downcomer */
#define HOP_A_SUBDEVICE       1                          /*  */
#define GATE_A_SUBDEVICE      HOP_A_SUBDEVICE            /*  */
#define MAIN_HOP_SUBDEVICE    HOP_A_SUBDEVICE            /*  */
#define ADD_A_SUBDEVICE       MAIN_HOP_SUBDEVICE + 1     /*  */

#define BWH_SUBDEVICE         96
#define MIXER_SUBDEVICE       97
#define REFEED_SUBDEVICE      98

/* RESIN and RECIPE Definitions */
#define REGS_PER_STRING       5
#define R2_RESIN_DENSITY      0
#define R2_RESIN_INVEN_WT     1
#define R2_RECIPE_DENSITY     0
#define R2_RECIPE_INVEN_WT    1

/* register definitions for PCC Type 2 Addressing */

#define PCC2_SYSTEM_OFFSET       6400000
#define PCC2_RESIN_NAME_OFFSET   6300000
#define PCC2_RESIN_DATA_OFFSET   6200000
#define PCC2_RECIPE_NAME_OFFSET  6100000
#define PCC2_RECIPE_DATA_OFFSET  6000000
#define PCC2_UCB_DATA_OFFSET     5900000
#define PCC2_UCB_DEF_OFFSET      5800000
#define PCC2_WTH_OFFSET          5200000
#define PCC2_SHO_OFFSET          5001000
#define PCC2_HO_OFFSET           5000000
#define PCC2_DEVICE_OFFSET        100000
#define PCC2_EXTRUDER_OFFSET      100000
#define PCC2_REFEED_OFFSET         98000
#define PCC2_SUBDEVICE_OFFSET       1000
#define PCC2_HOPPER_OFFSET          1000
#define PCC2_EXT_TOT_OFFSET         0000             /* totals for an extruder       */
#define PCC2_TYPES_PER              1000

#define MAX_EXT_ID               19

#define UCB_SIZE  50

#define MAX_REG            500
#define REG_INTEGER        300
#define REG_DOUBLE         200
#define REG_DEV_TYPE       500

/* subroutine module device function table offsets                            */
#define SYS_FUNC_OFFSET    0     /* System function table offset              */
#define FLF_FUNC_OFFSET    1     /* Gravifluff function table offset          */
#define WTH_FUNC_OFFSET    2     /* Width function table offset               */
#define HO_FUNC_OFFSET     3     /* Hauloff function table offset             */
#define LIW_FUNC_OFFSET    4     /* Liw functions offset                      */
#define DC_FUNC_OFFSET     5     /* Downcomer function table offset           */
#define BWH_FUNC_OFFSET    5     /* Batch Weigh Hopper function table offset  */
#define MIX_FUNC_OFFSET    6     /* Mixer function table offset               */
#define GATE_FUNC_OFFSET   7     /* Gate function table offset                */

/* flags of configuration */
    /* FOR GUARDIAN */
    /* FOR BG */
#define FLAG_BLENDER_DOWNCOMER         0x00000001
#define FLAG_CONTROL_LOADING           0x00000002
#define FLAG_USE_RS_TABLE              0x00000004
#define FLAG_REMOTE_SELF_LOADING       0x00000008
#define FLAG_REFEED                    0x00000020
#define FLAG_REFEED_GRAVIFLUFF         0x00000001

/* flags of security*/
#define FLAG_PE_NEEDED(A)      (unsigned int)1<<A
#define FLAG_RESET_MIN_SECURITY_LEVEL    0x00000001
#define FLAG_VALID_PASSWORD              0x00000002
#define FLAG_INVALID_PASSWORD            0x00000004

/* flags of communication*/
#define FLAG_PRINT_VERBOSE               0x00000001
#define FLAG_CLEAR_TOTALS_ON_PRINT       0x00000002
#define FLAG_LOG_ALARMS                  0x00000004
#define FLAG_AUTO_REPORTS                0x00000008
#define FLAG_REPORT_CHANGE_RECIPE        0x00000010
#define FLAG_PRINTER_BIAS_ENABLED        0x00000020
#define FLAG_PRINTER_MULTIDROP_ENABLED   0x00000040
#define FLAG_RETURN_SET_IF_ACT_0         0x00000080
#define FLAG_USE_PCC2_ADDRESS            0x00000100
#define FLAG_COMPATIBILITY_MODE          0x00000200
#define FLAG_AB_USE_CRC_CHKSUM           0x00000400
#define FLAG_AUTORUN_AFTER_RATE_CHANGED  0x00000800
#define FLAG_USING_32BITS_MODE           0x00001000
#define FLAG_REMOTE_BIAS_ENABLED         0x00002000
#define FLAG_REMOTE_MULTIDROP_ENABLED    0x00004000
#define FLAG_REMOTE_OP_BIAS_ENABLED      0x00008000
#define FLAG_REMOTE_OP_MULTIDROP_ENABLED 0x00010000
#define FLAG_GRAVILINK_USES_ETHERNET     0x00020000
#define FLAG_WEB_SERVER_ENABLED          0x00040000
#define FLAG_MBENET_ENABLED              0x00080000
#define FLAG_ABTCP_ENABLED               0x00100000
#define FLAG_PROFIBUS_AVAIL              0x00200000
#define FLAG_PROFIBUS_ENABLED            0x00400000

/* flags of user interface */
/* Guardian 3 and Bg 7 */
#define FLAG_SPD_IN_PERCENT            0x00000001
#define FLAG_NO_AUTO_FROM_PAUSE        0x00000002
#define FLAG_EXT_IN_RATIO              0x00000004
#define FLAG_HO_IN_RATIO               0x00000008
#define FLAG_MANUAL_RATE_SPEED         0x00000010
#define FLAG_USE_ANSI_DISPLAY          0x00000020
#define FLAG_KEYBOARD_ENABLED          0x00000040
#define FLAG_AUTO_SWITCH_AUTO          0x00000080
#define FLAG_PRINT_SUMMARY_REPORT      0x00000100
#define FLAG_PRINT_RECIPE_REPORT       0x00000200
#define FLAG_PRINT_ALARM_LOG_REPORT    0x00000400
#define FLAG_PRINT_CONFIG_REPORT       0x00000800
#define FLAG_PRINT_RESIN_REPORT        0x00001000
#define FLAG_USE_NEW_WTP_ALGORITHM     0x00002000
#define FLAG_LINE_SPEED_IS_MASTER      0x00004000
#define FLAG_WIDTH_IN_RATIO            0x00008000
#define FLAG_DISABLE_LOADING           0x00010000
#define FLAG_AUTO_SET_MAN_SPDS         0x00020000
#define FLAG_USE_NEW_WTP_CLIP          0x00040000
#define FLAG_INTL_DATE_FORMAT          0x00080000

/* flags of alarms */
#define FLAG_CLEAR_LOGGED_ALARMS       0x00000001
#define FLAG_SILENCE_OLD_ALARMS        0x00000002
#define FLAG_SILENCE_ACKED_ALARMS      0x00000004

/* flags of relay */
#define FLAG_SELECT_RECIPES_REMOTELY   0x00000001
#define FLAG_USE_BRIDGE_BREAK_RELAYS   0x00000002
#define FLAG_RELAY_FUNC_ACTIVE         0x00000004
#define FLAG_RELAY_FUNC_INVERT         0x00000008

/* flags of recipe */
/* FOR ALL */
#define FLAG_JOB_NAME_USED             0x00000001
#define FLAG_CLEAR_REC_TOTAL           0x00000002
#define FLAG_ENABLE_SPIN_PUMP_INPUT    0x00000004
#define FLAG_ENABLE_RESIN_CODES        0x00000008
#define FLAG_ENABLE_PRODUCT_CODE       0x00000010
#define FLAG_FORCE_DENSITY             0x00000020
#define FLAG_SPIN_PUMP_INPUT_IN_RECIPE 0x00000040
#define FLAG_LEARN_MAX_SPIN_PUMP_RATE  0x00000080
#define FLAG_STRETCH_FACTOR_IN_RECIPE  0x00000100
#define FLAG_JOB_WT_IN_RECIPE          0x00000200
#define FLAG_MIX_TIME_IN_RECIPE        0x00000400   /* FOR GUARDIAN */
#define FLAG_GATE_ORDER_IN_RECIPE      0x00000800   /* FOR GUARDIAN */
#define FLAG_RECIPE_TYPE               0x00001000   /* FOR GUARDIAN */
#define FLAG_MANUAL_IN_RECIPE          0x00002000
#define FLAG_STEP_SIZE_IN_RECIPE       0x00004000
#define FLAG_WIDTH_IN_RECIPE           0x00008000
#define FLAG_DENSITY_IN_RECIPE         0x00010000

/* flags of manual */
#define FLAG_MANUAL_BACKUP             0x00000001

/* FLAGS FOR GATE*/
#define FLAG_CLEAR_RECIPE_ON_CRIT_LOW  0x00000001
#define USE_SECONDARY_GATE             0x00000002
#define CLEAR_GATE_CAL_AFTER_OUTOFSPEC 0x00000004
#define USE_POWDER_RECEIVER            0x00000008
#define USE_AUGER_TO_FEED              0x00000010
#define ENABLE_PREPULSE                0x00000020

/* FLAGS FOR MIXER */
#define FLAG_AUX_STARTER_USED          0x00000001
#define FLAG_MATERIAL_REQUEST_INVERT   0x00000002

/*FLAGS FOR PUMP */
#define FLAG_DUST_USED                 0x00000001
#define FLAG_CONT_USED                 0x00000002

/* FLAGS FOR EXTRUDER */
/* FLAGS FOR LIW(HOPPER) OF BG */
#define  FLAG_ENABLE_AGITATOR_DRIVE    0x00000001
#define  FLAG_LIMIT_SPEED_CHANGE       0x00000002
#define  FLAG_REMOVE_HOPPER_ON_ALARM   0x00000004
#define  FLAG_USE_A_POSITIVE_WEIGHT    0x00000008
#define  FLAG_USING_SWING_GATE         0x00000010

/* FLAGS FOR DC & EXTRUDER */
#define  FLAG_EXT_SYN_PROX_INSTALLED   0x00000020
#define  FLAG_SYNC_WEIGHT_TO_EXT_SCREW 0x00000040
#define  FLAG_DI_SIGNAL_ACTIVE_ON      0x00000080

#define  FLAG_USE_SECOND_WM            0x00000100
#define  FLAG_STOP_INSTANTLY           0x00000200   /* FLAGS FOR HO */
#define  FLAG_USE_INTERNAL_LOADING     0x00000400
#define  FLAG_BLENDER_AS_WEIGH_HOPPER  0x00000800
#define  FLAG_USE_GRAVILINK_ETHERNET   0x00001000
#define  FLAG_USE_PCC_PLC              0x00002000

/* FLAGS FOR SELF_LOADING RECEIVER AND PUMP STATUS */
#define  FLAG_LEFT_SOLENOID            0x00000001
#define  FLAG_RIGHT_SOLENOID           0x00000002
#define  FLAG_FLAPPER_VALVE            0x00000004
#define  FLAG_SWING_GATE               0x00000008
#define  FLAG_PROXY_SWITCH             0x00000010
#define  FLAG_DUMP_STATUS              0x00000020
#define  FLAG_PUMP_STATUS              0x00000040
#define  FLAG_DUST_A                   0x00000080
#define  FLAG_DUST_B                   0x00000100
#define  FLAG_CONTINUOUS_RUN           0x00000200


/* FLAGS FOR BG CONTROL REGISTER */
   /*SYSTEM*/
#define  FLAG_RESTART_MINOP            0x00000001
#define  FLAG_CLEAR_CTL_LOOP_DIAG_DATA 0x00000002
#define  FLAG_CLEAR_NET_DIAG_DATA      0x00000004
#define  FLAG_RESET_TO_FACTORY_DEFAULT 0x00000008
#define  FLAG_DEFAULT_ALARMS           0x00000010
#define  FLAG_DELETE_RECIPE            0x00000020
#define  FLAG_DEFAULT_RECIPES          0x00000040
#define  FLAG_RESTART_REMOTE_COMM      0x00000080
#define  FLAG_START_AUTOMATIC_NW_SETUP 0x00000100
#define  FLAG_DISABLE_REMOTE_WRITES    0x00000200
#define  FLAG_CLEAR_RECIPE_INVENTORY   0x00000400
#define  FLAG_CLEAR_RESIN_INVENTORY    0x00000800

#define  FLAG_PORT_1_RS485_422_TX_FAIL 0x00000001
#define  FLAG_PORT_1_RS485_RX_FAIL     0x00000002
#define  FLAG_PORT_1_RS422_RX_FAIL	   0x00000004
#define  FLAG_PORT_2_RS485_TX_FAIL     0x00000008
#define  FLAG_PORT_2_RS485_RX_FAIL     0x00000010
#define  FLAG_PORT_3_RS422_TX_FAIL     0x00000020
#define  FLAG_PORT_3_RS422_RX_FAIL	   0x00000040
#define  FLAG_PORT_3_RS232_TX_FAIL     0x00000080
#define  FLAG_PORT_3_RS232_RX_FAIL     0x00000100
#define  FLAG_PORT_3_OPEN_FAIL		   0x00000200
#define  FLAG_PORT_2_OPEN_FAIL         0x00000400
#define  FLAG_PORT_1_OPEN_FAIL		   0x00000800
#define  FLAG_COM_PORT_ALL_PASSED	   0x00001000
#define  FLAG_PROFACE                  0x00002000
#define  FLAG_FORCE_RESTART            0x00004000
#define  FLAG_STORE_CONFIG             0x00008000
#define  FLAG_VALIDATE_RECIPE          0x00010000
#define  FLAG_STORE_RECIPE             0x00020000
#define  FLAG_SPAWN_SHELL_PORT_1       0x00040000
#define  FLAG_SPAWN_SHELL_PORT_3_TYPE232      0x00080000
#define  FLAG_SPAWN_SHELL_PORT_3_TYPE422      0x00100000

  /* Hopper & DC*/
/* These are part of the same register and defined in
   sys.h. They are shown here for clarity.
#define REM_CAL_NO_COM                    0x00000001
#define REM_CAL_UNSTABLE                  0x00000002
#define REM_CAL_RUNNING                   0x00000004
#define REM_CAL_NO_WEIGHT                 0x00000008
*/
/* for both hopper and dc*/
#define  FLAG_UNABLE_TALK_TO_WEIGH_MODULE 0x00000100
#define  FLAG_ZERO_WT_BITS_READY          0x00000200
#define  FLAG_CALIBRATION_DONE            0x00000400
#define  FLAG_REM_CALIB_ZERO_SCALE_DONE    0x00000800
#define  FLAG_REM_CALIB_AUTO_TARE_DONE    0x00001000
#define  FLAG_REM_CALIB_AUTO_TARE_ERROR   0x00002000

/* Liw */
#define  FLAG_CALLING_FOR_LOAD            0x00000800
/* command flags that perform action. Writing just these
   bits will not affect the other status bits. These
   command bits are in the upper word */
#define  FLAG_CALIBRATION_ZERO_WT         0x00010000
#define  FLAG_CALIBRATION_TEST_WT         0x00020000
#define  FLAG_CALIBRATION_CLEAR_STATUS    0x00040000

#define  FLAG_DISABLE_SERVICE_CODES       0x00040000
#define  FLAG_DISPENSE_TEST_AUTO_REPEAT   0x00000100  /* for dispense test auto repeat */
#define  FLAG_DISPENSE_TEST_CLEAR_CALIB   0x00000800  /* for dispense test --clear calibration */
#define  FLAG_DISPENSE_TEST_METHOD_WEIGHT 0x00001000  /* for dispense test -- using weight method */
#define  FLAG_DISPENSE_TEST_METHOD_TIME   0x00080000  /* for dispense test -- using  time method */
#define  FLAG_DISPENSE_TEST_START         0x00100000  /* for dispense test */
#define  FLAG_DISPENSE_TEST_MAN_REPEAT    0x00200000  /* for dispense test */
#define  FLAG_DUMP_FEED_TABLE             0x00400000  /* for print gate feed table */
#define  FLAG_DEFAULT_GATE                0x00800000  /* for diag. */
#define  FLAG_DEFAULT_BWH                 0x01000000  /* for diag. */
#define  FLAG_DEFAULT_MIXER               0x02000000  /* for diag. */
#define  FLAG_PCC_DEMO_LINE               0x04000000
#define  FLAG_COM_PORT_TEST				   0x00800000  /* for hardware com port test */
#define  FLAG_IO_TEST_1				         0x01000000  /* for hardware I/O test (w/o second io card)*/
#define  FLAG_IO_TEST_2				         0x02000000  /* for hardware I/O test (w/second io card */

/* FLAGS FOR BG SIMULATION DEFINITIONS*/
#define  FLAG_SIMU_WEIGH_MODULE        0x00000001   /* for each Hopper */
#define  FLAG_SIMU_LOADING             0x00000002
#define  FLAG_SIMU_DRIVE_MODULE        0x00000004
#define  FLAG_SIMU_HOPPER              0x00000008
#define  FLAG_SIMU_PULSES              0x00000001

#define  FLAG_DC_SIMULATE_WM           0x00000001  /* for Downcomer */
#define  FLAG_DC_SIMULATE_WM_HW_FAIL   0x00000002
#define  FLAG_DC_SIMULATE_WM_SW_FAIL   0x00000004
#define  FLAG_DC_SIMULATE_WM_OVERLOAD  0x00000008
#define  FLAG_DC_SIMULATE_DM           0x00000010
#define  FLAG_DC_SIMULATE_DM_HW_FAIL   0x00000020
#define  FLAG_DC_SIMULATE_DM_SW_FAIL   0x00000040
#define  FLAG_DC_SIMULATE_DM_MANUAL    0x00000080
#define  FLAG_DC_SIMULATE_DM_INHIBIT   0x00000100
#define  FLAG_DC_SIMULATE_BOTH         0x00000200

#define  FLAG_SIMULATE_HO              0x00000001
#define  FLAG_SIMULATE_SHO             0x00000001

#define  FLAG_SIMU_SYSTEM              0x00000100   /* for system */
#define  FLAG_SIMU_GATE                0x00000200
#define  FLAG_SIMU_BWH                 0x00000400
#define  FLAG_SIMU_MIXER               0x00000800
#define  FLAG_SIMU_WIDTH               0x00001000

/* FLAGS FOR BG CONTROL STATUS REGISTER */
/* DOWNCOMER*/
#define  FLAG_LOW_LOW_PROX_STATUS      0x00000008
#define  FLAG_LOW_PROX_STATUS          0x00000010
#define  FLAG_HIGH_PROX_STATUS         0x00000020
#define  FLAG_HIGH_HIGH_PROX_STATUS    0x00000040
/* HO */
#define  FLAG_RAMPING                  0x00000001
/* SYSTEM*/
#define  FLAG_MINOP_NEED_TO_REBOOT     0x00000001



