#ifndef PRINTERDEF_H
#define PRINTERDEF_H

#define PRN_PrintOK                  0
#define PRN_ConnectErr               1
#define PRN_InvalidArg               2
#define PRN_Busy                     3
#define PRN_LibErr                   4
#define PRN_Failed                   5
#define PRN_NotConnected             6
#define PRN_NoPaper                  7
#define PRN_BusyCountBeforeErr       10

#define Def_StatePrinterInit         1
#define Def_StatePrinterStatus       2
#define Def_StatePrinterReady        3
#define Def_StatePrintSummary        4
#define Def_StatePrintAlarms         5
#define Def_StatePrintTotals         6
#define Def_StatePrintRecipe         7
#define Def_StatePrintAllRecipes     8
#define Def_StatePrintCurrrentAlarms 9
#define Def_StatePrintCurrrentEvents 10
#define Def_StateSerialPrintInit     11
#define Def_StateDiagnostics         12
#define Def_StateInventoryTotals     13

#define Def_StateGetVCHandle         0
#define Def_StateVChandleDone        1


#define Def_PrintALLogHeader     0
#define Def_PrintALLogFirst      2
#define Def_PrintALLogNext       3
#define Def_PrintALLogLast       5
#define Def_PrintALLogDone       6

#define Def_BuildHeader          0
#define Def_PrintHeader          1

#define Def_AlmFetch             0
#define Def_AlmPrint             1

#define SERIAL_PRINTER_INIT      0
#define SERIAL_PRINTER_BUILD     1
#define SERIAL_PRINTER_WRITE     2
#define SERIAL_PRINTER_CLOSE     3
#define SERIAL_PRINTER_WAIT      4

#define PRINT_LOG_INIT           0
#define PRINT_LOG_OPEN           1
#define PRINT_LOG_FILESIZE       2
#define PRINT_LOG_WRITE          3 
#define PRINT_LOG_CREATE         4
#define PRINT_LOG_CLOSE          5
#define PRINT_LOG_DONE_WRITING   6
#define PRINT_LOG_READ           7 
#define PRINT_LOG_RENAME         8
#define PRINT_LOG_DELETE         9

#define PRINT_TO_NONE            0
#define PRINT_TO_SERIAL_PRINTER  1
#define PRINT_TO_CF_CARD         2
#define PRINT_TO_USB1_DRIVE      3
#define PRINT_TO_USB2_DRIVE      4

#define ERROR                    -1
#define PRINT_LOG_MAX_DESC       80
#define PRINT_LOG_MSG            10

typedef struct 
{
   int  inptr;
   int  outptr;
   char desc[PRINT_LOG_MSG][PRINT_LOG_MAX_DESC];
} config_PrintLog;

config_PrintLog sPrintLog;

#endif
