#include "pcc_math.h"

short int add_to_table(data_table *table,ordered_pair *data)
{
    short int hidx;
    short int lidx;
    int idx;
    int retval=0;

    if(table->current_length < table->max_length)
    {
        find_table_idx(table,data->x, &hidx, &lidx);

        if(lidx >=0)
        {
            /* move table entries from lidx+1 to end one entry down */
            idx = lidx+1;
            table_insert_space(table,idx);
        }
        else
        {
            if(hidx >= 0)
            {
                idx = 0;
                table_insert_space(table,idx);
            }
            else
            {
                /* there are no entries in the table */
                /* add this as the first entry */
                idx = 0;
            }
        }
        ++table->current_length;
        table->data[idx].x = data->x;
        table->data[idx].y = data->y;

    }
    else
    {
        retval = 1;
    }

    return retval;
}


