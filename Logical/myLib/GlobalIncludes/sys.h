/************************************************************************/
/*
   File:  sys.h

   Description:


      $Log:   F:\Software\BR_Guardian\sys.h_v  $
 *
 *    Rev 1.20   Jul 29 2008 08:53:26   jlr
 * added #define for debugging Allen Bradley communications
 *
 *    Rev 1.19   Jul 28 2008 11:02:32   jlr
 * added a global #define that enables or disables the comparison
 * for determining which piece of debug information should be turned on
 * or off.  This does NOT affect the debug information, only whether or not the
 * shr_global.dbg_what and shr_global.dbg_who values are used to determine whether or not
 * to display a particular piece of debug information
 *
 *    Rev 1.18   Jun 06 2008 15:56:44   Barry
 * Made changes
 *
 *    Rev 1.17   May 06 2008 15:07:30   FMK
 * Setup remote port settings in visualization as
 * well as config_super.
 *
 *    Rev 1.16   May 06 2008 09:32:30   FMK
 * The frame size reference was incorrectly defined.
 *
 *    Rev 1.15   May 02 2008 15:12:28   FMK
 * Changed drive type for extruder and haul off drives.
 *
 *    Rev 1.14   Apr 30 2008 14:59:46   Barry
 * Changed MAX_AD_VALUE to 7fffff and add
 * MAX_BIT_RANGE
 *
 *    Rev 1.13   Apr 16 2008 09:10:26   Barry
 * added MIXER_CHECK_MATERIAL state.
 *
 *    Rev 1.12   Apr 09 2008 16:25:46   FMK
 * Added defines for Extrusion/HO control.
 *
 *    Rev 1.11   Apr 09 2008 10:52:02   FMK
 * Added recipe error definitions and demo defintions.
 *
 *    Rev 1.10   Mar 26 2008 10:26:42   FMK
 * Added define for remote protocol modbus_serial.
 *
 *    Rev 1.9   Mar 24 2008 14:30:12   FMK
 *
 *
 *    Rev 1.8   Mar 21 2008 10:39:58   FMK
 * Fixed issue with menu choices and application.
 * Reduced defines to zero based for application.
 * Corrected visibility issue with extrusion control
 * used/unused.
 *
 *    Rev 1.7   Mar 21 2008 09:52:48   Barry
 * Changed TICKSPERSEC and FTICKSPERSEC to 1000
 *
 *    Rev 1.6   Mar 13 2008 12:44:44   vidya
 * Added BLENDER_RECIPE and EXTRUDER_RECIPE
 *
 *    Rev 1.5   Mar 12 2008 14:24:36   Barry
 * Added TICKSPERCEC and FTICKSPERSEC
 *
 *    Rev 1.4   Mar 06 2008 10:08:44   Barry
 * Added Mixer state definitions
 *
 *    Rev 1.3   Feb 06 2008 10:25:54   Barry
 * Added SUSPEND and RESUME modes
 *
 *    Rev 1.2   Jan 25 2008 14:07:28   FMK
 * Made changes to the setup pages to allow the
 * user to store/restore configuration files to a thumb
 * drive installed in the USB ports.
 *
 *    Rev 1.1   Jan 23 2008 14:01:22   FMK
 * Added define for invalid file device/path.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/


/* JLR moved these to the top so I could use them in the definition of
 * SETTLE_TIME */
#define TICKSPERSEC     1000
#define FTICKSPERSEC    1000.0


#define Frame_Size_4    0
#define Frame_Size_6    1
#define Frame_Size_8    2
#define Frame_Size_12   3
/************************************************************************/
/* unit definitions                                                     */
/************************************************************************/
#define ENGLISH             0       /* units are english                */
#define METRIC              1       /* units are metric                 */
#define USER_DEFINED        2       /* units are user defined           */

/************************************************************************/
/* port definitions                                                     */
/************************************************************************/
#define REMOTE_PORT     1
#define REMOTE_INT_PORT 2
#define MODEM_PORT      3
#define PRINTER_PORT    4
#define WIDTH_PORT      5
#define NOT_AVAILABLE   99
#define MAX_PORTS       8

/************************************************************************/
/* MACH type definitions                                                */
/************************************************************************/
#define GRAVITROL      1
#define BGBC_BLENDER   2
#define BE_BLENDER     3
#define BATCH          4
#define WX_BATCH       5
#define WE_BATCH       6

/************************************************************************/
/* Control Mode definitions                                             */
/* cs->control_mode = (wtp_mode+1)*3 + (ltp_mode+1)                     */
/************************************************************************/
#define MONITOR             -1
#define UNUSED              0
#define CONTROL             1

#define MWTP_MLTP           0   /* wtp monitored, ltp monitored         */
#define MWTP_ULTP           1   /* wtp monitored, ltp unused            */
#define MWTP_CLTP           2   /* wtp monitored, ltp controlled        */
#define UWTP_MLTP           3   /* wtp unused, ltp monitored (illegal)  */
#define UWTP_ULTP           4   /* wtp unused, ltp unused (illegal)     */
#define UWTP_CLTP           5   /* wtp unused, ltp controlled (illegal) */
#define CWTP_MLTP           6   /* wtp controlled, ltp monitored        */
#define CWTP_ULTP           7   /* wtp controlled, ltp unused           */
#define CWTP_CLTP           8   /* wtp controlled, ltp controlled       */

#define REC_NONE                          0
#define REC1_WTP                          1
#define REC1_WPL_WPA                      2
#define REC1_WTP_WPL_WPA                  3
#define REC1_WPL_WPA_THICK                4
#define REC1_WPL_OD_THICK                 5
#define REC1_WTP_WPL_WPA_THICK            6
#define REC1_WTP_WPL_OD_THICK             7
#define REC1_WPL_ID_OD_THICK              8
#define REC1_WTP_WPA_WPL_ID_OD_THICK      9

#define REC2_WIDTH                        1
#define REC2_ID                           2
#define REC2_OD                           3
#define REC2_THICK                        4
#define REC2_ID_OD                        5

#define REC3_WTP                          1
#define REC3_LTP                          2
#define REC3_WTP_LTP                      3

#define PARTS      '\x00'
#define PERCENT    '\x01'
#define DISABLED   '\x00'
#define ENABLED    '\x01'

/************************************************************************/
/* Recipe type definitions                                              */
/************************************************************************/
#define NORMAL_RECIPE            0
#define MOLDING_RECIPE           1

/************************************************************************/
/* Recipe use definitions                                              */
/************************************************************************/
#define COLOR_HOP              0
#define VIRGIN_HOP             1
#define ADDITIVE_HOP           2
#define NONE_HOP               3

/************************************************************************/
/* Algorithm definitions                                                */
/************************************************************************/
#define STANDARD            0
#define STATISTIC           1

/************************************************************************/
/* Recipe Mode definitions                                              */
/*   recipe mode is xyz, where x is main recipe entry, y is other       */
/*    controlled element (-,wtp,ltp), and z is constant used (-,OD,ID,  */
/*    thickness, or width)                                              */
/************************************************************************/
                                /* NONE is defined as 0 elsewhere       */
#define WTP             1       /* weight throughput                    */
#define LTP             2       /* length throughput                    */
#define WPL             3       /* weight per running length            */
#define THICKNESS       4       /* thickness of sheet, film, pipe wall  */
#define OD              5       /* OD of pipe, wire                     */
#define ID              6       /* ID of pipe                           */
#define WIDTH           7       /* width of sheet, layflat              */
#define WPA             8       /* weight per area                      */
#define MAX_RECIPE      9       /* number of elements in recipe         */

/************************************************************************/
/* Application definitions                                              */
/************************************************************************/
#define BLOWN_FILM      0       /* blown film line - has layflat        */
#define SHEET           1       /* sheet line (no diameter)             */
#define PIPE_AND_TUBING 2       /* tubing - no "layflat"                */
#define WIRE_AND_CABLE  3       /* tubing process w/ no i.d.            */
#define PROFILE         4       /* profile - no thickness allowed       */

/************************************************************************/
/* Scrap refeed definitions for cs->refeed                              */
/************************************************************************/
#define TRIM_ONLY       1       /* flag for "faking" by constant scrap  */
#define GRAVIFLUFF      2       /* flag for we have gravifluff          */

/************************************************************************/
/* MACH type definitions                                                */
/************************************************************************/
#define GRAVITROL      1
#define BGBC_BLENDER   2
#define BE_BLENDER     3
#define BATCH          4
#define WX_BATCH       5
#define WE_BATCH       6

/************************************************************************/
/* width controller definitions - cs->wth_type                          */
/************************************************************************/
#define KUNDIG            0
#define KUNDIG_PROGRAM    "kundig"
#define DR_JOSEPH         1
#define DR_JOSEPH_PROGRAM "drjoseph"
#define DR_JOSEPH_ETHERNET "drj_eth"

/************************************************************************/
/* General Stuff                                                        */
/************************************************************************/
#define FALSE     0     /* these codes are multipurpose  */
#define TRUE      1     /* these codes are multipurpose  */
#define VISIBLE   1     /* these codes are multipurpose  */
#define INVISIBLE 0     /* these codes are multipurpose  */
#define ABORT     2     /* these codes are multipurpose  */
#define OFF       0     /* these codes are multipurpose  */
#define ON        1     /* these codes are multipurpose  */
#define FAIL      0     /* these codes are multipurpose  */
#define SUCCESS   1     /* these codes are multipurpose  */
#define DONE      2     /* these codes are multipurpose  */
#define NOTDONE   3     /* these codes are multipurpose  */
#define NOTFOUND  4     /* these codes are multipurpose  */
#define PATHNOTFOUND  5     /* these codes are multipurpose  */
#define NOERROR   0

#define NONE      0     /* these codes are multipurpose  */

#define SINGLE_SOLENOID    0
#define DOUBLE_SOLENOID    1

#define ENGLISH_LANGUAGE   0     /* these are for dual language indexing*/
#define GERMAN_LANGUAGE    1     /* switched w/ dutch because of error  */
#define DUTCH_LANGUAGE     2
#define FRENCH_LANGUAGE    3
#define SPANISH_LANGUAGE   4
#define PORTUGESE_LANGUAGE 5
#define FINNISH_LANGUAGE   6
#define SLOVAC_LANGUAGE    7
#define CHINESE_LANGUAGE   8
#define ENDOFDATA          (0xFFFF)
#define NUMBER_OF_LANGS    8

/************************************************************************/
/* Control loop tuning stuff                                            */
/************************************************************************/
#define TUNE_MANUAL     0
#define TUNE_AUTOMATIC  1

/************************************************************************/
/* GATE definitions                                                     */
/************************************************************************/
#define NO_GATE -1                    /* value for s->curgate if no gate*/
                                      /* is currently feeding           */
#define STD_ALGORITHM   0
#define SINGLE_SHOT     1
#define DUAL_SHOT       2

/************************************************************************/
/* Drive types/protocols for devs & net                                 */
/************************************************************************/
#define PCC_BR_AO           0   /* B&R modules for analog output ctrl   */
#define PCC_BR_AOAI         5   /* B&R modules for analog output/analog input for bumpless */
#define PCC_BR_AODI         2   /* B&R modules for analog output/digital input for bumpless */
#define PCC_BR_AID0         3   /* B&R modules for analog input/digital output for bumpless */
#define PCC                 4   /* PCC Type I - internal reference only */
#define AB_PLC_DRIVE        1   /*Allan Bradley Encapsulated DF1 Drive  */
#define MODBUS_PLC_DRIVE    6   /* Modbus TCP Drive Type                */
#define PCC_TYPE_II         7   /* c1200 that follows external ref      */

#define INTERNAL_CTRL       0
#define EXTERNAL_CTRL       1

#define AUTO_CONTROL        0
#define MANUAL_BACKUP       1

/************************************************************************/
/* Control loop types                                                   */
/************************************************************************/
#define PCC_LOSS_IN_WT  0       /* loss in wt using our alg             */
#define PCC_LENGTH      1       /* length contol using our alg          */
#define GENERIC         2       /* generic control loop in scaled units */

/************************************************************************/
/* default speeds for no rate speed auto runs                           */
/************************************************************************/
#define HO_NRS_SPD      25.0
#define LIW_NRS_SPD     25.0

/************************************************************************/
/* Hopper types for Guardian                                                      */
/************************************************************************/
#define NORMAL  0
#define REGRIND 1      /* Not sure how this could interfer with the define of recipe type */
                       /* This needs to be 1 to work properly with visualization */
					   
					   
/************************************************************************/	
/* Hopper types for AutoBatch                                                      */


#define MEDIUM     2        /* REO VIB */
#define SMALL      3 
#define SCR_MEDIUM 4
#define SCR_SMALL  5

#define STANDARD_MAIN 0

#if 0
#define REGRIND_MENU 1 /* NOTE: This is for the hopper type menu selection */
#define REGRIND      4 /* NOTE: This is 4 because of the HMI interface     */
                       /*    changing this value will affect the recipe    */
                       /*    entry panel. values 1,2, and 3, are used if   */
                       /*    molding recipes are used                      */
#endif
#define NON_CRITICAL 2 /* treated like normal for recipe, if goes low      */
                       /* system skips to next hopper                      */
/************************************************************************/
/* Mixer motor modes                                                    */
/************************************************************************/
#define MIX_BY_TIME 0
#define ALWAYS_ON   1
#define NO_MIXER    2

/************************************************************************/
/* Mixer gate modes                                                     */
/************************************************************************/
#define CLOSE_WHILE_MIXING  0
#define ALWAYS_OPEN         1

/************************************************************************/
/* Mixer material request source                                        */
/************************************************************************/
#define MATERIAL_REQUEST_MIXER_PROX 0
#define MATERIAL_REQUEST_EXTERNAL   1
#define MATERIAL_REQUEST_GRAVITROL  2

/************************************************************************/
/* Relay Types                                                          */
/************************************************************************/
#define NORMALLY_OPEN      0
#define NORMALLY_CLOSED    1

/************************************************************************/
/* Haul Off Gear Selection                                              */
/************************************************************************/
#define NO_GEAR_INPUT      0
#define GEAR_IN_RECIPE     1
#define DISCRETE_GEARS     2
#define ENCODED_GEARS      3

/************************************************************************/
/* Operating Mode definitions for oper_mode                             */
/************************************************************************/
#define BLN_OPER_MODE         shr_global.bln_oper_mode
#define EXT_HO_OPER_MODE      shr_global.ext_ho_oper_mode
#define OPER_MODE             (shr_global.bln_oper_mode|shr_global.ext_ho_oper_mode)

#define NULL_MODE            FALSE    /* No Mode/Command preset         */
#define PAUSE_MODE            'P'     /* system paused, no drives on    */
#define MAN_MODE              'M'     /* manual mode on                 */
                                      /* idividual gate speeds          */
#define AUTO_MODE             'A'     /* auto mode                      */
#define WAITING_FOR_NEW_FEED  'W'     /* waiting for next feed command  */
#define FAST_AUTO_MODE        'F'     /* fast auto mode                 */
#define MONITOR_MODE          'B'     /* monitor wtp only               */
#define CALIBRATE_MODE        'C'     /* calibrate feeders, batch       */
#define PURGE_MODE            'X'     /* purge mode, batch              */
                                      /* blender only                   */
#define DISPENSING_MODE       'H'
#define NEW_MODE              'Z'
#define SUSPEND               's'     /* Suspend loop                   */
#define RESUME                'e'     /* Resume loop                    */
#define REINIT                'J'     /* reinitialize hardware settings */

#define DUMP_DEBUG_DATA       'd'     /* command to dump debug data     */

/************************************************************************/
/* Mixer Definitions                                                    */
/************************************************************************/
                                         /* counting the mixer revolutions */
#define TIME_MIXER            	'T'     /* mixing length by time          */
#define MIXER_DUMPING         	'D'
#define MIXER_WAITING_EMPTY   	'E'     /* mixer empty & waiting for batch*/
#define MIXER_WAITING_FULL    	'F'     /* mixer is full and waiting for  */
                                         /* material request               */
#define MIXER_UNKNOWN_STATE   	'?'     /* mixer is in an unknown condition*/
#define MIXER_CHECK_MATERIAL   	'C'     /* mixer is checking for material */

/************************************************************************/
/* PAUSE Commands                                                       */
/************************************************************************/
#define PAUSE_NOW             'N'
#define PAUSE_AT_END_OF_BATCH 'B'

/************************************************************************/
/* Operating Mode definitions for shr_LIW[GRAVIFLUFF]->scrap_mode       */
/************************************************************************/
#define MAX_SCRAP             'S'     /* used for liw_flf only          */
#define MAX_VIRGIN            'V'     /* used for liw_flf only          */
#define CUT_BACK              'C'     /* used for liw_flf only          */

/************************************************************************/
/* Definitions for shr_LOOP[].GATE->feed_state                          */
/************************************************************************/
#define START                 'I'
#define PULSE_GATE            'P'
#define PULSE_GATE_DELAY      'd' //drt delay 
#define WAITING_FOR_GATE      'G'
#define WAITING_FOR_SETTLING  'S'
#define CHECK_WT              'W'
#define GATE_DONE             'D'
#define PREPULSING			   'R' /* all the p's were taken */	
#define PREPULSE_GATE         'p'
#define WAITING_FOR_PREPULSE  'g'
#define CHECKING_PREPULSE     'c'
#define ZERO_CALC             'Z'

/************************************************************************/
/* Security definitions                                                 */
/************************************************************************/
#define NO_LOCK               (unsigned char)0x00
#define PAUSE_LOCK            (unsigned char)0x01
#define OBSERVER_LOCK         (unsigned char)0x00   /* only used for web*/
#define OPERATOR_LOCK         (unsigned char)0x01
#define MAINTENANCE_LOCK      (unsigned char)0x02
#define SUPERVISOR_LOCK       (unsigned char)0x03
#define SERVICE_LOCK          (unsigned char)0x04
#define SECURITY_LEVEL_MASK   (unsigned char)0xf8

/************************************************************************/
/* pe_needed[] flag index definitions, 0-PE_SIZE valid                  */
/*   used to flag what security level is needed to access function      */
/************************************************************************/
#define PE_SETUP              0
#define PE_CALIBRATE          1
#define PE_CHANGE_RECIPE      2       /* needed to get into recipe      */
#define PE_STORE_RECIPE       3
#define PE_STOP               4
#define PE_RUN                5
#define PE_EXT_MANUAL         6      /* Extruder manual backup */
#define PE_CLEAR_SHIFT        7      /* flags for clearing totals      */
#define PE_CLEAR_INVEN        8
#define PE_CLEAR_RESIN        9
#define PE_CLEAR_RECIPE       10
#define PE_BLN_MANUAL         11      /* Blender manual backup */
#define PE_ALARM_CONTROL      12      /* User level to clear alarms     */
#define PE_SELECT_RECIPE      13      //G2-662
#define PE_TOTAL_ENTRIES      14      /* change when adding add'l       */

/* calibration */
#if 1
#define NUMBER_OF_WT_READINGS 100  /* changed this to more accurately reflect the new shorter task class timings */
#else
#define NUMBER_OF_WT_READINGS 10   /* added a check of wt stable to this fcn */
#endif
/***************************************************************************/
/* A/D Definitions                                                         */
/***************************************************************************/
#define MAX_AD_VALUE     0x007fffff    /* maximum a/d value for 24 bit (signed) */
#define MAX_LC_AD_VALUE  5075108       /* derived from 2.2mv/V LC & A/D w/ 4mV/V gain + 10% */
#define MAX_BIT_RANGE    150           /* range of bit values when stable  */
#define WT_STABLE_TARGET 50            /* # of consecutive wt stable readings sought */

/* the following are error conditions for the calibrate process. These  */
/* must be retained in the same order to facilitate the text errors     */
/* used in the touch panel error window/language file                   */
#define ERR_NOTPAUSED         0x01
#define ERR_HARDWARE          0x02
#define ERR_UNSTABLEWEIGHT    0x04
#define ERR_WEIGHTTOOSMALL    0x08
#define ERR_AD_LOCKED         0x10
#define ERR_UNKOWN            0x20

#define NOT_USED              0
#define ILLEGAL_LOOP          127     /* illegal loop# for tests, same as LIW */


/* #defines for B&R variables */

#define brNOERROR           0    /* function return of 0 indicates completion of task */
#define brFILENOTFOUND  20708    /* attempted to open non-existent file */
#define brPATHNOTFOUND  20709    /* invalid file path illegal file device */
#define brBUSY          65535    /* function is still processing */
#define brSystemError   20799    /* system error code, FileIOGetSysError for details */

/* these values are for copying and clearing parts of the recipe */
#define BLENDER_RECIPE      0x00000001
#define EXTRUDER_RECIPE     0x00000002
#define DENSITY_ONLY        0x00000004
#define ALL_RECIPE          0xffffffff

#define Modbus_Serial   1
#define Abdhp_Serial    2
#define Kundig_Serial   3

/************************************************************************/
/* service codes definitions  (0 = OFF) for shr_global.dbg_what                 */
/*  note that dbg_who is 1<<net# for control loops, not used for others */
/************************************************************************/
#define ALL_DATA                  0xffffffff
#define RAW_TIME_WT               0x00000001
#define UPDATE_COAST              0x00000002
#define UPDATE_RATE               0x00000004
#define REMOTE_TCPIP_DATA         0x00000008
#define GATE_PARAMETERS           0x00000010
#define GATE_ACTIONS              0x00000020
#define BATCH_STATISTICS          0x00000040
#define BATCH_ACTIONS             0x00000080
#define OUTPUT_BATCH_WTP_DATA     0x00000400
#define ALLEN_BRADLEY             0x00000800
#define BATCH_WEIGH_HOPPER        0x00001000
#define MIXER                     0x00002000
#define OUTPUT_REMOTE_READ_DATA   0x10000000
#define OUTPUT_REMOTE_WRITE_DATA  0x20000000
#define ECHO_REMOTE_DATA          0x40000000
#define PROC_DATA                 0x80000000

/************************************************************************/
/* Recipe error definitions                                             */
/************************************************************************/
#define RECIPE_UNDERSPEED_AL    0x0001    /* recipe may cause underspeed*/
#define RECIPE_OVERSPEED_AL     0x0002    /* recipe may cause ovespeed  */
#define RECIPE_SMALL_AUGER      0x0004    /* auger too small for recipe */
#define RECIPE_LARGE_AUGER      0x0008    /* auger too large for recipe */
#define RECIPE_NO_CAL           0x0010    /* this hopper not calibrated */
#define RECIPE_BAD_THROUGHPUT   0x0020    /* total LTP or WTP is bad    */
#define RECIPE_BAD_PROD         0x0040    /* bad production recipe msg  */
#define RECIPE_NOT_100          0x0080    /* recipe does not add to 100 */
#define RECIPE_NO_RATESPEED     0x0100    /* device has no rate vs speed*/

/************************************************************************/
#define REM_CAL_NO_COM             0x00000001
#define REM_CAL_UNSTABLE           0x00000002
#define REM_CAL_RUNNING            0x00000004
#define REM_CAL_NO_WEIGHT          0x00000008

/* comm port definitions */
/* baud */
#define Comm_Baud_1152k    0
#define Comm_Baud_576k     1
#define Comm_Baud_384k     2
#define Comm_Baud_192k     3
#define Comm_Baud_9600     4
#define Comm_Baud_4800     5
#define Comm_Baud_2400     6
#define Comm_Baud_1200     7
#define Comm_Baud_600      8

/* parity */
#define Comm_Parity_None   0
#define Comm_Parity_Even   1
#define Comm_Parity_Odd    2
#define Comm_Parity_Mark   3
#define Comm_Parity_Space  4

/* stop bits */
#define Comm_Stop_1     0
#define Comm_Stop_2     1

/* data bits */
#define Comm_Data_7  0
#define Comm_Data_8  1

/*
 * defines for return value from rezero_hopper_wt function
 */
#define REZERO_OK       1
#define OFFSET_TOO_BIG  2
#define TIMER_EXPIRED   3
#define REZERO_NOT_DONE 4

/* number of times we should try to rezero the batch weight hopper */
#define NUM_REZERO_RETRIES 1

#define ACTUATE 16

#define ALARM_EVENTS              0x00000008
