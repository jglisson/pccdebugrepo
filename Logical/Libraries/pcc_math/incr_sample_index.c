#include <stdio.h>
#include "pcc_math.h"

signed short incr_sample_index(samples *s_data,signed short idx)
{
    if(++idx >= s_data->length) idx = 0;
    return idx;
}


