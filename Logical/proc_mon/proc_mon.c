#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  proc_mon.c

   Description:  Main task that starts the system and monitors the status
      of the other tasks/processes.


      $Log:   F:\Software\BR_Guardian\proc_mon.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.procMon to toggle debug information in this file
 *
 *    Rev 1.16   May 01 2008 13:05:40   FMK
 * ABTCP task has been added to the system. So
 * now if abtcp protocol not enabled then this task
 * is suspended.
 *
 *    Rev 1.15   Apr 14 2008 13:35:58   FMK
 * Renamed the extruder loop so the suspend function
 * for the extruder loop had to change.
 *
 *    Rev 1.14   Apr 11 2008 11:52:40   FMK
 * Task status will suspend the modbus tcp if
 * not enabled.
 *
 *    Rev 1.13   Apr 11 2008 08:56:20   FMK
 *
 *
 *    Rev 1.12   Mar 26 2008 14:19:44   FMK
 * Needed more cleanup for task suspending.
 *
 *    Rev 1.11   Mar 26 2008 12:35:50   FMK
 * Added a task suspend function. Based on the config.
 * a task may be removed from execution freeing up
 * processing time.
 *
 *    Rev 1.10   Mar 24 2008 14:30:12   FMK
 *
 *
 *    Rev 1.9   Mar 21 2008 14:59:14   FMK
 * HMI.h cannot be in here. HMI structure is LOCAL only to
 * to interface task.
 *
 *    Rev 1.8   Mar 12 2008 14:20:24   FMK
 * PLC Rack changed configuration. Updated monitoring
 * of PLC rack modules for hardware errors.
 *
 *    Rev 1.7   Mar 07 2008 10:35:44   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.6   Feb 04 2008 15:54:08   FMK
 * Initialized extruder oper mode.
 *
 *    Rev 1.5   Feb 04 2008 09:45:14   FMK
 * Added checking for the PLC rack modules. Generate
 * error condition if module missing/bad base on
 * configuration.
 *
 *    Rev 1.4   Jan 30 2008 13:53:48   FMK
 * Fixed problem with path for PVCS comments
 *
 *    Rev 1.3   Jan 25 2008 11:02:32   FMK
 * Changed names of global variables to reflect
 * naming standard.
 *
 *    Rev 1.2   Jan 15 2008 15:21:40   FMK
 * Change systemloaded initialization. Was not loading
 * configuration on startup.
 *
 *    Rev 1.1   Jan 14 2008 11:02:50   FMK
 * Changed the signal variables to a structure format.
 *
 *    Rev 1.0   Jan 11 2008 10:33:38   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include "globalinc.h"

#include <bur/plctypes.h>
#include "hmi.h"
#include "file_rw.h"
#include "signal_pcc.h"
#include "alarm.h"
#include "plc.h"
#include "usb.h"
#include "demo.h"
//#include "security.h"
#include "recipe.h"

extern int  default_system();
extern void default_internet();
extern void Recipe_Init();

void init_dev(void);

extern void visible(unsigned char *Runtime,unsigned char state);

void Security_Reboot(); /* copied over from security.c */
void Verify_PLC_Rack_Status();
void link_to_data();
void SET_ALARM(int Device, unsigned int Alarm);
void CLEAR_ALARM(int Device, unsigned int Alarm);
void TaskControl();
int SuspendTask(char *TaskName, UDINT *TaskID);
_LOCAL unsigned char EXT_Analog_Failure_Counter; 
_LOCAL unsigned char EXT_Analog_Failure_Logger;

typedef struct TaskStatus_Struct
{
   UDINT TaskID;
   unsigned char Suspended;
}TaskStatus_Type;

struct TaskList_Struct
{
   TaskStatus_Type ModBus_Serial;
   TaskStatus_Type AbdhP_Serial;
   TaskStatus_Type Profibus;
   TaskStatus_Type Modbus_TCP;
   TaskStatus_Type AB_TCP;
   TaskStatus_Type HO_Driver;
   TaskStatus_Type Ext_Driver;
   TaskStatus_Type SHO_Loop;
   TaskStatus_Type HO_Loop;
   TaskStatus_Type Ext_Loop;
   TaskStatus_Type Print;
   TaskStatus_Type Gate[MAX_GATES];
   TaskStatus_Type Smtp;
	TaskStatus_Type Kundig;
};
struct TaskList_Struct TaskList;

unsigned long int gVCHandle;         /* a handle to the visual component */
unsigned int  gState_TouchCalib;     /* current state of touch calibration process*/
void CalibrateTouch(void);
CfgGetIPAddr_typ gGetIP;    /* instance variable for getting the ip address */
unsigned char gEthDevice[16];

unsigned char procmon_initialized = 0;
int gStatus_SuspendGates;
extern void default_recipes();
extern int Restore_Recipes(void);

extern void copy_recipe(recipe_struct *,recipe_struct *,int);

TON_typ TON_01;   /* Timer for delay between dispense tests */
unsigned int Delay_X_MSecs(unsigned int DelayMSecs);
TIME ElapsedTime;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;


/************************************************************************/
/*
   Function:  proc_mon_init()

   Description:  defaults the system and brings it out of reset.
                                                                        */
/************************************************************************/
_INIT void proc_mon_init(void)
{   
   gState_TouchCalib = 0;
   gVCHandle = 0;
   gStatus_SuspendGates = TRUE;
}

/************************************************************************/
/*
   Function:  proc_mon_cycle()

   Description:  main cyclic task for the interface function
                                                                        */
/************************************************************************/
_CYCLIC void proc_mon_cycle(void)
{
   int Result;    /* Return value of function call */
   static int loadsystem = 0;
   int status;
   int index;

   if (!procmon_initialized)
   {
      init_dev();
      procmon_initialized = 1;
   }

   if (!gTouchCalib)
   { 
      if (HMI.CurrentPage != pg99_touch_calib)
      {
         HMI.ChangePage = pg99_touch_calib;
         visible(&HMI.SetupRuntime2,TRUE); /* show aux_alarm_time*/
      }
      if (Delay_X_MSecs(5000) == TRUE) 
      {
         visible(&HMI.SetupRuntime2,FALSE);  /* hide aux_alarm_time */
         CalibrateTouch();
      }
   }
   else
   {
      if (HMI.CurrentPage == pg99_touch_calib)
	   {	
	  	   if (!gSetupWizzardHadRun)
	      {
			   if (HMI.CurrentPage != pg990_SetupWizzard)
		   	{
				   brsstrcpy((UDINT)sWizSetupInfoText, (UDINT)"Before using the blender,\nsome critical parameters must be set!");
				   HMI.ChangePage = pg990_SetupWizzard;
			   }
		   }
		   else
		   { 
            HMI.CurrentPage = pg000_MainPage;
            HMI.ChangePage  = pg000_MainPage;
		   }
	   }

	   if (cfg_super.EnteredPassword != 0)
      {
           if ((cfg_super.TimeOut > 0) & (cfg_super.TimeOut < 60000)) /* if more than zero and less than 1 minute GTF */
              cfg_super.TimeOut = 60000;  /* min value is 1 minute */
           else 
           {
              if ((elap_time(cfg_super.EnteredPassword) > (unsigned int)cfg_super.TimeOut) && (cfg_super.TimeOut > 60000)) 
              {
                 cfg_super.current_security_level = cfg_super.minimum_security_level;
                 visible(&HMI.pg000_Lang_Layer_On, FALSE);  /* Turn off language layer GTF */
                 visible(&HMI.pg000_Debug_Layer_On, FALSE); /* Turn off debug layer GTF    */
                 HMI.CurrentPage = pg000_MainPage;
                 HMI.ChangePage  = pg000_MainPage;
                 cfg_super.EnteredPassword = 0;
             }
         }
      }

      if (Signal.DefaultSystem == TRUE)
      {
         Result = default_system();
         Signal.DefaultSystem  = FALSE;
         Signal.ChangeIPAddr   = TRUE;
         Signal.ChangeSubNet   = TRUE;
         Signal.ChangeHostName = TRUE; //G2-620;
         HMI.CurrentPage = pg000_MainPage;
         HMI.ChangePage  = pg000_MainPage;
      }
      if (!Signal.SystemLoaded)   /* the system is not yet loaded so need to perform the tasks required to do the load */
      {
         if (!loadsystem)
         {
            loadsystem = LOADIP;
            Signal.IPLoaded = FALSE;
         }
         switch (loadsystem)
         {
            case LOADIP:
               if (!Signal.IPLoaded)
               {
                  #ifdef PP65
                  brsstrcpy((UDINT)gEthDevice, (UDINT)"IF5");
                  #endif
                  #ifdef C70
                  brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
                  #endif
                  #ifdef C70_003
                  brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
                  #endif
                  gGetIP.enable = 1;
                  gGetIP.pDevice = (UDINT) &gEthDevice[0];
                  gGetIP.pIPAddr = (UDINT) &gIPAddress[0];
                  gGetIP.Len     = sizeof(gIPAddress);
                  CfgGetIPAddr(&gGetIP);
                  status = gGetIP.status;
                  if (!status)
                  {
                     Signal.IPLoaded = TRUE;
                     loadsystem = LOADFILE;
                  }
                  else if (gGetIP.status != brBUSY)
                  {
                     Signal.IPLoaded = FALSE;
                     loadsystem = LOADFILE;
                  }
               }
            break;
            case LOADFILE:
               if (gDataObj_Init == 0)
                  Result = FAIL;
               else
                  Result = Signal.DataObj_read;
               if (Result == SUCCESS)
               {
                  Signal.SystemLoaded = TRUE;
                  Signal.LoadTimeWt = TRUE;
                  Signal.DefaultLoaded = FALSE;
                  loadsystem = SYSTEMLOADED;
                  Signal.FILEIO_INUSE = FALSE;
                  Signal.DataObj_read = NOTDONE;
              }
               else if (Result == FAIL)   /* system did not retrieve properly */
               {
                  Signal.DefaultLoaded  = TRUE;
                  Signal.ChangeIPAddr   = TRUE;  /* make sure ip address is reset */
                  Signal.ChangeSubNet   = TRUE;
                  Signal.ChangeHostName = TRUE; //G2-620
                  /* NOTE: possible issue with setting the ip address after this default_system.
                     if we reset the ip_address then a system restart occurrs. A system restart
                     cannot occur unless we first store the configuration, otherwise this task
                     will fail in the retrieve again and then default the system, resetting the ip
                     again, will cause the system to start a circular process  */
                  Signal.LoadTimeWt = FALSE;
                  loadsystem = LOADDEFAULT;
                  Signal.FILEIO_INUSE = FALSE;
               }
            break;
            case LOADDEFAULT:
               for (index = 0; index < NUM_STRUCTURES; index++)
                  HMI.Load_status[index] = STRUCTURE_DEFAULT;
               Result = default_system(); /* error getting configuration, get to known state */
               if (Result == SUCCESS)
               {
                  Recipe_Init();
                  copy_recipe(&UI_RECIPE, BATCH_RECIPE_NOW, BLENDER_RECIPE);
                  copy_recipe(&BATCH_RECIPE_EDIT, BATCH_RECIPE_NOW, BLENDER_RECIPE);

                  Signal.SystemLoaded = TRUE;
                  loadsystem = SYSTEMLOADED;
               }
            break;
         }
         //HMI.Action_Input1 = gcActive;    /* select reset to minimum level action */
         //Security_Reboot();                 /* call security function to set security level */
      }
      else  /* The system is now loaded */
      {
         /* copy wiz data to global vars */
		 if (G_bWizCopyValuesToGlobal == TRUE)
		 {
		    G_bWizCopyValuesToGlobal = FALSE;
			 cfg_super.pg175_System_Size = G_u8WizBlenderSize;
          pg175_sys_size_change = 1; /* signal to setup that blender size has changed; it will trigger batch size recalculation */
			 Signal.StoreConfig = TRUE;
       }
		 Verify_PLC_Rack_Status();
         TaskControl();
      }
   }
//	if (cfg_super.current_security_level == cfg_super.minimum_security_level)
//		visible(&HMI.SetupLock,FALSE);     /* hidden CLEAR ALL  */
}

/************************************************************************/
/*
   Function:  SuspendTask()

   Description:  This function will suspend the specified task.
                                                                        */
/************************************************************************/
int SuspendTask(char *TaskName, UDINT *TaskID)
{
   UDINT st_ident;
   int Result;

   Result = FALSE;
   if (ST_ident(TaskName,0,&(st_ident)) == 0)
   {
      ST_tmp_suspend(st_ident);
      *TaskID = st_ident;
      Result = TRUE;
   }
   return(Result);
}

/************************************************************************/
/*
   Function:  TaskControl()

   Description:  This function runs cyclically. It will suspend any
         unecessary tasks.
                                                                        */
/************************************************************************/
void TaskControl()
{
   config_super *cs = &cfg_super;

/* the modbus serial task will suspend unless enabled */
   if(cs->profibus_enabled != TRUE)
   {
      if (!TaskList.Profibus.Suspended)
         TaskList.Profibus.Suspended = SuspendTask("profibus",&(TaskList.Profibus.TaskID));
   }

/* the modbus serial task will suspend unless enabled */
   if(cs->remote_protocol!=Modbus_Serial)
   {
      if (!TaskList.ModBus_Serial.Suspended)
         TaskList.ModBus_Serial.Suspended = SuspendTask("modbus",&(TaskList.ModBus_Serial.TaskID));
   }

/* the Allen Bradley DH plus serial task will suspend unless enabled */
   if(cs->remote_protocol!=Abdhp_Serial)
   {
      if (!TaskList.AbdhP_Serial.Suspended)
         TaskList.AbdhP_Serial.Suspended = SuspendTask("abdhp",&(TaskList.AbdhP_Serial.TaskID));
   }

/* the modbus ethernet task will suspend unless enabled */
   if(cs->mbenet_enabled != TRUE)
   {
      if (!TaskList.Modbus_TCP.Suspended)
         TaskList.Modbus_TCP.Suspended = SuspendTask("mbenet",&(TaskList.Modbus_TCP.TaskID));
   }

	/* the modbus ethernet task will suspend unless enabled */
	if(smtp_info.active == UNUSED)
	{
		if (!TaskList.Smtp.Suspended)
			TaskList.Smtp.Suspended = SuspendTask("Smtp",&(TaskList.Smtp.TaskID));
	}
	
	/* the modbus ethernet task will suspend unless enabled */
   if(cs->remote_protocol!=Kundig_Serial)
   {
      if (!TaskList.Kundig.Suspended)
         TaskList.Kundig.Suspended = SuspendTask("Kundig",&(TaskList.Kundig.TaskID));
   }

/* the allen bradley ethernet task will suspend unless enabled */
   if(cs->abtcp_enabled != TRUE)
   {
      if (!TaskList.AB_TCP.Suspended)
         TaskList.AB_TCP.Suspended = SuspendTask("abtcp",&(TaskList.AB_TCP.TaskID));
   }

/* for extrusion control there are two tasks that can be removed. The main control loop and the
   interface driver to the plc */
   if (cs->wtp_mode == UNUSED)
   {
      if (!TaskList.Ext_Driver.Suspended)
         TaskList.Ext_Driver.Suspended = SuspendTask("extdrive",&(TaskList.Ext_Driver.TaskID));
      if (!TaskList.Ext_Loop.Suspended)
         TaskList.Ext_Loop.Suspended = SuspendTask("ext_loop",&(TaskList.Ext_Loop.TaskID));
   }

/* for haul off control there are two tasks that can be removed. The main control loop and the
   interface driver to the plc */
   if (cs->ltp_mode == UNUSED)
   {
      if (!TaskList.HO_Driver.Suspended)
         TaskList.HO_Driver.Suspended = SuspendTask("ho_drive",&(TaskList.HO_Driver.TaskID));
      if (!TaskList.HO_Loop.Suspended)
         TaskList.HO_Loop.Suspended = SuspendTask("ho_loop",&(TaskList.HO_Loop.TaskID));
   }

/* for haul off control there are two tasks that can be removed. The main control loop and the
   interface driver to the plc */
   if (cs->sltp_mode == UNUSED)
   {
      if (!TaskList.SHO_Loop.Suspended)
         TaskList.SHO_Loop.Suspended = SuspendTask("sho_loop",&(TaskList.SHO_Loop.TaskID));
   }
}

/************************************************************************/
/*
   Function:  Verify_PLC_Rack_Status()

   Description:  This function monitors the modules that make up the plc
         i/o rack. If any module goes dead then a system alarm is used.
         In Diagnostics it is possible to see what module is bad.
                                                                        */
/************************************************************************/
void Verify_PLC_Rack_Status()
{
   config_super *cs = &cfg_super;
   char ValidAlarm;

   ValidAlarm = FALSE;
   if (cs->demo)
   {
      CLEAR_ALARM(TYPE_SYSTEM,HARDWARE_FAILURE_ALARM);
      return;
   }

		/* ST2 */
		if (PLC.StrainGauge[BWH_ADNum].ModuleOK != sts_present)
			ValidAlarm = TRUE;

   /* ST3 */
   if (PLC.DigIn[0].ModuleOK != sts_present)
      ValidAlarm = TRUE;

   /* ST4 */
   if (PLC.DigOut[0].ModuleOK != sts_present)
      ValidAlarm = TRUE;

#if 0
   /* communications port */
   /* ST5 */
   if (PLC.DigOut[0].ModuleOK != sts_present)
      ValidAlarm = TRUE;
#endif

   if (cs->total_gates > 4)
   {
      /* ST6 */
      if (PLC.DigOut[1].ModuleOK != sts_present)
         ValidAlarm = TRUE;
   }

   if (cs->control_loading == TRUE)
   {
      /* ST7 */
      if (PLC.DigIn[1].ModuleOK != sts_present)
         ValidAlarm = TRUE;

      /* ST8 */
      if (PLC.DigOut[2].ModuleOK != sts_present)
         ValidAlarm = TRUE;

      if (cs->total_gates > 6)
      {
         /* ST9 */
         if (PLC.DigOut[3].ModuleOK != sts_present)
            ValidAlarm = TRUE;
      }
   }

   if ((cs->wtp_mode == CONTROL) || (cs->ltp_mode == CONTROL))
	{
		if (cfgEXT.drive_type == PCC_BR_AODI)
		{
			/* ST9 used for bumpless transfer */
			if (PLC.DigOut[3].ModuleOK != sts_present)
				ValidAlarm = TRUE;
		}

		//      /* ST10 */
		//      if (PLC.StrainGauge[EXT_ADNum].ModuleOK != sts_present)
		//         ValidAlarm = TRUE;
         
			/* ST11 */
			if ((PLC.DigIn[2].ModuleOK != sts_present) && ((!cfgEXT.demo & DEMO_DRIVE_MOD) || (!cfgHO.demo & DEMO_DRIVE_MOD) || (!cfgSHO.demo & DEMO_DRIVE_MOD)))
				ValidAlarm = TRUE;

			/* ST12 */
			if ((cfg_super.wtp_mode != CONTROL) && (cfg_super.ltp_mode != CONTROL))
			{
				if (PLC.RelayOut.ModuleOK != sts_present)
					ValidAlarm = TRUE;
			}

			if (cs->ltp_mode == CONTROL)
			{
				/* ST14 */
				if (PLC.PulseCounter.ModuleOK != sts_present)
					ValidAlarm = TRUE;
			}

			if ((cfgEXT.drive_type == PCC_BR_AOAI) || (cfgEXT.drive_type == PCC_BR_AID0))
			{
				/* ST15 */
				if (PLC.AnaOut.ModuleOK != sts_present)
					ValidAlarm = TRUE;
			}
		}

		if (cs->ltp_mode == MONITOR)
		{
			/* ST14 */
			if (PLC.PulseCounter.ModuleOK != sts_present)
				ValidAlarm = TRUE;
		}

	
	if (GravitrolHopperAlone)
	{
		ValidAlarm = FALSE;
	}
	
	/* ST1 */
	if (PLC.BusRecvr.ModuleOK == sts_present)
	{  /* over current or i/o power fail */
		if ((PLC.BusRecvr.Status1 > 0) || (PLC.BusRecvr.Status2 > 0))
			ValidAlarm = TRUE;
	}
	else
		ValidAlarm = TRUE;
	
	if (PLC.StrainGauge[EXT_ADNum].ModuleOK != sts_present)
	{
		EXT_Analog_Failure_Logger++;
		shrEXT.adscan_reinitialize = TRUE;
		if (EXT_Analog_Failure_Counter++ > 30)
		{
			ValidAlarm = TRUE;
			EXT_Analog_Failure_Counter = 0;
		}
	}
	else 
		EXT_Analog_Failure_Counter = 0;
	
	//if (GravitrolHopperAlone)
	//{   
	//	if (PLC.DigOut[4].ModuleOK != sts_present)
	//    ValidAlarm = TRUE;
    //}
		
   //if (PLC.TouchPanel.BatteryStatus == 0)
	//	SET_ALARM(TYPE_SYSTEM,BATTERY_LOW_ALARM);
   //else
	//	CLEAR_ALARM(TYPE_SYSTEM,BATTERY_LOW_ALARM);
	
   //if (ValidAlarm)
   //   SET_ALARM(TYPE_SYSTEM,HARDWARE_FAILURE_ALARM);
   //else
   //   CLEAR_ALARM(TYPE_SYSTEM,HARDWARE_FAILURE_ALARM);
}

/************************************************************************/
/*
   Function:  SET_ALARM()

   Description:  This function will set the appropriate alarm bits in the
      correct device alarm word based on parameters sent in.
                                                                        */
/************************************************************************/
void SET_ALARM(int Device, unsigned int Alarm)
{
   switch (Device)
   {
      case TYPE_SYSTEM:
         shr_global.alarm_bits |= ((Alarm)|shr_global.alarm_force_bits);
         shr_global.alarm_latch_bits |= ((Alarm)|shr_global.alarm_force_bits);
         break;
   }
}

/************************************************************************/
/*
   Function:  CLEAR_ALARM()

   Description:  This function will clear the alarm defined by the device
         and alarm bit passed in.
                                                                        */
/************************************************************************/
void CLEAR_ALARM(int Device, unsigned int Alarm)
{
   switch (Device)
   {
      case TYPE_SYSTEM:
         shr_global.alarm_bits&=((~(Alarm))|shr_global.alarm_force_bits);
         break;
      case TYPE_BWH:
         shrBWH.alarm_bits&=((~(Alarm))|shrBWH.alarm_force_bits);
         break;
   }
}

/*****************************************************************************/
/*
   Function:  link_to_data()

   Description:  setup pointers to config and shared structures
                                                                             */
/*****************************************************************************/
void link_to_data()
{
   int i;

   for (i=0; i<MAX_GATES; i++)
   {
      ((config_loop_ptr *)(&cnf_LOOP[i]))->GATE = &cfgGATE[i];
   }
   ((config_loop_ptr *)(&cnf_LOOP[WEIGH_HOP_LOOP]))->BWH = &cfgBWH;
   ((config_loop_ptr *)(&cnf_LOOP[MIXER_LOOP]))->MIX = &cfgMIXER;
   ((config_loop_ptr *)(&cnf_LOOP[EXT_LOOP]))->LIW = &cfgEXT;
   ((config_loop_ptr *)(&cnf_LOOP[WTH_LOOP]))->WTH = &cfgWTH;
   ((config_loop_ptr *)(&cnf_LOOP[GRAVIFLUFF_FDR_LOOP]))->LIW = &cfgGFEEDER;
   ((config_loop_ptr *)(&cnf_LOOP[GRAVIFLUFF_LOOP]))->LIW = &cfgGFLUFF;
   ((config_loop_ptr *)(&cnf_LOOP[HO_LOOP]))->HO = &cfgHO;
   ((config_loop_ptr *)(&cnf_LOOP[SHO_LOOP]))->HO = &cfgSHO;

   for (i=0; i<MAX_GATES; i++)
   {
      ((shared_loop_ptr *)(&shr_LOOP[i]))->GATE = &shrGATE[i];
   }
   ((shared_loop_ptr *)(&shr_LOOP[WEIGH_HOP_LOOP]))->BWH = &shrBWH;
   ((shared_loop_ptr *)(&shr_LOOP[MIXER_LOOP]))->MIX = &shrMIXER;
   ((shared_loop_ptr *)(&shr_LOOP[EXT_LOOP]))->LIW = &shrEXT;
   ((shared_loop_ptr *)(&shr_LOOP[WTH_LOOP]))->WTH = &shrWTH;
   ((shared_loop_ptr *)(&shr_LOOP[GRAVIFLUFF_FDR_LOOP]))->LIW = &shrGFEEDER;
   ((shared_loop_ptr *)(&shr_LOOP[GRAVIFLUFF_LOOP]))->LIW = &shrGFLUFF;
   ((shared_loop_ptr *)(&shr_LOOP[HO_LOOP]))->HO = &shrHO;
   ((shared_loop_ptr *)(&shr_LOOP[SHO_LOOP]))->HO = &shrSHO;

   RECIPES = &recipes;
   BATCH_RECIPE_LAST    = &(RECIPES->entry[MAXRECIPE_S - 6]);
   BATCH_RECIPE_NOW     = &(RECIPES->entry[MAXRECIPE_S - 5]);
   ACTUAL_RECIPE        = &(RECIPES->entry[MAXRECIPE_S - 4]);
   BATCH_RECIPE_NEW     = &(RECIPES->entry[MAXRECIPE_S - 3]);
   LAST_ACTUAL_RECIPE   = &(RECIPES->entry[MAXRECIPE_S - 2]);
   EMPTY_RECIPE         = &(RECIPES->entry[MAXRECIPE_S - 1]); /* last entry in recvipe file GTF */   
}

/************************************************************************/
/*
   Function:  CalibrateTouch()

   Description: This function is run cyclically to calibrate the touch
        screen. A handle to the visual componenet of the system is obtained
        and then the process to calibrate the touch system is started.
                                                                        */
/************************************************************************/
void CalibrateTouch(void)
{
   UINT Result;     /* Return value of function call */

   if (!gTouchCalib) /* if permanent variable is 0/false then calibrate */
   {
      if (!gVCHandle) /* need to get a handle to the visualization */
      {
#ifdef PP65
         gVCHandle = VA_Setup(1, "visual");
#endif
#ifdef C70
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
#ifdef C70_003
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
         if (gVCHandle)             /* we have a handle move to next state */
            gState_TouchCalib = 1;  /* to start the calibration process */
      }
      else
      {
         switch (gState_TouchCalib)
         {
            case 0: /* Touch is waiting to calibrate */
               break;
            case 1: /* start the calibration process */
               if (!VA_Saccess(1,gVCHandle))
               {
                  Result=VA_StartTouchCal(1, gVCHandle);
                  if (Result == 0)       /* any other value is error */
                  {
                      VA_Srelease(1,gVCHandle);
                      gState_TouchCalib = 2;
                  }
               }
               break;
            case 2: /* If calibrated then exit state machine */
               if(!VA_Saccess(1,gVCHandle))
               {
                  Result = VA_GetCalStatus(1, gVCHandle);
                  VA_Srelease(1,gVCHandle);
                  if (Result == 0)    /* if 0xFFFF then error else still active */
                  {
                     gState_TouchCalib = 0;       /* Touch Calib is done, wait */
                     gTouchCalib = TRUE;          /* permanent variable is true */
                     gVCHandle = 0;               /* reset the gVCHandle variable */
                  }
               }
               break;
         }
      }
   }
}

/************************************************************************/
/*
   Function:  Security_Reboot()

   Description:  watches the HMI for the security page and appropriately calls
                 the correct page setup routines.

   Action_Input0 is for submitting a new passcode
   Action_Input1 is for resetting to minimum security level
   Action_Input2 is for manually clearing the change password status visibility

   Popup_Runtime1 is to show the password was accepted
   Popup_Runtime2 is to show the password was denied

************************************************************************/
void Security_Reboot()
{
   int i;
   config_super *cs = &cfg_super;

   for (i=0; i<gPasswordLength; i++)
      HMI.input_password[i] = '\0';
   cs->current_security_level = cs->minimum_security_level;
   reboot_SetSecurity = TRUE;  /* Visibility might change as a result of security change */
}

/************************************************************************/
/*  
   Function:  DelayXMSecs(unsigned int DelayMSecs)                                 
}      
                                                                      
   Description:  Generic millisecond delay routine.
                            
                                                                        */
/************************************************************************/
unsigned int Delay_X_MSecs(unsigned int DelayMSecs)                                       
{

	/* setup timer for 1 second */
	TON_01.IN = 1; /* enable timer */
	TON_01.PT = DelayMSecs;
	/* start the timer */
	TON(&TON_01);
	
	ElapsedTime = TON_01.ET;
	if (ElapsedTime >= TON_01.PT)
	{
		/* reset the timer */
		TON_01.IN = 0;        
		TON(&TON_01);
		return TRUE;
	}
	else return FALSE;
}
/************************************************************************/
/*
   Function:  init_dev()

   Description: This function initialize all shared and local variables for the
                 gate device.

        Returns:  VOID
                                                                        */
/************************************************************************/
void init_dev(void)
{
   config_super *cs = &cfg_super;

   link_to_data();

   //default_system();

   cs->current_security_level  = SERVICE_LOCK; /* TEMPORARILY HERE FOR DEVELOPMENT */
   shr_global.bln_oper_mode    = PAUSE_MODE;
   shr_global.ext_ho_oper_mode = PAUSE_MODE;

   Signal.DefaultSystem = FALSE;
   Signal.SystemLoaded  = FALSE;
}
