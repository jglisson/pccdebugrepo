/************************************************************************/
/*
   File:  hmi.h

   Description:  This file contains definitions and structures used to
      manipulate the visualization.


      $Log:   F:\Software\BR_Guardian\hmi.h_v  $
 *
 *    Rev 1.26   Sep 15 2008 13:15:52   gtf
 * Fixed debud layer visibility
 *
 *    Rev 1.25   Sep 03 2008 10:40:38   gtf
 * Finalized bug fix to fire gate secondary (if enabled) in manual backup screen
 *
 *    Rev 1.24   Aug 25 2008 08:24:10   gtf
 * Changed to accomodate bug fixes
 *
 *    Rev 1.23   Aug 07 2008 17:18:34   gtf
 * added various variable to allow display to work better
 *
 *    Rev 1.22   Jul 25 2008 15:38:14   gtf
 * Display changes to show coast time in Sec instead of mSec on pg044
 *
 *    Rev 1.21   May 20 2008 08:42:12   vidya
 * Added DeleteRecipeRuntime.
 *
 *    Rev 1.20   May 19 2008 11:36:46   vidya
 * Added pg454_SetRecipeEntry.
 *
 *    Rev 1.19   May 13 2008 13:23:50   vidya
 * Added HMI.pg450_RecipeRuntime.
 *
 *    Rev 1.18   May 09 2008 15:46:22   FMK
 * Added/changed page references.
 *
 *    Rev 1.17   May 06 2008 15:07:30   FMK
 * Setup remote port settings in visualization as
 * well as config_super.
 *
 *    Rev 1.16   Apr 30 2008 15:17:12   vidya
 * Added RecipeName variable to HMI structure.
 *
 *    Rev 1.15   Apr 29 2008 15:46:32   FMK
 * Added variable for totals of recipe and resins
 *
 *    Rev 1.14   Apr 29 2008 14:46:52   FMK
 * I believe the feed by weight is now ready for
 * testing.
 *
 *    Rev 1.13   Apr 16 2008 11:52:58   FMK
 * Added diagnostics pages for extruder and
 * haul off devices.
 *
 *    Rev 1.12   Apr 07 2008 14:32:12   FMK
 * Now showing device modes correctly in the
 * diagnostics modes.
 *
 *    Rev 1.11   Apr 03 2008 08:06:10   vidya
 * Added a variable for hoppertype visibility.
 *
 *    Rev 1.10   Apr 02 2008 08:35:22   vidya
 * Added Resin page variables.
 *
 *
 *    Rev 1.9   Mar 28 2008 10:00:18   FMK
 * Got the simulation screens working on the
 * visualization.
 *
 *    Rev 1.8   Mar 27 2008 08:12:08   vidya
 * Changed HMI variable SetParts to SetPartsTotal
 *
 *    Rev 1.7   Mar 24 2008 13:02:28   FMK
 * Added runtime variables for popup used when pressing
 * auto with extrusion/haul off control.
 *
 *    Rev 1.6   Mar 19 2008 14:54:54   FMK
 * Made changes to the extrusion control mode of
 * manual and manual entry.
 *
 *    Rev 1.5   Mar 19 2008 07:47:26   vidya
 * Added "GoIntoAuto" variable.
 *
 *
 *    Rev 1.4   Mar 13 2008 13:11:46   vidya
 * Added a bunch of variables to control the
 * visibility of recipe panels and to execute recipe
 * related functions.
 *
 *    Rev 1.3   Mar 07 2008 10:30:32   FMK
 * Changed reference of wtp_mode and ltp_mode.
 * Made changes to the alarm button on every
 * page. Now shows when active alarm and with
 * color/level of active alarm.
 * Modified the monitor system page to add graphic.
 *
 *    Rev 1.2   Jan 23 2008 14:16:32   FMK
 * Added 2 page definitions for storing and retrieving
 * configuration files from system setup.
 *
 *    Rev 1.1   Jan 14 2008 15:25:58   FMK
 * Added variable for page 430 dispense testing.
 * Number of times to repeat test.
 *
 *    Rev 1.0   Jan 11 2008 10:33:36   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include "file_rw.h"


/* page defintions */
#define pg000_MainPage               0

#define pg001_ChangeLanguage         1

#define pg002_Setup_Main             2
#define pg004_InitPage               4
#define pg005_BlendManualBackup1     5
#define pg006_Setup_Gate_CLoop4      6
#define pg007_LicenseKey             7
#define pg008_PanelGotOld			    8
#define pg009_ExtManual              9

#define pg010_Setup_GateSel          10
#define pg012_Setup_Gates            12
#define pg013_Setup_Gate_Net         13
#define pg014_Setup_Gate_Alarm       14
#define pg015_Setup_Gate_Load        15
#define pg016_Setup_Gate_Load1       16
#define pg017_Setup_Gate_CLoop1      17
#define pg018_Setup_Gate_CLoop2      18
#define pg019_Setup_Gate_CLoop3      19

#define pg020_Setup_BWH              20
#define pg021_Setup_BWH_CLoop1       21
#define pg022_Setup_BWH_CLoop2       22
#define pg023_Setup_Gate_AlarmB      23
#define pg025_histogram              25 //G2-561
#define pg024_Loading                24 //G2-401

#define pg030_Setup_Mixer            30
#define pg031_Setup_Mixer_Alarm      31
#define pg032_Setup_Mixer_CLoop      32
#define pg033_Setup_Mixer_CLoop1     33
#define pg034_Setup_Mixer_CLoop2     34
#define pg040_Setup_Ext              40
#define pg041_Setup_Ext_Net1         41
#define pg042_Setup_Ext_Net2         42
#define pg043_Setup_Ext_Net3         43
#define pg044_Setup_Ext_Alarm1       44
#define pg045_Setup_Ext_Alarm2       45
#define pg046_Setup_Ext_Alarm3       46
#define pg047_Setup_Ext_Load         47
#define pg048_Setup_Ext_CLoop1       48
#define pg049_Setup_Ext_CLoop2       49
#define pg050_Setup_Ext_CLoop3       50
#define pg051_Setup_Ext_Drive        51
#define pg052_Setup_Ext_Drive1       52
#define pg075_select_config_set      75
#define pg080_Setup_HO               80
#define pg081_Setup_HO_Net           81
#define pg082_Setup_HO_Alarm1        82
#define pg083_Setup_HO_Alarm2        83
#define pg084_Setup_HO_CLoop1        84
#define pg085_Setup_HO_CLoop2        85
#define pg086_Setup_HO_Drive1        86
#define pg087_Setup_HO_Drive2        87

#define pg99_touch_calib             99

#define pg100_Setup_Pump             100
#define pg101_Setup_Pump_Net         101
#define pg102_Setup_Pump_CLoop       102
#define pg103_Setup_Pump_CLoop1      103

#define pg110_Setup_GLoad            110
#define pg111_Setup_GLoad_Net        111
#define pg112_Setup_Gload_Net        112
#define pg113_Setup_GLoad_Alarm      113
#define pg114_Setup_GLoad_Load       114
#define pg115_Setup_GLoad_CLoop      115

#define pg130_Setup_GFluff           130
#define pg131_Setup_GFluff_Net1      131
#define pg132_Setup_GFluff_Net2      132
#define pg133_Setup_GFluff_Alarm1    133
#define pg134_Setup_GFluff_Alarm2    134
#define pg135_Setup_GFluff_Alarm3    135
#define pg136_Setup_GFluff_Load      136
#define pg137_Setup_GFluff_CLoop1    137
#define pg138_Setup_GFluff_CLoop2    138
#define pg139_Setup_GFluff_CLoop2    139
#define pg140_Setup_GFluff_Drive     140

#define pg150_Setup_Width            150
#define pg151_Setup_Width_Net        151
#define pg152_Setup_Wdith_Alarm      152
#define pg153_Setup_Width_CLoop      153

#define pg160_Setup_Winder           160
#define pg161_Setup_Winder_Net       161
#define pg163_Setup_Winder_Alarm1    163
#define pg164_Setup_Winder_Alarm2    164
#define pg165_Setup_Winder_CLoop1    165
#define pg166_Setup_Winder_CLoop2    166
#define pg167_Setup_Winder_CLoop3    167
#define pg168_Setup_Winder_Drive1    168
#define pg169_Setup_Winder_Drive2    169

#define pg170_Setup_System           170
#define pg175_Setup_System_Config    175
#define pg176_Setup_System_Config1   176
#define pg179_Setup_System_Misc      179
#define pg180_Setup_System_Alarms    180
#define pg181_Setup_System_Alarms1   181
#define pg182_Setup_System_Accel     182
#define pg183_Setup_System_UI        183
#define pg184_Setup_System_UI1       184
#define pg185_Setup_System_Sec       185
#define pg186_Setup_System_Sec1      186
#define pg187_Setup_System_Sec2      187
#define pg188_Setup_System_Sec3      188
#define pg189_Setup_System_Sec4      189
#define pg190_Setup_System_Sec5      190
#define pg191_Setup_System_Sec6      191
#define pg195_Setup_System_Units     195
#define pg196_Setup_System_Units1    196
#define pg200_Setup_System_Time      200
#define pg201_Setup_System_Relays    201
#define pg205_Setup_System_Recipe1   205
#define pg206_Setup_System_Recipe2   206
#define pg207_Setup_System_Recipe3   207
#define pg210_Setup_System_Ethernet  210
#define pg211_Setup_System_Serial    211
#define pg212_Setup_System_Comm      212
#define pg213_Setup_System_Smtp      213
#define pg214_Setup_System_UCBs      214
#define pg217_Setup_System_OPC       217

#define pg225_Setup_System_Backup    225
#define pg226_Setup_System_Restore   226
#define pg227_System_Load_Status     227

#define pg230_Setup_System_Sim1      230
#define pg231_Setup_System_Sim2      231
#define pg232_Setup_System_Sim3      232
#define pg233_Setup_System_Sim4      233

#define pg300_Print                  300

#define pg305_AlarmSelect            305
#define pg306_ActiveAlarms           306
#define pg307_AlarmHistory           307

#define pg310_Security               310

#define pg320_Totals_System          320
#define pg321_Totals_AtoF            321 
#define pg322_Totals_System64        322

#define pg324_Totals_ExtNip          324
#define pg325_Totals_Resin           325
#define pg326_Totals_AtoF64          326 
#define pg328_Totals_Resin64         328
#define pg329_Totals_ExtNip64        329

#define pg330_Monitor_Sys            330
#define pg331_Monitor_Sys1           331
#define pg332_Monitor_Sys2           332
#define pg334_Monitor_Hop_AtoH       334
#define pg335_Monitor_Hop_AtoH2      335
#define pg338_Monitor_Ext            338
#define pg339_Monitor_Line           339

#define pg350_Calibrate              350
#define pg351_Calibrate_Edit         351
#define pg352_Calibrate_Weigh        352

#define pg399_Diag                   399
#define pg400_Diag1                  400
#define pg401_Diag_SelectDevice      401
#define pg402_Diag_BWH               402
#define pg403_Diag_Mixer             403
#define pg404_Diag_System            404
#define pg405_Diag_Gate              405
#define pg406_Diag_Gate              406
#define pg407_Diag_SWD               407
#define pg408_Diag_Regrind           408
#define pg411_Diag_TimeWt            411

#define pg415_Diag_Ext               415
#define pg416_Diag_HO                416
#define pg417_Diag_Comm              417
#define pg418_Diag_SHO               418

#define pg425_Diag_Output_Debug1     425
#define pg426_Diag_Output_Debug2     426
#define pg427_Diag_Output_Debug3     427
#define pg428_Diag_Output_Debug4     428
#define pg429_Diag_Output_Debug5     429
#define pg430_Diag_DispenseTest      430
#define pg435_GateDelayTest			 435
#define pg436_GateTestView		       436
#define pg445_Diag_IORack            445
#define pg446_Env_Temp               446

#define pg451_Recipe_Check           451
#define pg453_Recipe_Input           453
#define pg454_Recipe_Entry           454
#define pg455_Change_Recipe          455
#define pg456_Recipe_Resin           456
#define pg457_Resin_Editor           457

#define pg990_SetupWizzard			    990
#define pg999_In_House_Testing		 999
/* runtime status bit definitions */
#define gcVisibleBit 1
#define gcActive     1   /* bit to indicate active button press/set datapoint */
#define gcInActive   0   /* bit to indicate inactive button press/set datapoint */

#define gNeutralDevice  65532
#define gNextDevice     0
#define gPrevDevice     1

#define gPasswordLength  10

#define gChangeContrast    1
#define gChangeBrightness  2

#define gYellow     34
#define gDarkYellow 40
#define gRed        45
#define gRedOrange  43
#define gLtRed      37
#define gGreen       2
#define gLtGreen   221
#define gBlue      212
#define gWhite      16
#define gOrange     48
#define gBlack       0

struct StrMode_Struct
{
   unsigned char OperMode[12];
   unsigned char RunMode[12];
   unsigned char FeedMode[12];
};

struct hmi_struct
{
	unsigned char UnitIDChange;
	unsigned char UnitLengthChange;
	unsigned char UnitLengthRateChange;
	unsigned char UnitMassChange;
	unsigned char UnitMassAreaChange;
	unsigned char UnitMassLengthChange;
	unsigned char UnitMassRateChange;
	unsigned char UnitODChange;
	unsigned char UnitTempChange;
	unsigned char UnitThicknessChange;
	unsigned char UnitWidthChange;
	unsigned char UnitPercentChange;

	unsigned char UnitIDCurrent;
	unsigned char UnitLengthCurrent;
	unsigned char UnitLengthRateCurrent;
	unsigned char UnitMassCurrent;
	unsigned char UnitMassAreaCurrent;
	unsigned char UnitMassLengthCurrent;
	unsigned char UnitMassRateCurrent;
	unsigned char UnitODCurrent;
	unsigned char UnitTempCurrent;
	unsigned char UnitThicknessCurrent;
	unsigned char UnitWidthCurrent;
	unsigned char UnitPercentCurrent;

	unsigned short int CurrentPage;     /* current page displayed */
	unsigned short int PrevPage;     /* previous page # GTF */
	unsigned short int ChangePage;      /* used to change displayed pages. assign new page # */

	int CurrentDevice;   /* defines which gate/hopper,ho device to set up */
	int ChangeDevice;    /* Normally 65532, if change then switch to next device */

	unsigned char SetupLock;       /* controls editability of visual objects during set up */
	unsigned char CalibrateLock;
	unsigned char ChangeRecipeLock;
	unsigned char StoreRecipeLock;
   unsigned char SelectRecipeLock; //G2-662

	unsigned char BlendStopRuntime;     /* blender stop button runtime control */
	unsigned char BlendRunRuntime;      /* blender auto button runtime control */
	unsigned char BlendManualRuntime;   /* blender stop button runtime control */
	unsigned char ExtStopRuntime;       /* extruder stop button runtime control */

	unsigned char BlendManualLock;
	unsigned char ServiceLock;
	unsigned char SupervisorLock;
	unsigned char MaintenanceLock;

	unsigned char OperatorLock;
	unsigned char ClearShiftLock;
	unsigned char ClearInvenLock;
	unsigned char ClearResinLock;

	unsigned char BlendPauseLock;       /* used for locking controls that require blender be paused */
	
	unsigned char BlendMainRestartRuntime;   /* blender stop button runtime control */
	
	unsigned char SimulationPopupRuntime;        /* simulation popup on the main page */

	unsigned char ClearRecipeLock;
	unsigned char ExtRunRuntime;        /* extruder auto button runtime control */
	unsigned char ExtManualRuntime;     /* extruder stop button runtime control */
   unsigned char ExtManualRuntimelck;  /* lock button when in Monitor Mode */ //G2-641
	unsigned char ExtControlRuntime1;   /* extrusion or ltp control is enabled  */
	unsigned char AlarmRuntime;         /* runtime/visibility value for alarm button */
	unsigned char MainRuntime;          /* runtime/visibility value for Main button */
	
	unsigned char SelfCleanLockRuntime; /* runtime/visibility value for Main button      */
  	unsigned char SelfCleanPopup;       /* runtime/visibility value for self clean popup */

	unsigned short int BlendMainBtnColor;    /* blender Main btn color control */
	unsigned short int BlendStopLedColor;    /* blender stop led runtime control */
	unsigned short int BlendRunLedColor;     /* blender auto led runtime control */
	unsigned short int BlendManualLedColor;  /* blender manual led runtime control */
	unsigned short int ExtStopLedColor;      /* extruder stop led runtime control */
	unsigned short int ExtRunLedColor;       /* extruder auto led runtime control */
	unsigned short int ExtManualLedColor;    /* extruder manual led runtime control */
	unsigned short int AlarmBtnColor;        /* color of alarm button */
	unsigned short int SecLevelColor;        /* color displayed for the current security level */
	unsigned short int InvalidTouchColor;
	unsigned short int BlendModeText;
	unsigned short int BlendModeColor;

	char input_password[gPasswordLength]; /* page 310 passcode input string */

	unsigned char Action_BlendMode;
	unsigned char ValidateKey;
	unsigned char SetupWizzardProceed;
	unsigned char SetupWizzardBack;
	unsigned char Action_ExtruderMode;

	unsigned char Action_Input0;
	unsigned char Action_Input1;
	unsigned char Action_Input2;
	unsigned char Action_Input3;
	unsigned char Action_Input4;
	unsigned char Action_Input5;
	unsigned char Action_Input6;
	unsigned char Action_Input7;
	
	unsigned char RestartRequired; /* setup change indicates the system must restart */
	unsigned char Request_SYSReset;
	unsigned char Completion_Input;
	unsigned char NumDecimalPoints; /* The number of decimal points to show for float values */
	unsigned char Touch_Calibrated;

    /* some values in setup trigger actions in the c code if changed.
       the following variables will indicate that a field was edited.
       this is similar to num_change call we had in the old code. */
	unsigned char SetupNumChanged1;
	unsigned char SetupNumChanged2;
	unsigned char SetupNumChanged3;
	unsigned char SetupNumChanged4;
	unsigned char SetupNumChanged5;

	/* The OptionPoint variables are used to control the visiblity of items in a multi-text */
	/* menu/list box */
	unsigned char SetupOptionPoint1[14];
	unsigned char SetupOptionPoint2[14];
	unsigned char SetupOptionPoint3[14];
	unsigned char SetupOptionPoint4[14];
	unsigned char SetupOptionPoint5[20];

	unsigned char SetupOptionfirstrec[10];
	unsigned char SetupOptionsecondrec[10];
	unsigned char SetupOptionthirdrec[10];

	unsigned char SetupOptionDevice[18];
	unsigned char SetupOptionAlarm[100];

    /* The runtime status variable can be used to control the
       behavior of objects during runtime. For example bit 0
       will control the visiblilty of the object.

       Refer to the B&R manual for all the bit defintions */
	unsigned char HopARuntime;
	unsigned char HopBRuntime;
	unsigned char HopCRuntime;
	unsigned char HopDRuntime;
	unsigned char HopERuntime;
	unsigned char HopFRuntime;
	unsigned char HopGRuntime;
	unsigned char HopHRuntime;
	unsigned char HopIRuntime;
	unsigned char HopJRuntime;
	unsigned char HopKRuntime;
	unsigned char HopLRuntime;

	unsigned char SetupRuntime1;    
	unsigned char SetupRuntime2;    
	unsigned char SetupRuntime3;    
	unsigned char SetupRuntime4; 
	unsigned char SetupRuntime5;   
	unsigned char SetupRuntime6; 
	unsigned char SetupRuntime7; 
	unsigned char SetupRuntime8;    
	unsigned char SetupRuntime9;
	unsigned char SetupRuntime10;

	unsigned char PopupRuntime1;   
	unsigned char PopupRuntime2;
	unsigned char PopupRuntime3;   
	unsigned char PopupRuntime4;   
	unsigned char PopupRuntime5; 
	unsigned char PopupElapTime; 

   //G2-651
   unsigned char BatchStopFlg;
	
	unsigned char pg000_LayerRuntime1;
	unsigned char pg320_LayerRuntime1;
	unsigned char pg321_LayerRuntime1; 
	unsigned char pg227_LayerRuntime1; 
	unsigned char pg334_LayerRuntime1; 
	unsigned char pg335_LayerRuntime1; 

	int pg345change_layer;
	unsigned char pg345_LayerRuntime1; 	
	unsigned char pg345_LayerRuntime2; 

	int pg405change_layer;
	unsigned char pg405_LayerRuntime1; 
	unsigned char pg405_LayerRuntime2; 

	int pg406change_layer;
	unsigned char pg406_LayerRuntime0;
	unsigned char pg406_LayerRuntime1; 
	unsigned char pg406_LayerRuntime2;
	unsigned char pg406_ClearButtonA_Visibilty;
	unsigned char pg406_ClearButtonB_Visibilty;
	unsigned char pg406_ClearButtonC_Visibilty;
	unsigned char pg406_ClearButtonD_Visibilty;

	int pg453change_layer;
	unsigned char pg453_LayerRuntime1; 
	unsigned char pg453_LayerRuntime2; 
	unsigned char pg453_LayerRuntime3;
	unsigned char pg453_LayerRuntime4;
	unsigned char pg453_LayerRuntime5;   
    
    /* these variables are used to control visibility of objects 
       who are based on the configuration of the blender */
	unsigned char PrintRuntime;
	unsigned char WTHRuntime;
	unsigned char WTPRuntime;
	unsigned char LTPRuntime;
	unsigned char BLNRuntime;

	unsigned char PUMPRuntime;
	unsigned char SLTPRuntime;
	unsigned char GFLFRuntime;
	
	unsigned char ExtInControlRuntime;
	unsigned char ExtInMonitorRuntime;

	config_GATE_device HMI_cfgGate;
	shared_GATE_device HMI_shrGate;

	int Load_status[NUM_STRUCTURES];
	int Load_status_color[NUM_STRUCTURES];
    
	unsigned char pg000_Debug_Layer_On; /*Is the page button visible on title bar GTF*/
	unsigned char pg000_Lang_Layer_On; /*Is the debug language layer displayed in Visualization GTF*/
	unsigned char pg000_DisplayLanguage; /*what language is currently displayed */
	unsigned char pg000_RestartRequiredRuntime;
	unsigned char pg000_PopupFinishBatchRuntime;
	unsigned char pg000_PopupInValidRecipeRuntime;
	unsigned char pg000_InValidJobWeightRuntine;    
	unsigned char pg000_InValidMixTimeRuntine;    
	unsigned char pg000_LanguageRuntime;
	unsigned char pg000_PopupClearAlarms;
	unsigned char pg000_PopupCannotAutoPause;
	unsigned char pg000_PopupNoRateSpd;
	unsigned char pg000_PopupNoCalEXT;
	unsigned char pg000_CannotRunRecipe_Big;
	unsigned char pg000_CannotRunRecipe_Small;
	unsigned char pg000_PopupRecipeUseRuntime;
	unsigned char pg000_PopupRecipeUsed_ACK;
	unsigned char pg000_PopupGateSwitchRuntime;
	unsigned char pg000_PopupGateSwitch_ACK;
	unsigned char pg000_CalNeedDoneRuntime;
	unsigned char pg000_PopupBWHCalibration_ACK;
		
	unsigned char pg004_InitTextIndex;
	unsigned char pg005_PopupGateSwitchRuntime[12];
	unsigned char pg005_PopupMixerGateRuntime;
	
	unsigned char pg005_SelfCleaningBtn;

   
	float pg004_MaxRatio;
	float pg004_MaxExt;
	float pg004_MaxHO;
	float pg004_MaxRefeed;
	float pg014_AlarmFactorCalc;
	int   pg005_manual_GateControl; /* Gate A-L manual Control GTF */
	float pg018_gate_target_min;    /* lower limit for gate target accuracy equals sys target accuracy GTF */
	float pg018_gate_target_max;    /* upper limit for gate target accuracy equals batch oversize 5-50% GTF */

   unsigned char pg012_loadingON;  //G2-590

	unsigned char pg032_MixerGateAutobatchRuntime;	/* AUTOBATCH only */

   //G2-561
   int   pg025_hist[10];
   int   pg025_chan;
   int   pg025_label[10];
   UDINT pg025_Count;
   UDINT pg025_Counts[10];
   UDINT pg025_Center;
   int   pg025_Range;
   unsigned char pg025_Clear;  
   
   unsigned char pg024_LoadAll;        //G2-401
   unsigned char pg024_loadreqvis[12]; //G2-401
   unsigned char pg024_load[12];       //G2-401
   unsigned char pg024_firstpass;      //G2-401
   unsigned char pg024_remoteSLvis;

   unsigned char pg034_FloodFeedSet;   //G2-596
   unsigned char pg034_FloodFeedModeSet; 
   float pg034_Flood_Feed_Timer;

	float pg044_Coast_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg044_Coast_changed; /*Added so we can display time is Sec not mS GTF*/
	float pg047_Load_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg047_load_changed; /*Added so we can display time is Sec not mS GTF*/
	float pg048_Updates_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg048_updates_changed; /*Added so we can display time is Sec not mS GTF*/
   int   pg_075_config_set;     /* blender size to config for*/
	float pg082_Coast_display;   /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg082_Coast_changed; /*Added so we can display time is Sec not mS GTF*/
	float pg084_Calc_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg084_Calc_changed; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg087_Pop_ack; /*Acknowledge popup message GTF*/
	float pg163_Coast_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg163_Coast_changed; /*Added so we can display time is Sec not mS GTF*/
	float pg165_Calc_display; /*Added so we can display time is Sec not mS GTF*/
	BOOL  pg165_Calc_changed; /*Added so we can display time is Sec not mS GTF*/
	
	char  pg017_Guardian_display;   /* Added so we can display pages associated with Guardian  */
	char  pg017_AutoBatch_display;   /* Added so we can display pages associated with AutoBatch  */
	
	int pg175_WTPMode;
	int pg176_LTPMode;
	int pg176_SLTPMode;
	int pg176_WTHMode;

	unsigned char pg181_AlarmDevicesSetupOptions[20];
	unsigned char pg181_AlarmsSetupOptions[100];
	unsigned char pg181_AlarmDevice;
	unsigned char pg181_AlarmType;
	unsigned char pg181_AlarmActionPaused;
	unsigned char pg181_AlarmActionManual;
	unsigned char pg181_AlarmActionAutomatic;
	unsigned int  pg181_Max_Index;

	int pg190_TimeOut;  /* The allowable time in ms before timing out current security level 0=never */

	unsigned char pg195_units_changed;

	//unsigned char pg201_SelectedRelay;
	unsigned char pg201_RelayActive;
	unsigned char pg201_RelayInvert;
	unsigned char pg201_RelayChannel;
   unsigned char pg201_SetRelay;
   unsigned char pg201_SetChannel;

	unsigned char pg206_RecipeEntryTextIndex;

	unsigned char pg207_SelectedGate;
	unsigned char pg207_GateRecipeUse;
	unsigned int  pg212_uiRegisterReturn;
	float         pg212_fRegisterReturn;
	unsigned char pg212_UcbNumber;
	unsigned char pg212_UcbRegister;
	unsigned int  pg212_UcbValue;
   unsigned int  pg212_OPCvis; 

	unsigned char pg213_AlarmAction;
    
	unsigned char pg214_LoadDefaultUCBs;
	unsigned char pg214_ClearDefaultUCBs;
	
   int pg225_FailCode;
	int pg225_err_state;
	int pg225_err_code;
	int pg225_save_what;
	int pg225_save_where;
	unsigned char pg225_sErrorText[100];

	unsigned char pg232_SimExtDrive;
	unsigned char pg232_SimExtWeigh;
	unsigned char pg232_SimHOPulses;
	unsigned char pg232_SimHODrive;
	unsigned char pg233_SimSHOPulses;
	unsigned char pg233_SimSHODrive;
	unsigned char pg300_PrinterInitialized;
	unsigned char pg300_PrintingRuntime;
	unsigned char pg310_SecurityLvlIndex;

	unsigned char pg320_PopupTextIndex;
	float pg320_RecipeInven;
	float pg320_ResinInven;
	char pg320_product_code[16];

	unsigned char pg330_GateFeedingRuntime[MAX_GATES];
	unsigned char pg330_IconsRuntime;
	unsigned char pg330_HopperAnimIndex;
	unsigned char pg330_HopperAnimTextIndex;
	unsigned char pg330_BWHAnimIndex;
	unsigned char pg330_MixerAnimIndex;
	unsigned char pg330_BoxAnimIndex;
    
	unsigned char pg330_JobNameRuntime;
	unsigned char pg330_JobCompleteRuntime;
	unsigned char pg330_JobPercentRuntime;
	unsigned char pg330_JobWeighInRecipe;
	unsigned char pg330_BlendModeIndex;
	unsigned char pg330_ExtModeIndex;
	unsigned char pg330_MaterialReqRuntime;

	unsigned short int pg330_GateFeedingColor[MAX_GATES];
   unsigned short int pg330_GateFeedingColor2[MAX_GATES];
	unsigned short int pg330_BatchStsColor;
	unsigned short int pg330_MixerStsColor;
	unsigned short int pg330_MixerProxStsColor;
	unsigned short int pg330_MixerGateStsColor;
	unsigned short int pg330_MaterialReqColor;

	float pg330_job_percent_done;

	unsigned short int pg330_BlendModeColor;
	unsigned short int pg330_ExtModeColor;
    
	char pg334_product_code[16];

   float pg338_wtp_set;
   float pg338_wtp_act;
	
   float pg339_ltp_set;
   float pg339_ltp_act;
   float pg339_sltp_act;
   float pg339_stretch_factor;
   float pg339_refeed_wtp_set;

	unsigned char pg339_thk_display;
	unsigned char pg339_wpl_display;
	unsigned char pg339_wpa_display;
	unsigned char pg339_wtp_display;
    
	int pg345_proxtimerCHANGED;
	
	unsigned char pg350_BWH_calibration_flag;

	int   pg351_ZeroFactor;
	int   pg351_TestFactor;
	float pg351_TestWeight;

	float pg351_NipCircum;
	float pg351_NipPulses;
	int   pg351_CalibDeviceTextIndex;

	unsigned char pg352_CalibErrorTextIndex;
   float pg352_dispwtbits;

	struct StrMode_Struct pg402_BWH;
	struct StrMode_Struct pg402_Gate[12];
	struct StrMode_Struct pg402_Mix;

	int pg406_clearA;
	int pg406_clearB;
	int pg406_clearC;
	int pg406_clearD;
	int pg406_clearE;
	int pg406_clearF;
	int pg406_clearG;
	int pg406_clearH;
	int pg406_clearI;
	int pg406_clearJ;
	int pg406_clearK;
	int pg406_clearL;

	unsigned char pg408_ucRegrindHighCovered;
	unsigned char pg408_ucRegrindMiddleCovered;
	unsigned char pg408_ucRegrindLowCovered;
	int pg408_GraphIndex;
	int pg408_Simulation;

	unsigned char pg411_time_index;
	unsigned char pg411_gate_index;
	unsigned char pg411_current_gate;
	unsigned char pg411_mgt_index;
        
	unsigned char pg411_TWTSubstitutionRuntime; 
	
	float pg415_PercentGravimetric;
	struct StrMode_Struct pg415_ExtMode;

	struct StrMode_Struct pg416_HOMode;
	unsigned char pg417_ToggleComm;
	UINT    TCPIP_State;  
   
   unsigned long pg417_ulIndex;
   unsigned long pg417_ulIndexMax;
   unsigned long pg417_ulSend;
   unsigned long pg417_ulSendError;
   unsigned long pg417_ulRecv;
   unsigned long pg417_ulRecvError;
   unsigned long pg417_ulServer;
   unsigned long pg417_ulServerError;
   unsigned long pg417_ulOpen;
   unsigned long pg417_ulOpenError;
  
	unsigned char pg425_DiagOutputWhat[32];
	unsigned char pg425_DiagOutputWho[32];
	BOOL  pg425_enable_all_debug; /*Added to enable all debug data GTF*/
	BOOL  pg425_disable_all_debug; /*Added to disable all debug data GTF*/
    
	int pg430_TargetGate;
	int pg430_NumberOfCycles;
	unsigned char pg430_HopperToTest;
	unsigned char pg430_ClearGateCalib;
	/*    unsigned char pg430_DispenseMethod; */
	int pg430_DispenseMethod;
	unsigned short int pg430_dispensetestColor;  /* Dispense Test Color runtime control GTF */
	unsigned char pg430_numtests;
	int   pg430_TargetTime;                /* time in mS GTF */
	float pg430_TargetWt;
	float pg430_TargetTol;
	int   pg430_max_gates;               /* max selctable gate number for pulldown GTF */
	
	BOOL  pg435_bGatesToTest[6], pg435_bGoPressed;
	UINT  pg435_u16FirstTime, pg435_u16LastTime, pg435_u16FirstDelay, pg435_u16LastDelay;
	USINT pg435_u8ButtonPressed, pg435_u8DelayStep, pg435_u8TimeStep, pg435_u8AveragePoints;

	unsigned char pg445_ModuleID[20];
	unsigned int pg445_HW_Vers;
	unsigned int pg445_FW_Vers;
	int pg445_SerialNum;
	int pg445_Module_Status;
	float pg445_SupplyCurrent; 
	float pg445_SupplyVoltage;
	int pg445_Status1;
	int pg445_Status2;
	int pg445_BatteryVoltage;
	int pg445_CpuTemperature;
	int pg445_EnvTemperature;

	unsigned char pg460_RecipeRuntime;
	/*************************************/
	/* B&R001-48 old recipe pages  BEB */

	int pg450_RecipeEntryColor[12+1];
	unsigned char pg450_AutoRuntime;
	unsigned char pg450_StopRuntime;

	
	unsigned char pg450_RecipeTotalRuntime;
	unsigned char pg450_StoredRecipeRuntime;
	unsigned char pg450_RecipeCurrentRuntime[12];
	unsigned char pg450_RecipeEditLock[12+1];
	unsigned char pg450_RecipeHopperReqRateRuntime[13];
	unsigned char pg450_JobDonePercent;
	unsigned char pg450_BeginNewJobRunTime;
	unsigned char pg450_RecipeNewMaterialRunTime;
	unsigned char pg450_RecipeRateSpeedRequest;
	unsigned char pg450_CalcRateSpeed;
	unsigned char pg450_JobWtYesRuntime;
	unsigned char pg450_JobWtNoRuntime;
	unsigned char pg450_CalNotDoneRuntime;
	unsigned char pg450_JobNameRuntime;
	unsigned char pg450_JobNameAssigned;
	unsigned char pg450_GoIntoAuto;
	unsigned char pg450_HopperTypeRuntime;
	unsigned char pg450_RecipeOptionalRuntime;
	unsigned char pg450_RecipeOptionalOrder[12+1];
	
   unsigned char pg450_Show_Next; //G2-691
	unsigned char pg450_RecipeCurrentRuntimeAll[12];

	unsigned char pg451_HopperRateSpeed;
 
	unsigned char pg453_Gate_Order_Index[12];
	unsigned char pg453_Recipe_Action;
	unsigned char pg453_LayControlsRunTime;
	unsigned char pg453_RecipeSaveRunTime;
	unsigned char pg453_RecipeUseRunTime;
	unsigned char pg453_DeleteRecipeRunTime;
	/*    unsigned char pg453_Layer1;  not used anymore? */
	unsigned char pg453_ChangeSaveRecipe;
	int pg453_RecipeNum;
	float pg453_SetPartsTotal;
	unsigned char pg453_ChangeRecipe;

	/* used on pg456 & pg457 */
	unsigned char pg453_RecipeNumChange;

	float pg450_RecipeEntry[3];
	float pg454_SetRecipeEntry[3];
	unsigned char pg450_txtFirstRecipeEntry[10];
	unsigned char pg450_txtSecRecipeEntry[10];
	unsigned char pg450_txtThirdRecipeEntry[10];
    
	resin_str UI_ResinTable;
	unsigned char pg457_ResinNum;
	unsigned char pg457_ResinSaveRuntime;
	STRING pg457_ResinDesc[100][11];
	unsigned char pg457_DefLayerRuntime;
	unsigned char pg457_LayerOneRuntime;
	
	unsigned char pg990_u8WizBlenderType;
	unsigned char pg990_u8WizBlenderSize_Guardian;
	unsigned char pg990_u8WizBlenderSize_Autobatch;
	unsigned char pg990_u8WizBlenderOperation;
	unsigned char pg990_u8WizBlndOpBtnPressed;
	unsigned char pg990_sWizOpMode[12];
	unsigned char pg990_au8WizBlndOpBtnBitmap[3];
	unsigned char pg990_u8WizSummary;
	unsigned char pg990_u8WizSummary1;
	unsigned char pg990_HopperSafetySwitch;
	unsigned char pg990_GravitrolHopperAlone;
	USINT pg990_au8BlenderSizeAvailability[20];	
	
	unsigned char pg990_HopperDoorSwitch;
	unsigned char pg990_HopperDoorSwitchSet; //G2-558


	unsigned short int pg991_TWTLearningRuntime;
	
	/* visibility of pg454 recipe entry */
	unsigned char pg454_recipe_wtp_visible_1;	
	unsigned char pg454_recipe_wpl_visible_1;
	unsigned char pg454_recipe_thickness_visible_1;
	unsigned char pg454_recipe_width_visible_1;
	unsigned char pg454_recipe_id_visible_1;
	unsigned char pg454_recipe_od_visible_1;   
	unsigned char pg454_recipe_ltp_visible_1; 
	unsigned char pg454_recipe_wpa_visible_1; 	
	unsigned char pg454_recipe_refeed_visible_1; 	
    
	unsigned char pg454_recipe_wtp_visible_2;	
	unsigned char pg454_recipe_wpl_visible_2;
	unsigned char pg454_recipe_thickness_visible_2;
	unsigned char pg454_recipe_width_visible_2;
	unsigned char pg454_recipe_id_visible_2;
	unsigned char pg454_recipe_od_visible_2;   
	unsigned char pg454_recipe_ltp_visible_2; 
	unsigned char pg454_recipe_wpa_visible_2; 
    
	unsigned char pg454_recipe_wtp_visible_3;	
	unsigned char pg454_recipe_wpl_visible_3;
	unsigned char pg454_recipe_thickness_visible_3;
	unsigned char pg454_recipe_width_visible_3;
	unsigned char pg454_recipe_id_visible_3;
	unsigned char pg454_recipe_od_visible_3;   
	unsigned char pg454_recipe_ltp_visible_3; 
	unsigned char pg454_recipe_wpa_visible_3; 

	unsigned char Flood_Feed_Timer_visible; //3/28/16 drt
	unsigned char Flood_Feed_Timer_Flag;    //3/28/16 drt

	//float         gate_delay; //NOT USED drt

   BOOL RestoreError;

	//3/29/16 drt control of OPC option
	unsigned char OPCenable;
	unsigned char OPCenableVis;
	unsigned char OPCenableRead;
	unsigned char OPCenableWrite;
         
	unsigned char pg990_SetUpSwitchSafetyRuntime;	/*	this will apply only for 1Kg Guardian 3	*/
	USINT         u8LockedIfBlndNotStopped;		   /* restart */
};

typedef struct  
{
   int   index;
   unsigned int numpts;    /* number of measurements for this feed time         */
   int   nominal_time;     /* nominal (center) feed time for this bin in mS GTF */
   int   avg_time;         /* avg feed time for this bin              in mS GTF */
   float avg_wt;           /* avg wt for this time                              */
   float expected_wt;      /* expected wt for this time                         */
   unsigned short int bg_color;
} hmi_time_wt_str;

_GLOBAL struct hmi_struct HMI;

_GLOBAL unsigned char gIPAddress[16];
_GLOBAL unsigned char gSubNetMask[16];
_GLOBAL unsigned char gGateway[16];
_GLOBAL unsigned char gHostname[16]; //G2-620

#if 0
_GLOBAL unsigned char gPLCDrive1Address[16];
_GLOBAL unsigned char gPLCDrive2Address[16];
#endif

_GLOBAL int COUNTtimewt;

_GLOBAL unsigned int remote_recipe_write_flag;
_GLOBAL unsigned int remote_auto_write_flag;
_GLOBAL unsigned int remote_manual_write_flag;
_GLOBAL unsigned int remote_pause_write_flag;
_GLOBAL unsigned int remote_pause_batch_write_flag;

_GLOBAL unsigned int remote_OneTimeAuto_write_flag;

_GLOBAL unsigned int remote_ext_recipe_write_flag;
_GLOBAL unsigned int remote_ext_auto_write_flag;
_GLOBAL unsigned int remote_ext_manual_write_flag;
_GLOBAL unsigned int remote_ext_pause_write_flag;
