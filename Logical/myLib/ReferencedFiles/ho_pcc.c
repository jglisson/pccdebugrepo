#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  ho_pcc.c

   Description:


      $Log:   F:\Software\BR_Guardian\ho_pcc.c_v  $
 *
 * 2008/12/01 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.hoPcc to toggle debug information in this file
 *
 *    Rev 1.4   Aug 25 2008 17:24:08   gtf
 * #if DEBUG_HO_PCC = 0
 *
 *    Rev 1.3   Jul 31 2008 10:57:12   jlr
 * fixed an oops where I'd put a DBGDBG line
 * above a declaration in fuction add_len
 *
 *    Rev 1.2   Jul 31 2008 10:36:56   jlr
 * added some debug information
 *
 *    Rev 1.1   Apr 02 2008 10:24:34   FMK
 * The HO loop now builds. The NetCommand function
 * and AB_PLC control is not yet finished though.
 *
 *    Rev 1.0   Apr 01 2008 09:58:12   FMK
 * Initial version

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <math.h>
#include <astime.h>
#include <string.h>
#include <timer.h>

#include "plc.h"
#include "relaydef.h"
#include "sys.h"
#include "exd_drive.h"
#include "mem.h"
#include "libbgdbg.h"
#include "signal_pcc.h"
#include "alarm.h"
#include "vars.h"
#include "net.h"
#include "demo.h"
#include "recipe.h"
#include "hmi.h"


#define PLC_5_READ   0x68
#define PLC_5_WRITE  0x67
#define TIME_BETWEEN_LOAD_OFF_ON  500
#define INT             0
#define FLOAT           1
#define LOAD_ON         258             /* turn loading relay on (wm)   */
#define LOAD_OFF        259             /* turn loading relay off (wm)  */
#define LOW_ON          260             /* turn low alarm on (wm)       */
#define LOW_OFF         261             /* turn low alarm off (wm)      */
#define STANDARD_TIMEBASE       1228800 /* standard wm,exd timebase     */
#define AB_PLC_TIMEBASE       100 /* standard wm,exd timebase     */
#define EXD_DEAD (s->alarm_bits & EXD_TIMEOUT)
#define Float_NEQ(A,B) (fabs((double)(A-B))>.0003)
#define SET_ALARM(A) {s->alarm_bits|=((A)|s->alarm_force_bits);s->alarm_latch_bits|=((A)|s->alarm_force_bits);}
#define CLEAR_ALARM(A) {s->alarm_bits&=((~(A))|s->alarm_force_bits);}
#define Float_EQ(A,B) (fabs((double)(A-B)) < 0.0003)
#define WM_DEAD (s->alarm_bits & WM_TIMEOUT)
#define Connection_timed_out 27160
#define POINTS_IN_RS_TABLE  7
#define EXT_SPD_DIFF  0.05

#define DEBUG_HO_PCC 0  /* define for debug data JLR */

extern void SendUnitDataCommand(int chan, int FNC, short FileNum, short RegNum, short ElementSize, int DataType);
extern void RecvSendUnitDataResponse(int chan, int FNC);
extern void OpenRemoteSocketPLC(int PortVal, unsigned long HostAddr, unsigned char unit_id, int chan);
extern void SendRegSessionCommand(int chan);
extern void RecvRegSessionResponse(int chan);
extern void SendRRData(int chan);
extern void RecvSendRRDataResponse(int chan);
extern void BuildPLCEthAddress();

//_LOCAL  unsigned int ho_ramptime;
_LOCAL  unsigned int ho_ramptime_plc;	   /* in seconds */
_LOCAL  unsigned int interval_timer;
_LOCAL   float expected;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

#ifdef ALLEN_BRADLEY_DRIVE
void init_ethernet(void);
#endif
void reset_drive(void);
void init_dev();
void HO_PCC();
void check_system();
void totals(void);
void auto_mode(void);
void manual_mode(void);
void pause_mode(void);
void add_len(double len);
void handle_drive_inhibit(void);
void coast_totals(void);
void check_coast(void);
void set_update_flags(void);
int ready_to_update(void);
float percent_error(float a,float b);
void update(void);
void calc_new_initial_rate_speed(void);
void net_command(int cmd);
void set_initial_speed(void);
void monitor_mode(void);
void calc_demo_values(void);

#ifdef ALLEN_BRADLEY_DRIVE
void send_plc_alive_flag(void);
_LOCAL int plc_alive_count;                /* send alive flag to plc every 30 times thru this loop */
#endif
void poke(void);
void Check_ManualBackup_HO();
void set_exd_status(void);
REAL fHONoise(void);

extern int abs(int);
extern int rand(void);

extern int SocketNew, ethnetpn;
extern char connectedPLC;

extern unsigned char inbfr[256], outbfr[256];
extern int counter;
extern unsigned char session_handle[4];
extern int PLC_WRITTEN_INT_DATA[10], PLC_READ_INT_DATA[10];
extern float PLC_WRITTEN_FLOAT_DATA[10], PLC_READ_FLOAT_DATA[10];
unsigned char Encap_Header[24];
int recv_length;
extern char ConnectedPLC_Flag;
//unsigned long HostAddr;
//unsigned char ip[4];

extern unsigned char gDev_Init;
extern UINT EthError;

extern void ClosePLCRemoteSocket(void);

float last_set_ltp = 0.0; 
float last_act_spd = 0.0;
float last_set_spd = 0.0;

unsigned int exd_timebase = 0;          /* timebase of exd              */
int local_ltp_mode;
int numbad;                             /* number of bad updates        */
unsigned int ramptime;                  /* ramptime in TICKS            */
unsigned int out_of_spec_count;        /* update # for when to check   */
unsigned int no_update_timer;           /* timer for no updates         */
_LOCAL char reset_rate_speed;          /* flag to restart rate speed   */
char no_update_timer_on;                /* flag for update timer active */
char process_command = FALSE;
TIME rampstart;
float min_per_tbit = 0.0;               /* mins per time bit factor     */
unsigned int coast_timer;               /* timer for coast alarms       */
char request_for_update = FALSE;
unsigned char demo = FALSE;
char process_command;
float ovf; 
unsigned int act_spd_bits;
_GLOBAL unsigned int HO_pulses_per_min;	     /* pulse per minute for micrologix plc */
unsigned int max_ramptime = 0;

_LOCAL float len, minutes;
_LOCAL unsigned int G_p_bits;

config_super *cs_=&cfg_super;
shared_HO_device *s = &shrHO;
config_HO_device *c = &cfgHO;
global_struct *g = &shr_global;

_LOCAL unsigned int current_time;
_GLOBAL float fHoUpdateSeconds;

/************************************************************************/
/* HO_PCC()                                                             */
/************************************************************************/
void HO_PCC()
{
   if (gDev_Init == FALSE)
   {
      init_dev();
      gDev_Init = TRUE;
   }
   
   current_time = start_time();

   #ifdef ALLEN_BRADLEY_DRIVE
    if(c->drive_type == AB_PLC_DRIVE)
     send_plc_alive_flag();
   #endif
   
   check_system();
   handle_drive_inhibit();
   if ((cfg_super.ltp_mode == CONTROL) && (!cfgHO.demo & DEMO_DRIVE_MOD))
      Check_ManualBackup_HO();
	
	if (cfgHO.demo & DEMO_DRIVE_MOD)
	{
		CLEAR_ALARM(EXD_IN_MANUAL)
		CLEAR_ALARM(EXD_INHIBIT)
	}

   if (shrHO.reset_drive || (exd_timebase == 0))
   {
     if (debugFlags.hoPcc)
     {
        bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
          BG_DEBUG_LEVEL_DEBUG,
          "about to reset drive, exd_timebase=%d\r\n",
          exd_timebase);
     }
        reset_drive();
   }
   if(shrHO.clear_inven)
   {
      PMEM.HO.InvTotal = 0.0;
      PMEM.HO.InvArea = 0.0;
      shrHO.clear_inven = FALSE;
   }

   if(shrHO.clear_shift)
   {
      PMEM.HO.ShftTotal = 0.0;
      PMEM.HO.ShftArea = 0.0;
      shrHO.clear_shift = FALSE;
   }

   switch (shrHO.oper_mode)
   {
      case AUTO_MODE:
         auto_mode();
         break;
      case MAN_MODE:
         manual_mode();
         break;
      case PAUSE_MODE:
         pause_mode();
         break;
      case MONITOR_MODE:
         monitor_mode();
         break;
   }
}
/************************************************************************/
/* poke() - gets the pulses & exd speed                                 */
/************************************************************************/
void poke(void)
{
//   unsigned char alt;  
//
//   if (alt) 
//   { 	
//      net_command(GET_PULSES);
//      alt = 0;
//   }
//   else  
//   {
//      net_command(GET_SPEED);
//      alt = 1;
//   }
   net_command(GET_PULSES);
   net_command(GET_SPEED);
}
/************************************************************************/
/* auto_mode() - automatic lenght throughput control loop               */
/************************************************************************/
void auto_mode(void)
{
	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG,
			"in auto mode\r\n");
	}

	if (s->ramping)
	{
		no_update_timer_on = FALSE;
		if ((elap_time(rampstart) > ramptime * 1000))
		{
			s->ramping = FALSE;
			totals();
		}
	}
	else if (!no_update_timer_on)
	{
		no_update_timer_on = TRUE;
		no_update_timer = current_time;
	}
	switch (Ho_running_mode)
	{
		case AUTO_MODE:
			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "case AUTO_MODE\r\n");
			}

			poke();
			
			if (c->cal_done)
			{
				check_coast();
				
				if (!s->coast && !s->ramping)
				{
					if (ready_to_update())
						update();
				}
			}
			break;

		case PAUSE_MODE:                                    /*lint -e616    */
			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "case PAUSE_MODE\r\n");
			}
			if (c->GearInputMethod != NO_GEAR_INPUT)
			{
				if (s->GearNum != shr_global.NewGearNum)
				{
					s->GearNum = shr_global.NewGearNum;
					calc_new_initial_rate_speed();
				}
			}
		case MONITOR_MODE:
			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "case MONITOR_MODE\r\n");
			}
		case NEW_MODE:
			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "case NEW_MODE\r\n");
			}
		case MAN_MODE:
			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "case MAN_MODE\r\n");
			}

			if (s->coast)
			coast_totals();
			else
			totals();
			set_initial_speed();
			net_command(SET_SPEED);
#if 0
			s->act_ltp = 0.0;
#endif
			Ho_running_mode = AUTO_MODE;
			s->accel = c->accel;      /* for correction, use device factors   */
			s->decel = c->decel;
			out_of_spec_count = 0;    /* restart count on any process change  */
			CLEAR_ALARM(DEV_OUT_OF_SPEC)
			break;                                            /*lint +e616    */
	}
}

/************************************************************************/
/* monitor_mode() - monitors the line speed only - no control           */
/************************************************************************/
void monitor_mode(void)
{
	if (debugFlags.hoPcc)
	{
		bgDbgInfo(&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	switch (Ho_running_mode)
	{
		case MONITOR_MODE:
			poke();
			if (c->cal_done)
			{
				check_coast();
				if (!s->coast)
				{
					if (ready_to_update())
						update();
				}
			}
			break;

		case PAUSE_MODE:
		case NEW_MODE:
		case MAN_MODE:
		case AUTO_MODE:
			Ho_running_mode = MONITOR_MODE;
			CLEAR_ALARM(DEV_OUT_OF_SPEC)
			totals();
			break;
	}
}

/************************************************************************/
/* manual_mode() - sets the motor speeds & monitors ltp                 */
/************************************************************************/
void manual_mode(void)
{
	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	if (s->ramping)
	{
		no_update_timer_on = FALSE;
		if ((elap_time(rampstart) > ramptime * 1000))
		{
			s->ramping = FALSE;
			totals();
		}
	}
	else if (!no_update_timer_on)
	{
		no_update_timer_on = TRUE;
		no_update_timer = current_time;
	}

	switch (Ho_running_mode)
	{
		case MAN_MODE:
			if ((s->speed_ctrl == EXTERNAL_CTRL) && (s->ho_manual_backup == AUTO_CONTROL))
				s->set_spd = (float)(PLC.AnaIn.AnaInp[1].Value / 32768);
			else if (s->ho_manual_backup == MANUAL_BACKUP)
			{
				IO_dig[0].channel[HOManual_Out_0] = OFF;
				IO_dig[0].channel[HOManual_Out_1] = OFF;
			}

			poke();
       
			if ((s->set_spd == 0.0) && (c->zero_crossing >= 0) && (s->ho_manual_backup == AUTO_CONTROL))
				s->act_ltp = 0.0;
			else
			{
				if (c->cal_done)
				{
					check_coast();
					
					if (!s->coast && !s->ramping)
					{
						if (ready_to_update())
							update();
					}
				}
			}
			
			if (c->GearInputMethod != NO_GEAR_INPUT)
			{
				if (s->act_spd == 0.0)
				{
					if (s->GearNum != shr_global.NewGearNum)
					{
						s->GearNum = shr_global.NewGearNum;
						calc_new_initial_rate_speed();
					}
				}
			}
			break;

		case MONITOR_MODE:                                    /*lint -e616    */
			c->rs_num = 0;
		case PAUSE_MODE:
			if (c->GearInputMethod != NO_GEAR_INPUT)
			{
				if (s->GearNum != shr_global.NewGearNum)
				{
					s->GearNum = shr_global.NewGearNum;
					calc_new_initial_rate_speed();
				}
			}
		case AUTO_MODE:
			s->coast = 0;
			s->diff_bits = 0;
		case NEW_MODE:
			if (s->speed_ctrl == EXTERNAL_CTRL)
				s->set_spd = s->act_spd;
#if 0
			s->act_ltp = 0.0;
#endif
			s->set_ltp = 0.0;
			totals();
			net_command(SET_SPEED);
			Ho_running_mode = MAN_MODE;
			CLEAR_ALARM(DEV_OUT_OF_SPEC)
			break;                                            /*lint +e616    */
	}
}
/************************************************************************/
/* pause_mode() - pauses the control loop and stops measuring ltp       */
/************************************************************************/
void pause_mode(void)
{
	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	switch (Ho_running_mode)
	{
		case PAUSE_MODE:
			poke();
			
			if (s->ho_manual_backup == MANUAL_BACKUP)
			{
				no_update_timer_on = FALSE;
				IO_dig[0].channel[HOManual_Out_0] = OFF;
				IO_dig[0].channel[HOManual_Out_1] = OFF;
			
				if (c->cal_done)
				{
					check_coast();
					if (!s->coast && !s->ramping)
					{
						if (ready_to_update())
							update();
					}
				}
			}
			else if (s->ramping)                           /* still slowing down   */
			{
				no_update_timer_on = FALSE;
				if (elap_time(rampstart)>ramptime * 1000)      /* done    */
				{
					s->ramping = FALSE;
					if (c->cal_done)                      /* don't worry w/coast  */
						totals();
				}
				else                                    /* not stopped yet      */
				{
					if (c->cal_done)
						check_coast();                      /* add in delta_b       */
				}
			}
			else if ((s->speed_ctrl == EXTERNAL_CTRL) && (s->ho_manual_backup == AUTO_CONTROL) &&
				!(shr_global.alarm_latch_bits & SYS_SHUTDOWN) && !shr_global.forced_pause )
			{
				s->oper_mode = MAN_MODE;
				Ho_running_mode = NEW_MODE;
				shrHO.init_man_extspd = 1;
			}
			else
			{
				s->act_ltp = 0.0;   /* now not running      */
				CLEAR_ALARM(DEV_OUT_OF_SPEC|DEV_OVERSPEED|DEV_UNDERSPEED|
					DEV_EXCESSIVE_COAST|DEV_DRV_SYS_FAILURE)
				s->coast = 0;
				s->drive_fail_count = 0;
				no_update_timer_on = FALSE;
			}
			
			if (c->GearInputMethod != NO_GEAR_INPUT)
			{
				if (s->act_spd == 0.0)
				{
					if (s->GearNum != shr_global.NewGearNum)
					{
						s->GearNum = shr_global.NewGearNum;
						calc_new_initial_rate_speed();
					}
				}
			}
			break;

		case MONITOR_MODE:                                  /*lint -e616    */
			s->set_ltp = s->act_ltp = s->set_spd = s->act_spd = 0.0;
			s->rate_uses_spd = FALSE;
			Ho_running_mode = PAUSE_MODE;
			break;

		case NEW_MODE:
		case MAN_MODE:
		case AUTO_MODE:
			s->set_ltp = 0.0;
			/*      s->set_spd = 0.0;*/
			net_command(SET_SPEED);
			CLEAR_ALARM(DEV_OUT_OF_SPEC|DEV_OVERSPEED|DEV_UNDERSPEED|
				DEV_EXCESSIVE_COAST|DEV_DRV_SYS_FAILURE)
			s->coast = 0;
			Ho_running_mode = PAUSE_MODE;
			break;                                            /*lint +e616    */
	}
}

/************************************************************************/
/* this handles going from drv inhibit to not w/ an actual speed > 0    */
/************************************************************************/
void handle_drive_inhibit(void)
{
  static unsigned int in_drive_inhibit = FALSE;
  //float f;
  float spd;
  //unsigned int new_ramptime, new_rampstart;

  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  if (s->set_spd > s->act_spd)
     spd = s->set_spd;
  else
     spd = s->act_spd;


  if (in_drive_inhibit && !(s->alarm_bits & EXD_INHIBIT) && (spd > 0.0))
  {
//    if ((f=c->spd_factor*c->accel) > 0.0)
//      new_ramptime = (unsigned int)((float)((spd * (float)100.0 * FTICKSPERSEC)/f));
//    else
//      new_ramptime = 10*TICKSPERSEC;
//    rampstart = current_time;
//	
//	   if (!s->ramping)
//    {
//      /*s->ramping = TRUE;*/
//      rampstart = current_time;
//      ramptime = new_ramptime;
//    }
//    else if (new_rampstart+new_ramptime > rampstart+ramptime)
//    {
//      ramptime = new_ramptime;
//      rampstart = new_rampstart;
//    }
//	
      no_update_timer_on = FALSE;
   }
  
   in_drive_inhibit = s->alarm_bits & EXD_INHIBIT;
	
   if (cfgHO.demo & DEMO_DRIVE_MOD)
		CLEAR_ALARM(EXD_INHIBIT)

   if ((cfg_super.ltp_mode == CONTROL) && (!cfgHO.demo & DEMO_DRIVE_MOD))
	{
		if (c->drive_type != PCC)
		{
			if (c->drive_inhibit_invert)
			{
				if  (!IO_dig[0].channel[DriveInh_HO]) 
					CLEAR_ALARM(EXD_INHIBIT)
				else if (!(s->alarm_bits & EXD_INHIBIT))
					SET_ALARM(EXD_INHIBIT)
			}
			else
			{
				if  (IO_dig[0].channel[DriveInh_HO])
					CLEAR_ALARM(EXD_INHIBIT)
				else if (!(s->alarm_bits & EXD_INHIBIT))
					SET_ALARM(EXD_INHIBIT)
			}
		}
   }
}
/************************************************************************/
/* check_system() -                                                     */
/************************************************************************/
void check_system()
{
   char oper_mode;
   float f1;

   if (debugFlags.hoPcc)
   {
     bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }

   oper_mode = HOCommand.Command;

   if (c->movepts < 10)
      c->algorithm = STANDARD;
   else
      c->algorithm = STATISTIC;

   if(HOCommand.CommandPresent)
   {
      switch(HOCommand.Command)
      {
         case PAUSE_MODE:
         	 if (HOCommand.SetAccelDecel < 0.0001)              /* no decel factor included    */
            {
                    /* use device a/d factors       */
              s->decel = c->decel;
            }
            s->set_spd = 0;
            s->have_new_act = FALSE;
	         shrHO.oper_mode = PAUSE_MODE;
            break;
         case AUTO_MODE:
          f1 = HOCommand.SetLTP;
	       if (HOCommand.SetLTP > 0.001)
          {
              if ((fabs((double)percent_error(s->set_ltp,f1)) > .005) || (Ho_running_mode != AUTO_MODE))
              {
                 if (c->rs_resetchg > 0.0)
                 {
                   if (fabs((double)percent_error(f1,last_set_ltp)) > c->rs_resetchg)
                      reset_rate_speed = TRUE;
                 }
                 if (BATCH_RECIPE_NOW->step_size > 0.0)
                    reset_rate_speed = TRUE;
                    s->set_ltp = HOCommand.SetLTP;
                 if (Ho_running_mode == AUTO_MODE)
                   Ho_running_mode = NEW_MODE;
              }
              
              if (HOCommand.SetAccelDecel >= 0.0 )
                 s->decel = HOCommand.SetAccelDecel;
              
              if (HOCommand.SetAccelDecel >= 0.0)
                 s->accel = HOCommand.SetAccelDecel;
           }
           else
           {
             oper_mode = PAUSE_MODE;
	          HOCommand.Command = PAUSE_MODE;
           }
           s->have_new_act = FALSE;
           break;

         case MAN_MODE:
            if (HOCommand.SetSpeed < 0.001)              /* no speed included    */
            {
              /* f1 = s->act_spd; */
			     f1 = 0;
              s->accel = c->accel;          /* use device a/d factors       */
              s->decel = c->decel;
            }
            else
            {
               f1 = (float)HOCommand.SetSpeed;

               if (HOCommand.SetAccelDecel >= 0.0 )
                  s->decel = HOCommand.SetAccelDecel;
               else
                  s->decel = c->decel;

               if (HOCommand.SetAccelDecel >= 0.0)
                  s->accel = HOCommand.SetAccelDecel;
               else
                  s->accel = c->decel;
            }

            if (Float_NEQ(f1,s->set_spd) || ((c->zero_crossing < 0) && (f1 == 0.0)))
            {
              s->set_spd = f1;
              if (Ho_running_mode == MAN_MODE)
                Ho_running_mode = NEW_MODE;
            }
            s->have_new_act = FALSE;
            break;
         case MONITOR_MODE:
           s->have_new_act = FALSE;
           break;
         case REINIT:
            shrHO.reset_drive = TRUE;
            break;
      }
      /* insure accel & decel not fried here                              */
      if ((shrHO.accel < 0.0) || (shrHO.accel > c->decel))
         shrHO.accel = c->accel;
      if ((shrHO.decel < 0.0) || (shrHO.decel > c->decel))
         shrHO.decel = c->decel;

      switch(oper_mode)
      {
         case MAN_MODE:
            if ((shrHO.set_spd == 0.0) && (shrHO.act_spd == 0.0) && (c->zero_crossing >= 0.0))
	         {
               oper_mode = PAUSE_MODE;
	            HOCommand.Command = PAUSE_MODE;
	         }
            break;
         case AUTO_MODE:
            if ((shrHO.set_ltp == 0.0) && (shrHO.set_spd == 0.0) && (shrHO.act_spd == 0.0))
	         {
               oper_mode = PAUSE_MODE;
	            HOCommand.Command = PAUSE_MODE;
	         }
            break;
         default:
            break;
      }
      shrHO.oper_mode = oper_mode;   /* must do here - keep alarms off       */
      HOCommand.CommandPresent = 0;
   }
   else
      process_command = FALSE;
     
   if (s->set_ltp > 0.0)
      last_set_ltp = s->set_ltp;
   if (s->set_spd > 0.0)
      last_set_spd = s->set_spd;
   shrHO.alarm_bits       |= shrHO.alarm_force_bits;
   shrHO.alarm_latch_bits |= shrHO.alarm_force_bits;
   if (shrHO.oper_mode != MONITOR_MODE)
      shrHO.rate_uses_spd = ((cfgHO.rs_num > 0) && (!(shrHO.alarm_bits & (EXD_INHIBIT|DEV_DRV_SYS_FAILURE))));
      
   fHoUpdateSeconds = ((float)s->delta_t / FTICKSPERSEC );  //divide
}
/************************************************************************/
/* init_dev() - setup the device                                        */
/************************************************************************/
void init_dev()
{
  //unsigned long HostAddr; 
  //int i;
  
  if (debugFlags.hoPcc)
  {
     bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  cs_ = &cfg_super;

   shrHO.oper_mode = PAUSE_MODE;
   Ho_running_mode = NEW_MODE;  
   DriveCommand[HO_DRIVE].CMDRdy = TRUE;

   numbad = 0;
   ramptime = 0;
   out_of_spec_count = 0;
   no_update_timer_on = FALSE;
   reset_rate_speed = FALSE;

   if (cs_->ltp_mode == CONTROL)
   {
      if (c->GearInputMethod == GEAR_IN_RECIPE)
         shr_global.NewGearNum = (unsigned char)UI_RECIPE.ho_gear_num;
      else if (c->GearInputMethod == NO_GEAR_INPUT)
         shr_global.NewGearNum = 0;
      shrHO.GearNum = shr_global.NewGearNum;
   }

   shrHO.rate_uses_spd = (c->rs_num > 0);
   reset_drive();                        /* reset drive functions        */

#if (MACH!=GRAVITROL)   /* Special stuff only for blenders */
   c->min_ltp = 0.0;
#endif
   shrHO.have_new_act = FALSE;

//   if(c->drive_type==AB_PLC_DRIVE)
//   {
//      brsmemset(&HostAddr, 0, sizeof(HostAddr)); 
//      brsmemcpy(&HostAddr, cfgHO.drive_ip_address, sizeof(cfgHO.drive_ip_address));
//
//      OpenRemoteSocketPLC(0xaf12, HostAddr, 1, c->drive_mod.chan);
//
//      if (connectedPLC)
//      {
//         SendRegSessionCommand(c->drive_mod.chan);
//         RecvRegSessionResponse(c->drive_mod.chan);
// /*        tsleep(3);*/
//         SendRRData(c->drive_mod.chan);
//         RecvSendRRDataResponse(c->drive_mod.chan);
//      }
//      exd_timebase = AB_PLC_TIMEBASE;
//#if 0
//   if (connectedPLC)
//      printf("ho_pcc: chan = %d connected, counter = %d\n", c->drive_mod.chan, counter);
//   else
//      printf("ho_pcc: chan = %d NOT CONNECTED, counter = %d\n", c->drive_mod.chan, counter);
//#endif
//
//   }
}

/************************************************************************/
/* reset_drive() - resets various flags & gets set for when board       */
/*  comes back on line                                                  */
/************************************************************************/
void reset_drive(void)
{
  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if(local_ltp_mode == CONTROL)
   {
      switch (c->drive_type)
      {
         case PCC:
            net_command(SET_ACCEL);
            net_command(SET_DECEL);
            if (!EXD_DEAD)
            {
               if (s->speed_ctrl != EXTERNAL_CTRL)
               {
                  s->accel = 0.0;                 /* make sure done immediately   */
                  s->decel = 0.0;
                  net_command(SET_SPEED);         /* reset speed                  */
               }
            }
            if (!EXD_DEAD)
            {
               demo = c->demo & DEMO_DRIVE_MOD;
#if 0  /* fmk */
               read_info(c->drive_mod.addr,cs_->net_max_tries);
#endif
               exd_timebase = shr_global.stats[c->drive_mod.addr].timebase;
               if (exd_timebase != 0)
               {
                  min_per_tbit = 1.0 / (60.0 * (float) exd_timebase);
                  s->reset_drive = FALSE;
               }
               if (debugFlags.hoPcc)
               {
                 bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                     BG_DEBUG_LEVEL_DEBUG, "timebase=%d min_per_tbit=%f\r\n",
                     exd_timebase, min_per_tbit);
               }
#if 0  /* fmk */
               check_exd_sw_rev();
#endif
            }
            break;
         case AB_PLC_DRIVE:
            s->reset_drive = FALSE;
            exd_timebase = 100;
            min_per_tbit = 1.0 / (60.0 * (float) 100);
            if (debugFlags.hoPcc)
            {
              bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                  BG_DEBUG_LEVEL_DEBUG, "exd_timebase=%d\n", exd_timebase);
            }
            break;
      }
   }
   else
   {
      if(local_ltp_mode == MONITOR)
      {

         switch (c->drive_type)
         {
            case AB_PLC_DRIVE:
               s->reset_drive = FALSE;
               exd_timebase = 100;
               min_per_tbit = 1.0 / (60.0 * (float) 100);
               break;
            case PCC:
            default:
#if 0  /* fmk */
               read_info(c->drive_mod.addr,cs_->net_max_tries);
#endif
               exd_timebase = shr_global.stats[c->drive_mod.addr].timebase;
               if (exd_timebase == 0)
                  exd_timebase = 1228800;     /* have some default here   */
               if (exd_timebase != 0)
               {
                  min_per_tbit = 1.0 / (60.0 * (float) exd_timebase);
                  s->reset_drive = FALSE;
               }
               if (debugFlags.hoPcc)
               {
                 bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "timebase=%d min_per_tbit=%f\r\n",
                   exd_timebase,min_per_tbit);
               }
               break;
         }
         net_command(GET_PULSES);            /* check for pulses             */
      }
   }
}
/************************************************************************/
/* totals() - adds in totals                                            */
/************************************************************************/
void totals(void)
{
  double lgt;

  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if ((s->delta_b > 0) )
   {
      if (local_ltp_mode == MONITOR || !(s->alarm_bits & EXD_INHIBIT))
      {
         lgt = s->delta_b * c->lenperbit;
         add_len(lgt);
      }
   }
   s->delta_b = 0;
   s->delta_t = 0;
}

/************************************************************************/
/* add_len(len) - adds in len to totals                                 */
/************************************************************************/
void add_len(double len)
{
   config_super *cs = &cfg_super;

   if (debugFlags.hoPcc)
   {
     bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }

   if ((Ho_running_mode == MONITOR_MODE) && (s->newrate < c->min_ltp))
      return;

   PMEM.HO.ShftTotal = PMEM.HO.ShftTotal + len;
   PMEM.HO.InvTotal = PMEM.HO.InvTotal + len;
   PMEM.HO.ShftArea = PMEM.HO.ShftArea + (len * ACTUAL_RECIPE->width * cs->width_k * cs->area_k);
   PMEM.HO.InvArea = PMEM.HO.InvArea + (len * ACTUAL_RECIPE->width * cs->width_k * cs->area_k);
}
/************************************************************************/
/* percent_error(a,b) - computes the percent difference between a & b.         */
/************************************************************************/
float percent_error(float a,float b)
{
  float f;

  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  if (b == 0.0)
    f = 0.0;
  else
    f = (a-b)/b;
  return (f);
}
/************************************************************************/
/* check_coast() - checks for coasting conditions and sets into coast   */
/*      depending upon current running mode                             */
/************************************************************************/
void check_coast(void)
{
 
   int max_bits, min_bits;
   int bad;
   float HoSpeedFactor;			 

   if (debugFlags.hoPcc)
   {
     bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }

   HoSpeedFactor = c->spd_factor[s->GearNum];

   if (s->curtime == 0)          /* if same reading, don't do anything   */
      return;

   if ((Ho_running_mode == AUTO_MODE) && !s->ramping)
   {
      if (c->lenperbit > 0.0)
      {
         #ifdef ALLEN_BRADLEY_DRIVE
         if (cfgHO.drive_type == AB_PLC_DRIVE)
         expected = s->set_ltp * (float) s->curtime/6000/c->lenperbit;     
         else
         #endif
         expected = s->set_ltp * (float) s->curtime/60000/c->lenperbit; 
      }    
      else
         expected = 0.0;

     
      max_bits = (int) ((float)((expected * c->coast_error) + (float)c->coast_tolerance));
      if (c->coast_error > 0)
         min_bits = (int) ((float)((expected / c->coast_error) - (float)c->coast_tolerance));
      else
         min_bits = 0;

      s->diff_bits = (int) (s->curbits - (unsigned int) expected);
      bad = ((int)s->curbits > (int)max_bits) || ((int)s->curbits < (int)min_bits);
      if (debugFlags.hoPcc)
      {
        bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
            BG_DEBUG_LEVEL_DEBUG,
            "bad=%d cubits=%d max_bits =%d min_bits=%d coasts=%d\r\n",
            bad, s->curbits, max_bits, min_bits, s->coast);
      }
   }
   else                                  /* man, or ramping              */
      bad = FALSE;

   if (bad)
   {
      if (s->coast)                       /* still bad                    */
      {
         s->coast++;
         /* check for too many coasts but not drive sys fail               */
         /* printf("check_coast(bad): coast=%d \n",s->coast);              */
         if ((elap_time((TIME)coast_timer) > c->max_coast_time * (unsigned int)TICKSPERSEC) &&
            !(s->alarm_bits & DEV_DRV_SYS_FAILURE))
         {
           if (debugFlags.hoPcc)
           {
             bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                 BG_DEBUG_LEVEL_DEBUG, "set unstable (alarm = %d)\r\n",
                 DEV_EXCESSIVE_COAST);
           }
           SET_ALARM(DEV_EXCESSIVE_COAST)
           if (s->coast > 3*c->initial_coast)
              s->coast = (unsigned char)(3 * c->initial_coast);  /* limit it  */
         }
         else if (s->alarm_bits & DEV_DRV_SYS_FAILURE)
            s->coast = (unsigned char)(2 * c->initial_coast); /*limit coast*/
      }
      else
      {
         s->coast = c->initial_coast;      /* was good, now bad            */
         coast_timer = current_time;      /* start timer for coast alarms */
         if (debugFlags.hoPcc)
         {
            bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                BG_DEBUG_LEVEL_DEBUG, "CD (START), %08x\r\n", s->time_bits);
         }
      }
   }
   else                                  /* good reading                 */
   {
      if (s->coast)
      {
         s->coast--;
         if (!s->coast)                    /* coming out of coast          */
         {
            coast_totals();
            if (debugFlags.hoPcc)
            {
               bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "CD(END-B),%08x\r\n",
                   s->time_bits);
            }
         }
      }
      else                      /* not coasting, reading good           */
         s->delta_b += s->curbits;
   }
  
   if (no_update_timer_on)
   {
      if (!(s->alarm_bits & DEV_DRV_SYS_FAILURE)) /* don't check w/drvfail*/
      {
         if (elap_time((TIME)no_update_timer) > ((unsigned int)15 * c->calctime))
         {
            SET_ALARM(DEV_EXCESSIVE_COAST)
         }
      }
      else
         no_update_timer = current_time;  /* only start w/no drive fail   */
   }
  
   /* check for drive system failure     */ /* all new for v220  */
   if ((s->act_spd > c->max_spd*c->alarm_low_spd*HoSpeedFactor/1.0) && !s->ramping && (c->max_drive_fail > 0))
   {
      if (s->curbits < c->drive_fail_error)
      {
        s->drive_fail_count++;
      }
      else if (s->drive_fail_count > 0)
      {
         s->drive_fail_count--;
      }
      if (s->drive_fail_count > c->max_drive_fail)
      {
         s->drive_fail_count = c->max_drive_fail;
         SET_ALARM(DEV_DRV_SYS_FAILURE)
         s->act_ltp = 0.0;
         s->newrate = 0.0;
         if (s->coast)
         {
            s->coast = (unsigned char) (2 * c->initial_coast);/* limit coast*/
            coast_timer = current_time;            /* alarm stuff          */
         }
      }
   }
   else if ((s->curbits > c->drive_fail_error) && (s->drive_fail_count > 0))
      s->drive_fail_count--;
   if (s->drive_fail_count == 0)
      CLEAR_ALARM(DEV_DRV_SYS_FAILURE)
       
   if (s->coast)
      s->coasts =1;
   else 
      s->coasts = 0;
}
/************************************************************************/
/* coast_totals() - adds in estimated totals                            */
/************************************************************************/
void coast_totals(void)
{
   float lgt;
   
  if (debugFlags.hoPcc)
  {
     bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  switch(Ho_running_mode)
  {
    case AUTO_MODE:
      #ifdef ALLEN_BRADLEY_DRIVE
      if (cfgHO.drive_type == AB_PLC_DRIVE)
         lgt = s->set_ltp * (float) s->delta_t/6000;
      else 
      #endif
      lgt = s->set_ltp * (float) s->delta_t/60000;
      add_len(lgt);
      s->delta_b = 0;
      s->delta_t = 0;
      break;
    case MAN_MODE:
    case MONITOR_MODE:
      if (s->act_ltp == 0.0)
        totals();
      else
      {
         #ifdef ALLEN_BRADLEY_DRIVE
         if (cfgHO.drive_type == AB_PLC_DRIVE)
            lgt = s->set_ltp * (float) s->delta_t/6000;
         else 
         #endif
         lgt = s->set_ltp * (float) s->delta_t/60000;
        add_len(lgt);
        s->delta_b = 0;
        s->delta_t = 0;
      }
      break;
    case PAUSE_MODE:
      totals();
      break;
  }
}
/************************************************************************/
/* ready_to_update() - returns true if time to update                   */
/************************************************************************/
int ready_to_update(void)
{
  unsigned int ticks;
  int retcode = FALSE;
  char ok_to_check = TRUE;

  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  if(ok_to_check)
  {
    ticks = s->delta_t ;
 
    if (debugFlags.hoPcc)
    {
      bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
          BG_DEBUG_LEVEL_DEBUG, "ticks=%d calctime=%d delta_b=%d\n",
          ticks, c->calctime, s->delta_b);
    }

    if (cfgHO.drive_type != AB_PLC_DRIVE)
    {
      if (ticks >= c->calctime)
        retcode = TRUE;
    }
    else 
    {		       
      if (ticks >= (c->calctime/10))
        retcode = TRUE;
    }		       		       
  }
  return retcode;
}

/************************************************************************/
/* set_update_flags() -                                                 */
/************************************************************************/
void set_update_flags(void)
{
  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  request_for_update = FALSE;
  s->updated_flag = TRUE;
}
/************************************************************************/
/* set_initial_speed() - calculate initial set->speed when going into   */
/*  auto mode based upon history                                        */
/************************************************************************/
void set_initial_speed(void)
{
  if (debugFlags.hoPcc)
  {
    bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (c->rs_num == 0)                           /* never in auto before */
   {
      if (s->act_ltp > 0.0)                       /* in man               */
      {
         if ((c->zero_crossing < 0) && (s->set_spd == 0))
            s->set_spd = (s->set_ltp * (float)fabs((double)c->zero_crossing) * (float)100.0) /
                         (s->act_ltp * (float)((float)100.0 + (float)fabs((double)c->zero_crossing)));
         else
            s->set_spd = (s->set_ltp / s->act_ltp) * s->set_spd;
      }
      else
         s->set_spd = HO_NRS_SPD;
   }
   else
   {
      if (c->rs_ave > 0.0)
         s->set_spd = s->set_ltp / c->rs_ave;
      else
         s->set_spd = HO_NRS_SPD;
   }
}
/************************************************************************/
/* update() - update ltp                                                */
/************************************************************************/
void update(void)
{
	float update_error;
	float rs_error;
	float act_rsfact;
	unsigned char rs_num;
	register float HoSpeedFactor;

	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	HoSpeedFactor = c->spd_factor[s->GearNum];

#ifdef ALLEN_BRADLEY_DRIVE
	if (cfgHO.drive_type == AB_PLC_DRIVE)
		minutes = ((float) s->delta_t)/6000;
	else 
#endif
	minutes = ((float) s->delta_t)/60000;
   
	len = (float) s->delta_b * c->lenperbit;

	if (minutes > 0.0)  
	{
		s->newrate = len / minutes;		   /* B&R way to calculate new rate */
		if (c->drive_type==AB_PLC_DRIVE)
			s->newrate = HO_pulses_per_min * c->lenperbit;      /*   AB PLC way to calculate new rate */   
	}
	else
	{
		totals();
		return;
	}

	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "len=%f minutes=%f rate=%f\r\n",
			len, minutes, s->newrate);
	}

	   if ((Ho_running_mode == MONITOR_MODE) ||
	      (s->alarm_bits & (EXD_INHIBIT|EXD_IN_MANUAL|EXD_PLC_LOW_BATTERY|DEV_DRV_SYS_FAILURE)))
	      //(s->set_spd < (c->alarm_low_spd*HoSpeedFactor/1.0)))  /* don't use alarms for speed   */
	   {
	      no_update_timer = current_time;
	      if ((Ho_running_mode == MONITOR_MODE) && (s->newrate < (c->min_ltp + .0001)))
	      {
	         s->rate_uses_spd = FALSE;
	         s->act_ltp = 0.0;
	         set_update_flags();
	         return;
	      }
	      s->rate_uses_spd = FALSE;
	      s->act_ltp = s->newrate;
	      s->have_new_act = TRUE;
	      set_update_flags();
	      totals();
	      if ((debugFlags.hoPcc) && (exd_timebase != 0))
	      {
	        bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
	            BG_DEBUG_LEVEL_DEBUG, "UTRS,%f,%f,%f,%f\r\n",
	            (float) s->delta_t / (float) exd_timebase, s->set_spd,
	            s->newrate, s->act_ltp);
	      }
	      return;
	   }

	/* drive system failure detection                             */
	if (((Ho_running_mode == AUTO_MODE) && (s->newrate < (0.1 * s->set_ltp))) ||
		((Ho_running_mode == MAN_MODE) && (s->act_spd > 0.0) && (s->newrate <= 0.0)))
	{
		s->drive_fail_count++;	
		if (s->drive_fail_count > c->max_drive_fail)
		{
			s->drive_fail_count = c->max_drive_fail;
			SET_ALARM(DEV_DRV_SYS_FAILURE)
			s->drive_fail_count = c->max_drive_fail;
			/*     no_update_timer = current_time;*/
			s->rate_uses_spd = FALSE;
			s->act_ltp = s->newrate;
			/*   set_update_flags();*/
			totals();
		}
		if ((debugFlags.hoPcc) && (exd_timebase != 0))
		{
			bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
				BG_DEBUG_LEVEL_DEBUG, "UTRS, %f, %f, %f, %f\r\n",
				(float) s->delta_t / (float) exd_timebase, s->set_spd,
				s->newrate, s->act_ltp);
		}
		return;
	}
	else
	{
		CLEAR_ALARM(DEV_DRV_SYS_FAILURE)
		s->drive_fail_count = 0;
	}

	if ((Ho_running_mode == MONITOR_MODE) && (s->set_spd == 0.0))
		s->rate_uses_spd = FALSE;
	else
		s->rate_uses_spd = TRUE;

	if (reset_rate_speed)
	{
		rs_num = 0;
		reset_rate_speed = FALSE;
	}
	else
		rs_num = c->rs_num;

	update_error = (float)fabs((double)percent_error(s->newrate,s->set_ltp));
	if ((update_error < c->max_error) || (++numbad > c->max_bad) ||
		(rs_num == 0))
	{
		no_update_timer = current_time;

		if (s->set_spd != 0.0)
			act_rsfact = s->newrate / s->set_spd;
		else if (c->zero_crossing < 0)
			act_rsfact = s->newrate / (float)fabs((double)c->zero_crossing);
		else
			act_rsfact = c->rs_ave;

		if (rs_num == 0)
		{
			rs_num = 1;
			c->rs_last = 0;
			c->rs_fact[c->rs_last] = act_rsfact;
			c->rs_sum = act_rsfact;
			c->rs_ave = act_rsfact;
		}
		else
		{
			if ((Ho_running_mode == AUTO_MODE) && (c->rs_clip))
			{
				rs_error = percent_error(act_rsfact,c->rs_ave);
				if ((float)fabs((double)rs_error) > c->rs_maxchg)
				{
					if (rs_error < 0.0)
						rs_error = -(c->rs_maxchg);
					else
						rs_error = c->rs_maxchg;
					act_rsfact = (1 + rs_error) * c->rs_ave;
				}
			}

			if (debugFlags.hoPcc)
			{
				bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
					BG_DEBUG_LEVEL_DEBUG, "RS, %f, %f, %f, %f\r\n",
					s->newrate, s->set_spd, act_rsfact, c->rs_ave);
			}

			c->rs_last = (c->rs_last + 1) % c->movepts;
			if (++(rs_num) > c->movepts)
			{
				rs_num = c->movepts;
				c->rs_sum -= c->rs_fact[c->rs_last];
			}
			c->rs_sum = c->rs_sum + act_rsfact;
			c->rs_fact[c->rs_last] = act_rsfact;
			if (rs_num > 0)
				c->rs_ave = c->rs_sum / rs_num;
		}
		
		c->rs_num = rs_num;   
   	s->act_ltp = c->rs_ave * s->act_spd;
			
		s->have_new_act = TRUE;

		if ((debugFlags.hoPcc) && (exd_timebase != 0))
		{
			bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
				BG_DEBUG_LEVEL_DEBUG, "UTRS, %f, %f, %f, %f\r\n",
				s->delta_t / exd_timebase, s->set_spd, s->newrate, s->act_ltp);
		}

		if ((update_error > c->dead_band) && (Ho_running_mode == AUTO_MODE))
		{
			if (c->rs_ave > 0.0)
				s->set_spd = s->set_ltp / c->rs_ave;
			else
				s->set_spd = 0;

			net_command(SET_SPEED);
		}
		numbad = 0;
		if ((Ho_running_mode == AUTO_MODE) && (s->set_ltp > 0.0))
		{
			if (out_of_spec_count++ > c->movepts)       /* time to check*/
			{
				out_of_spec_count = (c->movepts + 1);
				if (((float)fabs((double)(s->act_ltp-s->set_ltp)/s->set_ltp)) > c->out_of_spec_limit)
					SET_ALARM(DEV_OUT_OF_SPEC)
				else
					CLEAR_ALARM(DEV_OUT_OF_SPEC)
			}
		}
		CLEAR_ALARM(DEV_EXCESSIVE_COAST)
	}
	else
	{
		if (c->zero_crossing < 0)
		{
			reset_rate_speed = TRUE;
		}
		numbad++;
	}
	totals();
	set_update_flags();
}

/************************************************************************/
/************************************************************************/
void calc_new_initial_rate_speed(void)
{
  if (cs_->spd_in_percent)
    c->rs_num = 0;
  else if (!c->cal_done)
    c->rs_num = 0;
  else   /* have a speed factor, and this means rate speed is = 1)   */
  {
    c->rs_num = 1;
    c->rs_last = 0;
    c->rs_fact[0] = 1.0;
    c->rs_sum = 1.0;
    c->rs_ave = 1.0;
  }
}
/************************************************************************/
/* net_command(cmd) - sends out cmd on the network, parses the response,*/
/*   and sets the alarm_bits                                            */
/************************************************************************/
void net_command(int cmd)
{
	//unsigned int timeperbit;
	float accel_decel, ftmmp = 0.0;
	float HoSpeedFactor;
	//unsigned int last_time, last_bits; // rpm;
   
	if (debugFlags.hoPcc)
	{
		bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	HoSpeedFactor = c->spd_factor[s->GearNum];

	switch(cmd)
	{
		case GET_PULSES:
#ifdef ALLEN_BRADLEY_DRIVE
			if (c->drive_type==AB_PLC_DRIVE)
			{
				if (!connectedPLC) 
					init_ethernet();
 
				if (connectedPLC)
				{
					SendUnitDataCommand(c->drive_mod.chan, PLC_5_READ, 21,  20, 2, FLOAT);
					RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_READ);
				}

				s->pulse_bits = (int)PLC_READ_FLOAT_DATA[0];
				s->time_bits = (int)PLC_READ_FLOAT_DATA[1];
				if (s->pulse_bits >= s->last_pulse_bits)
					s->curbits =  s->pulse_bits - s->last_pulse_bits;
				if (s->time_bits >= s->last_time_bits)
					s->curtime = s->time_bits - s->last_time_bits;
				s->delta_t += s->curtime;
				if (s->curtime != 0)
					HO_pulses_per_min = (s->curbits * 100 * 60)/ s->curtime;	   /* AB PLC uses 0.01s as timebase */ 
				s->last_pulse_bits = s->pulse_bits;
				s->last_time_bits = s->time_bits;
			}
			else
#endif
			{
				if (!EXD_DEAD)
				{	   
               if (cfgHO.demo & DEMO_PULSES)               
               {
                  calc_demo_values();
                  s->pulse_bits = G_p_bits;           
               }
               else
					   s->pulse_bits = PLC.PulseCounter.CounterValue[0];
               
					s->curtime = elap_time(interval_timer);     /* in ms */
					if (s->last_pulse_bits < s->pulse_bits) 
						s->curbits = s->pulse_bits - s->last_pulse_bits;
					s->delta_t += s->curtime;
					s->last_pulse_bits = s->pulse_bits;
					interval_timer = current_time;
				}
			}
			break;

		case SET_SPEED:
         
		/* if (demo)
		 s->set_spd = HOCommand.SetSpeed;  */
			
			if (s->set_spd > c->max_spd * c->alarm_high_spd)
			{
				SET_ALARM(DEV_OVERSPEED)
				if (s->set_spd > c->max_spd)
					s->set_spd = c->max_spd;
			}
			else
				CLEAR_ALARM(DEV_OVERSPEED)

			/* if ((s->set_spd < (c->max_spd*c->alarm_low_spd*HoSpeedFactor)) && (s->oper_mode == AUTO_MODE))  */
		
			if ((s->set_spd < c->max_spd * c->alarm_low_spd) && (s->oper_mode == AUTO_MODE)) 	
			{
			   SET_ALARM(DEV_UNDERSPEED)
			   if (s->set_spd < 0.0)
			      s->set_spd = 0.0;
			}
			else
		   	CLEAR_ALARM(DEV_UNDERSPEED)

			//      if (((double)fabs((double)s->set_spd-s->act_spd) < (0.0003*HoSpeedFactor)) ||/*same spd   */
			//            (s->alarm_bits & EXD_IN_MANUAL))      /* in manual backup     */
			//            accel_decel = 0.0;
			if (s->set_spd < s->act_spd)                 /* decel        */
				accel_decel = s->decel;
			else                                              /* accel        */
				accel_decel = s->accel;
#if 1
			/* took this out to calculate the ramptime differently */
			if (HoSpeedFactor > 0.0)
			{
				if (accel_decel > 0.0)
				{
					ovf = ((float)fabs((s->set_spd-s->act_spd)/(accel_decel*HoSpeedFactor)));
					max_ramptime = (float) (1/ accel_decel);
				}
				else
					ovf = 0.0;
				if (ovf > OVF_INT)  /* overflow */
				{
					accel_decel = 0.0;
					ovf = 0;
				}

				if (ovf > max_ramptime)
					ovf = max_ramptime;

				ramptime = (unsigned int) ovf;
			}
			else
			{
				ramptime = 0;
				accel_decel = 0.0;
			}

			ho_ramptime_plc = ramptime;	      /* in ms */
			if (ramptime > 1)
			{
				// add some extra time 
				//        if (ramptime < (unsigned)(3 * TICKSPERSEC))
				//          ramptime = ramptime * 2 + 1;
				//        else if (s->set_spd > 0.0)
				//          ramptime += (unsigned)(10 * TICKSPERSEC);
				//        else
				//          ramptime += (unsigned)(3 * TICKSPERSEC);
				s->ramping = TRUE;
			}
			else
				s->ramping = FALSE;   
#endif

			if (HoSpeedFactor > 0.0)
			{
				ovf = s->set_spd * OVF_INT;       /* 16bits resolution */    
				/* adjust speed bits for zero crossing of motor speed */
        
				if (ovf == 0)
					s->spd_bits = 0;
        
				if ((ovf > 0.0) && (c->zero_crossing > 0.0))
				{
					/* Scale down for bandwidth */
					ovf = ovf * (((float)100.0 - c->zero_crossing) / (float)100.0);
					/* add in offset adjustment */
					ovf = ovf + ((float)327.67 * c->zero_crossing);
				}

				if (ovf > OVF_INT)
					s->spd_bits = OVF_INT;
				else
					s->spd_bits = ovf;
			}
			else
				s->spd_bits = 0;
		
			/*ipwrite((char *)&packet.data[CMD+2],(unsigned int)s->spd_bits,2);*/
	  
			DriveCommand[HO_DRIVE].CMDRdy = TRUE;
			DriveCommand[HO_DRIVE].CMDReturn = FALSE;
			DriveCommand[HO_DRIVE].SetSpeed = (int)s->spd_bits;
			DriveCommand[HO_DRIVE].AccelDecelValue =  accel_decel;

#ifdef ALLEN_BRADLEY_DRIVE
			if(c->drive_type==AB_PLC_DRIVE)
			{
				if (!connectedPLC)
					init_ethernet();
   
				if (connectedPLC)
				{
					if (HoSpeedFactor != 0)
						PLC_WRITTEN_FLOAT_DATA[0] = (s->set_spd * 100.0)/HoSpeedFactor;
					
					PLC_WRITTEN_FLOAT_DATA[1] = ho_ramptime_plc;
					SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 20, 2 * c->drive_mod.chan, 2, FLOAT);
					RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
				}
	  
				set_exd_status();
			}
			else
#endif
			{
				if (HoSpeedFactor > 0.0)
				{
					ovf = (float)(s->set_spd * OVF_INT / HoSpeedFactor);

					/* adjust speed bits for zero crossing of motor speed */
					if ((ovf > 0.0) && (c->zero_crossing > 0.0))
					{
						/* Scale down for bandwidth */
						ovf = (float)(ovf * ((100.0 - c->zero_crossing) / 100.0));
						/* add in offset adjustment */
						ovf = (float)(ovf + (327.67 * c->zero_crossing));
					}

					if (ovf > OVF_INT)
						s->spd_bits = OVF_INT;
					else
						s->spd_bits = (unsigned short) ovf;
				}
				else
					s->spd_bits = 0;
			}
			if (s->ramping)
				rampstart = current_time;
			break;

		case GET_SPEED:
			/*          printf("net_command: GET_SPEED, chan = %d\n",c->drive_mod.chan); */
			demo = c->demo & DEMO_DRIVE_MOD;
			/*       printf("net_command: get speed ,drive_type = %d\n", c->drive_type); */
#ifdef ALLEN_BRADLEY_DRIVE
			if (c->drive_type == AB_PLC_DRIVE)
			{
				if (!connectedPLC) 
					init_ethernet();
				
				if (connectedPLC)
				{
					/* printf("net_command: sending get speed ,chan= %d\n", c->drive_mod.chan); */
					SendUnitDataCommand(c->drive_mod.chan, PLC_5_READ, 21,  c->drive_mod.chan, 1, FLOAT);
					/* printf("net_command: get response to get speed ,chan= %d\n", c->drive_mod.chan); */
					RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_READ);
					/* printf("net_command: g0t response to get speed ,chan= %d\n", c->drive_mod.chan); */
					/* set_exd_status();*/
					s->act_spd = (PLC_READ_FLOAT_DATA[0] * HoSpeedFactor)/100;
					/* printf("HO: s->act_spd = %f\n", s->act_spd); */
				}
			}
			else
#endif
			{
				//send_net(c->drive_mod.addr,GET_SPEED,c->drive_mod.chan,0);
				//set_exd_status();
				if (!EXD_DEAD)
				{
					act_spd_bits = ANALOG_OUT_HAULOFF;
					ftmmp = (float)act_spd_bits;
		             
					if ((ftmmp > 0.0) && (c->zero_crossing > 0.0))
					{
						ftmmp = (float)(ftmmp - (327 * c->zero_crossing));  /* Take out offset */
						ftmmp = (float)(OVF_INT * (ftmmp / (OVF_INT - (327 * c->zero_crossing))));  /* DIVIDE */
					} 
				}
				else
					ftmmp = 0.0;
	              
				if (ftmmp > OVF_INT)
					ftmmp = OVF_INT;
				else if (ftmmp < 0.0)
					ftmmp = 0.0;

				/*  s->act_spd = ftmmp * HoSpeedFactor / (float)OVF_INT; */
				s->act_spd = ftmmp / (float)OVF_INT;
			}
         
//			if (s->rate_uses_spd)
//				s->act_ltp = c->rs_ave * s->act_spd;
			break;
	}
}

#ifdef ALLEN_BRADLEY_DRIVE
/************************************************************************/
/* set_plc_alive_flag - sends alive flag to plc every periodically     */
/************************************************************************/
void send_plc_alive_flag(void)
{
  static unsigned char toggle_bit; 
   
  if (connectedPLC)
  { 
    if ((!g->stats[32 + c->drive_mod.chan].alive) || (EthError == Connection_timed_out))
    {
       if(!EXD_DEAD)
       {
       SET_ALARM(EXD_TIMEOUT)
 /*      ClosePLCRemoteSocket();*/
       }
       return;
    }
//  else if ((s->alarm_bits & EXD_TIMEOUT) && g->stats[32 + c->drive_mod.chan].alive)
//         /*CLEAR_ALARM(EXD_TIMEOUT)*/
  }
  
  if (EXD_DEAD)
     shrHO.act_spd = 0;       /* lost communication to plc, act_spd is reset to zero */ 
  
  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (plc_alive_count++ > 10)
   {
      plc_alive_count = 0;
	   
      if (!connectedPLC) 
         init_ethernet();

      if (connectedPLC)
      {
         PLC_WRITTEN_INT_DATA[0] = 0x0001;
         SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 7, 1, 1, INT);
         RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
         toggle_bit = 1;
         /*ClosePLCRemoteSocket();*/
      }
   }
   else if (toggle_bit)
   {
      toggle_bit = 0;
      if (!connectedPLC) 
         init_ethernet();

      if (connectedPLC) 
      {
         PLC_WRITTEN_INT_DATA[0] = 0x0000;
         SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 7, 1, 1, INT);
         RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
         /*ClosePLCRemoteSocket();*/
      }
   }
 
   
}
/************************************************************************/
/* init_ethernet) -                                                     */
/************************************************************************/
void init_ethernet(void)
{
   unsigned long HostAddr;

   brsmemset((UDINT)&HostAddr, 0, (UDINT)sizeof(HostAddr)); 
   brsmemcpy((UDINT)&HostAddr, (UDINT)cfgHO.drive_ip_address, (UDINT)sizeof(cfgHO.drive_ip_address));
			  
   OpenRemoteSocketPLC(0xaf12, HostAddr, 1, c->drive_mod.chan);

   if (connectedPLC) 
   {
      SendRegSessionCommand(c->drive_mod.chan);
      RecvRegSessionResponse(c->drive_mod.chan);
      SendRRData(c->drive_mod.chan);
      RecvSendRRDataResponse(c->drive_mod.chan);
      //if (connection_counter > 0)
      //   connection_counter--;
      //if (EXD_DEAD)
      //   CLEAR_ALARM(EXD_TIMEOUT);
      ConnectedPLC_Flag = connectedPLC;
   }
   else 
   {
      //connection_counter++; 
      //if (connection_counter> 0) 
      if (EthError == Connection_timed_out)
      {
        if (!EXD_DEAD)
        {
           SET_ALARM(EXD_TIMEOUT)
           /*ClosePLCRemoteSocket();*/
        }
        return;
     }	   
   }
}
#endif

/************************************************************************/
/* calc_demo_values() - calculates return values for demo/simulation    */
/*                      mode and returns them in packet                 */
/************************************************************************/
void calc_demo_values(void)
{
	config_HO_device *c = &cfgHO;
	shared_HO_device *s = &shrHO;

	/* for haul off exd speed setting */
	static unsigned int actual_spd = 0;	/* tracks actual speed in bits      */
	static unsigned int p_bits = 0;		/* current pulses in bits           */
	//unsigned int delta_t_bits;     		/* stores change in time in bits    */
	unsigned int delta_p_bits;     		/* stores change in pulses in bits  */

	float delta_t;                 /* elapsed time in seconds              */
	static float error_len = 0.0f; /* error length for cumulative errors   */
	float Ltp, Len, Pulses;
	int ZeroCrossBits;
	/*----------------------------------------------------------------------*/
   
	if (exd_timebase != 0 && c->accel > 0.0f)
		ovf = ((float) exd_timebase) / (c->accel * 327.67f);

	/* without this section, split simulation will not work since     */
	/* actual_spd is not set unless drive is emulated                 */
	if (cfgHO.spd_factor[shrHO.GearNum] > 0.0f)
		actual_spd = (unsigned int)(s->act_spd * 4095.0f * (100.0f / cfgHO.spd_factor[shrHO.GearNum]) + 0.5f);
	else
		actual_spd = 0;

	if (s->oper_mode == MONITOR_MODE)
		actual_spd = 4095; /* 100 % spd */

	/* get elapsed time (secs) from last pulse reading */
	delta_t = (float) elap_time(interval_timer);
			
	/* calculate theoretical pulses based on delta_t in min           */
	/* SPD_SEC_MIN = 4095 (for max spd) * 60 (sec/min)                */
	/* note for monitor mode actual speed is always 50%             */
	ZeroCrossBits = (int)((c->zero_crossing * 40.95f) + 0.5f);
         
	if (c->zero_crossing <= 0.0f)
		Ltp = ((int)actual_spd - ZeroCrossBits) * c->demo_max_ltp / 4095.0f;
	else
	{
		if (actual_spd <= (unsigned int) abs(ZeroCrossBits))
			Ltp = 0.0f;
		else
			Ltp = (float)((int)actual_spd - ZeroCrossBits) * c->demo_max_ltp / 4095.0f;
	}

	Len = (Ltp * delta_t) / 60.0f + fHONoise();
	Pulses = (c->lenperbit > 0.000001f) ? (Len / c->lenperbit) : 0.0f;

	/* convert elapsed pulses into bits */
	delta_p_bits = (unsigned int)(Pulses);

	/* determine sub-bit error & accumulate it      */
	error_len += (Pulses - (float) delta_p_bits);
         
	if (error_len > 1.0f)
	{
		++delta_p_bits;
		error_len -= 1.0f;
	}
			
	bgDbgInfo (&g_RingBuffer1, "pulses", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
		"dpb %7u|p %7f|l %7f|ltp %7f|dt %7f|\r\n", delta_p_bits, Pulses, Len, Ltp, delta_t);      

	/* calculate current pulses in bits */
	p_bits += delta_p_bits;
	G_p_bits = p_bits / 1000;
}


void Check_ManualBackup_HO()
{  
  if (IO_dig[0].channel[IntExtDrv_CTRL_HO])
     shrHO.speed_ctrl = EXTERNAL_CTRL;
  else
     shrHO.speed_ctrl = INTERNAL_CTRL;  
     
  if (!IO_dig[0].channel[AutoMan_HO])	      
   {
     shrHO.ho_manual_backup = MANUAL_BACKUP;
     IO_dig[0].channel[HOManual_Out_0] = OFF;
     IO_dig[0].channel[HOManual_Out_1] = OFF;
   }
  else
   {
     shrHO.ho_manual_backup = AUTO_CONTROL; 
     IO_dig[0].channel[HOManual_Out_0] = ON;
     IO_dig[0].channel[HOManual_Out_1] = ON;
   } 
     
   if (IO_dig[0].channel[AutoMan_HO])
      CLEAR_ALARM(EXD_IN_MANUAL)
   else if (!(s->alarm_bits & EXD_IN_MANUAL))
      SET_ALARM(EXD_IN_MANUAL)   
}

/************************************************************************/
/* set_exd_status() - sets the exd status bits in alarm_bits            */
/************************************************************************/
void set_exd_status(void)
{
   static int last_ctrl = INTERNAL_CTRL;

	if ((cfg_super.ltp_mode == CONTROL) && (!cfgHO.demo & DEMO_DRIVE_MOD))
	{
		if (IO_dig[0].channel[AutoMan_HO])
			CLEAR_ALARM(EXD_IN_MANUAL)
		else if (!(s->alarm_bits & EXD_IN_MANUAL))
			SET_ALARM(EXD_IN_MANUAL)
	}
    
	if (cfgHO.demo & DEMO_DRIVE_MOD)
		CLEAR_ALARM(EXD_IN_MANUAL)
	
   #ifdef ALLEN_BRADLEY_DRIVE
   if (c->drive_type==AB_PLC_DRIVE)
   {
      if (!connectedPLC) 
         init_ethernet();
     
    if ((!g->stats[32 + c->drive_mod.chan].alive) || (EthError == Connection_timed_out))
    {
      if (!EXD_DEAD)
      {
         SET_ALARM(EXD_TIMEOUT)
         /*ClosePLCRemoteSocket();*/
      }
      return;
    }
    else
    {
      if ((s->alarm_bits & EXD_TIMEOUT) && g->stats[32 + c->drive_mod.chan].alive)
      {
         /*s->reset_drive = TRUE;*/
         /*CLEAR_ALARM(EXD_TIMEOUT)*/
      }
    } 
  }
  else
  #endif
  {
     if (s->speed_ctrl == INTERNAL_CTRL)
     {	
        if (last_ctrl == EXTERNAL_CTRL)
        {
          reset_rate_speed = TRUE;
          if (!cs_->line_spd_is_master)
             g->liw_spd[(int)HO_DRIVE] = s->act_spd * shr_global.man_ratio / (float)100.0;
        }
     }
     last_ctrl = s->speed_ctrl;
  }
}

REAL fHONoise(void)
{
	REAL fReturn;
	REAL fNoiseBase = 0.441;
	
	fReturn = rand() % 10000; /* 0 -> 10000 */
	fReturn = fReturn / 10000.0f; /* 0.00% -> 100.00% */	
	fReturn = fNoiseBase * (1.0f - 2.0f * fReturn); /* -0.441 -> +0.441 */	
	return fReturn;
}
