#include "math.h"
#include "pcc_math.h"

int linear_least_squared(samples *sample_data,float *m,float *b)
{
    int i;
    ordered_pair *d;
    float xi;
    float xi2;
    float yi;
    float xiyi;
    float determinant;
    int length;
    int retval = 0;

    xi = xi2 = yi = xiyi = 0.0;
    d = sample_data->pts;

    length = sample_data->num_taken;
    if(length > sample_data->length)
        length = sample_data->length;

    for(i=0; i<length; ++i)
    {
        /* calculate sum( x(i) ) */
        xi += d->x;

        /* calculate sum( x(i)^2 ) */
        xi2 += (d->x) * (d->x);

        /* calculate sum( y(i) ) */
        yi += d->y;

        /* calculate sum( x(i)*y(i) ) */
        xiyi += (d->y) * (d->x);

        ++d;
    }
    determinant =  length * xi2 - xi * xi;

    if(fabs((double)determinant) > .00001)
    {
        *m = (length * xiyi - xi * yi) / determinant;
        *b = (xi2 * yi - xiyi * xi) / determinant;
    }
    else
    {
        /* system of equations is ill-conditioned */
        *m = 0;
        *b = 0;
        retval = 1;
    }

    return retval;
}


