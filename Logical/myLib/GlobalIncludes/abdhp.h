/************************************************************************/
/* FILE:        abdhp.h

      $Log:   F:\Software\BR_Guardian\abdhp.h_v  $
 *
 *    Rev 1.0   Apr 29 2008 14:33:10   YZS
 * Initial revision.
 *
 *

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/

#define NO_ERROR              0
#define LOW                   1
#define HIGH                  2

#define DATA                  0
/* #define CONTROL              1  already in sys.h */

/* codes to indicate type of data in msg_pipe */
#define REPLY_MSG_CODE        1
#define SEND_ACK_CODE         (unsigned char)6
#define SEND_NAK_CODE         (unsigned char)0x15
#define NO_RESPONSE           (unsigned char)0xFF

/* defines to work with message queue */
#define QUEUE_BFR_SIZE        1024
#define MAX_QUEUE_MSGS        3
#define BUFFER_FULL           1
#define TOO_MANY_MSGS         2
#define FROM_PIPE             1
#define RETRY                 0

/* for error checking message being read in rx task */
#define MAX_MSG_LENGTH        244
#define INVALID_POINT         1
#define READ_ONLY             2
#define BAD_BCC               4
#define MSG_SINK_FULL         8
#define MSG_TOO_SMALL         0x10
#define MSG_PIPE_ERROR        0x20
#define TOO_MANY_CHRS         0x40
#define BAD_ETX_SEQUENCE      0x80
#define CHAR_TIMEOUT          0x0A

/* for retry/timeout control of outgoing msg */
#define ACK_TIMEOUT           100      /* 1 sec (1 tick = 10ms) */
#define RESPONSE_TIMEOUT      2 * TICKSPERSEC   /*  (1 sec: 1 tick = 10ms) */
#define ENQ_LIMIT             0
#define NAK_LIMIT             1
#define ENQ_LIMIT_EXCEEDED    1
#define NAK_LIMIT_EXCEEDED    2
#define INVALID_ACK_CHRS      4

/* control byte definitions */
#define        STX            (unsigned char)0x02
#define        ETX            (unsigned char)0x03
#define        ENQ            (unsigned char)0x05
#define        ACK            (unsigned char)0x06
#define        DLE            (unsigned char)0x10
#define        NAK            (unsigned char)0x15

/* command bytes for protocol */
#define PROTECTED_WRITE       0x00
#define UNPROTECTED_READ      0x01
#define PROTECTED_BIT_WRITE   0x02
#define UNPROTECTED_BIT_WRITE 0x05
#define DIAGNOSTIC_STATUS     0x06
#define UNPROTECTED_WRITE     0x08
#define EXTENDED_COMMAND      0x0F
#define PLC5_TYPED_WRITE      0x67
#define PLC5_TYPED_READ       0x68
#define WORD_RANGE_READ       0x01
#define WORD_RANGE_WRITE      0x00
#define READ                  1
/* #define WRITE                 2   defined in remote.h */
#define RELPY_BIT             0x40

/* command and reply message offsets */
#define DST                   0     /* destination */
#define SRC                   1     /* source      */
#define CMD                   2     /* command     */
#define STS                   3     /* status      */
#define TNS_LO                4     /* low byte of transaction number */
#define TNS_HI                5     /* high byte of transaction number */
#define REPLY_DATA            6     /* 1st byte of reply data */
#define FNC                   6     /* function byte for extended commands */
#define ADDR_LO               6     /* low byte of command address      */
#define ADDR_HI               7     /* high byte of command address      */
#define OFFSET_LO             7     /* low byte of packet offset(ext. cmds */
#define OFFSET_HI             8     /* high byte of packet offset(ext. cmds */
#define PLC2_SIZE             8     /* # of bytes requested PLC-2 read   */
#define PLC2_DATA             8     /* first byte of data for PLC-2 write  */
#define TOTAL_LO              9     /* low byte of total trans(Typed cmds */
#define TOTAL_HI              10    /* high byte of total trans(Typed cmds */
#define PLC5_ADDR             11    /* 1st byte of address for extended cmd */

/* plc5 typed read/write type definitions */
#define BIT_TYPE              1
#define INTEGER_TYPE          4
#define FLOAT_TYPE            8
#define ARRAY_TYPE            9

/* PLC-5 logical binary address offsets & definitions */
#define ADDR_FLAG             0        /* flag byte offset in address */
#define LEVEL_1               1        /* level 1 bit mask */
#define LEVEL_2               2        /* level 2 bit mask */
#define LEVEL_3               4        /* level 3 bit mask */
#define LEVEL_4               8        /* level 4 bit mask */
#define DELIMITER (unsigned char)0xFF  /* delimeter for 2 byte values */

#define INT             0       /* type definitions for rem_vars  */
#define FLOAT           1


/* return status codes in STS byte */
#define ILLEGAL_CMD_OR_FMT    0x10
#define ADDRESSING_PBLM       0x50
#define FUNCTION_NOT_ALLOWED  0x60

/* mode button and alarm acknowledge masks/registers */
#define PUT_INTO_AUTO         0x00000008
#define PUT_INTO_MANUAL       0x00000004
#define PUT_INTO_PAUSE        0x00000001
#define MODE_BUTTON           (SYSTEM_OFFSET+R_MODE_BUTTON)
#define LATCHED_ALARMS        (SYSTEM_OFFSET+R_ALARM_LATCH_WORD)

/* structure containing circular buffer and pointers */
typedef struct {
  unsigned char buffer[QUEUE_BFR_SIZE];/* pool of bytes for outgoing msgs*/
  int start;                    /* indicates start byte in circ buffer  */
  int end;                      /* indicates last byte loc in cir buffer*/
  int count;                    /* tracks number of msgs in circ buffer */
  int length[MAX_QUEUE_MSGS];   /* tracks len of messages in buffer     */
} MSG_QUEUE;



