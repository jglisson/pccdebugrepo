
#define Def_StateROnlyInit          0  /* defined state for Read initialization */
#define Def_StateROnlyOpen          1  /* defined state for Read open a file */
#define Def_StateROnlyBlock         2  /* defined state for Read a file */
#define Def_StateROnlyParseBlock    3  /* defined state for Read Parse the record/block */
/* #define Def_StateWriteBlock        4  /* defined state for Read writing the block */
#define Def_StateROnlyClose         5  /* defined state for Read closing the file */
#define Def_StateROnlyErr           6  /* defined state for Read error */


#define FileMaxBlocks  2         /* Length of file block processed at one time */
#define FileBlockSize  2048      /* Length of file block processed at one time */
#define FileKeySize  30          /* max length of keys used in configuration files on cf card */


