#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/**************************************************************************
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.runExt to toggle debugging information in this file
 **************************************************************************/

#include <bur/plctypes.h>
#include <math.h>
#include "mem.h"
#include "sys.h"
#include "vars.h"
#include "libbgdbg.h"
#include "recipe.h"
#include  "hmi.h"

extern int validate_recipe(recipe_struct *r,int *validation_errors);
extern int calc_recipe(recipe_struct *r);
extern void copy_recipe(recipe_struct *,recipe_struct *,int);
extern void clear_recipe(recipe_struct *r, int what);
extern void change_recipe(recipe_struct *);

void run_ext();
void clear_variables(void);
void check_recipe(char in_remote);
void check_operation(char in_remote);
void check_recipe(char in_remote);
void calc_extruder_speeds(void);
char calc_ho_speeds(void);
void search_ext_spd(float wtp, int loop);
float calc_etas(void);
float calc_extruder_accel(float max_eta,int k);
float calc_haul_off_accel(float max_eta);
extern unsigned char recipe_confirmed; 
extern void Set_Recipe(recipe_struct *);
extern void visible(unsigned char *Runtime, unsigned char state);

float gHO_ad;
float gHO_ns;
float gHO_eta;
float gLIW_ad[MAX_LOOP];               /* accel / decel factors        */
float gLIW_eta[MAX_LOOP];              /* eta's using max accel/decel  */
float gLIW_ns[MAX_LOOP];               /* new speeds from rs_avg       */

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;
_LOCAL char CannotRunRecipeBig_Ack;
_LOCAL char CannotRunRecipeSmall_Ack;
_LOCAL char CannotRunRecipeSmall_Change;
_LOCAL char CannotRunRecipeBig_Change;
extern unsigned char NoRateSpd_Ack;

_LOCAL int err_ext;

void run_ext()
{
   config_super *cs = &cfg_super;
   char in_remote;
   char ho_decel = TRUE;                 /* flag for ho is decelerating  */
   float max_eta;                        /* eta for system ramp          */

   in_remote = FALSE;   /* need to make correction for this when get remote working */
   
   change_recipe(BATCH_RECIPE_NEW);                /* do recipe calculation*/
   clear_variables();
   check_recipe(in_remote);
         
   if(cs->sltp_mode == MONITOR)
   {
      SHOCommand.CommandPresent = TRUE;
      SHOCommand.Command = MONITOR_MODE;
      SHOCommand.SetLTP = 0.0;
      SHOCommand.SetAccelDecel = 0.0;
   }
   
   /*   check_operation(in_remote);*/
   if(cs->ltp_mode == CONTROL)
      ho_decel = calc_ho_speeds();

   if(cs->wtp_mode == CONTROL)
      calc_extruder_speeds();

   /* copy recipe from temp to currently running                         */
   if ((cs->ltp_mode == CONTROL) || (cs->wtp_mode == CONTROL))
   {
       if (recipe_confirmed)		       /* confirmed to run recipe anyway */
       {
          copy_recipe(BATCH_RECIPE_NOW,BATCH_RECIPE_NEW,EXTRUDER_RECIPE);
	       copy_recipe(&UI_RECIPE,&BATCH_RECIPE_EDIT,EXTRUDER_RECIPE);
          Set_Recipe(BATCH_RECIPE_NEW);	        /* display set point at extrusion control recipe */
          recipe_confirmed = 0;
       }
       else if (!err_ext)
       {
          copy_recipe(BATCH_RECIPE_NOW,BATCH_RECIPE_NEW,EXTRUDER_RECIPE);
	       copy_recipe(&UI_RECIPE,&BATCH_RECIPE_EDIT,EXTRUDER_RECIPE);
          Set_Recipe(BATCH_RECIPE_NEW);
       }
   }
   else
   {
      clear_recipe(BATCH_RECIPE_NOW,EXTRUDER_RECIPE);
      copy_recipe(BATCH_RECIPE_NOW,BATCH_RECIPE_NEW,DENSITY_ONLY);
      if (cs->density_in_recipe)
      {
         BATCH_RECIPE_NOW->density.t = BATCH_RECIPE_NEW->density.t;
         BATCH_RECIPE_NOW->width = BATCH_RECIPE_NEW->width;
         BATCH_RECIPE_NOW->od = BATCH_RECIPE_NEW->od;
         BATCH_RECIPE_NOW->id = BATCH_RECIPE_NEW->id;
      }
   }

   if (cs->ltp_mode == UNUSED)
   {
      shrHO.set_ltp = BATCH_RECIPE_NOW->ltp;        /* fill in for udata    */
      shrHO.act_ltp = BATCH_RECIPE_NOW->ltp;
   }
   if (cs->wth_mode == UNUSED)
   {
      shrWTH.set_wth = BATCH_RECIPE_NOW->width;        /* fill in for udata    */
      shrWTH.act_wth = BATCH_RECIPE_NOW->width;
   }

   max_eta = calc_etas();

   if (cs->wtp_mode == CONTROL)
   {
      gLIW_ad[EXT_LOOP] = calc_extruder_accel(max_eta,EXT_LOOP);
      if (cs->scrap && (cs->refeed == GRAVIFLUFF))
         gLIW_ad[GRAVIFLUFF_FDR_LOOP] = calc_extruder_accel(max_eta,GRAVIFLUFF_FDR_LOOP);
   }

   if (cs->ltp_mode == CONTROL)
   {
      gHO_ad = calc_haul_off_accel(max_eta);
   }

   /* send out auto messages to LIW's & HO                               */

   if (ho_decel)         /* see if should do ho first                    */
   {
      if (cs->ltp_mode == CONTROL)
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = AUTO_MODE;
         HOCommand.SetLTP = BATCH_RECIPE_NOW->ltp;
         HOCommand.SetAccelDecel = gHO_ad;
      }
      else
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MONITOR_MODE;
         HOCommand.SetLTP = 0.0;
         HOCommand.SetAccelDecel =0.0;
      }
   }

   if (cs->wtp_mode == CONTROL)
   {
      if (BATCH_RECIPE_NOW->ext_wtp > 0.0)
      {
         LiwCommand[0].CommandPresent = TRUE;
         LiwCommand[0].Command = AUTO_MODE;
         LiwCommand[0].SetWTP = BATCH_RECIPE_NOW->ext_wtp;
         LiwCommand[0].SetAccelDecel = gLIW_ad[EXT_LOOP];
      }
      else
      {
         LiwCommand[0].CommandPresent = TRUE;
         LiwCommand[0].Command = PAUSE_MODE;
         LiwCommand[0].SetWTP = 0.0;
         LiwCommand[0].SetAccelDecel = cfgEXT.accel;
      }
   }
   else
   {
      LiwCommand[0].CommandPresent = TRUE;
      LiwCommand[0].Command = MONITOR_MODE;
      LiwCommand[0].SetWTP = 0.0;
      LiwCommand[0].SetAccelDecel = 0.0;
   }

   if (cs->scrap && (cs->refeed == GRAVIFLUFF))
   {
      if (BATCH_RECIPE_NOW->ext_wtp > 0.0)
      {
          LiwCommand[1].CommandPresent = TRUE;
          LiwCommand[1].Command = AUTO_MODE;
          LiwCommand[1].SetWTP = BATCH_RECIPE_NOW->refeed_wtp;
          LiwCommand[1].SetAccelDecel = gLIW_ad[GRAVIFLUFF_FDR_LOOP];
      }
      else
      {
         LiwCommand[1].CommandPresent = TRUE;
         LiwCommand[1].Command = PAUSE_MODE;
         LiwCommand[1].SetWTP = 0.0;
         LiwCommand[1].SetAccelDecel = 0.0;
      }
   }

   if (!ho_decel)        /* if accel ho, do last                         */
   {
      if (cs->ltp_mode == CONTROL)
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = AUTO_MODE;
         HOCommand.SetLTP = BATCH_RECIPE_NOW->ltp;
         HOCommand.SetAccelDecel = gHO_ad;
      }
      else
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MONITOR_MODE;
         HOCommand.SetLTP = 0.0;
         HOCommand.SetAccelDecel = 0.0;
      }
   }

   if (cs->wth_mode != UNUSED)
   {
      WTHCommand.CommandPresent = TRUE;
      WTHCommand.Command = MAN_MODE;
      WTHCommand.Wth = BATCH_RECIPE_NOW->width;
   }
  
   if ((cfg_super.wtp_mode == CONTROL) || (cfg_super.ltp_mode == CONTROL))
   	EXT_HO_OPER_MODE = AUTO_MODE;

 }
/****************************************************************************/
/****************************************************************************/
float calc_extruder_accel(float max_eta,int k)
{
    register float f;
    register float accel;

    if ((f=max_eta*cfgEXT.spd_factor) > 0.001)
        accel = (float)fabs((gLIW_ns[k]-shrEXT.set_spd)/f);     /* DIVIDE */
    else
        accel = 0.0;

    return(accel);
}

/****************************************************************************/
/****************************************************************************/
float calc_haul_off_accel(float max_eta)
{
    register float accel;
    register float f;
    register float HoSpeedFactor;

    /* HoSpeedFactor = cfgHO.spd_factor[shrHO.GearNum]; */
	HoSpeedFactor = 1; /* disable speed factor now */	
  
    if ((f=max_eta*HoSpeedFactor) > 0.0)
      accel = (float)fabs((gHO_ns-shrHO.set_spd)/f);      /* DIVIDE */
    else
      accel = 0.0;

    return(accel);
}

/****************************************************************************/
/****************************************************************************/
float calc_etas(void)
{
   config_super *cs = &cfg_super;
   float max_eta = 0.0;

   /* figure out eta's for controlling stuff  */
   if (cs->ltp_mode == CONTROL)
      max_eta = gHO_eta;

   if (cs->wtp_mode == CONTROL)
   {
      if(gLIW_eta[EXT_LOOP] > max_eta)
         max_eta = gLIW_eta[EXT_LOOP];

      if (cs->scrap && (cs->refeed == GRAVIFLUFF))
      {
         if(gLIW_eta[GRAVIFLUFF_FDR_LOOP] > max_eta)
            max_eta = gLIW_eta[GRAVIFLUFF_FDR_LOOP];
      }
   }
   return(max_eta);
}
/****************************************************************************/
/****************************************************************************/
char calc_ho_speeds(void)
{
    config_super *cs = &cfg_super;
    register float f;
    char ho_decel;
    register float HoSpeedFactor;

    HoSpeedFactor = cfgHO.spd_factor[shrHO.GearNum];

    /* do accel/decel stuff   */
    if (!cfgHO.drive_has_control)
    {
        if ((f=cfgHO.rs_ave) > 0.001)
            gHO_ns = BATCH_RECIPE_NEW->ltp / f;            /* DIVIDE */
        else
            gHO_ns = HO_NRS_SPD;       /* default speed if no rate speed       */
    }
    else
        gHO_ns = BATCH_RECIPE_NEW->ltp;

    if (shrHO.set_spd > gHO_ns)              /* decel        */
    {
        if (cs->decel < cfgHO.decel)
            gHO_ad = cs->decel;
        else
            gHO_ad = cfgHO.decel;
        ho_decel = TRUE;
    }
    else                                      /* accel        */
    {
        if (cs->accel < cfgHO.accel)
            gHO_ad = cs->accel;
        else
            gHO_ad = cfgHO.accel;
        ho_decel = FALSE;
    }

    if ((f=gHO_ad) > 0.001)
        gHO_eta = (float)fabs((gHO_ns-shrHO.set_spd)/f);       /* DIVIDE */
    else
        gHO_eta = 0.0;

    return(ho_decel);
}

/****************************************************************************/
/****************************************************************************/
void calc_extruder_speeds(void)
{
   config_super *cs = &cfg_super;

   /* search for fastest allowable extruder accel/decel factors */
   search_ext_spd(BATCH_RECIPE_NEW->ext_wtp, EXT_LOOP);

   if (cs->scrap && (cs->refeed == GRAVIFLUFF))
      search_ext_spd(BATCH_RECIPE_NEW->refeed_wtp, GRAVIFLUFF_FDR_LOOP);

}
/****************************************************************************/
/****************************************************************************/
void search_ext_spd(float wtp, int loop)
{
    config_super *cs = &cfg_super;
    register float f;
    register int i = loop;

    if (wtp > 0.0001)
    {
        if ((f=cfgEXT.rs_ave) > 0.0001)
        {
             gLIW_ns[i] = wtp / f;           /* DIVIDE */
        }
        else
            gLIW_ns[i] = LIW_NRS_SPD;      /* default if no rt spd     */

        if (shrEXT.set_spd > gLIW_ns[i])        /* decel        */
        {
            if (cs->decel < cfgEXT.decel)
                gLIW_ad[i] = cs->decel;
            else
                gLIW_ad[i] = cfgEXT.decel;
        }
        else                                        /* accel        */
        {
            if (cs->accel < cfgEXT.accel)
                gLIW_ad[i] = cs->accel;
            else
                gLIW_ad[i] = cfgEXT.accel;
        }

        if ((f=gLIW_ad[i]*cfgEXT.spd_factor) > 0.001)
            gLIW_eta[i] = (float)fabs((gLIW_ns[i]-shrEXT.set_spd)/f);        /* DIVIDE */
        else
            gLIW_eta[i] = 0.0;
    }
    else
    {
        gLIW_ns[i] = 0.0;
        gLIW_ad[i] = 0.0;
        gLIW_eta[i] = 0.0;
    }
}

/****************************************************************************/
/****************************************************************************/
void check_recipe(char in_remote)
{
	int i;

	/* clear errors */
	for (i=0; i<=MAX_LOOP; i++)
		shr_global.recipe_errors[i] = 0;
     
	if ((err_ext = validate_recipe(BATCH_RECIPE_NEW, shr_global.recipe_errors)) != 0)
	{ 
#if 0
		global_struct *g = &shr_global;

		if (err & (RECIPE_BAD_THROUGHPUT|RECIPE_BAD_PROD))
		{
			brsstrcpy(bfr, txtERROR_BAD_PRODUCTION_RECIPE);
			exit_error(bfr, in_remote);
		}
#endif		
		if (shr_global.recipe_errors[HO_LOOP])
		{
#if 0			
			if (shr_global.recipe_errors[HO_LOOP] & RECIPE_NO_RATESPEED)
			{
				sprintf(bfr, txtERROR_s_HAS_NO_RATE_SPEED,ho_text(10));
				exit_error(bfr, in_remote);
			}
			
			if (shr_global.recipe_errors[HO_LOOP] & RECIPE_NO_CAL)
			{
				sprintf(bfr, txtERROR_s_NOT_CALIBRATED,ho_text(10));
				exit_error(bfr, in_remote);
			}
#endif			
			if (shr_global.recipe_errors[HO_LOOP] & RECIPE_OVERSPEED_AL)
			{
				if (!recipe_confirmed)
					visible(&HMI.pg000_CannotRunRecipe_Big, TRUE);
			}
			
			if (shr_global.recipe_errors[HO_LOOP] & RECIPE_UNDERSPEED_AL)
			{
				if (!recipe_confirmed)
					visible(&HMI.pg000_CannotRunRecipe_Small, TRUE);
			}
		}
		
		if (shr_global.recipe_errors[EXT_LOOP])
		{
#if 0
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_NO_RATESPEED)
			{
				visible(&HMI.pg000_PopupNoRateSpd, TRUE);
			}
			
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_NO_CAL)
			{
				sprintf(bfr, txtERROR_s_NOT_CALIBRATED,txtEXTRUDER);
				exit_error(bfr, in_remote);
			}
#endif 
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_OVERSPEED_AL)
			{
				if (!recipe_confirmed)
					visible(&HMI.pg000_CannotRunRecipe_Big, TRUE);
			}
			
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_UNDERSPEED_AL)
			{
				if (!recipe_confirmed)
					visible(&HMI.pg000_CannotRunRecipe_Small, TRUE);
			}
		}
#if 0
		if (shr_global.recipe_errors[GRAVIFLUFF_FDR_LOOP])
		{
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_NO_RATESPEED)
			{
				sprintf(bfr, txtERROR_s_HAS_NO_RATE_SPEED, txtGRAVIFLUFF_FEEDER);
				exit_error(bfr, in_remote);
			}
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_NO_CAL)
			{
				sprintf(bfr,txtERROR_s_NOT_CALIBRATED,txtGRAVIFLUFF_FEEDER);
				exit_error(bfr, in_remote);
			}
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_OVERSPEED_AL)
			{
				sprintf(bfr,txtERROR_s_WILL_RUN_TOO_FAST,txtGRAVIFLUFF_FEEDER);
				exit_error(bfr, in_remote);
			}
			if (shr_global.recipe_errors[EXT_LOOP] & RECIPE_UNDERSPEED_AL)
			{
				sprintf(bfr,txtERROR_s_WILL_RUN_TOO_SLOW,txtGRAVIFLUFF_FEEDER);
				exit_error(bfr, in_remote);
			}
		}
		brsstrcpy(bfr, txtERROR_BAD_PRODUCTION_RECIPE);
		exit_error(bfr, in_remote);
#endif
	}
}

#if 0
/****************************************************************************/
/****************************************************************************/
void check_operation(char in_remote)
{
   global_struct *g = &shr_global;
   config_super *cs = &cfg_super;

   bfr[0] = '\0';

   if (cs->wtp_mode == CONTROL)
   {
     if (shr_LOOP[EXT_LOOP].LIW->alarm_bits & EXD_IN_MANUAL)
        sprintf(bfr,txtERROR_s_DRIVE_IN_MANUAL_BACKUP,txtEXTRUDER);
     if (shr_LOOP[EXT_LOOP].LIW->alarm_bits & EXD_INHIBIT)
        sprintf(bfr,txtERROR_s_DRIVE_INHIBITED,txtEXTRUDER);
     if (cs->scrap && (cs->refeed == GRAVIFLUFF))
     {
       if (shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->alarm_bits & EXD_IN_MANUAL)
          sprintf(bfr,txtERROR_s_DRIVE_IN_MANUAL_BACKUP,txtGRAVIFLUFF_FEEDER);
       if (shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->alarm_bits & EXD_INHIBIT)
          sprintf(bfr,txtERROR_s_DRIVE_INHIBITED,txtGRAVIFLUFF_FEEDER);
     }
   }
   if (cs->ltp_mode == CONTROL)
   {
     if (shr_LOOP[HO_LOOP].HO->alarm_bits & EXD_IN_MANUAL)
        sprintf(bfr,txtERROR_s_DRIVE_IN_MANUAL_BACKUP,ho_text(99));
     if (shr_LOOP[HO_LOOP].HO->alarm_bits & EXD_INHIBIT)
        sprintf(bfr,txtERROR_s_DRIVE_INHIBITED,ho_text(99));
   }
   /* need stretch factor */
   if(cs->wtp_mode == CONTROL || cs->ltp_mode == CONTROL)
   {
     if (cs->sltp_mode==MONITOR)
     {
        if(shr_LOOP[SHO_LOOP].HO->act_ltp <= .001)
            sprintf(bfr,txtERROR_STRETCH_FACTOR_NOT_MEASURED);
     }
   }
  if (bfr[0] != '\0')
     exit_error(bfr, in_remote);
}
/****************************************************************************/
/****************************************************************************/
void exit_error(char *error,int r)
{
  register config_super *cs = cs_;
  register global_struct *g = g_;

  if (r)
  {
    shr_global.pro_died++;
    unlink_to_data();
  }
  else
  {
    display(0,0,error);
    sleep((unsigned int)cs->errortime);
    deinit_proc();
  }
  exit(ERROR_CODE);
}
#endif

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
void clear_variables(void)
{
   int i;

   /* clear variables    */
   gHO_eta = 0.0;
   gHO_ns = 0.0;
   gHO_ad = 0.0;

   for (i=0; i<MAX_LOOP; ++i)
   {
      gLIW_eta[i] = gLIW_ad[i] = gLIW_ns[i] = 0.0;
   }

}


