#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_ivariance_p(isamples *sample_data, signed short *mean, signed short length)
{
    int avg;
    int err;
    int i;
    int var;
    ordered_ipair *d;
    int idx;

    avg = calc_imean_p(sample_data,length);
    if(mean != NULL)
        *mean = avg;

    if(length > sample_data->length) length = sample_data->length;
    d = sample_data->pts;

    idx = sample_data->next - length;
    if(idx < 0)
        idx += sample_data->length;

    var = 0;
    for(i=0;i<length;++i)
    {
        err = d[idx].y - avg;
        var = var + err * err;

        if(++idx >= sample_data->length)
            idx -= sample_data->length;
    }
    if(length > 1)
        var = (int)((float)var / (float)(length - 1) + .5);     /* DIVIDE */
    else
        var = 0;

    return var;
 }


