/************************************************************************/
/*
   File:  remsub.h

   Description: Remoet routines.


      $Log:   F:\Software\BR_Guardian\remsub.h_v  $
 *
 *    Rev 1.0   Feb 11 2008 11:23:36   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#ifndef REMSUB_H
#define REMSUB_H

#include "recipe.h"

extern int rv_set_wpa(fic * v, point_data * pd, int *type);
extern int rv_shift_area(fic * v, point_data * pd, int *type);
extern int rv_inven_area(fic * v, point_data * pd, int *type);
extern int rv_job_wt_completed(fic * v, point_data * pd, int *type);
extern int rv_temp_job_wt(fic * v, point_data * pd, int *type);
extern int rv_manual_backup_status(fic * v, point_data * pd, int *type);
extern int rv_set_mode(fic * v, point_data * pd, int *type);
extern int rv_current_mode(fic * v, point_data * pd, int *type);
extern int rv_status(fic * v, point_data * pd, int *type);
extern int rv_latched_alarm(fic * v, point_data * pd, int *type);
extern int rv_current_alarm(fic * v, point_data * pd, int *type);
extern int rv_set_mode(fic * v, point_data * pd, int *type);
extern int rv_current_mode(fic * v, point_data * pd, int *type);
extern int rv_demo_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_demo_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_batch_size(fic * v, point_data * pd, int *type);
extern int rv_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_mix_time(fic * v, point_data * pd, int *type);
extern int rv_mix_time_left(fic * v, point_data * pd, int *type);
extern int rv_temp_mix_time(fic * v, point_data * pd, int *type);
extern int rv_job_wt(fic * v, point_data * pd, int *type);
extern int rv_resin_name_1(fic * v, point_data * pd, int *type);
extern int rv_resin_name_2(fic * v, point_data * pd, int *type);
extern int rv_resin_name_3(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_1(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_2(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_3(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_4(fic * v, point_data * pd, int *type);
extern int rv_job_name_1(fic * v, point_data * pd, int *type);
extern int rv_job_name_2(fic * v, point_data * pd, int *type);
extern int rv_job_name_3(fic * v, point_data * pd, int *type);
extern int rv_job_name_4(fic * v, point_data * pd, int *type);
extern int rv_device_name_1(fic * v, point_data * pd, int *type);
extern int rv_device_name_2(fic * v, point_data * pd, int *type);
extern int rv_device_name_3(fic * v, point_data * pd, int *type);
extern int rv_device_name_4(fic * v, point_data * pd, int *type);
extern void clear_all_alarms(point_data *);
extern void clear_all_inven_wt(point_data *);
extern void clear_all_shift_wt(point_data *);
extern int rv_act_wtp(fic *,point_data *, int *);
extern int rv_hopper_weight(fic *,point_data *, int *);
extern int rv_inven_wt(fic *,point_data *, int *);
extern int rv_alarm_latch_word(fic *, point_data *, int *);
extern int rv_alarm_status_word(fic *, point_data *, int *);
extern int rv_alternate_set_addr(fic *, point_data *, int *);
extern int rv_aux_button(fic *, point_data *, int *);
extern int rv_config(fic *, point_data *, int *);
extern int rv_device_control(fic *, point_data *, int *);
extern int rv_mode_button(fic *, point_data *, int *);
extern int rv_mode_button_hi(fic *, point_data *, int *);
extern int rv_next_recipe(fic *, point_data *, int *);
extern int rv_num_hop(fic *, point_data *, int *);
extern int rv_recipe_desc(fic *, point_data *, int *);
extern int rv_recipe_index(fic *, point_data *, int *);
extern int rv_resin_density(fic *, point_data *, int *);
extern int rv_resin_desc(fic *, point_data *, int *);
extern int rv_status_word_lo(fic *, point_data *, int *);
extern int rv_shift_wt(fic *,point_data *, int *);
extern int rv_sys_batch_size(fic *,point_data *, int *);
extern int rv_set_parts(fic *, point_data *, int *);
extern int rv_std_dev(fic *, point_data *, int *);
extern int rv_act_parts(fic *, point_data *, int *);
extern int rv_set_wtp(fic *, point_data *, int *);
extern int rv_temp_set_wtp(fic *, point_data *, int *);
extern int rv_set_ltp(fic *, point_data *, int *);
extern int rv_act_ltp(fic *, point_data *, int *);
extern int rv_set_wpl(fic *, point_data *, int *);
extern int rv_act_wpl(fic *, point_data *, int *);
extern int rv_act_wpa(fic *v,point_data *pd, int *type);
extern int rv_temp_set_wpa(fic *v,point_data *pd, int *type);
extern int rv_set_spd(fic *, point_data *, int *);
extern int rv_act_spd(fic *, point_data *, int *);
extern int rv_inven_len(fic *, point_data *, int *);
extern int rv_shift_len(fic *, point_data *, int *);
extern int rv_last_inven_wt(fic *,point_data *, int *);
extern int rv_last_shift_wt(fic *,point_data *, int *);
extern int rv_last_inven_len(fic *,point_data *, int *);
extern int rv_last_shift_len(fic *,point_data *, int *);
extern int rv_man_spd(fic *, point_data *, int *);
extern int rv_set_thk(fic *, point_data *, int *);
extern int rv_act_thk(fic *, point_data *, int *);
extern int rv_set_od(fic *, point_data *, int *);
extern int rv_act_od(fic *, point_data *, int *);
extern int rv_set_id(fic *, point_data *, int *);
extern int rv_act_id(fic *, point_data *, int *);
extern int rv_set_width(fic *, point_data *, int *);
extern int rv_act_width(fic *, point_data *, int *);
extern int rv_set_density(fic *, point_data *, int *);
extern int rv_act_density(fic *, point_data *, int *);
extern int rv_set_ratio_spd(fic *, point_data *, int *);
extern int rv_act_ratio_spd(fic *, point_data *, int *);
extern int rv_temp_set_parts(fic *, point_data *, int *);
extern int rv_temp_set_ltp(fic *, point_data *, int *);
extern int rv_temp_set_wpl(fic *, point_data *, int *);
extern int rv_temp_man_spd(fic *, point_data *, int *);
extern int rv_temp_set_thk(fic *, point_data *, int *);
extern int rv_temp_set_od(fic *, point_data *, int *);
extern int rv_temp_set_id(fic *, point_data *, int *);
extern int rv_temp_set_width(fic *, point_data *, int *);
extern int rv_temp_set_density(fic *, point_data *, int *);
extern int rv_stretch_factor(fic *v,point_data *pd, int *type);
extern int rv_temp_stretch_factor(fic *v,point_data *pd, int *type);
extern int rv_accel(fic *, point_data *, int *);
extern int rv_decel(fic *, point_data *, int *);
extern int rem_validate_recipe(point_data *,recipe_struct *,int *);
extern int rem_calc_recipe(point_data *, recipe_struct *);
extern int wv_temp_set_wtp(fic *, point_data *);
extern int wv_temp_set_wpl(fic *, point_data *);
extern int wv_temp_set_thk(fic *, point_data *);
extern int wv_temp_set_od(fic *, point_data *);
extern int wv_temp_set_id(fic *, point_data *);
extern int wv_temp_set_width(fic *, point_data *);
extern int wv_temp_set_density(fic *, point_data *);
extern int wv_temp_set_wtp(fic *, point_data *);
extern int wv_temp_set_wpl(fic *, point_data *);
extern int wv_alarm(fic *, point_data *);
extern int wv_status(fic *, point_data *);
extern int wv_temp_set_parts(fic *, point_data *);
extern int wv_temp_mix_time(fic *, point_data *);
extern int wv_batch_size(fic *, point_data *);
extern int wv_demo_max_wtp(fic *, point_data *);
extern int wv_temp_man_start_spd(fic *, point_data *);
extern int wv_temp_set_ltp(fic *, point_data *);
extern int wv_set_mode(fic *, point_data *);
extern int wv_manual_backup_status(fic *, point_data *);
extern int wv_temp_job_wt(fic *, point_data *);
extern int wv_inven_area(fic *, point_data *);
extern int wv_shift_area(fic *, point_data *);
extern int wv_temp_set_wpa(fic *, point_data *);
extern int wv_temp_stretch_factor(fic *, point_data *);

extern int wv_accel(fic *, point_data *);
extern int wv_alarm_status_word(fic *, point_data *);
extern int wv_alternate_set_addr(fic *, point_data *);
extern int wv_alternate_set_value(fic *, point_data *);
extern int wv_decel(fic *, point_data *);
extern int wv_device_control(fic *, point_data *);
extern int wv_inven_len(fic *, point_data *);
extern int wv_inven_wt(fic *, point_data *);
extern int wv_std_dev(fic *v,point_data *);
extern int wv_man_spd(fic *, point_data *);
extern int wv_mix_time(fic *, point_data *);
extern int wv_mode_button(fic *, point_data *);
extern int wv_next_recipe(fic *, point_data *);
extern int wv_set_density(fic *, point_data *);
extern int wv_set_id(fic *, point_data *);
extern int wv_set_od(fic *, point_data *);
extern int wv_set_ratio_spd(fic *, point_data *);
extern int wv_set_spd(fic *, point_data *);
extern int wv_set_thk(fic *, point_data *);
extern int wv_set_width(fic *, point_data *);
extern int wv_set_wtp(fic *, point_data *);
extern int wv_shift_len(fic *, point_data *);
extern int wv_shift_wt(fic *, point_data *);
extern int wv_sys_batch_size(fic *, point_data *);
extern int wv_temp_sys_batch_size(fic *, point_data *);
extern int wv_read_only(fic *,point_data *);
extern int wv_set_ltp(fic *, point_data *);
extern int wv_set_wpl(fic *, point_data *);
extern int null_read_function(fic *,point_data *,int *);
extern int null_write_function(fic *, point_data *);

extern int wv_resin_name_1(fic * v,point_data * pd);
extern int wv_resin_name_2(fic * v,point_data * pd);
extern int wv_resin_name_3(fic * v,point_data * pd);

// for Haartz special ....
extern int wv_resin_name_temp1(fic * v,point_data * pd);
extern int wv_resin_name_temp2(fic * v,point_data * pd);
extern int wv_resin_name_temp3(fic * v,point_data * pd);
extern int wv_resin_name_temp4(fic * v,point_data * pd);
extern int wv_resin_name_temp5(fic * v,point_data * pd);
extern int wv_resin_name_temp6(fic * v,point_data * pd);
extern int wv_resin_name_temp7(fic * v,point_data * pd);
extern int wv_resin_name_temp8(fic * v,point_data * pd);
extern int wv_resin_name_temp9(fic * v,point_data * pd);

extern int rv_resin_name_temp1(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp2(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp3(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp4(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp5(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp6(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp7(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp8(fic * v, point_data * pd, int *type);
extern int rv_resin_name_temp9(fic * v, point_data * pd, int *type);

extern int rv_recipe_name_temp1(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp2(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp3(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp4(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp5(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp6(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp7(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp8(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_temp9(fic * v, point_data * pd, int *type);

extern int wv_recipe_name_temp1(fic * v,point_data * pd);
extern int wv_recipe_name_temp2(fic * v,point_data * pd);
extern int wv_recipe_name_temp3(fic * v,point_data * pd);
extern int wv_recipe_name_temp4(fic * v,point_data * pd);
extern int wv_recipe_name_temp5(fic * v,point_data * pd);
extern int wv_recipe_name_temp6(fic * v,point_data * pd);
extern int wv_recipe_name_temp7(fic * v,point_data * pd);
extern int wv_recipe_name_temp8(fic * v,point_data * pd);
extern int wv_recipe_name_temp9(fic * v,point_data * pd);

extern int rv_device_name_temp1(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp2(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp3(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp4(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp5(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp6(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp7(fic * v, point_data * pd, int *type);
extern int rv_device_name_temp8(fic * v, point_data * pd, int *type);

extern int wv_device_name_temp1(fic * v,point_data * pd);
extern int wv_device_name_temp2(fic * v,point_data * pd);
extern int wv_device_name_temp3(fic * v,point_data * pd);
extern int wv_device_name_temp4(fic * v,point_data * pd);
extern int wv_device_name_temp5(fic * v,point_data * pd);
extern int wv_device_name_temp6(fic * v,point_data * pd);
extern int wv_device_name_temp7(fic * v,point_data * pd);
extern int wv_device_name_temp8(fic * v,point_data * pd);


//extern int calc_recipe(config_super *cs_,config_loop_ptr *cnf_LOOP,global_struct *g_,recipe_struct *r_);
//extern void calc_parts_percent(config_super *cs_,config_loop_ptr *cnf_LOOP,recipe_struct *r_);
extern void store_recipe(config_super *cs_,stored_recipe *rec_,int to,recipe_struct *from);
extern void restore_recipe(config_super *cs_,stored_recipe *rec_,recipe_struct *to,int from);
extern float read_recipe_inven(config_super *cs_,stored_recipe *rec_,int from);
//extern int is_recipe_used(config_super *cs_,stored_recipe *rec_,int rnum);
extern int read_recipe_desc(config_super *cs_,stored_recipe *rec_,int rnum);
//extern int validate_recipe(config_super *cs_,config_loop_ptr *cnf_LOOP,shared_loop_ptr *shr_LOOP,recipe_struct *r,int *validation_errors);
//extern void clear_recipe(config_loop *cnf_LOOP,recipe_struct *r, int what);
extern void copy_recipe(recipe_struct *to, recipe_struct *from, int what);
extern float get_rec_val(recipe_struct *rec_ptr,int mode);
extern void put_rec_val(recipe_struct *rec_ptr,float value,int mode);
extern int rv_avg_batch_size(fic *v,point_data *pd, int *type);
extern int rv_last_gate_feed_wt(fic *v,point_data *pd, int *type);
extern int rv_avg_batch_time(fic *v,point_data *pd, int *type);

int (*fp_read_var[800])(fic *, point_data *, int *)={

/**************** SYSTEM Remote Read Functions ( Offset 0)  ******************************************/
             	null_read_function,                      /* 0 */
                null_read_function,                      /* 1 */
                null_read_function,                      /* 2 */
                rv_set_wtp,                              /* 3 */
                rv_act_wtp,                      /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                rv_set_wpl,                              /* 7 */
                rv_act_wpl,                              /* 8 */
                null_read_function,                      /* 9 */
                null_read_function,                      /* 10 */
                rv_inven_wt,                             /* 11 */
                rv_shift_wt,                             /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                null_read_function,                      /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                rv_set_thk,                              /* 18 */
                rv_act_thk,                              /* 19 */
                rv_set_od,                               /* 20 */
                rv_act_od,                               /* 21 */
                rv_set_id,                               /* 22 */
                rv_act_id,                               /* 23 */
                rv_set_width,                            /* 24 */
                rv_act_width,                            /* 25 */
                rv_set_density,                          /* 26 */
                rv_act_density,                          /* 27 */
                rv_set_ratio_spd,                        /* 28 */
                rv_act_ratio_spd,                        /* 29 */
                null_read_function,                      /* 30 */
                rv_temp_set_wtp,                         /* 31 */
                null_read_function,                      /* 32 */
                rv_temp_set_wpl,                         /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                rv_temp_set_thk,                         /* 36 */
                rv_temp_set_od,                          /* 37 */
                rv_temp_set_id,                          /* 38 */
                rv_temp_set_width,                       /* 39 */
                rv_temp_set_density,                     /* 40 */
                rv_temp_stretch_factor,                  /* 41 */
                rv_stretch_factor,                       /* 42 */
                rv_set_wpa,                              /* 43 */
                rv_act_wpa,                              /* 44 */
                rv_temp_set_wpa,                         /* 45 */
                rv_shift_area,                           /* 46 */
                rv_inven_area,                           /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                rv_accel,                                /* 50 */
                rv_decel,                                /* 51 */
                null_read_function,                      /* 52 */
                rv_last_inven_wt,                        /* 53 */
                null_read_function,                      /* 54 */
                rv_last_shift_wt,                        /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                rv_job_wt,                               /* 60 */
                rv_job_wt_completed,                     /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                rv_temp_job_wt,                          /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,		                 /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                rv_num_hop,                              /* 82 */
                null_read_function,                      /* 83 */
                null_read_function,                      /* 84 */
                null_read_function,                      /* 85 */
                rv_next_recipe,                          /* 86 */
                null_read_function,                      /* 87 */
                rv_manual_backup_status,                 /* 88 */
                null_read_function,                      /* 89 */
                rv_set_mode,                             /* 90 */
                rv_current_mode,                         /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                rv_current_mode,                         /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** GRAVIFLUFF Remote Read Functions ( Offset 1)  ******************************************/
                null_read_function,                      /* 0 */
                null_read_function,                      /* 1 */
                null_read_function,                      /* 2 */
                null_read_function,                      /* 3 */
                null_read_function,                      /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                null_read_function,                      /* 9 */
                null_read_function,                      /* 10 */
                null_read_function,                      /* 11 */
                null_read_function,                      /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                rv_hopper_weight,                        /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                null_read_function,                      /* 26 */
                null_read_function,                      /* 27 */
                null_read_function,                      /* 28 */
                null_read_function,                      /* 29 */
                null_read_function,                      /* 30 */
                null_read_function,                      /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                null_read_function,                      /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                null_read_function,                      /* 50 */
                null_read_function,                      /* 51 */
                null_read_function,                      /* 52 */
                null_read_function,                      /* 53 */
                null_read_function,                      /* 54 */
                null_read_function,                      /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                null_read_function,                      /* 60 */
                null_read_function,                      /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                null_read_function,                      /* 82 */
                null_read_function,                      /* 83 */
                null_read_function,                      /* 84 */
                null_read_function,                      /* 85 */
                null_read_function,                      /* 86 */
                null_read_function,                      /* 87 */
                null_read_function,                      /* 88 */
                null_read_function,                      /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** WIDTH Remote Read Functions ( Offset 2)  ******************************************/
                null_read_function,                      /* 0 */
                null_read_function,                      /* 1 */
                null_read_function,                      /* 2 */
                null_read_function,                      /* 3 */
                null_read_function,                      /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                null_read_function,                      /* 9 */
                null_read_function,                      /* 10 */
                null_read_function,                      /* 11 */
                null_read_function,                      /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                null_read_function,                      /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                rv_set_od,                               /* 20 */
                rv_act_od,                               /* 21 */
                rv_set_id,                               /* 22 */
                rv_act_id,                               /* 23 */
                rv_set_width,                            /* 24 */
                rv_act_width,                            /* 25 */
                null_read_function,                      /* 26 */
                null_read_function,                      /* 27 */
                null_read_function,                      /* 28 */
                null_read_function,                      /* 29 */
                null_read_function,                      /* 30 */
                null_read_function,                      /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                rv_temp_set_od,                          /* 37 */
                rv_temp_set_id,                          /* 38 */
                rv_temp_set_width,                       /* 39 */
                null_read_function,                      /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                null_read_function,                      /* 50 */
                null_read_function,                      /* 51 */
                null_read_function,                      /* 52 */
                null_read_function,                      /* 53 */
                null_read_function,                      /* 54 */
                null_read_function,                      /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                null_read_function,                      /* 60 */
                null_read_function,                      /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                rv_recipe_name_temp1,                    /* 82 */
                rv_recipe_name_temp2,                    /* 83 */
                rv_recipe_name_temp3,                    /* 84 */
                rv_recipe_name_temp4,                    /* 85 */
                rv_recipe_name_temp5,                    /* 86 */
                rv_recipe_name_temp6,                    /* 87 */
                rv_recipe_name_temp7,                    /* 88 */
                rv_recipe_name_temp8,                    /* 89 */
                rv_recipe_name_temp9,                    /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** HO Remote Read Functions ( Offset 3)  ******************************************/
                null_read_function,                      /* 0 */
                null_read_function,                      /* 1 */
                null_read_function,                      /* 2 */
                null_read_function,                      /* 3 */
                null_read_function,                      /* 4 */
                rv_set_ltp,                              /* 5 */
                rv_act_ltp,                              /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                rv_set_spd,                              /* 9 */
                rv_act_spd,                              /* 10 */
                null_read_function,                      /* 11 */
                null_read_function,                      /* 12 */
                rv_inven_len,                            /* 13 */
                rv_shift_len,                            /* 14 */
                null_read_function,                      /* 15 */
                rv_man_spd,                              /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                null_read_function,                      /* 26 */
                null_read_function,                      /* 27 */
                rv_set_ratio_spd,                        /* 28 */
                rv_act_ratio_spd,                        /* 29 */
                null_read_function,                      /* 30 */
                null_read_function,                      /* 31 */
                rv_temp_set_ltp,                         /* 32 */
                null_read_function,                      /* 33 */
                rv_temp_man_spd,                         /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                null_read_function,                      /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                rv_accel,                                /* 50 */
                rv_decel,                                /* 51 */
                rv_last_inven_len,                       /* 52 */
                null_read_function,                      /* 53 */
                rv_last_shift_len,                       /* 54 */
                null_read_function,                      /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                rv_demo_max_wtp,                         /* 60 */
                null_read_function,                      /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                rv_device_name_temp1,                    /* 82 */
                rv_device_name_temp2,                    /* 83 */
                rv_device_name_temp3,                    /* 84 */
                rv_device_name_temp4,                    /* 85 */
                rv_device_name_temp5,                    /* 86 */
                rv_device_name_temp6,                    /* 87 */
                rv_device_name_temp7,                    /* 88 */
                rv_device_name_temp8,                    /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** LIW Remote Read Functions ( Offset 4)  ******************************************/
                null_read_function,                      /* 0 */
                null_read_function,                      /* 1 */
                null_read_function,                      /* 2 */
                rv_set_wtp,                              /* 3 */
                rv_act_wtp,                              /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                rv_set_spd,                              /* 9 */
                rv_act_spd,                              /* 10 */
                rv_inven_wt,                             /* 11 */
                rv_shift_wt,                             /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                rv_hopper_weight,                        /* 15 */
                rv_man_spd,                              /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */ 
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                rv_set_density,                          /* 26 */
                rv_act_density,                          /* 27 */
                rv_set_ratio_spd,                        /* 28 */
                rv_act_ratio_spd,                        /* 29 */
                rv_temp_set_parts,                       /* 30 */
                rv_temp_set_wtp,                         /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                rv_temp_man_spd,                         /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                rv_temp_set_density,                     /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                rv_accel,                                /* 50 */
                rv_decel,                                /* 51 */
                null_read_function,                      /* 52 */
                rv_last_inven_wt,                        /* 53 */
                null_read_function,                      /* 54 */
                rv_last_shift_wt,                        /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                null_read_function,                      /* 60 */
                rv_demo_max_wtp,                         /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                null_read_function,                      /* 82 */
                null_read_function,                      /* 83 */
                null_read_function,                      /* 84 */
                null_read_function,                      /* 85 */
                null_read_function,                      /* 86 */
                null_read_function,                      /* 87 */
                null_read_function,                      /* 88 */
                null_read_function,                      /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** BWH Remote Read Functions ( Offset 5)  ******************************************/
                null_read_function,                      /* 0 */
                rv_max_wtp,                              /* 1 */
                null_read_function,                      /* 2 */
                null_read_function,                      /* 3 */
                rv_act_wtp,                              /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                null_read_function,                      /* 9 */
                null_read_function,                      /* 10 */
                rv_inven_wt,                             /* 11 */
                rv_shift_wt,                             /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                rv_hopper_weight,                        /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                rv_set_density,                          /* 26 */
                rv_act_density,                          /* 27 */
                null_read_function,                      /* 28 */
                null_read_function,                      /* 29 */
                rv_avg_batch_size,                       /* 30 */
                rv_avg_batch_time,                       /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                rv_temp_set_density,                     /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                null_read_function,                      /* 50 */
                null_read_function,                      /* 51 */
                null_read_function,                      /* 52 */
                rv_last_inven_wt,                        /* 53 */
                null_read_function,                      /* 54 */
                rv_last_shift_wt,                        /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                rv_batch_size,                           /* 60 */
                null_read_function,                      /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                null_read_function,                      /* 82 */
                null_read_function,                      /* 83 */
                null_read_function,                      /* 84 */
                null_read_function,                      /* 85 */
                null_read_function,                      /* 86 */
                null_read_function,                      /* 87 */
                null_read_function,                      /* 88 */
                null_read_function,                      /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** MIXER Remote Read Functions ( Offset 6)  ******************************************/
                null_read_function,                      /* 0 */
                rv_max_wtp,                              /* 1 */
                null_read_function,                      /* 2 */
                null_read_function,                      /* 3 */
                null_read_function,                      /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                null_read_function,                      /* 9 */
                null_read_function,                      /* 10 */
                null_read_function,                      /* 11 */
                null_read_function,                      /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                null_read_function,                      /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                null_read_function,                      /* 26 */
                null_read_function,                      /* 27 */
                null_read_function,                      /* 28 */
                null_read_function,                      /* 29 */
                null_read_function,                      /* 30 */
                null_read_function,                      /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                null_read_function,                      /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                null_read_function,                      /* 50 */
                null_read_function,                      /* 51 */
                null_read_function,                      /* 52 */
                null_read_function,                      /* 53 */
                null_read_function,                      /* 54 */
                null_read_function,                      /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                null_read_function,                      /* 60 */
                null_read_function,                      /* 61 */
                rv_mix_time,                             /* 62 */
                rv_mix_time_left,                        /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                rv_temp_mix_time,                        /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                null_read_function,                      /* 82 */
                null_read_function,                      /* 83 */
                null_read_function,                      /* 84 */
                null_read_function,                      /* 85 */
                null_read_function,                      /* 86 */
                null_read_function,                      /* 87 */
                null_read_function,                      /* 88 */
                null_read_function,                      /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                rv_status,                               /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm,                        /* 99 */

/**************** GATE Remote Read Functions ( Offset 7)  ******************************************/
                null_read_function,                      /* 0 */
                rv_set_parts,                            /* 1 */
                rv_act_parts,                            /* 2 */
                rv_set_wt, 			                     /* 3 */
                rv_act_wt,    			                 /* 4 */
                null_read_function,                      /* 5 */
                null_read_function,                      /* 6 */
                null_read_function,                      /* 7 */
                null_read_function,                      /* 8 */
                rv_std_dev,                              /* 9 */
                null_read_function,                      /* 10 */
                rv_inven_wt,                             /* 11 */
                rv_shift_wt,                             /* 12 */
                null_read_function,                      /* 13 */
                null_read_function,                      /* 14 */
                rv_last_gate_feed_wt,                    /* 15 */
                null_read_function,                      /* 16 */
                null_read_function,                      /* 17 */
                null_read_function,                      /* 18 */
                null_read_function,                      /* 19 */
                null_read_function,                      /* 20 */
                null_read_function,                      /* 21 */
                null_read_function,                      /* 22 */
                null_read_function,                      /* 23 */
                null_read_function,                      /* 24 */
                null_read_function,                      /* 25 */
                rv_set_density,                          /* 26 */
                rv_act_density,                          /* 27 */
                null_read_function,                      /* 28 */
                null_read_function,                      /* 29 */
                rv_temp_set_parts,                       /* 30 */
                null_read_function,                      /* 31 */
                null_read_function,                      /* 32 */
                null_read_function,                      /* 33 */
                null_read_function,                      /* 34 */
                null_read_function,                      /* 35 */
                null_read_function,                      /* 36 */
                null_read_function,                      /* 37 */
                null_read_function,                      /* 38 */
                null_read_function,                      /* 39 */
                rv_temp_set_density,                     /* 40 */
                null_read_function,                      /* 41 */
                null_read_function,                      /* 42 */
                null_read_function,                      /* 43 */
                null_read_function,                      /* 44 */
                null_read_function,                      /* 45 */
                null_read_function,                      /* 46 */
                null_read_function,                      /* 47 */
                null_read_function,                      /* 48 */
                null_read_function,                      /* 49 */
                null_read_function,                      /* 50 */
                null_read_function,                      /* 51 */
                null_read_function,                      /* 52 */
                rv_last_inven_wt,                        /* 53 */
                null_read_function,                      /* 54 */
                rv_last_shift_wt,                        /* 55 */
                null_read_function,                      /* 56 */
                null_read_function,                      /* 57 */
                null_read_function,                      /* 58 */
                null_read_function,                      /* 59 */
                null_read_function,                      /* 60 */
                null_read_function,                      /* 61 */
                null_read_function,                      /* 62 */
                null_read_function,                      /* 63 */
                null_read_function,                      /* 64 */
                null_read_function,                      /* 65 */
                null_read_function,                      /* 66 */
                null_read_function,                      /* 67 */
                null_read_function,                      /* 68 */
                null_read_function,                      /* 69 */
                null_read_function,                      /* 70 */
                null_read_function,                      /* 71 */
                null_read_function,                      /* 72 */
                null_read_function,                      /* 73 */
                null_read_function,                      /* 74 */
                null_read_function,                      /* 75 */
                null_read_function,                      /* 76 */
                null_read_function,                      /* 77 */
                null_read_function,                      /* 78 */
                null_read_function,                      /* 79 */
                null_read_function,                      /* 80 */
                null_read_function,                      /* 81 */
                rv_resin_name_temp1,   	                 /* 82 */
                rv_resin_name_temp2,                     /* 83 */
                rv_resin_name_temp3,                     /* 84 */
                rv_resin_name_temp4,                     /* 85 */
                rv_resin_name_temp5,                     /* 86 */
                rv_resin_name_temp6,                     /* 87 */
                rv_resin_name_temp7,                     /* 88 */
                rv_resin_name_temp8,                     /* 89 */
                null_read_function,                      /* 90 */
                null_read_function,                      /* 91 */
                rv_status,                               /* 92 */
                rv_latched_alarm,                        /* 93 */
                rv_current_alarm,                        /* 94 */
                null_read_function,                      /* 95 */
                null_read_function,                      /* 96 */
                null_read_function,                      /* 97 */
                rv_latched_alarm,                        /* 98 */
                rv_current_alarm                         /* 99 */
};


int (*fp_write_var[800])(fic *, point_data *)={

/**************** SYSTEM Remote Read Functions ( Offset 0)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                wv_temp_set_wtp,                         /* 3 */
                wv_read_only,                            /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                wv_temp_set_wpl,                         /* 7 */
                wv_read_only,                            /* 8 */
                null_write_function,                     /* 9 */
                null_write_function,                     /* 10 */
                wv_inven_wt,                             /* 11 */
                wv_shift_wt,                             /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                wv_read_only,                            /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                wv_temp_set_thk,                         /* 18 */
                wv_read_only,                            /* 19 */
                wv_temp_set_od,                          /* 20 */
                wv_read_only,                            /* 21 */
                wv_temp_set_id,                          /* 22 */
                wv_read_only,                            /* 23 */
                wv_temp_set_width,                       /* 24 */
                wv_read_only,                            /* 25 */
                wv_temp_set_density,                     /* 26 */
                wv_read_only,                            /* 27 */
                wv_set_ratio_spd,                        /* 28 */
                wv_read_only,                            /* 29 */
                null_write_function,                     /* 30 */
                wv_temp_set_wtp,                         /* 31 */
                null_write_function,                     /* 32 */
                wv_temp_set_wpl,                         /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                wv_temp_set_thk,                         /* 36 */
                wv_temp_set_od,                          /* 37 */
                wv_temp_set_id,                          /* 38 */
                wv_temp_set_width,                       /* 39 */
                wv_read_only,                            /* 40 */
                wv_temp_stretch_factor,                  /* 41 */
                wv_read_only,                            /* 42 */
                wv_read_only,                            /* 43 */
                wv_read_only,                            /* 44 */
                wv_temp_set_wpa,                         /* 45 */
                wv_shift_area,                           /* 46 */
                wv_inven_area,                           /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                wv_accel,                                /* 50 */
                wv_decel,                                /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                wv_read_only,                            /* 60 */
                wv_read_only,                            /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                wv_temp_job_wt,                          /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                wv_device_name_temp1,                    /* 72 */
                wv_device_name_temp2,                    /* 73 */
                wv_device_name_temp3,                    /* 74 */
                wv_device_name_temp4,                    /* 75 */
                wv_device_name_temp5,                    /* 76 */
                wv_device_name_temp6,                    /* 77 */
                wv_device_name_temp7,                    /* 78 */
                wv_device_name_temp8,                    /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                null_write_function,                     /* 82 */
                null_write_function,                     /* 83 */
                null_write_function,                     /* 84 */
                null_write_function,                     /* 85 */
                wv_next_recipe,                          /* 86 */
                null_write_function,                     /* 87 */
                wv_manual_backup_status,                 /* 88 */
                null_write_function,                     /* 89 */
                wv_set_mode,                             /* 90 */
                null_write_function,                     /* 91 */
                wv_status,                               /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_status,                               /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** GRAVIFLUFF Remote Read Functions ( Offset 1)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                null_write_function,                     /* 3 */
                null_write_function,                     /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                null_write_function,                     /* 9 */
                null_write_function,                     /* 10 */
                null_write_function,                     /* 11 */
                null_write_function,                     /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                wv_read_only,                            /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                null_write_function,                     /* 26 */
                null_write_function,                     /* 27 */
                null_write_function,                     /* 28 */
                null_write_function,                     /* 29 */
                null_write_function,                     /* 30 */
                null_write_function,                     /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                null_write_function,                     /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                null_write_function,                     /* 50 */
                null_write_function,                     /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                null_write_function,                     /* 60 */
                null_write_function,                     /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                null_write_function,                     /* 82 */
                null_write_function,                     /* 83 */
                null_write_function,                     /* 84 */
                null_write_function,                     /* 85 */
                null_write_function,                     /* 86 */
                null_write_function,                     /* 87 */
                null_write_function,                     /* 88 */
                null_write_function,                     /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_read_only,                            /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_read_only,                            /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** WIDTH Remote Read Functions ( Offset 2)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                null_write_function,                     /* 3 */
                null_write_function,                     /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                null_write_function,                     /* 9 */
                null_write_function,                     /* 10 */
                null_write_function,                     /* 11 */
                null_write_function,                     /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                null_write_function,                     /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                wv_temp_set_od,                          /* 20 */
                wv_read_only,                            /* 21 */
                wv_temp_set_id,                          /* 22 */
                wv_read_only,                            /* 23 */
                wv_temp_set_width,                       /* 24 */
                wv_read_only,                            /* 25 */
                null_write_function,                     /* 26 */
                null_write_function,                     /* 27 */
                null_write_function,                     /* 28 */
                null_write_function,                     /* 29 */
                null_write_function,                     /* 30 */
                null_write_function,                     /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                wv_temp_set_od,                          /* 37 */
                wv_temp_set_id,                          /* 38 */
                wv_temp_set_width,                       /* 39 */
                null_write_function,                     /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                null_write_function,                     /* 50 */
                null_write_function,                     /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                null_write_function,                     /* 60 */
                null_write_function,                     /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                wv_recipe_name_temp1,                    /* 82 */
                wv_recipe_name_temp2,                    /* 83 */
                wv_recipe_name_temp3,                    /* 84 */
                wv_recipe_name_temp4,                    /* 85 */
                wv_recipe_name_temp5,                    /* 86 */
                wv_recipe_name_temp6,                    /* 87 */
                wv_recipe_name_temp7,                    /* 88 */
                wv_recipe_name_temp8,                    /* 89 */
                wv_recipe_name_temp9,                    /* 90 */
                null_write_function,                     /* 91 */
                wv_read_only,                            /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_read_only,                            /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** HO Remote Read Functions ( Offset 3)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                null_write_function,                     /* 3 */
                null_write_function,                     /* 4 */
                wv_temp_set_ltp,                         /* 5 */
                wv_read_only,                            /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                wv_set_spd,                              /* 9 */
                wv_read_only,                            /* 10 */
                null_write_function,                     /* 11 */
                null_write_function,                     /* 12 */
                wv_inven_len,                            /* 13 */
                wv_shift_len,                            /* 14 */
                null_write_function,                     /* 15 */
                wv_temp_man_start_spd,                   /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                null_write_function,                     /* 26 */
                null_write_function,                     /* 27 */
                wv_set_ratio_spd,                        /* 28 */
                wv_read_only,                            /* 29 */
                null_write_function,                     /* 30 */
                null_write_function,                     /* 31 */
                wv_temp_set_ltp,                         /* 32 */
                null_write_function,                     /* 33 */
                wv_temp_man_start_spd,                   /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                null_write_function,                     /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                wv_accel,                                /* 50 */
                wv_decel,                                /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                wv_demo_max_wtp,                         /* 60 */
                null_write_function,                     /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                wv_device_name_temp1,  		             /* 82 */
                wv_device_name_temp2,                    /* 83 */
                wv_device_name_temp3,                    /* 84 */
                wv_device_name_temp4,                    /* 85 */
                wv_device_name_temp5,                    /* 86 */
                wv_device_name_temp6,                    /* 87 */
                wv_device_name_temp7,                    /* 88 */
                wv_device_name_temp8,                    /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_read_only,                            /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_read_only,                            /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** LIW Remote Read Functions ( Offset 4)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                wv_read_only,                            /* 3 */
                wv_read_only,                            /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                wv_set_spd,                              /* 9 */
                wv_read_only,                            /* 10 */
                wv_inven_wt,                             /* 11 */
                wv_shift_wt,                             /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                wv_read_only,                            /* 15 */
                wv_temp_man_start_spd,                   /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                wv_temp_set_density,                     /* 26 */
                wv_read_only,                            /* 27 */
                wv_set_ratio_spd,                        /* 28 */
                wv_read_only,                            /* 29 */
                wv_temp_set_parts,                       /* 30 */
                wv_temp_set_wtp,                         /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                wv_temp_man_start_spd,                   /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                wv_read_only,                            /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                wv_accel,                                /* 50 */
                wv_decel,                                /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                null_write_function,                     /* 60 */
                wv_demo_max_wtp,                         /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                null_write_function,                     /* 82 */
                null_write_function,                     /* 83 */
                null_write_function,                     /* 84 */
                null_write_function,                     /* 85 */
                null_write_function,                     /* 86 */
                null_write_function,                     /* 87 */
                null_write_function,                     /* 88 */
                null_write_function,                     /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_read_only,                            /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_read_only,                            /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */


/**************** BWH Remote Read Functions ( Offset 5)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                null_write_function,                     /* 3 */
                wv_read_only,                            /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                null_write_function,                     /* 9 */
                null_write_function,                     /* 10 */
                null_write_function,                     /* 11 */
                null_write_function,                     /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                wv_read_only,                            /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                wv_read_only,                            /* 26 */
                wv_read_only,                            /* 27 */
                null_write_function,                     /* 28 */
                null_write_function,                     /* 29 */
                null_write_function,                     /* 30 */
                null_write_function,                     /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                wv_read_only,                            /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                null_write_function,                     /* 50 */
                null_write_function,                     /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                wv_batch_size,                           /* 60 */
                null_write_function,                     /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                null_write_function,                     /* 82 */
                null_write_function,                     /* 83 */
                null_write_function,                     /* 84 */
                null_write_function,                     /* 85 */
                null_write_function,                     /* 86 */
                null_write_function,                     /* 87 */
                null_write_function,                     /* 88 */
                null_write_function,                     /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_status,                               /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_status,                               /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** MIXER Remote Read Functions ( Offset 6)  ******************************************/
                null_write_function,                     /* 0 */
                null_write_function,                     /* 1 */
                null_write_function,                     /* 2 */
                null_write_function,                     /* 3 */
                null_write_function,                     /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                null_write_function,                     /* 9 */
                null_write_function,                     /* 10 */
                null_write_function,                     /* 11 */
                null_write_function,                     /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                null_write_function,                     /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                null_write_function,                     /* 26 */
                null_write_function,                     /* 27 */
                null_write_function,                     /* 28 */
                null_write_function,                     /* 29 */
                null_write_function,                     /* 30 */
                null_write_function,                     /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                null_write_function,                     /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                null_write_function,                     /* 50 */
                null_write_function,                     /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                null_write_function,                     /* 60 */
                null_write_function,                     /* 61 */
                wv_mix_time,                             /* 62 */ //drt0328 added this for sealed air for mix time via A. B. comm
                //wv_temp_mix_time,                      /* 62 */ // wrong function was for mix time in recipe
                wv_read_only,                            /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                wv_temp_mix_time,                        /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                null_write_function,                     /* 82 */
                null_write_function,                     /* 83 */
                null_write_function,                     /* 84 */
                null_write_function,                     /* 85 */
                null_write_function,                     /* 86 */
                null_write_function,                     /* 87 */
                null_write_function,                     /* 88 */
                null_write_function,                     /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_read_only,                            /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_read_only,                            /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm,                                /* 99 */

/**************** GATE Remote Read Functions ( Offset 7)  ******************************************/
                null_write_function,                     /* 0 */
                wv_read_only,                            /* 1 */
                wv_read_only,                            /* 2 */
                null_write_function,                     /* 3 */
                null_write_function,                     /* 4 */
                null_write_function,                     /* 5 */
                null_write_function,                     /* 6 */
                null_write_function,                     /* 7 */
                null_write_function,                     /* 8 */
                wv_std_dev,                              /* 9 */
                null_write_function,                     /* 10 */
                wv_inven_wt,                             /* 11 */
                wv_shift_wt,                             /* 12 */
                null_write_function,                     /* 13 */
                null_write_function,                     /* 14 */
                null_write_function,                     /* 15 */
                null_write_function,                     /* 16 */
                null_write_function,                     /* 17 */
                null_write_function,                     /* 18 */
                null_write_function,                     /* 19 */
                null_write_function,                     /* 20 */
                null_write_function,                     /* 21 */
                null_write_function,                     /* 22 */
                null_write_function,                     /* 23 */
                null_write_function,                     /* 24 */
                null_write_function,                     /* 25 */
                wv_temp_set_density,                     /* 26 */
                wv_read_only,                            /* 27 */
                null_write_function,                     /* 28 */
                null_write_function,                     /* 29 */
                wv_temp_set_parts,                       /* 30 */
                null_write_function,                     /* 31 */
                null_write_function,                     /* 32 */
                null_write_function,                     /* 33 */
                null_write_function,                     /* 34 */
                null_write_function,                     /* 35 */
                null_write_function,                     /* 36 */
                null_write_function,                     /* 37 */
                null_write_function,                     /* 38 */
                null_write_function,                     /* 39 */
                wv_temp_set_density,                     /* 40 */
                null_write_function,                     /* 41 */
                null_write_function,                     /* 42 */
                null_write_function,                     /* 43 */
                null_write_function,                     /* 44 */
                null_write_function,                     /* 45 */
                null_write_function,                     /* 46 */
                null_write_function,                     /* 47 */
                null_write_function,                     /* 48 */
                null_write_function,                     /* 49 */
                null_write_function,                     /* 50 */
                null_write_function,                     /* 51 */
                null_write_function,                     /* 52 */
                null_write_function,                     /* 53 */
                null_write_function,                     /* 54 */
                null_write_function,                     /* 55 */
                null_write_function,                     /* 56 */
                null_write_function,                     /* 57 */
                null_write_function,                     /* 58 */
                null_write_function,                     /* 59 */
                null_write_function,                     /* 60 */
                null_write_function,                     /* 61 */
                null_write_function,                     /* 62 */
                null_write_function,                     /* 63 */
                null_write_function,                     /* 64 */
                null_write_function,                     /* 65 */
                null_write_function,                     /* 66 */
                null_write_function,                     /* 67 */
                null_write_function,                     /* 68 */
                null_write_function,                     /* 69 */
                null_write_function,                     /* 70 */
                null_write_function,                     /* 71 */
                null_write_function,                     /* 72 */
                null_write_function,                     /* 73 */
                null_write_function,                     /* 74 */
                null_write_function,                     /* 75 */
                null_write_function,                     /* 76 */
                null_write_function,                     /* 77 */
                null_write_function,                     /* 78 */
                null_write_function,                     /* 79 */
                null_write_function,                     /* 80 */
                null_write_function,                     /* 81 */
                wv_resin_name_temp1, 	                 /* 82 */
                wv_resin_name_temp2,                     /* 83 */
                wv_resin_name_temp3,                     /* 84 */
                wv_resin_name_temp4,                     /* 85 */
                wv_resin_name_temp5,                     /* 86 */
                wv_resin_name_temp6,                     /* 87 */
                wv_resin_name_temp7,                     /* 88 */
                wv_resin_name_temp8,                     /* 89 */
                null_write_function,                     /* 90 */
                null_write_function,                     /* 91 */
                wv_status,                               /* 92 */
                wv_alarm,                                /* 93 */
                wv_alarm,                                /* 94 */
                null_write_function,                     /* 95 */
                null_write_function,                     /* 96 */
                wv_status,                               /* 97 */
                wv_alarm,                                /* 98 */
                wv_alarm                                 /* 99 */
};


#endif
