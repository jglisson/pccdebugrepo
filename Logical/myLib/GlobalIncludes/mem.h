/************************************************************************/
/*
   File:  mem.h

   Description: Defines the global structures used by the entire system.


      $Log:   F:\Software\BR_Guardian\mem.h_v  $
 *
 *    Rev 1.33   Sep 15 2008 13:17:26   gtf
 * Fixed addressability of relay's
 *
 *    Rev 1.32   Sep 03 2008 10:42:12   gtf
 * modified structure full_address to add members card and relay
 * to allow for addressing of user input channel in the structure.
 *
 *    Rev 1.31   Jul 17 2008 16:34:36   vidya
 * Added num_recipes.
 *
 *    Rev 1.30   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.29   May 16 2008 09:45:30   Barry
 * Added act_spd_bits to shrEXT
 *
 *    Rev 1.28   May 08 2008 14:15:24   FMK
 * reduced the number of max_alarms to 704.
 *
 *    Rev 1.27   May 02 2008 06:55:54   Barry
 * Deleted time_wt_str(too big)
 *
 *    Rev 1.26   Apr 30 2008 15:07:12   vidya
 * Deleted GLOBAL char arrays for Setparts,
 * GateOrder, ResinParts etc and made them
 * local to the function.
 *
 *
 *    Rev 1.25   Apr 30 2008 14:37:06   Barry
 * added time_wt_str
 *
 *    Rev 1.24   Apr 23 2008 11:02:38   Barry
 * Added elepsed_unstable_time to shrBWH.
 *
 *    Rev 1.23   Apr 22 2008 10:09:52   FMK
 * Added min max ad values to shrbwh structure.
 *
 *    Rev 1.22   Apr 22 2008 09:01:24   Barry
 * Added settling_timer to shared_gate and calibrating to
 * shared global
 *
 *    Rev 1.21   Apr 14 2008 13:37:16   FMK
 * Moved the configuration file storage to the
 * main partition so it can be read by an editor.
 *
 *    Rev 1.20   Apr 11 2008 08:56:20   FMK
 *
 *
 *    Rev 1.19   Apr 10 2008 13:58:02   vidya
 * Added FeedRateParts and changed ResinParts
 * to Resin_Parts.
 *
 *    Rev 1.18   Apr 04 2008 11:38:50   vidya
 * Added recipe_num to PMEM structure.
 *
 *    Rev 1.17   Apr 03 2008 13:53:58   vidya
 * Added temp_recipe, recipe_inventory,
 * resin_inventory to PMEM structure.
 *
 *    Rev 1.16   Apr 02 2008 15:06:08   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.15   Apr 02 2008 09:36:26   vidya
 * Added ResinParts.
 *
 *
 *    Rev 1.14   Apr 01 2008 09:53:20   FMK
 * Added structure for permanent memory variables.
 *
 *    Rev 1.13   Mar 24 2008 13:03:02   FMK
 * Added command variables for extruder/haul off
 * command structure.
 *
 *    Rev 1.12   Mar 21 2008 10:16:02   FMK
 * Got the extruder manual screen working. Added
 * command structures so extruder/ho loops should
 * get commands.
 *
 *    Rev 1.11   Mar 13 2008 13:05:20   vidya
 * Added global variables to hold hopper parts,
 * density etc.
 *
 *
 *    Rev 1.10   Mar 05 2008 17:59:50   Barry
 * Added aux_alarm_time and Aux_starter_input_steady
 *
 *    Rev 1.9   Mar 04 2008 13:33:14   Barry
 * Added gate_time_clip_factor
 *
 *    Rev 1.8   Feb 29 2008 14:11:28   FMK
 * Added variable defining which report needs to
 * be printed by the print manager.
 *
 *    Rev 1.7   Feb 27 2008 14:30:46   FMK
 * Added command structure for external c1200
 * like drives. Printer is no longer connected to
 * serial port. Removed from configuration.
 *
 *    Rev 1.6   Feb 27 2008 14:12:48   Barry
 * Added Mixer command structure.
 *
 *    Rev 1.5   Feb 06 2008 10:37:28   FMK
 * Added global variables for device calibration. Removed
 * unecessary variable for alarm map on the visualization.
 *
 *    Rev 1.4   Feb 06 2008 10:25:16   Barry
 * Added Gatecommand and Bwh Command
 *
 *    Rev 1.3   Feb 04 2008 09:48:18   FMK
 * Changed the variable names when referenceing
 * the weigh input for the extruder.
 *
 *    Rev 1.2   Jan 28 2008 13:52:56   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.1   Jan 25 2008 14:05:52   FMK
 * Changed the path definitions to the different
 * cf drives on the system.
 *
 *    Rev 1.0   Jan 11 2008 10:33:36   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include "pcc_math.h"
/*#include "recipe.h" */

#ifndef MEM_H
#define MEM_H

#define PI 3.1415926
#define OVF_INT        32767        /* overflow value for signed integer */

_GLOBAL unsigned char Ext_running_mode;            /* current mode running EXT   */
_GLOBAL unsigned char Ho_running_mode;             /* current mode running HO    */
_GLOBAL unsigned char SHo_running_mode;            /* current mode running HO    */

#define PE_SIZE           50         /* size of prog enable flag matrix */
#define PRODUCT_CODE_LEN  16

#define SAVED_AD_SIZE     1800       // was 200 G2-517

#define FileNameSize 25              /* length of path/filenames used on cf card */
#define MAX_FILES    25              /* max # of files allowed in system */
#define SYSDrivePath  "SYSDISK"      /* Name of path for the system files */
#define USB1DrivePath "USB1PATH"     /* Name of path for the thumbdrive in usb port 1 */
#define USB2DrivePath "USB2PATH"     /* Name of path for the thumbdrive in usb port 2 */

#define MAX_RESINS   100
#define MAX_RECIPES  (400+11)
#define RESIN_LENGTH 10

#define MAX_DEFINED_BLOCKS            10
#define MAX_DEFINED_ENTRIES_PER_BLOCK 50

#define MAX_ADDR         128         /* # of available addresses - 0-127     */
#define MAX_GEARS         16         /* max # of gears on primary haul off        */
#define MAX_RELAYS        44         /* Maximum number of user configurable relays. */
#define MAX_MOVE          30         /* maximum # of moving points                */
#define MAX_AVG_PTS       10         /* max number of moving pt avg pts        */
#define MAX_GATE_CL_DATA  80         /* must be set less than                  */
#define MAX_ALARMS       705         /* # of available alarm conditions      */
#define MAX_DRIVES         3         /* number of c1200 equiv drives available */
#define MAX_LIW            2         /* number of LIW loops possible ext, gravifluff */
#define MAX_GATES         12         /* Maximum number of feed gates */
#define MAX_EXT            8

#define TC_ONE_CYCLE_TIME  5       /* Not used only here to show cyccle time of this task class */
#define TIME_WT_RESOLUTION 5       /* This number must be 2 * TC_ONE_CYCLE_TIME + 1 */
#define MIN_GATE_TIME      50      /* Default gate minimum firing time 10 times tc_1 cycle time OS9 = 50mS GTF*/
#define TIME_WT_MAX_STEP   100     /* Largest resolution entry in time_wt */

#define WEIGH_HOP_LOOP  (MAX_GATES)         /* weight hopper control loop #         */
#define MIXER_LOOP      (MAX_GATES+1)       /* MIXER control loop #                 */
#define EXT_LOOP        (MAX_GATES+2)       /* EXT control loop #                   */
#define WTH_LOOP        (MAX_GATES+3)       /* WIDTH control loop #                 */
#define GRAVIFLUFF_FDR_LOOP (MAX_GATES+4)   /* max # of liw procs                   */
#define GRAVIFLUFF_LOOP (MAX_GATES+5)       /* gravifluff control loop#             */
#define HO_LOOP         (MAX_GATES+6)       /* haul off control loop #              */
#define SHO_LOOP        (MAX_GATES+7)       /* Secondary haul off loop #            */
#define MAX_LOOP        (MAX_GATES+8)       /* maximum # of control loops           */
#define LOADING_LOOP    (MAX_GATES+11)

#define HopperSwitchOFF 0
#define HopperSwitchON  1
//#define InputChannel    127  // hopper A magnetic switch for 1Kg blender only
#define GateKeyswitchON 1

#define MAX_HOPPER      (MAX_GATES+1)   /* maximum # hoppers/extruder           */
#define REFEED          (MAX_HOPPER-1)  /* refeed 'hopper' number, for rec,mon  */

/* define different device types */
#define TYPE_NONE          0
#define TYPE_SYSTEM        1
#define TYPE_LIW           2
#define TYPE_BWH           3
#define TYPE_GATE          4
#define TYPE_HO            5
#define TYPE_MIX           6
#define TYPE_FLF           7
#define TYPE_WTH           8
#define TYPE_LOADING       9
#define TYPE_RECIPE_NAME   11
#define TYPE_RECIPE_DATA   12
#define TYPE_RESIN_DATA    13
#define TYPE_RESIN_NAME    14

#define AUTOBATCH 1
#define GUARDIAN  0

// switch sates for load system in procmon.c 
#define SYSTEMLOADED  0
#define LOADIP        1
#define LOADFILE      2
#define LOADTIME      3
#define LOADDEFAULT   4

// min and max for learn mode entry GTF 
#define PG018_MIN_LEARN 15
#define PG018_MAX_LEARN 25

#define OUTPUT_TWT_BUILD FALSE // flag for outputing each store in the twt 
#define STARTUPDELAY (20*10)   //G2-639

// The CHECK_LICENSE_DURING_OPERATION configuration defines weather or not the license key 
// will be checked during operation or just at startup.

//#define RELEASE_CODE

#ifdef RELEASE_CODE
#define CHECK_LICENSE_DURING_OPERATION   // define for release.
#define CRYPT_ENABLED                    // define for release.
#endif

//#define TEST_BUTTONS                   // Do NOT define for release.  page navigation test

_GLOBAL unsigned char sWizSetupInfoText[300];
_GLOBAL unsigned char sWizSetupFrameText[50];
_GLOBAL unsigned char G_au8LoadingSignal[12];
#ifdef ALLEN_BRADLEY_DRIVE
_GLOBAL unsigned char HostMacAddr[6];
#endif

_GLOBAL BOOL  reboot_SetSecurity;        // reset security to minimum on reboot
_GLOBAL char  CryptCheck;
_GLOBAL USINT u8ReadPanelIdTimeout;
_GLOBAL USINT u8PanelErrorCode;
_GLOBAL char  sSerialNumber[12];
_GLOBAL char  sMacAddr[18];
_GLOBAL BOOL  G_bWizCopyValuesToGlobal;
_GLOBAL USINT G_u8WizBlenderSize;
_GLOBAL UINT  u16PageToGoTo;

typedef struct
{
   ordered_pair wtp_samples[MAX_MOVE];
   samples wtp_signal;
   float wtp_std_dev;           /* standard deviation of last few update rates */

   /* rate vs speed moving point average */
   struct moving_avg_str rs_avg;
   float rs_pts[MAX_MOVE];
   int   rapid_rs_updates;
   int   updates_at_this_wtp;
   
   /* least squared error variables */
   float lse_temps[4];
   int   sample_count;
   float wtp;
   float yintercept;

   unsigned int update_timer;    /* timer (OS9) from beginning of update            */
   unsigned int last_update_timer;
   int   update_bits;              /* wt bits at beginning of update interval         */
   float last_set_wtp;
   unsigned int calc_wm_ticks;   /* number of wm ticks maximum per update           */
 
   /* stats about updates */
   int long_update;              /* count of long updates                           */
   int short_update;             /* count of short updates                          */
   int long_update_sample_cnt;   /* number of weight samples in the longest update  */
   int short_update_sample_cnt;  /* number of weight samples in the shortest update */
   char update_scan; 
} wtp_data_str;


#ifdef TEST_BUTTONS
/* in-house test definitions */
#define TEST_NOT_STARTED 	   	0
#define OPEN_TEST_PLAN_FILE	   1
#define TEST_CASE_INIT			   2
#define TEST_CASE_ACTION		   3
#define TEST_CASE_READ_OUTPUT	   4
#define TEST_CASE_WRITE_RESULT   5
#define NEXT_TEST_CASE			   6
#define CLOSE_TEST_PLAN_FILE	   7
#define TEST_FINISHED			   8
/* end in-house test definitions */	
#endif

// needs to be defined for visual
_GLOBAL USINT u8ButtonsTestState;   
_GLOBAL USINT pg991StartButtonTest;    
_GLOBAL USINT pg991StopButtonTest; 

typedef struct 
{
  char cBuffer[40];
  char ucUsed;
} SMTP_MSG_QUEUE;

#define MAX_SMTP_QUEUE_MSGS 4
_GLOBAL SMTP_MSG_QUEUE SmtpQueue[MAX_SMTP_QUEUE_MSGS];

#define SIZE_TIME_WT_ARRAY 200

struct FileListStruct            /* structure definition of file system used on cf cards */
{
   char Filename[FileNameSize];  /* name of the file stored on the cf card */
   int  NumBlocks;               /* Number of blocks in the built file */
   char bWrite;                  /* Boolean value, when TRUE save this configuration file */
   char bRead;                   /* Boolean value, when TRUE save this configuration file */
};

_GLOBAL char SYSFilename[FileNameSize];   /* name of the file stored on the cf card */
_GLOBAL char SYSFilename2[FileNameSize];  /* name of the file stored on the cf card */

_GLOBAL UDINT gAllAlarmsAcked;  /* have all active alarms been acked? */

/* this structure was added to test for the presence of a popup at the
   beginning of several main routines. The problem being using an external
   keyboard and then swapping to a new page while the popup is still being
   displayed. This could cause a conflict on the page that you jump to. This
   variable is used to insure that the popup visibility is set to false
   unless needed 
*/
struct PopupActiveStruct
{
   unsigned char Recipe;
   unsigned char Totals;
   unsigned char Security;
   unsigned char Alarms;
   unsigned char Monitor;
};

struct Alarm_Map
{
   BOOL Gen[MAX_ALARMS];
   BOOL Info[MAX_ALARMS];
   BOOL Shut[MAX_ALARMS];
};

_GLOBAL struct Alarm_Map Alarm;

/************************************************************************/
/* batch statistics                                                     */
/************************************************************************/
struct batch_stat_str
{
    float gate_set_wt;            /* gate total lbs to feed             */
    float gate_set_tol;           /* gate tolerance for feeding         */
    float gate_act_wt;            /* gate total lbs fed                 */
    float gate_set_percent;       /* set blend percent in this batch    */
    float gate_act_percent;       /* actual blend percent in this batch */
    float feed_time;              /* total feed time in seconds         */
    float gate_avg_wt_deviation;  /* average deviation (UFS)    	   	*/
    float sparef1; 				  
    float sparef2;    			  
    float sparef3; 				  
    int   numOutOfSpec;           /* # average deviation out of spec    */
    int   sparei1;        		  
/*  char  clear_gate_cal;         Flag to say we need to clear cal. Use shrGATE instead*/
    char  sparec[3];
};

/************************************************************************/
/* recipe storage descriptions                                          */
/************************************************************************/
typedef union
{
  float f;              /* space for a float    */
  char  c[4];           /* space for 4 chars    */
  unsigned int i;       /* space for an integer */
  double d;             /* space for an double  */
} stored_recipe;

/************************************************************************/
/* general union descriptions                                           */
/************************************************************************/
typedef union
{
  unsigned int i;         /* space for an integer */
  float  f;               /* space for a float    */
  char   c[4];            /* space for 4 chars    */
  double d;
} fic;

typedef struct
{
  short type;              /* bit defined receiver or pump                 */
                           /* all times are in 0.1 second increments       */
  short clean_time;        /* bag clean time for dust collector/rp's       */
  unsigned int fill_time;  /* fill time w/no prox, alarm time w/prox       */
  short dump_time;         /* dump time if have positive shutoff or knife  */
  short priority;          /* priority of receiver                         */
} receiver_type;

typedef struct
{
  short type;                /* bit defined receiver or pump            */     
        
  // all times are in 0.1 second increments       
  short clean_time;          /* bag clean time for dust collector       */
  short continuous_run_time; /* continuous run time                     */
  short starter_fail_time;   /* starter fail time                       */
  short start_delay_time;    /* start delay before pumping              */
} pump_type;

typedef struct                  /* full address on net of any device    */
{
  unsigned char addr;           /* network address                      */
  unsigned char chan;           /* channel on board at address addr     */
} full_address;

typedef struct                  /* address of I/O in the op station     */
{
  unsigned char chan;           /* channel on board at address addr     */
} half_address;

typedef struct 
{
  REAL fAverageMotorSpeed;
  REAL fWt;
  REAL fNominalMotorSpeed;
} PointInRateSpeedTable;

typedef struct
{
  char            number;              /* port number                                */
  unsigned char   function;            /* what function assigned to port            */
  unsigned char   protocol;
  unsigned char   address;             /* address                                    */

  char            baud;                /* PD_BAU baud rate (OS9 format)              */
  char            parity;              /* PD_PAR parity,bits (OS9 format)            */
  char            data_bits;
  char            stop_bits;

  char            options;             /* bit 0=bias, bit 2,1=422(00),485(01),232(10) */
  char            connect_type;        /* type of connection (Direct, modem, T1, ISDN etc)  */
  unsigned char   multidrop;           /* Mask for setting/clearing multidrop        */
  char            sixteen_bit_mode;    /* 16/32 bit int xfers for remote cntrl       */
} port_desc;

typedef struct
{
   full_address   location;             /* board addr(0=CPU SPI port)                 */
   char active;                         /* TRUE if function active                    */
   char invert;                         /* TRUE if signal is to be inverted           */
   char setup_a;
   char setup_b;
} relay_desc;

/************************************************************************/
/* structure for remote diagnostic data                                 */
/************************************************************************/
struct port_diag_data_str
{
  int dbg_remote_low_pt;
  int dbg_remote_high_pt;
  unsigned int good_remote_reads; /* counts good msgs in remote comm  */
  unsigned int bad_remote_messages;  /* counts bad msgs in remote comm   */
  unsigned int good_remote_writes;     /* Counts each message responded to */
  int last_point_processed;     /* Last point number processed */

  unsigned int remote_bytes_read;
  unsigned int remote_bytes_written;
  int bytes_in_bfr;             /* bytes currently in the input buffer waiting to be read */
  unsigned int rem_point_cnt;
  float remote_rate;                 /* remote messages per sec calculation */
  unsigned int acks_sent;
  unsigned int acks_rcvd;
  unsigned int naks_sent;
  unsigned int naks_rcvd;
  unsigned int enqs_sent;
  unsigned int enqs_rcvd;
  unsigned int rcv_timeouts;
  unsigned int xmit_timeouts;
  unsigned int last_nak_code;
  unsigned int enq_limit_exceeded;
  unsigned int naks_limit_exceeded;
  unsigned int bad_checksum;            /* count for bad checksum, CRC, LRC etc*/

  /* variables used to calculate statistics */
  unsigned int last_rem_pt_cnt;     /* message count at the last calculation*/
  unsigned int last_rem_calc;       /* time of last calculation */

  int good_mbenet_recvs;
  int good_mbenet_writes;

  int good_abtcp_recvs;
  int good_abtcp_writes;
  int bad_abtcp_message;

  char     port_alive;                 /* flag to monitor ports        */
  char     remote_alive;               /* flag to monitor remote proc  */
  char     mbenet_alive;               /* flag for monitor MBENET proc */
  char     abtcp_alive;                /* flag for monitor MBENET proc */
  char     ab_xmit_alive;              /* flag to monitor AB xmit proc */
};

struct rem_device_definition
{
   unsigned char id;
   unsigned char sub_device;
};

/************************************************************************/
/* structure for user definable remote register blocks                  */
/************************************************************************/
struct defined_block_str
{
    unsigned int points[MAX_DEFINED_ENTRIES_PER_BLOCK];   /* list of registers                            */
};

/************************************************************************/
/* configuration data                                                   */
/************************************************************************/
typedef struct
{
   float density;
   char  resin_desc[RESIN_LENGTH];
} resin_str;

typedef struct
{
  unsigned char device_type;    /* type of device (liw, ho, etc.)       */
  unsigned char device;         /* device index, based upon device_type */
  unsigned char alarm_type;     /* type of alarm (critical low, etc)    */
  unsigned char pause_action;   /* action if alarm true (shutdown, etc) */
  unsigned char man_action;     /* action if alarm true (shutdown, etc) */
  unsigned char auto_action;    /* action if alarm true (shutdown, etc) */
  unsigned char ack;            /*                                      */
} structAlarmAction;

typedef struct
{
   unsigned int   ConfigSize;
   unsigned int   TrailerOffset;
   char           Name[16];
} ConvertVersionInfo;

typedef struct
{
   char  VersionMarker[8];    /* this must be the first item in this structure and must contain the string "VERSION" */
   char  VersionString[16];
   char  FirmwareNum[16];
   char  DateString[16];
} version_info;

typedef struct
{
   full_address prox_primary;    /* address of receiver prox/flapper relay */
   full_address out_primary;     /* address of output value relay      */
   unsigned short fill_time;     /* fill time w/no prox, alarm time w/prox       */
   unsigned short dump_time;     /* dump time if have positive shutoff or knife  */
   unsigned char  enabled;       /* enable loading */   
} config_REC_device;

typedef struct
{
   full_address dc_pump_relay;   /* pump on or off relay                 */
   full_address cont_run_relay;  /* whether continous running or not     */
   full_address anx_cont;        /* anxiliay contact                     */
   full_address dust_a_relay;    /* dust collector A on or off           */
   full_address dust_b_relay;
   full_address vfd_high_relay;
   full_address vfd_low_relay;
                         /* all times are in 0.1 second increments       */
   unsigned short clean_time;            /* bag clean time for one dust collector    */
   unsigned short continuous_run_time;   /* continuous run time                      */
   unsigned short restart_time;          /* starter fail time                        */
   unsigned short start_delay_time;      /* start delay before pumping               */
   unsigned int   start_delay_timer; 
   unsigned char  dust_used;
   unsigned char  cont_used;
} config_PUMP_device;

typedef struct /*  config_super */
{
	char structureName[15];
	char VersionString[16];
	version_info Info;           /* structure containg version info */
	unsigned char device_name[16];
	BOOL current_blender_type;
   
	/* internet stuff */
	unsigned char ip_address[4];
	unsigned char subnet_mask[4];
	unsigned char gateway[4];

	/* system configuration flags */
	char language;                /* which language - ENGLISH or other    */
	char control_mode;            /* control mode of operation            */
	char wtp_mode;                /* control mode for extruders           */
	char ltp_mode;                /* control mode for line speed          */

	char sltp_mode;               /* mode for the secondary line speed    */
	char wth_mode;                /* control mode for width               */
	char wth_type;                /* type of width control                */
	char pccweb_language;

	unsigned char demo;           /* flag for we are in demo mode         */
	char use_remote_int;          /* if true, do i/o via serial port also */
	char control_loading;         /* flag to say we are doing loading     */
	char application;             /* application type                     */

	char recipe_mode[4];          /* recipe entry variable list           */
	char monitor_sys[4];          /* monitor system variable list         */
	char num_ext;                 /* number of extruders being controlled */
	/* must be set to 1 if extrusion control*/
	/* needed for common software           */
	char total_gates;             /* total number of usable hopper        */
	/* elements in system (see frame_size)  */
	char frame_size;              /* total # of possible elements on this */
	/* blender frame                        */
	char job_name[PRODUCT_CODE_LEN];
	char job_name_used;           /* whether use job number               */
	char clear_rec_total;
	char scrap;                   /* flag for scrap input to extruder     */
	char refeed;                  /* flag for type of scrap refeed        */
	char FeatureFlag;             /* Flag used to define which features are enabled */
	char gate_order_in_recipe;    /* select whether the gate feed order is in the recipe */
	unsigned char pe_needed[PE_SIZE];    /* security level needed flags   */
	unsigned char hopper_parts_lock[MAX_GATES+1]; /* need key to change gate parts */
	unsigned char current_security_level; /* security level of user interface */
	unsigned char minimum_security_level; /* lowest security level, for resetting security */
	unsigned char minimum_web_security_level;
	char observer_code[8];        /* observer password, here to keep mem aligned */
	char maintenance_code[8];     /* maintenance password, here to keep mem aligned */
	char operator_code[8];        /* operator password                    */
	char supervisor_code[8];      /* supervisor code                      */
	/* recipe stuff */
	/*char no_total_in_recipe;*/      /* flag for if no total+parts, just thks*/
	char layers_by;
	char ho_in_ratio;             /* flag for ho is ratio'd in manual     */
	char line_spd_is_master;      /* flag for line speed is master ratio  */
	char using_32bit_mode_holding_reg;  /* flag for 32 bit mode HOLDING REGISTERS of modbus */
	char continue_batch_after_stop;
	char width_in_recipe;         /* ask for width in recipe?             */
	char density_in_recipe;       /* densities part of recipe?            */
	char self_correct_TWT;		   /* use to enable/disable this feature	 */
	char man_in_recipe;           /* flag for have manual speeds in recipe*/
	char mix_time_in_recipe;      /* flag if mix time is in the recipe    */
	char job_wt_in_recipe;        /* flag set if job weight is included in recipe */
	char recipe_entry_mode;       /* Either PARTS or PERCENT              */
	char increase_accuracy_mode;  /* second feed not 80% of the rem WT    */
	char auto_set_man_spd;        /* flag to set manual start spds in auto*/
	char special_in_recipe;       /* number of special elements in recipe */
	char enable_resin_codes;      /* TRUE if resin descriptions/codes used*/
	char enable_product_code;     /* TRUE if require typing in of recipe  */
	char force_density;           /* enter densities in recipe            */
	char stretch_factor_in_recipe;
	char gear_num_in_recipe;
	char step_size_in_recipe;
	char first_gate_normal_tolerance;   /* use to enable/disable this feature		*/
	char batch_size_recalculation;		/* use to enable/disable this feature		*/
	int current_temp_recipe_num;
	unsigned int recipe_size;      /* # of bytes available for storage     */
	unsigned int num_recipes;
	unsigned short wordsperrecipe; /* # of ints or floats per recipe       */
	int recipe_inven_offset;       /* offset to recipe inventories         */
	float job_wt;                  /* stop blender after making this many lbs */
	float job_wt_subtotal;         /* this is how far it has come          */
	full_address  purge_valve_1;   /* address of purge valve 1       */
	full_address  purge_valve_2;   /* address of purge valve 2       */
	float purge_duration;          /* duration of purge */
	float purge_cycle_time;        /* purge cycle time, alternating time between purge valves*/
	float top_purge_time;          /* duration of purge */
	full_address  purge_valve_3;   /* address of top purge valve        */
	char remote_relay_stop_end_batch; /* flag if immediately stop by using remote relay*/
	char ok_to_run_output_signal;
	full_address ok_to_run_output_address;
	char recipe_type;
	char remote_purge;
	int G_u16ProfibusInputBuf;
	int G_u16ProfibusOutputBuf;
	full_address  remote_purge_start_relay;      /* address of purge valve 1       */
	full_address  remote_purge_complete_relay;   /* address of purge valve 2       */
	float default_wtp;            /* default wtp for recipe               */
	float default_ltp;            /* default ltp for recipe               */
	float default_wpa;            /* default wpa for recipe               */
	float default_wpl;            /* default wpl for recipe               */
	float default_width;          /* default width for recipe             */
	float default_id;             /* default id for recipe                */
	float default_od;             /* default od for recipe                */
	float default_thk;            /* default thickness for recipe         */
	resin_str resin_table[MAX_RESINS];
	/* misc */
	char spd_in_percent;          /* flag for if spd is in % or other     */
	char no_auto_from_pause;      /* flag - must go from man to auto      */
	char intl_date_format;        /* flag to indicate use of international date format */
	char use_batch_report;        /* flag to indicate use of batch report */
	unsigned int clear_shift_total_flag;	/* flag to indicate new job    */
	char auto_switch_to_auto;     /* if true, when # updates done, auto   */
	char auto_switch_updates;     /* number of updates needed to switch   */
	unsigned char job_wt_complete;/* flag for this job wt is done         */
	char silence_old_alarms;
	unsigned short net_retry_time;/* retry time for bad addr in ticks     */
	unsigned short net_timeout_time; /* timeout time in ticks for bad msg */
	unsigned short net_max_tries; /* max # of net attempts before bad     */
	int set_alarm_relay_type;     /* specifies what kind of alarm to set  */
	/* alarm relay for                      */
	float accel;                  /* entire line accel value in %/sec     */
	float decel;                  /* entire line decel value in %/sec     */
	float target_accuracy;        /* the master accuracy for the blend    */
	/* 0.0005 = 0.05%                       */
	float step_size;              /* step size for manual ramping         */
	pump_type pump;               /* pump control description             */
	full_address pump_mod;        /* address of pump module               */
	relay_desc pio_relays[MAX_RELAYS]; /* pio relay function structure    */
	/* index is function number             */
	/* alarms */
	char aux_alarm_msg[36];       /* space for aux alarm message          */
	structAlarmAction AlarmAction[MAX_ALARMS];     /* alarms structure    */
	int t_current_resin;
	int t_current_recipe;
	/* UNITS  */
	float wtp_k;                  /* 'k' factor for wtp                   */
	float wpl_k;                  /* 'k' factor for weight/running length */
	float wpa_k;                  /* 'k' factor for weight/area           */
	float area_k;                 /* 'k' factor for area                  */
	float t_k;                    /* 'k' factor for thickness             */
	float ltp_k;                  /* 'k' factor for ltp                   */
	float od_k;                   /* 'k' factor for od                    */
	float id_k;                   /* 'k' factor for id                    */
	float width_k;                /* 'k' factor for width                 */
	float ea_k;                   /* 'k' factor for thickness to ea       */
	char units;                   /* ENGLISH or METRIC                    */
	char wpl_unit;
	char wpa_unit;
	char od_unit;
	char id_unit;
	char width_unit;
	char wtp_unit;
	char thick_unit;
	char length_unit;
	char weight_unit;
	char ltp_unit;
	char temp_unit;
	/* remote & serial port stuff */
	char print_verbose;           /* verbose printing mode                */
	char log_alarms;              /* flag to log alarms to printer        */
	char auto_reports;            /* flag to gen automatic reports        */
	char clear_totals_on_print;   /* flag for clear totals on printout    */
	char print_job_wt_report;     /* flag to print report after each job  */
	char use_pcc2_address;        /* if true the new PCC2 remote addressing will be used */
	char compatibility_mode;
	unsigned char return_set_if_act_0;    /* For remote, if no computed actuals do not return zero - use sets */
	unsigned char remote_protocol;/* who's remote protocol to use         */
	unsigned char remote_address; /* our address                          */
	char autorun_after_rate_changed;  /* for remote when rate changed */
	char plc2_word_or_byte;       /* plc2 word or byte addressing         */
	unsigned char remote_multidrop;
	char device_on_port[8];       /* keep track for port configuration for new user interface */
	int report_start;             /* starting time in sec since midnight  */
	int report_interval;          /* interval between reports in seconds  */
	port_desc remote_port;        /* port stuff                                */
	port_desc width_port;         /* width controller port stuff               */
	port_desc remote_int_port;    /* remote int port stuff                     */
	port_desc spare_port_1;
	port_desc remote_gravitrol_link;/* If gravitrol is controlling the extruder  */
	port_desc spare_port_2;
	char dual_port_comm_enabled;  /* flags for comm options enabled/disabled   */
	char web_server_enabled;
	char dual_port_byte_order;
	char mbenet_enabled;          /* MODBUS/TCP */
	char DC_num_loading_before_cleaning;
	char gravilink_uses_ethernet;   /* TRUE if gravilink over ethernet                */
	unsigned char select_recipes_remotely; /* TRUE if using i/o in mini-op to change recipes */
	unsigned char gravilink_version;       /* Set by gravitrol if doing gravilink */
	unsigned char gravilink_revision;
	struct defined_block_str user_def_blk[MAX_DEFINED_BLOCKS];
	float register_multiplier[100]; /* multipliers for integer remote protocols */
	struct rem_device_definition rem_device_defs[100];
	unsigned char  total_receivers;
	char blend_control_remote;
	char ab_use_crc_chksum;
	char abtcp_enabled;             /* for ALLEN BRADLEY ETHERNET */
	unsigned int diagnostics_start; /* last time diagnostics cleared*/
	config_PUMP_device cpump;
	char blend_control;             /* for choosing which style to be used */
	char report_change_recipe;
	unsigned short trailer;         /* trailer must equal for recall        */
	char self_cleaning;             /* for choosing if self cleaning on */
	char self_cleaning_flag;
	int scrBrightness;
	int scrContrast;
	int scrBacklightSaver;
	int calib_minute;
	int calib_hour;
	int calib_day;
	int calib_month;
	int calib_year;
	unsigned short scrBacklightTimeout;
   unsigned short scrPageTimeout;
	unsigned char MultiLingual;
	int pg175_System_Size;
	TIME EnteredPassword;          /* The time that Supervisor or Service level was entered                     */
	int TimeOut;                   /* The allowable time in ms before timing out current security level 0=never */
	unsigned char BlendMainBkgdColor; /* saved blender Main background color control, config_super.BlendMainBkgdColor Dragan */
	int loading_controlled_remotely;  /* For Cryovac loading done completely by remote BMM 1*/
	int purge_input;               /*  enable self_cleaning mode (purge mode) */
	unsigned char ucModbusSwapBytes;
	unsigned char profibus_enabled;
	unsigned char ucIconInterface;  
	float fRegrindStep;  
	unsigned char uc3ProxyRegrindSleep;	
	unsigned short Keep_Alive_Toggle;
	char  new_wtp_smooth;             /* new wtp smooht factor */
   float new_wtp_smooth_clip;        /* new wtp change limit */
   float gate_delay;                 //9/9/16 drt
   unsigned char pg201RunOutEna;     //G2-224 2/1/17 drt
   //G2-225 1/31/17 drt
   unsigned char pg201_SelChannel[7];
   unsigned char pg201_SelInv[7];
   unsigned char pg201_SelAct[7];
   //G2-343 4/21/17 drt
   unsigned char DisplayLanguage;   //global for language selected
   USINT u8XmlFilename[40];         // G2-362 drt
   USINT pg201_self_clean;          // G2-465 drt
   unsigned char pg201_SelectedRelay;
   BOOL tenhzflag[2];
   unsigned char G3Mode;
   unsigned char pg990_HopperDoorSwitch;           // G2-558
	unsigned char SetUpSwitchSafetyRuntime;	      //	this will apply only for 1Kg Guardian 3
   unsigned char Flood_Feed_Timer_Flag; //G2-596
   float Flood_Feed_Timer;              //G2-596
   unsigned char Hostname[16];          //G2-620
   unsigned char pg2180_aux5_invert;    //G2-662


	//char  filler[6];    // when you need space, subtract your new variable size so struct size doesnt change
	/*
	* do NOT... under any circumstances define ANY variables below the
	* CRC definition below.  If you do, the world will end, the universe
	* will implode, and hell will freeze over.  Well, that and the CRC
	* calculations will always be wrong.
	*/
	unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_super;



typedef struct   /* config_LIW_device loss-in-weight config struct defs */
{
  char   structureName[15];
  char   VersionString[16];
  char   used;                        /* flag for this liw used         */
  char   ext;                         /* extruder number                */
  char   hop;                         /* hopper number for that ext     */
  char   extruder_with_additives;     /* true if LIW has additives      */
  char   cal_done;                    /* flag set when cal complete     */
  char   drive_type;                  /* type of drive                  */
  char   drive_has_control;           /* flag for drive has control of  */
                                      /* weight loss functions - use    */
                                      /* liw_int & no weigh module      */
  char   blender_has_wm;              /* flag to indicate that blender  */
                                      /* has the wm and calculates wtp  */
                                      /* should use cntl_bln instead    */
                                      /* of liw_pcc                     */
  int           offsetbits;           /* calculated each time a batch is
                                       * finished to rezero hopper weight */				
  full_address weigh_mod;               /* address of weigh module      */
  full_address drive_mod;               /* address of drive module      */
  full_address load_relay;              /* address of load relay        */
  full_address alarm_relay;             /* address of alarm relay       */
  unsigned char demo;                   /* drive or weigh mod in demo mode ? */
  char     sync;                        /* set if sync used             */
  short    sync_angle;                  /* syncronization angle in deg  */
  unsigned short drive_inhibit_invert;  /* set to DRV_INHIBIT if true   */
  float    ad_filter;                   /* a/d filter frequency         */
  receiver_type receiver;               /* receiver definitions         */
  /*            calibration                                             */
  int      zerowt;              /* # of bits when hopper empty          */
  int      testwt;              /* # of bits with test wt on hop        */
  float    test_wt;             /* size of test weight (in wt)          */
  float    resolution;          /* 1/10,000 of range of loadcells       */
  float    wtperbit;            /* # of wt per bit                      */
  short    year;                /* date of last calibration             */
  char     month;
  char     day;
  //****************************
  // alarm/load weights         
  float    load_wt_on;          /* below this weight, signal load       */
  float    load_wt_off;         /* above this weight, load off          */
  float    high_alarm_wt;       /* Above this weight, High HOPPER ALARM */
  float    alarm_wt;            /* below this weight, LOW HOPPER ALARM  */
  float    crit_wt;             /* below this weight, CRIT LOW ALARM    */
  float    min_dump_wt;         /* below this weight, LOW DUMP ALARM    */
  unsigned short load_time;     /* load time if in time mode            */
  unsigned short detect_time;   /* weight trip detect time              */
  // speeds - all in %'s except spd factor         
  float    warning_high_spd;    /* above this speed, recipe warning     */
  float    warning_low_spd;     /* below this speed, recipe warning     */
  float    alarm_high_spd;      /* above this speed, OVERSPEED ALARM    */
  float    alarm_low_spd;       /* below this speed, UNDERSPEED ALARM   */
                                /* also, no rate spd below this         */
  float    max_spd;             /* never try to run above this speed    */
  float    max_spd_rpm;
  float    spd_factor;          /* multiplier for rpm or linear speed   */
  float    zero_crossing;       /* speed at which drive starts to turn  */
  float    accel;               /* motor accelleration value in %/sec   */
  float    decel;               /* motor decelleration value in %/sec   */
  char     load_disabled;       /* flag, if set, disables loading       */
  char     load_relay_type;     /* type of load relay installed         */
  // rate/speed info       
  unsigned char algorithm;      /* type of algoritm used                */
  unsigned char rs_spare;       /*                                      */
  unsigned char movepts;        /* number of mov points(0->MAX_MOVE)    */
  unsigned char rs_clip;        /* specifies whether rs_maxchg active   */
  unsigned char rs_num;         /* number of rs factors calculated      */
  unsigned char rs_last;        /* ptr to last r-s factor stored        */
  float    rs_resetchg;         /* % change in speed to cause new rs    */
  float    rs_maxchg;           /* specifies max dev from ave_rs        */
  float    rs_fact[MAX_MOVE];   /* array of last rs_num r-s factors     */
  float    rs_sum;              /* sum of last rs_num r-s factors       */
  float    rs_ave;              /* ave of last rs_num r-s factors       */
  // control loop parameters 
  float    dead_band;           /* control dead band percent muliplier  */
  float    max_error;           /* max error before throwing away       */
  unsigned short calctime;      /* calc time in 68000 ticks             */
  unsigned short max_bad;       /* max bad updates before forcing update*/
  unsigned short coast_tolerance; /* bit tolerance for coast            */
  unsigned char  max_drive_fail;  /* max drive fails before alarm       */
  unsigned char  drive_fail_error; /* num bits/scan needed for no fail  */
  unsigned char  ad_filter_table;  /* selects which filter table to use */
  float    coast_error;         /* percent error for coast              */
  float    out_of_spec_limit;   /* out of spec alarm limit              */
  unsigned char initial_coast;  /* initial coast count when coasting    */
  unsigned short max_coast_time;/* max coast time in 68000 time ticks   */
  unsigned char out_of_spec_delay; /* LIW waits this many updates before
                                      checking for out-of-spec          */	
  //           miscellaneous 
                      
  //           diagnostic data            
  unsigned int loads;           /* number of loads (good coasts)        */
  unsigned int coasts;          /* number of bad coasts                 */
  float    avg_dump_size;       /* average dump size                    */
  float    accum_coast;         /* accumulated coast time (auto)        */
  float    accum_grav;          /* accumulated gravimetric time (auto)  */
  float    demo_max_wtp;        /* max wtp for demo mode                */
  float    min_wtp;             /* min wtp for this device              */
  float    demo_trim_rate;      /* trim rate for gravifluff demo mode   */
  float    update_wt;           /* amount of wt to run through on an update */
  int      sec_ad_gain;
  int      sec_ad_offset;
  char     skip;
  char     fast;
  char     ac;
  char     chop;
  int      delay;
  char     use_secondary_weight_card;
  char     use_internal_loading_signal_only;
  unsigned char drive_ip_address[4];
  char     num_bln_hoppers;     /* number of blender hoppers           */
  unsigned char use_positive_off_weight;
  unsigned char enable_multi_rs_factor;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_LIW_device;

typedef struct                      /*  config_gate gate config struct defs */
{
   char structureName[15];
   char VersionString[16];
   /*            flags, net addresses, etc.                                  */
   full_address  primary_gate;       /* address of drive modules             */
   full_address  secondary_gate;     /* set if two gates are used to feed material */
   full_address  out_secondary;      /* for powder receiver, secondary output add */
   full_address  proximity_switch;       /* address of proximity switch */
   
   unsigned char clear_recipe_on_crit_low; /* clear setpoint if out of material */
   unsigned char auger_used;         /* true if auger used instead of gate   */
   config_REC_device   creceiver;
   char          use_powder_receiver;/* set if powder receiver is used */
   unsigned char algorithm;          /* 0 is standard algorithm */
   unsigned char hopper_type;        /* NORMAL, REGRIND, NON CRITICAL        */
   unsigned char demo;               /* set if in demo mode                  */
   unsigned char tuning_method;      /* sets manual or autotuning            */
   unsigned char num_bad_feeds_before_alarm;
   unsigned char clear_gate_cal_after_out_of_spec;
   unsigned char secondary_gate_enabled;
   unsigned char recipe_use;
   unsigned char num_no_feeds_before_out_of_mat; /* feed count before out of material */ 
   char          prepulse_enabled;   /* enable a pre-gate pulse before feeding        */
   //char        clear_gate_cal;     /* set to clear the gate cal the next pulse  USE shrGATE instead */
                                     /* this is the master overide - normally use     */
                                     /* the recipe clear_gate_cal flag                */
                                     /* setting this flag also resets the tuning      */
   unsigned int  time_wt_resolution; /* gate time_wt table resolution in mS GTF       */
   unsigned int  minimum_gate_time;  /* minimum gate time in mS GTF                   */
   unsigned int  min_gate_time_index;
   //            control loop parameters  
   float         out_of_spec_limit;  /* out of spec alarm limit per ingredient   */
   float         target_accuracy;    /* percent accuracy target per ingredient   */
   int           learn_limit;        /* max nuber of batches in learn mode GTF   */
   float         gate_time_clip_factor;  /* gate time clip factor in percent,  0 = no clip  */
   // inventory         
   float        this_batch_wt;       /* total for this feed, used by bwh loop    */
                                     /* to add to shift & inven wt (gate doesn't)*/
   //            control loop data storage 
   float         cl_data[MAX_GATE_CL_DATA];
   unsigned int  initial_nominal_time;       /* time in ms GTF */
   float         last_cl_data;
   float         last_avg_time;
   int           gate_pulse_number;  /* accumulate the gate pulsing number for changing pin */
   char          proximityswitch_exist;    /* some Autobatch have proximity switch for hoppers */
   unsigned int  step_size_zc;  /* step size index for zero-crossing  */
   int           weight_settle_time;  
	float         firstshot_percentage;    /* for standard algrorithm */
	// controlled feed per Scott Martin
	float         firstshot_percentage_accuracy;     /* for standard algrorithm */
	float         secondshot_percentage_accuracy;    /* for standard algrorithm */
	float         thirdshot_percentage_accuracy;     /* for standard algrorithm */
	float         fourthshot_percentage_accuracy;    /* for standard algrorithm */	
   int           wt_sz_mux_1;   /*  step size multiplexer 1    autobatch small/medium tray only  */ 
   int           wt_sz_mux_2;   /*  step size multiplexer 2    autobatch small/medium tray only  */ 
   int           wt_sz_mux_3;   /*  step size multiplexer 3    autobatch small/medium tray only  */ 
   int           wt_sz_mux_4;   /*  step size multiplexer 4    autobatch small/medium tray only  */ 
   float         alarm_factor;
   char          allowdump_proxoff;  /* allow hopper A to dump only when prox uncovered,  for autobatch only */ 
   unsigned int  OOM_Alarm_Time;     /*  time to trigger out of material alarm when prox gets uncovered, for autobatch only */ 
   char          PreFeed_Enabled;    /* option for medium, small tray in autobatch  */
	char          ucRegrind3ProxEnabled;  /* REGRIND 3 Proxy option     */
	unsigned char num_before_dump_low;    /* feed count before dump low */
	char          filler[15];    // when you need space, subtract your new variable size so struct size doesnt change

  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_GATE_device;

typedef struct    /* config_bwh - batch weigh hopper config struct defs    */
{
   char structureName[15];
   char  VersionString[16];
   full_address  weigh_hopper;          /* address of weigh module      */
   full_address  dump_valve;            /* address of dump valve        */

  /*            calibration                                                    */
  int           zerowt;                /* # of bits when hopper empty          */
  int           testwt;                /* # of bits with test wt on hop        */
  float         test_wt;               /* size of test weight (in wt)          */
  float         wtperbit;              /* # of wt per bit                      */
  float         resolution;            /* 1/10,000 of range of loadcells       */
  int           offsetbits;              /* calculated each time a batch is
                                        * finished to rezero hopper weight */

  float         ad_filter;             /* a/d filter frequency                 */
  float         set_batch_size;        /* optimum batch size in lbs            */
  float         max_batch_size;        /* maximum batch size in lbs            */
  float         min_batch_size;        /* minimum batch size in lbs            */
  float         batch_oversize;
  float         batch_start_wt;

  int            min_bwh_dump_time;    /* max coast time in mS GTF */
  int            max_coast_time;       /* max coast time in mS GTF */
  char           cal_done;             /* flag set when cal complete           */
  char           autozero_on;          /* flag set if autozeroing on           */
  char 		     check_zero_wt_flag;
  unsigned char  demo;                 /* is this device in simulation */
  unsigned char  ad_filter_table;      /* selects which filter table to use */
  unsigned char  ad_loop_timer_factor; /* how long to sleep within one loop to calculate ad data */
  int			     load_cell_tolerance;  /* for BWH calibration selfcheck feature  */
  float          hop_wt_rezero_thres;
  char           filler[30];
  char           jiggle_on;          /* flag used to set the BWH to jiggle DF          */		
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_BWH_device;

typedef struct    /* config_mixer config struct defs             */
{
   char  structureName[15];
   char  VersionString[16];
   int   mix_time;                  /* target mix time in mS GTF                            */
   float dump_cycle_time;           /* dump cycle time                                      */
   float max_dump_time;             /* max tome before valve stuck alarm                    */
   float mixer_low_level_time;      /* time for setting mixer low level                     */
   int   gate_close_delay;          /* max time before valve stuck alarm-extra time mix gate is open in mS GTF */
   float demo_max_wtp;              /* max wtp for demo mode                                */
   char  keep_mixer_empty_ext_prox;	/* Tim Payton does not want the mixer full if waiting for external prox	*/
   char  bypass_mixer_prox_switch;  /* true if mixer prox switch is bypassed                */
   char  aux_starter_used;          /* true if have auxiliary starter panel                 */
   char  mixer_motor_mode;          /* control mode of mixer motor                          */
   char  mixer_gate_mode;           /* control mode of mixer gate                           */
   char  material_request_source;   /* set to source of material request - mixer prox,      */
                                    /* external prox, or Gravitrol hopper directly under    */
   char  material_request_invert;   /* set if material request is open condition            */
                                    /* only used if external source selected                */
   int   mixer_gate_mode_autobatch; /* flag used only for autobatch                         */	
   float aux_alarm_time;		      /* 5 to 20 seconds */
   char  aux_starter_input_steady;  /* yes--STEADY(default), no---PULSE                     */
   float UncoveredMatReqSwitchAlarmTime;  /* Tim Peyton special alarm timer setup value     */
   unsigned char demo;              /* is this device in simulation */
   unsigned char invert_mixer_prox;
   unsigned char filler[40];        /* use space in here for new variables                  */

  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_MIXER_device;

/************************************************************************/
/* structure for blender values via GRAVILINK                           */
/************************************************************************/
typedef struct
{
  int   num_hoppers;
  float dc_act_weight;
  float set_wtp_for_system;
  float act_wtp_for_system;
  float hop_weight[8];
  float hop_motor_spd[8];
  float hop_thruput_set[8];
  float hop_thruput_act[8];
  float hop_parts_set[8];
  float hop_parts_act[8];
  char  comm_error;
  unsigned char good_update;
} blender_points;

typedef struct                       /* haul off config struct defs        */
{
   char structureName[15];
   char VersionString[16];
   unsigned char demo;               /* drive or weigh mod. in demo mode?   */
   /*            rate/speed info                                            */
   unsigned char algorithm;          /* type of algoritm used               */
   unsigned char rs_spare;           /*                                     */
   unsigned char movepts;            /* number of mov points(0->MAX_MOVE)   */

  unsigned char rs_clip;             /* specifies whether max_chg active    */
  unsigned char rs_num;              /* number of rs factors calculated     */
  unsigned char rs_last;             /* ptr to last r-s factor stored       */
  char stop_instantly;               /* do not ramp down speed when pausing */
  //            network addresses     
  full_address drive_mod;                 /* address of drive module       */
  char     drive_type;                    /* type of drive                 */
  char     drive_has_control;             /* flag for drive has control of */
                                          /* haul off functions - use      */
                                          /* ho_int (always true if !PCC)  */
  unsigned short drive_inhibit_invert;    /* set to DRV_INHIBIT if true    */

  /*            calibration                                                */
  /* for blown film & sheet, enter in nip_circum   & pulses_per_rev or     */
  /* enter in bitsperlen or run calibration. for tubing, since have no     */
  /* nip, must enter in bitsperlen or run calibration                      */
  short    cal_done;                /* flag set when cal complete          */
  float    nip_circumference;       /* circumference of nip/pickup in inches*/
  float    pulses_per_rev;          /* # of pulses per nip revolution      */
  float    lenperbit;               /* # of len per bit                    */
  short    year;         /* date of last calibration */
  char     month;
  char     day;
                  /****************************/

  /*            speeds - all in %'s except spd factor                      */
  float    warning_high_spd;        /* above this speed, recipe warning    */
  float    warning_low_spd;         /* below this speed, recipe warning    */
  float    alarm_high_spd;          /* above this speed, OVERSPEED ALARM   */
  float    alarm_low_spd;           /* below this speed, UNDERSPEED ALARM  */
                                    /* also, no rate spd below this        */
  float    max_spd;                 /* never try to run above this speed   */
  float    spd_factor[MAX_GEARS];   /* multiplier for rpm or linear speed  */
  float    zero_crossing;           /* speed at which drive starts to turn */
  float    accel;                   /* motor accelleration value in %/sec  */
  float    decel;                   /* motor decelleration value in %/sec  */

  float    rs_resetchg;             /* % change in speed to cause new rs   */
  float    rs_maxchg;               /* specifies max dev from ave_rs       */
  float    rs_fact[MAX_MOVE];       /* array of last rs_num r-s factors    */
  float    rs_sum;                  /* sum of last rs_num r-s factors      */
  float    rs_ave;                  /* ave of last rs_num r-s factors      */

  /*            control parameters                                         */
  float    dead_band;               /* control dead band percent muliplier  */
  float    max_error;               /* max error before throwing away       */
  unsigned short calctime;          /* calc time in 68000 ticks             */
  unsigned short max_bad;           /* max bad updates before forcing update*/
  unsigned short coast_tolerance;   /* bit tolerance for coast             */
  unsigned char max_drive_fail;     /* max drive fails before alarm        */
  unsigned char drive_fail_error;   /* num bits/scan needed for no fail    */
  float    coast_error;             /* percent error for coast             */
  float    out_of_spec_limit;       /* out of spec alarm limit             */
  unsigned char out_of_spec_delay;  /* LIW waits this many updates before checking for out-of-spec */
  unsigned char initial_coast;      /* initial coast count when coasting   */
  unsigned short max_coast_time;    /* max coast time in 68000 time ticks  */
  float    max_spd_rpm;

  /*            diagnostic data                                            */
  unsigned int coasts;              /* number of coasts (should be never)   */
  float    demo_max_ltp;            /* max ltp for demo mode                */
  float   visual_max_ltp;
  float    min_ltp;                 /* min ltp for this device              */
  float    stretch_update_delay;
  unsigned char GearInputMethod;    /* Gear number selection method         */
  unsigned char drive_ip_address[4];/* PLC Drive IP Address                 */
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} config_HO_device;

typedef struct                      /* width    config struct defs          */
{
  char     mode;                    /* mode controller is in                */
  unsigned char demo;               /* set if loop in demo mode             */
  unsigned char width_address;      /* address of width controller          */
                                    /* drjoseph only                        */
  char     dr_joseph_ethernet;      /* DR Joseph ethernet?                  */
  float    alarm_wth_min;           /* minimum width alarm                  */
  float    alarm_wth_max;           /* maximum width alarm value            */
  float    out_of_spec_limit;       /* out of spec alarm limit on % of set  */
  unsigned short dr_joseph_ethernet_port;   /* ethernet port number of DR Joseph Ethernet Controoler, default = 502 */
  unsigned char dr_joseph_ip_address[4];    /* DR Joseph Ethernet Controller IP Address */
} config_WTH_device;

typedef struct
{
  unsigned int receive_time;
  unsigned int receive_start_time;
  unsigned int dump_time;
  unsigned int dump_start_time;
  unsigned int receive_secondary_time;
  unsigned int receive_secondary_start_time;
  unsigned int receive_both_time;
  unsigned int receive_both_start_time;
  char receiving_on;
  char secondary_receiving_on;
  char both_receiving_on;
  char dumping_on;
  char prox_on;               /* set if relay_a is on                 */
  char out_relay_on;          /* set if relay of output value is on   */
} shared_REC_device;

typedef struct
{
  unsigned int last_time_pump;  /* prev time reading for pumping */
  unsigned int pump_dead_start_time;
  char dc_pump_on;                /* set if the relay of dc_pump is on     */
  char cont_run_on;
  char dust_a_on;
  char dust_b_on;
  char working;                 /* set if pump is not continous running */
  char pump_resting;            /* set if pump has been cont running beyond 5 mins */
  char pump_fail;               /* set if no anx contact signal        */
  char noreceiver_ready;
  char anx_cont_on;             /* set if the anxiliary contact is on  */
  char shut_down;
  char restarting;
  char DC_cleaning;             /* dusct collector is cleaning */
} shared_PUMP_device;

typedef struct                  /* shared_LIW_device                    */
{
  char         oper_mode;       /* mode (auto,man,...) loop in          */
  char         ramping;         /* flag set when ramping to speed       */
  char         speed_ctrl;      /* source of speed ref. (internal/external) */
  char         loading;         /* flag set when loading                */
  char         ext_manual_backup;   /* hand manual backup or BR control */
  char         scrap_mode;      /* flag used by liw_flf                 */
  unsigned char coasts;         /* flag set when coasting,holds count   */
  unsigned char coast;          /* of coasts                            */
  unsigned char drive_fail_count;/* counter for drive system fail test  */
  char         have_new_act;    /* cleared when get new command         */
                                /* set when get new rate                */
  unsigned int alarm_bits;      /* bit-mapped array details error       */
  unsigned int alarm_latch_bits;/* latched alarm bits                   */
  unsigned int alarm_force_bits;/* used for testing alarms        */
  int wt_bits;                  /* holds the curr A/D reading           */
  unsigned int time_bits;       /* holds the curr time reading          */
  int          last_wt_bits;    /* prev wt reading                      */
  int          last_poke_bits;
  unsigned int last_time_bits;  /* prev time reading                    */
  float        set_spd;         /* holds set motor speed (percent)      */
  float        act_spd;         /* current actual speed                 */
  float        set_spd_rpm;     /* holds set motor speed (rpm)          */
  float        act_spd_rpm;     /* current actual speed  (rpm)          */
  int          act_spd_bits;    /* current actual speed                 */
  float        accel;           /* current accel value                  */
  float        decel;           /* current decel value                  */
  float        hop_wt;          /* weight of the hopper                 */
  float        last_hop_wt;     /* prev hopper wt                       */
  float        last_hop_hold_wt;     /* prev hopper wt hold             */
  float        set_wtp;         /* set WTP for this element             */
  float        act_wtp;         /* actual (average) WTP                 */
  float        newrate;         /* instantaneous measured WTP           */
  float        add_set_wtp;     /* sum of additives wtp for rate calc   */
  float        add_act_wtp;     /* sum of additives wtp for rate calc   */
  int          delta_b;         /* number of bits since last update     */
  unsigned int delta_t;         /* elapsed time since last update       */
  int  curbits;         /* current # of bits since last read    */
  unsigned int curtime;         /* current # of time bits "   "   "     */
  unsigned     spd_bits;        /* actual D/A bits sent                 */
  int          diff_bits;       /* difference between expected & got    */
  float        prod_inven;      /* used to store inven wt if gravilink  */
  blender_points blender_data;  /* used to store values from a blender if gravilink */
  /* the blender_points structure ends on even address but not on 4 byte boundary */
  char         clear_shift;     /* flag to signal shift totals should be cleared */
  char         clear_inven;     /* flag to signal inventory totals should be cleared */
  char         not_wt_stable;   /* TRUE if weight is not stable         */
  unsigned int wt_stable_count;
  int          MinAD;           /* minimum adc value, in diagnostics    */
  int          MaxAD;           /* maximum adc value, in diagnostics    */
  unsigned int  cal_counter;
  float         demo_extruder_percent_speed; /* percent speed if in gravilink */
  float         last_inven_wt;
  float         last_shift_wt;
  int           raw_wt_bits;              /* holds the curr A/D reading           */
  float         delta_wt;                 /* weight difference with last reading  */
  char          reset_wm;        /* flags to issue reset functions to smartcards */
  char          reset_drive;
  char          wt_stable;              /* TRUE if second weight stable         */
  char          adscan_alive;             /* set true by adscan on every reading  */

  char          adscan_reinitialize;      /* set this flag to reinitialize adscan */
  char          zero_scale_calibration;     /* set this flag to perform zero-scale calibration */
  char          rate_uses_spd;   /* flag to use rate speed for rate      */
  char          last_cal_reason;          /* character to stuff last recal reason */
  unsigned int  unstable_wt_timer;        /* # of ticks wt has been unstable for  */
  unsigned int  unstable_wt_counter;      /* counter to mark specific restarting  */
  unsigned int  queue_underflows;         /* number of queue underflows in a/d    */
  float         demo_cal_test_wt;         /* value of test wt to add, 0.0 for no wt   */
  float         record_wt;
  float         record_period_wt;
  float         record_delta_wt;
  unsigned int  record_time;
  unsigned int  record_period_time;
  unsigned int  record_delta_time;
  unsigned int  glink_time_bits;
  unsigned int  glink_delta_time;
  char          remote_cal_zero_done;
  char          remote_cal_all_done;
  int           remote_calibration_status;
  char          prod_code[17];   /* used to store prod code if gravilink */
  char          temp_prod_code[17];   /* used to store prod code if gravilink */
  char          demo_calibrating;         /* flag used for demoing that we are cal    */
  char          spd_not_percent_visible; 
  unsigned char init_man_extspd;
  PointInRateSpeedTable  stSpeed_vs_Rate[101];
} shared_LIW_device;

typedef struct                  /* shared_GATE_device                    */
{
  char          oper_mode;              /* mode (auto,man,...) loop in          */
  char          running_mode;
  char          feed_state;             /* set to internal feeder states        */
  char          gate_done;              /* indicates gate is done feeding       */
  char          clear_single_shot_error;/* flag for single shot algorithm       */
  char          clear_gate_cal;         /* flag to clear the gate cal           */
  char          retuning;               /* set if in autotune cycle             */
  char          last_recal_cause;       /* code for last cause of recal         */
  unsigned short num_feeds;             /* number of feeds for current dispense */
  unsigned short num_old_feeds;         /* number of feeds not including new feeds */
  unsigned int  alarm_bits;             /* bit-mapped array details error       */
  unsigned int  alarm_latch_bits;       /* latched alarm bits                   */
  unsigned int  alarm_force_bits;       /* used for testing alarms              */

  //G2-707
  unsigned int  oldRegrindOOM;          /* used to signify regrind OOM but is   */
                                        /* nolonger in receipe, but still OOM   */
  unsigned int  settling_timer;
  float set_wt;
  float set_wt2;
  float set_tol;
  float set_tol2;
  float act_wt;
  float last_act_wt;                    /* for keeping track of error        */
  float rem_wt;                         /* remaining wt                      */
  float rem_wt2;
  float sum_error_wt;                   /* error wt for integration term     */
  float last_set_wt;                    /* for keeping track of error        */
  unsigned int curr_gate_time;          /* current gate pulse time in mS GTF */
  unsigned int  num_self_recals;        /* number of self recalibrates       */
  /* variables used for demo mode */
  float         demo_wt;                /* amount of weight fed since last get_weight in demo mode */
  float         last_inven_wt;
  float         last_shift_wt;
  float         last_inven_percent;
  float         last_shift_percent;
  shared_REC_device sreceiver;
  int           ma_batch_time_pts[10];
  int           ma_batch_time_count;
/* variables for zero crossing calc GTF*/
   int   zc_feed_cnt;
   unsigned int   zc_gate_time;       /* in mS GTF */
   float zc_min_wt;
   int   zero_calc_state;
   int   zero_calc_done;
   int   learn_mode;
	char  ucRegrindLowCovered;      /* REGRIND state for Low Prox  */
	char  ucRegrindMiddleCovered;     /* REGRIND state for High Prox  */
	char  ucRegrindHighCovered;     /* REGRIND state for High High Prox  */	
} shared_GATE_device;

typedef struct                  /* shared_BWH_device                    */
{
	char          oper_mode;                /* mode (auto,man,...) loop in          */
	char          running_mode;
	char          new_batch_flag;           /* flag to signal start of a batch      */
	char          batch_finished;           /* flag to signal current batch is ready*/
	
	char          recalc_batch_size_flag;           /* flag to signal to recalc the batch only for the first ingredient      */	
	float		     new_batch_size_after_first_gate;	/* after first gate recalculate the batch for all the other gates*/ 

	unsigned int  alarm_bits;               /* bit-mapped array details error       */
	unsigned int  alarm_latch_bits;         /* latched alarm bits                   */
	unsigned int  alarm_force_bits;         /* used for testing alarms        */
	int           wt_bits;                  /* holds the curr A/D reading           */
	int           last_wt_bits;             /* prev wt reading                      */
	float         hop_wt;                   /* weight of the hopper                 */
	float         last_hop_wt;              /* prev hopper wt                       */
	float         last_hop_hold_wt;			 /* hold value for remote comms */
	float         delta_wt;                 /* weight difference with last reading  */
	int           MinAD;                    /* minimum adc value, in diagnostics    */
	int           MaxAD;                    /* maximum adc value, in diagnostics    */
   int           DifAD; //drt test
	char          wt_stable;                /* TRUE if weight stable                */
	char          not_wt_stable;            /* TRUE if weight is not stable         */

	int           wt_stable_bits;           /* holds the curr A/D reading           */
	unsigned int  wt_stable_count;

	char          adscan_reinitialize;      /* set this flag to reinitialize adscan */
	char          last_cal_reason;          /* character to stuff last recal reason */

	DATE_AND_TIME unstable_wt_timer;        /* # of ticks wt has been unstable for  */
	unsigned int  unstable_wt_counter;      /* counter to mark specific restarting  */
	unsigned int  elapsed_unstable_time;    /*# of ticks since last stable reading  */
	/* events                               */
	unsigned int  cal_counter;              /* number of times it has recalibrated  */
	/* (bad calibrates)                     */
	unsigned int  queue_overflows;          /* number of queue overflows in a/d     */
	/* receive queue                        */
	unsigned int  queue_underflows;         /* number of queue underflows in a/d    */
	/* receive queue                        */
	int BWH_count_bad_calib;	
	int BWH_calib_cv;
	char BWH_self_check_flag;
	
	/* batch & blender throughput calculations */
	/* last_wtp = last_batch_size / last_batch_time                                      */
	float         curr_batch_time;          /* time to spent on this batch               */
	float         last_batch_size;          /* last complete batch size                  */
	float         last_batch_time;          /* time to complete the last batch in mS GTF */
	float         last_wtp;                 /* actual wtp of last batch (not averaged)GTF*/
	float         max_wtp;                  /* avg max wtp of bwh                        */
	float         current_batch_size;       /* current batch we are making, GTF          */
	ma_tp_f_struct max_wtp_rate;            /* 10 point MA of wtp readings GTF           */
	ma_tp_f_struct avg_wtp_rate;            /* 10 point MA of wtp readings GTF           */
	ma_tp_i_struct avg_batch_time;          /* 10 point MA of batch times time is in mS GTF */
	ma_tp_f_struct avg_batch_size;          /* 10 point MA of batch sizes GTF            */
	ma_tp_f_struct avg_wt_deviation;        /* 10 point MA of batch sizes                */
	unsigned int  num_batches;
	unsigned int  batch_target;

	/* moving point averages */
	struct batch_stat_str batch_stat[MAX_GATES];      /* stats for the curr batch   */
	struct batch_stat_str last_batch_stat[MAX_GATES]; /* stats for the last batch   */
	float  temp_percents[MAX_GATES];
	char          remote_batch_completed;   /* for setting or clearing the status bit of batch finish*/
	char       	hopper_gate_open;         /* set if weigh hopper gate open        */
	short         cur_gate;                 /* gate that is currently running       */
	char          demo_calibrating;         /* flag used for demoing that we are cal    */

	float         demo_cal_test_wt;         /* value of test wt to add, 0.0 for no wt   */
	int           remote_calibration_status;
	int           raw_wt_bits;               /* holds the curr A/D reading           */

	char          zero_scale_calibration;    /* set this flag to perform zero-scale calibration */
	char   batch_suspended;                    /* set when system suspended(stopped during batch */
	char   continue_batch;                     /* set when the batch is to be continued after a suspend*/
	char            remote_cal_zero_done;

	unsigned short  print_bwh_time;
	char            remote_cal_all_done;
} shared_BWH_device;


typedef struct                  /* shared_HO_device                     */
{
  char         oper_mode;       /* mode (auto,man,...) loop in          */
  char         ramping;         /* flag set when ramping to speed       */
  char         speed_ctrl;      /* source of speed ref. (internal/external) */
  unsigned char coast;          /* flag set when coasting               */
  unsigned char coasts;          /* flag set when coasting,holds count   */
  char         ho_manual_backup;   /* hand manual backup or BR control */

  char         rate_uses_spd;   /* flag to use rate speed for rate      */
  char         reset_drive;     /* flag to reset drive functions        */
  unsigned char drive_fail_count;/* counter for drive system fail test  */
  char         have_new_act;    /* cleared when get new command         */
                                /* set when get new rate                */
  int          act_spd_bits;    /* current actual speed                 */
  unsigned int alarm_bits;      /* bit-mapped array details error       */
  unsigned int alarm_latch_bits;/* latched alarm bits                   */
  unsigned int alarm_force_bits;/* used for testing alarms        */
  unsigned int pulse_bits;      /* holds the curr pulse count           */
  unsigned int time_bits;       /* holds the curr time reading          */
  unsigned int last_pulse_bits; /* prev pulses reading                  */
  unsigned int last_time_bits;  /* prev time reading                    */
  float        set_spd;         /* holds set motor speed (0.0 - 100.0 %)*/
  float        act_spd;         /* current actual speed                 */
  float        set_spd_rpm;         /* holds set motor speed (rpm)      */
  float        act_spd_rpm;         /* current actual speed  (rpm)      */
  float        accel;           /* current accel value                  */
  float        decel;           /* current decel value                  */
  float        set_ltp;         /* set LTP for this element             */
  float        act_ltp;         /* actual LTP for this element          */
  float        newrate;         /* instantaneous measured LTP           */
  unsigned int delta_b;         /* number of bits since last update     */
  unsigned int delta_t;         /* elapsed time since last update       */
  unsigned int curbits;         /* current # of bits since last read    */
  unsigned int curtime;         /* current # of time bits "   "   "     */
  unsigned short spd_bits;      /* actual D/A bits sent                 */
  int          diff_bits;       /* difference between expected & got    */
  float        demo_ltp;        /* current ltp in demo mode             */

  char         updated_flag;    /* used to coordinate updates and calculation of stretch factor */
  unsigned char GearNum;        /* current Gear number                  */
  char         clear_shift;     /* flag to signal shift totals should be cleared */
  char         clear_inven;     /* flag to signal inventory totals should be cleared */

  float       last_inven_len;
  float       last_shift_len;
  int         remote_calibration_status;
  
  char          spd_not_percent_visible; 
  char         remote_cal_zero_done;
  char         remote_cal_all_done;
  unsigned char init_man_extspd;
  float         act_thk;         /* current actual thickness                */
  float         act_wpa;  
  float         act_wpl;  
} shared_HO_device;


typedef struct                  /* shared_WTH_device                    */
{
  unsigned int alarm_bits;      /* bit-mapped array details error       */
  unsigned int alarm_latch_bits;/* latched alarm bits                   */
  unsigned int alarm_force_bits;/* used for testing alarms        */
  float        set_wth;         /* set width                            */
  float        act_wth;         /* actual width                         */
  unsigned int good_msgs_in;    /* # of good messages received          */
  unsigned int good_msgs_out;   /* # of good messages sent              */
  unsigned int timeouts;        /* # of timeouts                        */
  unsigned int bad_msgs;        /* # of bad messages received          */

  char         oper_mode;       /* mode (auto,man,...) loop in          */
} shared_WTH_device;


typedef struct                  /* shared_MIXER_device                  */
{
  char oper_mode;                       /* mode (auto,man,...) loop in  */
  char mixer_mode;                      /* internal state machine state */
  char mix_done;                        /* set if batch waiting in mixer*/
  char extruder_needs_material;         /* set if extruder has req      */
                                        /* material from mixer          */
  char mixer_needs_material;            /* set if batch hopper material */

  float MatReqSwitchUncoveredTimerStart;    /* Tim Peyton special alarm timer */
  float MatReqSwitchUncoveredTimerElap;     /* Tim Peyton special alarm timer */

  char batch_received;                  /* set by bwh, cleared by mixer  */
  char starting_to_dump;                /* set when first starting to dump mixer */
  char mixer_low_level;                 /* mixer low level prox sw       */
  int  remote_material_req;   		    /* hardware io test - not used   */	
  char mixer_motor_on;                  /* set if mixer motor is on      */
  char mixer_starter_aux_contact;       /* set if the aux contact closed */
  char mixer_prox_switch;               /* set if prox covered           */

  unsigned int alarm_bits;              /* bit-mapped array details error*/
  unsigned int alarm_latch_bits;        /* latched alarm bits            */
  unsigned int alarm_force_bits;        /* used for testing alarms       */
  unsigned int total_mix_time;          /* total mix time for this batch */
  unsigned int mix_elap_time;           /* elap mix time for this batch  */

  /* variables for bwh_pcc to communicate with mixer when in demo mode   */
  float demo_wt_pending;
  char sim_low_level;
  char sim_interlock;
  char sim_cal_flag;
  //char bypass_mixer_prox;
} shared_MIXER_device;


typedef struct                  /* network stats by net addr            */
{
  /* network performance statistics                                     */
  unsigned int dead_timer;      /* timer for periodic checks of bad addr*/
  unsigned int good_msgs;       /* # of good messages                   */
  unsigned int timeouts;        /* # of timeouts                        */

  char         alive;           /* flag meaning is currently alive      */
  /* board information                                                  */
  unsigned char drive_type;     /* type of drive board                  */
  char date_of_manuf[10];       /* 8 character date of manufacture      */

  unsigned char hardware_id;    /* hardware id - board type             */
  unsigned char hardware_rev;   /* hardware revision level              */
  unsigned char software_rev;   /* software revision level              */
  unsigned char num_weigh;      /* # of weigh inputs installed          */

  unsigned char num_digital;    /* # of digital i/o's possible          */
  unsigned char num_analog_in;  /* # of analog inputs installed         */
  unsigned char num_analog_out; /* # of analog outputs possible         */
  unsigned char num_pulse;      /* # of high res pulse pickups          */

  unsigned int  timebase;       /* # of ticks/sec on board              */
  unsigned int cal_counter;

} net_stats;

typedef struct
{
  unsigned char dest;
  unsigned char notdest;
  unsigned char source;
  unsigned char notsource;

  unsigned char bytecount;
  unsigned char data[255];
} net_packet;

typedef struct                   /* global to all processes              */
{
   unsigned int  CurrentPage;    /* value used globally to determine what page */
                                 /* is currently the active page.        */

   unsigned int  dbg_what;       /* bit defined word on what debugging   */
   unsigned int  dbg_who;        /* bit defined word on who debugging    */
                                 /* number corresponds w/ menu numbers   */
   unsigned short opengate_relay;/* relay number that opengate should    */
                                 /* open                                 */
   unsigned short opengate_time; /* amount of time to pulse relay for    */
                                 /* units are 1/COUNTSPERSEC             */

   char opengate_done;           /* flag set by opengate every time a    */
                                 /* gate is closed to account            */
                                 /* for missed signals                   */
   char ack_all_alarms;          /* flag to ack all alarms               */
   char halt_alarm_processing;   /* flag to say stop alarm scanning      */
   char system_restart;          /* setup variable change req's restart  */

   char bln_oper_mode;           /* current blender mode                 */
   char ext_ho_oper_mode;        /* current extruder/haul-off mode       */
   char  man_valid;              /* flag to say man speeds are valid     */
   unsigned char NewGearNum;     /* Gear # used to select spd factor     */

   net_stats stats[MAX_ADDR];    /* net stats by node addr               */

   float man_ratio;              /* manual ratio last set                */
   float ho_spd;                 /* displayed manual setpoint storage    */
   float ho_spd_rpm;                 
   float width;                  /* "                                    */
   float liw_spd[MAX_LOOP];      /* displayed ext & add setpoint storage */
   float liw_spd_rpm[MAX_LOOP];  /* displayed ext & add setpoint storage in rpm */

   float sys_shift_wt;          /* system total of shift wts            */
   float last_sys_shift_wt;     /* saved value of shift wt when last cleared */
   float sys_inven_wt;          /* system total of inventory wts        */
   float last_sys_inven_wt;     /* saved value of inventary wt when last cleared */

   float ext_shift_wt;          /* shift wts for all extruders          */
   float ext_inven_wt;          /* inven wts for all extruders          */
   float recipe_inven_wt;       /* accumulator for recipe totals        */

   char sw_version[10];
   char sw_version_date[16];
   char machine[20];             /* machine text                         */
   char printeravail;            /* flag for printer is ok to print      */
   char printfunc;               /* value to determine what to print     */

   unsigned int alarm_bits;      /* global alarm conditions              */
   unsigned int alarm_latch_bits;
   unsigned int alarm_force_bits;/* used for testing alarms        */

   int invalid_remote_recipe;    /* non zero if remote recipe is invalid */
                                /* equal to retval of validate_recipe   */
   unsigned int proc_deaths;     /* # of process deaths                  */
   unsigned int last_proc_status;

   char     last_proc_to_die[40];/* name of last process to die          */

   char print_active;            /* flag for print function active       */
   char material_request;        /* set if the material request input    */
                                 /* is active                            */
   char interlock_closed;        /* set if door & interlock A closed     */
   char bln_manual_backup;       /* set if blender is in manual backup   */
                                 /* = key_manual_backup | comp_manual_backup     */

   char use_remote_int;
   char key_manual_backup;       /* set if keyswitch is used to put into manual  */
   char comp_manual_backup;      /* set if computer is in manual backup  */
   char power_supply_ok;         /* set if +24V supply ok                */
                                 /* if not, then all other conditions    */
                                 /* cannot be determined                 */

   char pause_pending;           /* set if pausing at the end of current batch */
   char purge_pending;           /* set if purging at the end of current batch */
   char new_recipe;
   char in_remote;               /* flag to say we are in remote         */
   BOOL remote_build_TWT;	/* remote comms. Americhem*/

  int alternate_set_addr;       /* Gets address from alternate register set, value at R_ALTERNATE_SET_VALUE */
  int current_recipe_num;
  int weigh_module_temp;        /* temperature in degrees F of weigh mod*/
  struct port_diag_data_str rem_diag;
  unsigned int detected_options;/* options detected during startup      */
  unsigned int remote_status;   /* exit status of remotely called proc  */

  char remote_lockout;          /* ignore remote writes, force local mode */
  unsigned char exception_number;
  unsigned char waiting_for_update;
  unsigned char ProfibusState;         /* status flag for profibus  */

  shared_PUMP_device  spump;
  int loading_counter;

  char new_extruder_instant_rate;   /* TRUE each time this recomputed, FALSE when read remotely */
  char new_extruder_gravilink_rate; /* TRUE when new rate, FALSE when read by Gravilink */
  char forced_pause;                /* TRUE when run_stop relay is in stop position */
  char loading_changed;             /* TRUE if loading style changed     */

  int last_recipe_num;

  char recipe_changed;
  char begin_print_recp_summary;
  char print_recipe_change;
  char end_of_batch;

  char last_job_name[PRODUCT_CODE_LEN];

  char remote_run_recipe_changed;  /* flag for remote recipe changed status */
  char disable_remote_writes;      /* flag set if disabling remote writes in service */
  char manual_backup_state[50];    /* state for manual backup controlling remotely */

  unsigned int saved_raw_data[SAVED_AD_SIZE];
  unsigned int saved_raw_data_ext[SAVED_AD_SIZE];
  short iteration;
  short iteration_ext;

  char ok_to_run_signal_flag;
  char stop_using_button;           /* flag if stop by using relay or button */
  char turn_off_relay_output_once;
  char gate_index;

  char powder_clean;
  char verify_password[8];
  char password_validation;                 /* for indication input password is invalid*/
  unsigned char serial_number[6];

  char new_job_name[PRODUCT_CODE_LEN];

  unsigned char mac_address[6];
  char remote_system_restart;
  char unable_to_talk_weigh_mod;

  char rem_calib_zero_scale_done;
  char rem_calib_auto_tare_done;
  char current_alarm_num;                /* for remote alarm setting */
  char current_alarm_status;

  char backup_restore_pcmcia_block_num; /* PCMCIA block number */
  char backup_restore_pcmcia_block_n_ex;   /* if the block number is exist */
  char backup_restore_port_number;     /* 1--port 1; 3--port 3 */
  char backup_restore_port_type;    /* 1--RS485; 2--RS422; 3--RS232 */

  char backup_restore_baud_rate;    /* 0x0f--19200;0x0e--9600; etc. */
  char backup_restore_if_pcmcia_present; /* if PCMCIA card is present when backup/restore  through PCMCIA */
  char backup_restore_if_overwrite;    /* if overwrite existing block */
  char backup_restore_destination;     /* 1--PCMCIA; 2--Port */

  char backup_restore_what;            /* 1--CONFIG; 2--RECIPE */
  char backup_restore_done;
  char backup_restore_who;          /* 1--Backup; 2--Restore */
  char backup_restore_error;

  int  recipe_errors[MAX_LOOP + 1];
  int  remote_recipe_status;

  char print_report_option;
  char spawn_shell_port_1;
  char spawn_shell_port_3_type232;
  char spawn_shell_port_3_type422;

  int  config_relay_num;
  int num_recipe_inven;         /* number used by remote to scroll through different stored recipe inventories*/
  int num_resin_inven;         /* number used by remote to scroll through different stored resin inventories*/
  float target_grams;  /* for remote dispense test */
  float target_tol;    /* for remote dispense test */
  float target_time;   /* for remote dispense test */

  int  gate_to_test;  /* for remote dispense test */
  int  num_of_cycles;         /* for dispense test */
  int  cycle_of_testing;      /* for dispense test */
  float act_testing_wt;       /* for dispense test */

  char dispense_test_method;  /* for remote dispense test */
  char dispense_test_done;    /* for dispense test */
  char  spi_cs_asserted;      /* semaphore to control access to the spi chip select  */
  unsigned char com_port_test_done;   /* for remote hardware test */

  unsigned char com_port_test_all_passed;
  unsigned char io_test_done;      /* for remote hardware test */
  unsigned int com_port_open_fail;
  unsigned int io_test_fail_1;   /* hardware io test */

  char auto_repeat;    /* for remote dispense test  */
  char clear_gate_cal;  /* for remote dispense test */
  unsigned int io_test_fail_2;
  unsigned char if_had_sec_io_card;  /* used for remote test */

  unsigned char new_total_gates;
  unsigned char   Calib_ADError; /* flag register identifying problems with ad */
  unsigned char CalibrateDevice; /* the loop number for the device to calibrate */
  unsigned char   calibrating; /* flag register identifying problems with ad */
} global_struct;

typedef union     /* union for the different control loop configurations in the system */
{
  config_LIW_device LIW;
  config_BWH_device BWH;
  config_GATE_device GATE;
  config_HO_device HO;
  config_WTH_device WTH;
  config_MIXER_device MIX;
} config_loop;

typedef union    /* union for pointers which point to the different control loop configurations */
{
  config_LIW_device *LIW;
  config_BWH_device *BWH;
  config_GATE_device *GATE;
  config_HO_device *HO;
  config_WTH_device *WTH;
  config_MIXER_device *MIX;
} config_loop_ptr;

typedef union  /* union for the different shared data values of the loops */
{
    shared_BWH_device BWH;
    shared_GATE_device GATE;
    shared_LIW_device LIW;
    shared_HO_device HO;
    shared_WTH_device WTH;
    shared_MIXER_device MIX;
} shared_loop;

typedef union  /* union for the pointers to the different shared memory areas */
{
  shared_BWH_device *BWH;
  shared_GATE_device *GATE;
  shared_LIW_device *LIW;
  shared_HO_device *HO;
  shared_WTH_device *WTH;
  shared_MIXER_device *MIX;
} shared_loop_ptr;

typedef struct
{
   unsigned char _0[sizeof(config_loop_ptr)];
} config_ptr_grp;

typedef struct
{
   unsigned char _0[sizeof(shared_loop_ptr)];
} shared_ptr_grp;

struct gate_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
	float SetWeight;
	float Tolerance;
	float DispensingTime;
};

struct bwh_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
	char Mode;
	char PauseAtEndOfBatch;
	float SetWeight;
	float Tolerance;
};

struct mixer_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
	char Mode;
	char PauseAtEndOfBatch;
};

struct liw_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
   float SetWTP;
	float SetSpeed;
	float SetAccelDecel;
	float SetAccel;
   float SetDecel;
};

struct ho_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
   float SetLTP;
	float SetSpeed;
	float SetAccel;
	float SetAccelDecel;
   float SetDecel;
};

struct wth_command_struct
{
	unsigned char CommandPresent;
	unsigned char Command;
	float Wth;
};

struct drive_command_struct
{
  unsigned char CMDRdy;
  unsigned char CMDReturn;
  unsigned char IOStatus;
  unsigned char Status;
  unsigned char CMD;
  float AccelDecelValue;
  int SetSpeed;
  unsigned int Pulses;
  int RampTime;
  TIME Time;
};

typedef struct
{
   unsigned char active;         
   unsigned char ip[40];            // 1st parameter
   unsigned char user_name[40];     // 2nd
   unsigned char user_pass[40];     // 3rd
   unsigned char sender_name[40];   // 4th
   unsigned char receiver_name[40]; // 5th
} smtp_struct;
_GLOBAL smtp_struct smtp_info;

/* structures defining the different flags for turning on and off DEBUG print
 * statements 2008-11-25 JLR
 */
/* having a structure for each of the files that defines debug flags for each
 * of the functions in the file gives us an incredible granularity of control
 * over the debug information spewed out on the telnet connection.  However,
 * it's important to keep things simple too... each structure should contain
 * a "global" flag that will turn on the debug for all of the file
 */
typedef struct openGateDebugStruct
{
  BOOL global;
  BOOL init_opengate;
  BOOL cyclic_opengate;
} openGateDebugStructType;

typedef struct debugFlagStruct
{
	openGateDebugStructType openGate;
	BOOL extDrive;
	BOOL exdDrive;
	BOOL relay;
	BOOL timer;
	BOOL hoDrive;
	BOOL adScan;
	BOOL calAd;
	BOOL calBwh;
	BOOL abTcp;
	BOOL abCom;
	BOOL readFun;
	BOOL readVar;
	BOOL recFun;
	BOOL remVars;
	BOOL remLink;
	BOOL writeFun;
	BOOL writeVar;
	BOOL extLoop;
	BOOL plcExd;
	BOOL hoPcc;
	BOOL hoLoop;
	BOOL shoLoop;
	BOOL bwhPcc;
	BOOL gatePcc;
	BOOL gatePccFirst;
	BOOL gatePccCalcGateTime;
	BOOL gatePccStoreMeasurement;
	BOOL gatePccDeltaWeight;
	BOOL mixer;
	BOOL procMon;
	BOOL defaults;
	BOOL fileRw;
	BOOL alarm;
	BOOL super;
	BOOL vars;
	BOOL print;
	BOOL modbus;
	BOOL modCom;
	BOOL mbeNet;
	BOOL remFun;
	BOOL alarms;  /* NOTE!  different from alarm! */
   BOOL alarmsys;
	BOOL fileRonly;
	BOOL diagnostics;
	BOOL extMan;
	BOOL recipeRw;
	BOOL recCom;
	BOOL runExt;
	BOOL recipe;
	BOOL security;
	BOOL setup;
	BOOL totals;
	BOOL liwPcc;
	BOOL printFeedTable[MAX_GATES];
	BOOL batchstats;
	BOOL jeff;
	BOOL greg;
	BOOL dan; 
   BOOL drt;
	BOOL manualPgTest;
	INT numptPrev;
	INT numptCurr;
	INT numptNext;
	INT recipeIdx;
	INT dan0;
	INT ValidRemoteAutoTest;
	INT InvalidRemoteAutoTest;
	INT numberOfOverShoot;
	INT numberOfUnderShoot;
	BOOL batch_timing;
	BOOL wt_stable;
	BOOL dispense_test;
	BOOL time_weight_to_csv;
	BOOL ext_wtp_enable;
	INT calib_minute;
	INT calib_hour;
	INT calib_day;
	INT calib_month;
	INT calib_year;
	BOOL BWH_calibration_flag;
	INT BWH_calib_cv;
	INT BWH_count_bad_calib;
	BOOL BWH_self_check_flag;	 /* flag for self check BWH calibration	 */
	
} debugFlagStructType;

_GLOBAL debugFlagStructType debugFlags;

_GLOBAL config_ptr_grp cnf_LOOP[MAX_LOOP]; /* create a _GLOBAL pointer to the configuraiton data */
_GLOBAL shared_ptr_grp shr_LOOP[MAX_LOOP]; /* create a _GLOBAL pointer to the shared data */

_GLOBAL config_super cfg_super;
_GLOBAL global_struct shr_global;

/* define the memory map                                                */
_GLOBAL config_GATE_device cfgGATE[MAX_GATES];
_GLOBAL config_BWH_device cfgBWH;
_GLOBAL config_MIXER_device cfgMIXER;
_GLOBAL config_HO_device cfgHO;
_GLOBAL config_HO_device cfgSHO;
_GLOBAL config_LIW_device cfgEXT;
_GLOBAL config_LIW_device cfgGFEEDER;
_GLOBAL config_LIW_device cfgGFLUFF;
_GLOBAL config_WTH_device cfgWTH;

_GLOBAL shared_GATE_device shrGATE[MAX_GATES];
_GLOBAL shared_BWH_device shrBWH;
_GLOBAL shared_MIXER_device shrMIXER;
_GLOBAL shared_HO_device shrHO;
_GLOBAL shared_HO_device shrSHO;
_GLOBAL shared_LIW_device shrEXT;
_GLOBAL shared_LIW_device shrGFEEDER;
_GLOBAL shared_LIW_device shrGFLUFF;
_GLOBAL shared_WTH_device shrWTH;

_GLOBAL struct FileListStruct FileList[MAX_FILES];
_GLOBAL unsigned int HMI_RAWAD; /* To show rawad values in calibrate screen for different devices */
_GLOBAL struct gate_command_struct GateCommand[MAX_GATES];
_GLOBAL struct bwh_command_struct BwhCommand;
_GLOBAL struct mixer_command_struct MixerCommand;
_GLOBAL struct liw_command_struct LiwCommand[MAX_LIW];
_GLOBAL struct ho_command_struct HOCommand;
_GLOBAL struct ho_command_struct SHOCommand;
_GLOBAL struct wth_command_struct WTHCommand;
_GLOBAL struct drive_command_struct DriveCommand[MAX_DRIVES];

/* NOTE: the following two definitions should be set to the largest     */
/* data elements which are currently                                    */
#define CONFIG_LOOP_SIZE sizeof(config_loop)
#define SHARED_LOOP_SIZE sizeof(temp_shared)

#define SUP_SIZE        sizeof(config_super)
#define HO_SIZE         sizeof(config_HO_device)
#define LIW_SIZE        sizeof(config_LIW_device)
#define BWH_SIZE        sizeof(config_BWH_device)
#define CONFIG_SIZE     (SUP_SIZE+(CONFIG_LOOP_SIZE*MAX_LOOP))


#define GLOB_SIZE       sizeof(global)
#define HOS_SIZE        sizeof(shared_HO_device)
#define LIWS_SIZE       sizeof(shared_LIW_device)
#define BWHS_SIZE       sizeof(shared_BWH_device)
#define SHARED_SIZE     (GLOB_SIZE+(SHARED_LOOP_SIZE*MAX_LOOP))

#define NVM_SIZE        0x7f00        /* 32 K - 256 bytes             */
#define NVRAM_SIZE      0x100  /* add this to NVRAM_BEGIN in nv.h      */
#define RECIPE_SIZE     (NVM_SIZE-CONFIG_SIZE-1000)

/* moved from 3 different files BEB */
#define NORMAL_REG 0
#define DEFINED_DATA_REG 1
#define DEFINED_POINT_REG 2
#define BLOCK_DEF_START_REG 50

/* These are needed in Open_gate and displayed in gate_pcc GTF */
_GLOBAL unsigned int FiveMSCounter;
_GLOBAL unsigned int GateOpenTime;
/* used in cal_ad and reset elsewhere GTF */
_GLOBAL  int MA_AD_count;
/* used in opengate to record time of the gate GTF */
_GLOBAL unsigned int OPENGATE_timeopen;
_GLOBAL unsigned int OPENGATE_timeclose;

/* moved this from bhw_pcc so can reset when AUTO is pressed GTF */
_GLOBAL DATE_AND_TIME batch_start_time;

/* moved here to avoid file conflicts */
_GLOBAL int gPage;
_GLOBAL unsigned char gNewMaterial;

/* Licence key variables */
_GLOBAL char sLicensePart1[2];
_GLOBAL char sLicensePart2[9];
_GLOBAL char sLicensePart3[9];
_GLOBAL char sLicensePart4[14];

//
#define MAX_MODBUS_TCP_CLIENTS 8
#define MAX_ABTCP_CLIENTS      8

_GLOBAL UINT Abtcp_State[MAX_ABTCP_CLIENTS];
_GLOBAL UINT Mbenet_State[MAX_MODBUS_TCP_CLIENTS];

_GLOBAL unsigned long ulSendModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulSendErrorModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulRecvModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulRecvErrorModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulServerModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulServerErrorModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulOpenModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
_GLOBAL unsigned long ulOpenErrorModbusTcpIp[MAX_MODBUS_TCP_CLIENTS];
 
_GLOBAL unsigned long ulSendABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulSendErrorABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulRecvABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulRecvErrorABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulServerABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulServerErrorABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulOpenABTcpIp[MAX_ABTCP_CLIENTS];
_GLOBAL unsigned long ulOpenErrorABTcpIp[MAX_ABTCP_CLIENTS];

/* following has to do with WTP GTF */
_GLOBAL DATE_AND_TIME last_batch_start_time;
/* upper limit for learn mode GTF */
_GLOBAL int HMI_PG018_MAX_LEARN;
_GLOBAL int NEW_Recipe_after_batch;
//_GLOBAL unsigned char Flood_Feed_Timmer_Flag; //drt 03/28/16
_GLOBAL unsigned char hmi_pg005_controls[18];

#endif


