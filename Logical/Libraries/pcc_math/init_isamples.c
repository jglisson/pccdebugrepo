#include <stdio.h>
#include "pcc_math.h"
#include "math.h"

signed short init_isamples(isamples *s_data, signed short length, ordered_ipair *d)
{
    s_data->length = length;
    s_data->pts = d;
    s_data->num_taken = length;  /* force clear samples to zero out data */
    clear_isamples(s_data);
    return (0);
}


