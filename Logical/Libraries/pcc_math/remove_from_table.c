#include <math.h>
#include "pcc_math.h"

signed short remove_from_table(data_table *table,ordered_pair *data)
{
    int err = 0;
    int i;
    ordered_pair *d = table->data;

    for(i=0;i<table->current_length;++i)
    {
        if(fabs((float)(d[i].x - data->x)) < .00001 && fabs((float)(d[i].y - data->y)) < .00001)
        {
            err = remove_from_table_by_index(table,i);
            break;
        }
    }

    return err;
}


