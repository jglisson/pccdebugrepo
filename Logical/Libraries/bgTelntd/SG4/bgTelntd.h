/* Automation Studio generated header file */
/* Do not edit ! */
/* bgTelntd 0.00.0 */

#ifndef _BGTELNTD_
#define _BGTELNTD_
#ifdef __cplusplus
extern "C" 
{
#endif
#ifndef _bgTelntd_VERSION
#define _bgTelntd_VERSION 0.00.0
#endif

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#ifdef _SG4
		#include "brsystem.h"
		#include "AsTCP.h"
		#include "sys_lib.h"
		#include "asstring.h"
#endif
#ifdef _SG3
		#include "brsystem.h"
		#include "AsTCP.h"
		#include "sys_lib.h"
		#include "asstring.h"
#endif
#ifdef _SGC
		#include "brsystem.h"
		#include "AsTCP.h"
		#include "sys_lib.h"
		#include "asstring.h"
#endif




/* Prototyping of functions and function blocks */
_BUR_PUBLIC signed long bgTelnetdInit(unsigned long pInstance, signed long portNumber, unsigned long pRingBufferInstance);
_BUR_PUBLIC signed long bgTelnetdCyclic(unsigned long pInstance);


#ifdef __cplusplus
};
#endif
#endif /* _BGTELNTD_ */

