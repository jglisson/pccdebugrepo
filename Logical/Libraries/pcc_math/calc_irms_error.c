#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_irms_error(isamples *error_data)
{
    int error;
    int length;

    length = error_data->num_taken;
    if(length > error_data->length) length = error_data->length;
    error = calc_irms_error_p(error_data,length);

    return error;
}


