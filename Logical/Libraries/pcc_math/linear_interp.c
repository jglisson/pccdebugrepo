#include<stdio.h>
#include "math.h"
#include "pcc_math.h"

float linear_interp(register float x1,register float y1,float x2,register float y2,float x3)
{
	float y3;
    register float diff;

    diff = x2 - x1;

    if(fabs((double)diff) > .001)
    {
        y3 = (x3-x1)*(y2-y1)/diff + y1;                         /* DIVIDE */
    }
    else
    {
        y3 = y1;
    }

    return(y3);
}


