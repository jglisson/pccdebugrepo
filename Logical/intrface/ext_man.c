#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
#include <bur/plctypes.h>
#include <math.h>
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "libbgdbg.h"
#include "recipe.h"
#include "demo.h"

char gTrulyPaused;
int ghave_ratio;
float gman_ratio;
float gext_spd;
float gho_spd;
float grefeed_spd;
float gwidth;
float gHO_SetSpd;
float gHO_ActSpd;
float gWTH_Set;
float gWTH_Act;
float gEXT_Set;
float gEXT_Act;
float gRFD_Set;
float gRFD_Act;

/* globals for ramp sync */
float gext_ad;                 /* accel / decel factors        */
float grefeed_ad;              /* accel / decel factors        */
float gho_ad;
float gext_eta;                /* eta's using max accel/decel  */
float grefeed_eta;             /* eta's using max accel/decel  */
float gho_eta;
float gext_ns;                 /* new speeds from rs_avg       */
float grefeed_ns;              /* new speeds from rs_avg       */
float gho_ns;
float gmax_eta;                /* eta for system ramp          */

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/*extern clear_recipe(&shr_global.run_recipe,EXTRUDER_RECIPE);*/
extern void visible(unsigned char *Runtime,unsigned char state);
extern void process_mode_buttons();
extern void clear_recipe(recipe_struct *r, int what);

void init_manual();
void init_run_recipe(void);
void calc_max_ratio(void);
void calc_limit(void);
void set_manual();
void sync_manual_ramp(void);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;


/************************************************************************/
/*
   Function:  set_manual()

   Description:  This function will set the system parameters needed to
         process the manual speeds requested on the page.

   GOBAL:   Action_Input0 - change to the main page
            Action_Input1 - Change manual ratio
            Action_Input2 - Change the extruder speed
            Action_Input3 - Change the Haul off speed
            Action_Input4 - Change the width speed
                                                                        */
/************************************************************************/
void set_manual(void)
{
   config_super *cs = &cfg_super;
   unsigned char reset_liw_spd;	 /* flag to reset liw spd when swtiching from rpm to % */
	unsigned char reset_ho_spd;
   
   if (!cfg_super.spd_in_percent)
   {
      visible((unsigned char *)&shrEXT.spd_not_percent_visible, TRUE);
      cfgEXT.max_spd_rpm = cfgEXT.max_spd * cfgEXT.spd_factor;
      shrEXT.act_spd_rpm = shrEXT.act_spd * cfgEXT.spd_factor;
      shrEXT.set_spd_rpm = shrEXT.set_spd * cfgEXT.spd_factor;
      shr_global.liw_spd[0] = shr_global.liw_spd_rpm[0] / cfgEXT.spd_factor;
      reset_liw_spd = 1;
      
      //bgDbgInfo (&g_RingBuffer1, "set_manual", __func__, __LINE__,
      //    BG_DEBUG_LEVEL_DEBUG, " | max_spd=%f| max_spd_rpm=%f| act_spd=%f| act_spd_rpm=%f| set_spd=%f| set_spd_rpm=%f| \r\n",
      //       cfgEXT.max_spd, cfgEXT.max_spd_rpm, shrEXT.act_spd, shrEXT.act_spd_rpm, shrEXT.set_spd, shrEXT.set_spd_rpm);
      //  bgDbgInfo (&g_RingBuffer1, "set_manual", __func__, __LINE__,
      //    BG_DEBUG_LEVEL_DEBUG, " | liw_spd[0]=%f| liw_spd_rpm[0]=%f| spd_factor=%f| \r\n",
      //       shr_global.liw_spd[0], shr_global.liw_spd_rpm[0], cfgEXT.spd_factor );
   }
   else 
   {
      visible((unsigned char *)&shrEXT.spd_not_percent_visible, FALSE);
      if (reset_liw_spd == 1) 
      {
         shr_global.liw_spd[0] = 0;
         reset_liw_spd = 0;
      }
   }
   
	if (!cfg_super.spd_in_percent)
	{
		visible((unsigned char *)&shrHO.spd_not_percent_visible, TRUE);
		cfgHO.max_spd_rpm = cfgHO.max_spd * cfgHO.spd_factor[shrHO.GearNum];
		shrHO.act_spd_rpm = shrHO.act_spd * cfgHO.spd_factor[shrHO.GearNum];
		shrHO.set_spd_rpm = shrHO.set_spd * cfgHO.spd_factor[shrHO.GearNum];
		shr_global.ho_spd = shr_global.ho_spd_rpm / cfgHO.spd_factor[shrHO.GearNum];
		reset_ho_spd = 1;
	}
	else 
	{
		visible((unsigned char *)&shrHO.spd_not_percent_visible, FALSE);
		if (reset_ho_spd == 1) 
		{
			shr_global.ho_spd = 0;
			reset_ho_spd = 0;
		}
	}
	
   HMI.pg004_MaxExt = 100;
   HMI.pg004_MaxHO = 100;

   if (HMI.Action_Input0 == 3)
   {
      HMI.Action_Input0 = gcInActive;
      HMI.ChangePage = pg000_MainPage;
   }

   /* close/hide the popup, clear alarms before entering extruder manual */
   if (HMI.Action_Input0 == 1)
   {
      visible(&HMI.pg000_PopupClearAlarms, FALSE);
      HMI.Action_Input0 = gcInActive;
   }

   /* close/hide the popup, cannot go from pause to auto directly */
   if (HMI.Action_Input0 == 2)
   {
      visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
      HMI.Action_Input0 = gcInActive;
   }

   if (HMI.Action_Input1 == gcActive)
   {  /* ratio changed */
      HMI.Action_Input1 = gcInActive;
		
		HMI.Action_ExtruderMode = 2;
		process_mode_buttons();

      /* gman_ratio = shr_global.man_ratio; */
		
		if (shr_global.man_valid)
			gman_ratio = shr_global.man_ratio;
		else if (gTrulyPaused && cs->man_in_recipe)
			gman_ratio = 0.0;
		else
			gman_ratio = 1.0;

      sync_manual_ramp();

      if ((cs->ltp_mode == CONTROL) && cs->ho_in_ratio)
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MAN_MODE;
         HOCommand.SetSpeed = (float)(gman_ratio*gho_spd);
         HOCommand.SetAccelDecel = gho_ad;
      }

      LiwCommand[0].CommandPresent = TRUE;
      LiwCommand[0].Command = MAN_MODE;
      LiwCommand[0].SetSpeed = (float)(gman_ratio * gext_spd);
      LiwCommand[0].SetAccelDecel = gext_ad;

      calc_limit();
   }

   if (HMI.Action_Input2 == gcActive)
   {  /* extruder changed */
      HMI.Action_Input2 = gcInActive;
		
		HMI.Action_ExtruderMode = 2;
		process_mode_buttons();
		
      gext_spd = shr_global.liw_spd[0];
		
      if (ghave_ratio)
      {
         /* sync ramping here */
         LiwCommand[0].CommandPresent = TRUE;
         LiwCommand[0].Command = MAN_MODE;
         LiwCommand[0].SetSpeed = (float)(gman_ratio * gext_spd);
         LiwCommand[0].SetAccelDecel = gext_ad;

         if (cs->scrap && (cs->refeed == GRAVIFLUFF))
         {
            LiwCommand[1].CommandPresent = TRUE;
            LiwCommand[1].Command = MAN_MODE;
            LiwCommand[1].SetSpeed = (float)(gman_ratio * gext_spd * grefeed_spd);
            LiwCommand[1].SetAccelDecel = grefeed_ad;
         }
      }
      else
      {
         LiwCommand[0].CommandPresent = TRUE;
         LiwCommand[0].Command = MAN_MODE;
         LiwCommand[0].SetSpeed = gext_spd;
         LiwCommand[0].SetAccelDecel = gext_ad;
			
         if (cs->scrap && (cs->refeed == GRAVIFLUFF))
         {
            LiwCommand[1].CommandPresent = TRUE;
            LiwCommand[1].Command = MAN_MODE;
            LiwCommand[1].SetSpeed = (float)(gext_spd * grefeed_spd);
            LiwCommand[1].SetAccelDecel = grefeed_ad;
         }
      }
      calc_max_ratio();
      calc_limit();
   }

   if (HMI.Action_Input3 == gcActive)
   {  /* haul off changed */
      HMI.Action_Input3 = gcInActive;
		
		HMI.Action_ExtruderMode = 2;
		process_mode_buttons();

      gho_spd = shr_global.ho_spd;
		
      if (ghave_ratio && cs->ho_in_ratio)
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MAN_MODE;
         HOCommand.SetSpeed = (float)(gman_ratio * gho_spd);
         HOCommand.SetAccelDecel = 0.0;
         calc_max_ratio();
         calc_limit();
      }
      else
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MAN_MODE;
         HOCommand.SetSpeed = gho_spd;
         HOCommand.SetAccelDecel = 0.0;
      }
   }

   if (HMI.Action_Input4 == gcActive)
   {  /* gwidth changed */
      HMI.Action_Input4 = gcInActive;
		
		HMI.Action_ExtruderMode = 2;
		process_mode_buttons();

      gwidth = shr_global.width;
      WTHCommand.CommandPresent = TRUE;
      WTHCommand.Command = MAN_MODE;
      WTHCommand.Wth = gwidth;
   }
}
/************************************************************************/
/*  Function:  init_manual()                                            */
/*                                                                      */
/* Description:  initializes data areas & puts each control loop into   */
/*               manual mode.                                           */
/************************************************************************/
void init_manual()
{
   config_super *cs = &cfg_super;

   gTrulyPaused = (shr_global.liw_spd[0] < 0.003) && (shr_global.ho_spd < 0.003);
   shr_global.current_recipe_num = cs->current_temp_recipe_num;
   
   if (!cfg_super.spd_in_percent)
   {
      cfgEXT.max_spd_rpm = cfgEXT.max_spd * cfgEXT.spd_factor;
   }
	
	if (!cfg_super.spd_in_percent)
	{
		cfgHO.max_spd_rpm = cfgHO.max_spd * cfgHO.spd_factor[shrHO.GearNum];
	}

   #ifdef ALLEN_BRADLEY_DRIVE
   if (cfgEXT.drive_type == AB_PLC_DRIVE)
      shrEXT.init_man_extspd = 0 ;  /*  start to toggle plc live signal */
   
   if (cfgHO.drive_type == AB_PLC_DRIVE)
      shrHO.init_man_extspd = 0 ;  /*  start to toggle plc live signal */
   #endif
   
   init_run_recipe();

   if ((cs->wtp_mode == CONTROL) &&
       ((cs->refeed == GRAVIFLUFF) ||
       ((cs->ltp_mode == CONTROL) && cs->ho_in_ratio)))
   {  /* show ratio entry box */
      visible(&HMI.SetupRuntime1,TRUE);
      ghave_ratio = TRUE;
   }
   else
   {  /* hide ratio entry box */
      visible(&HMI.SetupRuntime1,FALSE);
      ghave_ratio = FALSE;
   }

   /* set master speed ratio */
   //if (shr_global.man_valid)
   //   gman_ratio = shr_global.man_ratio;
   //else if (gTrulyPaused && cs->man_in_recipe)
   //   gman_ratio = 0.0;
   //else
      gman_ratio = 1.0;

   if (cs->ltp_mode == CONTROL)
   {
      /* set initial HO speed */
      if (shrHO.speed_ctrl == INTERNAL_CTRL)
      {
         if (shr_global.man_valid)
         {
             gho_spd = shr_global.ho_spd; 
         }
         else if (gTrulyPaused && cs->man_in_recipe && cs->ho_in_ratio)
         {
            /* manual start speed */
            gho_spd = BATCH_RECIPE_NEW->ho_man_spd; 
		      /*	gho_spd = shr_global.ho_spd; */
         }
         else if ((gman_ratio > 0.0) && cs->ho_in_ratio)
         {
            /* set ho ratio based on act speed */
            gHO_ActSpd = shrHO.act_spd ;
            gHO_SetSpd = shrHO.set_spd ;
            if (gHO_SetSpd > 0.0)
            {
               if (fabs((gHO_SetSpd - gHO_ActSpd) / gHO_SetSpd) < 0.005)
                  gho_spd = gHO_SetSpd;
               else
                  gho_spd = gHO_ActSpd;
            }
            else
               gho_spd = gHO_ActSpd;
         }
         else
         {
            gHO_ActSpd = shrHO.act_spd;
            gHO_SetSpd = shrHO.set_spd;
            if (gHO_SetSpd > 0.0)
            {
               if (fabs((gHO_SetSpd - gHO_ActSpd) / gHO_SetSpd) < 0.005)
                  gho_spd = gHO_SetSpd;
               else
                  gho_spd = gHO_ActSpd;
            }
            else
               gho_spd = gHO_ActSpd;
	         gext_spd = shr_global.ho_spd* gman_ratio;
         }
      }
      else
      {
         /* speed is controlled externally, use actual speed */
         gho_spd = shr_global.ho_spd = (shrHO.act_spd / gman_ratio);
      }
   }

   if (cs->wth_mode == CONTROL)
   {
      if (shr_global.man_valid)
         gwidth = shr_global.width;
      else
      {
         gWTH_Set = shrWTH.set_wth;
         gWTH_Act = shrWTH.act_wth;
         if (gWTH_Set > 0.0)
         {
            if (fabs((gWTH_Set - gWTH_Act)/gWTH_Set) < 0.005)
               gwidth = gWTH_Set;
            else
               gwidth = gWTH_Act;
         }
         else
            gwidth = gWTH_Act;
      }
   }

   if (cs->wtp_mode == CONTROL)
   {
      if (shrEXT.speed_ctrl == INTERNAL_CTRL)
      {
         if (shr_global.man_valid)
            gext_spd = shr_global.liw_spd[0];
         else if (gTrulyPaused && cs->man_in_recipe)
            gext_spd = BATCH_RECIPE_NEW->ext_man_spd;
         else  /* invalid manual ratio - put in actuals                */
         {
            gEXT_Set = shrEXT.set_spd;
            gEXT_Act = shrEXT.act_spd;
            if (gEXT_Set > 0.0)
            {
               if (fabs((gEXT_Set - gEXT_Act)/gEXT_Set) < 0.005)
                  gext_spd = gEXT_Set;
               else
                  gext_spd = gEXT_Act;
            }
            else
               gext_spd = gEXT_Act;
	         gext_spd = shr_global.liw_spd[0] * gman_ratio;
         }
      }
      else
      {
         /* speed is controlled externally, use actual speed */
         gext_spd = shr_global.liw_spd[0] = (shrEXT.act_spd / gman_ratio);
      }

      if (cs->refeed == GRAVIFLUFF)
      {
         if (shrGFEEDER.speed_ctrl == INTERNAL_CTRL)
         {
            if (shr_global.man_valid)
               grefeed_spd = shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP];
            else if (gTrulyPaused && cs->man_in_recipe)
               grefeed_spd = BATCH_RECIPE_NEW->ext_man_spd; /* ? need to add refeed man speed in recipe */
            else          /* invalid manual ratio - put in actuals                */
            {
               gRFD_Act = shrGFEEDER.act_spd;
               gRFD_Set = shrGFEEDER.set_spd;

               if (gRFD_Set > 0.0)
               {
                  if (fabs(((double)gRFD_Set - (double)gRFD_Act) / (double)gRFD_Set) < 0.005)
                     grefeed_spd = gRFD_Set;
                  else
                     grefeed_spd = gRFD_Act;
               }
               else
                  grefeed_spd = gRFD_Act;
            }
         }
         else
         {
            /* speed is controlled externally, use actual speed */
            grefeed_spd = shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP] = (shrGFEEDER.act_spd / gman_ratio) * 100;
         }
      }
   }

/* open communication paths to device loops */

   /* synchronize ramping of all devices */
   sync_manual_ramp();

   /* send MANUAL  speed or put in MONITOR */
   if (cs->wtp_mode == CONTROL)
   {
      LiwCommand[0].CommandPresent = TRUE;
      LiwCommand[0].Command = MAN_MODE;
	   if (!cfgEXT.demo & DEMO_DRIVE_MOD)
         LiwCommand[0].SetSpeed = (float)(gext_spd * gman_ratio);
      LiwCommand[0].SetAccelDecel = gext_ad;

      if (cs->refeed == GRAVIFLUFF)
      {
         LiwCommand[1].CommandPresent = TRUE;
         LiwCommand[1].Command = MAN_MODE;
         LiwCommand[1].SetSpeed = (float)(gman_ratio * gext_spd * grefeed_spd);
         LiwCommand[1].SetAccelDecel = grefeed_ad;
      }
   }
   else if (cs->wtp_mode == MONITOR)
   {
      LiwCommand[0].CommandPresent = TRUE;
      LiwCommand[0].Command = MONITOR_MODE;
      LiwCommand[0].SetSpeed = 0.0;
      LiwCommand[0].SetAccelDecel = 0.0;
   }

   if (cs->ltp_mode == CONTROL)
   {
      if (!cs->ho_in_ratio)
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MAN_MODE;
		   if (!cfgHO.demo & DEMO_DRIVE_MOD)
            HOCommand.SetSpeed = gho_spd;
         HOCommand.SetAccelDecel = gho_ad;
      }
      else
      {
         HOCommand.CommandPresent = TRUE;
         HOCommand.Command = MAN_MODE;
		   if (!cfgHO.demo & DEMO_DRIVE_MOD)
            HOCommand.SetSpeed = (float)(gho_spd * gman_ratio);
         HOCommand.SetAccelDecel = gho_ad;
      }
   }
   else if (cs->ltp_mode == MONITOR)
   {
      HOCommand.CommandPresent = TRUE;
      HOCommand.Command = MONITOR_MODE;
      HOCommand.SetSpeed = 0.0;
      HOCommand.SetAccelDecel = 0.0;
   }

   if (cs->sltp_mode == MONITOR)
   {
      SHOCommand.CommandPresent = TRUE;
      SHOCommand.Command = MONITOR_MODE;
      SHOCommand.SetSpeed = 0.0;
      SHOCommand.SetAccelDecel = 0.0;
   }

   if (cs->wth_mode == CONTROL)
   {
      WTHCommand.CommandPresent = TRUE;
      WTHCommand.Command = MAN_MODE;
      WTHCommand.Wth = gwidth;
   }
   else if (cs->wth_mode == MONITOR)
   {
      WTHCommand.CommandPresent = TRUE;
      WTHCommand.Command = MONITOR_MODE;
      WTHCommand.Wth = 0;
   }

   EXT_HO_OPER_MODE = MAN_MODE;

   calc_max_ratio();

   calc_limit();

   shr_global.width = gwidth;
   //shr_global.ho_spd = gho_spd;
   //shr_global.liw_spd[0] = gext_spd;
   shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP] = grefeed_spd;

   shr_global.man_ratio = gman_ratio;
   shr_global.man_valid = TRUE;
}

/************************************************************************/
/*  Function   calc_max_ratio()                                         */
/*                                                                      */
/* Description:  calculates the maximum value that the ratio can have   */
/*               based upon the extruder & ho values & max spds         */
/************************************************************************/
void calc_max_ratio(void)
{
   config_super *cs = &cfg_super;
   float f;

   f = 0.0;
   if (cs->spd_in_percent)
   {
      HMI.pg004_MaxRatio = 100.0;
   }

   if (( cs->ltp_mode == CONTROL) && (cs->ho_in_ratio) && (gho_spd > 0.0))
      HMI.pg004_MaxRatio = cfgHO.max_spd * cfgHO.spd_factor[shrHO.GearNum] / gho_spd;
   else
      HMI.pg004_MaxRatio = 99999.0;

   if (gext_spd > 0.0)
      f = (cfgEXT.max_spd * cfgEXT.spd_factor) / gext_spd;
   else
      HMI.pg004_MaxRatio = 99999.0;
   if (f < HMI.pg004_MaxRatio)
      HMI.pg004_MaxRatio = f;

   if (cs->scrap && (cs->refeed == GRAVIFLUFF))
   {
      if ((gext_spd > 0.0) && (grefeed_spd > 0.0))
         f = (float)(100.0 * cfgGFLUFF.max_spd * cfgGFLUFF.spd_factor) / (grefeed_spd * gext_spd);
      else
         f = 99999.0;
      if (f < HMI.pg004_MaxRatio)
         HMI.pg004_MaxRatio = f;
   }
}

/************************************************************************/
/*  Function:  calc_limit()                                             */
/*                                                                      */
/* Description:  calculates the maximum values that extruders & ho can  */
/*               have based upon the current ratio & max spd values     */
/************************************************************************/
void calc_limit(void)
{
   config_super *cs = &cfg_super;
   float HoSpeedFactor;
//
//   if (gman_ratio > 0.0)
//      HMI.pg004_MaxExt = cfgEXT.max_spd * gman_ratio*100;
//   else
//      HMI.pg004_MaxExt = 100;
//      

   if (cs->refeed == GRAVIFLUFF)
   {
      if ((gman_ratio > 0.0) && (gext_spd > 0.0))
        HMI.pg004_MaxRefeed = (float)(cfgGFLUFF.max_spd * cfgGFLUFF.spd_factor * 100.0) / (gman_ratio * gext_spd);
      else
        HMI.pg004_MaxRefeed = cfgGFLUFF.max_spd * cfgGFLUFF.spd_factor / 100.0;
   }

   if (cs->ho_in_ratio)
   {
      HoSpeedFactor = cfgHO.spd_factor[shrHO.GearNum];

      if (gman_ratio > 0.0)
         HMI.pg004_MaxHO = (cfgHO.max_spd * HoSpeedFactor) /gman_ratio*100;
      else
         HMI.pg004_MaxHO = cfgHO.max_spd * HoSpeedFactor *100.0;
   }
   else
      HMI.pg004_MaxHO = cfgHO.max_spd * HoSpeedFactor / 100.0;
}

/************************************************************************/
/*  Function:  init_run_recipe()                                        */
/*                                                                      */
/* Description:  initialize running recipe values                       */
/************************************************************************/
void init_run_recipe(void)
{
   config_super *cs = &cfg_super;

  clear_recipe(BATCH_RECIPE_NOW,EXTRUDER_RECIPE); 
  if (cs->ltp_mode == UNUSED)
    shrHO.act_ltp = BATCH_RECIPE_NEW->ltp;
  if (cs->wth_mode == UNUSED)
    shrWTH.act_wth = BATCH_RECIPE_NEW->width;
}

/*
***************************************************************************

               Synchronize Manual Ramping

***************************************************************************
*/
void sync_manual_ramp(void)
{
   config_super *cs = &cfg_super;
   float f;
   float HoSpeedFactor;

   HoSpeedFactor = cfgHO.spd_factor[shrHO.GearNum];

   /* clear variables    */
   gho_eta = 0.0;
   gext_eta = 0.0;

   /* do accel/decel stuff for HO  */
   if (cs->ltp_mode == CONTROL && (shrHO.speed_ctrl == INTERNAL_CTRL))
   {
      gho_ns = shr_global.man_ratio * shr_global.ho_spd;
      if (shrHO.set_spd > gho_ns)              /* decel        */
      {
         if (cs->decel < cfgHO.decel)
            gho_ad = cs->decel;
         else
            gho_ad = cfgHO.decel;
      }
      else                                      /* accel        */
      {
         if (cs->accel < cfgHO.accel)
            gho_ad = cs->accel;
         else
            gho_ad = cfgHO.accel;
      }

      if ((f=gho_ad*HoSpeedFactor) > 0.0)
         gho_eta = (float)fabs((gho_ns-shrHO.set_spd)/f);
      else
         gho_eta = 0.0;
   }

   /* search for fastest allowable accel/decel factors for extruders   */
   if (shrEXT.speed_ctrl == INTERNAL_CTRL)
   {
      gext_ns = shr_global.man_ratio * shr_global.liw_spd[0];

      if (shrEXT.set_spd > gext_ns)        /* decel        */
      {
         if (cs->decel < shrEXT.decel)
            gext_ad = cs->decel;
         else
            gext_ad = cfgEXT.decel;
      }
      else                                        /* accel        */
      {
         if (cs->accel < cfgEXT.accel)
            gext_ad = cs->accel;
         else
            gext_ad = cfgEXT.accel;
      }

      if ((f=gext_ad*cfgEXT.spd_factor) > 0.0)
         gext_eta = (float)fabs((gext_ns-shrEXT.set_spd)/f);
      else
         gext_eta = 0.0;
   }

   if (cs->refeed == GRAVIFLUFF)
   {
      if (shrGFLUFF.speed_ctrl == INTERNAL_CTRL)
      {
         grefeed_ns = shr_global.man_ratio * shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP] * shr_global.liw_spd[0];

         if (shrGFLUFF.set_spd > grefeed_ns)        /* decel        */
         {
            if (cs->decel < cfgGFLUFF.decel)
               grefeed_ad = cs->decel;
            else
               grefeed_ad = cfgGFLUFF.decel;
         }
         else                                        /* accel        */
         {
            if (cs->accel < cfgGFLUFF.accel)
               grefeed_ad = cs->accel;
            else
               grefeed_ad = cfgGFLUFF.accel;
         }

         if ((f=grefeed_ad*cfgGFLUFF.spd_factor) > 0.0)
            grefeed_eta = (float)fabs((grefeed_ns - shrGFLUFF.set_spd)/f);
         else
            grefeed_eta = 0.0;
      }
   }
   /* figure out eta's for controlling stuff  */
   gmax_eta = gho_eta;
   if (gext_eta > gmax_eta)
      gmax_eta = gext_eta;
   if (grefeed_eta > gmax_eta)
      gmax_eta = grefeed_eta;

   if (cs->wtp_mode == CONTROL)
   {
      if ((f=gmax_eta*cfgEXT.spd_factor) > 0.0)
         gext_ad = (float)fabs((gext_ns-shrEXT.set_spd)/f);
      else
         gext_ad = 0.0;

      if (cs->refeed == GRAVIFLUFF)
      {
         if ((f=gmax_eta*cfgGFLUFF.spd_factor) > 0.0)
            grefeed_ad = (float)fabs((grefeed_ns-shrGFLUFF.set_spd)/f);
         else
            grefeed_ad = 0.0;
      }
   }
   if (cs->ltp_mode == CONTROL)
   {
      if ((f=gmax_eta) > 0.0)
         gho_ad = (float)fabs((gho_ns-shrHO.set_spd)/f);
      else
         gho_ad = 0.0;
   }
}


