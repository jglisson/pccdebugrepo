#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short incr_isample_index(isamples *s_data,signed short idx)
{
    if(++idx >= s_data->length) idx = 0;
    return idx;
}


