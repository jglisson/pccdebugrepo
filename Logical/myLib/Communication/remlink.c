#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  remlink.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\remlink.c_v  $
 *
 * 2008/12/01 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.remLink to toggle debug information in this file
 *
 *    Rev 1.0   Feb 11 2008 11:01:06   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "mem.h"
#include "remote.h"
#include "register.h"
#include "libbgdbg.h"

#define FALSE 0

void set_liw_spd(int liw,float spd,float accel_decel);
void set_ho_spd(float spd, float accel_decel);
extern config_super *cs;
extern stored_recipe *rec_;

void null_function(void);
void debug_rem_write(point_data *pd, int point, fic *value, int type);
void debug_rem_read(point_data *pd, int point, fic *value, int type);
int match_point_to_debug();

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

int init_remote_subroutines(point_data *pd)
{
	config_super *cs_ = &cfg_super;
	global_struct *g_ = &shr_global;
    int i;

    pd->loop = 0;
    pd->dev_type = 0;
    pd->ext = 0;
    pd->hop = 0;
    pd->total = 0;
    pd->reg = 0;
#if ((MACH != WX_BATCH) && (MACH != WE_BATCH))
    pd->c_LIW = NULL;
    pd->s_LIW = NULL;
#endif
    pd->recipe_changed = 0;
    pd->cs = cs_;
    pd->g = g_;
    pd->next_recipe_num =0;
    pd->current_resin_idx = 0;
    pd->current_recipe_idx = 0;

    for (i=0; i<MAX_LOOP; i++)
    {
        pd->cnf_LOOP[i].LIW = ((config_loop_ptr*)(&cnf_LOOP[i]))->LIW;
        pd->shr_LOOP[i].LIW = ((shared_loop_ptr*)(&cnf_LOOP[i]))->LIW;
        pd->mode_button_stick[i] = FALSE;
        pd->mode_button_value[i] = 0;
    }

    return 0;
}

/************************************************************************/
/* null_function()                                                      */
/************************************************************************/
void null_function(void)
{
/*     dprintf("RAC","null function\n"); */
}

void debug_rem_read(point_data *pd, int point, fic *value, int type)
{
#if 0
   global *g = &shr_global;
#endif
   config_super *cs = &cfg_super;
   int reg, device;
   int use_ucb = FALSE;
   int ucb_num = 0;
   int ucb_reg = 0;
   int ucb_pt = 0;
   int num_regs;

   if (debugFlags.remLink)
   {
      if (cs->use_pcc2_address)
         num_regs = PCC2_TYPES_PER;
      else
         num_regs = TYPES_PER;

      reg = point % num_regs;
      if (match_point_to_debug(pd,reg))
      {
         if (cs->use_pcc2_address)
         {
            device = point / PCC2_DEVICE_OFFSET;
            if ((device == REM_UCB_DEF_ID) || (device == REM_UCB_DATA_ID))
            {
               ucb_num = (point % PCC2_TYPES_PER) / UCB_SIZE;
               ucb_reg = (point % PCC2_TYPES_PER) % UCB_SIZE;
               ucb_pt = cs->user_def_blk[ucb_num].points[ucb_reg];
               use_ucb = TRUE;
            }
         }
         else if(point >= USER_BLK_START_REG && point < USER_BLK_START_REG+1000)
         {
            ucb_num = (point - USER_BLK_START_REG) / 100;
            ucb_reg = ((point - USER_BLK_START_REG) % 50);
            ucb_pt = cs->user_def_blk[ucb_num].points[ucb_reg];
            use_ucb = TRUE;
         }
 #if 0
         switch(type)
         {
           case FLOAT:
             if (use_ucb)
             {
               bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "point=%d (ucb%d reg%d,%d) = %f\r\n",
                   point, ucb_num + 1, ucb_reg + 1, ucb_pt, value->f);
             }
             else
             {
               bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "point=%d = %f\r\n",
                   point, value->f);
             }
            break;
            case INT:
               if (use_ucb)
               {
                  bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                      BG_DEBUG_LEVEL_DEBUG, "point=%d(ucb%d reg%d,%d) = %d\r\n",
                      point, ucb_num + 1, ucb_reg + 1, ucb_pt, value->i);
               }
               else
               {
                  bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                      BG_DEBUG_LEVEL_DEBUG, "point=%d = %d\r\n",
                      point, value->i);
               }
               break;
            case STRINGS:
               bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "point=%d = '%s'\r\n", point,
                   (char *) value->i);
               break;
         }
#endif
      }
   }
}

void debug_rem_write(point_data *pd, int point, fic *value, int type)
{
#if 0
   global *g = &shr_global;
#endif
   config_super *cs = &cfg_super;
   int reg, device;
   int use_ucb = FALSE;
   int ucb_num = 0;
   int ucb_reg = 0;
   int ucb_pt = 0;
   int num_regs;

   if (debugFlags.remLink)
   {
      if (cs->use_pcc2_address)
         num_regs = PCC2_TYPES_PER;
      else
         num_regs = TYPES_PER;

      reg = point % num_regs;
      if (match_point_to_debug(pd,reg))
      {
         if (cs->use_pcc2_address)
         {
            device = point / PCC2_DEVICE_OFFSET;
            if ((device == REM_UCB_DEF_ID) || (device == REM_UCB_DATA_ID))
            {
               ucb_num = (point % PCC2_TYPES_PER) / UCB_SIZE;
               ucb_reg = (point % PCC2_TYPES_PER) % UCB_SIZE;
               ucb_pt = cs->user_def_blk[ucb_num].points[ucb_reg];
               use_ucb = TRUE;
            }
         }
         else if(point >= USER_BLK_START_REG && point < USER_BLK_START_REG+1000)
         {
            ucb_num = (point - USER_BLK_START_REG) / 100;
            ucb_reg = ((point - USER_BLK_START_REG) % 50);
            ucb_pt = cs->user_def_blk[ucb_num].points[ucb_reg];
            use_ucb = TRUE;
         }
#if 0
         switch(type)
         {
            case FLOAT:
               if (use_ucb)
               {
                 bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                     BG_DEBUG_LEVEL_DEBUG, "point=%d(ucb%d reg%d,%d) = %f\r\n",
                     point, ucb_num + 1, ucb_reg + 1, ucb_pt, value->f);
               }
               else
               {
                  bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                      BG_DEBUG_LEVEL_DEBUG, "point=%d = %f\r\n", point,
                      value->f);
               }
               break;
           case INT:
               if (use_ucb)
               {
                  bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                      BG_DEBUG_LEVEL_DEBUG, "point=%d(ucb%d reg%d,%d) = %d\r\n",
                      point, ucb_num + 1, ucb_reg + 1, ucb_pt, value->i);
               }
               else
               {
                  bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                      BG_DEBUG_LEVEL_DEBUG, "point=%d = %d\r\n",
                      point, value->i);
               }
             break;
           case STRINGS:
             bgDbgInfo (&g_RingBuffer1, "remlink.c", __func__, __LINE__,
                 BG_DEBUG_LEVEL_DEBUG, "point=%d = '%s'\n", point,
                 (char *) value->i);
             break;
         }
#endif
      }
   }
}


int match_point_to_debug(point_data *pd, int reg)
{
#if 0
   register int retcode = FALSE;
   register global *g = g_;

   if((reg >= (shr_global.rem_diag.dbg_remote_low_pt)) && (reg <= (shr_global.rem_diag.dbg_remote_high_pt)))
   {
      if (pd->dev_type == TYPE_SYSTEM)
      {
         if(shr_global.dbg_who & (1<<MAX_LOOP))
            retcode = TRUE;
      }
      else
      {
         if(pd->loop <= MAX_LOOP)
         {
            if(shr_global.dbg_who & (1<<pd->loop))
               retcode = TRUE;
         }
      }
   }
   return retcode;
#endif
	return 0;
}


void set_liw_spd(int liw, float spd, float accel_decel)
{
	switch (liw)
	{
		case EXT_LOOP:
		case GRAVIFLUFF_FDR_LOOP:
			LiwCommand[0].CommandPresent = TRUE;
			LiwCommand[0].Command = (spd > 0.001) ? MAN_MODE : PAUSE_MODE;
			LiwCommand[0].SetSpeed = spd;
			LiwCommand[0].SetAccelDecel = accel_decel;
		default:
			break;
	}
}

void set_ho_spd(float spd, float accel_decel)
{
	HOCommand.CommandPresent = TRUE;
	HOCommand.Command = (spd > 0.001) ? MAN_MODE : PAUSE_MODE;
	HOCommand.SetSpeed = spd;
	HOCommand.SetAccelDecel = accel_decel;
}


