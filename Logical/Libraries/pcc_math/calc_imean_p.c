#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_imean_p(isamples *sample_data, signed short length)
{
    int i;
    int mean = 0;
    ordered_ipair *d;
    int idx;

    if(length > sample_data->length) length = sample_data->length;
    d = sample_data->pts;

    idx = sample_data->next - length;
    if(idx < 0)
        idx += sample_data->length;

    mean = 0;
    for(i=0;i<length;++i)
    {
        mean = mean + d[idx].y;
        if(++idx >= sample_data->length)
            idx -= sample_data->length;
    }

    if(length > 0)
        mean = (int)((float)mean / (float)length + .5);     /* DIVIDE */
    else
        mean = 0;

    return mean;
}


