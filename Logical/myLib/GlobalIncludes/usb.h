/* //////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////// */
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <asusb.h>
#include <fileio.h>
#include <string.h>

/* //////////////////////////////////////////////////////////////////
// DEFINES
////////////////////////////////////////////////////////////////// */
//#define _REPLACE_CONST

#define OK    0
#define FALSE 0
#define TRUE  1


#define MAXUSBDEVICES      32
#define SANDISK_VENDOR_ID  0x0781
#define SANDISK_PRODUCT_ID 0x7105
#define SANDISK_BCD        0x1033

#define USB_START        0
#define USB_GETNODELIST  1
#define USB_SEARCHDEVICE 2
#define USB_DEVICELINK   3
#define USB_DEVICEUNLINK 4
#define USB_FILEACCESS   5
#define USB_DONE         15
#define USBBUSY          65535    /* function is still processing */

/* USB classes */
#define USB_HUB          9
#define USB_MASS_STORAGE 8
#define USB_PRINTER      7
#define USB_HID          3
/*usb sub classes */
#define USB_SUBCLASS_SCSI_COMSET 6
#define USB_SUBCLASS_UFI_COMSET  4
#define USB_SUBCLASS_PRINTER     1
#define USB_SUBCLASS_HIDBOOT     1
/* usb error codes were asusb codes*/
#define USB_OK            0
#define USB_NOTFOUND      32900
#define USB_BUFSIZE       32901
#define USB_NULLPOINTER   32902
#define USB_BUSY          65535
#define MSDEVICE          "MS_DEVICE"

/* //////////////////////////////////////////////////////////////////
// DECLARATION
////////////////////////////////////////////////////////////////// */
_LOCAL  UsbNodeListGet_typ UsbNodeListGetFub;
_LOCAL  UsbNodeGet_typ     UsbNodeGetFub;
_LOCAL  DevLink_typ        DevLinkFub;
_LOCAL  DevUnlink_typ      DevUnlinkFub;
_LOCAL  DirInfo_typ        DirInfoUsbDev;

_GLOBAL usbNode_typ        usbDevice;

/* function prototypes for functions defined in usb.c */
void usb_init(void);
int  usb_check(int *usbAction);


