#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  readfun.c

   Description: Serial Modbus routines.


      $Log:   F:\Software\BR_Guardian\readfun.c_v  $
 *
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.readFun to toggle on/off the debug information in this file
 *
 *    Rev 1.8   Sep 15 2008 13:18:08   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.7   May 28 2008 15:05:58   FMK
 * Got rid of compile warnings.
 *
 *    Rev 1.6   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.5   May 06 2008 13:18:06   YZS
 * Fixed response data errors.
 *
 *
 *    Rev 1.4   Apr 24 2008 14:50:12   YZS
 * Fixed an error in rv_act_ltp( ) for HAULOFF.
 *
 *    Rev 1.3   Apr 04 2008 11:36:28   vidya
 * Changed cs->temp_recipe to PMEM.RECIPE->
 *
 *    Rev 1.2   Apr 02 2008 15:06:08   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.1   Feb 29 2008 14:58:58   FMK
 * Removed printer from serial ports. So removed
 * references for remote tags.
 *
 *    Rev 1.0   Feb 11 2008 11:03:30   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <math.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "vars.h"
#include <plc.h>
#include "relaydef.h"
#include "register.h"
#include "remote.h"
#include "libbgdbg.h"
#include "recipe.h"

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

#define DEBUG_PORT 3

char get_recipe_1_menu(point_data * pd);
char get_recipe_2_menu(point_data * pd);
char get_recipe_3_menu(point_data * pd);

extern float read_recipe_inven(config_super *cs_, stored_recipe *rec_, int from);
extern int   read_recipe_desc(config_super *cs_, stored_recipe *rec_, int rnum);
extern int   rem_validate_recipe(point_data *pd, recipe_struct *r, int *validation_errors);

/****************************************************************************
 * Function: rv_special_devices
 ****************************************************************************/
int rv_special_devices(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int resin_num, recipe_num, resin_offset, recipe_offset, i;
	int retcode = OK;
	char null_str[5];
	char *recipe_name;

	switch (pd->dev_type)
	{
		case TYPE_RESIN_NAME:
			resin_num = (pd->reg - 1) / REGS_PER_STRING;
			resin_offset = ((pd->reg - 1) % REGS_PER_STRING) * 4;
			for (i = 0; i < 4; i++)
			{
				if (resin_offset + i < RESIN_LENGTH)
					v->c[i] = cs->resin_table[resin_num].resin_desc[resin_offset + i];
				else
					v->c[i] = 0;
			}
			*type = RESIN_STRING;
			break;
		case TYPE_RESIN_DATA:
			resin_num = (pd->reg - 1) / REGS_PER_STRING;
			resin_offset = ((pd->reg - 1) % REGS_PER_STRING);
			switch (resin_offset)
			{
				case R2_RESIN_DENSITY:
					v->f = cs->resin_table[resin_num].density;
					break;
				case R2_RESIN_INVEN_WT:
					v->f = (float) PMEM.resin_inven[resin_num];
					break;
			}

			*type = FLOAT;
			break;
		case TYPE_RECIPE_NAME:
			recipe_num = (pd->reg / REGS_PER_STRING) + 1;
			recipe_offset = (((pd->reg ) % REGS_PER_STRING)-1) * 4;
			if(cs->enable_product_code)
			{
				recipe_name = (char*)read_recipe_desc(cs,pd->rec,recipe_num);

				for (i = 0; i < 4; i++)
				{
					if (recipe_offset + i < 15)
						v->c[i] = *(recipe_name + recipe_offset + i);
					else
						v->c[i] = 0;
				}
			}
			else
				v->i = (unsigned int)&null_str[0];

			*type = RESIN_STRING;
			break;
		case TYPE_RECIPE_DATA:
			recipe_num = (pd->reg / REGS_PER_STRING) + 1;
			if(cs->enable_product_code)
			{
				v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,recipe_num);
			}
			else
				v->f = 0.0;

			*type = FLOAT;
			break;
		case TYPE_SYSTEM:
			break;

		default:
			v->i = 0;
			retcode = INVALID_POINT;
			break;
	}
	return (retcode);
}
/****************************************************************************
 * Function: rv_current_recipe_inven
 ****************************************************************************/
int rv_current_recipe_inven(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code)
	{
		if (cfg_super.units == METRIC)
			v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,shr_global.current_recipe_num) * 0.453592;
		else
			v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,shr_global.current_recipe_num);
	}
	else
		v->f = 0.0;

	*type = FLOAT;
	return (retcode);
}
/****************************************************************************
 * Function: rv_next_recipe_inven
 ****************************************************************************/
int rv_next_recipe_inven(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code)
	{
		if (cfg_super.units == METRIC)
			v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,cs->current_temp_recipe_num) * 0.453592;
		else
			v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,cs->current_temp_recipe_num);
	}
	else
		v->f = 0.0;

	*type = FLOAT;
	return (retcode);
}
/****************************************************************************/
/* rv_config()                                                              */
/****************************************************************************/
int rv_config(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	v->i = 0;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			if(cs->wtp_mode != UNUSED)
				v->i |= R_EXT;

			if (cs->ltp_mode != UNUSED)
			   v->i |= R_HO;

			if (cs->wth_mode != UNUSED)
				v->i |= R_WIDTH;

			if (cs->refeed)
				v->i |= R_SCRAP;

			if (cs->refeed == GRAVIFLUFF)
				v->i |= R_GRAVIFLUFF;

			break;

		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return retcode;
}
/****************************************************************************/
/****************************************************************************/
int rv_total_receivers(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->i = cs->total_receivers;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return retcode;
}

/****************************************************************************/
/****************************************************************************/
int rv_keep_alive_flag(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	if (cs->Keep_Alive_Toggle == 0)
		cs->Keep_Alive_Toggle = 1;
	else
		cs->Keep_Alive_Toggle = 0;
	
	v->i = cs->Keep_Alive_Toggle;
	*type = INT;
		return retcode;
}

/****************************************************************************/
/****************************************************************************/
/****************************************************************************/
/* rv_ho_gear_num()                                                         */
/****************************************************************************/
int rv_ho_gear_num(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	if ((cs->ltp_mode == CONTROL) &&
		(cfgHO.GearInputMethod != NO_GEAR_INPUT))
		v->i = shrHO.GearNum;  /* ho gear number */
	else
		v->i = 0;

	*type = INT;

		return OK;
}

/****************************************************************************/
/* rv_num_hop()                                                             */
/****************************************************************************/
int rv_num_hop(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned)(int)cs->total_gates;
	*type = INT;

		return OK;
}

/****************************************************************************/
/****************************************************************************/
int rv_set_mode(fic *v, point_data *pd, int *type)
{
	if (pd->mode_button_stick[MAX_LOOP])
	{
		/* if first read after a write then return the value written */
		v->i = pd->mode_button_value[MAX_LOOP];
		pd->mode_button_stick[MAX_LOOP] = FALSE;
	}
	else
	{
		/* if already read once then return 0 */
		v->i = 0;
	}
	*type = INT;
	return OK;
}

/****************************************************************************/
/****************************************************************************/
int rv_current_mode(fic *v, point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = 0;

	if(cs->wtp_mode != UNUSED)
	{
		switch(EXT_HO_OPER_MODE)
		{
			case PAUSE_MODE:
				v->i |= R_EXT_PAUSE;
				break;
			case MAN_MODE:
				v->i |= R_EXT_MANUAL;
				break;
			case AUTO_MODE:
				v->i |= R_EXT_AUTO;
				break;
		}
	}

	switch(BLN_OPER_MODE)
	{
		case PAUSE_MODE:
			if (shr_global.pause_pending)
				v->i |= R_BLN_PAUSE_PENDING;
			else
			   v->i |= R_BLN_PAUSE;
			break;
		case MAN_MODE:
			v->i |= R_BLN_MANUAL;
			break;
		case PURGE_MODE:
			v->i |= R_PURGE;
			break;
		case AUTO_MODE:
			v->i |= R_BLN_AUTO;
			break;
	}

	if (shr_global.pause_pending)
		v->i |= R_BLN_PAUSE_PENDING;
	
   // per Holger to use in Germany	
	if (cfgBWH.cal_done == 0 )
		v->i |= R_BLN_NOT_CALIBRATED;
	else
		v->i |= R_BLN_CALIBRATED;
	*type = INT;
	return OK;
}

/****************************************************************************/
/* rv_status()                                                              */
/****************************************************************************/
int rv_status(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;
	int loop;
	int recipe_errors[MAX_LOOP+1];

	v->i = 0;
	loop = pd->loop;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			shr_global.invalid_remote_recipe = (unsigned int)rem_validate_recipe(pd, &BATCH_RECIPE_EDIT, recipe_errors);
			if(shr_global.invalid_remote_recipe)
			   v->i |= R_BAD_RECIPE;
			if(shr_global.remote_run_recipe_changed)
			   v->i |= R_RUN_RECIPE_CHANGED;
			if(shr_global.remote_recipe_status & R_RECIPE_INVALID)
				v->i |= R_RECIPE_INVALID;
			if(shr_global.remote_recipe_status & R_RECIPE_VALID)
				v->i |= R_RECIPE_VALID;
			if(shrBWH.remote_batch_completed)
				v->i |= R_BATCH_COMPLETED;
			if(cs->job_wt_complete)
				v->i |= R_JOB_COMPLETED;
			if(shr_global.purge_pending)
				v->i |= R_PURGE_PENDING;

			if(shr_global.spump.anx_cont_on)
				v->i |= R_AUX_CONTACT;

			if(shr_global.spump.dc_pump_on)
				v->i |= W_PUMP;

			if(shr_global.spump.cont_run_on)
				v->i |= W_CONT_RUN;

			if(shr_global.spump.dust_a_on)
				v->i |= W_DUST_COLL_A;


			if(shr_global.spump.dust_b_on)
				v->i |= W_DUST_COLL_B;

			if (shr_global.spump.pump_fail)
				v->i |= R_PUMP_FAIL;

			//         if (shr_global.spump.pump_resting)
			//            v->i = R_PUMP_RESTING;

			if (shr_global.bln_manual_backup || !shr_global.interlock_closed)
				v->i |= R_DISPENSE_TEST_ERROR;

			if ((cs->remote_port.number == DEBUG_PORT) ||
				(cs->remote_int_port.number == DEBUG_PORT) ||
				(cs->width_port.number == DEBUG_PORT))
				v->i |= R_DIAG_DEBUG_PORT_UNAVAIL;

			if (shr_global.dispense_test_done)
				v->i |= R_DISPENSE_TEST_DONE;

			break;

		case TYPE_BWH:
			if(shrBWH.hopper_gate_open)
				v->i |= R_BATCH_GATE_STATUS;
			if (shrBWH.wt_stable)
				v->i |= R_BWH_WEIGHT_STABLE;
			if(cfgBWH.demo)
				v->i |= R_BWH_IN_DEMO;
			if (cs->job_wt_complete)
				v->i |= R_JOB_COMPLETED;
			if (shrBWH.last_cal_reason == 'R')
				v->i |= R_BWH_LAST_CAL_REASON_R;
			if (shrBWH.last_cal_reason == 'I')
				v->i |= R_BWH_LAST_CAL_REASON_I;
			if (shrBWH.last_cal_reason == ' ')
				v->i |= R_BWH_LAST_CAL_REASON_NONE;

			switch (shrBWH.oper_mode)
			{
				case AUTO_MODE:
					v->i |= R_BWH_OPER_MODE_AUTO;
					break;
				case PAUSE_MODE:
					v->i |= R_BWH_OPER_MODE_PAUSE;
					break;
				case MAN_MODE:
					v->i |= R_BWH_OPER_MODE_MAN;
					break;
				case PURGE_MODE:
					v->i |= R_BWH_OPER_MODE_PURGE;
					break;
			}

			break;

		case TYPE_MIX:
			if(IO_dig[X20].channel[MIXER_GATE_OUT])
				v->i |= R_MIXER_GATE_OPEN;
			if(shrMIXER.mixer_motor_on)
				v->i |= R_MIXER_MOTOR_ON;
			if(shr_global.material_request)
				v->i |= R_EXTERNAL_MATERIAL_REQUEST_STATUS;
			if(shrMIXER.extruder_needs_material)
				v->i |= R_EXT_NEED_MATERIAL;
			if(shrMIXER.mixer_needs_material)
				v->i |= R_MIXER_NEED_MATERIAL;
			if(shrMIXER.mixer_prox_switch)
				v->i |= R_MIXER_PROX_SWITCH;
			if(shr_global.interlock_closed)
				v->i |= R_MIXER_INTERLOCK_CLOSED;
			if(cfgMIXER.demo)
				v->i |= R_MIXER_IN_DEMO;

			switch (shrMIXER.mixer_mode)
			{
				case PAUSE_MODE:
					v->i |= R_MIXER_MODE_PAUSE;
					break;
				case MAN_MODE:
					v->i |= R_MIXER_MODE_MAN;
					break;
				case PURGE_MODE:
					v->i |= R_MIXER_MODE_PURGE;
					break;
				case MIXER_WAITING_EMPTY:
					v->i |= R_MIXER_MODE_WAITING_EMPTY;
					break;
				case TIME_MIXER:
					v->i |= R_MIXER_MODE_TIME;
					break;
				case MIXER_WAITING_FULL:
					v->i |= R_MIXER_MODE_WAITING_FULL;
					break;
				case MIXER_DUMPING:
					v->i |= R_MIXER_MODE_DUMPING;
					break;
				default:
					break;
			}

			switch (shrMIXER.oper_mode)
			{
				case PAUSE_MODE:
					v->i |= R_MIXER_OPER_MODE_PAUSE;
					break;
				case AUTO_MODE:
					v->i |= R_MIXER_OPER_MODE_AUTO;
					break;
				case MAN_MODE:
					v->i |= R_MIXER_OPER_MODE_MAN;
					break;
				case PURGE_MODE:
					v->i |= R_MIXER_OPER_MODE_PURGE;
					break;
			}

			if (cfgMIXER.aux_starter_used)
			{
				if (shrMIXER.mixer_starter_aux_contact)
					v->i |= R_MIXER_AUX_PLUS;
				else
					v->i |= R_MIXER_AUX_MINUS;
			}
			else
				v->i |= R_MIXER_AUX_NONE;

			break;

		case TYPE_GATE:
			if(shrGATE[pd->loop].gate_done)
				v->i |= R_GATE_DONE;
			if (shrGATE[pd->loop].feed_state != DONE)
			   v->i |= R_HOPPER_GATE_STATUS;
			/* for self loading remote */
			if (shrGATE[pd->loop].sreceiver.prox_on)
				v->i |= R_LOADING_PROX_SWITCH;

			if(shrGATE[pd->loop].sreceiver.out_relay_on)
				v->i |= W_PRIMARY_SOLINOID;

			if (shrGATE[pd->loop].sreceiver.receiving_on)
				v->i |= R_GATE_RECEIVING_ON;

			if (shrGATE[pd->loop].sreceiver.dumping_on)
				v->i |= R_GATE_DUMPING_ON;

			if (cfgGATE[pd->loop].demo)
				v->i |= R_GATE_IN_DEMO;
			if (shrGATE[pd->loop].last_recal_cause == 'P')
				v->i |= R_GATE_LAST_RECAL_CAUSE_P;
			else if (shrGATE[pd->loop].last_recal_cause == 'F')
				v->i |= R_GATE_LAST_RECAL_CUASE_F;
			else
				v->i |= R_GATE_LAST_RECAL_CAUSE_NO;
			switch (shrGATE[pd->loop].feed_state)
			{
				case START:
					v->i |= R_GATE_FEED_STATE_start;
					break;
				case PREPULSE_GATE:
					v->i |= R_GATE_FEED_STATE_prepulse_gate;
					break;
				case CHECKING_PREPULSE:
					v->i |= R_GATE_FEED_STATE_checking_prepulse;
					break;
				case PULSE_GATE:
					v->i |= R_GATE_FEED_STATE_pulse_gate;
					break;
				case WAITING_FOR_GATE:
					v->i |= R_GATE_FEED_STATE_waiting_for_gate;
					break;
				case WAITING_FOR_SETTLING:
					v->i |= R_GATE_FEED_STATE_waiting_for_settling;
					break;
				case CHECK_WT:
					v->i |= R_GATE_FEED_STATE_check_wt;
					break;
				case DONE:
					v->i |= R_GATE_FEED_STATE_done;
					break;
			}

			switch(shrGATE[pd->loop].oper_mode)
			{
				case AUTO_MODE:
					v->i |= R_GATE_AUTO_MODE;
					break;
				case PAUSE_MODE:
					v->i |= R_GATE_PAUSE_MODE;
					break;
				case MAN_MODE:
					v->i |= R_GATE_MAN_MODE;
					break;
				case CALIBRATE_MODE:
					v->i |= R_GATE_CALIBRATE_MODE;
					break;
				case DISPENSING_MODE:
					v->i |= R_GATE_DISPENSING_MODE;
					break;
				default:
					v->i |= R_GATE_PAUSE_MODE;
					break;
			}

			switch (shrGATE[pd->loop].running_mode)
			{
				case NEW_MODE:
					v->i |= R_GATE_RUNNING_MODE_NEW;
					break;
				case MAN_MODE:
					v->i |= R_GATE_RUNNING_MODE_MAN;
					break;
				case WAITING_FOR_NEW_FEED:
					v->i |= R_GATE_RUNNING_MODE_WAITING;
					break;
				case SUSPEND:
					v->i |= R_GATE_RUNNING_MODE_SUSPEND;
					break;
				case AUTO_MODE:
					v->i |= R_GATE_RUNNING_MODE_AUTO;
					break;
				case PAUSE_MODE:
					v->i |= R_GATE_RUNNING_MODE_PAUSE;
					break;
			}
			break;
		case TYPE_LIW:
			if (shrEXT.loading)
				v->i |= R_LOADING;
			if (shrEXT.ramping)
				v->i |= R_RAMPING;
			if (shrEXT.coast)
				v->i |= R_COASTING;
			if (!cfgEXT.cal_done)
				v->i |= R_NO_CALIBRATION;
			if (cfgEXT.rs_num == 0)
				v->i |= R_NO_RATE_SPEED;
			if (cfgEXT.demo == DEMO_BOTH_MODS)
				v->i |= R_DEMO_BOTH_MODS;
			if (cfgEXT.demo & DEMO_DRIVE_MOD)
				v->i |= R_DEMO_DRIVE_MOD;
			if (cfgEXT.demo & DEMO_WEIGH_MOD)
				v->i |= R_DEMO_WEIGH_MOD;

			switch (shrEXT.oper_mode)
			{
				case AUTO_MODE:
					v->i |= R_LIW_OPER_MODE_AUTO;
					break;
				case MAN_MODE:
					v->i |= R_LIW_OPER_MODE_MAN;
					break;
				case PAUSE_MODE:
					v->i |= R_LIW_OPER_MODE_PAUSE;
					break;
				case MONITOR_MODE:
					v->i |= R_LIW_OPER_MODE_MONITOR;
					break;
			}

			break;

		case TYPE_HO:
			if (shrHO.ramping)
				v->i |= R_RAMPING;
			if (shrHO.coast)
			   v->i |= R_COASTING;
			if (!cfgHO.cal_done)
			   v->i |= R_NO_CALIBRATION;
			if (cfgHO.rs_num == 0)
				v->i |= R_NO_RATE_SPEED;
			if (cfgHO.demo == DEMO_BOTH_MODS)
				v->i |= R_DEMO_BOTH_MODS;
			if (cfgHO.demo & DEMO_DRIVE_MOD)
				v->i |= R_DEMO_DRIVE_MOD;
			if (cfgHO.demo & DEMO_PULSES)
				v->i |= R_DEMO_PULSES;
			break;

		case TYPE_FLF:
			if (shrGFLUFF.loading)
				v->i |= R_LOADING;
			if (cfgGFLUFF.demo == DEMO_BOTH_MODS)
			   v->i |= R_DEMO_BOTH_MODS;
			if (cfgGFLUFF.demo & DEMO_DRIVE_MOD)
			   v->i |= R_DEMO_DRIVE_MOD;
			if (cfgGFLUFF.demo & DEMO_WEIGH_MOD)
				v->i |= R_DEMO_WEIGH_MOD;

			switch (shrGFLUFF.oper_mode)
			{
				case AUTO_MODE:
					v->i |= R_LIW_OPER_MODE_AUTO;
					break;
				case MAN_MODE:
					v->i |= R_LIW_OPER_MODE_MAN;
					break;
				case PAUSE_MODE:
					v->i |= R_LIW_OPER_MODE_PAUSE;
					break;
			}

			switch (shrGFLUFF.scrap_mode)
			{
				case MAX_SCRAP:
					v->i |= R_FLF_MAX_SCRAP;
					break;
				case CUT_BACK:
					v->i |= R_FLF_CUT_BACK;
					break;
				case MAX_VIRGIN:
					v->i |= R_FLF_MAX_VIRGIN;
					break;
			}

			break;
		case TYPE_WTH:  /* no bits yet defined */
			break;

		default:
			retcode = INVALID_POINT;
	}
	/* check if returning only upper word or only lower word */
	if(pd->sixteen_bit_mode)
	{
		if ( ( pd->reg == R_STATUS_WORD_HI )  ||                    /* upper word           */
			( pd->reg == R_STATUS_HIGH ) )

			v->i = (unsigned int)(0xFFFF0000 & v->i) >> 16;     /* mask off lower bits  */
			/* shift to lo word     */
		else                                                    /* lower word           */
			v->i &= (unsigned int)0xFFFF;                       /* mask off upper bits  */
	}

	*type = INT;

	return retcode;
}

/****************************************************************************/
/* rv_alarm_status_word()                                                   */
/****************************************************************************/
int rv_current_alarm(fic *v,point_data *pd, int *type)
{
	int retcode = OK;
	int hop = pd->hop;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->i = shrBWH.alarm_bits;
			break;

		case TYPE_GATE:
			v->i = shrGATE[hop].alarm_bits;
			break;

		case TYPE_MIX:
			v->i = shrMIXER.alarm_bits;
			break;

		case TYPE_SYSTEM:
			v->i = shr_global.alarm_bits;
			break;

		case TYPE_LIW:
			v->i = shrEXT.alarm_bits;
			break;

		case TYPE_HO:
			v->i = shrHO.alarm_bits;
			break;

		case TYPE_WTH:
			v->i = shrWTH.alarm_bits;
			break;

		default:
			v->i = 0;
			retcode = INVALID_POINT;
	}

	/* check if returning only upper word or only lower word */
	if (pd->sixteen_bit_mode)
	{
      if ((pd->reg == R_ALARM_STATUS_WORD) || (pd->reg == R_ALARM_CURRENT_HIGH))
         /* upper word           */
			v->i = (unsigned int)(0xFFFF0000 & v->i) >> 16;     /* mask off lower bits  */
			/* shift to lo word     */
		else                                                   /* lower word           */
			v->i &= (unsigned int)0xFFFF;                       /* mask off upper bits  */
	}
	*type = INT;

	return retcode;
}

/****************************************************************************/
/* rv_alarm_latch_word()                                                      */
/****************************************************************************/
int rv_latched_alarm(fic *v,point_data *pd, int *type)
{
	int retcode = OK;
	int hop = pd->hop;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->i = shrBWH.alarm_latch_bits;
			break;

		case TYPE_GATE:
			v->i = shrGATE[hop].alarm_latch_bits;
			break;

		case TYPE_MIX:
			v->i = shrMIXER.alarm_latch_bits;
			break;

		case TYPE_SYSTEM:
			v->i = shr_global.alarm_latch_bits;
			break;

		case TYPE_LIW:
			v->i = shrEXT.alarm_latch_bits;
			break;

		case TYPE_HO:
			v->i = shrHO.alarm_latch_bits;
			break;

		case TYPE_WTH:
			v->i = shrWTH.alarm_latch_bits;
			break;

		default:
			v->i = 0;
			retcode = INVALID_POINT;
	}

	/* check if returning only upper word or only lower word */
	if (pd->sixteen_bit_mode)
	{
      if( (pd->reg == R_ALARM_LATCH_WORD)   ||
         (pd->reg == R_ALARM_LATCHED_HIGH) )
         /* upper word           */
			v->i = (unsigned int)(0xFFFF0000 & v->i)>> 16;      /* mask off lower bits*/
			/* shift to lo word     */
		else                                                    /* lower word           */
			v->i &= (unsigned int)0xFFFF;                       /* mask off upper bits  */
	}
	*type = INT;

	return retcode;
}

/****************************************************************************/
/* rv_next_recipe()                                                         */
/****************************************************************************/
int rv_next_recipe(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned int)cs->current_temp_recipe_num;
	*type = INT;

	return OK;
}
/****************************************************************************/
/* rv_next_recipe()                                                         */
/****************************************************************************/
int rv_current_recipe_num(fic *v,point_data *pd, int *type)
{
	v->i = (unsigned int)shr_global.current_recipe_num;
	*type = INT;

	return OK;
}

/****************************************************************************/
/* rv_set_manual()                                                           */
/****************************************************************************/
int rv_set_manual(fic *v,point_data *pd, int *type)
{
	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->i = 0;

			if (hmi_pg005_controls[0])
			v->i |= MANUAL_GATE_A;
			if (hmi_pg005_controls[1])
			v->i |= MANUAL_GATE_B;
			if (hmi_pg005_controls[2])
				v->i |= MANUAL_GATE_C;
			if (hmi_pg005_controls[3])
				v->i |= MANUAL_GATE_D;
			if (hmi_pg005_controls[4])
				v->i |= MANUAL_GATE_E;
			if (hmi_pg005_controls[5])
				v->i |= MANUAL_GATE_F;
			if (hmi_pg005_controls[6])
				v->i |= MANUAL_GATE_G;
			if (hmi_pg005_controls[7])
				v->i |= MANUAL_GATE_H;
			if (hmi_pg005_controls[8])
				v->i |= MANUAL_GATE_I;
			if (hmi_pg005_controls[9])
				v->i |= MANUAL_GATE_J;
			if (hmi_pg005_controls[10])
				v->i |= MANUAL_GATE_K;
			if (hmi_pg005_controls[11])
				v->i |= MANUAL_GATE_L;

			if (hmi_pg005_controls[12])
				v->i |= MANUAL_MIXER_MOTOR;
			if (hmi_pg005_controls[13])
				v->i |= MANUAL_MIXER_GATE;
			if (hmi_pg005_controls[14])
				v->i |= MANUAL_BWH;
			if (hmi_pg005_controls[15])
				v->i |= MANUAL_ALARM_OUT;
			if (hmi_pg005_controls[16])
				v->i |= MANUAL_ACTUATE;
			break;

		default:
			return INVALID_POINT;
	}
	*type = INT;
	return OK;
}

/****************************************************************************/
/* rv_set_parts()                                                           */
/****************************************************************************/
int rv_set_parts(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_GATE:
			v->f = BATCH_RECIPE_NOW->parts.s[pd->hop];
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->f = BATCH_RECIPE_NOW->parts.ext;
			else
				v->f = BATCH_RECIPE_NOW->parts.s[REFEED];
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;

	return(retcode);
}

/****************************************************************************/
/* rv_std_dev()                                                           */
/****************************************************************************/
int rv_std_dev(fic *v,point_data *pd, int *type)
{
	switch (pd->dev_type)
	{
		case TYPE_GATE:
			v->f = shrBWH.batch_stat[pd->hop].gate_avg_wt_deviation;
			break;
		default:
			return INVALID_POINT;
	}
	*type = FLOAT;
	return OK;
}

/****************************************************************************/
/* rv_temp_set_parts()                                                      */
/****************************************************************************/
int rv_temp_set_parts(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
				v->f = GATE_TEMP_SET_PARTS(pd->hop);
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
					v->f = EXT_TEMP_SET_PARTS;
			else
					v->f = REFEED_TEMP_SET_PARTS;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}

/****************************************************************************/
/* rv_act_parts()                                                           */
/****************************************************************************/
int rv_act_parts(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
				v->f = ACTUAL_RECIPE->parts.s[pd->hop];
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
					v->f = ACTUAL_RECIPE->parts.ext;
			else
					v->f = ACTUAL_RECIPE->parts.s[REFEED];
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}


/****************************************************************************/
/* rv_set_wtp()                                                             */
/****************************************************************************/
int rv_set_wtp(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->f = shrEXT.set_wtp;
			else
   			v->f = BATCH_RECIPE_NOW->refeed_wtp;
			break;
		case TYPE_SYSTEM:
			v->f = BATCH_RECIPE_NOW->total_wtp;     
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = FLOAT;
	return(retcode);
}
/****************************************************************************/
/* rv_temp_set_wtp()                                                        */
/****************************************************************************/
int rv_temp_set_wtp(fic *v,point_data *pd, int *type)
{
	int retcode = OK;
	switch(pd->dev_type)
	{
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->f = EXT_TEMP_SET_WTP;
			else
				v->f = REFEED_TEMP_SET_WTP;
			break;
		case TYPE_SYSTEM:
			v->f = SYS_TEMP_SET_WTP;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}

/****************************************************************************/
/* rv_act_wtp()                                                             */
/****************************************************************************/
int rv_act_wtp(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			//v->f = BATCH_HOP_ACT_WTP;
         v->f = shrBWH.avg_wtp_rate.avg; //G2-673
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
			{
				v->f = shrEXT.act_wtp;
			}
			else
				v->f = ACTUAL_RECIPE->refeed_wtp;
			break;
		case TYPE_SYSTEM:
 			v->f = ACTUAL_RECIPE->total_wtp;
			break;
		case TYPE_FLF:
			v->f = shrGFLUFF.act_wtp;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_set_ltp()                                                             */
/****************************************************************************/
int rv_set_ltp(fic *v,point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_NOW->ltp;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_temp_set_ltp()                                                        */
/****************************************************************************/
int rv_temp_set_ltp(fic *v,point_data *pd, int *type)
{
	v->f = HO_TEMP_SET_LTP;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_act_ltp()                                                             */
/****************************************************************************/
int rv_act_ltp(fic *v,point_data *pd, int *type)
{
	if (pd->loop == SHO_LOOP)
		v->f = SHO_ACT_LTP;
	else
		v->f = shrHO.act_ltp;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_set_thk()                                                             */
/****************************************************************************/
int rv_set_thk(fic *v,point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_NOW->thk;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_temp_set_thk()                                                        */
/****************************************************************************/
int rv_temp_set_thk(fic *v,point_data *pd, int *type)
{
	v->f = SYS_TEMP_SET_THK;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_act_thk()                                                             */
/****************************************************************************/
int rv_act_thk(fic *v,point_data *pd, int *type)
{
   v->f = shrHO.act_thk;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_set_wpl()                                                             */
/****************************************************************************/
int rv_set_wpl(fic *v,point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_NOW->wpl;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_temp_set_wpl()                                                        */
/****************************************************************************/
int rv_temp_set_wpl(fic *v,point_data *pd, int *type)
{
	v->f = SYS_TEMP_SET_WPL;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_act_wpl()                                                             */
/****************************************************************************/
int rv_act_wpl(fic *v,point_data *pd, int *type)
{
	v->f = shrHO.act_wpl;
	*type = FLOAT;

	return OK;
}
/************************************************************************/
/* rv_new_set_wpa()                                                    */
/*      ability to read the temp wpa remotely added for completeness.   */
/*      RAC 5/19/98                                                     */
/************************************************************************/
int rv_new_set_wpa(fic *v,point_data *pd, int *type)
{
	v->f = SYS_TEMP_SET_WPA;

	*type = FLOAT;
	return OK;
}
/************************************************************************/
/* rv_set_wpa()                                                         */
/************************************************************************/
int rv_set_wpa(fic *v,point_data *pd,int *type)
{
	v->f = BATCH_RECIPE_NOW->wpa;
	*type = FLOAT;

	return OK;
}
/************************************************************************/
/* rv_temp_set_wpa()                                                    */
/************************************************************************/
int rv_temp_set_wpa(fic *v,point_data *pd, int *type)
{
	v->f = SYS_TEMP_SET_WPA;
	*type = FLOAT;

	return OK;
}
/************************************************************************/
/* rv_act_wpa()                                                         */
/************************************************************************/
int rv_act_wpa(fic *v,point_data *pd, int *type)
{
	v->f = shrHO.act_wpa;
	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_set_od()                                                              */
/****************************************************************************/
int rv_set_od(fic *v,point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_NOW->od;
	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_act_od()                                                              */
/****************************************************************************/
int rv_act_od(fic *v,point_data *pd, int *type)
{
	v->f = ACTUAL_RECIPE->od;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_set_id()                                                              */
/****************************************************************************/
int rv_set_id(fic *v,point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_NOW->id;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_act_id()                                                              */
/****************************************************************************/
int rv_act_id(fic *v,point_data *pd, int *type)
{
	v->f = ACTUAL_RECIPE->id;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_set_spd()                                                             */
/****************************************************************************/
int rv_set_spd(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->f = shrEXT.set_spd * cfgEXT.spd_factor;
			break;
		case TYPE_HO:
			v->f = shrHO.set_spd * cfgHO.spd_factor[shrHO.GearNum];
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	
	*type = FLOAT;
	return(retcode);
}
/****************************************************************************/
/* rv_act_spd()                                                             */
/****************************************************************************/
int rv_act_spd(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = shrEXT.act_spd * cfgEXT.spd_factor;
			break;
		case TYPE_HO:
			v->f = shrHO.act_spd * cfgHO.spd_factor[shrHO.GearNum];
			break;
		default:
			v->f = 0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_inven_wt()                                                            */
/****************************************************************************/
int rv_inven_wt(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			if (cfg_super.units == METRIC)
				v->f = (float)SYS_INVEN_WT * 0.453592;	
			else 
				v->f = (float)SYS_INVEN_WT;
			break;
		case TYPE_GATE:
			if (cfg_super.units == METRIC)
				v->f = (float)GATE_INVEN_WT(pd->hop) * 0.453592;	
			else
				v->f = (float)GATE_INVEN_WT(pd->hop);
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
			{
				if (cfg_super.units == METRIC)
					v->f = (float) EXT_INVEN_WT * 0.453592;
				else 
					v->f = (float) EXT_INVEN_WT;
			}
			break;
		case TYPE_SYSTEM:
			if (cfg_super.units == METRIC)
				v->f = (float)SYS_INVEN_WT * 0.453592; //G2-610
			else
				v->f = (float)SYS_INVEN_WT;            //G2-610
			break;
		default:
			v->f = (float)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_wt_per_bit()                                                            */
/****************************************************************************/
int rv_wt_per_bit(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = (float)(cfgBWH.wtperbit);
			break;
		case TYPE_LIW:
			v->f = (float)cfgEXT.wtperbit;
			break;
		case TYPE_FLF:
			v->f = (float)(cfgGFLUFF.wtperbit);
			break;
		default:
			v->f = (float)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_last_max_wtp()                                                            */
/****************************************************************************/
int rv_last_max_wtp(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = (float)(shrBWH.last_wtp);
			break;
		case TYPE_MIX:
			v->f = (float)(shrBWH.last_wtp);
			break;
		default:
			v->f = (float)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_last_inven_wt()                                                            */
/****************************************************************************/
int rv_last_inven_wt(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = (float)shr_global.last_sys_inven_wt;
			break;
		case TYPE_GATE:
			v->f = (float)shrGATE[pd->hop].last_inven_wt;
			break;
		case TYPE_LIW:
			v->f = (float)shrEXT.last_inven_wt;
			break;
		case TYPE_SYSTEM:
			v->f = (float)shr_global.last_sys_inven_wt;
			break;
		default:
			v->f = (float)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_shift_wt()                                                            */
/****************************************************************************/
int rv_shift_wt(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			if (cfg_super.units == METRIC)
				v->f = (float)SYS_SHIFT_WT * 0.453592;
			else 
				v->f = (float)SYS_SHIFT_WT;
			break;
		case TYPE_GATE:
			if (cfg_super.units == METRIC)
				v->f = (float)GATE_SHIFT_WT(pd->hop) * 0.453592;
			else 	
				v->f = (float)GATE_SHIFT_WT(pd->hop);
			break;
		case TYPE_LIW:
			/* if (pd->loop == EXT_LOOP) */
			if (cfg_super.units == METRIC)
				v->f = (float) EXT_SHIFT_WT * 0.453592;	
			else 
				v->f = (float) EXT_SHIFT_WT;
			break;
		case TYPE_SYSTEM:
			if (cfg_super.units == METRIC)
				v->f = (float)SYS_SHIFT_WT * 0.453592;	//G2-610
			else 	
				v->f = (float)SYS_SHIFT_WT;            //G2-610
			break;
		default:
			v->f = (float)0.0;
			retcode = (int)INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return(retcode);
}

/****************************************************************************/
/* rv_last_shift_wt()                                                            */
/****************************************************************************/
int rv_last_shift_wt(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			if (cfg_super.units == METRIC)
				v->f = (float)shr_global.last_sys_shift_wt * 0.453592;
			else
				v->f = (float)shr_global.last_sys_shift_wt;
			break;
		case TYPE_GATE:
			if (cfg_super.units == METRIC)
				v->f = (float)shrGATE[pd->hop].last_shift_wt * 0.453592;
			else
				v->f = (float)shrGATE[pd->hop].last_shift_wt;
			break;
		case TYPE_LIW:
			if (cfg_super.units == METRIC)
				v->f = (float)shrEXT.last_shift_wt * 0.453592;
			else
				v->f = (float)shrEXT.last_shift_wt;
			break;
		case TYPE_SYSTEM:
			if (cfg_super.units == METRIC)
				v->f = (float)shr_global.last_sys_shift_wt * 0.453592;
			else
				v->f = (float)shr_global.last_sys_shift_wt;
			break;
		default:
			v->f = (float)0.0;
			retcode = (int)INVALID_POINT;
			break;
	}

	*type = FLOAT;

	return(retcode);
}
/****************************************************************************/
/* rv_inven_len()                                                           */
/****************************************************************************/
int rv_inven_len(fic *v,point_data *pd, int *type)
{
	if (cfg_super.units == METRIC)
		v->f = (float)PMEM.HO.InvTotal * 0.453592;
	else
		v->f = (float)PMEM.HO.InvTotal;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_last_inven_len()                                                           */
/****************************************************************************/
int rv_last_inven_len(fic *v,point_data *pd, int *type)
{
	if (cfg_super.units == METRIC)
		v->f = (float)shrHO.last_inven_len * 0.453592;
	else
		v->f = (float)shrHO.last_inven_len;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_shift_len()                                                           */
/****************************************************************************/
int rv_shift_len(fic *v,point_data *pd, int *type)
{
	if (cfg_super.units == METRIC)
		v->f = (float)PMEM.HO.ShftTotal * 0.453592;
	else
		v->f = (float)PMEM.HO.ShftTotal;
	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_last_shift_len()                                                           */
/****************************************************************************/
int rv_last_shift_len(fic *v,point_data *pd, int *type)
{
	v->f = (float)shrHO.last_shift_len;
	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_inven_area()                                                          */
/****************************************************************************/
int rv_inven_area(fic *v,point_data *pd, int *type)
{
	v->f = PMEM.HO.InvArea;
	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_shift_area()                                                           */
/****************************************************************************/
int rv_shift_area(fic *v,point_data *pd, int *type)
{
	v->f = PMEM.HO.ShftArea;
	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_rem_weight()                                                       */
/****************************************************************************/
int rv_rem_weight(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->f = shrGATE[pd->loop].rem_wt;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_set_tol()                                                       */
/****************************************************************************/
int rv_set_tol(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->f = shrGATE[pd->loop].set_tol;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_curr_gate_time()                                                       */
/****************************************************************************/
int rv_curr_gate_time(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->f = (int)(shrGATE[pd->loop].curr_gate_time * 1000.0 + 0.4);
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}

/****************************************************************************/
/* rv_hopper_weight()                                                       */
/****************************************************************************/
int rv_hopper_weight(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			if (cfg_super.units == METRIC)
				v->f = shrBWH.hop_wt * 0.453592;
			else
				v->f = shrBWH.hop_wt;
			break;
		case TYPE_LIW:
			if (cfg_super.units == METRIC)
				v->f = shrEXT.hop_wt * 0.453592;
			else
				v->f = shrEXT.hop_wt;
			break;
		case TYPE_FLF:
		case TYPE_SYSTEM:
			if (cs->refeed == GRAVIFLUFF)
				if (cfg_super.units == METRIC)
					v->f = pd->HOP_WT(GRAVIFLUFF_LOOP) * 0.453592;
				else
					v->f = pd->HOP_WT(GRAVIFLUFF_LOOP);
			else
			{
				v->f = (float)0.0;
				retcode = INVALID_POINT;
			}
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}

/****************************************************************************/
/* rv_man_spd()                                                           */
/****************************************************************************/
int rv_man_spd(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->f = BATCH_RECIPE_NOW->ext_man_spd;
			else
				v->f = BATCH_RECIPE_NOW->refeed_man_spd;
			break;

		case TYPE_HO:
			v->f = BATCH_RECIPE_NOW->ho_man_spd;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;

	return retcode;
}

/****************************************************************************/
/* rv_new_man_spd()                                                        */
/****************************************************************************/
int rv_temp_man_spd(fic *v,point_data *pd, int *type)
{
	int retcode = OK;


	switch(pd->dev_type)
	{
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->f = BATCH_RECIPE_EDIT.ext_man_spd;
			else
				v->f = BATCH_RECIPE_EDIT.refeed_man_spd;
			break;

		case TYPE_HO:
			v->f = BATCH_RECIPE_EDIT.ho_man_spd;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_set_density()                                                         */
/****************************************************************************/
int rv_set_density(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
		case TYPE_BWH:
		case TYPE_SYSTEM:
			v->f = BATCH_RECIPE_NOW->avg_density;
			break;

		case TYPE_GATE:
			v->f = BATCH_RECIPE_NOW->density.s[pd->hop];
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;

	return(retcode);
}
/****************************************************************************/
/* rv_act_density()                                                         */
/****************************************************************************/
int rv_act_density(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
		case TYPE_BWH:
		case TYPE_SYSTEM:
			v->f = ACTUAL_RECIPE->avg_density;
			break;

		case TYPE_GATE:
			v->f = ACTUAL_RECIPE->density.s[pd->hop];
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_temp_set_density()                                                    */
/****************************************************************************/
int rv_temp_set_density(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
		case TYPE_BWH:
		case TYPE_SYSTEM:
			v->f = SYS_TEMP_SET_DENSITY;
			break;

		case TYPE_GATE:
			v->f = GATE_TEMP_SET_DENSITY(pd->hop);
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_accel()                                                               */
/****************************************************************************/
int rv_accel(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;


	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->f = cs->accel;
			break;

		case TYPE_LIW:
			v->f = cfgEXT.accel;
			break;

		case TYPE_HO:
			v->f = cfgHO.accel;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_decel()                                                               */
/****************************************************************************/
int rv_decel(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->f = cs->decel;
			break;

		case TYPE_LIW:
			v->f = cfgEXT.decel;
			break;

		case TYPE_HO:
			v->f = cfgHO.decel;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/******************************************************
    rv_temp_mix_time()
******************************************************/
int rv_temp_mix_time(fic *v, point_data *pd, int *type)
{
	v->f = BATCH_RECIPE_EDIT.mix_time/FTICKSPERSEC;

	*type = FLOAT;

	return OK;

}
/******************************************************
    rv_mix_time()
******************************************************/
int rv_mix_time(fic *v, point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
     
	if (cs->mix_time_in_recipe) 
		v->f = BATCH_RECIPE_NOW->mix_time/FTICKSPERSEC;
	else
		v->f = cfgMIXER.mix_time/FTICKSPERSEC;

	*type = FLOAT;

	return OK;
}

/******************************************************
    rv_mix_time_left()
******************************************************/
int rv_mix_time_left(fic *v, point_data *pd, int *type)
{
	int i;

	if (shrMIXER.mixer_mode == TIME_MIXER)
	{
		i = (int)shrMIXER.total_mix_time - (int)shrMIXER.mix_elap_time;
		if (i < 0)
			i = 0;
		v->f = (float)i / FTICKSPERSEC;
	}
	else
		v->f = 0.0;

	*type = FLOAT;

	return OK;
}

/****************************************************************************/
/* rv_batch_size()                                                          */
/****************************************************************************/
int rv_batch_size(fic *v, point_data *pd, int *type)
{
	int retcode = OK;
 
   //G2-498
   //if (cfg_super.units == METRIC) //G2-347
	//   v->f = cfgBWH.set_batch_size * 0.453592;
   //else

   v->f = cfgBWH.set_batch_size;
	*type = FLOAT;
	return(retcode);
}

/****************************************************************************/
/* rv_set_job_wt()                                                          */
/****************************************************************************/
int rv_job_wt(fic *v,point_data *pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if (cfg_super.units == METRIC)
		v->f = SYS_JOB_WT * 0.453592;
	else
		v->f = SYS_JOB_WT;
	*type = FLOAT;
	return(retcode);
}

/****************************************************************************/
/* rv_temp_job_wt()                                                             */
/****************************************************************************/
int rv_temp_job_wt(fic *v,point_data *pd, int *type)
{
	v->f  = SYS_TEMP_JOB_WT;
	*type = FLOAT;
	return OK;
}
/**************************************************/
/*rv_job_wt_completed()                           */
/**************************************************/
int rv_job_wt_completed(fic *v, point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->f  = SYS_JOB_WT_DONE;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_temp_step_size()                                                             */
/****************************************************************************/
int rv_temp_step_size(fic *v,point_data *pd, int *type)
{
	v->f  = SYS_TEMP_STEP_SIZE;
	*type = FLOAT;
	return OK;
}

/****************************************************************************/
/* rv_step_size()                                                             */
/****************************************************************************/
int rv_step_size(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->f  = SYS_STEP_SIZE;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_act_target_weight()                                                             */
/****************************************************************************/
int rv_act_target_weight(fic *v,point_data *pd, int *type)
{
	v->f  = shr_global.act_testing_wt;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_set_wt()                                                              */
/****************************************************************************/
int rv_set_wt(fic *v,point_data *pd, int *type)
{
	v->f  = GATE_SET_WT(pd->loop);
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_act_wt()                                                              */
/****************************************************************************/
int rv_act_wt(fic *v,point_data *pd, int *type)
{
	if (cfg_super.units == METRIC)
	{
		v->f = GATE_ACT_WT(pd->loop) * 0.453592;
		if (shrBWH.remote_batch_completed == TRUE)
			v->f = shrGATE[pd->loop].last_act_wt * 0.453592;
	}
	else
	{
		v->f = GATE_ACT_WT(pd->loop);
		if (shrBWH.remote_batch_completed == TRUE)
			v->f = shrGATE[pd->loop].last_act_wt;
	}
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_remaining_wt()                                                              */
/****************************************************************************/
int rv_remaining_wt(fic *v,point_data *pd, int *type)
{
	v->f  = GATE_REMAINING_WT(pd->loop);
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_max_wtp()                                                             */
/****************************************************************************/
int rv_max_wtp(fic *v,point_data *pd, int *type)
{
	int retcode = OK;
	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = shrBWH.max_wtp;
         //if (cfg_super.units == METRIC) //G2-394
	      //   v->f *= 0.453592;
			break;

		case TYPE_MIX:
			v->f = shrBWH.max_wtp;
         //if (cfg_super.units == METRIC) //G2-394
	      //   v->f *= 0.453592;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_set_ratio_spd()                                                       */
/****************************************************************************/
int rv_set_ratio_spd(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->f = shr_global.man_ratio;
			break;

		case TYPE_LIW:
			if (shr_global.man_valid)
				v->f = shr_global.liw_spd[pd->loop];
			else
			{
				if (pd->loop == EXT_LOOP)
					v->f = EXT_SET_SPD;
              /*else
                v->f = REFEED_SET_SPD;*/
			}
			break;
		case TYPE_HO:
			if (shr_global.man_valid)
				v->f = shr_global.ho_spd;
			else
				v->f = HO_SET_SPD;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;

	return(retcode);
}
/****************************************************************************/
/* rv_act_ratio_spd()                                                       */
/****************************************************************************/
int rv_act_ratio_spd(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;
	float f;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			if(shr_global.man_valid && (shr_global.man_ratio > (float)0.0))
			{
				v->f = 0.0;
				if (cs->ltp_mode == CONTROL)
				{
					if((f=HO_SET_SPD) > (float)0.0)
					{
						v->f = (HO_ACT_SPD*shr_global.man_ratio)/f;
						break;
					}
				}

				if ((f=EXT_SET_SPD) > 0.0)
					v->f = (EXT_ACT_SPD*shr_global.man_ratio)/f;

			}
			break;

		case TYPE_LIW:
			if (shr_global.man_valid && ((f=shr_global.man_ratio) > 0.0))
			{
				if (pd->loop == EXT_LOOP)
					v->f = (float)(EXT_ACT_SPD * 100.0/f);
              /*else
                v->f = (float)(REFEED_ACT_SPD * 100.0/f);*/
			}
			else
			{
				if (pd->loop == EXT_LOOP)
					v->f = EXT_SET_SPD;
              /*else
                v->f = REFEED_SET_SPD;*/
			}
			break;

		case TYPE_HO:
			if (shr_global.man_valid && ((f=shr_global.man_ratio) > (float)0.0))
				v->f = (float)(HO_ACT_SPD*(float)100.0/f);
			else
				v->f = HO_ACT_SPD;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_temp_set_od()                                                         */
/****************************************************************************/
int rv_temp_set_od(fic *v, point_data *pd, int *type)
{
	v->f  = SYS_TEMP_SET_OD;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_temp_set_id()                                                         */
/****************************************************************************/
int rv_temp_set_id(fic *v, point_data *pd, int *type)
{
	v->f  = SYS_TEMP_SET_ID;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_set_width()                                                           */
/****************************************************************************/
int rv_set_width(fic *v, point_data *pd, int *type)
{
	v->f  = BATCH_RECIPE_NOW->width;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_temp_set_width()                                                      */
/****************************************************************************/
int rv_temp_set_width(fic *v,point_data *pd, int *type)
{
	v->f = SYS_TEMP_SET_WIDTH;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_act_width()                                                           */
/****************************************************************************/
int rv_act_width(fic *v,point_data *pd, int *type)
{
	v->f  = ACTUAL_RECIPE->width;
	*type = FLOAT;
	return OK;
}
/****************************************************************************/
/* rv_temp_stretch_factor()                                                 */
/****************************************************************************/
int rv_temp_stretch_factor(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	if (cs->stretch_factor_in_recipe)
		v->f = SYS_TEMP_STRETCH_FACTOR;
	else
		v->f = (float)0.0;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/* rv_stretch_factor()                                                      */
/****************************************************************************/
int rv_stretch_factor(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	if (cs->stretch_factor_in_recipe)
		v->f = BATCH_RECIPE_NOW->stretch_factor;
	else
		v->f = (float)0.0;

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_avg_batch_size()                                                      */
/****************************************************************************/
int rv_avg_batch_size(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = shrBWH.avg_batch_size.avg;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_last_gate_feed_wt()                                                      */
/****************************************************************************/
int rv_last_gate_feed_wt(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			if (cfg_super.units == METRIC)
				v->f = shrBWH.last_batch_stat[pd->hop].gate_act_wt * 0.453592;
			else
				v->f = shrBWH.last_batch_stat[pd->hop].gate_act_wt;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_avg_batch_time()                                                      */
/****************************************************************************/
int rv_avg_batch_time(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = shrBWH.avg_batch_time.avg;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_resolution()                                                      */
/****************************************************************************/
int rv_resolution(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = cfgBWH.resolution;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_batch_oversize()                                                      */
/****************************************************************************/
int rv_batch_oversize(fic *v, point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = cfgBWH.batch_oversize;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* null_read_function()                                                     */
/****************************************************************************/
int null_read_function(fic *f,point_data *pd,int *type)
{
	if (pd->reg < REG_INTEGER)
		*type = FLOAT;
	else
		*type = INT;

	f->i = 0;

	return INVALID_POINT;
}
/****************************************************************************/
/* rv_target_accuracy()                                              */
/****************************************************************************/
int rv_target_accuracy(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->f = cs->target_accuracy*100;
			break;
		case TYPE_GATE:
			v->f = cfgGATE[pd->loop].target_accuracy*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************/
/* rv_frame_size()                                              */
/****************************************************************************/
int rv_frame_size(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;
	char frame;
   
	//frame size is 0=4, 1=6, 2=8, 3=12 GTF 

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			switch(cs->frame_size)
			{
				case 0:
					frame = 4;
					break;
				case 1:
					frame = 6;
					break;
				case 2:
					frame = 8;
					break;
				case 3:
					frame = 12;
					break;
				default:
					frame = 4;
			}
			v->i = frame;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************/
/* rv_total_gates()                                              */
/****************************************************************************/
int rv_total_gates(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_SYSTEM:
			if (cfg_super.units == METRIC)
				v->i = (unsigned char) cs->total_gates * 0.453592 ;
			else
				v->i = (unsigned char) cs->total_gates;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************/
/* rv_wtp_mode()                                              */
/****************************************************************************/
int rv_wtp_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->wtp_mode)
	{
		case UNUSED:
			v->i = 0;
			break;
		case MONITOR:
			v->i = 1;
			break;
		case CONTROL:
			v->i = 2;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_ltp_mode()                                              */
/****************************************************************************/
int rv_ltp_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->ltp_mode)
	{
		case UNUSED:
			v->i = 0;
			break;
		case MONITOR:
			v->i = 1;
			break;
		case CONTROL:
			v->i = 2;
			break;
	}
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_sltp_mode()                                              */
/****************************************************************************/
int rv_sltp_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->sltp_mode)
	{
		case UNUSED:
			v->i = 0;
			break;
		case MONITOR:
			v->i = 1;
			break;
		case CONTROL:
			v->i = 2;
			break;
	}
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_ltp_mode()                                              */
/****************************************************************************/
int rv_refeed_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->refeed)
	{
		case NONE:
			v->i = NONE;
			break;
		case GRAVIFLUFF:
			v->i = GRAVIFLUFF;
			break;
		case TRIM_ONLY:
			v->i = TRIM_ONLY;
			break;
	}
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_ltp_mode()                                              */
/****************************************************************************/
int rv_width_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->wth_mode)
	{
		case UNUSED:
			v->i = 0;
			break;
		case MONITOR:
			v->i = 1;
			break;
		case CONTROL:
			v->i = 2;
			break;
	}
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_application()                                              */
/****************************************************************************/
int rv_application(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned char) cs->application;
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_system_flags()                                              */
/****************************************************************************/
int rv_system_flags(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;

	if(cs->blend_control)
		flags |= FLAG_CONTROL_LOADING;

	if(cs->blend_control_remote)
		flags |= FLAG_REMOTE_SELF_LOADING;

	if(cs->refeed)
		flags |= FLAG_REFEED;

	if (cs->refeed == GRAVIFLUFF)
		flags |= FLAG_REFEED_GRAVIFLUFF;

	v->i = flags;
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_minimum_security_level()                                              */
/****************************************************************************/
int rv_minimum_security_level(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = cs->minimum_security_level;
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_a(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[0];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_b(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[1];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_c(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[2];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_d(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[3];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_e(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[4];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_f(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[5];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_g(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[6];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_h(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[7];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_i(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[8];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_j(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[9];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_k(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[10];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_hop_l(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->hopper_parts_lock[11];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_setup(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_SETUP];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_calibrate(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CALIBRATE];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_change_recip(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CHANGE_RECIPE];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_store_recipe(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_STORE_RECIPE];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_stop(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_STOP];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_run(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_RUN];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_manual(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_EXT_MANUAL];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_clear_shift(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CLEAR_SHIFT];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_clear_inven(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CLEAR_INVEN];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_clear_resin(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CLEAR_RESIN];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_clear_recipe(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_CLEAR_RECIPE];
	*type = INT;
	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_pe_man_backup(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->pe_needed[PE_BLN_MANUAL];
	*type = INT;
	return OK;
}
/****************************************************************************
 ****************************************************************************/
int rv_current_security_level(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i  = (unsigned int)cs->current_security_level;
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_pe_needed_flags()                                              */
/****************************************************************************/
int rv_pe_needed_flags(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;
	int i;

	for (i=0; i< PE_TOTAL_ENTRIES; i++)
		if (cs->pe_needed[i])
			flags |= FLAG_PE_NEEDED(i);
	v->i = flags;
	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_supervisor_code_1()                                              */
/****************************************************************************/
int rv_supervisor_code_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->supervisor_code[0];
	v->c[1] = cs->supervisor_code[1];
	v->c[2] = cs->supervisor_code[2];
	v->c[3] = cs->supervisor_code[3];

	return OK;
}
/****************************************************************************/
/* rv_supervisor_code_2()                                              */
/****************************************************************************/
int rv_supervisor_code_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->supervisor_code[4];
	v->c[1] = cs->supervisor_code[5];
	v->c[2] = cs->supervisor_code[6];
	v->c[3] = cs->supervisor_code[7];

	return OK;
}
/****************************************************************************/
/* rv_maintenance_code_1()                                              */
/****************************************************************************/
int rv_maintenance_code_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->maintenance_code[0];
	v->c[1] = cs->maintenance_code[1];
	v->c[2] = cs->maintenance_code[2];
	v->c[3] = cs->maintenance_code[3];

	return OK;
}
/****************************************************************************/
/* rv_maintenance_code_2()                                              */
/****************************************************************************/
int rv_maintenance_code_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->maintenance_code[4];
	v->c[1] = cs->maintenance_code[5];
	v->c[2] = cs->maintenance_code[6];
	v->c[3] = cs->maintenance_code[7];

	return OK;
}
/****************************************************************************/
/* rv_operator_code_1()                                              */
/****************************************************************************/
int rv_operator_code_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->operator_code[0];
	v->c[1] = cs->operator_code[1];
	v->c[2] = cs->operator_code[2];
	v->c[3] = cs->operator_code[3];

	return OK;
}
/****************************************************************************/
/* rv_operator_code_2()                                              */
/****************************************************************************/
int rv_operator_code_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->operator_code[4];
	v->c[1] = cs->operator_code[5];
	v->c[2] = cs->operator_code[6];
	v->c[3] = cs->operator_code[7];

	return OK;
}
/****************************************************************************/
/* rv_units()                                              */
/****************************************************************************/
int rv_units(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->units;

	*type = INT;
	return OK;
}
/****************************************************************************/
/* rv_wpl_unit()                                              */
/****************************************************************************/
int rv_wpl_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->wpl_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_long_wpl_unit()                                              */
/****************************************************************************/
int rv_long_wpl_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->long_wpl_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_wpa_unit()                                              */
/****************************************************************************/
int rv_wpa_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->wpa_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_long_wpa_unit()                                              */
/****************************************************************************/
int rv_long_wpa_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->long_wpa_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_t_unit()                                              */
/****************************************************************************/
int rv_t_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->t_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_long_t_unit()                                              */
/****************************************************************************/
int rv_long_t_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->long_t_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_od_unit()                                              */
/****************************************************************************/
int rv_od_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->od_unit.i;

	*type = INT;
#endif
	return OK;
}
/****************************************************************************/
/* rv_long_od_unit()                                              */
/****************************************************************************/
int rv_long_od_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->long_od_unit.i;

	*type = INT;
#endif

	return OK;
}
/****************************************************************************/
/* rv_id_unit()                                              */
/****************************************************************************/
int rv_id_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->id_unit.i;

	*type = INT;
#endif

	return OK;
}
/****************************************************************************/
/* rv_long_id_unit()                                              */
/****************************************************************************/
int rv_long_id_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->long_id_unit.i;

	*type = INT;
#endif

	return OK;
}
/****************************************************************************/
/* rv_width_unit()                                              */
/****************************************************************************/
int rv_width_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->width_unit.i;

	*type = INT;
#endif

	return OK;
}
/****************************************************************************/
/* rv_long_width_unit()                                              */
/****************************************************************************/
int rv_long_width_unit(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->long_width_unit.i;

	*type = INT;
#endif

	return OK;
}
/****************************************************************************/
/* rv_device_on_port_1()                                              */
/****************************************************************************/
int rv_device_on_port_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->device_on_port[1];
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_device_on_port_3()                                              */
/****************************************************************************/
int rv_device_on_port_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->device_on_port[3];
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_print_id()                                              */
/****************************************************************************/
int rv_print_id(fic * v, point_data * pd, int *type)
{
/*   config_super *cs = &cfg_super;

   v->i = (unsigned char) cs->print_id;*/
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_remote_protocol()                                              */
/****************************************************************************/
int rv_remote_protocol(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = cs->remote_protocol;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_remote_address()                                              */
/****************************************************************************/
int rv_remote_address(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = cs->remote_address;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_remote_gravilink_address()                                              */
/****************************************************************************/
int rv_remote_gravilink_address(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = cs->remote_gravitrol_link.address;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_dual_port_byte_order()                                              */
/****************************************************************************/
int rv_dual_port_byte_order(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->dual_port_byte_order;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_report_start()                                              */
/****************************************************************************/
int rv_report_start(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned int) cs->report_start;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_report_interval()                                              */
/****************************************************************************/
int rv_report_interval(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned int) cs->report_interval;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_communication_flags()                                              */
/****************************************************************************/
int rv_communication_flags(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;

	if(cs->print_verbose)
		flags |= FLAG_PRINT_VERBOSE;
	if(cs->clear_totals_on_print)
		flags |= FLAG_CLEAR_TOTALS_ON_PRINT;
	if(cs->log_alarms)
		flags |= FLAG_LOG_ALARMS;
	if(cs->auto_reports)
		flags |= FLAG_AUTO_REPORTS;
	if(cs->report_change_recipe)
		flags |= FLAG_REPORT_CHANGE_RECIPE;
	if(cs->return_set_if_act_0)
		flags |= FLAG_RETURN_SET_IF_ACT_0;
	if(cs->use_pcc2_address)
		flags |= FLAG_USE_PCC2_ADDRESS;
	if(cs->compatibility_mode)
		flags |= FLAG_COMPATIBILITY_MODE;
	if(cs->ab_use_crc_chksum)
		flags |= FLAG_AB_USE_CRC_CHKSUM;
	if(cs->autorun_after_rate_changed)
		flags |= FLAG_AUTORUN_AFTER_RATE_CHANGED;
	if(cs->using_32bit_mode_holding_reg)
		flags |= FLAG_USING_32BITS_MODE;
	if(cs->remote_port.options & 0x01)
		flags |= FLAG_REMOTE_BIAS_ENABLED;
	if(cs->remote_port.multidrop)
		flags |= FLAG_REMOTE_MULTIDROP_ENABLED;
	if(cs->remote_int_port.options & 0x01)
		flags |= FLAG_REMOTE_OP_BIAS_ENABLED;
	if(cs->remote_int_port.multidrop)
		flags |= FLAG_REMOTE_OP_MULTIDROP_ENABLED;
	if(cs->web_server_enabled)
		flags |= FLAG_WEB_SERVER_ENABLED;
	if(cs->gravilink_uses_ethernet)
		flags |= FLAG_GRAVILINK_USES_ETHERNET;
	if(cs->mbenet_enabled)
		flags |= FLAG_MBENET_ENABLED;
	if(cs->abtcp_enabled)
		flags |= FLAG_ABTCP_ENABLED;
	if(cs->dual_port_comm_enabled)
		flags |= FLAG_PROFIBUS_ENABLED;
	if (shr_global.detected_options & DP_COMMS_1_PRESENT)
		flags |= FLAG_PROFIBUS_AVAIL;
	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_user_interface_flags()                                              */
/****************************************************************************/
int rv_user_interface_flags(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;

	if(cs->intl_date_format)
		flags |= FLAG_INTL_DATE_FORMAT;
	if(cs->spd_in_percent)
		flags |= FLAG_SPD_IN_PERCENT;
	if(cs->ho_in_ratio)
		flags |= FLAG_HO_IN_RATIO;
#if 0
	if(cs->use_ANSI_display)
		flags |= FLAG_USE_ANSI_DISPLAY;
#endif

	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_set_alarm_relay_type()                                              */
/****************************************************************************/
int rv_set_alarm_relay_type(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned int) cs->set_alarm_relay_type;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_1()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[0];
	v->c[1] = cs->aux_alarm_msg[1];
	v->c[2] = cs->aux_alarm_msg[2];
	v->c[3] = cs->aux_alarm_msg[3];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_2()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[4];
	v->c[1] = cs->aux_alarm_msg[5];
	v->c[2] = cs->aux_alarm_msg[6];
	v->c[3] = cs->aux_alarm_msg[7];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_3()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[8];
	v->c[1] = cs->aux_alarm_msg[9];
	v->c[2] = cs->aux_alarm_msg[10];
	v->c[3] = cs->aux_alarm_msg[11];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_4()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[12];
	v->c[1] = cs->aux_alarm_msg[13];
	v->c[2] = cs->aux_alarm_msg[14];
	v->c[3] = cs->aux_alarm_msg[15];

	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_5()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_5(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[16];
	v->c[1] = cs->aux_alarm_msg[17];
	v->c[2] = cs->aux_alarm_msg[18];
	v->c[3] = cs->aux_alarm_msg[19];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_6()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_6(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[20];
	v->c[1] = cs->aux_alarm_msg[21];
	v->c[2] = cs->aux_alarm_msg[22];
	v->c[3] = cs->aux_alarm_msg[23];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_7()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_7(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[24];
	v->c[1] = cs->aux_alarm_msg[25];
	v->c[2] = cs->aux_alarm_msg[26];
	v->c[3] = cs->aux_alarm_msg[27];

	return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_8()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_8(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[28];
	v->c[1] = cs->aux_alarm_msg[29];
	v->c[2] = cs->aux_alarm_msg[30];
	v->c[3] = cs->aux_alarm_msg[31];

	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_aux_alarm_msg_9()                                              */
/****************************************************************************/
int rv_aux_alarm_msg_9(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->aux_alarm_msg[32];
	v->c[1] = cs->aux_alarm_msg[33];
	v->c[2] = cs->aux_alarm_msg[34];
	v->c[3] = cs->aux_alarm_msg[35];

	return OK;
}
/****************************************************************************/
/* rv_recipe_entry_mode()                                              */
/****************************************************************************/
int rv_recipe_entry_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char) cs->recipe_entry_mode;
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_recipe_flags()                                              */
/****************************************************************************/
int rv_recipe_flags(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;

	if(cs->job_name_used)
		flags |= FLAG_JOB_NAME_USED;
	if(cs->clear_rec_total)
		flags |= FLAG_CLEAR_REC_TOTAL;
	if(cs->enable_resin_codes)
		flags |= FLAG_ENABLE_RESIN_CODES;
	if(cs->enable_product_code)
		flags |= FLAG_ENABLE_PRODUCT_CODE;
	if(cs->force_density)
		flags |= FLAG_FORCE_DENSITY;
	if(cs->job_wt_in_recipe)
		flags |= FLAG_JOB_WT_IN_RECIPE;
	if(cs->mix_time_in_recipe)
		flags |= FLAG_MIX_TIME_IN_RECIPE;
	if(cs->gate_order_in_recipe)
		flags |= FLAG_GATE_ORDER_IN_RECIPE;
	if(cs->recipe_type)
		flags |= FLAG_RECIPE_TYPE;
	if(cs->density_in_recipe)
		flags |= FLAG_DENSITY_IN_RECIPE;
	if(cs->width_in_recipe)
		flags |= FLAG_WIDTH_IN_RECIPE;

	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_job_name_1
 * Filename: readfun.c    Date: 12/03/02   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int rv_new_job_name_1(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.new_job_name[0];
	v->c[1] = shr_global.new_job_name[1];
	v->c[2] = shr_global.new_job_name[2];
	v->c[3] = shr_global.new_job_name[3];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_job_name_2
 * Filename: readfun.c    Date: 12/03/02   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int rv_new_job_name_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.new_job_name[4];
	v->c[1] = shr_global.new_job_name[5];
	v->c[2] = shr_global.new_job_name[6];
	v->c[3] = shr_global.new_job_name[7];

	return OK;
}
/****************************************************************************
 * Function: rv_job_name_3
 ****************************************************************************/
int rv_new_job_name_3(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.new_job_name[8];
	v->c[1] = shr_global.new_job_name[9];
	v->c[2] = shr_global.new_job_name[10];
	v->c[3] = shr_global.new_job_name[11];

	return OK;
}
/****************************************************************************
 * Function: rv_job_name_4
 ****************************************************************************/
int rv_new_job_name_4(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.new_job_name[12];
	v->c[1] = shr_global.new_job_name[13];
	v->c[2] = shr_global.new_job_name[14];
	v->c[3] = shr_global.new_job_name[15];

	return OK;
}
/****************************************************************************
 * Function: rv_job_name_1
 ****************************************************************************/
int rv_job_name_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->job_name[0];
	v->c[1] = cs->job_name[1];
	v->c[2] = cs->job_name[2];
	v->c[3] = cs->job_name[3];

	return OK;
}
/****************************************************************************
 * Function: rv_job_name_2
 ****************************************************************************/
int rv_job_name_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->job_name[4];
	v->c[1] = cs->job_name[5];
	v->c[2] = cs->job_name[6];
	v->c[3] = cs->job_name[7];

	return OK;
}

/****************************************************************************
 * Function: rv_job_name_3
 ****************************************************************************/
int rv_job_name_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->job_name[8];
	v->c[1] = cs->job_name[9];
	v->c[2] = cs->job_name[10];
	v->c[3] = cs->job_name[11];

	return OK;
}
/****************************************************************************
 * Function: rv_job_name_4
 ****************************************************************************/
int rv_job_name_4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->job_name[12];
	v->c[1] = cs->job_name[13];
	v->c[2] = cs->job_name[14];
	v->c[3] = cs->job_name[15];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp1
 ****************************************************************************/
int rv_device_name_temp1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[0];
	v->c[1] = cs->device_name[1];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp2
 ****************************************************************************/
int rv_device_name_temp2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[2];
	v->c[1] = cs->device_name[3];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp3
 ****************************************************************************/
int rv_device_name_temp3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[4];
	v->c[1] = cs->device_name[5];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp4
 ****************************************************************************/
int rv_device_name_temp4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[6];
	v->c[1] = cs->device_name[7];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp5
 ****************************************************************************/
int rv_device_name_temp5(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[8];
	v->c[1] = cs->device_name[9];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp6
 ****************************************************************************/
int rv_device_name_temp6(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[10];
	v->c[1] = cs->device_name[11];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp7
 ****************************************************************************/
int rv_device_name_temp7(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[12];
	v->c[1] = cs->device_name[13];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_temp8
 ****************************************************************************/
int rv_device_name_temp8(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[14];
	v->c[1] =  '\0';

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_1
 ****************************************************************************/
int rv_device_name_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[0];
	v->c[1] = cs->device_name[1];
	v->c[2] = cs->device_name[2];
	v->c[3] = cs->device_name[3];


	return OK;
}
/****************************************************************************
 * Function: rv_device_name_2
 ****************************************************************************/
int rv_device_name_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[4];
	v->c[1] = cs->device_name[5];
	v->c[2] = cs->device_name[6];
	v->c[3] = cs->device_name[7];

	return OK;
}
/************************************************************************** **
 * Function: rv_device_name_3
 ****************************************************************************/
int rv_device_name_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[8];
	v->c[1] = cs->device_name[9];
	v->c[2] = cs->device_name[10];
	v->c[3] = cs->device_name[11];

	return OK;
}
/****************************************************************************
 * Function: rv_device_name_4
 ****************************************************************************/
int rv_device_name_4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->device_name[12];
	v->c[1] = cs->device_name[13];
	v->c[2] = cs->device_name[14];
	v->c[3] = cs->device_name[15];

	return OK;
}
/****************************************************************************
 * Function: rv_out_of_spec
 ****************************************************************************/
int rv_out_of_spec_limit(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->f = cfgGATE[pd->loop].out_of_spec_limit*100.0;
			break;
		case TYPE_LIW:
			v->f = cfgEXT.out_of_spec_limit*100.0;
			break;
		case TYPE_HO:
			v->f = cfgHO.out_of_spec_limit*100.0;
			break;
		case TYPE_WTH:
			v->f = cfgWTH.out_of_spec_limit*100.0;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_receiver_fill_time
 *                                                                          *
 ****************************************************************************/
int rv_bad_feeds_before_alarm(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].num_bad_feeds_before_alarm;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_receiver_fill_time
 *                                                                          *
 ****************************************************************************/
int rv_receiver_fill_time(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].creceiver.fill_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_receiver_dump_time
 *                                                                          *
 ****************************************************************************/
int rv_receiver_dump_time(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].creceiver.dump_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_primary_gate_channel
 *                                                                          *
 ****************************************************************************/
int rv_second_gate_channel(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].secondary_gate.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_primary_gate_channel
 *                                                                          *
 ****************************************************************************/
int rv_primary_gate_channel(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].primary_gate.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_hopper_type
 *                                                                          *
 ****************************************************************************/
int rv_hopper_type(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].hopper_type;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_algorithm
 *                                                                          *
 ****************************************************************************/
int rv_algorithm(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].algorithm;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_minimum_gate_time
 *                                                                          *
 ****************************************************************************/
int rv_minimum_gate_time(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].minimum_gate_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_tableresolution
 *                                                                          *
 ****************************************************************************/
int rv_tableresolution(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].time_wt_resolution;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_primary_proxy_channel
 *                                                                          *
 ****************************************************************************/
int rv_primary_proxy_channel(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].creceiver.prox_primary.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_primary_output_channel
 *                                                                          *
 ****************************************************************************/
int rv_second_output_channel(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].out_secondary.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_primary_output_channel
 *                                                                          *
 ****************************************************************************/
int rv_primary_output_channel(fic * v, point_data * pd, int *type)
{
	v->i = cfgGATE[pd->loop].creceiver.out_primary.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_set_batch_size
 *                                                                          *
 ****************************************************************************/
int rv_set_batch_size(fic * v,point_data * pd,int * type)
{
	v->f = cfgBWH.set_batch_size;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_ad_filter
 *                                                                          *
 ****************************************************************************/
int rv_ad_filter(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->f = cfgBWH.ad_filter;
			break;
		case TYPE_LIW:
			v->f = cfgEXT.ad_filter;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_max_coast_alarm_time
 *                                                                          *
 ****************************************************************************/
int rv_max_coast_time(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->i = cfgBWH.max_coast_time/TICKSPERSEC;
			break;
		case TYPE_LIW:
			v->i = cfgEXT.max_coast_time/TICKSPERSEC;
			break;
		case TYPE_HO:
			v->i = cfgHO.max_coast_time/TICKSPERSEC;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_ad_filter_table
 *                                                                          *
 ****************************************************************************/
int rv_ad_filter_table(fic * v,point_data * pd,int * type)
{
	v->i = cfgBWH.ad_filter_table;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_adloop_time_factor
 *                                                                          *
 ****************************************************************************/
int rv_adloop_time_factor(fic * v,point_data * pd,int * type)
{
	v->i = cfgBWH.ad_loop_timer_factor;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_min_dump_time
 *                                                                          *
 ****************************************************************************/
int rv_min_dump_time(fic * v,point_data * pd,int * type)
{
	v->i = cfgBWH.min_bwh_dump_time/TICKSPERSEC; /* in mS GTF */
	*type = INT;
		return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: rv_mixer_setup_flags
 *                                                                          *
 ****************************************************************************/
int rv_mixer_setup_flags(fic * v,point_data * pd,int * type)
{
	unsigned int flags = 0;
	if(cfgMIXER.aux_starter_used)
		flags |= FLAG_AUX_STARTER_USED;
	if(cfgMIXER.material_request_invert)
		flags |= FLAG_MATERIAL_REQUEST_INVERT;
	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mixer_motor_mode
 *                                                                          *
 ****************************************************************************/
int rv_mixer_low_level_time(fic * v,point_data * pd,int * type)
{
	v->f = cfgMIXER.mixer_low_level_time;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mixer_motor_mode
 *                                                                          *
 ****************************************************************************/
int rv_mixer_dump_cycle_time(fic * v,point_data * pd,int * type)
{
	v->f = cfgMIXER.dump_cycle_time;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_max_dump_time
 *                                                                          *
 ****************************************************************************/
int rv_max_dump_time(fic * v,point_data * pd,int * type)
{
	v->f = cfgMIXER.max_dump_time;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_total_mix_time
 *                                                                          *
 ****************************************************************************/
int rv_total_mix_time(fic * v,point_data * pd,int * type)
{
	v->f = shrMIXER.total_mix_time/FTICKSPERSEC;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mix_elap_time
 *                                                                          *
 ****************************************************************************/
int rv_mix_elap_time(fic * v,point_data * pd,int * type)
{
	v->f = shrMIXER.mix_elap_time/FTICKSPERSEC;
	*type = FLOAT;
	return OK;
}
 /****************************************************************************
 *                                                                          *
 * Function: rv_last_batch_time
 *                                                                          *
 ****************************************************************************/
int rv_last_batch_time(fic * v,point_data * pd,int * type)
{
	v->f = shrBWH.last_batch_time;
	*type = FLOAT;
	return OK;
}
 /****************************************************************************
 *                                                                          *
 * Function: rv_last_batch_weight
 *                                                                          *
 ****************************************************************************/
int rv_last_batch_weight(fic * v,point_data * pd,int * type)
{
	v->f = shrBWH.last_hop_hold_wt;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_gate_delay_time
 *                                                                          *
 ****************************************************************************/
int rv_gate_delay_time(fic * v,point_data * pd,int * type)
{
	v->f = cfgMIXER.gate_close_delay; /* in mS GTF */
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mixer_motor_mode
 *                                                                          *
 ****************************************************************************/
int rv_mixer_motor_mode(fic * v,point_data * pd,int * type)
{
	v->i = (unsigned char)cfgMIXER.mixer_motor_mode;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mixer_gate_mode
 *                                                                          *
 ****************************************************************************/
int rv_mixer_gate_mode(fic * v,point_data * pd,int * type)
{
	v->i = (unsigned char)cfgMIXER.mixer_gate_mode;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_material_request_source
 *                                                                          *
 ****************************************************************************/
int rv_material_request_source(fic * v,point_data * pd,int * type)
{
	v->i = (unsigned char)cfgMIXER.material_request_source;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_dc_pump_relay_chan
 *                                                                          *
 ****************************************************************************/
int rv_dc_pump_relay_chan(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.dc_pump_relay.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_cont_run_relay_chan
 *                                                                          *
 ****************************************************************************/
int rv_cont_run_relay_chan(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.cont_run_relay.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_anx_cont_chan
 *                                                                          *
 ****************************************************************************/
int rv_anx_cont_chan(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.anx_cont.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_dust_a_relay_chan
 *                                                                          *
 ****************************************************************************/
int rv_dust_a_relay_chan(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.dust_a_relay.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_dust_b_relay_chan
 *                                                                          *
 ****************************************************************************/
int rv_dust_b_relay_chan(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.dust_b_relay.chan;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_pump_flags
 *                                                                          *
 ****************************************************************************/
int rv_pump_flags(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;
	unsigned int flags = 0;

	if(cs->cpump.dust_used)
		flags |= FLAG_DUST_USED;
	if(cs->cpump.cont_used)
		flags |= FLAG_CONT_USED;
	v->i = flags;

	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_pump_clean_time
 *                                                                          *
 ****************************************************************************/
int rv_pump_clean_time(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.clean_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_pump_continuous_run_time
 *                                                                          *
 ****************************************************************************/
int rv_pump_continuous_run_time(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.continuous_run_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_pump_restart_time
 *                                                                          *
 ****************************************************************************/
int rv_pump_restart_time(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.restart_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_pump_start_delay_time
 *                                                                          *
 ****************************************************************************/
int rv_pump_start_delay_time(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = cs->cpump.start_delay_time;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_low_alarm_weight
 *                                                                          *
 ****************************************************************************/
int rv_low_alarm_weight(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.alarm_wt;
	*type = FLOAT;

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_critical_weight
 *                                                                          *
 ****************************************************************************/
int rv_critical_weight(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.crit_wt;
	*type = FLOAT;

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_min_dump_weight
 *                                                                          *
 ****************************************************************************/
int rv_min_dump_weight(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.min_dump_wt;
	*type = FLOAT;

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_high_speed_alarm
 *                                                                          *
 ****************************************************************************/
int rv_recipe_high_speed_alarm(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.warning_high_spd;
			break;
		case TYPE_HO:
			v->f = cfgHO.warning_high_spd;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_low_speed_alarm
 *                                                                          *
 ****************************************************************************/
int rv_recipe_low_speed_alarm(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.warning_low_spd;
			break;
		case TYPE_HO:
			v->f = cfgHO.warning_low_spd;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_drive_high_speed_limit
 *                                                                          *
 ****************************************************************************/
int rv_drive_high_speed_limit(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.alarm_high_spd;
			break;
		case TYPE_HO:
			v->f = cfgHO.alarm_high_spd;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_drive_low_speed_limit
 *                                                                          *
 ****************************************************************************/
int rv_drive_low_speed_limit(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.alarm_low_spd;
			break;
		case TYPE_HO:
			v->f = cfgHO.alarm_low_spd;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_rs_maxchg
 *                                                                          *
 ****************************************************************************/
int rv_max_speed_change(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.rs_maxchg*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.rs_maxchg*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_control_dead_band
 *                                                                          *
 ****************************************************************************/
int rv_control_dead_band(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.dead_band*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.dead_band*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_rate_speed_table_reset
 *                                                                          *
 ****************************************************************************/
int rv_rate_speed_table_reset(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.rs_resetchg*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.rs_resetchg*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_max_error_before_updates
 *                                                                          *
 ****************************************************************************/
int rv_max_error_before_updates(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.max_error*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.max_error*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_percent_error_to_coast
 *                                                                          *
 ****************************************************************************/
int rv_percent_error_to_coast(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.coast_error*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.coast_error*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_coast_tolerance
 *                                                                          *
 ****************************************************************************/
int rv_coast_tolerance(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.coast_tolerance;
			break;
		case TYPE_HO:
			v->i = cfgHO.coast_tolerance;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_zero_crossing_speed
 *                                                                          *
 ****************************************************************************/
int rv_zero_crossing_speed(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.zero_crossing*100;
			break;
		case TYPE_HO:
			v->f = cfgHO.zero_crossing*100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_max_spd
 *                                                                          *
 ****************************************************************************/
int rv_max_spd(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.max_spd;
			break;
		case TYPE_HO:
			v->f = cfgHO.max_spd;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_spd_factor
 *                                                                          *
 ****************************************************************************/
int rv_spd_factor(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.spd_factor;
			break;
		case TYPE_HO:
			v->f = cfgHO.spd_factor[0];
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_min_wtp
 *                                                                          *
 ****************************************************************************/
int rv_min_wtp(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.min_wtp;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_demo_max_wtp
 *                                                                          *
 ****************************************************************************/
int rv_demo_max_wtp(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.demo_max_wtp;
			break;
		case TYPE_HO:
			v->f = cfgHO.demo_max_ltp;
			break;
		case TYPE_MIX:
			v->f = cfgMIXER.demo_max_wtp;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_loading_on_weight
 *                                                                          *
 ****************************************************************************/
int rv_loading_on_weight(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.load_wt_on;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_loading_off_weight
 *                                                                          *
 ****************************************************************************/
int rv_loading_off_weight(fic * v,point_data * pd,int * type)
{
	v->f = cfgEXT.load_wt_off;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_loading_time
 *                                                                          *
 ****************************************************************************/
int rv_loading_time(fic * v,point_data * pd,int * type)
{
	v->i = cfgEXT.load_time / FTICKSPERSEC;
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_hopper_flags(fic * v, point_data * pd, int *type)
{
	unsigned int flags = 0;

	if(cfgEXT.drive_inhibit_invert)
		flags |= FLAG_DI_SIGNAL_ACTIVE_ON;
	if(cfgEXT.sync)
		flags |= FLAG_SYNC_WEIGHT_TO_EXT_SCREW;
	if(cfgEXT.use_secondary_weight_card)
		flags |= FLAG_USE_SECOND_WM;
	if(cfgEXT.use_internal_loading_signal_only)
		flags |= FLAG_USE_INTERNAL_LOADING;

	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_dm_network_add(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.drive_mod.addr;
			break;
		case TYPE_HO:
			if (pd->loop == HO_LOOP)
				v->i = cfgHO.drive_mod.addr;
			else
				v->i = cfgSHO.drive_mod.addr;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_dm_network_chan(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.drive_mod.chan+1;
			break;
		case TYPE_HO:
			if (pd->loop == HO_LOOP)
				v->i = cfgHO.drive_mod.chan+1;
			else
				v->i = cfgSHO.drive_mod.chan+1;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_wm_network_add (fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.weigh_mod.addr;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_wm_network_chan (fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.weigh_mod.chan+1;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_wm_good_message(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->f = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].good_msgs;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].good_msgs;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_wm_bad_message(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->f = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].timeouts;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].timeouts;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_wm_hardwarerev(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->f = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].hardware_rev;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].hardware_rev;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_wm_hardwareid(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->f = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].hardware_id;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].hardware_id;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_wm_softwarerev(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->f = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].software_rev;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].software_rev;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_load_relay
 *                                                                          *
 ****************************************************************************/
int rv_load_relay(fic * v,point_data * pd,int * type)
{
	short addr, chan;

	if(!cfgEXT.use_secondary_weight_card)
	{
		chan = (short)cfgEXT.load_relay.chan+1;
	}
	else
	{
		chan = (short)cfgEXT.load_relay.chan;
	}

	addr = (short)cfgEXT.load_relay.addr;
	v->i = (unsigned int) (chan + (addr << 8));

	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_alarm_relay
 *                                                                          *
 ****************************************************************************/
int rv_alarm_relay(fic * v,point_data * pd,int * type)
{
	int retcode = OK;
	short addr, chan;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			addr = (short)cfgEXT.alarm_relay.addr;
			chan = (short)cfgEXT.alarm_relay.chan+1;
			v->i = (unsigned int) (chan + (addr << 8));
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_drive_fail_tolerance
 *                                                                          *
 ****************************************************************************/
int rv_drive_fail_tolerance(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.drive_fail_error;
			break;
		case TYPE_HO:
			v->i = cfgHO.drive_fail_error;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: rv_max_drive_fail_before_alarm
 *                                                                          *
 ****************************************************************************/
int rv_max_drive_fail_before_alarm(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.max_drive_fail;
			break;
		case TYPE_HO:
			v->i = cfgHO.max_drive_fail;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;

}

/****************************************************************************
 *                                                                          *
 * Function: rv_num_of_moving_points
 *                                                                          *
 ****************************************************************************/
int rv_movepts(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.movepts;
			break;
		case TYPE_HO:
			v->i = cfgHO.movepts;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;


}
/****************************************************************************
 *                                                                          *
 * Function: rv_time_between_updates
 *                                                                          *
 ****************************************************************************/
int rv_time_between_updates(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.calctime/100;
			break;
		case TYPE_HO:
			v->i = cfgHO.calctime/100;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: rv_initial_coast_value
 *                                                                          *
 ****************************************************************************/
int rv_initial_coast_value(fic * v,point_data * pd,int * type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.initial_coast;
			break;
		case TYPE_HO:
			v->i = cfgHO.initial_coast;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;

}
/****************************************************************************
****************************************************************************/
int rv_gate_flags(fic * v, point_data * pd, int *type)
{
	unsigned int flags = 0;

	if(cfgGATE[pd->loop].prepulse_enabled)
		flags |= ENABLE_PREPULSE;  /* value 1 */

	if(cfgGATE[pd->loop].clear_recipe_on_crit_low)
		flags |= FLAG_CLEAR_RECIPE_ON_CRIT_LOW;  /* value 1 */

	if(cfgGATE[pd->loop].secondary_gate_enabled)
		flags |= USE_SECONDARY_GATE;  /* value 2 */

	if(cfgGATE[pd->loop].clear_gate_cal_after_out_of_spec)
		flags |= CLEAR_GATE_CAL_AFTER_OUTOFSPEC;   /* value 4 */

	if(cfgGATE[pd->loop].use_powder_receiver)
		flags |= USE_POWDER_RECEIVER;   /* value 4 */

	if(cfgGATE[pd->loop].auger_used)
		flags |= USE_AUGER_TO_FEED;   /* value 16 */

	v->i = flags;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_gate_pulse_number(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->i = cfgGATE[pd->loop].gate_pulse_number;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}

/****************************************************************************
****************************************************************************/
int rv_num_self_recals(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->i = shrGATE[pd->loop].num_self_recals;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}

/****************************************************************************
****************************************************************************/
int rv_num_of_feeds(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->i = shrGATE[pd->loop].num_feeds;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}


/****************************************************************************
 *                                                                          *
 * Function: rv_min_ltp
 *                                                                          *
 ****************************************************************************/
int rv_min_ltp(fic * v,point_data * pd,int * type)
{
	v->f = cfgHO.min_ltp;
	*type = FLOAT;
	return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: rv_ho_flags
 *                                                                          *
 ****************************************************************************/
int rv_ho_flags(fic * v,point_data * pd,int * type)
{
	unsigned int flags = 0;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_HO:
			if(cfgHO.stop_instantly)
				flags |= FLAG_STOP_INSTANTLY;
			if(cfgHO.drive_inhibit_invert)
				flags |= FLAG_DI_SIGNAL_ACTIVE_ON;
			if(cfgHO.rs_clip)
				flags |= FLAG_LIMIT_SPEED_CHANGE;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	v->i = flags;
	*type = INT;

		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_GearInputMethod
 *                                                                          *
 ****************************************************************************/
int rv_GearInputMethod(fic * v,point_data * pd,int * type)
{
	v->i = cfgHO.GearInputMethod;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_alarm_wth_min
 *                                                                          *
 ****************************************************************************/
int rv_alarm_wth_min(fic * v,point_data * pd,int * type)
{
	v->f = cfgWTH.alarm_wth_min;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_alarm_wth_max
 *                                                                          *
 ****************************************************************************/
int rv_alarm_wth_max(fic * v,point_data * pd,int * type)
{
	v->f = cfgWTH.alarm_wth_max;
	*type = FLOAT;
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_mode
 *                                                                          *
 ****************************************************************************/
int rv_mode(fic * v,point_data * pd,int * type)
{
	v->i = (unsigned char)cfgWTH.mode;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_wth_type
 *                                                                          *
 ****************************************************************************/
int rv_wth_type(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->wth_type;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_width_port_number
 *                                                                          *
 ****************************************************************************/
int rv_width_port_number(fic * v,point_data * pd,int * type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned char)cs->width_port.number;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_width_address
 *                                                                          *
 ****************************************************************************/
int rv_width_address(fic * v,point_data * pd,int * type)
{
	v->i = cfgWTH.width_address;
	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp1
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
		v->c[0] = cs->job_name[0];
		v->c[1] = cs->job_name[1];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp2
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[2];
	v->c[1] = cs->job_name[3];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp3
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[4];
	v->c[1] = cs->job_name[5];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp4
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[6];
	v->c[1] = cs->job_name[7];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp5
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp5(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[8];
	v->c[1] = cs->job_name[9];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp6
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp6(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[10];
	v->c[1] = cs->job_name[11];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp7
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp7(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[12];
	v->c[1] = cs->job_name[13];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp8
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp8(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[14];
	v->c[1] = cs->job_name[15];
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_temp9
 * Filename: readfun.c    Date: 1/03/15   Author:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_temp9(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
		
	v->c[0] = cs->job_name[16];
	v->c[1] = '\0';
	
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_recipe_name_1
 * Filename: readfun.c    Date: 12/03/02   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int rv_recipe_name_1(fic * v, point_data * pd, int *type)
{
	int i;

	for (i = 0;i < 4 ;i++ )
		v->c[i] = BATCH_RECIPE_NOW->product_code[i];

	return OK;
}
/****************************************************************************
 * Function: rv_recipe_name_2
 ****************************************************************************/
int rv_recipe_name_2(fic * v, point_data * pd, int *type)
{
	int i;

	for (i = 0;i < 4 ;i++ )    
		v->c[i] = BATCH_RECIPE_NOW->product_code[i+4];
	return OK;
}
/****************************************************************************
 * Function: rv_recipe_name_3
 ****************************************************************************/
int rv_recipe_name_3(fic * v, point_data * pd, int *type)
{
	int i;

	for (i = 0;i < 4 ;i++ )          
		v->c[i] = BATCH_RECIPE_NOW->product_code[i+8];

	return OK;
}
/****************************************************************************
 * Function: rv_recipe_name_4
 ****************************************************************************/
int rv_recipe_name_4(fic * v, point_data * pd, int *type)
{
	int i;

	for (i = 0;i < 4 ;i++ )   
		v->c[i] = BATCH_RECIPE_NOW->product_code[i+12];
      
	return OK;

}
/****************************************************************************
 * Function: rv_next_recipe_name_1
 *                                                                          *
 ****************************************************************************/
int rv_next_recipe_name_1(fic * v, point_data * pd, int *type)
{
	int i;
	recipe_struct *r = &BATCH_RECIPE_EDIT;
   
	for (i = 0;i < 4 ;i++ )   
		v->c[i] = r->product_code[i];
	return OK;
}
/****************************************************************************
 * Function: rv_next_recipe_name_2
 ****************************************************************************/
int rv_next_recipe_name_2(fic * v, point_data * pd, int *type)
{
	int i;
	recipe_struct *r = &BATCH_RECIPE_EDIT;
   
	for (i = 0;i < 4 ;i++ )   
		v->c[i] = r->product_code[i+4];
	return OK;
}
/****************************************************************************
 * Function: rv_next_recipe_name_3
 ****************************************************************************/
int rv_next_recipe_name_3(fic * v, point_data * pd, int *type)
{
	int i;
	recipe_struct *r = &BATCH_RECIPE_EDIT;
   
	for (i = 0;i < 4 ;i++ )   
		v->c[i] = r->product_code[i+8];
	return OK;
}
/****************************************************************************
 * Function: rv_next_recipe_name_4
 ****************************************************************************/
int rv_next_recipe_name_4(fic * v, point_data * pd, int *type)
{
	int i;
	recipe_struct *r = &BATCH_RECIPE_EDIT;
   
	for (i = 0;i < 4 ;i++ )   
		v->c[i] = r->product_code[i+12];

	return OK;
}
/****************************************************************************
 * Function: rv_resin_name_temp1
 ****************************************************************************/
int rv_resin_name_temp1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[0];
	v->c[1] = cs->resin_table[resin_num].resin_desc[1];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp2
 ****************************************************************************/
int rv_resin_name_temp2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[2];
	v->c[1] = cs->resin_table[resin_num].resin_desc[3];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp3
 ****************************************************************************/
int rv_resin_name_temp3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[4];
	v->c[1] = cs->resin_table[resin_num].resin_desc[5];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp4
 ****************************************************************************/
int rv_resin_name_temp4(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[6];
	v->c[1] = cs->resin_table[resin_num].resin_desc[7];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp5
 ****************************************************************************/
int rv_resin_name_temp5(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[8];
	v->c[1] = cs->resin_table[resin_num].resin_desc[9];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp6
 ****************************************************************************/
int rv_resin_name_temp6(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[10];
	v->c[1] = cs->resin_table[resin_num].resin_desc[11];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp7
 ****************************************************************************/
int rv_resin_name_temp7(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[12];
	v->c[1] = cs->resin_table[resin_num].resin_desc[13];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp8
 ****************************************************************************/
int rv_resin_name_temp8(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[14];
	v->c[1] = cs->resin_table[resin_num].resin_desc[15];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_temp9
 ****************************************************************************/
int rv_resin_name_temp9(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[16];
	v->c[1] = '\0';

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_1
 ****************************************************************************/
int rv_resin_name_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = r->resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[0];
	v->c[1] = cs->resin_table[resin_num].resin_desc[1];
	v->c[2] = cs->resin_table[resin_num].resin_desc[2];
	v->c[3] = cs->resin_table[resin_num].resin_desc[3];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_2
 ****************************************************************************/
int rv_resin_name_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = BATCH_RECIPE_EDIT.resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[4];
	v->c[1] = cs->resin_table[resin_num].resin_desc[5];
	v->c[2] = cs->resin_table[resin_num].resin_desc[6];
	v->c[3] = cs->resin_table[resin_num].resin_desc[7];

	return(OK);
}
/****************************************************************************
 * Function: rv_resin_name_3
 ****************************************************************************/
int rv_resin_name_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short resin_num;

	if(OPER_MODE != PAUSE_MODE)
		resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	else
		resin_num = BATCH_RECIPE_EDIT.resin_recipe_num[pd->hop];

	v->c[0] = cs->resin_table[resin_num].resin_desc[8];
	v->c[1] = cs->resin_table[resin_num].resin_desc[9];
	v->c[2] = cs->resin_table[resin_num].resin_desc[10];
	v->c[3] = '\0';

	return(OK);
}
/****************************************************************************
 * Function: rv_receiver_status
 ****************************************************************************/
int rv_receiver_status(fic * v, point_data * pd, int *type)
{
	unsigned int flags = 0;

	if(shrGATE[pd->hop].sreceiver.receiving_on)
		flags |= FLAG_LEFT_SOLENOID;
	if(shrGATE[pd->hop].sreceiver.dumping_on)
		flags |= FLAG_DUMP_STATUS;
	if(shrGATE[pd->hop].sreceiver.prox_on)
		flags |= FLAG_PROXY_SWITCH;
	if(shrGATE[pd->hop].sreceiver.out_relay_on)
		flags |= FLAG_FLAPPER_VALVE;

	v->i = flags;
	*type = INT;

		return OK;
}
/****************************************************************************
 * Function: rv_pump_status
 ****************************************************************************/
int rv_pump_status(fic * v, point_data * pd, int *type)
{
	unsigned int flags = 0;

	if(shr_global.spump.dc_pump_on)
		flags |= FLAG_PUMP_STATUS;
	if(shr_global.spump.dust_a_on)
		flags |= FLAG_DUST_A;
	if(shr_global.spump.dust_b_on)
		flags |= FLAG_DUST_B;
	if(shr_global.spump.cont_run_on)
		flags |= FLAG_CONTINUOUS_RUN;

	v->i = flags;
	*type = INT;
		return OK;

}
/****************************************************************************
 * Function: rv_manual_backup_status
 ****************************************************************************/
int rv_manual_backup_status(fic * v, point_data * pd, int *type)
{
	unsigned int flags = 0;
	int i;

	/* if(shr_global.manual_backup_state[0]) */
	/*       flags |= R_MAN_MIXER_MOTOR;   */
	/*    if(shr_global.manual_backup_state[1])    */
	/*       flags |= R_MAN_MIXER_GATE;    */
	/*    if(shr_global.manual_backup_state[2])    */
	/*       flags |= R_MAN_BATCH_GATE;    */
	/*    if(shr_global.manual_backup_state[3])    */
	/*       flags |= R_MAN_ALARM;         */
	for(i=0; i<16; i++)
	{
		if(shr_global.manual_backup_state[i])
			flags |= 1 << i;
	}

	v->i = flags;
	*type = INT;
		return OK;
}
/****************************************************************************
 * Function: rv_nip_circumference
 ****************************************************************************/
int rv_nip_circumference(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{

		case TYPE_HO:
			if (pd->loop == HO_LOOP)
				v->f = cfgHO.nip_circumference;
			else
				v->f = cfgSHO.nip_circumference;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 * Function: rv_pulses_per_rev
 ****************************************************************************/
int rv_pulses_per_rev(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{

		case TYPE_HO:
			if (pd->loop == HO_LOOP)
				v->f = cfgHO.pulses_per_rev;
			else
				v->f = cfgSHO.pulses_per_rev;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_lm_network_add(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.load_relay.addr;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_lm_network_chan(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.load_relay.chan+1;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;

}
/****************************************************************************
****************************************************************************/
int rv_num_max_error_before_updates(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.max_bad;
			break;
		case TYPE_HO:
			v->i = cfgHO.max_bad;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_current_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = shrEXT.curbits;
			break;
		case TYPE_FLF:
			v->i = shrGFLUFF.curbits;
			break;
		case TYPE_HO:
			v->i = shrHO.curbits;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_delta_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_HO:
			v->i = shrHO.delta_b;
			break;
		case TYPE_LIW:
			v->i = shrEXT.delta_b;
			break;
		case TYPE_FLF:
			v->i = shrGFLUFF.delta_b;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_pulse_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_HO:
			v->i = shrHO.pulse_bits;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_coast(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = shrEXT.coast;
			break;
		case TYPE_FLF:
			v->i = shrGFLUFF.coast;
			break;
		case TYPE_HO:
			v->i = shrHO.coast;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_coasts(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.coasts;
			break;
		case TYPE_HO:
			v->i = cfgHO.coasts;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_drive_fail_count(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = shrEXT.drive_fail_count;
			break;
		case TYPE_FLF:
			v->i = shrGFLUFF.drive_fail_count;
			break;
		case TYPE_HO:
			v->i = shrHO.drive_fail_count;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_recipe_errors(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	v->i = 0;

	/*    shr_global.invalid_remote_recipe = (unsigned int) (validate_recipe(&cfg_super, &pd->cnf_LOOP[0], &pd->shr_LOOP[0], g, (float) 0.0, PMEM.RECIPE, &cfg_super.recipe_errors[0])); */

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			v->i =  shr_global.recipe_errors[MAX_LOOP];
			break;
		case TYPE_HO:
			v->i =  shr_global.recipe_errors[HO_LOOP];
			break;
		case TYPE_BWH:
			v->i =  shr_global.recipe_errors[pd->loop];
			break;
		case TYPE_LIW:
			v->i =  shr_global.recipe_errors[pd->loop];
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_alarm_setting(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
#if 0
	config_super *cs = &cfg_super;
	int i;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_LIW) && (cs->alarms[i].device == pd->hop))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}

				}
			}
			break;
		case TYPE_SYSTEM:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_SYSTEM))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_HO:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_HO) && (!cs->alarms[i].device))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_BWH:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_BWH) && (!cs->alarms[i].device))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_MIX:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_MIX) && (!cs->alarms[i].device))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_GATE:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_GATE) && ((cs->alarms[i].device == pd->hop)))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_WTH:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_WTH) && (!cs->alarms[i].device))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		case TYPE_FLF:
			if (shr_global.current_alarm_status==55)
			{
				v->i = shr_global.current_alarm_status;
			}
			else
			{
				for (i = 0; i < MAX_ALARMS; i++)
				{
					if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_FLF) && (!cs->alarms[i].device))
					{
						v->c[3] = cs->alarms[i].pause_action;
						v->c[2] = cs->alarms[i].man_action;
						v->c[1] = cs->alarms[i].auto_action;
						v->c[0] = 0;

						return OK;
					}
				}
			}
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
#endif
	return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_simulation(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int retcode = OK;
	unsigned int flags;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			flags = 0;
			if (cs->demo)
				flags |= FLAG_SIMU_SYSTEM;
			else
				flags &= ~FLAG_SIMU_SYSTEM;
			v->i = flags;
			break;
		case TYPE_GATE:
			flags = 0;
			if (cfgGATE[pd->loop].demo)
				flags |= FLAG_SIMU_GATE;
			else
				flags &= ~FLAG_SIMU_GATE;

			v->i = flags;
			break;
		case TYPE_BWH:
			flags = 0;
			if (cfgBWH.demo)
				flags |= FLAG_SIMU_BWH;
			else
				flags &= ~FLAG_SIMU_BWH;
			v->i = flags;

			break;
		case TYPE_MIX:
			flags = 0;
			if (cfgMIXER.demo)
				flags |= FLAG_SIMU_MIXER;
			else
				flags &= ~FLAG_SIMU_MIXER;
			v->i = flags;

			break;
		case TYPE_HO:
			flags = 0;
			if(cfgHO.demo & DEMO_DRIVE_MOD)
			flags |= FLAG_SIMU_DRIVE_MODULE;
			else
			flags &= ~FLAG_SIMU_DRIVE_MODULE;

			if(cfgHO.demo & DEMO_PULSES)
				flags |= FLAG_SIMU_PULSES;
			else
				flags &= ~FLAG_SIMU_PULSES;

			v->i = flags;
			break;
		case TYPE_WTH:
			flags = 0;

			if (cfgWTH.demo)
				flags |= FLAG_SIMU_WIDTH;
			else
				flags &= ~FLAG_SIMU_WIDTH;
			v->i = flags;

			break;
		case TYPE_LIW:
		default:
			flags = 0;

			if(cfgEXT.demo & DEMO_DRIVE_MOD)
				flags |= FLAG_SIMU_DRIVE_MODULE;
			else
				flags &= ~FLAG_SIMU_DRIVE_MODULE;

			if(cfgEXT.demo & DEMO_WEIGH_MOD)
				flags |= FLAG_SIMU_WEIGH_MODULE;
			else
				flags &= ~FLAG_SIMU_WEIGH_MODULE;

			v->i = flags;
			break;
	}

	*type = INT;
		return retcode;

}
/****************************************************************************
****************************************************************************/
int rv_dm_good_message(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.drive_mod.addr].good_msgs;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.drive_mod.addr].good_msgs;
			break;
		case TYPE_HO:
			v->i = (unsigned int)shr_global.stats[cfgHO.drive_mod.addr].good_msgs;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_dm_bad_message(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.drive_mod.addr].timeouts;
			break;
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.drive_mod.addr].timeouts;
			break;
		case TYPE_HO:
			v->i = (unsigned int)shr_global.stats[cfgHO.drive_mod.addr].timeouts;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_dm_hardwareid(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.drive_mod.addr].hardware_id;
			break;
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.drive_mod.addr].hardware_id;
			break;
		case TYPE_HO:
			v->i = (unsigned int)shr_global.stats[cfgHO.drive_mod.addr].hardware_id;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_dm_softwarerev(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.drive_mod.addr].software_rev;
			break;
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.drive_mod.addr].software_rev;
			break;
		case TYPE_HO:
			v->i = (unsigned int)shr_global.stats[cfgHO.drive_mod.addr].software_rev;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_dm_hardwarerev(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.drive_mod.addr].hardware_rev;
			break;
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.drive_mod.addr].hardware_rev;
			break;
		case TYPE_HO:
			v->i = (unsigned int)shr_global.stats[cfgHO.drive_mod.addr].hardware_rev;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_control_status(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	unsigned int flags;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			flags = 0;
			if (shr_global.unable_to_talk_weigh_mod)
			flags |= FLAG_UNABLE_TALK_TO_WEIGH_MODULE;
			if(shrEXT.remote_cal_zero_done)
				flags |= FLAG_ZERO_WT_BITS_READY;
			if(shrEXT.remote_cal_all_done)
				flags |= FLAG_CALIBRATION_DONE;
			v->i = flags;
			break;
		case TYPE_HO:
			flags = 0;
			if(shrHO.ramping)
				flags |= FLAG_RAMPING;
			v->i = flags;
			break;
		case TYPE_SYSTEM:
			flags = 0;
			if(shr_global.remote_system_restart)
			flags |= FLAG_MINOP_NEED_TO_REBOOT;
			if (shr_global.spawn_shell_port_1)
				flags |= FLAG_SPAWN_SHELL_PORT_1;
			if (shr_global.spawn_shell_port_3_type232)
				flags |= FLAG_SPAWN_SHELL_PORT_3_TYPE232;
			if (shr_global.spawn_shell_port_3_type422)
				flags |= FLAG_SPAWN_SHELL_PORT_3_TYPE422;

			if (shr_global.com_port_test_done)
				flags |= FLAG_COM_PORT_TEST;  /* used as DONE */
			if (shr_global.com_port_test_all_passed)
				flags |= FLAG_COM_PORT_ALL_PASSED;

			if (shr_global.io_test_done && !shr_global.if_had_sec_io_card)      /* used as DONE for both io test */
				flags |= FLAG_IO_TEST_1;
			if (shr_global.io_test_done && shr_global.if_had_sec_io_card)      /* used as DONE for both io test */
				flags |= FLAG_IO_TEST_2;
			if (shr_global.com_port_open_fail)
				flags |= shr_global.com_port_open_fail;

			v->i = flags;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return retcode;

}
/****************************************************************************
****************************************************************************/
int rv_avg_dump_size(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.avg_dump_size;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_percent_grav(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			if (cfgEXT.accum_grav <= 0.0001 && cfgEXT.accum_coast <= 0.0001)
				v->f = 0.0;
			else
				v->f = (cfgEXT.accum_grav / (cfgEXT.accum_coast + cfgEXT.accum_grav))*100;

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = FLOAT;
	return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_hopper_rs_factor(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->f = cfgEXT.rs_ave;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = FLOAT;
	return retcode;
}
/****************************************************************************
 * Function: rv_test_wt
 ****************************************************************************/
int rv_test_wt(fic * v, point_data * pd, int *type)
{
	int retcode = OK;


	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->f = cfgBWH.test_wt;
			break;
		case TYPE_LIW:
		default:
			v->f = cfgEXT.test_wt;
			break;
	}

	*type = FLOAT;
	return retcode;

}
/****************************************************************************
****************************************************************************/
int rv_zerowt_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->i = cfgBWH.zerowt;
			break;
		case TYPE_LIW:
		default:
			v->i = cfgEXT.zerowt;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_offset_wt(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			/*v->i = cfgBWH.offsetbits;*/
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}

/****************************************************************************
****************************************************************************/
int rv_testwt_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->i = cfgBWH.testwt;
			break;
		case TYPE_LIW:
		default:
			v->i = cfgEXT.testwt;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_raw_wt_bits(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->i = shrBWH.raw_wt_bits;
			break;
		case TYPE_LIW:
		default:
			v->i = shrEXT.wt_bits;
			break;
	}

	*type = INT;

		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_wm_calibrates(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = (unsigned int)shr_global.stats[cfgEXT.weigh_mod.addr].cal_counter;
			break;
		case TYPE_FLF:
			v->i = (unsigned int)shr_global.stats[cfgGFLUFF.weigh_mod.addr].cal_counter;
			break;
		case TYPE_BWH:
			v->i = (unsigned int)shrBWH.cal_counter;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_calibrate_status(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	unsigned int flags;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			flags = shrBWH.remote_calibration_status;
			if (shr_global.unable_to_talk_weigh_mod)
				flags |= FLAG_UNABLE_TALK_TO_WEIGH_MODULE;
			if(shrBWH.remote_cal_zero_done)
				flags |= FLAG_ZERO_WT_BITS_READY;
			if(shrBWH.remote_cal_all_done)
				flags |= FLAG_CALIBRATION_DONE;
			if(shrBWH.remote_calibration_status)
				flags |= shrBWH.remote_calibration_status;
			if (shr_global.rem_calib_zero_scale_done)
				flags |= FLAG_REM_CALIB_ZERO_SCALE_DONE;
			if (shr_global.rem_calib_auto_tare_done)
				flags |= FLAG_REM_CALIB_AUTO_TARE_DONE;

			v->i = flags;
			break;
		case TYPE_LIW:
		default:
			flags = shrEXT.remote_calibration_status;
			if (shr_global.unable_to_talk_weigh_mod)
			flags |= FLAG_UNABLE_TALK_TO_WEIGH_MODULE;
			if(shrEXT.remote_cal_zero_done)
			flags |= FLAG_ZERO_WT_BITS_READY;
			if(shrEXT.remote_cal_all_done)
				flags |= FLAG_CALIBRATION_DONE;
			if(shrEXT.remote_calibration_status)
				flags |= shrEXT.remote_calibration_status;
			if (shr_global.rem_calib_zero_scale_done)
				flags |= FLAG_REM_CALIB_ZERO_SCALE_DONE;
			if (shr_global.rem_calib_auto_tare_done)
				flags |= FLAG_REM_CALIB_AUTO_TARE_DONE;

			v->i = flags;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_mac_address_1(fic * v, point_data * pd, int *type)
{
	int i;

	for (i =0; i<6; i++)
		shr_global.mac_address[i] = i;

	v->c[0] = shr_global.mac_address[0];
	v->c[1] = shr_global.mac_address[1];
	v->c[2] = shr_global.mac_address[2];
	v->c[3] = shr_global.mac_address[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_mac_address_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.mac_address[4];
	v->c[1] = shr_global.mac_address[5];
	v->c[2] = 0;
	v->c[3] = 0;

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_serial_number_1(fic * v, point_data * pd, int *type)
{
	int i;

	for (i=0;i<6;i++)
		shr_global.serial_number[i] = i;

	v->c[0] = shr_global.serial_number[0];
	v->c[1] = shr_global.serial_number[1];
	v->c[2] = shr_global.serial_number[2];
	v->c[3] = shr_global.serial_number[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_serial_number_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.serial_number[4];
	v->c[1] = shr_global.serial_number[5];
	v->c[2] = 0;
	v->c[3] = 0;

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_1(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version[0];
	v->c[1] = shr_global.sw_version[1];
	v->c[2] = shr_global.sw_version[2];
	v->c[3] = shr_global.sw_version[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version[4];
	v->c[1] = shr_global.sw_version[5];
	v->c[2] = shr_global.sw_version[6];
	v->c[3] = shr_global.sw_version[7];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_3(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version[8];
	v->c[1] = shr_global.sw_version[9];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_date_1(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version_date[0];
	v->c[1] = shr_global.sw_version_date[1];
	v->c[2] = shr_global.sw_version_date[2];
	v->c[3] = shr_global.sw_version_date[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_date_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version_date[4];
	v->c[1] = shr_global.sw_version_date[5];
	v->c[2] = shr_global.sw_version_date[6];
	v->c[3] = shr_global.sw_version_date[7];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_sw_version_date_3(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.sw_version_date[8];
	v->c[1] = shr_global.sw_version_date[9];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_fw_version_1(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->Info.FirmwareNum[0];
	v->c[1] = cs->Info.FirmwareNum[1];
	v->c[2] = cs->Info.FirmwareNum[2];
	v->c[3] = cs->Info.FirmwareNum[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_fw_version_2(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->Info.FirmwareNum[4];
	v->c[1] = cs->Info.FirmwareNum[5];
	v->c[2] = cs->Info.FirmwareNum[6];
	v->c[3] = cs->Info.FirmwareNum[7];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_fw_version_3(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = cs->Info.FirmwareNum[8];
	v->c[1] = cs->Info.FirmwareNum[9];
	v->c[2] = cs->Info.FirmwareNum[10];
	v->c[3] = cs->Info.FirmwareNum[11];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_wtp_unit(fic *v, point_data *pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;

	v->i = (unsigned int) cs->wtp_unit.i;
#endif
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_language(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->i = (unsigned int) cs->language;
	*type = INT;
		return OK;

}

/****************************************************************************
****************************************************************************/
int rv_security_flags(fic * v, point_data * pd, int *type)
{
	v->i = shr_global.password_validation;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_super_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->sup_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_sub_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->pro_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_device_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->dev_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_remote_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->rem_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_web_server_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->rptask_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_high_priority(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	v->i = cs->high_pr;
#endif

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_current_uptime(fic * v, point_data * pd, int *type)
{
#if 0
	config_super *cs = &cfg_super;
	unsigned int32 time_stamp,date,tick;
	unsigned int16 day;

	_os9_getime(3,&time_stamp,&date,&day,&tick);
	time_stamp = (time_stamp * TICKSPERSEC) + (date * (86400*TICKSPERSEC)) + (tick & 0xffff);
	v->i = (time_stamp-cs->diagnostics_start) / (TICKSPERSEC * 3600);
#endif

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_number_of_recipes(fic * v, point_data * pd, int *type)
{
	v->i = (unsigned int)MAX_RECIPES;
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_debug_who(fic * v, point_data * pd, int *type)
{
	v->i = shr_global.dbg_who;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_num_of_cycles(fic * v, point_data * pd, int *type)
{
	v->i = shr_global.cycle_of_testing;
	*type = INT;
		return OK;

}

/****************************************************************************
****************************************************************************/
int rv_debug_what(fic * v, point_data * pd, int *type)
{

	v->i = shr_global.dbg_what;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_debug_remote_low_pt(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;
	r_diag = &(shr_global.rem_diag);

	v->i = r_diag->dbg_remote_low_pt;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_debug_remote_high_pt(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;
	r_diag = &(shr_global.rem_diag);

	v->i = r_diag->dbg_remote_high_pt;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_recipe_mode(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
#if 0
	/*    v->c[0] = cs->control_mode; */
	v->c[1] = cs->recipe_mode[0];
	v->c[2] = cs->recipe_mode[1];
	v->c[3] = cs->recipe_mode[2];
#endif
	v->c[2] = cs->recipe_mode[0];
	v->c[1] = cs->recipe_mode[1];
	v->c[0] = cs->recipe_mode[2];

	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_recipe_menu(fic * v, point_data * pd, int *type)
{
	v->c[1] = get_recipe_1_menu(pd);  /* set recipe 1 Menu */
	v->c[2] = get_recipe_2_menu(pd);  /* set recipe 2 Menu */
	v->c[3] = get_recipe_3_menu(pd);  /* set recipe 3 Menu */

	*type = INT;
		return OK;

}

/****************************************************************************
****************************************************************************/
char get_recipe_1_menu(point_data * pd)
{
	config_super *cs = &cfg_super;
	char menu;

	switch (cs->application)
	{
		case BLOWN_FILM:
		case SHEET:
			switch (cs->control_mode)
			{
				case MWTP_MLTP:
				case MWTP_CLTP:
					menu = REC1_WPL_WPA_THICK;  /* set recipe 1 Menu */
					break;
				case MWTP_ULTP:
					menu = REC1_WPL_WPA_THICK;
					break;
				case CWTP_MLTP:
				case CWTP_ULTP:
					menu = REC1_WTP_WPL_WPA_THICK;  /* set recipe 1 Menu */
					break;
				case CWTP_CLTP:
					menu = REC1_WPL_WPA_THICK;  /* set recipe 1 Menu */
					break;
			}
			break;
		case PIPE_AND_TUBING:
			switch (cs->control_mode)
			{
				case MWTP_MLTP:
				case MWTP_CLTP:
					menu = REC1_WPL_ID_OD_THICK;  /* set recipe 1 Menu */
					break;
				case MWTP_ULTP:
					menu = REC1_WTP_WPA_WPL_ID_OD_THICK;
					break;
				case CWTP_MLTP:
				case CWTP_ULTP:
					menu = REC1_WTP_WPL_OD_THICK;  /* set recipe 1 Menu */
					break;
				case CWTP_CLTP:
					menu = REC1_WPL_ID_OD_THICK;  /* set recipe 1 Menu */
					break;
			}
			break;
		case WIRE_AND_CABLE:
			switch (cs->control_mode)
			{
				case MWTP_MLTP:
				case MWTP_CLTP:
					menu = REC1_WPL_OD_THICK;  /* set recipe 1 Menu */
					break;
				case MWTP_ULTP:
					menu = REC1_WTP_WPL_OD_THICK;
					break;
				case CWTP_MLTP:
				case CWTP_ULTP:
					menu = REC1_WTP_WPL_OD_THICK;  /* set recipe 1 Menu */
					break;
				case CWTP_CLTP:
					menu = REC1_WPL_OD_THICK;  /* set recipe 1 Menu */
					break;
			}
			break;
		case PROFILE:
			switch (cs->control_mode)
			{
				case MWTP_MLTP:
				case MWTP_CLTP:
					menu = REC1_WPL_WPA;  /* set recipe 1 Menu */
					break;
				case MWTP_ULTP:
					menu = REC1_WTP_WPL_WPA;
					break;
				case CWTP_MLTP:
				case CWTP_ULTP:
					menu = REC1_WTP_WPL_WPA;  /* set recipe 1 Menu */
					break;
				case CWTP_CLTP:
					menu = REC1_WPL_WPA;  /* set recipe 1 Menu */
					break;
			}
			break;
	}


	return(menu);

}

/****************************************************************************
****************************************************************************/
char get_recipe_2_menu(point_data * pd)
{
	config_super *cs = &cfg_super;
	char menu;

	switch (cs->recipe_mode[0])
	{
		case WPA:
			menu = REC2_WIDTH;
			break;
		case THICKNESS:
			switch (cs->application)
			{
				case BLOWN_FILM:
				case SHEET:
					menu = REC2_WIDTH;
					break;
				case WIRE_AND_CABLE:
					menu = REC2_ID;
					break;
				case PIPE_AND_TUBING:
					menu = REC2_ID_OD;
					break;
			}
			break;
		case WTP:
			/*          if ((cs->application == PIPE_AND_TUBING) && (cs->control_mode == ONF_MWTP_MLTP)) */
			/*             menu = REC2_THICK;                                                            */
			/*          else                                                                             */
			menu = REC_NONE;
			break;
		default:
			menu = REC_NONE;
	}

	return(menu);

}
/****************************************************************************
****************************************************************************/
char get_recipe_3_menu(point_data * pd)
{
	config_super *cs = &cfg_super;
	char menu;

	switch (cs->recipe_mode[0])
	{
		case NONE:
		case WTP:
			menu = REC_NONE;
			break;
		case WPL:
		case WPA:
		case THICKNESS:
			switch (cs->control_mode)
			{
				case MWTP_MLTP:
				case MWTP_CLTP:
				case CWTP_MLTP:
					menu = REC_NONE;
					break;
				case MWTP_ULTP:
				case CWTP_ULTP:
					menu = REC3_LTP;
					break;
				case CWTP_CLTP:
					menu = REC3_WTP_LTP;
					break;
			}
			break;

	}
	return(menu);

}

/****************************************************************************
****************************************************************************/
#define NOT_USED  0
#define PRINTER   1
#define REMOTE    2
#define REMOTE_OP 3
#define WIDTH_CONTROLLER   4
#define GRAVILINK 5

int rv_port_1_device_baud_rate(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch ((unsigned int) cs->device_on_port[1])
	{
		case REMOTE:
			v->i = cs->remote_port.baud;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.baud;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.baud;
			break;
		case GRAVILINK:
			v->i = cs->remote_gravitrol_link.baud;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_port_3_device_baud_rate(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch ((unsigned int) cs->device_on_port[3])
	{
		case REMOTE:
			v->i = cs->remote_port.baud;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.baud;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.baud;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_port_1_device_parity(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->device_on_port[1])
	{
		case REMOTE:
			v->i = cs->remote_port.parity;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.parity;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.parity;
			break;
		case GRAVILINK:
			v->i = cs->remote_gravitrol_link.parity;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_port_3_device_parity(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch (cs->device_on_port[3])
	{
		case REMOTE:
			v->i = cs->remote_port.parity;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.parity;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.parity;
			break;
		case GRAVILINK:
			v->i = cs->remote_gravitrol_link.parity;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_port_1_device_port_type(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch ((unsigned int) cs->device_on_port[1])
	{
		case REMOTE:
			v->i = cs->remote_port.options;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.options;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.options;
			break;
		case GRAVILINK:
			v->i = cs->remote_gravitrol_link.options;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_port_3_device_port_type(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	switch ((unsigned int) cs->device_on_port[3])
	{
		case REMOTE:
			v->i = cs->remote_port.options;
			break;
		case REMOTE_OP:
			v->i = cs->remote_int_port.options;
			break;
		case WIDTH_CONTROLLER:
			v->i = cs->width_port.options;
			break;
		case GRAVILINK:
			v->i = cs->remote_gravitrol_link.options;
			break;
		default: /* NOT_USED */
			v->i = 0;
			break;
	}

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_ip_address(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = (unsigned char)cs->ip_address[0];
	v->c[1] = (unsigned char)cs->ip_address[1];
	v->c[2] = (unsigned char)cs->ip_address[2];
	v->c[3] = (unsigned char)cs->ip_address[3];

	return OK;


}
/****************************************************************************
****************************************************************************/
int rv_subnet_mask(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = (unsigned char)cs->subnet_mask[0];
	v->c[1] = (unsigned char)cs->subnet_mask[1];
	v->c[2] = (unsigned char)cs->subnet_mask[2];
	v->c[3] = (unsigned char)cs->subnet_mask[3];

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_gateway(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;

	v->c[0] = (unsigned char)cs->gateway[0];
	v->c[1] = (unsigned char)cs->gateway[1];
	v->c[2] = (unsigned char)cs->gateway[2];
	v->c[3] = (unsigned char)cs->gateway[3];

	return OK;

}
/****************************************************************************
****************************************************************************/
int rv_current_alarm_num(fic * v, point_data * pd, int *type)
{

	v->i = (unsigned int)shr_global.current_alarm_num;
	*type = INT;
		return OK;

}
/****************************************************************************
****************************************************************************/
int rv_spawn_counts(fic * v, point_data * pd, int *type)
{

	v->i = (unsigned int)shr_global.proc_deaths;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_process_status(fic * v, point_data * pd, int *type)
{

	v->i = (unsigned int)shr_global.last_proc_status;

	*type = INT;
		return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: rv_device_name_1
 * Filename: readfun.c    Date: 12/03/02   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int rv_last_process_spawned_1(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.last_proc_to_die[0];
	v->c[1] = shr_global.last_proc_to_die[1];
	v->c[2] = shr_global.last_proc_to_die[2];
	v->c[3] = shr_global.last_proc_to_die[3];

	return OK;
}
/****************************************************************************
 ****************************************************************************/
int rv_last_process_spawned_2(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.last_proc_to_die[4];
	v->c[1] = shr_global.last_proc_to_die[5];
	v->c[2] = shr_global.last_proc_to_die[6];
	v->c[3] = shr_global.last_proc_to_die[7];

	return OK;
}
/****************************************************************************
 ****************************************************************************/
int rv_last_process_spawned_3(fic * v, point_data * pd, int *type)
{

	v->c[0] = shr_global.last_proc_to_die[8];
	v->c[1] = shr_global.last_proc_to_die[9];
	v->c[2] = '\0';
	v->c[3] = '\0';

	return OK;
}
/****************************************************************************
****************************************************************************/
int rv_last_point_processed(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;

	r_diag = &(shr_global.rem_diag);
	v->i = (unsigned int)r_diag->last_point_processed;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_mbenet_receives(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;

	r_diag = &(shr_global.rem_diag);
	v->i = (unsigned int)r_diag->good_mbenet_recvs;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_mbenet_writes(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;

	r_diag = &(shr_global.rem_diag);
	v->i = (unsigned int)r_diag->good_mbenet_writes;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_abtcp_receives(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;

	r_diag = &(shr_global.rem_diag);
	v->i = (unsigned int)r_diag->good_abtcp_recvs;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_abtcp_writes(fic * v, point_data * pd, int *type)
{
	struct port_diag_data_str *r_diag;

	r_diag = &(shr_global.rem_diag);
	v->i = (unsigned int)r_diag->good_abtcp_writes;

	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_backup_restore_control(fic * v, point_data * pd, int *type)
{

	v->i = 0;

#if 0
	if (!shr_global.backup_restore_if_pcmcia_present)
		v->i |= PCMCIA_CARD_NOT_PRESENT;
	if (shr_global.backup_restore_pcmcia_block_num_exist)
		v->i |= PCMCIA_BLOCK_EXISTING;
	if (shr_global.backup_restore_done)
		v->i |= BACKUP_RESTORE_DONE;
#endif

	v->i |= shr_global.backup_restore_error;

	*type = INT;
		return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_drive_mod_address
 *                                                                          *
 ****************************************************************************/
int rv_config_relay_location(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	relay_desc *relay;
	short addr, chan;

	int retcode = OK;

	relay = &cs->pio_relays[shr_global.config_relay_num];
	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = relay->location.addr;
			chan = relay->location.chan;
			v->i = chan + (addr << 8);
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_drive_mod_address
 *                                                                          *
 ****************************************************************************/
int rv_remote_stop_out(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->ok_to_run_output_address.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_drive_mod_address
 *                                                                          *
 ****************************************************************************/
int rv_config_relay_flags(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	relay_desc *relay;
	unsigned int flags = 0;

	int retcode = OK;

	relay = &cs->pio_relays[shr_global.config_relay_num];
	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			if (relay->active)
				flags |= 1;
			if (relay->invert)
			flags |= 2;
			if (cs->remote_relay_stop_end_batch)
				flags |= 4;
			if (cs->ok_to_run_output_signal)
				flags |= 8;
			if (cs->remote_purge)
				flags |= 16;

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}
	v->i = flags;
	return retcode;

}
/****************************************************************************/
/****************************************************************************/
int rv_clean_pulse_duration(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->f = cs->purge_duration;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/****************************************************************************/
int rv_clean_pulse_cyletime(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->f = cs->purge_cycle_time;

	*type = FLOAT;

	return OK;
}
/****************************************************************************/
/****************************************************************************/
int rv_clean_toppurge_time(fic *v,point_data *pd, int *type)
{
	config_super *cs = &cfg_super;

	v->f = cs->top_purge_time;

	*type = FLOAT;

	return OK;
}
/****************************************************************************
 ****************************************************************************/
int rv_remote_purge_stoprelay(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->remote_purge_complete_relay.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 ****************************************************************************/
int rv_remote_purge_startrelay(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->remote_purge_start_relay.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 ****************************************************************************/
int rv_selfclean_purge1(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->purge_valve_1.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 ****************************************************************************/
int rv_selfclean_purge2(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->purge_valve_2.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************
 ****************************************************************************/
int rv_selfclean_purge3(fic * v,point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	short addr, chan;
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_SYSTEM:
			addr = 0;
			chan = cs->purge_valve_3.chan;
			v->i = chan + (addr << 8);

			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	return retcode;

}
/****************************************************************************/
/* rv_accel()                                                               */
/****************************************************************************/
int rv_recipe_use(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_GATE:
			v->i = cfgGATE[pd->loop].recipe_use;
			break;

		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
 * Function: rv_next_recipe_name_1
 ****************************************************************************/
int rv_num_recipe_name_1(fic * v, point_data * pd, int *type)
{
	//config_super *cs = &cfg_super;
	//int i;
	//char * temp;

	//temp = (char *) read_recipe_desc(cs,pd->rec,(unsigned int)shr_global.num_recipe_inven);
   //if (temp != NULL)
   //{
   //   for (i = 0;i < 4 ;i++ )
   //      v->c[i] = temp[i];
   //}
	return OK;
}
/****************************************************************************
 * Function: rv_next_recipe_name_2
 ****************************************************************************/
int rv_num_recipe_name_2(fic * v, point_data * pd, int *type)
{
	//config_super *cs = &cfg_super;
	//int i;
	//char * temp;

	//temp = (char *) read_recipe_desc(cs,pd->rec,(unsigned int)shr_global.num_recipe_inven);
   //if (temp != NULL)
   //{
   //   for (i = 0;i < 4 ;i++ )
   //      v->c[i] = temp[i+4];
   //}
	return OK;

}
/****************************************************************************
 * Function: rv_next_recipe_name_3
 ****************************************************************************/
int rv_num_recipe_name_3(fic * v, point_data * pd, int *type)
{
	//config_super *cs = &cfg_super;
	//int i;
	//char * temp;

	//temp = (char *) read_recipe_desc(cs,pd->rec,(unsigned int)shr_global.num_recipe_inven);
   //if (temp != NULL)
  // {
   //   for (i = 0;i < 4 ;i++ )
   //      v->c[i] = temp[i+8];
   //}
	return OK;
}
/****************************************************************************
 * Function: rv_next_recipe_name_4
 ****************************************************************************/
int rv_num_recipe_name_4(fic * v, point_data * pd, int *type)
{
	//config_super *cs = &cfg_super;
	//int i;
	//char * temp;

	//temp = (char *) read_recipe_desc(cs,pd->rec,(unsigned int)shr_global.num_recipe_inven);
   //if (temp != NULL)
   //{
   //   for (i = 0;i < 4 ;i++ )
   //      v->c[i] = temp[i+12];
  // }
	return OK;
}
/****************************************************************************
 * Function: rv_num_recipe_inven
 ****************************************************************************/
int rv_num_recipe_inven(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code || (cs->current_temp_recipe_num > 0))
	{
		v->f = read_recipe_inven(cs,(stored_recipe *)pd->rec,shr_global.num_recipe_inven);
	}
	else
		v->f = 0.0;

	*type = FLOAT;
	return (retcode);
}
/****************************************************************************/
/* rv_number_for_recipe_inven()                                                         */
/****************************************************************************/
int rv_number_for_recipe_inven(fic *v,point_data *pd, int *type)
{

	v->i = (unsigned int)shr_global.num_recipe_inven;
	*type = INT;
		return OK;
}

/****************************************************************************
****************************************************************************/
int rv_loads(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_LIW:
			v->i = cfgEXT.loads;
			break;
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/****************************************************************************
****************************************************************************/
int rv_new_gate_order(fic * v, point_data * pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_GATE:
			v->i = BATCH_RECIPE_EDIT.gate_order[pd->loop].number;
			break;     
		default:
			retcode = INVALID_POINT;
			break;
	}

	*type = INT;
		return retcode;
}
/************************************************************************/
/* rv_stretch_update()                                                       */
/************************************************************************/
int rv_stretch_update(fic *v,point_data *pd, int *type)
{
	if (pd->dev_type != TYPE_HO)
		return(INVALID_POINT);

	v->f = cfgHO.stretch_update_delay;

	return(OK);
}
/****************************************************************************/
/* rv_resin_num()                                                         */
/****************************************************************************/
int rv_resin_num(fic *v,point_data *pd, int *type)
{

	v->i = (unsigned int)BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
	*type = INT;
		return OK;
}
/****************************************************************************/
/* rv_temp_resin_num()                                                         */
/****************************************************************************/
int rv_temp_resin_num(fic *v,point_data *pd, int *type)
{
	v->i = (unsigned int)BATCH_RECIPE_EDIT.resin_recipe_num[pd->hop];
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_hardware_test_1(fic * v, point_data * pd, int *type)
{

	v->i = shr_global.io_test_fail_1;
	*type = INT;
		return OK;
}
/****************************************************************************
****************************************************************************/
int rv_hardware_test_2(fic * v, point_data * pd, int *type)
{

	v->i = shr_global.io_test_fail_2;
	*type = INT;
		return OK;
}

/****************************************************************************

****************************************************************************/
int rv_read_dc_ad(fic *v,point_data *pd, int *type) /* BMM 1*/
{
	v->i = (unsigned int) PLC.StrainGauge[EXT_ADNum].AnalogInput01;
	*type = INT;
		return OK;
}

int rv_loading_controlled_remotely(fic *v, point_data *pd, int *type) /* BMM 1*/
{
	config_super *cs = &cfg_super;
	v->i = cs->loading_controlled_remotely;

	*type = INT;
		return OK;
}

int rv_loadin_dig_io(fic *v, point_data *pd, int *type) /* BMM 1*/
{
	if (IO_dig[0].channel[pd->hop + 37] == ON) 
		v->i = 1;
	else 
		v->i = 0;

	*type = INT;
		return OK;
}

int rv_loadout_dig_io(fic *v, point_data *pd, int *type) /* BMM 1*/
{
	if (IO_dig[0].channel[pd->hop + 55] == ON) v->i = 1;
	else v->i = 0;

	*type = INT;
		return OK;
}

int rv_pump_aux_input(fic *v, point_data *pd, int *type) /* BMM 1*/
{
	if (IO_dig[0].channel[1] == ON) v->i = 1;
	else v->i = 0;

	*type = INT;
		return OK;
}

/****************************************************************************
 * Function: rv_special_devices64
 ****************************************************************************/
int rv_special_devices64(fic * v, point_data * pd, int *type)
{
	config_super *cs = &cfg_super;
	int resin_num, recipe_num, resin_offset, recipe_offset, i;
	int retcode = OK;
	char null_str[5];
	char *recipe_name;

	switch (pd->dev_type)
	{
		case TYPE_RESIN_NAME:
			resin_num = (pd->reg - 1) / REGS_PER_STRING;
			resin_offset = ((pd->reg - 1) % REGS_PER_STRING) * 4;
			for (i = 0; i < 4; i++)
			{
				if (resin_offset + i < RESIN_LENGTH)
					v->c[i] = cs->resin_table[resin_num].resin_desc[resin_offset + i];
				else
					v->c[i] = 0;
			}
			*type = RESIN_STRING;
			break;
		case TYPE_RESIN_DATA:
			resin_num = (pd->reg - 1) / REGS_PER_STRING;
			resin_offset = ((pd->reg - 1) % REGS_PER_STRING);
			switch (resin_offset)
			{
				case R2_RESIN_DENSITY:
					v->f = cs->resin_table[resin_num].density;
					*type = FLOAT;
					break;
				case R2_RESIN_INVEN_WT:
					v->d = PMEM.dresin_inven[resin_num];
					*type = DOUBLE;
					break;
			}

			break;
		case TYPE_RECIPE_NAME:
			recipe_num = (pd->reg / REGS_PER_STRING) + 1;
			recipe_offset = (((pd->reg ) % REGS_PER_STRING)-1) * 4;
			if(cs->enable_product_code)
			{
				recipe_name = (char*)read_recipe_desc(cs,pd->rec,recipe_num);

				for (i = 0; i < 4; i++)
				{
					if (recipe_offset + i < 15)
						v->c[i] = *(recipe_name + recipe_offset + i);
					else
						v->c[i] = 0;
				}
			}
			else
				v->i = (unsigned int)&null_str[0];

			*type = RESIN_STRING;
			break;
		case TYPE_RECIPE_DATA:
			recipe_num = (pd->reg / REGS_PER_STRING) + 1;
			if(cs->enable_product_code)
			{
				v->d = RECIPE_INVEN_WT64(recipe_num);
			}
			else
				v->d = 0.0;

			*type = DOUBLE;
			break;
		case TYPE_SYSTEM:
			break;

		default:
			v->i = 0;
			retcode = INVALID_POINT;
			break;
	}
	return (retcode);
}
/****************************************************************************
 * Function: rv_current_recipe_inven64
 ****************************************************************************/
int rv_current_recipe_inven64(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code)
		v->d = RECIPE_INVEN_WT(shr_global.current_recipe_num);
	else
		v->d = 0.0;

	*type = DOUBLE;
	return (retcode);
}
/****************************************************************************
 * Function: rv_next_recipe_inven64
 ****************************************************************************/
int rv_next_recipe_inven64(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code)
		v->d = RECIPE_INVEN_WT(cs->current_temp_recipe_num);
	else
		v->d = 0.0;

	*type = DOUBLE;
	return (retcode);
}
/****************************************************************************/
/* rv_inven_wt64()                                                            */
/****************************************************************************/
int rv_inven_wt64(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->d = (double)SYS_INVEN_WT64;
			break;
		case TYPE_GATE:
			v->d = (double)GATE_INVEN_WT64(pd->hop);
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->d = (double) EXT_INVEN_WT64;
			break;
		case TYPE_SYSTEM:
			v->d = (double)SYS_INVEN_WT64;
			break;
		default:
			v->d = (double)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = DOUBLE;
	return retcode;
}

/****************************************************************************/
/* rv_last_inven_wt64()                                                            */
/****************************************************************************/
int rv_last_inven_wt64(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch(pd->dev_type)
	{
		case TYPE_BWH:
			v->d = (double)SYS_LAST_INVEN_WT64;
			break;
		case TYPE_GATE:
			v->d = (double)GATE_LAST_INVEN_WT64(pd->hop);
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->d = (double)EXT_LAST_INVEN_WT64;
			break;
		case TYPE_SYSTEM:
			v->d = (double)SYS_LAST_INVEN_WT64;
			break;
		default:
			v->d = (double)0.0;
			retcode = INVALID_POINT;
			break;
	}

	*type = DOUBLE;
	return retcode;
}
/****************************************************************************/
/* rv_shift_wt64()                                                            */
/****************************************************************************/
int rv_shift_wt64(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->d = (double)SYS_SHIFT_WT64;
			break;
		case TYPE_GATE:
			v->d = (double)GATE_SHIFT_WT64(pd->hop);
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->d = (double) EXT_SHIFT_WT64;
			break;
		case TYPE_SYSTEM:
			v->d = (double)SYS_SHIFT_WT64;
			break;
		default:
			v->d = (double)0.0;
			retcode = (int)INVALID_POINT;
			break;
	}

	*type = DOUBLE;
	return(retcode);
}
/****************************************************************************/
/* rv_last_shift_wt64()                                                            */
/****************************************************************************/
int rv_last_shift_wt64(fic *v,point_data *pd, int *type)
{
	int retcode = OK;

	switch (pd->dev_type)
	{
		case TYPE_BWH:
			v->d = (double)SYS_LAST_SHIFT_WT64;
			break;
		case TYPE_GATE:
			v->d = (double)shrGATE[pd->hop].last_shift_wt;
			break;
		case TYPE_LIW:
			if (pd->loop == EXT_LOOP)
				v->d = (double) shrEXT.last_shift_wt;
			break;
		case TYPE_SYSTEM:
			v->d = (double)SYS_LAST_SHIFT_WT64;
			break;
		default:
			v->d = (double)0.0;
			retcode = (int)INVALID_POINT;
			break;
	}

	*type = FLOAT;
	return(retcode);
}

/****************************************************************************/
/* rv_inven_len64()                                                           */
/****************************************************************************/
int rv_inven_len64(fic *v,point_data *pd, int *type)
{
	v->d = (double)PMEM.HO.dInvTotal;

	*type = DOUBLE;
	return OK;
}
/****************************************************************************/
/* rv_last_inven_len64()                                                           */
/****************************************************************************/
int rv_last_inven_len64(fic *v,point_data *pd, int *type)
{
	v->d = (double)HO_LAST_INVEN_LEN64;

	*type = DOUBLE;
	return OK;
}
/****************************************************************************/
/* rv_shift_len64()                                                           */
/****************************************************************************/
int rv_shift_len64(fic *v,point_data *pd, int *type)
{
	v->d = (double)HO_SHIFT_TOTAL64;

	*type = DOUBLE;
	return OK;
}

/****************************************************************************/
/* rv_last_shift_len64()                                                           */
/****************************************************************************/
int rv_last_shift_len64(fic *v,point_data *pd, int *type)
{
	v->d = (double)HO_LAST_SHIFT_LEN64;

	*type = DOUBLE;
	return OK;
}
/****************************************************************************/
/* rv_inven_area64()                                                          */
/****************************************************************************/
int rv_inven_area64(fic *v,point_data *pd, int *type)
{
	v->d = (double)HO_INVEN_AREA64;

	*type = DOUBLE;
	return OK;
}

/****************************************************************************/
/* rv_shift_area64()                                                           */
/****************************************************************************/
int rv_shift_area64(fic *v,point_data *pd, int *type)
{
	v->d = (double)HO_SHIFT_AREA64;
    
	*type = DOUBLE;
	return OK;
}
/****************************************************************************
 * Function: rv_num_recipe_inven64()
 ****************************************************************************/
int rv_num_recipe_inven64(fic * v, point_data * pd, int *type)
{
	int retcode = OK;
	config_super *cs = &cfg_super;

	if(cs->enable_product_code || (cs->current_temp_recipe_num > 0))
		v->d = RECIPE_INVEN_WT64(shr_global.num_recipe_inven);
	else
		v->d = 0.0;

	*type = DOUBLE;
	return (retcode);
}
