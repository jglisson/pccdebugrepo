#include "pcc_math.h"

float calc_rms_error(samples *error_data)
{
    float error;
    int length;

    length = error_data->num_taken;
    if(length > error_data->length) length = error_data->length;
    error = calc_rms_error_p(error_data,length);

    return error;
}


