#include <stdio.h>
#include <math.h>
#include "pcc_math.h"

signed short iroundf(float f)
{
    if(f >= 0.0)
        f += .5;
    else
        f -= .5;
    return((int)(f));
}


