#include "pcc_math.h"

signed short  clear_table(data_table *table, float initx,float inity)
{
    int i;
    ordered_pair *d;

    d = table->data;

    for(i=0;i<table->max_length;++i)
    {
        d->x = initx;
        d->y = inity;
        ++d;
    }
    table->current_length = 0;
    return(0);
}


