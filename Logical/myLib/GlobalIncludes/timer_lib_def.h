/*********************************************************
* FILE    	: 		timer_lib.h
*
* OBJECT	: 	N/A
*
* OVERVIEW	: 	Common Prototypes and Dependancies
*
* VERSION	: 	v2.0
*           
*
* HISTORY : 		02.20.06 	- Version 1
				05.28.08	- Add FIFO Type
*				
*
*				
*
* DEPENDANCIES:
*********************************************************/
#include <bur/plc.h>			
#include <bur/plctypes.h>
#include "SYS_LIB.h"
#include <AsString.h>
#include <string.h>
#include <standard.h>
#include <brsystem.h>
#include <StateLib.h>

/********************************************************
* BLOCK POINTERS:
*********************************************************/
TON_typ *t1,*t2,*t3;
SM_obj *sm;

/********************************************************
* DEFINITIONS:
*********************************************************/

/* General */
#define ON							1
#define OFF							0
#define YES							1
#define NO							0 

/* State Machine */
#define BASE_SM						0
#define BASE_SM2					6
#define ERROR_SM					100
#define BEGIN_SM 		sm_serv( sm );TExec(t1);TExec(t2);TExec(t3);switch( sm->Step ) {  
#define END_SM   		default:   break;}
#define INC_SM 			sm->Step++
#define GO_BASE_SM		sm->Step = BASE_SM
#define GO_BASE_SM2	sm->Step = BASE_SM2
#define GOTO_SM			sm->Step = 
#define RESET_SM		sm->Step = 0
#define GO_ERROR_SM	sm->ErrorStep	= sm->Step;sm->Step	= ERROR_SM

/* Timer */
#define IF_TMR1 if ( TDone( t1 ) )
#define IF_TMR2 if ( TDone( t2 ) )
#define IF_TMR3 if ( TDone( t3 ) )
#define TB sm_text( 
#define TE , sm, 0 );
#define TMR1S TGo( t1, 
#define TMRE );
#define T2S TGo( t2, 


#define TMR1_20s 	TGo( t1, 20.0 )
#define TMR1_15s 	TGo( t1, 15.0 )
#define TMR1_10s 	TGo( t1, 10.0 )
#define TMR1_5s 		TGo( t1, 5.0 )
#define TMR1_2s 		TGo( t1, 2.0 )
#define TMR1_1s 		TGo( t1, 1.0 )
#define TMR1_500ms	TGo( t1, 0.5 )
#define TMR1_250ms	TGo( t1, 0.25 )
#define TMR1_100ms	TGo( t1, 0.1 )

#define TMR2_20s 	TGo( t2, 20.0 )
#define TMR2_15s 	TGo( t2, 15.0 )
#define TMR2_10s 	TGo( t2, 10.0 )
#define TMR2_5s 		TGo( t2, 5.0 )
#define TMR2_2s 		TGo( t2, 2.0 )
#define TMR2_1s 		TGo( t2, 1.0 )
#define TMR2_500ms	TGo( t2, 0.5 )
#define TMR2_250ms	TGo( t2, 0.25 )
#define TMR2_100ms	TGo( t2, 0.1 )

#define TMR3_20s 	TGo( t3, 20.0 )
#define TMR3_15s 	TGo( t3, 15.0 )
#define TMR3_10s 	TGo( t3, 10.0 )
#define TMR3_5s 		TGo( t3, 5.0 )
#define TMR3_2s 		TGo( t3, 2.0 )
#define TMR3_1s 		TGo( t3, 1.0 )
#define TMR3_500ms	TGo( t3, 0.5 )
#define TMR3_250ms	TGo( t3, 0.25 )
#define TMR3_100ms	TGo( t3, 0.1 )

#define FIFO_TYPE_NOREPEAT		0
#define FIFO_TYPE_STANDARD	1
#define FIFO_INIT_OK			1
#define FIFO_ADD_EXISTS 		10
#define FIFO_ADD_FULL			11
#define FIFO_ADD_OK 			15
#define FIFO_CYC_OK 			20
#define FIFO_CLR_OK				2
/* EOF */
