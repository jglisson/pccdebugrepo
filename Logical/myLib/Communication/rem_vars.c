#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  rem_var.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\rem_vars.c_v  $
 *
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.remVars to toggle debug information in this file
 *
 *    Rev 1.2   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.1   Apr 04 2008 11:32:50   vidya
 * Changed cs->temp_recipe to PMEM.RECIPE->
 *
 *    Rev 1.0   Feb 11 2008 11:11:04   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <math.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "vars.h"
#include "register.h"
#include "remote.h"
#include "remsub.h"
#include "libbgdbg.h"

int write_remote_var(point_data *, unsigned int , fic );
void parse_alternate_set(point_data *,fic);
int identify_user_block_16(point_data *, unsigned int,int *,int *);
int user_block_write_16(point_data *, unsigned int *,unsigned int);
void set_ext_spd(point_data *, float);
void set_motor_device(point_data *, int loop, int cmd);
void set_gate_device(point_data *, int loop,int device_num,int status);
int rem_parse_register_16(point_data *, unsigned int);
extern int rem_parse_register(point_data *, unsigned int);
int translate_remote_register_16(point_data *, unsigned int *);
int read_resin_name(fic * v, point_data * pd, int *type);
int read_job_name(fic * v, point_data * pd, int *type);
int read_recipe_name(fic * v, point_data * pd, int *type);
int read_device_name(fic * v, point_data * pd, int *type);

extern int is_recipe_used(void);
extern int (*fp_read_var[800])(fic *, point_data *, int *);

char null_str[5];

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/****************************************************************************/
/* int read_remote_var()                                                           */
/****************************************************************************/
int read_remote_var(point_data *pd, unsigned int p,fic *v,int *pt_type)
{
   config_super *cs = &cfg_super;
   int retcode;
   int fn_offset=0;
   recipe_struct temp_recipe;
   char use_fn_tbl = TRUE;
  
   /* NOTE: cannot initialize the return value v to anything since it might be carrying */
   /* the recipe number to check in is_recipe_used()                                    */

   ++shr_global.rem_diag.rem_point_cnt;

   /* if return value is < 0 then was reading the block definition */

   /* register number is returned in p                             */
   /* correct value needs to be place in v.i 					   */
   retcode = translate_remote_register_16(pd,&p);
   
   if(retcode == DEFINED_POINT_REG)
   {
      v->i = (unsigned int)p;
      *pt_type = INT;
      return OK;
   }
   
   shr_global.rem_diag.last_point_processed = p; /* Copy for service diagnostic displays */

   if (p == NOT_USED)
      return (INVALID_POINT);

   retcode = OK;

   pd->dev_type = rem_parse_register_16(pd, p);
   fn_offset = MAX_PCC_REG;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         fn_offset *= SYS_FUNC_OFFSET;
         break;
      case TYPE_GATE:
         fn_offset *= GATE_FUNC_OFFSET;
         break;
      case TYPE_LIW:
         fn_offset *= LIW_FUNC_OFFSET;
         break;
      case TYPE_HO:
         fn_offset *= HO_FUNC_OFFSET;
         break;
      case TYPE_BWH:
         fn_offset *= BWH_FUNC_OFFSET;
         break;
      case TYPE_MIX:
         fn_offset *= MIX_FUNC_OFFSET;
         break;
      case TYPE_WTH:
         fn_offset *= WTH_FUNC_OFFSET;
         break;
      case TYPE_FLF:
         fn_offset *= FLF_FUNC_OFFSET;
         break;
      default:
         use_fn_tbl = FALSE;
         break;
   }

   /* set up return data type */
   if( pd->reg < R_INTEGER )
      *pt_type = FLOAT;
   else
      *pt_type = INT;

   if(use_fn_tbl)
   {
      if(pd->reg > 0 && pd->reg <= MAX_PCC_REG)
      { 
         if(p < DESCRIP_OFFSET)
         	(*fp_read_var[fn_offset + pd->reg])(v, pd, pt_type);
         else
         {
            switch (pd->dev_type)
            {
               case TYPE_GATE:
                  switch (pd->reg)
                  {
                     case R_RESIN_NAME_1:
                     case R_RESIN_NAME_2:
                     case R_RESIN_NAME_3:
                        retcode = read_resin_name(v,pd,pt_type);
                        break;
                     case R_JOB_NAME_1:
                     case R_JOB_NAME_2:
                     case R_JOB_NAME_3:
                     case R_JOB_NAME_4:
                        retcode = read_job_name(v,pd,pt_type);
                        break;
                     case R_RECIPE_NAME_1:
                     case R_RECIPE_NAME_2:
                     case R_RECIPE_NAME_3:
                     case R_RECIPE_NAME_4:
                        retcode = read_recipe_name(v,pd,pt_type);
                        break;
                     case R_DEVICE_NAME_1:
                     case R_DEVICE_NAME_2:
                     case R_DEVICE_NAME_3:
                     case R_DEVICE_NAME_4:
                        retcode = read_device_name(v,pd,pt_type);
                        break;
                     default:
                        retcode = INVALID_POINT;
                        break;
                  }
                  break;
               default:
                  retcode = INVALID_POINT;
            } /* end switch dev_typr */
         } /* end max pcc reg */
      }/* end use fnc tbl */         
      else    /* these are leftovers from register.h, above 20000 (system) */
      {
         switch(pd->dev_type)
         {
            case TYPE_SYSTEM:
               switch(pd->reg)
               {
                  case R_AUX_OUT_ACT:
                     retcode = INVALID_POINT;
                     break;
                  case R_AUX_OUT_VOLTS:
                     retcode = INVALID_POINT;
                     break;
                  case R_RESIN_DENSITY:
                     v->f = cs->resin_table[pd->current_resin_idx].density;
                     break;
                  case R_RECIPE_DENSITY:
                     restore_recipe(cs,pd->rec,&temp_recipe,pd->current_recipe_idx);
                     (void)calc_recipe(&temp_recipe);
                     v->f = (float)temp_recipe.avg_density;
                     break;
                  case R_RECIPE_INVEN:
                     v->f = read_recipe_inven(cs,pd->rec,pd->current_recipe_idx);
                     break;
                  case R_RESIN_INDEX:
                     v->i = pd->current_resin_idx;
                     break;
                  case R_RECIPE_INDEX:
                     v->i = pd->current_recipe_idx;
                     break;
                  case R_NUM_RECIPES:
                     v->i = MAX_RECIPES;
                     break;
                  case R_RECIPE_USED:
                     v->i = (unsigned int)is_recipe_used();
                     break;
                  case R_RESIN_DESC:
                      null_str[0] = '\0';
                     if(cs->enable_resin_codes)
                        v->i = (unsigned int)(&cs->resin_table[pd->current_resin_idx].resin_desc[0]);
                     else
                        v->i = (unsigned int)&null_str[0];
                     *pt_type = STRINGS;
                     break;
                  case R_RECIPE_DESC:
                      null_str[0] = '\0';
                     if(cs->enable_product_code)
                        v->i = (unsigned int)read_recipe_desc(cs,pd->rec,pd->current_recipe_idx);
                     else
                        v->i = (unsigned int)&null_str[0];
                     *pt_type = STRINGS;
                     break;
                  default:
                     retcode = INVALID_POINT;
               }
               break;

            default:
               retcode = INVALID_POINT;
               break;
         }
      }
   }
   else
      retcode = INVALID_POINT;

   if (retcode != OK)
      v->i = 0;

   if (retcode == INVALID_POINT)
   {
	   if( pd->reg < R_INTEGER )
	       *pt_type = FLOAT;
	   else
	       *pt_type = INT;
   }
   return (retcode);
}

/********************************************************
write_remote_var()
*********************************************************/
int write_remote_var(point_data *pd, unsigned int p,fic v)
{
   config_super *cs = &cfg_super;
   int retcode;
   int loop = pd->loop;
   int fn_offset=0;
   recipe_struct temp_recipe;
   char use_fn_tbl = TRUE;

   if(shr_global.disable_remote_writes)
	  return(INVALID_POINT);

   shr_global.rem_diag.last_point_processed = p; /* Copy for service diagnostic displays */

   if (shr_global.remote_lockout)
     return(EWARNING);
    
   retcode = user_block_write_16(pd,&p,v.i);
   
   if(retcode == DEFINED_POINT_REG)
       return (OK);

   pd->recipe_changed = FALSE;

   if (p == NOT_USED)
       return (INVALID_POINT);

   retcode = OK;
   
   pd->dev_type = rem_parse_register_16(pd,p);
   fn_offset = MAX_PCC_REG;
   
   if((pd->reg < R_INTEGER) && (v.f < 0.000001))
           v.f = 0.0;

   if ((loop == ILLEGAL_LOOP))                   /* check if not used    */
     return (INVALID_POINT);

   /* setup offset into function table based on device type*/

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         fn_offset *= SYS_FUNC_OFFSET;
         break;
      case TYPE_GATE:
         fn_offset *= GATE_FUNC_OFFSET;
         break;
      case TYPE_LIW:
         fn_offset *= LIW_FUNC_OFFSET;
         break;
      case TYPE_HO:
         fn_offset *= HO_FUNC_OFFSET;
         break;
      case TYPE_BWH:
         fn_offset *= BWH_FUNC_OFFSET;
         break;
      case TYPE_MIX:
         fn_offset *= MIX_FUNC_OFFSET;
         break;
      case TYPE_WTH:
         fn_offset *= WTH_FUNC_OFFSET;
         break;
      case TYPE_FLF:
         fn_offset *= FLF_FUNC_OFFSET;
         break;
      default:
         use_fn_tbl = FALSE;
         break;
   }

   if(use_fn_tbl)
   {
     if ((pd->reg > 0) && (pd->reg <= MAX_REG))
     {
        (*fp_write_var[fn_offset + pd->reg])(&v, pd);
        Signal.EnteredSetup = TRUE;
        shr_global.in_remote = TRUE;
     }
     else    /* these are leftovers from register.h, above 20000 (system) */
     {
        Signal.EnteredSetup = TRUE;
        shr_global.in_remote = TRUE;
        switch(pd->dev_type)
        {
             case TYPE_SYSTEM:
                 switch(pd->reg)
                 {
                   case R_AUX_OUT_ACT:
                     retcode = INVALID_POINT;
                     break;
                   case R_AUX_OUT_VOLTS:
                     retcode = INVALID_POINT;
                     break;
                   case R_RESIN_DENSITY:
                     cs->resin_table[pd->current_resin_idx].density = v.f;
                     break;
                   case R_RECIPE_DENSITY:
                     retcode = READ_ONLY;
                     break;
                   case R_RECIPE_INVEN:
                     /* should clear the recipe inventory here (into a dummy recipe holder) */
                     break;
                   case R_RESIN_INDEX:
                     pd->current_resin_idx = v.i;
                     break;
                   case R_RECIPE_INDEX:
                     pd->current_recipe_idx = v.i;
                     break;
                   case R_NUM_RECIPES:
                     retcode = READ_ONLY;
                     break;
                   case R_RECIPE_USED:
                     retcode = READ_ONLY;
                     break;
                   case R_RESIN_DESC:
                     brsstrcpy((UDINT)cs->resin_table[pd->current_resin_idx].resin_desc, (UDINT)v.i);
                     break;
                   case R_RECIPE_DESC:
                     if(cs->enable_product_code)
                     {
                        restore_recipe(cs, pd->rec,&temp_recipe, pd->current_recipe_idx);
                        brsstrcpy((UDINT)temp_recipe.product_code, (UDINT)v.i);
                        store_recipe(cs, pd->rec, pd->current_recipe_idx, &temp_recipe);
                     }
                     break;
                   default:
                     retcode = INVALID_POINT;
                 }
                 break;

             default:
                 retcode = INVALID_POINT;
                 break;
         }

     }
   }
   else
     retcode = INVALID_POINT;

   if(pd->recipe_changed)
   {
      shr_global.new_recipe = TRUE;
      rem_copy_recipe(pd, &BATCH_RECIPE_EDIT, BATCH_RECIPE_NEW);
      rem_copy_recipe(pd, &BATCH_RECIPE_EDIT, &RECIPES->entry[HMI.pg453_RecipeNum]);     
      rem_calc_recipe(pd, BATCH_RECIPE_NEW);
     
      shr_global.invalid_remote_recipe = rem_validate_recipe(pd,&BATCH_RECIPE_EDIT,shr_global.recipe_errors);
      if (shr_global.invalid_remote_recipe == 0)
      {
         shr_global.remote_run_recipe_changed = TRUE;        
         Signal.StoreRecipes = TRUE;
      }
   }

   return (retcode);

}
/************************************************************************/
/* int  identify_user_block_16()                                           */
/*      determines if the register is part of a user defined block      */
/*      fills in the block number and base point, returns register type */
/************************************************************************/
int identify_user_block_16(point_data *pd, unsigned int pt,int *block_num,int *base_point)
{
    int retcode = NORMAL_REG;
 
    if ((pt >= USER_BLK_START_REG) && (pt < (USER_BLK_START_REG+1000)))
    {                                 
        *block_num = (pt - USER_BLK_START_REG) / 100;
        *base_point = (pt - USER_BLK_START_REG) % 100;

        if (*base_point >= BLOCK_DEF_START_REG)
            retcode = DEFINED_POINT_REG;
        else
            retcode = DEFINED_DATA_REG;
    }
    return retcode;
}
/****************************************************************************/
/* int translate_remote_register_16()                                                 */
/*      takes block_number and base_point and translates to real register   */
/****************************************************************************/
int translate_remote_register_16(point_data *pd, unsigned int *point)
{
    config_super *cs = &cfg_super;
    int block_num;
    int base_point;
    int retcode = NORMAL_REG;

    switch( (retcode=identify_user_block_16(pd, *point,&block_num,&base_point)) )
    {
        case DEFINED_DATA_REG:
            *point = cs->user_def_blk[block_num].points[base_point];
            break;

        case DEFINED_POINT_REG:
            *point = cs->user_def_blk[block_num].points[base_point-BLOCK_DEF_START_REG];
            break;
    }
    return retcode;
}
/****************************************************************************/
/* int user_block_write()                                                   */
/*      if point is a normal register or part of a user defined data block  */
/*      then returns translated register. If trying to defined a new        */
/*      translation then stores the translation and returns -1              */
/****************************************************************************/
int user_block_write_16(point_data *pd, unsigned int * point,unsigned int val)
{
    config_super *cs = &cfg_super;
    int base_point;
    int block_num;
    int retcode = NORMAL_REG;    

    switch(retcode=identify_user_block_16(pd,*point,&block_num,&base_point))
    {
        case DEFINED_DATA_REG:
            *point = cs->user_def_blk[block_num].points[base_point];
            break;

        case DEFINED_POINT_REG:
            cs->user_def_blk[block_num].points[base_point-BLOCK_DEF_START_REG] = (unsigned int)val;
            break;
    }
    return retcode;
}

/************************************************************************/
/*  Function:  rem_parse_register_16(i,dat,ext,hop,total,loop)                  */
/*                                                                      */
/* Description:  gets a string from the user interface                  */
/************************************************************************/
int rem_parse_register_16(point_data *pd, unsigned int i)
{
    config_super *cs = &cfg_super;
    int dev_num;
           
    if (i < DESCRIP_OFFSET)
       dev_num = i / DEV_OFFSET;
    else       
       dev_num = (i - DESCRIP_OFFSET)/DEV_OFFSET;

    pd->total = FALSE;
    pd->hop = -1;

    pd->reg = i % TYPES_PER;                   /* data element number  */
    
    if (dev_num == 0)
    {
        if(i < DESCRIP_OFFSET)
           pd->hop = ((int)i)/HOPPER_OFFSET;      /* hopper number*/
        else
           pd->hop = ((int)i - DESCRIP_OFFSET)/HOPPER_OFFSET;      /* hopper number*/

        if( pd->hop < REFEED_OFFSET/HOPPER_OFFSET )
        {
            pd->loop = pd->hop;
            pd->dev_type = TYPE_GATE;
        }
        else if (pd->hop == REFEED_OFFSET/HOPPER_OFFSET)
        {
            if (cs->refeed == GRAVIFLUFF)
            {
                pd->loop = (int)(GRAVIFLUFF_FDR_LOOP);          /* get loop # for refeed */
                pd->dev_type = TYPE_LIW;
            }
            else
                pd->loop = ILLEGAL_LOOP;
        }
        else                                        /* extruder */
        {
            pd->loop = (int)(EXT_LOOP);
            pd->dev_type = TYPE_LIW;
        }
    }
    else if(dev_num == (int)HO_OFFSET/DEV_OFFSET)            /* haul off */
    {
        if ((int)i < SHO_OFFSET)
          pd->loop = HO_LOOP;
        else
          pd->loop = SHO_LOOP;
        pd->dev_type = TYPE_HO;
    }
    else if(dev_num == (int)WTH_OFFSET/DEV_OFFSET)           /* width contoller */
    {
        pd->loop = WTH_LOOP;
        pd->dev_type = TYPE_WTH;
    }
    else if(dev_num == (int)MIX_OFFSET/DEV_OFFSET)           /* mixer */
    {
        pd->loop = MIXER_LOOP;
        pd->dev_type = TYPE_MIX;
    }
    else if(dev_num == (int)BATCH_HOP_OFFSET/DEV_OFFSET)     /* batch hoppers */
    {
        pd->loop = WEIGH_HOP_LOOP;
        pd->dev_type = TYPE_BWH;
    }
    else if(dev_num == (int)GRAVIFLUFF_OFFSET/DEV_OFFSET)    /* gravifluff loader */
    {
        pd->loop = GRAVIFLUFF_LOOP;
        pd->dev_type = TYPE_FLF;
    }
    else
    {
        /* this is necessary for point above 20100 */
        pd->reg = (unsigned int)((int)i - SYSTEM_OFFSET);
        pd->ext = 0;
        pd->hop = 0;
        pd->loop = MAX_LOOP;
        pd->dev_type = TYPE_SYSTEM;
    }

    return(pd->dev_type);

}
/****************************************************************************
 *   read_resin_name()                                                      *                 *
 ****************************************************************************/
int read_resin_name(fic * v, point_data * pd, int *type)
{
   int retcode=INVALID_POINT;

   switch(pd->reg)
   {
      case R_RESIN_NAME_1:
         retcode = rv_resin_name_1(v,pd,type);
         break;
      case R_RESIN_NAME_2:
         retcode = rv_resin_name_2(v,pd,type);
         break;
      case R_RESIN_NAME_3:
         retcode = rv_resin_name_3(v,pd,type);
         break;
   }
   return(retcode);

}
/****************************************************************************
 *   read_job_name()                                                      *                 *
 ****************************************************************************/
int read_job_name(fic * v, point_data * pd, int *type)
{
   int retcode=INVALID_POINT;

   switch(pd->reg)
   {
      case R_JOB_NAME_1:
         retcode = rv_job_name_1(v,pd,type);
         break;
      case R_JOB_NAME_2:
         retcode = rv_job_name_2(v,pd,type);
         break;
      case R_JOB_NAME_3:
         retcode = rv_job_name_3(v,pd,type);
         break;
      case R_JOB_NAME_4:
         retcode = rv_job_name_4(v,pd,type);
         break;
   }
   return(retcode);
}
/****************************************************************************
 *   read_recipe_name()                                                      *                 *
 ****************************************************************************/
int read_recipe_name(fic * v, point_data * pd, int *type)
{
   int retcode=INVALID_POINT;

   switch(pd->reg)
   {
      case R_RECIPE_NAME_1:
         retcode = rv_recipe_name_1(v,pd,type);
         break;
      case R_RECIPE_NAME_2:
         retcode = rv_recipe_name_2(v,pd,type);
         break;
      case R_RECIPE_NAME_3:
         retcode = rv_recipe_name_3(v,pd,type);
         break;
      case R_RECIPE_NAME_4:
         retcode = rv_recipe_name_4(v,pd,type);
         break;
   }
   return(retcode);
}
/****************************************************************************
 *   read_device_name()                                                      *                 *
 ****************************************************************************/
int read_device_name(fic * v, point_data * pd, int *type)
{
   int retcode=INVALID_POINT;

   switch(pd->reg)
   {
      case R_DEVICE_NAME_1:
         retcode = rv_device_name_1(v,pd,type);
         break;
      case R_DEVICE_NAME_2:
         retcode = rv_device_name_2(v,pd,type);
         break;
      case R_DEVICE_NAME_3:
         retcode = rv_device_name_3(v,pd,type);
         break;
      case R_DEVICE_NAME_4:
         retcode = rv_device_name_4(v,pd,type);
         break;
   }
   return(retcode);
}


