#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/* //////////////////////////////////////////////////////////////////
// INCLUDES
////////////////////////////////////////////////////////////////// */
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <asusb.h>
#include <fileio.h>
#include <usb.h>
#include <sys.h>
#include <AsBrStr.h>

_LOCAL BOOL  deviceLinked;
_LOCAL UDINT usbAttachDetachCount;
_LOCAL UINT  usbNodeIx;
_LOCAL UDINT usbNodeList[MAXUSBDEVICES];
_LOCAL UDINT usbNodeId;
_LOCAL int   tinttest;
_GLOBAL char szDevParamName[20];

/* //////////////////////////////////////////////////////////////////
// INIT UP
////////////////////////////////////////////////////////////////// */
void usb_init(void)
{
    usbAttachDetachCount = 0;
    usbNodeIx            = 0;
}

/* //////////////////////////////////////////////////////////////////
// CYCLIC TASK
////////////////////////////////////////////////////////////////// */
int usb_check(int *usbAction)
{
   int result;

   result = USBBUSY;
   tinttest = *usbAction;  //debug
   switch (*usbAction)
   {
      case USB_GETNODELIST:
         UsbNodeListGetFub.enable = 1;
         UsbNodeListGetFub.pBuffer = (UDINT)&usbNodeList;
         UsbNodeListGetFub.bufferSize = sizeof(usbNodeList);
         UsbNodeListGetFub.filterInterfaceClass = USB_MASS_STORAGE;
         UsbNodeListGetFub.filterInterfaceSubClass = USB_SUBCLASS_SCSI_COMSET;
         UsbNodeListGet(&UsbNodeListGetFub);
         if (UsbNodeListGetFub.status == OK && UsbNodeListGetFub.listNodes)
         {
             /* USB Device Attach or detach */
             *usbAction = USB_SEARCHDEVICE;
             usbNodeIx = 0;
             usbAttachDetachCount = UsbNodeListGetFub.attachDetachCount;
             result = USBBUSY;

         }
         /* if problem with usb device */
         else if (UsbNodeListGetFub.status != USB_BUSY)
         {
            *usbAction = USB_DONE;
            result = FAIL;
         }
      break;
      case USB_SEARCHDEVICE:
         UsbNodeGetFub.enable = 1;
         UsbNodeGetFub.nodeId = usbNodeList[usbNodeIx];
         UsbNodeGetFub.pBuffer = (UDINT)&usbDevice;
         UsbNodeGetFub.bufferSize = sizeof(usbDevice);
         UsbNodeGet(&UsbNodeGetFub);
         if (UsbNodeGetFub.status == OK)
         {  
            if ((brsstrcmp((UDINT) usbDevice.ifName, (UDINT)"/bd0") == 0) || (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"/bd16") == 0))
				{
               brsstrcpy((UDINT)szDevParamName, (UDINT)"/DEVICE=");
               brsstrcat((UDINT)szDevParamName, (UDINT)usbDevice.ifName);
               usbNodeId = usbNodeList[usbNodeIx];
               *usbAction = USB_DONE;
               result = SUCCESS;
            }
            else
            {
               usbNodeIx++;
               if (usbNodeIx >= UsbNodeListGetFub.allNodes)
               {
                  /* Finished NODE list USB Mass Storage Device not found */
                  *usbAction = USB_DONE;
                  result = FAIL;
               }
            }
         }
         else if (UsbNodeGetFub.status == USB_BUSY)
         {
            /* USB Device not found */
            *usbAction = USB_GETNODELIST;
         }
         else if (UsbNodeGetFub.status != USB_DONE)
         {
             /* Error Handling */
             result = FAIL;
             *usbAction = USB_DONE;
         }
      break;

      case USB_DEVICELINK:
		if (deviceLinked == TRUE)
      {
			*usbAction = USB_DONE;
			deviceLinked = TRUE;
			result = SUCCESS;			
		}
      else
      {	
			DevLinkFub.enable  = 1;
			DevLinkFub.pDevice = (UDINT)MSDEVICE;
			DevLinkFub.pParam  = (UDINT)szDevParamName;
			DevLink(&DevLinkFub);
			if (DevLinkFub.status == OK)
			{
				*usbAction   = USB_DONE;
				deviceLinked = TRUE;
				result       = SUCCESS;
			}
			else if (DevLinkFub.status != USB_BUSY)
			{
				*usbAction = USB_DONE;
				result     = FAIL;
			}
		}
      break;
      case USB_DEVICEUNLINK:
         DevUnlinkFub.enable = 1;
         DevUnlinkFub.handle = DevLinkFub.handle;
         DevUnlink(&DevUnlinkFub);
         if (DevUnlinkFub.status == OK)
         {
            *usbAction = USB_DONE;
				deviceLinked = FALSE;
         }
      break;
      case USB_FILEACCESS:
          /* Check USB Device */
          UsbNodeGetFub.enable = 1;
          UsbNodeGetFub.nodeId = usbNodeId;
          UsbNodeGetFub.pBuffer = (UDINT)&usbDevice;
          UsbNodeGetFub.bufferSize = sizeof(usbDevice);
          UsbNodeGet(&UsbNodeGetFub);
          if (UsbNodeGetFub.status == OK)
          {
              /* File Access */
              DirInfoUsbDev.enable  = 1;
              DirInfoUsbDev.pDevice = (UDINT)MSDEVICE;
              DirInfoUsbDev.pPath   = 0;
              DirInfo(&DirInfoUsbDev);
          }
          else if (UsbNodeGetFub.status == asusbERR_USB_NOTFOUND)
          {
             /* USB Device detached */
             *usbAction = USB_DEVICEUNLINK;
             result = FAIL;
          }
      break;
      default:
         /* *usbAction = USB_DONE; */
      break;
   }
   return(result);
}


