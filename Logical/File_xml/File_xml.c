/********************************************************************
 * COPYRIGHT -- PAI
 ********************************************************************
 * Program: File_xml
 * File: File_xml.c
 * Author: K. Kaczkowski
 * Created: October 07, 2009
 ********************************************************************
 * Implementation of program File_xml
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif

#include "globalinc.h"
#include "mem.h"
#include "hmi.h"
#include "recipe.h"
#include "AsBrStr.h"
#include "signal_pcc.h"

char comp_buf[sizeof(cfg_super) + (sizeof(cfgGATE)+ sizeof(cfgBWH)+ sizeof(cfgMIXER)+ sizeof(cfgHO)+ sizeof(cfgSHO) + 
   sizeof(cfgEXT) + sizeof(cfgGFEEDER) + sizeof(cfgGFLUFF) + sizeof(cfgWTH) +  
   sizeof(smtp_info) + sizeof(recipes) + sizeof(unsigned int))]; 

_LOCAL char backupFilenamePlusExt[64];

/***************************************************/
/***************************************************/

/* Datatypes and datatypes of function blocks */
typedef struct DATA_IF
{	plcbit DataObjReady;
	plcbit WriteData;
	plcbit ReadData;
	plcbit CreateObj;
	plcbit DeleteObj;
	unsigned long DataLen;
	unsigned long Offset;
	unsigned short Status;
	unsigned char* pSource;
	unsigned char* pDest;
} DATA_IF;

typedef struct DataMgr_obj
{	struct DATA_IF* DataIF;
	plcstring ObjName[33];
	unsigned char MemType;
	plcbit _getInfo;
	unsigned long Ident;
	unsigned long ObjLen;
	unsigned long _ObjLen;
	plcbit ObjReady;
	unsigned short Status;
} DataMgr_obj;

extern int Store_Config(char * pFname, char * pData, unsigned long size);
extern int Restore_Config(char * pFname, char * pData, unsigned long size);

/********************/
DatObjCreate_typ    DOCreate;
DatObjWrite_typ 	  DOWrite;
DatObjRead_typ      DORead;
DatObjCopy_typ  	  DOCopy;
DatObjInfo_typ  	  DOInfo;
DatObjDelete_typ    DODelete;

_GLOBAL DataMgr_obj  ConfigData;
_GLOBAL DATA_IF		  ConfigDataIF;
/********************/

_GLOBAL UDINT xml_state_timer;       // when saving to DATAOBJ, failsafe timer for state machine
_GLOBAL UDINT dataobj_save_timer;    // timer for saving configuration

/*** Internal functions prototypes **/
void FromBufToConfig();
void FromConfigToBuf();
signed long DataMgr_Srv(struct DataMgr_obj* mgr);

_GLOBAL  UINT u16DatObj_state;
//_GLOBAL int  Gcfg_super_offset;
_GLOBAL unsigned char xVersionString[20];

#define DAT_OBJ_INIT            0
#define DAT_OBJ_DELETEDATA      1
#define DAT_OBJ_CREATEDATA      2
#define DAT_OBJ_READDATA        3
#define DAT_OBJ_OPENDIR         4
#define DAT_OBJ_OPENINGDIR      5
#define DAT_OBJ_PAUSE           6
#define DAT_OBJ_WAITING         7

#define MAX_WAIT_FOR_CREATE        (360*TICKSPERSEC) // 6 min
#define MAX_WAIT_FOR_SAVE_DATAOBJ  (300*TICKSPERSEC) // 5 min

/************************************************************************/
/*
   Function:  File_INIT()

   Description: This function is the _INIT function for the File_xml task.

   Returns:  VOID
                                                                        */
/************************************************************************/
void _INIT File_INIT( void )
{	
   brsmemset((UDINT)backupFilenamePlusExt, 0, sizeof(backupFilenamePlusExt));

   /** Config Data Object **/
   brsstrcpy((UDINT) ConfigData.ObjName , (UDINT)"ConfigData");
   ConfigData.MemType		= doUSRROM;
   ConfigDataIF.pDest 		= (unsigned char *)comp_buf;
   ConfigDataIF.pSource    = (unsigned char *)comp_buf;
   ConfigDataIF.DataLen 	= sizeof(comp_buf);
   ConfigData.ObjLen	   	= sizeof(comp_buf);
   ConfigData.DataIF	   	= &ConfigDataIF;
   ConfigData._getInfo     = 1;
   /*************************/
	
   Signal.DataObj_read = NOTDONE;
   u16DatObj_state = DAT_OBJ_INIT;
   dataobj_save_timer = start_time();	
}

/************************************************************************/
/*
   Function:  File_xmlCYCLIC()

   Description: This function is the _CYCLIC function for the File_xml task.

   Returns:  VOID
                                                                        */
/************************************************************************/
void _CYCLIC File_CYCLIC( void )
{
   unsigned int i;
   signed long  lStatus;   
   BOOL         versionOK;
   config_super *tptr;
   
   /********/
   if ((Signal.StoreConfig) && (u16DatObj_state == DAT_OBJ_WAITING))
   {
      /** Save immediately from the cfg structures to the dataobj buffer **/
      FromConfigToBuf();

      ConfigDataIF.Offset = 0;
      ConfigDataIF.WriteData = 1;
      Signal.StoreConfig = 0;
   }
   else
   {
      Signal.DataObj_read = NOTDONE;
   }
   
   lStatus = DataMgr_Srv(&ConfigData);

   if ((dataobj_save_timer > 0) && (u16DatObj_state == DAT_OBJ_WAITING))
   {
      if (elap_time(dataobj_save_timer) > (unsigned int)MAX_WAIT_FOR_SAVE_DATAOBJ)
      {
         Signal.StoreConfig = TRUE;
         dataobj_save_timer = start_time();			
      }
   }
 	  
   if (Signal.Binary_Store)
   {
      FromConfigToBuf();
      brsstrcpy((UDINT)backupFilenamePlusExt, (UDINT)cfg_super.u8XmlFilename);
      brsstrcat((UDINT)backupFilenamePlusExt, (UDINT)".pcc");
      Signal.Binary_Store_Status = Store_Config(backupFilenamePlusExt, comp_buf, sizeof(comp_buf));
      if ((Signal.Binary_Store_Status == SUCCESS) || (Signal.Binary_Store_Status == FAIL))
         Signal.Binary_Store = 0;
   }
   
   if (Signal.Binary_Restore)
   {
      brsstrcpy((UDINT)backupFilenamePlusExt, (UDINT)cfg_super.u8XmlFilename);
      brsstrcat((UDINT)backupFilenamePlusExt, (UDINT)".pcc");
      Signal.Binary_Restore_Status = Restore_Config(backupFilenamePlusExt, comp_buf, sizeof(comp_buf));
      if ((Signal.Binary_Restore_Status == SUCCESS) || (Signal.Binary_Restore_Status == FAIL))
         Signal.Binary_Restore = 0;
      if (Signal.Binary_Restore_Status == FAIL)
         FromConfigToBuf();  // copy running data to comp_buf
      else if (Signal.Binary_Restore_Status == SUCCESS)
      {
         tptr = (config_super *)&comp_buf[0]; // 0 is offset for cfg_super;
         brsstrcpy((UDINT)xVersionString, (UDINT)tptr->Info.FirmwareNum);
         if ((brsstrcmp((UDINT)cfg_super.Info.FirmwareNum, (UDINT)xVersionString) == 0) ||
             (brsstrcmp((UDINT)cfg_super.Info.FirmwareNum, (UDINT)cfg_super.u8XmlFilename) == 0))
            versionOK = TRUE;
         else
         {
            Signal.Binary_Restore_Status = FAIL;
            versionOK = FALSE;
         }

         if (versionOK)
         {
            FromBufToConfig();  // copy comp_buf to running data
            Signal.ChangeSubNet   = TRUE;
            Signal.ChangeIPAddr   = TRUE;
            Signal.ChangeGateway  = TRUE;
            Signal.ChangeHostName = TRUE; //G2-620
            HMI.Request_SYSReset  = TRUE;
            HMI.RestoreError      = FALSE;
         }
         else
         {
            HMI.RestoreError = TRUE;
         }
      }      
   }   

   switch (u16DatObj_state)
   {
      case DAT_OBJ_INIT: //
         if (gDataObj_Init == 0) // if we have never run, let the defaults load
         {
            if (Signal.SystemLoaded == TRUE) // after defaults load 
            {
               /** Save immediately from the cfg structures to the dataobj buffer **/
               FromConfigToBuf();
               gDataObj_Init = 1;

               xml_state_timer = start_time();
               u16DatObj_state = DAT_OBJ_CREATEDATA;
            }
         }
         else
         {
            if(!ConfigData._getInfo)
            {
               // according to Piedmont Auto. the size of current has to be < or =, for us to read ok
               if ((ConfigData.ObjLen == ConfigData._ObjLen) ||
                  (ConfigData.ObjLen <= ConfigData._ObjLen))
               {
                  ConfigDataIF.Offset = 0;
                  ConfigDataIF.ReadData = 1;
                  u16DatObj_state = DAT_OBJ_READDATA;
               }
               else
               {
                  ConfigDataIF.DeleteObj = 1;
                  u16DatObj_state = DAT_OBJ_DELETEDATA;
                  Signal.DataObj_read = FAIL;
               }
            }
         }
         break;

      case DAT_OBJ_DELETEDATA:
         if(!ConfigDataIF.DeleteObj)
         {
            ConfigDataIF.CreateObj = 1;
            u16DatObj_state = DAT_OBJ_CREATEDATA;
            xml_state_timer = start_time();
         }
         break;   

      case DAT_OBJ_CREATEDATA:
         if (elap_time(xml_state_timer) > (unsigned int)MAX_WAIT_FOR_CREATE)
         {
            u16DatObj_state = DAT_OBJ_WAITING;
            xml_state_timer = 0;
            Signal.DefaultSystem = TRUE;				
         }

         if(ConfigDataIF.DataObjReady)
         {
            u16DatObj_state = DAT_OBJ_OPENDIR;
         }
         
         break;

      case DAT_OBJ_READDATA:      
         if(!ConfigDataIF.ReadData)
         {
            /** Save immediately from the dataobj buffer to the cfg structures **/
            FromBufToConfig(); //NO, because data ob might be empty if the config was never saved or may be obsolete since the last time config was saved to XML. 
            //config are perm vars anyway so no need to update them from the data ob.
            //if ever config gets too big and does not fit into the perm var mem, then this should be reinstated but only if also a cyclic update
            //of the data ob is implemented to keep the data ob consistent with the "latest" config changes (every 1 min call FromConfigToBuf()) 
            Signal.DataObj_read = SUCCESS;
            for (i=1; i<22; i++)
            {
               HMI.Load_status[i] = STRUCTURE_LOADED;
            }            
            u16DatObj_state = DAT_OBJ_WAITING;
         }
         break;

      case DAT_OBJ_OPENDIR:
         if(!ConfigDataIF.ReadData)
         {
            u16DatObj_state = DAT_OBJ_WAITING;
         }
         break;

      case DAT_OBJ_PAUSE:
         u16DatObj_state = DAT_OBJ_WAITING;      
         break;

      case DAT_OBJ_WAITING:
         break;
   }
}

/******* Internal functions ******************/

void FromBufToConfig()
{
   UDINT off = 0;

   brsmemcpy((UDINT)&cfg_super, (UDINT)&comp_buf[off], sizeof(cfg_super));
   off += sizeof(cfg_super);
   /**/
   brsmemcpy((UDINT)cfgGATE, (UDINT)&comp_buf[off], sizeof(cfgGATE));
   off += sizeof(cfgGATE);
   /**/
   brsmemcpy((UDINT)&cfgBWH, (UDINT)&comp_buf[off], sizeof(cfgBWH));
   off += sizeof(cfgBWH);
   /**/
   brsmemcpy((UDINT)&cfgMIXER, (UDINT)&comp_buf[off], sizeof(cfgMIXER));
   off += sizeof(cfgMIXER);
   /**/
   brsmemcpy((UDINT)&cfgHO, (UDINT)&comp_buf[off], sizeof(cfgHO));
   off += sizeof(cfgHO);
   /**/
   brsmemcpy((UDINT)&cfgSHO, (UDINT)&comp_buf[off], sizeof(cfgSHO));
   off += sizeof(cfgSHO);
   /**/
   brsmemcpy((UDINT)&cfgEXT, (UDINT)&comp_buf[off], sizeof(cfgEXT));
   off += sizeof(cfgEXT);
   /**/
   brsmemcpy((UDINT)&cfgGFEEDER, (UDINT)&comp_buf[off], sizeof(cfgGFEEDER));
   off += sizeof(cfgGFEEDER);	
   /**/
   brsmemcpy((UDINT)&cfgGFLUFF, (UDINT)&comp_buf[off], sizeof(cfgGFLUFF));
   off += sizeof(cfgGFLUFF);	
   /**/
   brsmemcpy((UDINT)&cfgWTH, (UDINT)&comp_buf[off], sizeof(cfgWTH));
   off += sizeof(cfgWTH);
   /**/
   brsmemcpy((UDINT)&smtp_info, (UDINT)&comp_buf[off], sizeof(smtp_info));
   off += sizeof(smtp_info);
   /**/
   brsmemcpy((UDINT)&recipes, (UDINT)&comp_buf[off], sizeof(recipes));
}

void FromConfigToBuf()
{
   UDINT off = 0;
  
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfg_super, sizeof(cfg_super));
   off += sizeof(cfg_super);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)cfgGATE, sizeof(cfgGATE));
   off += sizeof(cfgGATE);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgBWH, sizeof(cfgBWH));
   off += sizeof(cfgBWH);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgMIXER, sizeof(cfgMIXER));
   off += sizeof(cfgMIXER);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgHO, sizeof(cfgHO));
   off += sizeof(cfgHO);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgSHO, sizeof(cfgSHO));
   off += sizeof(cfgSHO);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgEXT, sizeof(cfgEXT));
   off += sizeof(cfgEXT);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgGFEEDER, sizeof(cfgGFEEDER));
   off += sizeof(cfgGFEEDER);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgGFLUFF, sizeof(cfgGFLUFF));
   off += sizeof(cfgGFLUFF);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&cfgWTH, sizeof(cfgWTH));
   off += sizeof(cfgWTH);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&smtp_info, sizeof(smtp_info));
   off += sizeof(smtp_info);
   /**/
   brsmemcpy((UDINT)&comp_buf[off], (UDINT)&recipes, sizeof(recipes));
}

/********************************************/
signed long DataMgr_Srv(struct DataMgr_obj* mgr)
{
	if (!mgr) 
      return -101;
	if (!mgr->DataIF) 			
      return -102;
	if (brsstrlen((UDINT) mgr->ObjName) == 0)	
      return -103;

	if (mgr->_getInfo)
   {
		/* Initialize info structure */
		DOInfo.enable   		= 1;
		DOInfo.pName    		= (UDINT)mgr->ObjName;
		DatObjInfo(&DOInfo);
		mgr->Status 	      = DOInfo.status;

		if (mgr->Status == 65535) /* Function Block Busy - try again */ 
      { 
         return 1;
      }  
		else
      {					
			if (mgr->Status == 20609U) /* #define doERR_MODULNOTFOUND 20609U */	
         {			
				mgr->ObjReady				   = 0;
				mgr->DataIF->DataObjReady	= 0;
				mgr->DataIF->CreateObj		= 1;
				mgr->Ident				   	= 0;
			}			
			else if (mgr->Status == 0)
         {
		    	mgr->ObjReady			   	= 1;
				mgr->DataIF->DataObjReady	= 1;
				mgr->DataIF->CreateObj		= 0;
				
				mgr->Ident					= DOInfo.ident;
				mgr->_ObjLen				= DOInfo.len;
			}			
			mgr->DataIF->DeleteObj		= 0;
			mgr->DataIF->WriteData		= 0;
			mgr->DataIF->ReadData		= 0;
			mgr->_getInfo				   = 0;
		}
	}	
   if (mgr->DataIF->CreateObj)  
   {
		DOCreate.enable		= 1;
		DOCreate.grp       	= 0;
		DOCreate.pName  	   = (UDINT)mgr->ObjName;
		DOCreate.len         = mgr->ObjLen;
		DOCreate.MemType     = mgr->MemType;
		DOCreate.Option      = 0;
		DOCreate.pCpyData    = 0;

		/* Call FUB */
		DatObjCreate(&DOCreate);
		mgr->Status 		= DOCreate.status;
		mgr->Ident			= DOCreate.ident;
  
		/* Verify status */
		if ((mgr->Status == 0) || (mgr->Status == 20601))//ok or already exists
      {
		   mgr->DataIF->CreateObj	= 0;
		   mgr->_getInfo		   	= 1;			
	   }
		else if (mgr->Status == 65535) /* Function Block Busy - try again */ 
      {
         return 1;
      }		
		else if (mgr->Status != 0xFFFF) 
      {
			mgr->DataIF->CreateObj		= 0;
			mgr->ObjReady				   = 0;
			mgr->DataIF->DataObjReady	= 0;               /* Set error level for DatObjCreate */
		}
	}

	if (mgr->ObjReady)
   {
		if (mgr->DataIF->ReadData)  
      {
			if (!mgr->DataIF->pDest)   	
            return -104;
		
			DORead.enable       = 1;
			DORead.ident        = mgr->Ident;;
			DORead.Offset       = mgr->DataIF->Offset;
			DORead.pDestination = (UDINT) mgr->DataIF->pDest;
			DORead.len      	  = mgr->DataIF->DataLen;

			/* Call FUB */
			DatObjRead(&DORead);
			mgr->Status 	= DORead.status;
			if (mgr->Status  == 0) 
         {
            mgr->DataIF->ReadData = 0;
            Signal.DataObj_read   = SUCCESS;
         }
			else 				   
         {
            Signal.DataObj_read = FAIL;
            return 1;
         }
		}
		else if (mgr->DataIF->WriteData)
      {
			if (!mgr->DataIF->pSource) 	
            return -105;
			
			DOWrite.enable  		= 1;
			DOWrite.ident   		= mgr->Ident;
			DOWrite.Offset  		= mgr->DataIF->Offset;
			DOWrite.pSource 		= (UDINT) mgr->DataIF->pSource;
			DOWrite.len     		= mgr->DataIF->DataLen;

			/* Call FUB */
			DatObjWrite(&DOWrite);
			mgr->Status = DOWrite.status;
			if (mgr->Status == 0) 
         {
            mgr->DataIF->WriteData	= 0;
            Signal.DataObj_read = SUCCESS;
         }
			else
         {
            Signal.DataObj_read = FAIL;            
            return 1;
         }
		}	
		else if (mgr->DataIF->DeleteObj)	
      {
		  	/* Initialize delete structure */
		 	DODelete.enable = 1;
			DODelete.ident = mgr->Ident;
		   
		  	/* Call FUB */
		  	DatObjDelete(&DODelete);
	  
			/* Get status */
		  	mgr->Status	= DODelete.status;
	  
		  	/* Verify status */
			if (mgr->Status == 0)	
         {
				mgr->DataIF->DeleteObj 		= 0;	
				mgr->ObjReady				   = 0;
				mgr->DataIF->DataObjReady	= 0;
				mgr->Ident					   = 0;
			}
			else if (mgr->Status == 65535) /* Function Block Busy - try again */ 
         {
            return 1;
         }		
			else if (mgr->Status != 0xFFFF) 
         {
				mgr->DataIF->DeleteObj 		= 0;	
				mgr->ObjReady				   = 0;
				mgr->DataIF->DataObjReady	= 0;
				mgr->Ident					   = 0;
			}
		}
	}
	mgr->DataIF->Status = mgr->Status; 
	return mgr->Status;
}
