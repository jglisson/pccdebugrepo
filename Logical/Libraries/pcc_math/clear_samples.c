#include "pcc_math.h"

signed short clear_samples(samples *s_data)
{
    ordered_pair *data;
    int i;

    s_data->next = 0;

    if(s_data->num_taken > 0)
    {
        data = s_data->pts;
        for(i=0;i<s_data->length;++i)
        {
            data->x = 0.0;
            data->y = 0.0;
            ++data;
        }
    }
    s_data->num_taken = 0;

    return(0);
}


