#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  setup.c

   Description:   This file contains all of the interface between the
      visualization for setting up the system and the configuration
      variables used.


      $Log:   F:\Software\BR_Guardian\setup.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.setup to toggle debug information in this file
 *
 *    Rev 1.24   Sep 15 2008 13:21:38   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.23   Sep 03 2008 10:43:56   gtf
 * added code to set full_address card and relay based on user input channel
 *
 *    Rev 1.22   Aug 25 2008 08:42:34   gtf
 * fixed visiblity issue on language button
 *
 *    Rev 1.21   Aug 07 2008 17:20:26   gtf
 * Various change to screen display to make values appear correctly
 *
 *    Rev 1.20   Jul 25 2008 15:38:14   gtf
 *
 *
 *    Rev 1.19   May 16 2008 09:53:04   FMK
 * Fixed problem with setting up the control_mode
 * variable for extrusion control.
 *
 *    Rev 1.18   May 06 2008 15:07:30   FMK
 * Setup remote port settings in visualization as
 * well as config_super.
 *
 *    Rev 1.17   May 01 2008 13:06:22   FMK
 * If extrusion control mode is changed to unused,
 * the ho and subsequent control modes are also
 * changed to unused.
 *
 *    Rev 1.16   Apr 25 2008 09:16:06   vidya
 * Corrected the hopper type setting when recipe
 * is MOLDING.
 *
 *
 *    Rev 1.15   Apr 14 2008 13:38:48   FMK
 * Changed the way the system responds when
 * and ip address changes.
 *
 *    Rev 1.14   Apr 11 2008 11:51:36   FMK
 * Added configuration for ethernet communications.
 * Setting the IP address, subnet mask and configuring
 * protocols.
 *
 *    Rev 1.13   Apr 09 2008 14:25:12   FMK
 * Fixed issue with selecting 1,2,3 recipe entries.
 * Defines for application setting changed condition
 * for scanning recipe_modes table.
 *
 *    Rev 1.12   Mar 28 2008 10:00:18   FMK
 * Got the simulation screens working on the
 * visualization.
 *
 *    Rev 1.11   Mar 25 2008 13:10:12   vidya
 * Changed recipe_use to hopper_type.
 *
 *    Rev 1.10   Mar 21 2008 10:39:58   FMK
 * Fixed issue with menu choices and application.
 * Reduced defines to zero based for application.
 * Corrected visibility issue with extrusion control
 * used/unused.
 *
 *    Rev 1.9   Mar 19 2008 14:54:54   FMK
 *
 *
 *    Rev 1.8   Mar 07 2008 10:43:42   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.7   Mar 03 2008 15:49:06   FMK
 * Fixed visibility issues with HO ratio'd in manual.
 *
 *    Rev 1.6   Feb 27 2008 14:32:40   FMK
 * Removed printer setup. Now detected automatically.
 *
 *    Rev 1.5   Feb 01 2008 14:01:26   FMK
 * Fixed warning in compiles for implicit function
 * declaration.
 *
 *    Rev 1.4   Jan 25 2008 14:07:28   FMK
 * Made changes to the setup pages to allow the
 * user to store/restore configuration files to a thumb
 * drive installed in the USB ports.
 *
 *    Rev 1.3   Jan 18 2008 09:46:22   FMK
 * Changed global variables to new naming structure.
 *
 *    Rev 1.2   Jan 14 2008 11:02:50   FMK
 * Changed the signal variables to a structure format.
 *
 *    Rev 1.1   Jan 14 2008 10:56:50   FMK
 * Changed the signal variables to a structure format.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include "demo.h"
#include "sys.h"
#include "mem.h"
#include "hmi.h"
#include "alarm.h"
#include "units.h"
#include "relaydef.h"
#include "signal_pcc.h"
#include "file_rw.h"
#include "plc.h"
#include "libbgdbg.h"
#include <math.h>
//#include "security.h"
#include "setup.h"
#include "gate_pcc.h"
#include <string.h>

extern DATE_AND_TIME start_time(void);
extern int default_system();
extern void default_alarms();
extern void default_gate(int);
 
void System_BackupRestore();
void ReInit_Device(int loop);
void Setup_Main();
void Setup_Sys();
void Units_Setup();
void Save_SYS_HMI();
void Recipe_Setup();
void Setup_HO();
void Relays_Setup();
void Setup_Sys_Alarms();

void Setup_GLoad();
void Setup_GFluff();
void Init_Gate_HMI();
void Save_Gate_HMI();
void UpdateGate();
void Setup_Gates();
void Setup_BWH();
void Init_BWH_HMI();
void Save_BWH_HMI();
void Setup_MIX();
void Init_MIX_HMI();
void Save_MIX_HMI();
void Setup_EXT();
void Init_EXT_HMI();
void Save_EXT_HMI();
void Setup_WTH();
void Init_WTH_HMI();
void Save_WTH_HMI();
void Setup_Pump();
void Setup_SHO();
void Init_SHO_HMI();
void Save_SHO_HMI();
void HMIConfigModes();
void pg230_Simulation_Setup();
void pg231_Simulation_Setup();
void pg232_Simulation_Setup();
void pg233_Simulation_Setup();
void dummy_wth_loop(void);
void dummy_ho_loop(int liw);
void dummy_liw_loop(int i, unsigned char DemoMask);
void dummy_bwh_loop(void);
void dummy_gate_loop(int i);
void dummy_mix_loop(void);

extern void diagnostics();
extern void visible(unsigned char *Runtime,unsigned char state);

extern unsigned char gVC_ChangeDisplay;
extern unsigned char gCalib_Init;
extern unsigned char pg175_sys_size_change;

extern int Store_Recipes(void);
extern int Store_TimeWt(void);
extern int Restore_Recipes(void);
extern int Restore_TimeWt(void);
extern void calc_k_factor();
extern void default_units();

int gAlarmDevice;
int gAlarmDeviceLoop;
int gAlarmActionIndex;

int ActionTriggered=0;

_LOCAL int pg013_max_gate;

_LOCAL char stepsize_adjust_visible;
_LOCAL char hopper_allowdump_proxoff;
_LOCAL char hopper_oom_time;
//_LOCAL char prefeed_visible;
//_LOCAL char ho_calibration_changed;
_LOCAL char ext_loading_flag;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/* temporary var to hide recipe entry mode GTF */
_GLOBAL unsigned char recipe_parts;  

_GLOBAL unsigned int uiGraphCounter;       // when saving to DATAOBJ, popup counter


/************************************************************************/
/*
   Function:  Setup_Main()

   Description:  watches the HMI for the setup page and appropriately calls
                 the correct page setup routines.
                                                                        */
/************************************************************************/
void Setup_Main()
{
   int index;

   /* setup the locking of fields based on security and operating modes */
   if (cfg_super.current_security_level < cfg_super.pe_needed[PE_SETUP])
   {
      HMI.SetupLock = 2;   /* all lock */
   }
   else
   {
      if (((shr_global.bln_oper_mode|shr_global.ext_ho_oper_mode) == PAUSE_MODE)
         && (!shr_global.pause_pending))
         HMI.SetupLock = 0;  /* unlock all fields */
      else
         HMI.SetupLock = 1;  /* unlock fields that don't require pause mode */
   }
//   if(shrMIXER.oper_mode == AUTO_MODE)
//      HMI.CalibrateLock=TRUE;     /* locked */
//   else
//      HMI.CalibrateLock=FALSE;    /* unlocked */
	
   if (HMI.CurrentPage == pg002_Setup_Main)
   {
      /* because of definitions of operating modes not conforming to menu
         values needed to configure, the following code had to be added to
         translate the internal mode values to what the visualization needs
         for selection */
      HMIConfigModes();

      /* calibrate device, requires initial setup state parameter */
      if (HMI.Action_Input0 == gcActive)
      {
         HMI.Action_Input0 = gcInActive;
         gCalib_Init = NOTDONE;   /* must run initialization when started */
         HMI.ChangePage = pg350_Calibrate;
      }	  
	  
      /* this was originally done on page pg012 but since I changed page flow need to do it here GTF */
      Init_Gate_HMI();
		
	  if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_SETUP])
	     visible(&HMI.SetupRuntime1,TRUE);    /* shown RESET to factory defaults button */
	  else
		 visible(&HMI.SetupRuntime1,FALSE);    /* hide RESET to factory defaults button */

	  if(cfg_super.current_security_level >= SERVICE_LOCK)
		 visible(&HMI.SetupRuntime2,TRUE);    /* shown RESET to factory defaults button and Diagnostics Button */
	  else
		 visible(&HMI.SetupRuntime2,FALSE);    /* hide RESET to factory defaults button */

	  if (HMI.Action_Input1 == gcActive)   /* Reset to factory defaults pressed */
	  {
	     visible(&HMI.PopupRuntime1,TRUE);      /* show popup */
	     HMI.Action_Input1 = gcInActive; /* Reset keypress */
	  }
	  if (HMI.Action_Input6 == gcActive) /* YES was pressed on the popup */
	  {	
		  Signal.DefaultRecipes = TRUE;
		  Signal.DefaultSystem = TRUE;   /* Send notification to proc_mon to reset */
		  visible(&HMI.PopupRuntime1,FALSE);    /* hidden popup */
		  HMI.Action_Input6 = gcInActive;  /* Reset keypress */
		  for (index = 0; index < NUM_STRUCTURES; index++)
			 HMI.Load_status[index] = STRUCTURE_DEFAULT;
	  }
	  if (HMI.Action_Input7 == gcActive)    /* NO was pressed on the popup */
	  {
		 visible(&HMI.PopupRuntime1,FALSE);    /* hidden popup */
		 HMI.Action_Input7 = gcInActive;  /* Reset keypress */
	  }
   }
   else if (((HMI.CurrentPage >= pg012_Setup_Gates) && (HMI.CurrentPage <=pg019_Setup_Gate_CLoop3)) || (HMI.CurrentPage == pg006_Setup_Gate_CLoop4) ||
		              (HMI.CurrentPage == pg023_Setup_Gate_AlarmB))
   {
      Setup_Gates();
   }
   else if ((HMI.CurrentPage >= pg020_Setup_BWH) && (HMI.CurrentPage <=pg022_Setup_BWH_CLoop2))
   {
      Setup_BWH();
   }
   else if ((HMI.CurrentPage >= pg030_Setup_Mixer) && (HMI.CurrentPage <= pg033_Setup_Mixer_CLoop1))
   {
      Setup_MIX();
   }
   else if ((HMI.CurrentPage >= pg080_Setup_HO) && (HMI.CurrentPage <=pg087_Setup_HO_Drive2))
   {
      Setup_HO();
   }
   else if ((HMI.CurrentPage >= pg040_Setup_Ext) && (HMI.CurrentPage <=pg052_Setup_Ext_Drive1))
   {
      Setup_EXT();
   }
   else if ((HMI.CurrentPage >= pg100_Setup_Pump) && (HMI.CurrentPage <=pg103_Setup_Pump_CLoop1))
   {
      Setup_Pump();
   }
   else if ((HMI.CurrentPage >= pg110_Setup_GLoad) && (HMI.CurrentPage <=pg115_Setup_GLoad_CLoop))
   {
      Setup_GLoad();
   }
   else if ((HMI.CurrentPage >= pg130_Setup_GFluff) && (HMI.CurrentPage <=pg140_Setup_GFluff_Drive))
   {
      Setup_GFluff();
   }
   else if ((HMI.CurrentPage >= pg160_Setup_Winder) && (HMI.CurrentPage <= pg169_Setup_Winder_Drive2))
   {
      Setup_SHO();
   }
   else if ((HMI.CurrentPage >= pg150_Setup_Width) && (HMI.CurrentPage <= pg153_Setup_Width_CLoop))
   {
      Setup_WTH();
   }
    else if (((HMI.CurrentPage >= pg170_Setup_System) && (HMI.CurrentPage <= pg233_Setup_System_Sim4)) || (pg175_sys_size_change == 1) || (HMI.CurrentPage == pg990_SetupWizzard))
   {
      Setup_Sys();
   }
}
/************************************************************************/
/*
   Function:  Setup_GLoad()
 ************************************************************************/
void Setup_GLoad()
{
   switch (HMI.CurrentPage)
   {
      case pg111_Setup_GLoad_Net:
         Signal.EnteredSetup = TRUE;
         break;
      case pg112_Setup_Gload_Net:
         Signal.EnteredSetup = TRUE;
         break;
      case pg113_Setup_GLoad_Alarm:
         Signal.EnteredSetup = TRUE;
         break;
      case pg114_Setup_GLoad_Load:
         Signal.EnteredSetup = TRUE;
         break;
      case pg115_Setup_GLoad_CLoop:
         Signal.EnteredSetup = TRUE;
         break;
      default:
         break;
   }
}
/************************************************************************/
/*
   Function:  Setup_GFluff()
 ************************************************************************/
void Setup_GFluff()
{
   switch (HMI.CurrentPage)
   {
      case pg131_Setup_GFluff_Net1:
         Signal.EnteredSetup = TRUE;
         break;
      case pg132_Setup_GFluff_Net2:
         Signal.EnteredSetup = TRUE;
         break;
      case pg133_Setup_GFluff_Alarm1:
         Signal.EnteredSetup = TRUE;
         break;
      case pg134_Setup_GFluff_Alarm2:
         Signal.EnteredSetup = TRUE;
         break;
      case pg135_Setup_GFluff_Alarm3:
         Signal.EnteredSetup = TRUE;
         break;
      case pg136_Setup_GFluff_Load:
         Signal.EnteredSetup = TRUE;
         break;
      case pg137_Setup_GFluff_CLoop1:
         Signal.EnteredSetup = TRUE;
         break;
      case pg138_Setup_GFluff_CLoop2:
         Signal.EnteredSetup = TRUE;
         break;
      case pg139_Setup_GFluff_CLoop2:
         Signal.EnteredSetup = TRUE;
         break;
      case pg140_Setup_GFluff_Drive:
         Signal.EnteredSetup = TRUE;
         break;
      default:
         Signal.EnteredSetup = TRUE;
         break;
   }
}
/************************************************************************/
/*
   Function:  HMIConfigModes()

   Description:  because of definitinos of operating modes not conforming to menu
      values needed to configure, the following code had to be added to
      translate the internal mode values to what the visualization needs
      for selection
                                                                        */
/************************************************************************/
void HMIConfigModes()
{
   config_super *cs = &cfg_super;

   /* because of definitinos of operating modes not conforming to menu
      values needed to configure, the following code had to be added to
      translate the internal mode values to what the visualization needs
      for selection */
   switch (cfg_super.wtp_mode)
   {
   case MONITOR:
      HMI.pg175_WTPMode = 1;
      break;
   case UNUSED:
      HMI.pg175_WTPMode = 0;
      break;
   case CONTROL:
      HMI.pg175_WTPMode = 2;
      break;
   }
   switch (cfg_super.ltp_mode)
   {
   case MONITOR:
      HMI.pg176_LTPMode = 1;
      break;
   case UNUSED:
      HMI.pg176_LTPMode = 0;
      break;
   case CONTROL:
      HMI.pg176_LTPMode = 2;
      break;
   }
   switch (cs->sltp_mode)
   {
   case MONITOR:
      HMI.pg176_SLTPMode = 1;
      break;
   case UNUSED:
      HMI.pg176_SLTPMode = 0;
      break;
   case CONTROL:
      HMI.pg176_SLTPMode = 2;
      break;
   }

   switch (cs->wth_mode)
   {
   case MONITOR:
      HMI.pg176_WTHMode = 1;
      break;
   case UNUSED:
      HMI.pg176_WTHMode = 0;
      break;
   case CONTROL:
      HMI.pg176_WTHMode = 2;
      break;
   }
}
/******************************************************

*******************************************************/
void Setup_Sys()
{
   config_super *cs = &cfg_super;
   int i, j;
   int index = 0;
   
    if (pg175_sys_size_change || (HMI.CurrentPage == pg990_SetupWizzard))
         {
            switch(cfg_super.pg175_System_Size)
            {
               case 0: /* 1kg */
               cfgBWH.set_batch_size = 2.1; // this will ensure a 2.5lbs max batch size DF
				   cfgMIXER.material_request_source = 0; //drt MATERIAL_REQUEST_EXTERNAL;  // material request for 1Kg batch needs to be on External Input DF
				   //drt cfgMIXER.bypass_mixer_prox_switch = 1; // bypass mixer prox. sensor yes DF
			      cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.005;  //Default "Out of spec %" for 1Kg needs to be 0.5% DF
               // This must be set for G3 Compatibility //G2-541
               HMI.pg990_SetUpSwitchSafetyRuntime = 1;  //G2-541
               cfgBWH.max_batch_size = 2.3; //cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 0.4; //.2*2.2; //cfgBWH.set_batch_size * 0.50;
				  break;
               case 1: /* 2.5kg */
               cfgBWH.set_batch_size = 4.0; 
				   cfgMIXER.material_request_source = 0;  // material request for 2.5Kg batch needs to be on Mixer Prox. DF
				   cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01; //Default  "Out of spec %" for 2.5Kg needs to be 1% DF
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               cfgBWH.max_batch_size = 6.7; //cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 2.0; //cfgBWH.set_batch_size * 0.50;
				break;
               case 9: /* 12kg */
               cfgBWH.set_batch_size = 24.0; 
				   cfgMIXER.material_request_source = 0;  // material request for 12Kg batch needs to be on Mixer Prox. DF
               cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01; //Default "Out of spec %" for 12Kg needs to be 1% DF
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               cfgBWH.max_batch_size = 26.4; //cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 12.0; //cfgBWH.set_batch_size * 0.50;
				break;
			   case 11:  /* 15kg */
			      cfgBWH.set_batch_size = 15.0;
				   cfgMIXER.material_request_source = 0;  // material request for 15Kg batch needs to be on Mixer Prox. DF
				   cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01; //Default "Out of spec %" for 15Kg needs to be 1% DF
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               if (Blender_Type == AUTOBATCH)
            	{
                    cfgMIXER.material_request_source = MATERIAL_REQUEST_EXTERNAL;
            	     cfgBWH.set_batch_size = 36.0;
                    cfgBWH.max_batch_size = cfgBWH.set_batch_size * 1.10;
                    cfgBWH.min_batch_size = 10.0;             	}
            	else 
            	{
                  cfgBWH.set_batch_size = cfgBWH.set_batch_size * 2.2;
                  cfgBWH.max_batch_size = cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = cfgBWH.set_batch_size * 0.50;
               }
				break;
               case 14: /* 18kg */
               cfgBWH.set_batch_size = 38.0; 
				   cfgMIXER.material_request_source = 0;  // material request for 18Kg batch needs to be on Mixer Prox. DF
               cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01;   //Default "Out of spec %" for 18Kg needs to be 1%  DF
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               cfgBWH.max_batch_size = 41.0; // cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 19.0; //cfgBWH.set_batch_size * 0.50;
				break;
               case 19: /* 25kg */
               cfgBWH.set_batch_size = 52.0;
				   cfgMIXER.material_request_source = 0;  // material request for 25Kg batch needs to be on Mixer Prox. DF
				   cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01;  //Default "Out of spec %" for 25Kg needs to be 1%  DF   
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               cfgBWH.max_batch_size = 57.2; //cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 26.0; //cfgBWH.set_batch_size * 0.50;
				break;
               default:  /* 5kg, same as index 3 */
               //cfgBWH.set_batch_size = 1.25 * (cfg_super.pg175_System_Size + 1);
               cfgBWH.set_batch_size = 10.0;
				   cfgMIXER.material_request_source = 0;  // material request for 5Kg batch needs to be on Mixer Prox. DF
               cfgGATE[HMI.CurrentDevice].out_of_spec_limit = 0.01;  //Default "Out of spec %" for 5Kg needs to be 1% DF
				   HMI.pg990_SetUpSwitchSafetyRuntime = 0;
               cfgBWH.max_batch_size = 11.5; //cfgBWH.set_batch_size * 1.10;
               cfgBWH.min_batch_size = 5.0;  //cfgBWH.set_batch_size * 0.50;
				break;
            }
            pg175_sys_size_change = 0;
        }
		 
   switch (HMI.CurrentPage)
   {
      case pg170_Setup_System:
		 visible(&HMI.pg227_LayerRuntime1,FALSE);
         Signal.EnteredSetup = TRUE;
         /* simulation setup */
         if (HMI.Action_Input0 == 9)
         {
            HMI.Action_Input0 = gcInActive;
            HMI.ChangePage = pg230_Setup_System_Sim1;
            /* setting this here so that if units setup is selected we can run through units setup 1 time and the refresh page display GTF*/
            HMI.pg195_units_changed = 1;           }
         break;
      case pg175_Setup_System_Config:
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1,TRUE);
         visible(&HMI.SetupRuntime2,TRUE);
         visible(&HMI.SetupRuntime3,TRUE);
         visible(&HMI.SetupRuntime4,TRUE);
         visible(&HMI.SetupRuntime5,TRUE);
         /* MASS Unit */
         HMI.SetupOptionPoint5[0] = 0;     /* 1.25 */
         HMI.SetupOptionPoint5[1] = 0;     /* 2.50 */
         HMI.SetupOptionPoint5[2] = 0xff;  /* 3.75 */
         HMI.SetupOptionPoint5[3] = 0;     /* 5.00 */
         HMI.SetupOptionPoint5[4] = 0xff;  /* 6.25 */
         HMI.SetupOptionPoint5[5] = 0xff;  /* 7.50 */
         HMI.SetupOptionPoint5[6] = 0xff;  /* 8.75 */
         HMI.SetupOptionPoint5[7] = 0xff;  /* 10.00 */
         HMI.SetupOptionPoint5[8] = 0xff;  /* 11.25 */
         HMI.SetupOptionPoint5[9] = 0;     /* 12.50 */
         HMI.SetupOptionPoint5[10] = 0xff; /* 13.75 */
         HMI.SetupOptionPoint5[11] = 0xff; /* 15.00 */
         HMI.SetupOptionPoint5[12] = 0xff; /* 16.25 */
         HMI.SetupOptionPoint5[13] = 0xff; /* 17.50 */
         HMI.SetupOptionPoint5[14] = 0;    /* 18.00 */
         HMI.SetupOptionPoint5[15] = 0xff; /* 20.00 */
         HMI.SetupOptionPoint5[16] = 0xff; /* 21.25 */
         HMI.SetupOptionPoint5[17] = 0xff; /* 22.50 */
         HMI.SetupOptionPoint5[18] = 0xff; /* 23.75 */
         HMI.SetupOptionPoint5[19] = 0;    /* 25.00 */
	  
         if (HMI.Request_SYSReset) // && (cfg_super.current_blender_type == AUTOBATCH))
         {
	        for(i=0;i<cfg_super.total_gates;++i)	
	        {     
  	           default_gate(i);
	           default_alarms();
	        }
         }

         if(pg175_sys_size_change)
         {
            switch(cfg_super.pg175_System_Size)
            {
               case 0: /* 1.25kg */
                  cfgBWH.set_batch_size = 2.1; /* this will ensure a 2lbs max batch size */
                  cfgBWH.max_batch_size = 2.3; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 0.4; //cfgBWH.set_batch_size * 0.50;
                  break;
               case 1: /* 2.5kg */
                  cfgBWH.set_batch_size = 4.0;
                  cfgBWH.max_batch_size = 6.7; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 2.0; //cfgBWH.set_batch_size * 0.50;
                  break;
               case 9: /* 12kg */
                  cfgBWH.set_batch_size = 24;
                  cfgBWH.max_batch_size = 26.4; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 12.0; //cfgBWH.set_batch_size * 0.50;
                  break;
			      case 11:  /* 15kg */
			         cfgBWH.set_batch_size = 15;
                  cfgBWH.set_batch_size = cfgBWH.set_batch_size * 2.2;
                  cfgBWH.max_batch_size = cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = cfgBWH.set_batch_size * 0.50;
				   break;
               case 14: /* 18kg */
                  cfgBWH.set_batch_size = 38.0;
                  cfgBWH.max_batch_size = 41.0; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 19.0; //cfgBWH.set_batch_size * 0.50;
                  break;
               case 19: /* 25kg */
                  cfgBWH.set_batch_size = 25;
                  cfgBWH.max_batch_size = 57.2; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 26.0; //cfgBWH.set_batch_size * 0.50;
                  break;
               default:  /* 5kg, same as index 3 */
                  cfgBWH.set_batch_size = 10.0;
                  cfgBWH.max_batch_size = 11.5; //cfgBWH.set_batch_size * 1.10;
                  cfgBWH.min_batch_size = 5.0;  //cfgBWH.set_batch_size * 0.50;
                  break;
            }
            //cfgBWH.set_batch_size = cfgBWH.set_batch_size * 2.2;
            //cfgBWH.max_batch_size = cfgBWH.set_batch_size * 1.10;
            //cfgBWH.min_batch_size = cfgBWH.set_batch_size * 0.50;
            pg175_sys_size_change = 0;
            Signal.EnteredSetup = TRUE;
         }
         /* 1.5 chosen by observation               */
         /* 1.25kg will hold a max of 3.76 lbs of pellets backed up to slide gates */
         /* 2.50kg OS9 will hold a max of 7.25 lbs */
         /* 5.00kg will hold 19 lbs */
         if (HMI.Action_Input5 == gcActive)
         {
            Signal.EnteredSetup = TRUE;
            HMI.Action_Input5 = gcInActive;
            HMI.Request_SYSReset = TRUE;

            switch (HMI.pg175_WTPMode)
            {
               case 0:
                  cs->wtp_mode  = UNUSED;
                  cs->ltp_mode  = UNUSED;
                  cs->wth_mode  = UNUSED;
                  cs->sltp_mode = UNUSED;
                  cs->refeed    = NONE;
                  visible(&HMI.SetupRuntime6, FALSE);     /* hidden next/prev page buttons */
	               default_alarms();
                  break;
               case 1:
                  cs->wtp_mode = MONITOR;
                  visible(&HMI.SetupRuntime6, TRUE);    /* shown next/prev page buttons */
	               default_alarms();
                  break;
               case 2:
                  cs->wtp_mode = CONTROL;
                  visible(&HMI.SetupRuntime6, TRUE);    /* shown next/prev page buttons */
	               default_alarms();
                  break;
            }
         }
         /* there is no next page unless extrusion control is enabled */
         if (cs->wtp_mode == UNUSED)
            visible(&HMI.SetupRuntime6,FALSE);
         else
            visible(&HMI.SetupRuntime6,TRUE);

         cs->control_mode = (char)( ((cs->wtp_mode+1)*3) + (cs->ltp_mode+1) );
         for (i=0; i<MAX_GATES; i++)
		   {
		 	   if (cfgGATE[i].hopper_type == MEDIUM)
	            cfgGATE[i].minimum_gate_time = MIN_GATE_TIME_MEDIUM;
	         else if (cfgGATE[i].hopper_type == SMALL)  
		         cfgGATE[i].minimum_gate_time = MIN_GATE_TIME_SMALL;
            else if (cfgGATE[i].hopper_type == REGRIND)
	            cfgGATE[i].minimum_gate_time = MIN_GATE_TIME_REGRIND;
	         else if (cfgGATE[i].hopper_type == STANDARD_MAIN)
		         cfgGATE[i].minimum_gate_time = MIN_GATE_TIME_STANDARD;
			   else if (cfgGATE[i].hopper_type == NORMAL)
		         cfgGATE[i].minimum_gate_time = MIN_GATE_TIME;
			   else if (cfgGATE[i].hopper_type == SCR_MEDIUM)
	            cfgGATE[i].minimum_gate_time = MIN_GATE_TIME;
	         else if (cfgGATE[i].hopper_type == SCR_SMALL)
	            cfgGATE[i].minimum_gate_time = MIN_GATE_TIME; 
		   }           
         break;
      case pg176_Setup_System_Config1:
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         Save_SYS_HMI();  /* Always save the current settings */
         if ((cs->wtp_mode == FALSE)||(HMI.SetupLock > 0))
            HMI.SetupRuntime2 = 1; /* locked */
         else
            HMI.SetupRuntime2 = 0;
         /* secondary haul off mode */
         if ((cs->ltp_mode == FALSE)||(HMI.SetupLock > 0))
            HMI.SetupRuntime3 = 1; /* locked */
         else
            HMI.SetupRuntime3 = 0;
         if((cs->wtp_mode != UNUSED) && (cs->ltp_mode != UNUSED))
         {
            if(HMI.SetupLock == 0)
               HMI.SetupRuntime4 = FALSE;    /* shown width control mode */
            else
               HMI.SetupRuntime4 = TRUE;     /* locked width control mode */
            if (HMI.SetupNumChanged4 == TRUE)
            {
               HMI.SetupNumChanged4 = FALSE;

               cs->wth_type = KUNDIG;
               cs->width_port.number = 3;  /* default to port 3 */
               if (cs->remote_port.number == 3)
                 cs->remote_port.number = 0;
               if (cs->remote_int_port.number == 3)
                 cs->remote_int_port.number = 0;
               if (cs->remote_gravitrol_link.number == 3)
                 cs->remote_gravitrol_link.number = 0;
            }
            else
               cs->width_port.number = 0;
         }
         else
         {
            HMI.SetupRuntime4 = TRUE;     /* Locked width control mode */
            cs->wth_mode = UNUSED;
         }

         if ((cs->wtp_mode != UNUSED) && ((cs->application == BLOWN_FILM) || (cs->application == SHEET)))
            if (HMI.SetupLock == 0)
               HMI.SetupRuntime5 = FALSE;    /* shown refeed control mode */
            else
               HMI.SetupRuntime5 = TRUE;     /* locked refeed control mode */
         else
         {
            HMI.SetupRuntime5 = TRUE;     /* locked refeed control mode */
            cs->refeed = NONE;
         }
         /* if turning off extrusion control, must reset material request */
         if ((cs->wtp_mode == UNUSED) && (cfgMIXER.material_request_source == MATERIAL_REQUEST_GRAVITROL))
            cfgMIXER.material_request_source = MATERIAL_REQUEST_MIXER_PROX;

         if (HMI.Action_Input2 == gcActive)
         {
            Signal.EnteredSetup = TRUE;
            HMI.Action_Input2 = gcInActive;
            HMI.Request_SYSReset = TRUE;
            switch (HMI.pg176_LTPMode)
            {
               case 0:
                  cs->ltp_mode = UNUSED;
	               default_alarms();
                  break;
               case 1:
                  cs->ltp_mode = MONITOR;
	               default_alarms();
                  break;
               case 2:
                  cs->ltp_mode = CONTROL;
	               default_alarms();
                  break;
            }
            switch (HMI.pg176_SLTPMode)
            {
               case 0:
                  cs->sltp_mode = UNUSED;
                  break;
               case 1:
                  cs->sltp_mode = MONITOR;
                  break;
               case 2:
                  cs->sltp_mode = CONTROL;
                  break;
            }
            switch (HMI.pg176_WTHMode)
            {
               case 0:
                  cs->wth_mode = UNUSED;
                  break;
               case 1:
                  cs->wth_mode = MONITOR;
                  break;
               case 2:
                  cs->wth_mode = CONTROL;
                  break;
            }
         }

         cs->control_mode = (char)( ((cs->wtp_mode+1)*3) + (cs->ltp_mode+1) );

      break;
      case pg179_Setup_System_Misc:
         if(HMI.SetupLock > 0)
            HMI.SetupRuntime1 = TRUE;   /* Locked width control mode */
         else
            HMI.SetupRuntime1 = FALSE;     /* Unlocked width control mode */
         visible(&HMI.SetupRuntime2, FALSE);
         if ((cs->blend_control <= 2) && (HMI.SetupLock == 0))
         {
            HMI.SetupRuntime2 = FALSE;
            HMI.SetupRuntime3 = FALSE;
         }
         else
         {
            HMI.SetupRuntime2 = TRUE;   /* Locked */
            HMI.SetupRuntime3 = TRUE;   /* Locked */
         }
      break;
      case pg180_Setup_System_Alarms:
         Signal.EnteredSetup = TRUE;
         if (HMI.Action_Input0 == 1)  /* go to setup alarms page */
         {
            HMI.pg181_AlarmDevice = 0;
            HMI.pg181_AlarmType = 0;
            HMI.Action_Input0 = 0;
            visible(&HMI.SetupRuntime1,FALSE);
            visible(&HMI.SetupRuntime2,FALSE);
            HMI.ChangePage = pg181_Setup_System_Alarms1;
         }
      break;
      case pg181_Setup_System_Alarms1:
         Signal.EnteredSetup = TRUE;
         HMI.pg181_Max_Index = 99;
         Setup_Sys_Alarms();
         break;
      case pg182_Setup_System_Accel:
         Signal.EnteredSetup = TRUE;
         break;
      case pg183_Setup_System_UI:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         if (cs->ltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime3, TRUE);     /* hidden HO ratio'd in manual */
         }
         else
         {
            visible(&HMI.SetupRuntime3, FALSE);    /* shown HO ratio'd in manual */
         }

         if ((cs->wtp_mode != CONTROL) && (cs->ltp_mode != CONTROL))
         {
            visible(&HMI.SetupRuntime4, FALSE);     /* hidden auto switch to automatic */
         }
         else
         {
            visible(&HMI.SetupRuntime4, TRUE);    /* shown auto switch to automatic */
         }

         if (HMI.SetupNumChanged2 == TRUE)
         {
            cfgEXT.rs_num = 0;
            cfgGFEEDER.rs_num = 0;
            cfgGFLUFF.rs_num = 0;
            cfgHO.rs_num = 0;
            cfgSHO.rs_num = 0;
            HMI.SetupNumChanged2 = FALSE;
         }
         else
         {
         }

         if (cs->spd_in_percent)    /* clean up all factors if changed  */
         {
            cfgEXT.spd_factor = 100.0;
            cfgGFEEDER.spd_factor = 1.0;
            cfgHO.spd_factor[0] = 100.0;
            cfgSHO.spd_factor[0] = 1.0;
         }
         Save_SYS_HMI();  /* Always save the current settings */
         /* GTF Previously it was possible to go into this screen, changee multilingual and not have language runtime changed */
         if (cs->MultiLingual == TRUE)
            visible(&HMI.pg000_LanguageRuntime,TRUE);
         else
            visible(&HMI.pg000_LanguageRuntime,FALSE);
         /* GTF END */
         break;
      case pg184_Setup_System_UI1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if (HMI.Action_Input0 == gcActive)
         {
            gVC_ChangeDisplay |= gChangeContrast;
            HMI.Action_Input0 = gcInActive;
         }

         if (HMI.Action_Input1 == gcActive)
         {
            gVC_ChangeDisplay |= gChangeBrightness;
            HMI.Action_Input1 = gcInActive;
         }
         break;
      case pg185_Setup_System_Sec:
         Signal.EnteredSetup = TRUE;
         break;
      case pg186_Setup_System_Sec1:
         Signal.EnteredSetup = TRUE;
         break;
      case pg187_Setup_System_Sec2:
         Signal.EnteredSetup = TRUE;
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0 = gcInActive;
            if (cs->total_gates > 2 )
               HMI.ChangePage = pg188_Setup_System_Sec3;
            else
               HMI.ChangePage = pg190_Setup_System_Sec5;
         }
         break;
      case pg188_Setup_System_Sec3:
         Signal.EnteredSetup = TRUE;
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0 = gcInActive;
            if (cs->total_gates > 7 )
               HMI.ChangePage = pg189_Setup_System_Sec4;
            else
               HMI.ChangePage = pg190_Setup_System_Sec5;
         }
         break;
      case pg189_Setup_System_Sec4:
         Signal.EnteredSetup = TRUE;
         break;
      case pg190_Setup_System_Sec5:
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0 = gcInActive;
            if (cs->total_gates > 7)
               HMI.ChangePage = pg189_Setup_System_Sec4;
            else if (cs->total_gates > 2)
               HMI.ChangePage = pg188_Setup_System_Sec3;
            else
               HMI.ChangePage = pg187_Setup_System_Sec2;
         }

			if (HMI.Action_Input1 == gcActive)
			{
			   HMI.Action_Input1 = gcInActive;
			   if ((HMI.pg190_TimeOut > 0) && (HMI.pg190_TimeOut < 60000))
				   cfg_super.TimeOut = 60000;
			   else
				   cfg_super.TimeOut = HMI.pg190_TimeOut;
				cfg_super.EnteredPassword = start_time();
			}
			HMI.pg190_TimeOut = cfg_super.TimeOut;
			
			if ((cfg_super.TimeOut > 0) && (cfg_super.TimeOut < 60000))
				cfg_super.TimeOut = 60000;
			
        if (HMI.Action_Input2 == gcActive)
         {
            HMI.Action_Input2 = gcInActive;
            brsstrcpy((UDINT)cs->supervisor_code,  (UDINT)HMI.SetupOptionfirstrec);
            brsstrcpy((UDINT)cs->maintenance_code, (UDINT)HMI.SetupOptionsecondrec);
            brsstrcpy((UDINT)cs->operator_code,    (UDINT)HMI.SetupOptionthirdrec);
         }

         if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_SETUP])
         {
            brsstrcpy((UDINT)HMI.SetupOptionfirstrec,  (UDINT)cs->supervisor_code);
            brsstrcpy((UDINT)HMI.SetupOptionsecondrec, (UDINT)cs->maintenance_code);
            brsstrcpy((UDINT)HMI.SetupOptionthirdrec,  (UDINT)cs->operator_code);
         }
         else
         {
            brsstrcpy((UDINT)HMI.SetupOptionfirstrec,  (UDINT)"*********");
            brsstrcpy((UDINT)HMI.SetupOptionsecondrec, (UDINT)"*********");
            brsstrcpy((UDINT)HMI.SetupOptionthirdrec,  (UDINT)"*********");
         }
      break;
      case pg191_Setup_System_Sec6:
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0 = gcInActive;
            if (cs->total_gates > 7)
               HMI.ChangePage = pg189_Setup_System_Sec4;
            else if (cs->total_gates > 2)
               HMI.ChangePage = pg188_Setup_System_Sec3;
            else
               HMI.ChangePage = pg187_Setup_System_Sec2;
         }
      break;
      case pg195_Setup_System_Units:
         Units_Setup();
      break;
      case pg196_Setup_System_Units1:
         Units_Setup();
      break;
      case pg200_Setup_System_Time:
         Signal.EnteredSetup = TRUE;
         break;
      case pg201_Setup_System_Relays:
         Signal.EnteredSetup = TRUE;
         if (cfg_super.self_cleaning)
            visible(&HMI.PopupRuntime2, TRUE);
         //G2-225 1/31/17 drt
         Relays_Setup();
         break;
      case pg205_Setup_System_Recipe1:
      case pg206_Setup_System_Recipe2:
      case pg207_Setup_System_Recipe3:
         Signal.EnteredSetup = TRUE;
         Recipe_Setup();
         break;
      case pg217_Setup_System_OPC:
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0     = gcInActive;
            Signal.ChangeHostName = TRUE; //G2-620
         }
         break;
      case pg210_Setup_System_Ethernet:
         if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0     = gcInActive;
            Signal.ChangeSubNet   = TRUE;
            Signal.ChangeGateway  = TRUE;
            Signal.ChangeIPAddr   = TRUE;
            HMI.Request_SYSReset  = TRUE;
         }

         if (cs->abtcp_enabled)
            visible(&HMI.SetupRuntime3, TRUE);
         else
            visible(&HMI.SetupRuntime3, FALSE);

         if (cs->mbenet_enabled)
            visible(&HMI.SetupRuntime4, TRUE);
         else
            visible(&HMI.SetupRuntime4, FALSE);
         break;
      case pg211_Setup_System_Serial:
          if (HMI.Action_Input0 == gcActive)
          {
            Signal.EnteredSetup = TRUE;
            HMI.Action_Input0 = gcInActive;
            Signal.ChangeSubNet = TRUE;
            Signal.ChangeIPAddr = TRUE;
            HMI.Request_SYSReset = TRUE;
         }
         break;
      case pg212_Setup_System_Comm:
         if (HMI.Action_Input1 == 1)
         {
            HMI.Action_Input1 = gcInActive;
            Signal.EnteredSetup = TRUE;
            HMI.Request_SYSReset = TRUE;
         }
         break;
      case pg213_Setup_System_Smtp:
          if (HMI.Action_Input0 == gcActive) // this controls the cyclic
          {
             Signal.EnteredSetup = TRUE;
             smtp_info.active = HMI.pg213_AlarmAction;
             HMI.Request_SYSReset = TRUE;
             HMI.Action_Input0 = gcInActive;
          }          
          if (HMI.Action_Input1 == gcActive) // these are all others
          {
             Signal.EnteredSetup = TRUE;
             HMI.Action_Input1 = gcInActive;
          }          
         break;
      case pg214_Setup_System_UCBs:
			if (HMI.pg214_ClearDefaultUCBs == 1)
			{
				for (i = 0; i < 10; i++)
					for (j = 0; j < 50; j++)
						cfg_super.user_def_blk[i].points[j] = 0;		
		
				HMI.pg214_ClearDefaultUCBs = 0;
			}
			
		if (HMI.pg214_LoadDefaultUCBs == 1)
		{
			// Set default UCBs
			cfg_super.user_def_blk[0].points[0] = 1011; // Set Parts
			cfg_super.user_def_blk[0].points[1] = 2011;
			cfg_super.user_def_blk[0].points[2] = 3011;
			cfg_super.user_def_blk[0].points[3] = 4011;
			cfg_super.user_def_blk[0].points[4] = 5011;
			cfg_super.user_def_blk[0].points[5] = 6011;
			cfg_super.user_def_blk[0].points[6] = 1012; // Actual Parts
			cfg_super.user_def_blk[0].points[7] = 2012;
			cfg_super.user_def_blk[0].points[8] = 3012;
			cfg_super.user_def_blk[0].points[9] = 4012;
			cfg_super.user_def_blk[0].points[10] = 5012;
			cfg_super.user_def_blk[0].points[11] = 6012;
			cfg_super.user_def_blk[0].points[12] = 1302; // Hopper Status
			cfg_super.user_def_blk[0].points[13] = 2302; //drt
			cfg_super.user_def_blk[0].points[14] = 3302; //
			cfg_super.user_def_blk[0].points[15] = 4302; //
			cfg_super.user_def_blk[0].points[16] = 5302; //
			cfg_super.user_def_blk[0].points[17] = 6302;	//drt corrected 3/24/16	
			cfg_super.user_def_blk[0].points[18] = 1025;	// Hopper Shift Weight
			cfg_super.user_def_blk[0].points[19] = 2025;
			cfg_super.user_def_blk[0].points[20] = 3025;
			cfg_super.user_def_blk[0].points[21] = 4025;
			cfg_super.user_def_blk[0].points[22] = 5025;
			cfg_super.user_def_blk[0].points[23] = 6025;
			cfg_super.user_def_blk[0].points[24] = 6400025;	// Total Shift Weight
			cfg_super.user_def_blk[0].points[25] = 1026;	// Inventory Weight
			cfg_super.user_def_blk[0].points[26] = 2026;
			cfg_super.user_def_blk[0].points[27] = 3026;
			cfg_super.user_def_blk[0].points[28] = 4026;
			cfg_super.user_def_blk[0].points[29] = 5026;
			cfg_super.user_def_blk[0].points[30] = 6026;
			cfg_super.user_def_blk[0].points[31] = 6400026;	// Total Inventory Weight		
			cfg_super.user_def_blk[0].points[32] = 96024;	// Weigh Hopper Weight
			cfg_super.user_def_blk[0].points[33] = 1024;	// Hopper Weight
			cfg_super.user_def_blk[0].points[34] = 2024;
			cfg_super.user_def_blk[0].points[35] = 3024;
			cfg_super.user_def_blk[0].points[36] = 4024;
			cfg_super.user_def_blk[0].points[37] = 5024;
			cfg_super.user_def_blk[0].points[38] = 6024;
			cfg_super.user_def_blk[0].points[39] = 6400303;	// System Alarm	
			cfg_super.user_def_blk[0].points[40] = 1303; // Hopper Alarms
			cfg_super.user_def_blk[0].points[41] = 2303;
			cfg_super.user_def_blk[0].points[42] = 3303;
			cfg_super.user_def_blk[0].points[43] = 4303;
			cfg_super.user_def_blk[0].points[44] = 5303;
			cfg_super.user_def_blk[0].points[45] = 6303;
			cfg_super.user_def_blk[0].points[46] = 96303; // Weigh Hopper Alarm
			cfg_super.user_def_blk[0].points[47] = 97303;
			cfg_super.user_def_blk[0].points[48] = 6400302;	// System Status 
			cfg_super.user_def_blk[0].points[49] = 6400301;
	
			cfg_super.user_def_blk[1].points[0] = 1030; // New Parts
			cfg_super.user_def_blk[1].points[1] = 2030;
			cfg_super.user_def_blk[1].points[2] = 3030;
			cfg_super.user_def_blk[1].points[3] = 4030;
			cfg_super.user_def_blk[1].points[4] = 5030;
			cfg_super.user_def_blk[1].points[5] = 6030;
			cfg_super.user_def_blk[1].points[6] = 6400300;	// Set Mode
			cfg_super.user_def_blk[1].points[7] = 6400303;	// Suytem Alarms
			cfg_super.user_def_blk[1].points[8] = 1303; // Hopper Alarms
			cfg_super.user_def_blk[1].points[9] = 2303;
			cfg_super.user_def_blk[1].points[10] = 3303;
			cfg_super.user_def_blk[1].points[11] = 4303;
			cfg_super.user_def_blk[1].points[12] = 5303;
			cfg_super.user_def_blk[1].points[13] = 6303;
			cfg_super.user_def_blk[1].points[14] = 96303; // WM Alarms
			cfg_super.user_def_blk[1].points[15] = 1025; // Shift Weight Hoppers
			cfg_super.user_def_blk[1].points[16] = 2025;
			cfg_super.user_def_blk[1].points[17] = 3025;
			cfg_super.user_def_blk[1].points[18] = 4025;
			cfg_super.user_def_blk[1].points[19] = 5025;
			cfg_super.user_def_blk[1].points[20] = 6025;
			cfg_super.user_def_blk[1].points[21] = 6400025;	// Total Shift Weight
			cfg_super.user_def_blk[1].points[22] = 1026; // Hopper Inventory Weight
			cfg_super.user_def_blk[1].points[23] = 2026;
			cfg_super.user_def_blk[1].points[24] = 3026;
			cfg_super.user_def_blk[1].points[25] = 4026;
			cfg_super.user_def_blk[1].points[26] = 5026;
			cfg_super.user_def_blk[1].points[27] = 6026;
			cfg_super.user_def_blk[1].points[28] = 6400026;  // Total Inventory Weight			
			cfg_super.user_def_blk[1].points[29] = 97303;		
		
			HMI.pg214_LoadDefaultUCBs = 0;
		}
         if (HMI.pg212_UcbNumber == 0)
            HMI.pg212_UcbNumber = 1;            
         if (HMI.pg212_UcbRegister == 0)
            HMI.pg212_UcbRegister = 1;
         
         if (HMI.Action_Input0 == 1)
         {
           HMI.pg212_UcbValue = cs->user_def_blk[HMI.pg212_UcbNumber-1].points[HMI.pg212_UcbRegister-1];
           HMI.Action_Input0 = gcInActive;
         }
         if (HMI.Action_Input0 == 2)
         {
            // since this is zero based, subtract one from visual
            cs->user_def_blk[HMI.pg212_UcbNumber-1].points[HMI.pg212_UcbRegister-1] = HMI.pg212_UcbValue;
            Signal.EnteredSetup = TRUE;
            HMI.Action_Input0 = gcInActive;
         }
		   HMI.pg212_UcbValue = cs->user_def_blk[HMI.pg212_UcbNumber-1].points[HMI.pg212_UcbRegister-1];
         break;
      case pg226_Setup_System_Restore:
      case pg225_Setup_System_Backup:
         System_BackupRestore();
         break;
      case pg227_System_Load_Status:
         for (index = 0; index < NUM_STRUCTURES; index++)
         {
            if (!HMI.Load_status[index])
               HMI.Load_status_color[index] = FALSE; /* black */
            else
               HMI.Load_status_color[index] = HMI.Load_status[index]+1; /* color */
         }
         for (index = 0; index < MAX_GATES; index++)
         {
            /* if gate not enabled make it black */
            if (index >= cfg_super.total_gates)
            {
               HMI.Load_status_color[GATE_A_STATUS+index] = FALSE; /* black */
               HMI.Load_status_color[GATE_A_tw_STATUS+index] = FALSE; /* black */
            }
         }
      break;
      case pg230_Setup_System_Sim1:
         Signal.EnteredSetup = TRUE;
         pg230_Simulation_Setup();
         break;
      case pg231_Setup_System_Sim2:
         Signal.EnteredSetup = TRUE;
         pg231_Simulation_Setup();
         break;
      case pg232_Setup_System_Sim3:
         Signal.EnteredSetup = TRUE;
         pg232_Simulation_Setup();
         break;
      case pg233_Setup_System_Sim4:
         Signal.EnteredSetup = TRUE;
         pg233_Simulation_Setup();
         break;
      default:
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         visible(&HMI.SetupRuntime6, TRUE);
         visible(&HMI.SetupRuntime7, TRUE);
         Save_SYS_HMI();  /* Always save the current settings */
         break;
   }
}
void pg230_Simulation_Setup()
{
   int i;

   if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.total_gates > 2))
         visible(&HMI.SetupRuntime6, TRUE);
   else
         visible(&HMI.SetupRuntime6, FALSE);

   if (HMI.Action_Input0 == gcActive)
   {
      HMI.Action_Input0 = gcInActive;
      if (cfg_super.demo)
      {
         /* set all liw loops into demo mode */
         dummy_bwh_loop();

         for (i=0; i<cfg_super.total_gates; ++i)
            dummy_gate_loop(i);
         cfgMIXER.demo_max_wtp = 2000.0;
         cfgMIXER.demo = TRUE;
         if (cfg_super.wtp_mode != UNUSED)
            dummy_liw_loop(EXT_LOOP, DEMO_BOTH_MODS);
         if (cfg_super.refeed == GRAVIFLUFF)
         {
             dummy_liw_loop(GRAVIFLUFF_LOOP, DEMO_BOTH_MODS);
             dummy_liw_loop(GRAVIFLUFF_FDR_LOOP, DEMO_BOTH_MODS);
         }

         /* dummy up the haul off */
         if (cfg_super.ltp_mode != UNUSED)
            dummy_ho_loop(HO_LOOP);

         if (cfg_super.sltp_mode != UNUSED)
            dummy_ho_loop(SHO_LOOP);
         dummy_wth_loop();
      }
      else
      {
         cfgBWH.demo = FALSE;
         for (i=0; i<cfg_super.total_gates; ++i)
         {
            cfgGATE[i].demo = FALSE;
         }
//         cfgEXT.demo = FALSE;
//         cfgGFEEDER.demo = FALSE;
//         cfgGFLUFF.demo = FALSE;
//         cfgHO.demo = FALSE;
//         cfgSHO.demo = FALSE;
//         cfgWTH.demo = FALSE;
         cfgMIXER.demo = FALSE;
      }
   }

   if (HMI.Action_Input1 == gcActive)
   {
      HMI.Action_Input1 = gcInActive;
      if (cfgBWH.demo == TRUE)
         dummy_bwh_loop();
   }
   if (HMI.Action_Input2 == gcActive)
   {
      if (cfgMIXER.demo == TRUE)
         cfgMIXER.demo_max_wtp = 2000.0;
      HMI.Action_Input2 = gcInActive;
   }
   if (HMI.Action_Input5 == gcActive)
   {
      if ((cfg_super.wtp_mode == MONITOR) || (cfg_super.wtp_mode == CONTROL))
      {
         if (cfgEXT.demo & DEMO_DRIVE_MOD)
            HMI.pg232_SimExtDrive = TRUE;
         else
            HMI.pg232_SimExtDrive = FALSE;
         if (cfgEXT.demo & DEMO_WEIGH_MOD)
            HMI.pg232_SimExtWeigh = TRUE;
         else
            HMI.pg232_SimExtWeigh = FALSE;

         if (cfgHO.demo & DEMO_DRIVE_MOD)
            HMI.pg232_SimHODrive = TRUE;
         else
            HMI.pg232_SimHODrive = FALSE;
         if (cfgHO.demo & DEMO_PULSES)
            HMI.pg232_SimHOPulses = TRUE;
         else
            HMI.pg232_SimHOPulses = FALSE;
			
         /* setup up fields before changing pages */
         HMI.ChangePage = pg232_Setup_System_Sim3;
      }
      else
      {
         HMI.ChangePage = pg231_Setup_System_Sim2;
      }
   }
   HMI.Action_Input5 = gcInActive;
}
void pg231_Simulation_Setup()
{
   config_super *cs = &cfg_super;

   if (HMI.Action_Input5 == gcActive)
   {
      if ((cs->wtp_mode == MONITOR) || (cs->wtp_mode == CONTROL))
      {
         if (cfgEXT.demo & DEMO_DRIVE_MOD)
            HMI.pg232_SimExtDrive = TRUE;
         else
            HMI.pg232_SimExtDrive = FALSE;

         if (cfgEXT.demo & DEMO_WEIGH_MOD)
            HMI.pg232_SimExtWeigh = TRUE;
         else
            HMI.pg232_SimExtWeigh = FALSE;

         if (cfgHO.demo & DEMO_DRIVE_MOD)
            HMI.pg232_SimHODrive = TRUE;
         else
            HMI.pg232_SimHODrive = FALSE;

         if (cfgHO.demo & DEMO_PULSES)
            HMI.pg232_SimHOPulses = TRUE;
         else
            HMI.pg232_SimHOPulses = FALSE;
            
         if (cfgSHO.demo & DEMO_DRIVE_MOD)
            HMI.pg233_SimSHODrive = TRUE;
         else
            HMI.pg233_SimSHODrive = FALSE;

         if (cfgSHO.demo & DEMO_PULSES)
            HMI.pg233_SimSHOPulses = TRUE;
         else
            HMI.pg233_SimSHOPulses = FALSE;
            
         /* setup up fields before changing pages */
         HMI.ChangePage = pg232_Setup_System_Sim3;
      }
      else
      {
         HMI.ChangePage = pg230_Setup_System_Sim1;
      }
   }
   HMI.Action_Input5 = gcInActive;
}
void pg232_Simulation_Setup()
{
   config_super *cs = &cfg_super;

   if (HMI.Action_Input0 == gcActive)
   {
      HMI.Action_Input0 = gcInActive;
      if (HMI.pg232_SimExtWeigh == TRUE)
         cfgEXT.demo |= DEMO_WEIGH_MOD;
      else
         cfgEXT.demo &= ~DEMO_WEIGH_MOD;
   }
   if (HMI.Action_Input1 == gcActive)
   {
      HMI.Action_Input1 = gcInActive;
      if (HMI.pg232_SimExtDrive == TRUE)
         cfgEXT.demo |= DEMO_DRIVE_MOD;
      else
         cfgEXT.demo &= ~DEMO_DRIVE_MOD;
   }
   if (HMI.Action_Input2 == gcActive)
   {
      HMI.Action_Input2 = gcInActive;
      if (HMI.pg232_SimHODrive == TRUE)
         cfgHO.demo |= DEMO_DRIVE_MOD;
      else
         cfgHO.demo &= ~DEMO_DRIVE_MOD;
   }
   if (HMI.Action_Input3 == gcActive)
   {
      HMI.Action_Input3 = gcInActive;
      if (HMI.pg232_SimHOPulses == TRUE)
         cfgHO.demo |= DEMO_PULSES;
      else
         cfgHO.demo &= ~DEMO_PULSES;
   }
   if (HMI.Action_Input5 == gcActive)
   {
      HMI.Action_Input5 = gcInActive;
      if (cs->sltp_mode == MONITOR)
         HMI.ChangePage = pg233_Setup_System_Sim4;
      else
         HMI.ChangePage = pg230_Setup_System_Sim1;
   }

}

void pg233_Simulation_Setup()
{
   if (HMI.Action_Input0 == gcActive)
   {
      HMI.Action_Input0 = gcInActive;
      if (HMI.pg233_SimSHODrive == TRUE)
         cfgSHO.demo |= DEMO_DRIVE_MOD;
      else
         cfgSHO.demo &= ~DEMO_DRIVE_MOD;
   }
   if (HMI.Action_Input1 == gcActive)
   {
      HMI.Action_Input1 = gcInActive;
      if (HMI.pg233_SimSHOPulses == TRUE)
         cfgSHO.demo |= DEMO_PULSES;
      else
         cfgSHO.demo &= ~DEMO_PULSES;
   }
}

/***********************************************************************/
/*   dummy_ho_loop() - sets loop into demo mode                        */
/***********************************************************************/
void dummy_ho_loop(int liw)
{
   config_HO_device *c;

   if (liw == HO_LOOP)
   {
      HMI.pg232_SimHODrive   = 1;
      HMI.pg232_SimHOPulses  = 1;
      c = &cfgHO;
   }
   else
   {
      HMI.pg233_SimSHODrive   = 1;
      HMI.pg233_SimSHOPulses  = 1;
      c= &cfgSHO;
   }

   if ((!c->cal_done))
   {
      c->nip_circumference = 2.0; /* ft */
      c->pulses_per_rev = 100.0;
      c->lenperbit = (c->nip_circumference) / c->pulses_per_rev;
      c->cal_done = TRUE;
      c->demo_max_ltp = 100.0;
   }
  c->demo = (DEMO_DRIVE_MOD || DEMO_PULSES);
}

/***********************************************************************/
/*   dummy_wth_loop() - sets loop into demo mode                       */
/***********************************************************************/
void dummy_wth_loop(void)
{
  cfgWTH.demo = TRUE;
}

/***********************************************************************/
/*   dummy_liw_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_liw_loop(int i, unsigned char DemoMask)
{
   config_LIW_device *c_LIW;

   if (i == EXT_LOOP)
      c_LIW = &cfgEXT;
   else if (i == GRAVIFLUFF_LOOP)
      c_LIW = &cfgGFLUFF;
   else if (i == GRAVIFLUFF_FDR_LOOP)
      c_LIW = &cfgGFEEDER;

   if ((DemoMask & DEMO_WEIGH_MOD) && (!c_LIW->cal_done))
   {
      /* do calc time             */
      c_LIW->calctime = 10 * TICKSPERSEC;

      /* do cal coefficients      */
      if (!c_LIW->cal_done)
      {
        if(c_LIW->use_secondary_weight_card)
        {
           c_LIW->zerowt = (int)BWH_DEMO_ZEROWT;
           c_LIW->testwt = (int)BWH_DEMO_TESTWT;
           c_LIW->test_wt = (float)BWH_DEMO_TEST_WT;
        }
        else
        {
           c_LIW->zerowt = (int)LIW_DEMO_ZEROWT;
           c_LIW->testwt = (int)LIW_DEMO_TESTWT;
           c_LIW->test_wt = (float)LIW_DEMO_TEST_WT;
        }

        c_LIW->wtperbit = c_LIW->test_wt / (float)(c_LIW->testwt - c_LIW->zerowt);
        c_LIW->cal_done = TRUE;
      }
      if (i == GRAVIFLUFF_FDR_LOOP)
         c_LIW->demo_max_wtp = 250.0;
      else if (i == GRAVIFLUFF_LOOP)
         c_LIW->demo_max_wtp = 500.0;
      else
         c_LIW->demo_max_wtp = 1000;

      /* do alarm, load wts         */
      if (i == GRAVIFLUFF_LOOP)
      {
         c_LIW->load_wt_on = 50.0;
         c_LIW->load_wt_off = 60.0;
         c_LIW->alarm_wt = 40.0;
         c_LIW->crit_wt = 20.0;
      }
      else
      {
         c_LIW->load_wt_on = (float)LIW_DEMO_LOAD_WT_ON;
         c_LIW->load_wt_off = (float)LIW_DEMO_LOAD_WT_OFF;
         c_LIW->alarm_wt = (float)LIW_DEMO_ALARM_WT;
         c_LIW->crit_wt = (float)LIW_DEMO_CRIT_WT;
      }
      /* set demo mode flag */
      c_LIW->cal_done = TRUE;
   }

   /* set drive fail error tolerance */
   c_LIW->drive_fail_error = 5;

   c_LIW->demo |= DemoMask;
}
/***********************************************************************/
/*   dummy_bwh_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_bwh_loop(void)
{

  if(!cfgBWH.cal_done)
  {
    /* do cal coefficients      */
    cfgBWH.zerowt = (int)BWH_DEMO_ZEROWT;
    cfgBWH.testwt = (int)BWH_DEMO_TESTWT;
    cfgBWH.test_wt = (float)BWH_DEMO_TEST_WT;
    cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
    cfgBWH.resolution = ((float)MAX_AD_VALUE * cfgBWH.test_wt)/
                         (10000.0 * ((float)(cfgBWH.testwt - cfgBWH.zerowt)));
    cfgBWH.cal_done = TRUE;
  }
  /* set demo mode flag */
  cfgBWH.demo = TRUE;
}

/***********************************************************************/
/*   dummy_gate_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_gate_loop(int i)
{
    /* set demo mode flag */
    cfgGATE[i].demo = TRUE;
}
/*********************************************************************/
void System_BackupRestore()
{
   int Result = 0; /* return value from function */
   int i;

   switch (HMI.CurrentPage)
   {
      case pg225_Setup_System_Backup:
         if (HMI.Action_Input4) /* User has pressed "SAVE" GTF */
         {
            ActionTriggered = 1;
            HMI.Action_Input4 = 0;
            if (HMI.pg225_save_what == 1)
               Signal.Binary_Store = 1;
            if(HMI.pg225_save_where >= 2) /* USB selected */
               Signal.USB_store = TRUE;            
            return;
         }

         if (ActionTriggered) /* User has pressed "SAVE" GTF */
         {
            uiGraphCounter++;
            if (HMI.pg225_save_where >= 2) /* USB selected */
               Signal.USB_store = TRUE;
            else if (HMI.pg225_save_where == 0)/* no selection made */
            {
               HMI.pg225_save_where = 0;
               HMI.pg225_save_what = 0;
               ActionTriggered = FALSE;
            }
            if (HMI.pg225_save_what == 1) /* Save sys config */
            {               
               visible(&HMI.PopupRuntime1,INVISIBLE);
               visible(&HMI.PopupRuntime2,INVISIBLE);
               visible(&HMI.PopupRuntime3,INVISIBLE);
               visible(&HMI.PopupRuntime4,VISIBLE);
               Result = Signal.Binary_Store_Status;
               if (Result == SUCCESS) 
               {
                  visible(&HMI.PopupRuntime4,INVISIBLE);
                  visible(&HMI.PopupRuntime1,VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  uiGraphCounter=0;
               }
               else if (Result == FAIL)
               {
                  /* generate error message */
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Disk Error!\n");
                  if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try saving configuration again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry saving configuartion again.");
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  uiGraphCounter = 0;
               }
            }
            if (HMI.pg225_save_what == 2) /* Save Recipes */
            {
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Result = Store_Recipes();
               if (Result == SUCCESS)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime1, VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  Signal.DataObj_read = NOTDONE;                  
               }
               else if (Result == FAIL)
               {
                  /* generate error message */
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Disk Error!\n");
                  if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try saving recipes again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry saving recipes again.");
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  Signal.DataObj_read = NOTDONE;                  
               }
            }
            if (HMI.pg225_save_what == 3) /* Save TIME_WT */
            {
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Result = Store_TimeWt();
               if (Result == SUCCESS)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime1, VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  Signal.DataObj_read = NOTDONE;
               }
               else if (Result == FAIL)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Disk Error!\n");
                  if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try saving time-weight table again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry saving time-weight table again.");				  
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
               }
            }
         }
         else
         {
            visible(&HMI.PopupRuntime3, INVISIBLE);
            visible(&HMI.PopupRuntime4, INVISIBLE);
         }

         if ( (HMI.Action_Input1) || /* User has pressed "OK" BEB */
            (HMI.Action_Input2) ) /* User has pressed "CLOSE" BEB */
         {
            HMI.Action_Input1 = 0;
            HMI.Action_Input2 = 0;
            visible(&HMI.PopupRuntime1, INVISIBLE);
            visible(&HMI.PopupRuntime2, INVISIBLE);
         }
         break;

      case pg226_Setup_System_Restore:
         if (HMI.Action_Input4) /* User has pressed "SAVE" GTF */
         {
            ActionTriggered = 1;
            HMI.Action_Input4 = 0;
            if (HMI.pg225_save_what == 1)
               Signal.Binary_Restore = 1;
            if(HMI.pg225_save_where >= 2) /* USB selected */
               Signal.USB_store = TRUE;
            return;
         }

         if (ActionTriggered) /* User has pressed "LOAD" GTF */
         {
            uiGraphCounter++;
            if (HMI.pg225_save_where >= 2) /* USB selected */
               Signal.USB_store = TRUE;
            else if (HMI.pg225_save_where == 0)/* no selection made */
            {
               HMI.pg225_save_where = 0;
               HMI.pg225_save_what = 0;
               ActionTriggered = FALSE;
            }
            if (HMI.pg225_save_what == 1) /* Load sys config */
            {
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Result = Signal.Binary_Restore_Status;
               if (Result == SUCCESS)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime1, VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  uiGraphCounter=0;
               }
               else if (Result == FAIL)
               {
                  /* generate error message */
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Disk Error!\n");
                  if (HMI.RestoreError)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nVersions do not match, Can't restore.");				  
                  else if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try loading configuration again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry loading configuration again.");				  
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  HMI.RestoreError = FALSE;
                  uiGraphCounter = 0;                  
               }
            }
            if (HMI.pg225_save_what == 2) /* Load Recipes */
            {
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Result = Restore_Recipes();
               if (Result == SUCCESS)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime1, VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  HMI.Load_status[RECIPE_STATUS] = STRUCTURE_LOADED;
                  Signal.FILEIO_INUSE = FALSE;
               }
               else if (Result == FAIL)
               {
                  /* generate error message */
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText,  (UDINT)"Disk Error!\n");				  
                  if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try loading recipes again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry loading recipes again.");				  
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  HMI.Load_status[RECIPE_STATUS] = STRUCTURE_DEFAULT;
                  Signal.FILEIO_INUSE = FALSE;
               }
            }
            if (HMI.pg225_save_what == 3) /* LOAD TIME_WT */
            {
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Result = Restore_TimeWt();
               if (Result == SUCCESS)
               {
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime1, VISIBLE);
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
               }
               else if (Result == FAIL)
               {
                  /* generate error message */
                  visible(&HMI.PopupRuntime4, INVISIBLE);
                  visible(&HMI.PopupRuntime2, VISIBLE);
                  brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Disk Error!\n");			
                  if (Signal.USB_store)
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nMake sure your USB device is firmly connected and try loading time-weight table again.");
                  else
                     brsstrcat((UDINT)HMI.pg225_sErrorText, (UDINT)"\nTry loading time-weight table again.");				  
                  HMI.pg225_save_where = 0;
                  HMI.pg225_save_what = 0;
                  ActionTriggered = FALSE;
                  Signal.USB_store = FALSE;
                  Signal.FILEIO_INUSE = FALSE;
                  for(i=GATE_A_tw_STATUS; i>=GATE_L_tw_STATUS; i++)
                     HMI.Load_status[i] = STRUCTURE_CRC_FAIL;
               }
            }
            if (HMI.pg225_save_what == 4) /* Recipe Special */
            {
               Signal.USB_store = HMI.pg225_save_where;
               HMI.pg225_save_where = 0;
               HMI.pg225_save_what = 0;
               visible(&HMI.PopupRuntime1, INVISIBLE);
               visible(&HMI.PopupRuntime2, INVISIBLE);
               visible(&HMI.PopupRuntime3, INVISIBLE);
               visible(&HMI.PopupRuntime4, VISIBLE);
               Signal.Recipe_Special = TRUE;
               ActionTriggered = FALSE;
            }
         }
         else
         {
            visible(&HMI.PopupRuntime3, INVISIBLE);
            visible(&HMI.PopupRuntime4, INVISIBLE);
         }
         if ( (HMI.Action_Input1) || /* User has pressed "OK" BEB */
            (HMI.Action_Input2) ) /* User has pressed "CLOSE" BEB */
         {
            HMI.Action_Input1 = 0;
            HMI.Action_Input2 = 0;
            visible(&HMI.PopupRuntime1, INVISIBLE);
            visible(&HMI.PopupRuntime2, INVISIBLE);
         }
         break;
   }
   /* check popup window status and clear popup if close pressed */
   if (HMI.Action_Input1 == gcActive)
   {
      visible(&HMI.PopupRuntime1, FALSE);
      HMI.Action_Input1 = gcInActive;
   }
   if (HMI.Action_Input2 == gcActive)
   {
      HMI.Action_Input2 = gcInActive;
      visible(&HMI.PopupRuntime2, FALSE);
   }
}

void Relays_Setup()
{
   switch (cfg_super.pg201_SelectedRelay)
   {
      case AUX_ALARM_FUNC:
         visible(&HMI.PopupRuntime1, TRUE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime5, TRUE);
         break;
      case CLEAR_ALARMS_FUNC:
      case MANUAL_BACKUP_FUNC:
      case POWDER_CLEAN_FUNC:
         visible(&HMI.SetupRuntime5, FALSE);
         visible(&HMI.PopupRuntime1, TRUE);
         visible(&HMI.PopupRuntime2, FALSE);
         break;
      case REMOTE_ON_OFF_FUNC:
         visible(&HMI.SetupRuntime5, FALSE);
         visible(&HMI.PopupRuntime1, TRUE);
         visible(&HMI.PopupRuntime2, FALSE);
         break;

      case SELF_CLEANING_FUNC:
         visible(&HMI.SetupRuntime5, FALSE);
         visible(&HMI.PopupRuntime2, TRUE);
         visible(&HMI.PopupRuntime1, FALSE);
         break;
      default:
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.SetupRuntime5, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         break;
   }

   //G2-225 1/31/17 drt
   if (HMI.pg201_SetChannel)
   {
      cfg_super.pg201_SelChannel[cfg_super.pg201_SelectedRelay] = HMI.pg201_RelayChannel;
      cfg_super.pg201_SelAct[cfg_super.pg201_SelectedRelay]     = HMI.pg201_RelayActive;
	   cfg_super.pg201_SelInv[cfg_super.pg201_SelectedRelay]     = HMI.pg201_RelayInvert;
      HMI.pg201_SetChannel = 0;
   }

   if (HMI.pg201_SetRelay)
   {
      HMI.pg201_RelayChannel = cfg_super.pg201_SelChannel[cfg_super.pg201_SelectedRelay];
      HMI.pg201_RelayActive  = cfg_super.pg201_SelAct[cfg_super.pg201_SelectedRelay];
	   HMI.pg201_RelayInvert  = cfg_super.pg201_SelInv[cfg_super.pg201_SelectedRelay];
      HMI.pg201_SetRelay = 0;
   }
}

void Units_Setup()
{
   switch (HMI.CurrentPage)
   {
      case pg195_Setup_System_Units:         
			if (HMI.Action_Input0 == gcActive)
         {
            HMI.Action_Input0 = gcInActive;
            default_units();
            Signal.EnteredSetup = TRUE; /* all fields will set this if changed gtf */
         }
			
			if (HMI.Action_Input1 == gcActive)
			{
				HMI.Action_Input1 = gcInActive;
				calc_k_factor();
				Signal.EnteredSetup = TRUE; /* all fields will set this if changed gtf */
			}
			
         if (cfg_super.units == ENGLISH)
         {
            cfg_super.width_unit  = INCH;
            cfg_super.length_unit = FEET;

            /* Weight/Length Unit */
            HMI.SetupOptionPoint2[0] = 0;    /* lb/ft */
            HMI.SetupOptionPoint2[1] = 0;    /* lb/1000 ft */
            HMI.SetupOptionPoint2[2] = 0xff; /* g/m */
            HMI.SetupOptionPoint2[3] = 0xff; /* kg/m */

            /* Weight/Area Unit */
            HMI.SetupOptionPoint3[0] = 0;    /* lb/sq ft */
            HMI.SetupOptionPoint3[1] = 0;    /* lb/ream */
            HMI.SetupOptionPoint3[2] = 0xff; /* g/sq m */
            HMI.SetupOptionPoint3[3] = 0xff; /* kg/sq m */

            /* Thickness Unit */
            HMI.SetupOptionPoint4[0] = 0;    /* inch */
            HMI.SetupOptionPoint4[1] = 0;    /* mils */
            HMI.SetupOptionPoint4[2] = 0xff; /* cm */
            HMI.SetupOptionPoint4[3] = 0xff; /* mm */
            HMI.SetupOptionPoint4[4] = 0xff; /* um */

            /* MASS Unit */
            HMI.SetupOptionPoint5[0] = 0;    /* Ounces */
            HMI.SetupOptionPoint5[1] = 0;    /* Lbs */
            HMI.SetupOptionPoint5[2] = 0xff; /* Kgrams */
            HMI.SetupOptionPoint5[3] = 0xff; /* Grams */
            HMI.SetupOptionPoint5[4] = 0xff; /* MiliGrams */
         }
         else if (cfg_super.units == METRIC)
         {
            /* Weight/Length Unit */
            HMI.SetupOptionPoint2[0] = 0xff; /* lb/ft */
            HMI.SetupOptionPoint2[1] = 0xff; /* lb/1000 ft */
            HMI.SetupOptionPoint2[2] = 0;    /* g/m */
            HMI.SetupOptionPoint2[3] = 0;    /* kg/m */

            /* Weight/Area Unit */
            HMI.SetupOptionPoint3[0] = 0xff; /* lb/sq ft */
            HMI.SetupOptionPoint3[1] = 0xff; /* lb/ream */
            HMI.SetupOptionPoint3[2] = 0;    /* g/sq m */
            HMI.SetupOptionPoint3[3] = 0;    /* kg/sq m */

            /* Thickness Unit */
            HMI.SetupOptionPoint4[0] = 0xff; /* inch */
            HMI.SetupOptionPoint4[1] = 0xff; /* mils */
            HMI.SetupOptionPoint4[2] = 0;    /* cm */
            HMI.SetupOptionPoint4[3] = 0xff; /* mm */
            HMI.SetupOptionPoint4[4] = 0;    /* um */

            /* MASS Unit */
            HMI.SetupOptionPoint5[0] = 0xff; /* Ounces */
            HMI.SetupOptionPoint5[1] = 0xff; /* Lbs */
            HMI.SetupOptionPoint5[2] = 0;    /* Kgrams */
            HMI.SetupOptionPoint5[3] = 0;    /* Grams */
            HMI.SetupOptionPoint5[4] = 0xff; /* MiliGrams */
         }
         else /* user defined */
         {
            /* some action to be added here zzz GTF*/
         }
         break;
      case pg196_Setup_System_Units1:			
			if (HMI.Action_Input1 == gcActive)
			{
				HMI.Action_Input1 = gcInActive;
				calc_k_factor();
				Signal.EnteredSetup = TRUE; /* all fields will set this if changed gtf */
			}
			
         if (cfg_super.units == ENGLISH)
         {
            /* ID Unit */
            HMI.SetupOptionPoint1[0] = 0;    /* inch */
            HMI.SetupOptionPoint1[1] = 0xff; /* mils */
            HMI.SetupOptionPoint1[2] = 0xff; /* cm */
            HMI.SetupOptionPoint1[3] = 0xff; /* mm */
            HMI.SetupOptionPoint1[4] = 0xff; /* um */

            /* width Unit */
            HMI.SetupOptionPoint2[0] = 0;    /* inch */
            HMI.SetupOptionPoint2[1] = 0;    /* mils */
            HMI.SetupOptionPoint2[2] = 0xff; /* cm */
            HMI.SetupOptionPoint2[3] = 0xff; /* mm */
            HMI.SetupOptionPoint2[4] = 0xff; /* um */

         }
         else if (cfg_super.units == METRIC)
         {
            /* ID Unit */
            HMI.SetupOptionPoint1[0] = 0xff; /* inch */
            HMI.SetupOptionPoint1[1] = 0xff; /* mils */
            HMI.SetupOptionPoint1[2] = 0;    /* cm */
            HMI.SetupOptionPoint1[3] = 0;    /* mm */
            HMI.SetupOptionPoint1[4] = 0xff; /* um */

            /* Width Unit */
            HMI.SetupOptionPoint2[0] = 0xff; /* inch */
            HMI.SetupOptionPoint2[1] = 0xff; /* mils */
            HMI.SetupOptionPoint2[2] = 0;    /* cm */
            HMI.SetupOptionPoint2[3] = 0;    /* mm */
            HMI.SetupOptionPoint2[4] = 0xff; /* um */
         }
         else /* user defined */
         {
         }
         break;
   }
}

void Save_SYS_HMI()
{
   if (HMI.Request_SYSReset > 0)
        shr_global.system_restart = TRUE;
}
/************************************************************************/
/*
   Function:  Recipe_Setup()

   Description:  setup for recipe entry. This area is so intense a seperate
        function is devoted to this task.
                                                                        */
/************************************************************************/
void Recipe_Setup()
{
   config_super *cs = &cfg_super;
   int i;
   unsigned char found;

   switch (HMI.CurrentPage)
   {
      case pg205_Setup_System_Recipe1:
         cs->man_in_recipe = FALSE;
         cs->auto_set_man_spd = FALSE;
         cs->special_in_recipe = FALSE;
         cs->layers_by = FALSE;
			/* no densities unless extrusion control is enabled */
			if (cs->wtp_mode == UNUSED)
				visible(&HMI.SetupRuntime6, FALSE);
			else
				visible(&HMI.SetupRuntime6, TRUE);

         if ((cs->wtp_mode != CONTROL) && (cs->ltp_mode !=CONTROL))
            cs->auto_switch_to_auto = FALSE;
         break;
      case pg206_Setup_System_Recipe2:
         visible(&recipe_parts, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         if (cs->wtp_mode != UNUSED || cs->ltp_mode != UNUSED)
         {
            visible(&HMI.SetupRuntime3, TRUE);

            for (i=0; i<10; i++)
            {
               HMI.SetupOptionPoint3[i] = 0xff;
               HMI.SetupOptionPoint4[i] = 0xff;
               HMI.SetupOptionPoint5[i] = 0xff;
            }

            /* Recipe entry mode - first recipe entry menu creation */
            i=0;
            do
            {
               if ((recipe_modes[i][0] == cs->application) && (recipe_modes[i][1] == cs->control_mode))
               {
                  HMI.SetupOptionPoint3[(int)recipe_modes[i][2]] = 0;
               }
            } 
            while (recipe_modes[++i][0] != -1);

            /* Recipe entry mode - second recipe entry menu setup */
            i = 0;
            found = FALSE;  /* this variable is used to trigger the visibility of the third recipe entry */
                            /* if such a trigger is needed. This should insure the third recipe entry */
                            /* will not be shown until a valid second recipe has been entered */
            do
            {
               if ((recipe_modes[i][0] == cs->application) && (recipe_modes[i][1] == cs->control_mode) &&
                  (recipe_modes[i][2] == cs->recipe_mode[0]))
               {
                  HMI.SetupOptionPoint4[(int)recipe_modes[i][3]] = 0;
                  if (cs->recipe_mode[1] == recipe_modes[i][3])
                     found = TRUE;
               }
            }
            while (recipe_modes[++i][0] != -1);
            if (HMI.SetupOptionPoint4[0] != 0)
               visible(&HMI.SetupRuntime4, TRUE);
            else
            {
               cs->recipe_mode[1] = NONE;
               visible(&HMI.SetupRuntime4, FALSE);
            }

            /* recipe entry - third recipe entry menu selection. If only two entry modes and the second mode */
            /* happens to be assigned to the third recipe entry variable, the third recipe entry input box is */
            /* used and the description text is modified to show it is a second recipe entry. */
            i = 0;
            do
            {
               if ((recipe_modes[i][0] == cs->application) && (recipe_modes[i][1] == cs->control_mode) &&
                  (recipe_modes[i][2] == cs->recipe_mode[0]) && (recipe_modes[i][3] == cs->recipe_mode[1]))
               {
                  HMI.SetupOptionPoint5[(int)recipe_modes[i][4]] = 0;
               }
            } 
            while (recipe_modes[++i][0] != -1);

            if (HMI.SetupOptionPoint5[0] != 0)
            {
               if (cs->recipe_mode[1] != NONE)
               {
                  HMI.pg206_RecipeEntryTextIndex = 0x01; /* third recipe entry */
                  if ((cs->recipe_mode[1] > 0) && (cs->recipe_mode[1] < 9))
                     visible(&HMI.SetupRuntime5, TRUE);
               }
               else
               {
                  if (HMI.SetupRuntime4 == 0)
                     HMI.pg206_RecipeEntryTextIndex = 0x01; /* third recipe entry text, third recipe entry in code*/
                  else
                     HMI.pg206_RecipeEntryTextIndex = 0x00; /* second recipe entry text, third recipe entry in code*/
                  if (found == TRUE)  /* a valid second recipe is present so now show third recipe entry */
                      visible(&HMI.SetupRuntime5, TRUE);
                   else
                      visible(&HMI.SetupRuntime5, FALSE);
               }
            }
            else
            {
               cs->recipe_mode[2] = NONE;
               visible(&HMI.SetupRuntime5, FALSE);
            }

            cs->recipe_mode[3] = NONE;
            if (cs->recipe_mode[0] == WPA)
               cs->width_in_recipe = TRUE;
            if ((cs->recipe_mode[0] == THICKNESS) || (cs->recipe_mode[0] == OD) || (cs->recipe_mode[0] == ID) ||
                (cs->recipe_mode[1] == THICKNESS) || (cs->recipe_mode[1] == OD) || (cs->recipe_mode[1] == ID) ||
                (cs->recipe_mode[2] == THICKNESS) || (cs->recipe_mode[2] == OD) || (cs->recipe_mode[2] == ID))
            {
               cs->density_in_recipe = TRUE;
               cs->width_in_recipe = TRUE;
            }
            else
               cs->density_in_recipe = FALSE;
         }
         else
         {
            visible(&HMI.SetupRuntime3, FALSE);
            visible(&HMI.SetupRuntime4, FALSE);
            visible(&HMI.SetupRuntime5, FALSE);

            cs->density_in_recipe = FALSE;
            cs->width_in_recipe = FALSE;
            for (i=0; i<4; i++)
            {
               cs->recipe_mode[i] = NONE;
            }
         }
         break;
      case pg207_Setup_System_Recipe3:
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if (HMI.SetupNumChanged1 == FALSE)
            HMI.pg207_GateRecipeUse = cfgGATE[HMI.pg207_SelectedGate].recipe_use;

         for (i=0; i<MAX_GATES; i++)
         {
            if (i < cs->total_gates)

               if (cfgGATE[i].hopper_type != REGRIND)
               {
                  HMI.SetupOptionPoint1[i] = 0;
#if 0 /* what is this supposed to accomplish? */
                  if (cfgGATE[i].hopper_type == REGRIND)
                     cfgGATE[i].hopper_type = NORMAL;
#endif
               }
               else
               {
                  HMI.SetupOptionPoint1[i] = 0xff;
#if 0 /* what is this supposed to accomplish? */
                  cfgGATE[i].hopper_type = REGRIND;
#endif
               }
            else
               HMI.SetupOptionPoint1[i] = 0xff;
         }
         if (cs->recipe_type == MOLDING_RECIPE)
            visible(&HMI.SetupRuntime4,TRUE);
         else
            visible(&HMI.SetupRuntime4,FALSE);

         if (HMI.SetupNumChanged1 == TRUE)
         {
            HMI.SetupNumChanged1 = FALSE;
            cfgGATE[HMI.pg207_SelectedGate].recipe_use = HMI.pg207_GateRecipeUse;
         }
         break;
      default:
         break;
   }
}

/************************************************************************/
/*
   Function:  Setup_Gates()

   Description:  setup for gate devices. Panel visiblity, setting up the
                 gates and saving the data.
                                                                        */
/************************************************************************/
void Setup_Gates()
{
   config_GATE_device *cnf = ((config_loop_ptr*)(&cnf_LOOP[HMI.CurrentDevice]))->GATE;

   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */
   
   if (Signal.StoreGate)
   {
      Signal.EnteredSetup = 1;
      Save_Gate_HMI();  /* Save the current settings GTF*/
      Signal.StoreGate = 0;
   }

   if (HMI.SetupNumChanged1) // prev/next Hop
      UpdateGate();

   switch (HMI.CurrentPage)
   {
      case pg012_Setup_Gates:
         Init_Gate_HMI();
         UpdateGate();         
         if (!cfg_super.blend_control)
            visible(&HMI.SetupRuntime1, FALSE);    /* Hide Loading setup Button */
         else
            visible(&HMI.SetupRuntime1, TRUE);     /* Show loading setup button */
         break;
      case pg013_Setup_Gate_Net:
         pg013_max_gate = cfg_super.total_gates + GATE_A_OUT - 1; /* 21 + number of gates -1 to make it zero based */
         if ((cnf->secondary_gate_enabled == FALSE) || (HMI.SetupLock > 1))
            HMI.SetupRuntime1 = 1;
         else
            HMI.SetupRuntime1 = 0;
         break;
      case pg014_Setup_Gate_Alarm:
		case pg023_Setup_Gate_AlarmB:
         if (cfgGATE[HMI.CurrentDevice].allowdump_proxoff == ON)
            visible((unsigned char *)&hopper_oom_time, TRUE);
         else 
            visible((unsigned char *)&hopper_oom_time, FALSE);
         HMI.pg014_AlarmFactorCalc = HMI.HMI_cfgGate.alarm_factor * shrGATE[HMI.CurrentDevice].set_tol;
         break;
      case pg015_Setup_Gate_Load:
         if ((cnf->use_powder_receiver == FALSE)||(HMI.SetupLock > 0))
            HMI.SetupRuntime1 = 1; /* locked */
         else
            HMI.SetupRuntime1 = 0;
         break;
      case pg016_Setup_Gate_Load1:
         break;
      case pg017_Setup_Gate_CLoop1:
         if ((cnf->hopper_type != REGRIND)||(HMI.SetupLock > 1))
            HMI.SetupRuntime2 = 1; /* locked */
         else
            HMI.SetupRuntime2 = 0;
         break;
      case pg018_Setup_Gate_CLoop2:
         if(cfg_super.current_security_level < SERVICE_LOCK)
         {
            visible(&HMI.SetupRuntime3, INVISIBLE); // Learn mode batch limit
         }
         else
         {
            visible(&HMI.SetupRuntime3, VISIBLE); // Learn mode batch limit
         }
         
         if (cfgGATE[HMI.CurrentDevice].proximityswitch_exist == ON)
         {   
            visible((unsigned char *)&hopper_allowdump_proxoff, TRUE);
            if (cfgGATE[HMI.CurrentDevice].allowdump_proxoff == ON)
               visible((unsigned char *)&hopper_oom_time, TRUE);
            else 
               visible((unsigned char *)&hopper_oom_time, FALSE);
         }
         else 
         {
            visible((unsigned char *)&hopper_allowdump_proxoff, FALSE);
            visible((unsigned char *)&hopper_oom_time, FALSE);
         }
         HMI.pg018_gate_target_min = cfg_super.target_accuracy;
         HMI.pg018_gate_target_max = cfgBWH.batch_oversize;
         break;
      case pg019_Setup_Gate_CLoop3:
         break;
      case pg006_Setup_Gate_CLoop4:
         if (HMI.CurrentDevice == 0)
            visible((unsigned char *)&stepsize_adjust_visible, FALSE);
         else 
            visible((unsigned char *)&stepsize_adjust_visible, TRUE);
         break;
      default:
         Signal.StoreGate = TRUE;
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         break;
   }
}

/************************************************************************/
/*
   Function:  UpdateGate()

   Description:  determines which gate is being shown and then calls to
           updates the HMI process variables to correctly display the data.
                                                                        */
/************************************************************************/
void UpdateGate()
{
   config_super *cs = &cfg_super;

   if (HMI.ChangeDevice != gNeutralDevice)
   {
      if (HMI.ChangeDevice == gNextDevice)
      {
         HMI.CurrentDevice += 1;
         if (HMI.CurrentDevice > cs->total_gates-1)  /* zero base */
            HMI.CurrentDevice = 0;
      }
      else if (HMI.ChangeDevice == gPrevDevice) /* zero base */
      {
         HMI.CurrentDevice -= 1;
         if (HMI.CurrentDevice < 0)  /* zero base */
            HMI.CurrentDevice = cs->total_gates-1;
      }
      HMI.ChangeDevice = 0;
      HMI.ChangeDevice = gNeutralDevice;
   }
   Init_Gate_HMI();  /* do this every time GTF */   
}

/************************************************************************/
/*
   Function:  Init_Gate_HMI()

   Description:  update the process variables for a gate device.
                                                                        */
/************************************************************************/
void Init_Gate_HMI()
{
   HMI_PG018_MAX_LEARN = PG018_MAX_LEARN;

   /*page 13 */
   HMI.HMI_cfgGate.primary_gate.chan = cfgGATE[HMI.CurrentDevice].primary_gate.chan;
   HMI.HMI_cfgGate.secondary_gate_enabled = cfgGATE[HMI.CurrentDevice].secondary_gate_enabled;
   HMI.HMI_cfgGate.secondary_gate.chan = cfgGATE[HMI.CurrentDevice].secondary_gate.chan;
   HMI.HMI_cfgGate.proximity_switch.chan = cfgGATE[HMI.CurrentDevice].proximity_switch.chan;
   
    /*page 14 */
   HMI.HMI_cfgGate.out_of_spec_limit = cfgGATE[HMI.CurrentDevice].out_of_spec_limit;
   HMI.HMI_cfgGate.num_bad_feeds_before_alarm = cfgGATE[HMI.CurrentDevice].num_bad_feeds_before_alarm;
   HMI.HMI_cfgGate.clear_gate_cal_after_out_of_spec = cfgGATE[HMI.CurrentDevice].clear_gate_cal_after_out_of_spec;
   HMI.HMI_cfgGate.num_no_feeds_before_out_of_mat = cfgGATE[HMI.CurrentDevice].num_no_feeds_before_out_of_mat;
   HMI.HMI_cfgGate.OOM_Alarm_Time = cfgGATE[HMI.CurrentDevice].OOM_Alarm_Time;
   HMI.HMI_cfgGate.alarm_factor = cfgGATE[HMI.CurrentDevice].alarm_factor;
   if (HMI.HMI_cfgGate.alarm_factor > 0)
      HMI.pg014_AlarmFactorCalc = HMI.HMI_cfgGate.alarm_factor * shrGATE[HMI.CurrentDevice].set_tol;
	HMI.HMI_cfgGate.num_before_dump_low = cfgGATE[HMI.CurrentDevice].num_before_dump_low;
	
   /*page 15 */
   HMI.HMI_cfgGate.creceiver.prox_primary.chan = cfgGATE[HMI.CurrentDevice].creceiver.prox_primary.chan;
   HMI.HMI_cfgGate.creceiver.out_primary.chan = cfgGATE[HMI.CurrentDevice].creceiver.out_primary.chan;
   HMI.HMI_cfgGate.use_powder_receiver = cfgGATE[HMI.CurrentDevice].use_powder_receiver;
   HMI.HMI_cfgGate.out_secondary.chan = cfgGATE[HMI.CurrentDevice].out_secondary.chan;
   HMI.HMI_cfgGate.creceiver.fill_time = cfgGATE[HMI.CurrentDevice].creceiver.fill_time;
   /*page 16 */
   HMI.HMI_cfgGate.creceiver.dump_time = cfgGATE[HMI.CurrentDevice].creceiver.dump_time;
   HMI.HMI_cfgGate.creceiver.enabled = cfgGATE[HMI.CurrentDevice].creceiver.enabled;
   /*page 17 */
   HMI.HMI_cfgGate.hopper_type = cfgGATE[HMI.CurrentDevice].hopper_type;
   HMI.HMI_cfgGate.algorithm = cfgGATE[HMI.CurrentDevice].algorithm;
   HMI.HMI_cfgGate.gate_time_clip_factor = cfgGATE[HMI.CurrentDevice].gate_time_clip_factor;
   HMI.HMI_cfgGate.tuning_method = TUNE_MANUAL;
   HMI.HMI_cfgGate.auger_used= cfgGATE[HMI.CurrentDevice].auger_used;
   HMI.HMI_cfgGate.minimum_gate_time  = cfgGATE[HMI.CurrentDevice].minimum_gate_time;
   HMI.HMI_cfgGate.time_wt_resolution = cfgGATE[HMI.CurrentDevice].time_wt_resolution;
   /*page 18 */
   HMI.HMI_cfgGate.prepulse_enabled = cfgGATE[HMI.CurrentDevice].prepulse_enabled;
   HMI.HMI_cfgGate.target_accuracy  = cfgGATE[HMI.CurrentDevice].target_accuracy;
   HMI.HMI_cfgGate.clear_recipe_on_crit_low = cfgGATE[HMI.CurrentDevice].clear_recipe_on_crit_low;
   HMI.HMI_cfgGate.learn_limit = cfgGATE[HMI.CurrentDevice].learn_limit;
   HMI.HMI_cfgGate.proximityswitch_exist = cfgGATE[HMI.CurrentDevice].proximityswitch_exist;
	HMI.HMI_cfgGate.ucRegrind3ProxEnabled = cfgGATE[HMI.CurrentDevice].ucRegrind3ProxEnabled;
   
    /*page 19 */
   HMI.HMI_cfgGate.step_size_zc = cfgGATE[HMI.CurrentDevice].step_size_zc;
   HMI.HMI_cfgGate.weight_settle_time = cfgGATE[HMI.CurrentDevice].weight_settle_time;
   HMI.HMI_cfgGate.firstshot_percentage  = cfgGATE[HMI.CurrentDevice].firstshot_percentage;
   HMI.HMI_cfgGate.allowdump_proxoff  = cfgGATE[HMI.CurrentDevice].allowdump_proxoff;
   
   /*page 6 */
   HMI.HMI_cfgGate.wt_sz_mux_1  = cfgGATE[HMI.CurrentDevice].wt_sz_mux_1;
   HMI.HMI_cfgGate.wt_sz_mux_2  = cfgGATE[HMI.CurrentDevice].wt_sz_mux_2;
   HMI.HMI_cfgGate.wt_sz_mux_3  = cfgGATE[HMI.CurrentDevice].wt_sz_mux_3;
   HMI.HMI_cfgGate.wt_sz_mux_4  = cfgGATE[HMI.CurrentDevice].wt_sz_mux_4;
   
   /*page 458 */
   HMI.HMI_cfgGate.PreFeed_Enabled = cfgGATE[HMI.CurrentDevice].PreFeed_Enabled;
}

/************************************************************************/
/*
   Function:  Save_Gate_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_Gate_HMI()
{
   float result;
   int   result_int;

   if (HMI.Request_SYSReset > 0)
        shr_global.system_restart = TRUE;

   /* page 13 */
   cfgGATE[HMI.CurrentDevice].primary_gate.chan = HMI.HMI_cfgGate.primary_gate.chan;
   cfgGATE[HMI.CurrentDevice].secondary_gate_enabled = HMI.HMI_cfgGate.secondary_gate_enabled;
   cfgGATE[HMI.CurrentDevice].secondary_gate.chan = HMI.HMI_cfgGate.secondary_gate.chan;
   cfgGATE[HMI.CurrentDevice].proximity_switch.chan = HMI.HMI_cfgGate.proximity_switch.chan;
   
    /*page 14 */
   cfgGATE[HMI.CurrentDevice].out_of_spec_limit = HMI.HMI_cfgGate.out_of_spec_limit;
   cfgGATE[HMI.CurrentDevice].num_bad_feeds_before_alarm = HMI.HMI_cfgGate.num_bad_feeds_before_alarm;
   cfgGATE[HMI.CurrentDevice].clear_gate_cal_after_out_of_spec = HMI.HMI_cfgGate.clear_gate_cal_after_out_of_spec;
   cfgGATE[HMI.CurrentDevice].num_no_feeds_before_out_of_mat = HMI.HMI_cfgGate.num_no_feeds_before_out_of_mat;
   cfgGATE[HMI.CurrentDevice].OOM_Alarm_Time =  HMI.HMI_cfgGate.OOM_Alarm_Time;
   cfgGATE[HMI.CurrentDevice].alarm_factor = HMI.HMI_cfgGate.alarm_factor;
	 cfgGATE[HMI.CurrentDevice].num_before_dump_low = HMI.HMI_cfgGate.num_before_dump_low;
   
   /*page 15 */
   cfgGATE[HMI.CurrentDevice].creceiver.prox_primary.chan = HMI.HMI_cfgGate.creceiver.prox_primary.chan;
   cfgGATE[HMI.CurrentDevice].creceiver.out_primary.chan = HMI.HMI_cfgGate.creceiver.out_primary.chan;
   cfgGATE[HMI.CurrentDevice].use_powder_receiver = HMI.HMI_cfgGate.use_powder_receiver;
   cfgGATE[HMI.CurrentDevice].out_secondary.chan = HMI.HMI_cfgGate.out_secondary.chan;
   cfgGATE[HMI.CurrentDevice].creceiver.fill_time = HMI.HMI_cfgGate.creceiver.fill_time;
   /*page 16 */
   cfgGATE[HMI.CurrentDevice].creceiver.dump_time = HMI.HMI_cfgGate.creceiver.dump_time;
   cfgGATE[HMI.CurrentDevice].creceiver.enabled = HMI.HMI_cfgGate.creceiver.enabled;
   /*page 17 */
   cfgGATE[HMI.CurrentDevice].gate_time_clip_factor = HMI.HMI_cfgGate.gate_time_clip_factor;
   cfgGATE[HMI.CurrentDevice].hopper_type = HMI.HMI_cfgGate.hopper_type;
   cfgGATE[HMI.CurrentDevice].algorithm = HMI.HMI_cfgGate.algorithm;
   cfgGATE[HMI.CurrentDevice].auger_used = HMI.HMI_cfgGate.auger_used;
   /* GTF only init_time_wt if smaller */
   if (cfgGATE[HMI.CurrentDevice].minimum_gate_time > HMI.HMI_cfgGate.minimum_gate_time)
     shrGATE[HMI.CurrentDevice].clear_gate_cal = TRUE;

   cfgGATE[HMI.CurrentDevice].minimum_gate_time = HMI.HMI_cfgGate.minimum_gate_time;

   if (cfgGATE[HMI.CurrentDevice].time_wt_resolution != HMI.HMI_cfgGate.time_wt_resolution)
     shrGATE[HMI.CurrentDevice].clear_gate_cal = TRUE;
   /* GTF this needs to be in 8ms increments due to limitations on cycle timie of TC1 */
   result = (float) (((float)HMI.HMI_cfgGate.time_wt_resolution) / ((float)TIME_WT_RESOLUTION));
   result_int = (int)result;
   if (result_int != result)
      result_int ++;
   HMI.HMI_cfgGate.time_wt_resolution = result_int * TIME_WT_RESOLUTION;
   cfgGATE[HMI.CurrentDevice].time_wt_resolution = HMI.HMI_cfgGate.time_wt_resolution;

#if 0   /* not sure what this was for but pretty sure we should let the value selected by user in pg017 stand as is */
   if (cfgGATE[HMI.CurrentDevice].hopper_type != REGRIND_MENU)
   {
      if (cfgGATE[HMI.CurrentDevice].hopper_type == REGRIND)
         cfgGATE[HMI.CurrentDevice].hopper_type = NORMAL;
   }
   else
   {
      cfgGATE[HMI.CurrentDevice].hopper_type = REGRIND;
   }
#endif

   /*page 18 */
   if (cfgGATE[HMI.CurrentDevice].prepulse_enabled != HMI.HMI_cfgGate.prepulse_enabled)
   {
   	/* if the prepulse setting has changed, the gate output has changed so the gate has to relearn */
		cfgGATE[HMI.CurrentDevice].prepulse_enabled = HMI.HMI_cfgGate.prepulse_enabled;
		shrGATE[HMI.CurrentDevice].clear_gate_cal = TRUE;
   } 
   
   cfgGATE[HMI.CurrentDevice].target_accuracy = HMI.HMI_cfgGate.target_accuracy;
   cfgGATE[HMI.CurrentDevice].clear_recipe_on_crit_low = HMI.HMI_cfgGate.clear_recipe_on_crit_low;
   cfgGATE[HMI.CurrentDevice].proximityswitch_exist = HMI.HMI_cfgGate.proximityswitch_exist;
	cfgGATE[HMI.CurrentDevice].ucRegrind3ProxEnabled = HMI.HMI_cfgGate.ucRegrind3ProxEnabled;
   
	
   cfgGATE[HMI.CurrentDevice].tuning_method = TUNE_MANUAL;
   if (HMI.HMI_cfgGate.learn_limit >= PG018_MIN_LEARN) /* is it at least */
      cfgGATE[HMI.CurrentDevice].learn_limit = HMI.HMI_cfgGate.learn_limit;
   else
   {
      HMI.HMI_cfgGate.learn_limit = 0;
      cfgGATE[HMI.CurrentDevice].learn_limit = HMI.HMI_cfgGate.learn_limit;
   }
   
   /*page 19 */
   cfgGATE[HMI.CurrentDevice].step_size_zc = HMI.HMI_cfgGate.step_size_zc;
   cfgGATE[HMI.CurrentDevice].weight_settle_time = HMI.HMI_cfgGate.weight_settle_time;
   cfgGATE[HMI.CurrentDevice].firstshot_percentage = HMI.HMI_cfgGate.firstshot_percentage;
   cfgGATE[HMI.CurrentDevice].allowdump_proxoff = HMI.HMI_cfgGate.allowdump_proxoff;
   
   /* page 6 */
   cfgGATE[HMI.CurrentDevice].wt_sz_mux_1 = HMI.HMI_cfgGate.wt_sz_mux_1;
   cfgGATE[HMI.CurrentDevice].wt_sz_mux_2 = HMI.HMI_cfgGate.wt_sz_mux_2;
   cfgGATE[HMI.CurrentDevice].wt_sz_mux_3 = HMI.HMI_cfgGate.wt_sz_mux_3;
   cfgGATE[HMI.CurrentDevice].wt_sz_mux_4 = HMI.HMI_cfgGate.wt_sz_mux_4;
   
   /* page 458 */
   cfgGATE[HMI.CurrentDevice].PreFeed_Enabled = HMI.HMI_cfgGate.PreFeed_Enabled;
}

/************************************************************************/
/*
   Function:  Setup_BWH()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_BWH()
{
   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */
   switch (HMI.CurrentPage)
   {
      case pg020_Setup_BWH:
         Init_BWH_HMI();
         break;
      case pg021_Setup_BWH_CLoop1:
/*         Signal.EnteredSetup = TRUE; all fields will set this if changed gtf */

          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         break;
      case pg022_Setup_BWH_CLoop2:
/*         Signal.EnteredSetup = TRUE; all fields will set this if changed gtf */
         break;
      default:
         /* These variables must be reset in case the previous panel had affected */
         /* their state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
      break;
   }
}

/************************************************************************/
/*
   Function:  Init_BWH_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_BWH_HMI()
{
}

/************************************************************************/
/*
   Function:  Save_BWH_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_BWH_HMI()
{
}

/************************************************************************/
/*
   Function:  Setup_MIX()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_MIX()
{
   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */
   switch (HMI.CurrentPage)
   {
      case pg030_Setup_Mixer:
         Signal.EnteredSetup = TRUE;
      break;
      case pg031_Setup_Mixer_Alarm:
/*         Signal.EnteredSetup = TRUE; all fields will set this if changed gtf */
         if (cfgMIXER.aux_starter_input_steady)
            visible(&HMI.SetupRuntime2, FALSE);  /* hide aux_alarm_time */
         else
            visible(&HMI.SetupRuntime2, TRUE); /* show aux_alarm_time*/
         break;
      case pg032_Setup_Mixer_CLoop:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);

		   if ( (cfgMIXER.material_request_source == MATERIAL_REQUEST_EXTERNAL) || 
			     (cfgMIXER.material_request_source == MATERIAL_REQUEST_GRAVITROL) )		
            visible(&HMI.SetupRuntime4, TRUE); /* shown material request invert*/
         else
            visible(&HMI.SetupRuntime4, FALSE);  /* hidden material request invert */
			

         /* GTF Rich Almeda said this should always be visible */
         visible(&HMI.SetupRuntime5,TRUE); /* shown mix time*/
#if 0
         if (cfgMIXER.mixer_motor_mode == MIX_BY_TIME)
            visible(&HMI.SetupRuntime5, TRUE); /* shown mix time*/
         else
            visible(&HMI.SetupRuntime5, FALSE);  /* hidden mix time */
#endif
         break;
      case pg033_Setup_Mixer_CLoop1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if ( (cfgMIXER.material_request_source == MATERIAL_REQUEST_EXTERNAL) || 
			    (cfgMIXER.material_request_source == MATERIAL_REQUEST_GRAVITROL) )
            visible(&HMI.SetupRuntime2, TRUE); /* shown bypass mixer prox */
         else
         {
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden bypass mixer prox */
            cfgMIXER.bypass_mixer_prox_switch = FALSE;
         }
         break;
      default:
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         break;
   }
}

/************************************************************************/
/*
   Function:  Init_MIX_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_MIX_HMI()
{
}

/************************************************************************/
/*
   Function:  Save_MIX_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_MIX_HMI()
{
   if (HMI.Request_SYSReset > 0)
        shr_global.system_restart = TRUE;
}

/************************************************************************/
/*
   Function:  Setup_WTH()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_WTH()
{
   config_super *cs = &cfg_super;
   config_WTH_device *cnf = ((config_loop_ptr*)(&cnf_LOOP[WTH_LOOP]))->WTH;

   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */
   switch (HMI.CurrentPage)
   {
      case pg150_Setup_Width:
         break;
      case pg151_Setup_Width_Net:
         Signal.EnteredSetup = TRUE;
         Save_WTH_HMI();  /* Always save the current settings */
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         if (cs->wth_type == DR_JOSEPH)
         {
            visible(&HMI.SetupRuntime2, TRUE); /* shown dr joseph ethernet? */
            visible(&HMI.SetupRuntime3, TRUE); /* shown width address */
            if (cnf->dr_joseph_ethernet == TRUE)
            {
               visible(&HMI.SetupRuntime4, TRUE); /* shown ethernet port */
               visible(&HMI.SetupRuntime5, TRUE); /* shown ip address */
               visible(&HMI.SetupRuntime6, FALSE);/* hidden Port setup */
            }
            else
            {
               visible(&HMI.SetupRuntime4, FALSE);  /* hidden ethernet port */
               visible(&HMI.SetupRuntime5, FALSE);  /* hidden ip address */
               visible(&HMI.SetupRuntime6, TRUE);   /* shown port setup */
            }
         }
         else if (cs->wth_type == KUNDIG)
         {
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden dr joseph ethernet? */
            visible(&HMI.SetupRuntime3, FALSE);  /* hidden width address */
            visible(&HMI.SetupRuntime4, FALSE);  /* hidden ethernet port */
            visible(&HMI.SetupRuntime5, FALSE);  /* hidden ip address */
            visible(&HMI.SetupRuntime6, TRUE);   /* shown port setup */
         }
         break;
      case pg152_Setup_Wdith_Alarm:
         Signal.EnteredSetup = TRUE;
         break;
      case pg153_Setup_Width_CLoop:
         Signal.EnteredSetup = TRUE;
         break;
      default:
         Signal.EnteredSetup = TRUE;
         Save_WTH_HMI();  // Always save the current settings 
          // These variables must be reset in case the previous panel had affected there state. 
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         break;
   }
}

/************************************************************************/
/*
   Function:  Init_WTH_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_WTH_HMI()
{
}

/************************************************************************/
/*
   Function:  Save_WTH_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_WTH_HMI()
{
   if (HMI.Request_SYSReset > 0)
        shr_global.system_restart = TRUE;
}
/************************************************************************/
/*
   Function:  Init_HO_HMI()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_HO_HMI()
{
}
/************************************************************************/
/*
   Function:  Setup_HO()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_HO()
{
   config_super *cs = &cfg_super;
   int num_gears;
   int i, j;

   switch (HMI.CurrentPage)
   {
      case pg080_Setup_HO:
         Init_HO_HMI();
         break;
      case pg081_Setup_HO_Net:
         Signal.EnteredSetup = TRUE;
         cfgHO.drive_has_control = FALSE;
	      for (i=0; i<4; i++)
	      {
	        if (cfgHO.drive_ip_address[i] == 0) 
	           j++;
	      }
	      if (j==4)
	      {
	      	j=0;
		      cfgHO.drive_ip_address[0]= 100;
            cfgHO.drive_ip_address[1]= 2;
            cfgHO.drive_ip_address[2]= 168;
            cfgHO.drive_ip_address[3]= 192;
	      }
	      else 
	         j = 0;
	      
         if (cs->ltp_mode != MONITOR)
         {
            visible(&HMI.SetupRuntime1, TRUE);
            if (cfgHO.drive_type == PCC)
            {
               visible(&HMI.SetupRuntime2, TRUE);  /* shown PCC drive address  */
               visible(&HMI.SetupRuntime3, FALSE); /* hidden ip address  */
               visible(&HMI.SetupRuntime4, TRUE);  /* shown drive channel   */
            }
            else if (cfgHO.drive_type == PCC_BR_AO)
            {
               visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, FALSE); /* shown ip address         */
               visible(&HMI.SetupRuntime4, FALSE); /* shown drive channel      */
            }
            #ifdef ALLEN_BRADLEY_DRIVE
            else if (cfgHO.drive_type == AB_PLC_DRIVE)
            {
               visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, TRUE);  /* shown ip address          */
	            visible(&HMI.SetupRuntime4, TRUE);  /* shown ip address          */
            }
            #endif
            else 
	         {
	            visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, FALSE); /* shown ip address          */
	            visible(&HMI.SetupRuntime4, FALSE);
	     }	
            visible(&HMI.SetupRuntime5,TRUE); /* shown drive inhibit   */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* hidden drive type  */
            visible(&HMI.SetupRuntime2, FALSE); /* hidden drive address  */
            visible(&HMI.SetupRuntime3, FALSE); /* hidden ip address  */
            visible(&HMI.SetupRuntime4, FALSE); /* hidden drive channel */
            visible(&HMI.SetupRuntime5, FALSE); /* drive inhibit  */
         }
         break;
      case pg082_Setup_HO_Alarm1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);  /* shown drive fail tolerance */
		   if(HMI.pg082_Coast_changed)
         {
			   cfgHO.max_coast_time = HMI.pg082_Coast_display; /*store coast time in mS not Sec GTF*/
			   HMI.pg082_Coast_changed = FALSE;
		   }
		   else
		   {
			   HMI.pg082_Coast_display = cfgHO.max_coast_time; /*display coast time in Sec not mS GTF*/
		   }
         if (cs->ltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime2, TRUE);  /* shown drive fails before alarm */
            visible(&HMI.SetupRuntime3, TRUE);  /* shown drive high speed limit  */
            visible(&HMI.SetupRuntime4, TRUE);  /* shown drive low speed limit */
            visible(&HMI.SetupRuntime5, TRUE);  /* shown recipe high speed limit */
         }
         else
         {
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden drive fails before alarm */
            visible(&HMI.SetupRuntime3, FALSE);  /* hidden drive high speed limit */
            visible(&HMI.SetupRuntime4, FALSE);  /* hidden drive low speed limit */
            visible(&HMI.SetupRuntime5, FALSE);  /* hidden recipe high speed limit */
         }
      break;
      case pg083_Setup_HO_Alarm2:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         if(cs->ltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1, TRUE);  /* shown recipe low speed */
            visible(&HMI.SetupRuntime2, TRUE);  /* shown ouf of spec */
            visible(&HMI.SetupRuntime3, TRUE);  /* drive fail tolerance */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE);  /* hidden recipe low speed */
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden out of spec */
            visible(&HMI.SetupRuntime3, FALSE);  /* drive fail tolerance */
         }
      break;
      case pg084_Setup_HO_CLoop1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);

         cfgHO.stretch_update_delay = 0;
         if (HMI.pg084_Calc_changed)
         {
            cfgHO.calctime = HMI.pg084_Calc_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
            HMI.pg084_Calc_changed = FALSE;
         }
         else
         {
            HMI.pg084_Calc_display = cfgHO.calctime/TICKSPERSEC; /*display coast time in Sec not mS GTF*/
         }
         if (cs->ltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime3, TRUE); /* control dead band */
            visible(&HMI.SetupRuntime4, TRUE); /* max err before forced update */
            visible(&HMI.SetupRuntime5, TRUE); /* limit speed changes */
         }
         else
         {
            visible(&HMI.SetupRuntime3, FALSE); /* control dead band */
            visible(&HMI.SetupRuntime4, FALSE); /* max err before forced update */
            visible(&HMI.SetupRuntime5, FALSE); /* limit speed changes */
         }

         if (HMI.SetupNumChanged1 == TRUE)     /* number of moving points changed */
         {
            cfgHO.rs_num = 0;
            HMI.SetupNumChanged1 = FALSE;
         }
         break;
      case pg085_Setup_HO_CLoop2:
         Signal.EnteredSetup = TRUE;
         if (cs->ltp_mode == CONTROL)
         {
            if (cfgHO.rs_clip)
            {
               visible(&HMI.SetupRuntime1, TRUE);  /* shown max_speed_change */
            }
            else
            {
               visible(&HMI.SetupRuntime1, FALSE); /* hidden max_speed_change */
            }
            visible(&HMI.SetupRuntime2, TRUE); /* number err before update */
            visible(&HMI.SetupRuntime3, TRUE); /* coast tolerance */
            visible(&HMI.SetupRuntime4, TRUE); /* initial coast value */
            visible(&HMI.SetupRuntime5, TRUE); /* percent error to coast */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* hidden max_speed_change */
            visible(&HMI.SetupRuntime2, FALSE); /* number err before update */
            visible(&HMI.SetupRuntime3, FALSE); /* coast tolerance */
            visible(&HMI.SetupRuntime4, FALSE); /* initial coast value */
            visible(&HMI.SetupRuntime5, FALSE); /* percent error to coast */
         }
         break;
      case pg086_Setup_HO_Drive1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         if (cs->ltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1, TRUE); /* Accel rate */
            visible(&HMI.SetupRuntime2, TRUE); /* Decel rate */

            visible(&HMI.SetupRuntime3, TRUE); /* zero crossing speed */
            if (HMI.SetupNumChanged3 == TRUE)
            {
               HMI.SetupNumChanged3 = FALSE;
               cfgHO.rs_num = 0;
            }
            visible(&HMI.SetupRuntime4, TRUE); /* max drive speed */
            if (HMI.SetupNumChanged4 == TRUE)
            {
               HMI.SetupNumChanged4 = FALSE;
               cfgHO.rs_num = 0;
            }
            if (!cs->spd_in_percent)
            {
            }
            else
               cfgHO.spd_factor[0] = 100.0;
            visible(&HMI.SetupRuntime5, TRUE); /* stop instantly */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* Accel rate */
            visible(&HMI.SetupRuntime2, FALSE); /* Decel rate */
            visible(&HMI.SetupRuntime3, FALSE); /* zero crossing speed */
            visible(&HMI.SetupRuntime4, FALSE); /* max drive speed */
            visible(&HMI.SetupRuntime5, FALSE); /* stop instantly */
         }
         break;
      case pg087_Setup_HO_Drive2:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime4, TRUE);

         if (cs->ltp_mode == MONITOR)
         {
            visible(&HMI.SetupRuntime1, TRUE); /* minimum ltp */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* minimum ltp */
         }

         if (cs->ltp_mode == CONTROL)
         {
            if (!cs->spd_in_percent)
            {
              visible(&HMI.SetupRuntime2, TRUE); /* gear input method */
              if (cfgHO.GearInputMethod == GEAR_IN_RECIPE)
              {
                 cs->gear_num_in_recipe = TRUE;
                 num_gears = MAX_GEARS;
              }
              else if (cfgHO.GearInputMethod == NO_GEAR_INPUT)
              {
                 cs->gear_num_in_recipe = FALSE;
                 if (cfgHO.GearInputMethod == DISCRETE_GEARS)
                    num_gears = 5;
                 else
                    num_gears = MAX_GEARS;
              }
              else
                 cs->gear_num_in_recipe = FALSE;

              if (cfgHO.GearInputMethod != NO_GEAR_INPUT)
              {
                 /* max drive speed factor per gear */
                 visible(&HMI.SetupRuntime3, FALSE); /* MAX drive speed factor */
                 visible(&HMI.SetupRuntime5, TRUE); /* setup gears */
              }
              else
              {
                 visible(&HMI.SetupRuntime5, FALSE); /* setup gears */
                 visible(&HMI.SetupRuntime3, TRUE);  /* MAX drive speed factor */
                 if (HMI.SetupNumChanged3 == 1)
                 {
                   HMI.SetupNumChanged3 = 0;
                   cfgHO.rs_num = 0;
                 }
              }
            }
            else
            {
              visible(&HMI.SetupRuntime2,FALSE); /* gear input method */
            }
         }
         else
         {
         }
         break;
      default:
         break;
   }
}
/************************************************************************/
/*
   Function:  Setup_EXT()

   Description:  update the system with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_EXT()
{
   config_super *cs = &cfg_super;
 
   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */
   switch (HMI.CurrentPage)
   {
      case pg040_Setup_Ext:
         Init_EXT_HMI();
         break;
      case pg041_Setup_Ext_Net1:
         Signal.EnteredSetup = TRUE;
         if(cs->wtp_mode != MONITOR)
         {
            visible(&HMI.SetupRuntime1,TRUE);
            if(cfgEXT.drive_type == PCC)
            {
               visible(&HMI.SetupRuntime2, TRUE);  /* shown PCC drive address  */
               visible(&HMI.SetupRuntime3, FALSE); /* hidden ip address  */
               visible(&HMI.SetupRuntime4, TRUE);  /* shown drive channel   */
            }
            else if (cfgEXT.drive_type == PCC_BR_AO)
            {
               visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, FALSE); /* shown ip address         */
               visible(&HMI.SetupRuntime4, FALSE); /* shown drive channel      */
            }
            #ifdef ALLEN_BRADLEY_DRIVE
            else if (cfgEXT.drive_type == AB_PLC_DRIVE)
            {
               visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, TRUE);  /* shown ip address          */
	            visible(&HMI.SetupRuntime4, TRUE);
            }
            #endif
            else 
	         {
	            visible(&HMI.SetupRuntime2, FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3, FALSE); /* shown ip address          */
	            visible(&HMI.SetupRuntime4, FALSE);
	         }   	
            visible(&HMI.SetupRuntime5, TRUE);     /* shown drive inhibit   */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* hidden drive type     */
            visible(&HMI.SetupRuntime2, FALSE); /* hidden drive address  */
            visible(&HMI.SetupRuntime3, FALSE); /* hidden ip address     */
            visible(&HMI.SetupRuntime4, FALSE); /* hidden drive channel  */
            visible(&HMI.SetupRuntime5, FALSE); /* drive inhibit         */
         }
         if (HMI.Action_Input0 == 1)
         {
            HMI.Action_Input0    = 0;
            Signal.ChangeIPAddr  = TRUE;
            HMI.Request_SYSReset = TRUE;
         }
         
         break;
      case pg042_Setup_Ext_Net2:
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if(cfgEXT.use_secondary_weight_card)
         {
            visible(&HMI.SetupRuntime2, FALSE); /* hidden weigh module address */
            visible(&HMI.SetupRuntime3, FALSE); /* hidden weigh module channel */
         }
         else
         {
            visible(&HMI.SetupRuntime2, TRUE); /* shown weigh module address */
            visible(&HMI.SetupRuntime3, TRUE); /* shown weigh module channel  */
         }
         break;
      case pg043_Setup_Ext_Net3:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         if (!cfgEXT.use_secondary_weight_card)
         {
            /*cfgEXT.alarm_relay.addr = cnf->weigh_mod.addr;*/
         }
         else
         {
            cfgEXT.alarm_relay.addr = 1;
         }
			if (cfg_super.wtp_mode == CONTROL)
			{
				visible(&HMI.ExtInControlRuntime, TRUE);
				visible(&HMI.ExtInMonitorRuntime, FALSE);
			}
			else 
			{
				visible(&HMI.ExtInControlRuntime, FALSE);
				visible(&HMI.ExtInMonitorRuntime, TRUE);
			}			
         break;
      case pg044_Setup_Ext_Alarm1:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);

		   if(HMI.pg044_Coast_changed)
         {
			   cfgEXT.max_coast_time = HMI.pg044_Coast_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
			   HMI.pg044_Coast_changed = FALSE;
		   }
		   else
		   {
			   HMI.pg044_Coast_display = cfgEXT.max_coast_time/TICKSPERSEC; /*display coast time in Sec not mS GTF*/
		   }
         if(cs->wtp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime5, TRUE);  /* show next/prev page buttons */
         }
         else
         {
            visible(&HMI.SetupRuntime5, FALSE);  /* hide next/prev page buttons */
         }
         break;
      case pg045_Setup_Ext_Alarm2:
         Signal.EnteredSetup = TRUE;
         Save_EXT_HMI();
         if(cs->wtp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1, TRUE);  /* shown drive fail tolerance */
            visible(&HMI.SetupRuntime2, TRUE);  /* shown drive fails before alarm */
            visible(&HMI.SetupRuntime3, TRUE);  /* shown drive high speed limit  */
            visible(&HMI.SetupRuntime4, TRUE);  /* shown drive low speed limit */
            visible(&HMI.SetupRuntime5, TRUE);  /* shown recipe high speed limit */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE);  /* hidden drive fail tolerance */
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden drive fails before alarm */
            visible(&HMI.SetupRuntime3, FALSE);  /* hidden drive high speed limit */
            visible(&HMI.SetupRuntime4, FALSE);  /* hidden drive low speed limit */
            visible(&HMI.SetupRuntime5, FALSE);  /* hidden recipe high speed limit */
         }
         break;
      case pg046_Setup_Ext_Alarm3:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         Save_EXT_HMI();
         if (cs->wtp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1, TRUE);  /* shown recipe low speed */
            visible(&HMI.SetupRuntime2, TRUE);  /* shown ouf of spec */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE);  /* hidden recipe low speed */
            visible(&HMI.SetupRuntime2, FALSE);  /* hidden out of spec */
         }
         break;
      case pg047_Setup_Ext_Load:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

		   if(HMI.pg047_load_changed){
			   cfgEXT.load_time = HMI.pg047_Load_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
			   HMI.pg047_load_changed = FALSE;
		   }
		   else
		   {
			   HMI.pg047_Load_display = cfgEXT.load_time/TICKSPERSEC; /*display coast time in Sec not mS GTF*/
		   }
//       if (cfgMIXER.material_request_source == MATERIAL_REQUEST_GRAVITROL)
//       {
            visible(&HMI.SetupRuntime2,TRUE);  /* shown use positive off weight */

            if (cfgEXT.use_positive_off_weight)
            {
               visible(&HMI.SetupRuntime3, TRUE);  /* shown load wt off */
               visible(&HMI.SetupRuntime4, FALSE);  /* hidden load time */
	            cfgEXT.load_time = 0;
	            if (ext_loading_flag)
	            {
	               shrEXT.loading = 0;	    /* force loading to be off when changeing loading method on the fly */
	               IO_dig[0].channel[cfgEXT.load_relay.chan] = 0;
	               ext_loading_flag = 0;
	            }
	            if (cfgEXT.load_wt_off == 0)
	               cfgEXT.load_wt_off = cfgEXT.load_wt_on;
            }
            else
            {
               visible(&HMI.SetupRuntime3, FALSE);  /* hidden load wt off */
               visible(&HMI.SetupRuntime4, TRUE);  /* shown load time */
	            if (ext_loading_flag)
	            {   
	               shrEXT.loading = 0;	    /* force loading to be off when changeing loading method on the fly */
	               IO_dig[0].channel[cfgEXT.load_relay.chan] = 0;
	               ext_loading_flag = 0;
	            }
	            cfgEXT.load_wt_off = cfgEXT.load_wt_on;
            }
       /*  }*/
         break;
      case pg048_Setup_Ext_CLoop1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if (HMI.SetupNumChanged1 == TRUE)     /* number of moving points changed */
         {
            cfgEXT.rs_num = 0;
            HMI.SetupNumChanged1 = FALSE;
         }
   		 if(HMI.pg048_updates_changed)
          {
			    cfgEXT.calctime = HMI.pg048_Updates_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
			    HMI.pg048_updates_changed = FALSE;
		    }
		    else
		    {
			    HMI.pg048_Updates_display = cfgEXT.calctime/TICKSPERSEC; /*display coast time in Sec not mS GTF*/
		    }
          break;
      case pg049_Setup_Ext_CLoop2:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         Save_EXT_HMI();
         if (cfgEXT.rs_clip)
         {
            visible(&HMI.SetupRuntime1, TRUE); /* shown max_speed_change */
         }
         else
         {
            visible(&HMI.SetupRuntime1, FALSE); /* hidden max_speed_change */
         }
         break;
      case pg050_Setup_Ext_CLoop3:
         Signal.EnteredSetup = TRUE;
         break;
      case pg051_Setup_Ext_Drive:
         Signal.EnteredSetup = TRUE;
         /* NOTE:  at this time the next/prev page buttons are also attached to setupruntime5
            there is no second page if extruder is not in control mode. THis will have
            to change if new parameters are added to the next page */

         /* These variables must be reset in case the previous panel had affected */
         /* their state. */
         if(cs->wtp_mode == MONITOR) /* visibility for min_wtp */
            visible(&HMI.SetupRuntime1, TRUE);   /* shown */
         else
            visible(&HMI.SetupRuntime1, FALSE);  /* hidden */

         if (cs->wtp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime2, TRUE);   /* shown accel */
            visible(&HMI.SetupRuntime3, TRUE);   /* shown decel */
            visible(&HMI.SetupRuntime4, TRUE);   /* shown zero_crossing*/
            visible(&HMI.SetupRuntime5, TRUE); /* hidden max_speed */
            if (!cs->spd_in_percent)
            {
               visible(&HMI.SetupRuntime6, TRUE);   /* shown NEXT/PREV PAGE BUTTONS */
            }
            else
            {
               visible(&HMI.SetupRuntime6, FALSE);   /* hidden NEXT/PREV PAGE BUTTONS */
               cfgEXT.spd_factor = 100.00;   /* make sure 100% */
            }

            if (HMI.SetupNumChanged4 == TRUE)
            {
               HMI.SetupNumChanged4 = FALSE;
               cfgEXT.rs_num = 0;
            }

            visible(&HMI.SetupRuntime5, TRUE);   /* shown max_speed*/
            if (HMI.SetupNumChanged5 == TRUE)
            {
               HMI.SetupNumChanged5 = FALSE;
               cfgEXT.rs_num = 0;
            }
         }
         else
         {
            visible(&HMI.SetupRuntime2, FALSE); /* hidden accel */
            visible(&HMI.SetupRuntime3, FALSE); /* hidden decel */
            visible(&HMI.SetupRuntime4, FALSE); /* hidden zero_crossing */
            visible(&HMI.SetupRuntime5, FALSE); /* hidden max_speed */
         }
         break;
      case pg052_Setup_Ext_Drive1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);

         if (cs->wtp_mode == CONTROL)
         {
            if(!cs->spd_in_percent)  /* visibility for speed factor */
            {
               visible(&HMI.SetupRuntime1, TRUE);   /* shown drive speed factor*/
            }
            else
            {
               visible(&HMI.SetupRuntime1, FALSE);  /* hidden drive speed factor*/
               cfgEXT.spd_factor = 100;
            }
         }
         else
            visible(&HMI.SetupRuntime1, FALSE);     /* hidden drive speed factor*/
         break;
      default:
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1, TRUE);
         visible(&HMI.SetupRuntime2, TRUE);
         visible(&HMI.SetupRuntime3, TRUE);
         visible(&HMI.SetupRuntime4, TRUE);
         visible(&HMI.SetupRuntime5, TRUE);
         Save_EXT_HMI();  /* Always save the current settings */
         break;
   }
}

void Init_EXT_HMI()
{
// /*config_LIW_device *cnf = ((config_loop_ptr*)(&cnf_LOOP[EXT_LOOP]))->LIW;*/
//   config_LIW_device *cnf = &cfgEXT;
//
//   cnf->drive_has_control = FALSE;
}
void Save_EXT_HMI()
{
}

/************************************************************************/
/*
   Function:  Setup_Pump()

   Description:  update the pump device with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_Pump()
{
   config_PUMP_device *cnf;
   config_super *cs = &cfg_super;

   cnf = (config_PUMP_device*)&(cs->cpump);

   /* if the HMI is designed properly, you have to go to page 12 before you go  */
   /* to any other setup page. This will insure that the Init_gate_HMI function */
   /* will be called before the save_gate_HMI function is called                */

   switch (HMI.CurrentPage)
   {
      case pg101_Setup_Pump_Net:
         Signal.EnteredSetup = TRUE;
         break;
      case pg102_Setup_Pump_CLoop:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1,TRUE);
         visible(&HMI.SetupRuntime4,TRUE);
		if(cfg_super.cpump.cont_used == 0) //  if "Continuous Run Option" is NO  DF
			visible(&HMI.SetupRuntime4,FALSE);  // hide "Continuous Run Time" 	DF

         if(cnf->dust_used)
         {
            visible(&HMI.SetupRuntime2,TRUE);  /* clean time */
            visible(&HMI.SetupRuntime3,TRUE);  /* DC num loading before cleaning */
         }
         else
         {
            visible(&HMI.SetupRuntime2,FALSE); /* clean time */
            visible(&HMI.SetupRuntime3,FALSE); /* DC num loading before cleaning */
         }

         if(cnf->cont_used)
            visible(&HMI.SetupRuntime5,TRUE);  /* continuous run time */
         else
            visible(&HMI.SetupRuntime5,FALSE); /* continuous run time */

         break;
      case pg103_Setup_Pump_CLoop1:
         Signal.EnteredSetup = TRUE;
         break;
      default:
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         visible(&HMI.SetupRuntime1,TRUE);
         visible(&HMI.SetupRuntime2,TRUE);
         visible(&HMI.SetupRuntime3,TRUE);
         visible(&HMI.SetupRuntime4,TRUE);
         visible(&HMI.SetupRuntime5,TRUE);
         break;
   }
}

/************************************************************************/
/*
   Function:  Init_Pump_HMI()

        Description:  update the pump with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_Pump_HMI()
{

}

/************************************************************************/
/*
   Function:  Save_Pump_HMI()

   Description:  update the pump with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_Pump_HMI()
{
}

/************************************************************************/
/*
   Function:  Setup_SHO()

   Description:  update the secondary hauloff device with the hmi process variables.
                                                                        */
/************************************************************************/
void Setup_SHO()
{
   config_super *cs = &cfg_super;

   switch (HMI.CurrentPage)
   {
      case pg160_Setup_Winder:
         Init_SHO_HMI();
         break;
      case pg161_Setup_Winder_Net:
         Signal.EnteredSetup = TRUE;
         visible(&HMI.SetupRuntime5,TRUE);
         cfgSHO.drive_has_control = FALSE;
         if (cs->sltp_mode != MONITOR)
         {
            visible(&HMI.SetupRuntime1,TRUE);
            if(cfgSHO.drive_type == PCC)
            {
               visible(&HMI.SetupRuntime2,TRUE); /* shown PCC drive address  */
               visible(&HMI.SetupRuntime3,FALSE); /* hidden ip address  */
            }
            else
            {
               visible(&HMI.SetupRuntime2,FALSE); /* hidden PCC drive address */
               visible(&HMI.SetupRuntime3,TRUE); /* shown ip address  */
            }
            visible(&HMI.SetupRuntime4,TRUE); /* shown drive channel */
            visible(&HMI.SetupRuntime5,TRUE); /* shown drive inhibit  */
         }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE); /* hidden drive type  */
            visible(&HMI.SetupRuntime2,FALSE); /* hidden drive address  */
            visible(&HMI.SetupRuntime3,FALSE); /* hidden ip address  */
            visible(&HMI.SetupRuntime4,FALSE); /* hidden drive channel */
            visible(&HMI.SetupRuntime5,FALSE); /* drive inhibit  */
         }
         break;
      case pg163_Setup_Winder_Alarm1:
         Signal.EnteredSetup = TRUE;
         /* These variables must be reset in case the previous panel had affected */
         /* there state. */
         visible(&HMI.SetupRuntime1,TRUE);  /* shown drive fail tolerance */
		   if (HMI.pg163_Coast_changed)
         {
			   cfgSHO.max_coast_time = HMI.pg163_Coast_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
			   HMI.pg163_Coast_changed = FALSE;
		   }
		   else
		   {
			   HMI.pg163_Coast_display = cfgSHO.max_coast_time / TICKSPERSEC; /*display coast time in Sec not mS GTF*/
		   }
         if (cs->sltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime2,TRUE);  /* shown drive fails before alarm */
            visible(&HMI.SetupRuntime3,TRUE);  /* shown drive high speed limit  */
            visible(&HMI.SetupRuntime4,TRUE);  /* shown drive low speed limit */
            visible(&HMI.SetupRuntime5,TRUE);  /* shown recipe high speed limit */
         }
         else
         {
            visible(&HMI.SetupRuntime2,FALSE);  /* hidden drive fails before alarm */
            visible(&HMI.SetupRuntime3,FALSE);  /* hidden drive high speed limit */
            visible(&HMI.SetupRuntime4,FALSE);  /* hidden drive low speed limit */
            visible(&HMI.SetupRuntime5,FALSE);  /* hidden recipe high speed limit */
         }
         break;
       case pg164_Setup_Winder_Alarm2:
          Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
          visible(&HMI.SetupRuntime4,TRUE);
          visible(&HMI.SetupRuntime5,TRUE);
          if (cs->sltp_mode == CONTROL)
          {
             visible(&HMI.SetupRuntime1,TRUE);  /* shown recipe low speed */
             visible(&HMI.SetupRuntime2,TRUE);  /* shown ouf of spec */
             visible(&HMI.SetupRuntime3,TRUE);  /* drive fail tolerance */
          }
          else
          {
             visible(&HMI.SetupRuntime1,FALSE);  /* hidden recipe low speed */
             visible(&HMI.SetupRuntime2,FALSE);  /* hidden out of spec */
             visible(&HMI.SetupRuntime3,FALSE);  /* drive fail tolerance */
          }
          break;
       case pg165_Setup_Winder_CLoop1:
          Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
          visible(&HMI.SetupRuntime1,TRUE); /* moving points */
          visible(&HMI.SetupRuntime2,TRUE); /* time between updates */
          visible(&HMI.SetupRuntime3,TRUE); /* stretch update delay */
 		    if(HMI.pg165_Calc_changed)
          {
             cfgSHO.calctime = HMI.pg084_Calc_display * TICKSPERSEC; /*store coast time in mS not Sec GTF*/
             HMI.pg165_Calc_changed = FALSE;
          }
          else
          {
             HMI.pg165_Calc_display = cfgSHO.calctime/TICKSPERSEC; /*display coast time in Sec not mS GTF*/
          }
         if (cs->sltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime4,TRUE); /* limit speed changes */
            if (cfgSHO.rs_clip)
               visible(&HMI.SetupRuntime5,TRUE); /* max_speed_change */
            else
               visible(&HMI.SetupRuntime5,FALSE); /* max_speed_change */
         }
         else
         {
            visible(&HMI.SetupRuntime4,FALSE); /* limit speed changes */
            visible(&HMI.SetupRuntime5,FALSE); /* max speed change */
         }

         if (HMI.SetupNumChanged1 == TRUE)     /* number of moving points changed */
         {
            cfgSHO.rs_num = 0;
            HMI.SetupNumChanged1 = FALSE;
         }
         break;
      case pg166_Setup_Winder_CLoop2:
         Signal.EnteredSetup = TRUE;
         if (cs->sltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1,TRUE); /* control dead band */
            visible(&HMI.SetupRuntime2,TRUE); /* rate speed reset */
            visible(&HMI.SetupRuntime3,TRUE); /* max error */
            visible(&HMI.SetupRuntime4,TRUE); /* max bad */
            visible(&HMI.SetupRuntime5,TRUE); /* percent error to coast */
         }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE); /* control dead band  */
            visible(&HMI.SetupRuntime2,FALSE); /* rate speed reset */
            visible(&HMI.SetupRuntime3,FALSE); /* max error */
            visible(&HMI.SetupRuntime4,FALSE); /* max bad */
            visible(&HMI.SetupRuntime5,FALSE); /* percent error to coast */
         }
         break;
      case pg167_Setup_Winder_CLoop3:
         Signal.EnteredSetup = TRUE;
         visible(&HMI.SetupRuntime3,TRUE);
         visible(&HMI.SetupRuntime4,TRUE);
         visible(&HMI.SetupRuntime5,TRUE);
         if (cs->sltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime1,TRUE); /* coast error */
            visible(&HMI.SetupRuntime2,TRUE); /* coast tolerance */
         }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE); /* coast error */
            visible(&HMI.SetupRuntime2,FALSE); /* coast tolerance */
         }

         break;
      case pg168_Setup_Winder_Drive1:
         Signal.EnteredSetup = TRUE;
          /* These variables must be reset in case the previous panel had affected */
          /* there state. */
         if (cs->sltp_mode == MONITOR)
         {
            visible(&HMI.SetupRuntime1,TRUE); /* MINIMUM rate */
         }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE); /* MINIMUM rate */
         }
         if (cs->sltp_mode == CONTROL)
         {
            visible(&HMI.SetupRuntime2,TRUE); /* Accel rate */
            visible(&HMI.SetupRuntime3,TRUE); /* Decel rate */

            visible(&HMI.SetupRuntime4,TRUE); /* zero crossing speed */
            if (HMI.SetupNumChanged4 == TRUE)
            {
               HMI.SetupNumChanged4 = FALSE;
               cfgSHO.rs_num = 0;
            }

            visible(&HMI.SetupRuntime5,TRUE); /* max drive speed */
            if (HMI.SetupNumChanged5 == TRUE)
            {
               HMI.SetupNumChanged5 = FALSE;
               cfgSHO.rs_num = 0;
            }

            if (!cs->spd_in_percent)
            {
               visible(&HMI.SetupRuntime6,TRUE);  /* next - prev page buttons */
            }
            else
            {
               cfgSHO.spd_factor[0] = 1.0;
               visible(&HMI.SetupRuntime6,FALSE);  /* next - prev page buttons */
            }
            visible(&HMI.SetupRuntime5,TRUE); /* max drive speed */
         }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE); /* MINIMUM rate */
            visible(&HMI.SetupRuntime2,FALSE); /* accel rate */
            visible(&HMI.SetupRuntime3,FALSE); /* decel rate */
            visible(&HMI.SetupRuntime4,FALSE); /* zero crossing speed */
            visible(&HMI.SetupRuntime5,FALSE); /* max drive speed */
         }
         break;
      case pg169_Setup_Winder_Drive2:
         Signal.EnteredSetup = TRUE;
         visible(&HMI.SetupRuntime2,TRUE);
         visible(&HMI.SetupRuntime3,TRUE);
         visible(&HMI.SetupRuntime4,TRUE);
         visible(&HMI.SetupRuntime5,TRUE);

         if (!cs->spd_in_percent)
            visible(&HMI.SetupRuntime1,TRUE);  /* speed in percent */
            if (HMI.SetupNumChanged1)
            {
               HMI.SetupNumChanged1 = FALSE;
               cfgSHO.rs_num = 0;
            }
         else
         {
            visible(&HMI.SetupRuntime1,FALSE);  /* speed in percent */
            cfgSHO.spd_factor[0] = 100.0;
         }
         break;

      default:
         break;
   }
}

/************************************************************************/
/*
   Function:  Init_SHO_HMI()

        Description:  update the secondary hauloff with the hmi process variables.
                                                                        */
/************************************************************************/
void Init_SHO_HMI()
{

   HMI.SetupNumChanged1 = FALSE;
   HMI.SetupNumChanged2 = FALSE;
   HMI.SetupNumChanged3 = FALSE;
   HMI.SetupNumChanged4 = FALSE;
   HMI.SetupNumChanged5 = FALSE;
}

/************************************************************************/
/*
   Function:  Save_SHO_HMI()

   Description:  update the secondary hauloff  with the hmi process variables.
                                                                        */
/************************************************************************/
void Save_SHO_HMI()
{

}


#if 0
void ReInit_Device(int loop)
{


}
#endif
void Setup_Sys_Alarms()
{
   config_super *cs = &cfg_super;
   int i;
   int Done;

/* setup device list pull down menu selection */
   for (i=0;i<=20;i++ )
   {
      if (i == 0)
         HMI.pg181_AlarmDevicesSetupOptions[i] = 0xff;
      else if (i-1<cs->total_gates)
         HMI.pg181_AlarmDevicesSetupOptions[i] = 0;
      else
      {
         switch (i)
         {
            case 17:       /* system */
            case 18:       /* mixer */
            case 13:       /* batch hopper */
               HMI.pg181_AlarmDevicesSetupOptions[i] = 0;
               break;
            case 14:       /* extruder */
               if (cs->wtp_mode  != UNUSED)
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0;
               else
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0xff;
               break;
            case 15:       /* haul off */
               if (cs->ltp_mode  != UNUSED)
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0;
               else
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0xFF;
               break;
            case 16:       /* winder */
               if (cs->sltp_mode  != UNUSED)
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0;
               else
                  HMI.pg181_AlarmDevicesSetupOptions[i] = 0xFF;
               break;
            default:
               HMI.pg181_AlarmDevicesSetupOptions[i] = 0xFF;
               break;
         }

      }
   }

/* a device is selected, now set up the alarm type selection */
   if (HMI.SetupNumChanged1 == 1)
   {

      for (i=0;i<100;i++ )
      {
         HMI.pg181_AlarmsSetupOptions[i] = 0xff;
      }

      if (HMI.pg181_AlarmDevice-1 < MAX_GATES)
      {
         gAlarmDevice = TYPE_GATE;
         gAlarmDeviceLoop = HMI.pg181_AlarmDevice-1;
      }
      else
      {
         switch (HMI.pg181_AlarmDevice-1)
         {
            case 12:
               gAlarmDevice = TYPE_BWH;
               gAlarmDeviceLoop = 0;
               break;
            case 13:
               gAlarmDevice = TYPE_LIW;
               gAlarmDeviceLoop = 0;
               break;
            case 14:
               gAlarmDevice = TYPE_HO;
               gAlarmDeviceLoop = 0;
               break;
            case 15:
               gAlarmDevice = TYPE_HO;
               gAlarmDeviceLoop = 1;
               break;
            case 16:
               gAlarmDevice = TYPE_SYSTEM;
               gAlarmDeviceLoop = 0;
               break;
            case 17:
               gAlarmDevice = TYPE_MIX;
               gAlarmDeviceLoop = 0;
               break;
            default:
               return;
               break;
         }
      }

      i = 0;
      while (cs->AlarmAction[i].alarm_type != NOT_USED)
      {
         if (cs->AlarmAction[i].device_type == gAlarmDevice)
         {
            HMI.pg181_AlarmsSetupOptions[cs->AlarmAction[i].alarm_type] = 0;
         }
         i++;
      }
      for (i=0;i<14; i++ )
      {
         HMI.SetupOptionPoint1[i] = 0xff;
      }
      HMI.SetupOptionPoint1[0] = 0;
      HMI.SetupOptionPoint1[1] = 0;
      HMI.SetupOptionPoint1[2] = 0;
      HMI.SetupOptionPoint1[4] = 0;

     visible(&HMI.SetupRuntime1,TRUE);
     visible(&HMI.SetupRuntime2,FALSE);
      HMI.pg181_AlarmType = 0;
      HMI.pg181_AlarmActionPaused = 0;
      HMI.pg181_AlarmActionManual = 0;
      HMI.pg181_AlarmActionAutomatic = 0;
      HMI.SetupNumChanged1 = 0;
   }

/* the device and alarm type is selected, present the action for each mode. */
   if (HMI.SetupNumChanged2 == 1)
   {
      i=0;
      gAlarmActionIndex=0;
      Done = FALSE;
      while (!Done)
      {
         if (cs->AlarmAction[i].alarm_type == NOT_USED)
            Done = TRUE;
         else if ((cs->AlarmAction[gAlarmActionIndex].device_type == gAlarmDevice)&&(cs->AlarmAction[gAlarmActionIndex].alarm_type == HMI.pg181_AlarmType))
         {
            if (cs->AlarmAction[gAlarmActionIndex].device == gAlarmDeviceLoop  )
               Done = TRUE;
            else
            {
               Done = FALSE;
               gAlarmActionIndex++;
            }
         }
         else
         {
            Done = FALSE;
            gAlarmActionIndex++;
         }
         i++;
      }

      HMI.pg181_AlarmActionPaused = cs->AlarmAction[gAlarmActionIndex].pause_action;
      HMI.pg181_AlarmActionManual = cs->AlarmAction[gAlarmActionIndex].man_action;
      HMI.pg181_AlarmActionAutomatic = cs->AlarmAction[gAlarmActionIndex].auto_action;

      visible(&HMI.SetupRuntime2,TRUE);
      HMI.SetupNumChanged2 = 0;
   }

   /* an action for one of the modes has changed. Update the default values */
   if (HMI.SetupNumChanged3 == 1)
   {
      cs->AlarmAction[gAlarmActionIndex].pause_action = HMI.pg181_AlarmActionPaused;
      cs->AlarmAction[gAlarmActionIndex].man_action = HMI.pg181_AlarmActionManual;
      cs->AlarmAction[gAlarmActionIndex].auto_action = HMI.pg181_AlarmActionAutomatic;

      HMI.SetupNumChanged3 = 0;
   }
}

