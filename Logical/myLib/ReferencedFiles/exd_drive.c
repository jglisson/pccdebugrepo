#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  exd_drive.c

   Description: This file is the low level driver for the c1200 type exd
         interface.

      $Log:   F:\Software\BR_Guardian\exd_drive.c_v  $
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *
 *    Rev 1.6   Aug 04 2008 14:08:46   jlr
 * add debug stuff
 *
 *
 *    Rev 1.5   Jul 28 2008 09:44:18   gtf
 * Added logic to skip check_auto_man when extruder is paused
 *
 *    Rev 1.3   May 16 2008 09:23:02   Barry
 * Added setting of act_spd_bits
 *
 *    Rev 1.2   Mar 27 2008 08:46:00   FMK
 * Made changes for drive type identification.
 *
 *    Rev 1.1   Mar 07 2008 10:28:52   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.0   Feb 22 2008 10:42:14   FMK
 * Initial version. These are the lower level driver
 * files for controlling the output to drive systems.

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <math.h>
#include "plc.h"
#include "relaydef.h"
#include "sys.h"
#include "exd_drive.h"
#include "mem.h"
#include "libbgdbg.h"
#include "hmi.h"

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/* globals we'll need so we can do debug stuff */
extern global_struct *g;  /* the super global */


/* on the current EXD the external clock rate is 4.9152 Mhz. The internal clock
   rate is 1/4 of that = 1.2288 Mhz. This equates to 122.88 ticks/ms. The default
   accel/decel value is 15003 ticks/bit. This equates to 122 ms/bit accel rate.
   This task will run at an increment of 20 ms cycle time. The accel rate needs
   to reflect the cycle time / bit value */

_LOCAL unsigned char gDev_Init;

unsigned char gDriveType;

unsigned int gDefaultAccel;
unsigned int gDefaultDecel;
unsigned int gPulses;
TIME gPulseTime;
unsigned short gDriveInhibitCounter;
unsigned char gRampDir;
unsigned int gRampTime;
TIME gRampStartTime;
unsigned char gRampFlag;
unsigned char gIntExtControl;
unsigned char demo = FALSE;

extern TIME get_time(void);
extern unsigned int elap_time(TIME initial_DTime);

void CardLoop();
void UpdateDriveOutput();
void EmergencyShutdown();
void AdjustSpeed();
int Check_DriveInh();
void Check_AutoMan();
void ControlDrive();

int calc_demo_relay(int relay);

 shared_LIW_device *s;
 struct drive_command_struct DriveCommand[MAX_DRIVES];
/************************************************************************/
/*
   Function:  ControlDrive()

   Description:  This function is the main cyclic task for the driver. It
         processes the initial state and the drive commands.
                                                                        */
/************************************************************************/
void ControlDrive()
{
  if (gDev_Init == FALSE)
  {
    gDriveInhibitCounter = 0;
    gCurrentSpeed[EXD_DRIVE] = 0;
    gFinalSpeed[EXD_DRIVE] = 0;
    gDefaultDecel = 50;
    gDefaultAccel = 50;
    gIntExtControl = Bit_IntDrv_Ctrl;
    gRampDir = RAMPNONE;
    gRampFlag = FALSE;

    UpdateDriveOutput();

    gDev_Init = TRUE;

    if (debugFlags.exdDrive)
      bgDbgInfo (&g_RingBuffer1, "exd_drive.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "drive init'ed\r\n");
   }
   
   DriveCommand[EXD_DRIVE].CMD = SCP_SETSPEED;	       /* only adjust speed */

   if (DriveCommand[EXD_DRIVE].CMDRdy)
   {
      switch (DriveCommand[EXD_DRIVE].CMD)
      {
         case SCP_ID:
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETACCEL:
            gDefaultAccel = DriveCommand[EXD_DRIVE].AccelDecelValue;
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETDECEL:
            gDefaultDecel = DriveCommand[EXD_DRIVE].AccelDecelValue;
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETSPEED:
            gFinalSpeed[EXD_DRIVE] = DriveCommand[EXD_DRIVE].SetSpeed;
            if (DriveCommand[EXD_DRIVE].AccelDecelValue < 0)
            {
               if (gFinalSpeed[EXD_DRIVE] < gCurrentSpeed[EXD_DRIVE])
                  gRampTime = gDefaultDecel;
               else
                  gRampTime = gDefaultAccel;
            }
            else
              /* gRampTime = DriveCommand[EXD_DRIVE].AccelDecelValue;*/
	          gRampTime = DriveCommand[EXD_DRIVE].RampTime;   /* in seconds */
			  
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_GETSPEED:
            /*        DriveCommand[EXD_DRIVE].SetSpeed = gCurrentSpeed[EXD_DRIVE];*/
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_GETPULSES:
            DriveCommand[EXD_DRIVE].Pulses = gPulses;
            DriveCommand[EXD_DRIVE].Time = gPulseTime;
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_EMERGENCY:
            EmergencyShutdown();
            DriveCommand[EXD_DRIVE].CMDRdy = FALSE;
            DriveCommand[EXD_DRIVE].CMDReturn = TRUE;
            DriveCommand[EXD_DRIVE].Status  = SCP_ACK;
            break;
      }
   }
   if (cfgEXT.drive_type != AB_PLC_DRIVE)
      CardLoop();
   
   shrEXT.act_spd_bits = ANALOG_OUT_EXTRUDER;

   if (cfgEXT.drive_type != AB_PLC_DRIVE)
      shrEXT.act_spd = (float) shrEXT.act_spd_bits/OVF_INT;
   
   if (EXT_HO_OPER_MODE == PAUSE_MODE)
     gFinalSpeed[EXD_DRIVE] = 0;
}

/************************************************************************/
/*
   Function:  CardLoop()

   Description:  This function applies the correct output algorithm based
         on the drive type. It also monitors key digital inputs.
                                                                        */
/************************************************************************/
void CardLoop()
{
   unsigned short Result;
   int Result_DI;

	if (cfgEXT.drive_type == PCC_BR_AO)
	{
		if (gFinalSpeed[EXD_DRIVE] >= 0)
		{
			if (gCurrentSpeed[EXD_DRIVE] != gFinalSpeed[EXD_DRIVE])
				AdjustSpeed();
			else 
				s->ramping = FALSE;
		}
 
		Result_DI = Check_DriveInh();
		if(shr_global.ext_ho_oper_mode != PAUSE_MODE) /* gext_ho_oper_mode  ? Extruder mode is not Pause (80) GTF */
		{
			Check_AutoMan();
		}
	}
	else if (cfgEXT.drive_type == PCC_BR_AODI)
	{
		if ((gIntExtControl & Bit_IntDrv_Ctrl) > 0)
		{  /* currently the driver is in control */
  	      Result = IO_dig[X20].channel[IntExtDrv_CTRL_EXT];  /* if relay is true then the driver is set to control */
			if (Result == TRUE)
			{  /* the driver was controlling and it is still set to control */
				if (gRampFlag == FALSE)
				{  /* if speed not in window of resolution then adjust the speed */
					if (gFinalSpeed[EXD_DRIVE] >= 32)
					{
						if (gCurrentSpeed[EXD_DRIVE] != gFinalSpeed[EXD_DRIVE])
						{
							AdjustSpeed();
						}
					}
				}
				else
				{
					/* we are ramping, the AdjustSpeed routine will set the output when accel/decel times out */
					AdjustSpeed();
				}
				gIntExtControl = Bit_IntDrv_Ctrl;
				Result_DI = Check_DriveInh();
			}
			else
			{  /* the driver was controlling and now the external source want to control */
				gRampFlag = FALSE;
				Result_DI = Check_DriveInh();
				gIntExtControl = Bit_ExtDrv_Ctrl;
			}

		}
		else if ((gIntExtControl & Bit_ExtDrv_Ctrl) > 0)
		{  /* currently the external source is in control */
			Result = IO_dig[X20].channel[IntExtDrv_CTRL_EXT];  /* if relay is true then the driver is set to control */
			if (Result == TRUE)
			{  /* the external source was in control and now the driver wants to control */
				gRampFlag = FALSE;
				Result_DI = Check_DriveInh();
				gIntExtControl = Bit_IntDrv_Ctrl;
			}
			else
			{  /* the external source was in control and it is still in control */
				if (gRampFlag == FALSE)
				{  /* if speed not in window of resolution then adjust the speed */
					if (gFinalSpeed[EXD_DRIVE] >= 32)
					{
						if (gCurrentSpeed[EXD_DRIVE] != gFinalSpeed[EXD_DRIVE])
						{
							AdjustSpeed();
						}
					}
					else /* need to look at up down relays and see if we should adjust the output */
					{
						if (IO_dig[X20].channel[EXTSpeed_IN_Up])
						{
							gFinalSpeed[EXD_DRIVE] = gCurrentSpeed[EXD_DRIVE]+32;
							AdjustSpeed();
						}
						else if (IO_dig[X20].channel[EXTSpeed_IN_Dn])
						{
							gFinalSpeed[EXD_DRIVE] = gCurrentSpeed[EXD_DRIVE]-32;
							AdjustSpeed();
						}
						else
							gFinalSpeed[EXD_DRIVE] = gCurrentSpeed[EXD_DRIVE];
					}
				}
				else
				{
					/* we are ramping, the AdjustSpeed routine will set the output when accel/decel times out */
					AdjustSpeed();
				}
				gIntExtControl = Bit_ExtDrv_Ctrl;
				Result_DI = Check_DriveInh();
				if (Result_DI == TRUE)
				{  /* the plc is in control and it wants to drive inhibit, shut down immediately */
					gCurrentSpeed[EXD_DRIVE] = 0;
					gFinalSpeed[EXD_DRIVE] = 0;
					gRampFlag = FALSE;

					UpdateDriveOutput();
				}
			}
		}
	}
}
/************************************************************************/
/*
   Function:  Check_AutoMan()

   Description:  This function looks at the auto manual input relay for
         the device defined by gDnum and sets the appropriate status
         bit in the device specific status word.

         if auto/manual bit is low then in auto.
         if auto/manual bit is high then in manual
                                                                        */
/************************************************************************/
void Check_AutoMan()
{
   unsigned short RelayState;

   RelayState = (IO_dig[X20].channel[AutoMan_EXT]);

   /* if input is high then auto light on panel is lit, so then in automatic,
      clear the auto/manual bit to signal in auto */
   if (RelayState == TRUE)
   {
      /* drive in auto */
      DriveCommand[EXD_DRIVE].IOStatus &= ~SCP_BIT_DriveMan;
      IO_dig[X20].channel[EXTManual_Out_0] = ON;
      IO_dig[X20].channel[EXTManual_Out_1] = ON;
   }
   else
   {
      /* drive in manual */
      DriveCommand[EXD_DRIVE].IOStatus |= SCP_BIT_DriveMan;
      IO_dig[X20].channel[EXTManual_Out_0] = OFF;
      IO_dig[X20].channel[EXTManual_Out_1] = OFF;
   }
}
/************************************************************************/
/*
   Function:  Check_DriveInh()

   Description:  This function looks at the drive inhibit input relay for
         the device defined by gDnum and set the appropriate status bit
         in the device specific status word. It will also if inhibited
         reset the drive output to zero.  Because this can be a dangerous
         situation with noise spikes, the drive inhibit must be tested
         at least five times before triggerine the actual drive inhibit.
                                                                        */
/************************************************************************/
int Check_DriveInh()
{
   unsigned short RelayState;
   int Result;

   Result = FALSE;
   RelayState = (IO_dig[X20].channel[DriveInh_Ext]);

   if (RelayState == TRUE)
   {
      if (gDriveInhibitCounter <= DRIVEINH_TIMEOUT)
         gDriveInhibitCounter++;
   }
   else
      gDriveInhibitCounter = 0;

   if (gDriveInhibitCounter > DRIVEINH_TIMEOUT)
   {
      DriveCommand[EXD_DRIVE].IOStatus |= SCP_BIT_DriveInh;
      Result = TRUE;
   }
   else
      DriveCommand[EXD_DRIVE].IOStatus &= ~SCP_BIT_DriveInh;

   return(Result);
}
/************************************************************************/
/*
   Function:  AdjustSpeed()

   Description:  This function looks at the current and final speed values
         and appropriately sets the output in the correct direction. A
         new ramp start time is also retrieved so the calucation for the
         next bit change can be accurately made.
                                                                        */
/************************************************************************/
void AdjustSpeed()
{
	int  AccelOutputTolerance;
   REAL fSpeedChange=0.95;
   REAL fSpdDiff=0.0;  
   REAL fSpdChangePercentEXT;   
   REAL fSpdChangePercentHO;   

	if (DriveCommand[EXD_DRIVE].AccelDecelValue <= 0.001)    
		DriveCommand[EXD_DRIVE].AccelDecelValue = 0.02;
	
	AccelOutputTolerance = 16 * DriveCommand[EXD_DRIVE].AccelDecelValue * 100;
	s->ramping = gCurrentSpeed[EXD_DRIVE] != gFinalSpeed[EXD_DRIVE];

	if ((gCurrentSpeed[EXD_DRIVE] > gFinalSpeed[EXD_DRIVE] - AccelOutputTolerance) && 
		(gCurrentSpeed[EXD_DRIVE] < gFinalSpeed[EXD_DRIVE] + AccelOutputTolerance))
	{
		gCurrentSpeed[EXD_DRIVE] = gFinalSpeed[EXD_DRIVE];  
	}
	else
	{
		if (gCurrentSpeed[EXD_DRIVE] < gFinalSpeed[EXD_DRIVE])
		{
			if (gCurrentSpeed[EXD_DRIVE] <= (DINT)OVF_INT - AccelOutputTolerance)
				gCurrentSpeed[EXD_DRIVE] += AccelOutputTolerance;   /* default, increment by 0.1% speed per 50ms namely 2%/s */
			else
				gCurrentSpeed[EXD_DRIVE] = OVF_INT;
		}
		else if (gCurrentSpeed[EXD_DRIVE] > (gFinalSpeed[EXD_DRIVE]))
		{
			if (gCurrentSpeed[EXD_DRIVE] >= AccelOutputTolerance)
				gCurrentSpeed[EXD_DRIVE] -= AccelOutputTolerance;   /* decrement by 0.1% speed */
			else
				gCurrentSpeed[EXD_DRIVE] = 0;
		}
	}
   
   if  ((gFinalSpeed[EXD_DRIVE] == 0) && (gCurrentSpeed[EXD_DRIVE] <= AccelOutputTolerance))
		gCurrentSpeed[EXD_DRIVE] = 0;

   //SYNC Extruder speed and Haul-Off.  Haul-Off is the master
   if ((cfg_super.ltp_mode == CONTROL) && (shrHO.ramping == TRUE) && 
       (cfg_super.wtp_mode == CONTROL) && (s->ramping == TRUE))
   {
      fSpdChangePercentHO = fabs((gCurrentSpeed[HO_DRIVE] - gFinalSpeed[HO_DRIVE]) / 327.67);
      fSpdChangePercentEXT = fabs((gCurrentSpeed[EXD_DRIVE] - gFinalSpeed[EXD_DRIVE]) / 327.67);

      if ((fSpdChangePercentHO > .9) && (fSpdChangePercentHO > fSpdChangePercentEXT))
      {   
         fSpdDiff = fabs(fSpdChangePercentHO - fSpdChangePercentEXT);  //slow down
         if (fSpdDiff > 1.0)
         {
            if (gCurrentSpeed[EXD_DRIVE] < gFinalSpeed[EXD_DRIVE])
               gCurrentSpeed[EXD_DRIVE] -= (DINT)(fSpeedChange * AccelOutputTolerance);
            else
               gCurrentSpeed[EXD_DRIVE] += (DINT)(fSpeedChange * AccelOutputTolerance);
         }
      }
      else if ((fSpdChangePercentHO > .9) && (fSpdChangePercentHO < fSpdChangePercentEXT))
      {   
         fSpdDiff = fabs(fSpdChangePercentHO - fSpdChangePercentEXT); // Slow down
         if (fSpdDiff > 1.0)
         {
            if (gCurrentSpeed[EXD_DRIVE] < gFinalSpeed[EXD_DRIVE])
               gCurrentSpeed[EXD_DRIVE] += (DINT)(fSpeedChange * AccelOutputTolerance);
            else
               gCurrentSpeed[EXD_DRIVE] -= (DINT)(fSpeedChange * AccelOutputTolerance);         
         }
      }
      if (gCurrentSpeed[EXD_DRIVE] > OVF_INT)
         gCurrentSpeed[EXD_DRIVE] = OVF_INT;
   }
   else
   {
      fSpdChangePercentHO = 0.0;
      fSpdChangePercentEXT = 0.0;
   }

	UpdateDriveOutput();
}

/************************************************************************/
/*
   Function:  EmergencyShutdown()

   Description:  This function retruns the system to a know initial safe
         state. the outputs that were turned on are reset and the speed
         values are immediately returned to zero as well.
                                                                        */
/************************************************************************/
void EmergencyShutdown()
{
   gCurrentSpeed[EXD_DRIVE] = 0;
   gFinalSpeed[EXD_DRIVE] = 0;
   gRampDir = RAMPNONE;
   gRampFlag = FALSE;

   UpdateDriveOutput();

   IO_dig[X20].channel[EXTManual_Out_0] = OFF;
   IO_dig[X20].channel[EXTManual_Out_1] = OFF;
   IO_dig[X20].channel[HOManual_Out_0] = OFF;
   IO_dig[X20].channel[HOManual_Out_1] = OFF;
}

/************************************************************************/
/*
   Function:  UpdateDriveOutput()

   Description:  This function set the analog output on the x20 output
         module. The gDnum variable is used to determine which output to
         set.
                                                                        */
/************************************************************************/
void UpdateDriveOutput()
{
	if (cfg_super.wtp_mode == CONTROL) 
	{
		ANALOG_OUT_EXTRUDER = gCurrentSpeed[EXD_DRIVE];
	}
}

/************************************************************************/
/*
   Function:  calc_demo_relay(void)

   Description:


        Returns:  VOID
                                                                        */
/************************************************************************/
int calc_demo_relay(int relay)
{
  return(FALSE);

}


