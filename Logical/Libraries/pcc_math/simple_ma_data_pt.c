#include <stdio.h>
#include "pcc_math.h"

signed short simple_ma_add_pt(struct moving_avg_str *ma_ptr,float data)
{
    register int index;

	if ((ma_ptr == NULL) ||(ma_ptr->num_pts <= 0))
		return 0;

    index = ma_ptr->idx;

    if(ma_ptr->num_valid < ma_ptr->num_pts)
    {
        ma_ptr->num_valid++;                /* another valid entry made */
    }
    else
    {
        /* need to subtract out the entry that falls out the moving avg */
        ma_ptr->curr_sum -= ma_ptr->data[index];
    }

    /* enter data into array */
    ma_ptr->data[index] = data;

    /* add new entry into the sum */
    ma_ptr->curr_sum = ma_ptr->curr_sum + data;

    /* recalculate average */
    if(ma_ptr->num_valid < ma_ptr->num_pts)
    {
        if(ma_ptr->num_valid > 0)
            ma_ptr->curr_avg = ma_ptr->curr_sum / (float)ma_ptr->num_valid;     /* DIVIDE */
        else
            ma_ptr->curr_avg = 0.0;
    }
    else
    {
        if(ma_ptr->num_pts > 0)
            ma_ptr->curr_avg = ma_ptr->curr_sum / (float)ma_ptr->num_pts;       /* DIVIDE */
        else
            ma_ptr->curr_avg = 0.0;
    }

    /* get index ready for next entry */
    ma_ptr->idx = (ma_ptr->idx + 1) % ma_ptr->num_pts;

   return(0);
}


