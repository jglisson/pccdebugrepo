#ifndef _INC_PRINTLIB_
#define _INC_PRINTLIB_

#include "bur/plctypes.h"

#if 0
enum
{
    PRINT_OK = 0,
    PRINT_ERROR_CONNECT,
    PRINT_ERROR_INVALID_ARG,
    PRINT_ERROR_BUSY,
	PRINT_ERROR_VERSION_MISMATCH,
    PRINT_ERROR_FAILED,
    PRINT_ERROR_DISCONNECT
};
#endif

enum
{
    PRINT_STATUS_UNINIT=0,
    PRINT_STATUS_IDLE,
    PRINT_STATUS_BUSY,
    PRINT_STATUS_ERROR,
    PRINT_STATUS_NOPAPER
};

/*
  Note: Always sync this structure with the AR USB printer class driver
*/
typedef struct sShdMemPrinter
{
	unsigned int uiVersion;
	unsigned int uiPrinterState;
	unsigned int uiFifoID;
	unsigned int bShutdown;
} sShdMemPrinter;

typedef struct sPrinter
{
	sShdMemPrinter *pShdMemPrinter;
    unsigned int uiStatus;
    unsigned int uiErrorCode;
    unsigned int uiShdMemID;
} sPrinter;

#endif


