/********************************************************************
 * COPYRIGHT -- Piedmont Automation
 ********************************************************************
 * Package: Communication
 * File: commInc.h
 * Created: September 15, 2009
 *******************************************************************/

#ifndef COMM_INC_H
#define COMM_INC_H

#include <bur/plctypes.h>

#include <plc.h>
#include "remote.h"
#include "recipe.h"
#include "readsub.h"
#include "remsub.h"
#include "writesub.h"

#include <Communication/readfun.c>
#include <Communication/readvar.c>
#include <Communication/writefun.c>
#include <Communication/writevar.c>
#include <Communication/rem_fun.c>
#include <Communication/rem_vars.c>
#include <Communication/remlink.c>
#include <Communication/net.c>

#endif
