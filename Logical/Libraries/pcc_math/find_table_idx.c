#include "pcc_math.h"

signed short find_table_idx(data_table *table,float xval,signed short *hi_idx,signed short *low_idx)
{
    int idx;
    int hidx;
    int lidx;
    int done = 0;
    ordered_pair *d;

    d = table->data;
    idx = 0;
    hidx = -1;
    lidx = -1;

    if(table->current_length == 0) done = 1;

    while(!done)
    {
        if(d->x >= xval && hidx < 0)
        {
            hidx = idx;
        }

        if(d->x <= xval)
        {
            lidx = idx;
        }

        if(hidx >= 0 && lidx >= 0)
            done = 1;

        ++d;
        if(++idx >= table->current_length)
            done = 1;
    }

    *hi_idx = hidx;
    *low_idx = lidx;

    return(0);
}


