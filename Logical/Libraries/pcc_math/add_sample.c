#include "pcc_math.h"

signed short add_sample(samples *s_data,float xdata,float ydata)
{
    ordered_pair *d;

    d = s_data->pts;

    d[s_data->next].x = xdata;
    d[s_data->next].y = ydata;
    if(++s_data->next >= s_data->length) s_data->next = 0;
    ++s_data->num_taken;

    return(0);
}


