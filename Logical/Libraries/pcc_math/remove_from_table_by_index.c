#include <string.h>
#include "pcc_math.h"
#include <AsBrStr.h>

signed short remove_from_table_by_index(data_table *table, signed short idx)
{
    int err = 0;
    ordered_pair *d = table->data;
    unsigned int len;
    ordered_pair *start;

    if(table->current_length > 0)
    {
        if(idx < table->current_length && idx > 0 && idx < table->max_length)
        {
            /* move table entries from idx to end one entry up */
            start = &d[idx];
            len = (unsigned int)(table->max_length - idx - 1) * sizeof(ordered_pair);
            if(len != 0)
                brsmemmove((UDINT)(start), (UDINT)(start+1), (UDINT)len);

            --(table->current_length);
        }
        else
            err = 1;
    }
    else
        err = 1;

    return err;
}


