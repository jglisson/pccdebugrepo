#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  writefun.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\writefun.c_v  $
 *
 * 2008/12/01 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.writeFun to toggle debug information in this file
 *
 *    Rev 1.9   Sep 15 2008 13:22:02   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.8   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.7   May 09 2008 15:47:22   FMK
 * Removed HMI.h from include section. This should
 * only be in interface task.
 *
 *    Rev 1.6   May 06 2008 13:34:48   YZS
 * Fixed a typo.
 *
 *
 *    Rev 1.5   May 06 2008 13:20:08   YZS
 * Fixed response data errors.
 *
 *
 *    Rev 1.4   Apr 04 2008 11:18:04   vidya
 * Changed cs->temp_recipe PMEM.RECIPE->
 *
 *    Rev 1.3   Apr 02 2008 15:06:08   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.2   Mar 06 2008 10:08:08   Barry
 * Commentd out unused code in wv_current_mode
 *
 *    Rev 1.1   Feb 29 2008 14:58:58   FMK
 * Removed printer from serial ports. So removed
 * references for remote tags.
 *
 *    Rev 1.0   Feb 11 2008 11:04:56   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <math.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "vars.h"
#include <plc.h>
#include "relaydef.h"
#include "register.h"
#include "remote.h"
#include "signal_pcc.h"
#include "libbgdbg.h"
#include "recipe.h"
#include "hmi.h"

#define VALID_PASSWORD     2
#define INVALID_PASSWORD   4
#define RESET_TO_MIN_SECURITY  1

#define SYS_SHUTDOWN                0x80000000

#define LIW_DEMO_TESTWT 2000000
#define LIW_DEMO_TEST_WT 5.0
#define LIW_DEMO_ZEROWT 1000000
#define LIW_DEMO_LOAD_WT_ON 10.0
#define LIW_DEMO_LOAD_WT_OFF 0.0
#define LIW_DEMO_ALARM_WT 7.0
#define LIW_DEMO_CRIT_WT 1.0
#define BWH_DEMO_TESTWT 5000000
#define BWH_DEMO_TEST_WT 5.0
#define BWH_DEMO_ZEROWT 3000000
#define BWH_DEMO_LOAD_WT_ON 30.0
#define BWH_DEMO_LOAD_WT_OFF 0.1
#define BWH_DEMO_ALARM_WT 7.0
#define BWH_DEMO_CRIT_WT 1.0

void if_run_system(point_data *pd);
void dummy_ho_loop(point_data *pd, int liw, unsigned char DemoMask);
void dummy_wth_loop(point_data *pd);
void dummy_liw_loop(point_data *pd, int i, unsigned char DemoMask);
void dummy_bwh_loop(point_data *pd);
void dummy_gate_loop(point_data *pd, int);
void dummy_mix_loop(point_data *pd, int);
extern void clear_all_inven_wt(point_data *pd);
extern void clear_all_shift_wt(point_data *);
extern void restore_recipe(config_super *cs_, stored_recipe *rec_, recipe_struct *to, int from);
//extern int validate_recipe(config_super *cs_, config_loop_ptr *cnf_LOOP, shared_loop_ptr *shr_LOOP, recipe_struct *r, int *validation_errors);
//extern int calc_recipe(config_super *cs_, config_loop_ptr *cnf_LOOP, global_struct *g_, recipe_struct *r_);
extern void store_recipe(config_super *cs_, stored_recipe *rec_,int to,recipe_struct *from);
void manual_mode(void);
extern int validate_recipe(recipe_struct *r, int *validation_errors);
int rem_calc_recipe(point_data *pd, recipe_struct *r_);
extern int calc_recipe(recipe_struct *r);
void rem_copy_recipe(point_data *pd, recipe_struct *, recipe_struct *);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

void clear_all_gate_cal(point_data *pd);
void clear_all_alarms(point_data *pd);

/****************************************************************************
 *                                                                          *
 * Function: wv_special_devices
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_special_devices(fic v, point_data * pd)
{
   int resin_num, recipe_num, resin_offset, recipe_offset, i;
   double *inven;
   stored_recipe *rec;
   config_super *cs = &cfg_super;
   char *recipe_name;

   switch (pd->dev_type)
   {
      case TYPE_RESIN_NAME:
         resin_num = (pd->reg - 1) / REGS_PER_STRING;
         resin_offset = ((pd->reg - 1) % REGS_PER_STRING) * 4;
         for (i = 0; i < 4; i++)
         {
            if (resin_offset + i < RESIN_LENGTH)
               cs->resin_table[resin_num].resin_desc[resin_offset + i] = v.c[i];
            else
               v.c[i] = 0;
         }
         break;
      case TYPE_RESIN_DATA:
         resin_num = (pd->reg - 1) / REGS_PER_STRING;
         resin_offset = ((pd->reg - 1) % REGS_PER_STRING);
         switch (resin_offset)
         {
            case R2_RESIN_DENSITY:
               cs->resin_table[resin_num].density = v.f;
               break;
            case R2_RESIN_INVEN_WT:
               PMEM.resin_inven[resin_num] = 0.0;
               break;
         }
         break;
      case TYPE_RECIPE_NAME:
         recipe_num = (pd->reg / REGS_PER_STRING);
         recipe_offset = (((pd->reg ) % REGS_PER_STRING)-1) * 4;
         if(cs->enable_product_code)
         {
            rec = pd->rec + (recipe_num * cs->wordsperrecipe);
            rec +=  cs->recipe_inven_offset + 2;
            recipe_name = (char *)rec;
            for (i = 0; i < 4; i++)
            {
               if (recipe_offset + i < 15)
                  *(recipe_name + recipe_offset + i) = v.c[i];
            }
         }

         break;

      case TYPE_RECIPE_DATA:
         recipe_num = (pd->reg / REGS_PER_STRING);
         if(cs->enable_product_code)
         {
            rec = pd->rec + (recipe_num * cs->wordsperrecipe);
            rec +=  cs->recipe_inven_offset;
            inven = (double *)rec;
            *inven = 0.0;
         }
         break;

      case TYPE_SYSTEM:
         break;

      default:
         break;
   }

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_current_recipe_inven
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_current_recipe_inven(fic * v, point_data * pd)
{
   int retcode = OK;

   config_super *cs = &cfg_super;
   stored_recipe *rec;
   double *inven;

   if(cs->enable_product_code)
   {
      rec = pd->rec + (shr_global.current_recipe_num * cs->wordsperrecipe);
      rec +=  cs->recipe_inven_offset;
      inven = (double *)rec;
      *inven = 0.0;
   }

   return (retcode);
}

/****************************************************************************
 *                                                                          *
 * Function: wv_current_recipe_inven
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_set_manual(fic * v, point_data * pd)
{
   int j;
   int shift;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:

         if (BLN_OPER_MODE == MAN_MODE)
         {
            shift = MANUAL_GATE_A;
            for (j=0; j < cfg_super.total_gates; j++)
            {
               if ( j < cfg_super.total_gates)
                  hmi_pg005_controls[j]  = (v->i & shift) ? 1 : 0;
               shift<<=1;
            }

            hmi_pg005_controls[12] = (v->i & MANUAL_MIXER_MOTOR) ? 1 : 0;
            hmi_pg005_controls[13] = (v->i & MANUAL_MIXER_GATE) ? 1 : 0;
            hmi_pg005_controls[14] = (v->i & MANUAL_BWH) ? 1 : 0;
            hmi_pg005_controls[15] = (v->i & MANUAL_ALARM_OUT) ? 1 : 0;
            hmi_pg005_controls[16] = (v->i & MANUAL_ACTUATE) ? 1 : 0;
         }
         break;

      default:
         break;
   }

   return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_next_recipe_inven
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_next_recipe_inven(fic * v, point_data * pd)
{
   int retcode = OK;
   config_super *cs = &cfg_super;
   stored_recipe *rec;
   double *inven;

   if(cs->enable_product_code)
   {
      rec = pd->rec + (cs->current_temp_recipe_num * cs->wordsperrecipe);
      rec +=  cs->recipe_inven_offset;
      inven = (double *)rec;
      *inven = 0.0;
   }

   return (retcode);
}

/************************************************************************/
/* wv_temp_set_parts()                                                  */
/************************************************************************/
int wv_temp_set_parts(fic *v,point_data *pd)
{
    config_super *cs = &cfg_super;
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    int hop = pd->hop;
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
            if ((cs->recipe_entry_mode == PERCENT) && (v->f > 100.0))
            	v->f = 100.0;
            r->parts.s[pd->hop] = v->f;
            pd->recipe_changed = TRUE;
				remote_recipe_write_flag = TRUE;
			   remote_OneTimeAuto_write_flag = TRUE;
            break;

        case TYPE_LIW:
            if (loop == EXT_LOOP)
            {
            	r->parts.ext = v->f;
               pd->recipe_changed = TRUE;
            }
            else if (hop == REFEED)
            {
             	r->parts.s[REFEED] = v->f;
                r->max_scrap_parts = v->f;
                pd->recipe_changed = TRUE;
            }
            else
              return(INVALID_POINT);
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}

/************************************************************************/
/* wv_temp_set_wtp()                                                    */
/************************************************************************/
int wv_temp_set_wtp(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;
 
    if (r->total_wtp != v->f)
    {
       r->total_wtp = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;
       if_run_system(pd);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_spd()                                                         */
/************************************************************************/
int wv_set_spd(fic *v, point_data *pd)
{
	config_super *cs = &cfg_super;

	int loop = pd->loop;
	float accel_decel;

   /* if (EXT_HO_OPER_MODE != MAN_MODE)
      return(READ_ONLY); */

	switch(pd->dev_type)
	{
		case TYPE_LIW:
			if (cs->wtp_mode != CONTROL)
				return(INVALID_POINT);
			switch (loop)
			{
				case EXT_LOOP:
					if (cfgEXT.spd_factor > 0.001)
					{
						if (v->f < shrEXT.act_spd * cfgEXT.spd_factor)
							accel_decel = cfgEXT.decel;
						else
							accel_decel = cfgEXT.accel;
                 
					
						shr_global.liw_spd_rpm[0] = v->f;
						shr_global.liw_spd[0] = v->f / cfgEXT.spd_factor;
						//Signal.ProcessExtManChange = (EXT_HO_OPER_MODE == MAN_MODE);
					}
					break;

				case GRAVIFLUFF_FDR_LOOP:
					if (cs->scrap && (cs->refeed == GRAVIFLUFF))
					{
						if (v->f < shrGFLUFF.act_spd)
							accel_decel = cfgGFLUFF.decel;
						else
							accel_decel = cfgGFLUFF.accel;
						
						pd->set_liw_spd(GRAVIFLUFF_FDR_LOOP, v->f, accel_decel);
					}
					else
						return(INVALID_POINT);
					break;

				default:
					return(INVALID_POINT);
			}
			break;

		case TYPE_HO:
			if (cs->ltp_mode != CONTROL)
				return(INVALID_POINT);

			if (cfgHO.spd_factor[shrHO.GearNum] > 0.001)
			{
				if (v->f < shrHO.act_spd * cfgHO.spd_factor[shrHO.GearNum])
					accel_decel = cfgHO.decel;
				else
					accel_decel = cfgHO.accel;
			
				shr_global.ho_spd_rpm = v->f; 
			   shr_global.ho_spd = v->f / cfgHO.spd_factor[shrHO.GearNum];		
				//Signal.ProcessExtManChange = (EXT_HO_OPER_MODE == MAN_MODE);
			}
			break;

		default:
			return(INVALID_POINT);
	}
	shr_global.man_valid = TRUE;           /* not using ratio mode         */

	return(OK);
}

/************************************************************************/
/* wv_set_ratio_spd()                                                   */
/************************************************************************/
int wv_set_ratio_spd(fic *v,point_data *pd)
{
    config_super *cs = &cfg_super;

    int loop = pd->loop;
    float spd, accel_decel = 0.0;

    if ((cs->wtp_mode != CONTROL) && (cs->ltp_mode != CONTROL))
       return(INVALID_POINT);

    if (EXT_HO_OPER_MODE != MAN_MODE)
       return(READ_ONLY);

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (cs->wtp_mode != CONTROL)
               return(INVALID_POINT);

            shr_global.liw_spd[loop] = v->f;
            switch (loop)
            {
              case EXT_LOOP:
                 spd = (shr_global.man_ratio * v->f / 100.0);
                 if (spd < shrEXT.act_spd)
                    accel_decel = cfgEXT.decel;
                 else
                    accel_decel = cfgEXT.accel;

                 pd->set_liw_spd(EXT_LOOP, spd, accel_decel);
                 if (cs->scrap && (cs->refeed == GRAVIFLUFF))
                 {
                   spd = spd * shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP] / 100.0;
                   if (spd < shrGFLUFF.act_spd)
                      accel_decel = cfgGFLUFF.decel;
                   else
                      accel_decel = cfgGFLUFF.accel;
                   pd->set_liw_spd(GRAVIFLUFF_FDR_LOOP, spd, accel_decel);
                 }
                 break;

              case GRAVIFLUFF_FDR_LOOP:
                 if (cs->scrap && (cs->refeed == GRAVIFLUFF))
                 {
                   spd = shr_global.man_ratio * shr_global.liw_spd[EXT_LOOP] * v->f / 10000.0;
                   if (spd < shrGFLUFF.act_spd)
                     accel_decel = cfgGFLUFF.decel;
                   else
                     accel_decel = cfgGFLUFF.accel;
                   pd->set_liw_spd(GRAVIFLUFF_FDR_LOOP, spd, accel_decel);
                 }
                 else
                   return(INVALID_POINT);
                 break;

              default:
                 return(INVALID_POINT);
            }
            break;

        case TYPE_HO:
            if (cs->ltp_mode != CONTROL)
              return(INVALID_POINT);
            if (pd->loop != HO_LOOP)
              return(INVALID_POINT);
            shr_global.ho_spd = v->f;
            if (cs->ho_in_ratio)
              spd = shr_global.man_ratio * shr_global.ho_spd;
            else
              spd = shr_global.ho_spd;
            if (spd < shrHO.act_spd)
               accel_decel = cfgHO.decel;
            else
               accel_decel = cfgHO.accel;

            pd->set_ho_spd(spd,accel_decel);
            break;

        case TYPE_SYSTEM:
            shr_global.man_ratio = v->f;

            if ((cs->ltp_mode == CONTROL) && cs->ho_in_ratio)
            {
               (pd->set_ho_spd)((float)(shr_global.man_ratio * shr_global.ho_spd / 100.0), pd->HO_ad);
            }

            if (EXT_HO_OPER_MODE == MAN_MODE && (cs->wtp_mode == CONTROL))
            {
               (pd->set_liw_spd)(EXT_LOOP,(float)(shr_global.man_ratio * shr_global.liw_spd[EXT_LOOP] / 100.0), pd->LIW_ad[EXT_LOOP]);
            }

            if (cs->scrap && (cs->refeed == GRAVIFLUFF))
            {
               if (EXT_HO_OPER_MODE == MAN_MODE && (cs->wtp_mode == CONTROL))
               {
                  spd = (shr_global.man_ratio * shr_global.liw_spd[EXT_LOOP] * shr_global.liw_spd[GRAVIFLUFF_FDR_LOOP]/ 10000.0);
                  (pd->set_liw_spd)(GRAVIFLUFF_FDR_LOOP,spd,pd->LIW_ad[GRAVIFLUFF_FDR_LOOP]);
               }
            }
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/****************************************************************************/
/* wv_std_dev()                                                           */
/****************************************************************************/
int wv_std_dev(fic *v,point_data *pd)
{
    int loop = pd->loop;

    switch (pd->dev_type)
    {
        case TYPE_GATE:
            shrBWH.batch_stat[loop].gate_avg_wt_deviation = 0;
            break;
        default:
            return INVALID_POINT;
    }
    return OK;
}

/************************************************************************/
/* wv_inven_wt()                                                        */
/************************************************************************/
int wv_inven_wt(fic *v,point_data *pd)
{
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
            shrGATE[loop].last_inven_wt = PMEM.Gate[loop].InvTotal;
            PMEM.Gate[loop].InvTotal = 0.0;
            break;

        case TYPE_SYSTEM:
            clear_all_inven_wt(pd);
            break;

        case TYPE_LIW:
            shrEXT.last_inven_wt = PMEM.EXT.InvTotal;
            PMEM.EXT.InvTotal = 0.0;
            break;

        default:
            return(INVALID_POINT);
     }

    return(OK);
}

/************************************************************************/
/* wv_shift_wt()                                                        */
/************************************************************************/
int wv_shift_wt(fic *v,point_data *pd)
{
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
            shrGATE[loop].last_shift_wt = PMEM.Gate[loop].ShftTotal;
            PMEM.Gate[loop].ShftTotal = 0.0;
            break;

        case TYPE_SYSTEM:
            clear_all_shift_wt(pd);
            break;

        case TYPE_LIW:
            shrEXT.last_shift_wt = PMEM.EXT.ShftTotal;
            PMEM.EXT.ShftTotal = 0.0;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}

/************************************************************************/
/* wv_inven_len()                                                       */
/************************************************************************/
int wv_inven_len(fic *v,point_data *pd)
{
    PMEM.HO.InvTotal = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_shift_len()                                                       */
/************************************************************************/
int wv_shift_len(fic *v,point_data *pd)
{
    PMEM.HO.ShftTotal = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_inven_area()                                                       */
/************************************************************************/
int wv_inven_area(fic *v,point_data *pd)
{
    PMEM.HO.InvArea = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_shift_area()                                                       */
/************************************************************************/
int wv_shift_area(fic *v,point_data *pd)
{
    if (pd->dev_type != TYPE_HO)
      return(INVALID_POINT);

    PMEM.HO.ShftArea = 0.0;

    return(OK);
}


/************************************************************************/
/* wv_temp_man_start_spd()                                              */
/************************************************************************/
int wv_temp_man_start_spd(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (loop == GRAVIFLUFF_FDR_LOOP)
            {
            	r->refeed_man_spd = v->f;
                pd->recipe_changed = TRUE;
            }
            else
            {
            	r->ext_man_spd = v->f;
                pd->recipe_changed = TRUE;
            }
            break;

        case TYPE_HO:
            if (loop == HO_LOOP)
            {
            	r->ho_man_spd = v->f;
                pd->recipe_changed = TRUE;
            }
            else
              return(INVALID_POINT);
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}

/************************************************************************/
/* wv_temp_set_density()                                                */
/************************************************************************/
int wv_temp_set_density(fic *v,point_data *pd)
{
    //config_super *cs = &cfg_super;
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    int loop = pd->loop;

    r->density.s[loop] = v->f;
    cfg_super.resin_table[r->resin_recipe_num[loop]].density =  r->density.s[loop] ;
    
    pd->recipe_changed = TRUE;

    return(OK);
}

/************************************************************************/
/************************************************************************/
int wv_alarm(fic *v,point_data *pd)
{

    int i;
    config_super *cs = &cfg_super;
    int loop = pd->loop;


    switch(pd->dev_type)
    {
        case TYPE_BWH:
            shrBWH.alarm_latch_bits = shrBWH.alarm_bits;
            break;

        case TYPE_GATE:
            shrGATE[loop].alarm_latch_bits = shrGATE[loop].alarm_bits;
            break;

        case TYPE_MIX:
            shrMIXER.alarm_latch_bits = shrMIXER.alarm_bits;
            break;

        case TYPE_SYSTEM:
            shrBWH.alarm_latch_bits = shrBWH.alarm_bits;

            for (i=0; i<cs->total_gates; i++)
                shrGATE[i].alarm_latch_bits = shrGATE[i].alarm_bits;

            shrHO.alarm_latch_bits = shrHO.alarm_bits;
            shrSHO.alarm_latch_bits = shrSHO.alarm_bits;
            shrMIXER.alarm_latch_bits = shrMIXER.alarm_bits;
            shrEXT.alarm_latch_bits = shrEXT.alarm_bits;
            shrWTH.alarm_latch_bits = shrWTH.alarm_bits;
            shrGFLUFF.alarm_latch_bits = shrGFLUFF.alarm_bits;
            shrGFLUFF.alarm_latch_bits = shrGFLUFF.alarm_bits;
            break;

        case TYPE_LIW:
            shrEXT.alarm_latch_bits = shrEXT.alarm_bits;
            break;

        case TYPE_HO:
            shrHO.alarm_latch_bits = shrHO.alarm_bits;
            break;

        case TYPE_WTH:
            shrWTH.alarm_latch_bits = shrWTH.alarm_bits;
            break;

        default:
            return(INVALID_POINT);
    }
    shr_global.alarm_latch_bits = 0;
    shr_global.alarm_bits = 0;
    /*_os_send(shr_global.super_pid, ALARM_SIGNAL);*/

    return(OK);

}

/************************************************************************/
/* wv_read_only()                                                       */
/************************************************************************/
int wv_read_only(fic *v,point_data *pd)
{
    return(READ_ONLY);
}

/************************************************************************/
/* wv_accel()                                                           */
/************************************************************************/
int wv_accel(fic *v,point_data *pd)
{
	config_super *cs = &cfg_super;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.accel = v->f;
            break;

        case TYPE_HO:
            cfgHO.accel = v->f;
            break;

        case TYPE_SYSTEM:
            cs->accel = v->f;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}

/************************************************************************/
/* wv_decel()                                                           */
/************************************************************************/
int wv_decel(fic *v,point_data *pd)
{
	config_super *cs = &cfg_super;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.decel = v->f;
            break;

        case TYPE_HO:
            cfgHO.decel = v->f;
            break;

        case TYPE_SYSTEM:
            cs->decel = v->f;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_ltp()                                                         */
/* wv_temp_set_ltp()                                                    */
/************************************************************************/
int wv_temp_set_ltp(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    if (r->ltp != v->f)
    {
       r->ltp = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;       
       if_run_system(pd);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_thk()                                                         */
/* wv_temp_set_thk()                                                    */
/************************************************************************/
int wv_temp_set_thk(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    if (r->thk != v->f)
    {
       r->thk = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;
       if_run_system(pd);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_width()                                                       */
/* wv_temp_set_width()                                                  */
/************************************************************************/
int wv_temp_set_width(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    
    if (r->width != v->f)
    {
       r->width = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;
       if_run_system(pd);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_od()                                                          */
/* wv_temp_set_od()                                                     */
/************************************************************************/
int wv_temp_set_od(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    r->od = v->f;
    pd->recipe_changed = TRUE;

    return(OK);
}

/************************************************************************/
/* wv_set_id()                                                          */
/* wv_temp_set_id()                                                     */
/************************************************************************/
int wv_temp_set_id(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    
    r->id = v->f;
    pd->recipe_changed = TRUE;

    return(OK);
}

/************************************************************************/
/* wv_stretch_factor()                                                  */
/* wv_temp_stretch_factor()                                             */
/************************************************************************/
int wv_temp_stretch_factor(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    r->stretch_factor = v->f;
    pd->recipe_changed = TRUE;

    return(OK);
}

/************************************************************************/
/* wv_temp_set_wpl()                                                    */
/************************************************************************/
int wv_temp_set_wpl(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;
    if (r->wpl != v->f)
    {
       r->wpl = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;
       if_run_system(pd);
    }

    return(OK);
}

/*************************************************************/
/* wv_mix_time()                                             */
/*************************************************************/
int wv_mixer_low_level_time(fic *v, point_data *pd)
{
    cfgMIXER.mixer_low_level_time = v->f;

    return(OK);
}
/*************************************************************/
/* wv_mix_time()                                             */
/*************************************************************/
int wv_mixer_dump_cycle_time(fic *v, point_data *pd)
{
    cfgMIXER.dump_cycle_time = v->f;

    return(OK);
}
/*************************************************************/
/* wv_mix_time()                                             */
/*************************************************************/
int wv_max_dump_time(fic *v, point_data *pd)
{
    cfgMIXER.max_dump_time = v->f;

    return(OK);
}
/*************************************************************/
/* wv_mix_time()                                             */
/*************************************************************/
int wv_mix_time(fic *v, point_data *pd)
{
    cfgMIXER.mix_time = v->f *FTICKSPERSEC;

    return(OK);
}
/*************************************************************/
/* wv_gate_delay_time                                        */
/*************************************************************/
int wv_gate_delay_time(fic *v, point_data *pd)
{
    /* in mS GTF */
    cfgMIXER.gate_close_delay = v->f;

    return(OK);
}

/*************************************************************/
/* wv_temp_mix_time()                                        */
/*************************************************************/
int wv_temp_mix_time(fic *v, point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    r->mix_time = v->f * FTICKSPERSEC;
    pd->recipe_changed = TRUE;

    return(OK);
}
/*************************************************************/
/* wv_step_size()                                            */
/*************************************************************/
int wv_step_size(fic *v, point_data *pd)
{
	config_super *cs = &cfg_super;

	cs->step_size = v->f;

    return(OK);
}

/*************************************************************/
/* wv_target_weight()                                            */
/*************************************************************/
int wv_target_weight(fic *v, point_data *pd)
{
    shr_global.target_grams = v->f;
    return(OK);
}

/*************************************************************/
/* wv_target_tolerence()                                            */
/*************************************************************/
int wv_target_tolerence(fic *v, point_data *pd)
{
    shr_global.target_tol = v->f;
    return(OK);
}

/*************************************************************/
/* wv_target_tolerence()                                            */
/*************************************************************/
int wv_target_time(fic *v, point_data *pd)
{
    shr_global.target_time = v->f;

    return(OK);
}

/*************************************************************/
/* wv_temp_step_size()                                       */
/*************************************************************/
int wv_temp_step_size(fic *v, point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    r->step_size = v->f;
    pd->recipe_changed = TRUE;

    return(OK);
}

/************************************************************************/
/* wv_temp_set_wpa()                                                    */
/************************************************************************/
int wv_temp_set_wpa(fic *v,point_data *pd)
{
    recipe_struct *r = &BATCH_RECIPE_EDIT;

    if (r->wpa != v->f)
    {
       r->wpa = v->f;
       pd->recipe_changed = TRUE;
       remote_recipe_write_flag = TRUE;
       if_run_system(pd);
    }

    return(OK);
}

/************************************************************************/
/* wv_set_mode()                                                        */
/************************************************************************/
int wv_set_mode(fic *v,point_data *pd)
{
  register int loop;
  char togle = FALSE;

  loop = pd->loop;

  switch(pd->dev_type)
  {
    case TYPE_SYSTEM:
        if ((shr_global.alarm_bits & SYS_SHUTDOWN) && (v->i & (R_BLN_AUTO|R_EXT_AUTO|R_EXT_MANUAL)))
          return(OK);

        switch(v->i & R_BLN_BITS)
        {
          case R_BLN_AUTO:
            BLN_OPER_MODE &= ~MAN_MODE;
			   remote_auto_write_flag = TRUE;
			   break;

            /* pause pending stops blender after current batch is complete */
          case R_BLN_PAUSE_PENDING:
            BLN_OPER_MODE &= ~MAN_MODE;
            shr_global.pause_pending = TRUE;
			   remote_pause_batch_write_flag = TRUE;	
            break;

          case R_BLN_PAUSE:
            BLN_OPER_MODE &= ~MAN_MODE;
            remote_pause_write_flag = TRUE;
            break;

           case R_BLN_MANUAL:
             if (BLN_OPER_MODE == PAUSE_MODE)
             {
                remote_manual_write_flag = TRUE;
                BLN_OPER_MODE = MAN_MODE;
             }
             break;

           default:
             break;
        }
        switch(v->i & R_EXT_BITS)
        {
          case R_EXT_AUTO:
            if (shr_global.new_recipe == TRUE)
               shr_global.remote_run_recipe_changed = TRUE;
            if (shr_global.new_recipe || (EXT_HO_OPER_MODE != AUTO_MODE) || togle)
            {
               /*_os_send(shr_global.super_pid, EXT_AUTO_SIGNAL);*/
               shr_global.new_recipe = FALSE;
               togle = FALSE;
			      remote_ext_auto_write_flag = TRUE;
            }
            break;

          case R_EXT_MANUAL:
            /*_os_send(shr_global.super_pid, EXT_MANUAL_SIGNAL);*/
				remote_ext_manual_write_flag = TRUE;
            break;

          case R_EXT_PAUSE:
            /*_os_send(shr_global.super_pid, EXT_PAUSE_SIGNAL);*/
				remote_ext_pause_write_flag = TRUE;
            break;

          default:
            break;
        }

        if (v->i & R_AUX_CLR_ALARMS)
        {
           clear_all_alarms(pd);
        }

        if (v->i & R_AUX_CLR_INVEN)
        {
           clear_all_inven_wt(pd);
        }

        if (v->i & R_AUX_CLR_SHIFT)
        {
           clear_all_shift_wt(pd);
        }

        if (v->i & R_RATE_SPEED)
        {
           clear_all_gate_cal(pd);
        }
        if (v->i & R_PURGE)
        {
           shr_global.purge_pending = TRUE;
        }
      break;

    case TYPE_GATE:
      if (v->i & R_AUX_CLR_ALARMS)
         shrGATE[loop].alarm_latch_bits = shrGATE[loop].alarm_bits;

      if (v->i & R_AUX_CLR_INVEN)
         PMEM.Gate[loop].InvTotal = 0.0;

      if (v->i & R_AUX_CLR_SHIFT)
         PMEM.Gate[loop].ShftTotal = 0.0;
         
      if (v->i & R_RATE_SPEED)
         shrGATE[loop].clear_gate_cal = TRUE;
 
    case TYPE_LIW:
      if (v->i & R_AUX_CLR_ALARMS)
         shrEXT.alarm_latch_bits = shrEXT.alarm_bits;

      if (v->i & R_AUX_CLR_INVEN)
         PMEM.EXT.InvTotal = 0.0;

      if (v->i & R_AUX_CLR_SHIFT)
         PMEM.EXT.ShftTotal = 0.0;

      if (v->i & R_RATE_SPEED)
         cfgEXT.rs_num = 0;
      break;

    case TYPE_HO:
      if (v->i & R_AUX_CLR_ALARMS)
         shrHO.alarm_latch_bits = shrHO.alarm_bits;

      if (v->i & R_AUX_CLR_INVEN)
      {
         PMEM.HO.InvTotal = 0.0;
         PMEM.HO.InvArea = 0.0;
      }

      if (v->i & R_AUX_CLR_SHIFT)
      {
         PMEM.HO.ShftArea = 0.0;
         PMEM.HO.ShftTotal = 0.0;
      }

      if (v->i & R_RATE_SPEED)
         cfgHO.rs_num = 0;
      break;

    case TYPE_FLF:
      if (v->i & R_AUX_CLR_ALARMS)
         shrGFLUFF.alarm_latch_bits = shrGFLUFF.alarm_bits;
      break;

    case TYPE_WTH:
      if (v->i & R_AUX_CLR_ALARMS)
         shrWTH.alarm_latch_bits = shrWTH.alarm_bits;
      break;

    case TYPE_BWH:
      if (v->i & R_AUX_CLR_ALARMS)
         shrBWH.alarm_latch_bits = shrBWH.alarm_bits;
      break;

    case TYPE_MIX:
      if (v->i & R_AUX_CLR_ALARMS)
         shrMIXER.alarm_latch_bits = shrMIXER.alarm_bits;
      break;

    default:
      break;

      pd->mode_button_stick[loop] = TRUE;
      pd->mode_button_value[loop] = v->i;
   }
   return(OK);
}

/************************************************************************/
/************************************************************************/
void clear_all_alarms(point_data *pd)
{
    int i;
    config_super *cs = &cfg_super;

    shrBWH.alarm_latch_bits = shrBWH.alarm_bits;

    for (i=0; i<cs->total_gates; i++)
        shrGATE[i].alarm_latch_bits = shrGATE[i].alarm_bits;

    shrHO.alarm_latch_bits = shrHO.alarm_bits;
    shrMIXER.alarm_latch_bits = shrMIXER.alarm_bits;
    shrEXT.alarm_latch_bits = shrEXT.alarm_bits;
    shrWTH.alarm_latch_bits = shrWTH.alarm_bits;
    shrGFLUFF.alarm_latch_bits = shrGFLUFF.alarm_bits;
    shrGFLUFF.alarm_latch_bits = shrGFLUFF.alarm_bits;
}

/************************************************************************/
/************************************************************************/
void clear_all_inven_wt(point_data *pd)
{
    int i;
    config_super *cs = &cfg_super;


    for(i=0; i<cs->total_gates;++i)
    {
      shrGATE[i].last_inven_wt = PMEM.Gate[i].InvTotal;
      PMEM.Gate[i].InvTotal = 0.0;
    }

    shrHO.last_inven_len = PMEM.HO.InvTotal;
    PMEM.HO.InvTotal = 0.0;
    shrEXT.last_inven_wt = PMEM.EXT.InvTotal;
    PMEM.EXT.InvTotal = 0.0;
    shr_global.last_sys_inven_wt = shr_global.sys_inven_wt;
    shr_global.sys_inven_wt = 0.0;

  /*_os_send(shr_global.super_pid, VARS_SIGNAL);*/
}

/************************************************************************/
/************************************************************************/
void clear_all_shift_wt(point_data *pd)
{
  int i;
  config_super *cs = &cfg_super;
 
  for(i=0; i<cs->total_gates;++i)
  {
     shrGATE[i].last_shift_wt = PMEM.Gate[i].ShftTotal;
     PMEM.Gate[i].ShftTotal = 0.0;
  }
  shrHO.last_shift_len = PMEM.HO.ShftTotal;
  PMEM.HO.ShftTotal = 0.0;
  shrEXT.last_shift_wt = PMEM.EXT.ShftTotal;
  PMEM.EXT.ShftTotal = 0.0;
  shr_global.last_sys_shift_wt = shr_global.sys_shift_wt;
  shr_global.sys_shift_wt = 0.0;

  /*_os_send(shr_global.super_pid, VARS_SIGNAL);*/

}

/************************************************************************/
/************************************************************************/
void clear_all_gate_cal(point_data *pd)
{
   int i;

   shr_global.clear_gate_cal = TRUE; 
   for(i=0; i<cfg_super.total_gates; i++)
   {
      shrGATE[i].clear_gate_cal = TRUE;
   }
}

/****************************************************************************/
/* wv_batch_size()                                                          */
/****************************************************************************/
int wv_batch_size(fic *v,point_data *pd)
{
    //G2-347
    //if (cfg_super.units == METRIC)
    //   cfgBWH.set_batch_size = v->f * 2.2046;
    //else //G2-498 -> removed G2-347
    cfgBWH.set_batch_size = v->f;
    return(OK);
}
/****************************************************************************/
/* wv_batch_oversize()                                                          */
/****************************************************************************/
int wv_batch_oversize(fic *v,point_data *pd)
{
    cfgBWH.batch_oversize = v->f;
    return(OK);
}

/****************************************************************************/
/* wv_job_wt()                                                              */
/****************************************************************************/
int wv_job_wt(fic *v,point_data *pd)
{
	/*	config_super *cs = &cfg_super;	*/
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	if (cfg_super.units == METRIC)
	{
		r->job_wt = v->f * 2.2046;
		remote_recipe_write_flag = TRUE;
	}
	else
	{
		r->job_wt = v->f;
		remote_recipe_write_flag = TRUE;
	}
	return(OK);
}

/****************************************************************************/
/* wv_temp_job_wt()                                                         */
/****************************************************************************/
int wv_temp_job_wt(fic *v,point_data *pd)
{
/*		config_super *cs = &cfg_super;
	
		if (cfg_super.units == METRIC)
		    SYS_TEMP_JOB_WT = v->f * 2.2046;
			
		else
			SYS_TEMP_JOB_WT = v->f;
	
		config_super *cs = &cfg_super;	*/
	
	recipe_struct *r = &BATCH_RECIPE_EDIT;

	if (cfg_super.units == METRIC)
	{
		r->job_wt = v->f * 2.2046;
		remote_recipe_write_flag = TRUE;
	}
	else
	{
		r->job_wt = v->f;
		remote_recipe_write_flag = TRUE;	
	}
	return(OK);
}


/************************************************************************/
/* wv_next_recipe()                                                     */
/************************************************************************/
int wv_next_recipe(fic *v, point_data *pd)
{
    config_super *cs = &cfg_super;

    restore_recipe(cs,pd->rec,&BATCH_RECIPE_EDIT,v->i);
    cs->current_temp_recipe_num = (int)v->i;
    PMEM.recipe_num = cs->current_temp_recipe_num;
    pd->next_recipe_num = (int)v->i;
    pd->recipe_changed = TRUE;
    return(OK);
}


/****************************************************************************/
/* null_write_function()                                                    */
/****************************************************************************/
int null_write_function(fic *f, point_data *pd)
{
    return INVALID_POINT;
}

/************************************************************************/
/* int rem_validate_recipe()                                                */
/************************************************************************/
int rem_validate_recipe(point_data *pd, recipe_struct *r, int *validation_errors)
{
    int retval;

    retval = validate_recipe((recipe_struct *)r, validation_errors);

    return(retval);
}

/************************************************************************/
/*  Function:  rem_copy_recipe(pd,r)                                    */
/*                                                                      */
/* Description:  copies recipe change to hmi                            */
/*                                                                      */
/************************************************************************/
void rem_copy_recipe(point_data *pd, recipe_struct *from, recipe_struct *to)
{ 
   int i; 
   
   strncpy(to->product_code, from->product_code, 16);

   for (i=0;i<cfg_super.total_gates;i++)
   {
      to->parts.s[i]   = from->parts.s[i];
      to->density.s[i] = from->density.s[i];
   }
         
   to->parts.ext = from->parts.ext;
   to->parts.ext = from->parts.ext;
   to->parts_pct.ext = from->parts_pct.ext;
   to->parts.s[REFEED] = from->parts.s[REFEED];
   to->parts_pct.s[REFEED] = from->parts_pct.s[REFEED];
   to->resin_recipe_num[REFEED] = from->resin_recipe_num[REFEED];
   to->wpl = from->wpl;
   to->wpa = from->wpa;
   to->thk = from->thk;
   to->ltp = from->ltp;
   to->width = from->width;
   to->id = from->id;
   to->od = from->od;
   to->ea = from->ea;
   to->total_parts = from->total_parts;
   to->ho_man_spd = from->ho_man_spd;
   to->ext_man_spd = from->ext_man_spd;
   to->refeed_man_spd = from->refeed_man_spd;
   to->ho_gear_num = from->ho_gear_num;
   to->total_wtp = from->total_wtp;
   to->ext_wtp = from->ext_wtp;
   to->refeed_wtp = from->refeed_wtp;
   to->max_scrap_parts = from->max_scrap_parts;
   to->stretch_factor = from->stretch_factor;

   to->pg450_RecipeEntry[0] = from->pg450_RecipeEntry[0];
   to->pg450_RecipeEntry[1] = from->pg450_RecipeEntry[1];
   to->pg450_RecipeEntry[2] = from->pg450_RecipeEntry[2];
        
   to->pg454_SetRecipeEntry[0] = from->pg454_SetRecipeEntry[0];
   to->pg454_SetRecipeEntry[1] = from->pg454_SetRecipeEntry[1];
   to->pg454_SetRecipeEntry[2] = from->pg454_SetRecipeEntry[2];          
} 

/************************************************************************/
/*  Function:  rem_calc_recipe(pd,r)                                        */
/*                                                                      */
/* Description:  performs calculations on recipe entry to determine     */
/*                 pph's, etc.  .                                       */
/************************************************************************/
int rem_calc_recipe(point_data *pd, recipe_struct *r)
{
   int retval;

   retval = calc_recipe((recipe_struct *)r);

   return(retval);
}

/****************************************************************************
****************************************************************************/
int wv_max_speed_change(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.rs_maxchg = v->f/100;
         break;
      case TYPE_HO:
         cfgHO.rs_maxchg = v->f/100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_control_dead_band(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.dead_band = v->f/100;
         break;
      case TYPE_HO:
         cfgHO.dead_band = v->f/100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_max_error_before_updates(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.max_error = v->f/100;
         break;
      case TYPE_HO:
         cfgHO.max_error = v->f/100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_drive_high_speed_limit(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.alarm_high_spd = v->f;
         break;
      case TYPE_HO:
         cfgHO.alarm_high_spd = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_drive_low_speed_limit(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.alarm_low_spd = v->f;
         break;
      case TYPE_HO:
         cfgHO.alarm_low_spd = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_recipe_high_speed_alarm(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.warning_high_spd = v->f;
         break;
      case TYPE_HO:
         cfgHO.warning_high_spd = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_recipe_low_speed_alarm(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.warning_low_spd = v->f;
         break;
      case TYPE_HO:
         cfgHO.warning_low_spd = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_nip_circumference
 *                                                                          *
 ****************************************************************************/
int wv_nip_circumference(fic * v, point_data * pd)
{

   int retcode = OK;

   switch(pd->dev_type)
   {
      case TYPE_HO:
        if (pd->loop == HO_LOOP)
            cfgHO.nip_circumference = v->f;
       else
            cfgHO.nip_circumference = v->f;

         break;

      default:
          retcode = INVALID_POINT;
          break;
   }

   return retcode;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_pulses_per_rev
 *                                                                          *
 ****************************************************************************/
int wv_pulses_per_rev(fic * v, point_data * pd)
{
   int retcode = OK;

   switch(pd->dev_type)
   {

      case TYPE_HO:
        if (pd->loop == HO_LOOP)
            cfgHO.pulses_per_rev = v->f;
       else
            cfgSHO.pulses_per_rev = v->f;
         break;

      default:
          retcode = INVALID_POINT;
          break;
   }

   return retcode;

}
/* FLOAT FOR CONFIG*/

/****************************************************************************/
/****************************************************************************/
int wv_target_accuracy(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->target_accuracy = v->f/100.0;
         break;
       case TYPE_GATE:
         cfgGATE[pd->loop].out_of_spec_limit = v->f/100.0;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }
   return retcode;
}

/* INT FOR CONFIG */
/****************************************************************************/
/****************************************************************************/
int wv_frame_size(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;
   char frame;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
            //frame size is 0=4, 1=6, 2=8, 3=12 GTF       
            switch(v->i)
            {
                case 4:
                    frame = 0;
                    break;
                case 6:
                    frame = 1;
                    break;
                case 8:
                    frame = 2;
                    break;
                case 12:
                    frame = 3;
                    break;
                default:
                    frame = 0;
            }
      
         if (frame != cs->frame_size)
            shr_global.remote_system_restart = TRUE;
            
         cs->frame_size = frame;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_total_receivers(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if (v->i != cs->total_receivers)
            shr_global.remote_system_restart = TRUE;
         cs->total_receivers = (char)v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}

/****************************************************************************/
/****************************************************************************/
int wv_total_gates(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if ((char)v->i != cs->total_gates)
         {
            cs->total_gates = v->i;
            /*_os_send(shr_global.super_pid, REMOTE_CHANGE_NUM_GATES_SIGNAL);*/
            shr_global.remote_system_restart = TRUE;
         }
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_wtp_mode(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if ((char)v->i != cs->wtp_mode)
         {
            shr_global.remote_system_restart = TRUE;
            if ((cs->wtp_mode == UNUSED) && (v->i != UNUSED)) /* changing from unused */
            {
              cfgMIXER.material_request_source = MATERIAL_REQUEST_GRAVITROL;
              cfgEXT.load_wt_off = 0.0;
              cfgEXT.load_time = 0;
            }
            if ((cs->wtp_mode == UNUSED) && (cfgMIXER.material_request_source == MATERIAL_REQUEST_GRAVITROL))
              cfgMIXER.material_request_source = MATERIAL_REQUEST_MIXER_PROX;
         }
         switch (v->i)
         {
            case 0:
                cs->wtp_mode = UNUSED;
                break;
            case 1:
                cs->wtp_mode = MONITOR;
                break;
            case 2:
                cs->wtp_mode = CONTROL;
                break;
         }
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }
   cs->control_mode = (char)( ((cs->wtp_mode+1)*3) + (cs->ltp_mode+1) );
   return retcode;
}
/****************************************************************************/
/****************************************************************************/
int wv_ltp_mode(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if ((char)v->i != cs->ltp_mode)
            shr_global.remote_system_restart = TRUE;

            switch (v->i)
            {
               case 0:
                   cs->ltp_mode = UNUSED;
                   break;
               case 1:
                   cs->ltp_mode = MONITOR;
                   break;
               case 2:
                   cs->ltp_mode = CONTROL;
                   break;
            }

      break;
      default:
         retcode = INVALID_POINT;
         break;
   }
   cs->control_mode = (char)( ((cs->wtp_mode+1)*3) + (cs->ltp_mode+1) );
   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_refeed_mode(fic *v, point_data *pd)
{

   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if ((char)v->i != cs->refeed)
            shr_global.remote_system_restart = TRUE;
         cs->refeed = (char)v->i;
         cs->scrap = (cs->refeed != NONE); /* set scrap flag, which is used by some routines */
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_width_mode(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;


   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if ((char)v->i != cs->wth_mode)
         {
            shr_global.remote_system_restart = TRUE;
            if (v->i !=UNUSED)
            {
               cs->wth_type = KUNDIG;
               cs->width_port.number = 3;  /* default to port 3 */
            }
         }
         cs->wth_mode = (char)v->i;

         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_sltp_mode(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->sltp_mode = (char)v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_application(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->application = (char)v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_system_flags(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   unsigned int flags = 0;

   flags =  v->i;

   if(flags & FLAG_CONTROL_LOADING)
   {
      if ((cs->blend_control & FLAG_CONTROL_LOADING)==0)
            shr_global.remote_system_restart = TRUE;
      cs->blend_control = TRUE;
   }
   else
   {
      if ((cs->blend_control & FLAG_CONTROL_LOADING))
            shr_global.remote_system_restart = TRUE;
      cs->blend_control = FALSE;
   }

   if(flags & FLAG_REMOTE_SELF_LOADING)
      cs->blend_control_remote = TRUE;
   else
      cs->blend_control_remote = FALSE;

   /*if (flags & FLAG_PROFACE)
      _os_send(shr_global.proc_mon_pid, PROFACE_SIGNAL);*/

   if(flags & FLAG_REFEED)
      cs->refeed = TRUE;
   else
      cs->refeed = FALSE;

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_minimum_security_level(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->minimum_security_level = (unsigned char)v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_pe_needed_flags(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   unsigned int flags = 0;
   int i;

   flags = v->i;

   for(i=0; i< PE_TOTAL_ENTRIES; i++)
      if(flags & FLAG_PE_NEEDED(i))
         cs->pe_needed[i] = TRUE;
      else
         cs->pe_needed[i] = FALSE;
   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_supervisor_code_1(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->supervisor_code[0] = v->c[0];
   cs->supervisor_code[1] = v->c[1];
   cs->supervisor_code[2] = v->c[2];
   cs->supervisor_code[3] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_supervisor_code_2(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->supervisor_code[4] = v->c[0];
   cs->supervisor_code[5] = v->c[1];
   cs->supervisor_code[6] = v->c[2];
   cs->supervisor_code[7] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_maintenance_code_1(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->maintenance_code[0] = v->c[0];
   cs->maintenance_code[1] = v->c[1];
   cs->maintenance_code[2] = v->c[2];
   cs->maintenance_code[3] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_maintenance_code_2(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->maintenance_code[4] = v->c[0];
   cs->maintenance_code[5] = v->c[1];
   cs->maintenance_code[6] = v->c[2];
   cs->maintenance_code[7] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_operator_code_1(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->operator_code[0] = v->c[0];
   cs->operator_code[1] = v->c[1];
   cs->operator_code[2] = v->c[2];
   cs->operator_code[3] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_operator_code_2(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->operator_code[4] = v->c[0];
   cs->operator_code[5] = v->c[1];
   cs->operator_code[6] = v->c[2];
   cs->operator_code[7] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_units(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->units = (char)v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_wtp_unit
 *                                                                          *
 ****************************************************************************/
int wv_wtp_unit(fic * v, point_data * pd)
{
   int retcode = OK;
   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_wpl_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->wpl_unit.i = v->i;
#endif
   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_long_wpl_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->long_wpl_unit.i = v->i;

#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_wpa_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->wpa_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_long_wpa_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->long_wpa_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_t_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->t_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_long_t_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->long_t_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_od_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->od_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_long_od_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->long_od_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_id_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->id_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_long_id_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->long_id_unit.i = v->i;
#endif

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_width_unit(fic *v, point_data *pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->width_unit.i = v->i;
#endif

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_long_width_unit
 *                                                                          *
 ****************************************************************************/
int wv_long_width_unit(fic * v, point_data * pd)
{
   int retcode = OK;
#if 0
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->long_width_unit.i = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }
#endif
   return retcode;

}
/****************************************************************************/
/****************************************************************************/
int wv_report_start(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->report_start = v->i;

   return(OK);

}
/****************************************************************************/
/****************************************************************************/
int wv_report_interval(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->report_interval = v->i;

   return(OK);

}
/****************************************************************************/
/****************************************************************************/
int wv_communication_flags(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   unsigned int flags = 0;

   flags = v->i;

   if(flags & FLAG_PRINT_VERBOSE)
      cs->print_verbose = TRUE;
   else
      cs->print_verbose = FALSE;

   if(flags & FLAG_CLEAR_TOTALS_ON_PRINT)
      cs->clear_totals_on_print = TRUE;
   else
      cs->clear_totals_on_print = FALSE;

   if(flags & FLAG_LOG_ALARMS)
      cs->log_alarms = TRUE;
   else
      cs->log_alarms = FALSE;

   if(flags & FLAG_AUTO_REPORTS)
      cs->auto_reports = TRUE;
   else
      cs->auto_reports = FALSE;

   if(flags & FLAG_REPORT_CHANGE_RECIPE)
      cs->report_change_recipe = TRUE;
   else
      cs->report_change_recipe = FALSE;

   if(flags & FLAG_RETURN_SET_IF_ACT_0)
      cs->return_set_if_act_0 = TRUE;
   else
      cs->return_set_if_act_0 = FALSE;

   if(flags & FLAG_USE_PCC2_ADDRESS)
      cs->use_pcc2_address = TRUE;
   else
      cs->use_pcc2_address = FALSE;

   if(flags & FLAG_COMPATIBILITY_MODE)
      cs->compatibility_mode = TRUE;
   else
      cs->compatibility_mode = FALSE;

   if(flags & FLAG_AB_USE_CRC_CHKSUM)
      cs->ab_use_crc_chksum = TRUE;
   else
      cs->ab_use_crc_chksum = FALSE;

   if(flags & FLAG_AUTORUN_AFTER_RATE_CHANGED)
      cs->autorun_after_rate_changed = TRUE;
   else
      cs->autorun_after_rate_changed = FALSE;

   if(flags & FLAG_USING_32BITS_MODE)
      cs->using_32bit_mode_holding_reg = TRUE;
   else
      cs->using_32bit_mode_holding_reg = FALSE;
   if(flags & FLAG_REMOTE_BIAS_ENABLED)
      cs->remote_port.options |= 0x01;
   else
      cs->remote_port.options |= 0x00;
   if(flags & FLAG_REMOTE_MULTIDROP_ENABLED)
      cs->remote_port.multidrop = TRUE;
   else
      cs->remote_port.multidrop = FALSE;

   if(flags & FLAG_REMOTE_OP_BIAS_ENABLED)
      cs->remote_int_port.options |= 0x01;
   else
      cs->remote_int_port.options |= 0x00;
   if(flags & FLAG_REMOTE_OP_MULTIDROP_ENABLED)
      cs->remote_int_port.multidrop = TRUE;
   else
      cs->remote_int_port.multidrop = FALSE;

   if(flags & FLAG_GRAVILINK_USES_ETHERNET)
      cs->gravilink_uses_ethernet = TRUE;
   else
      cs->gravilink_uses_ethernet = FALSE;

   if(flags & FLAG_WEB_SERVER_ENABLED)
      cs->web_server_enabled = TRUE;
   else
      cs->web_server_enabled = FALSE;
   if(flags & FLAG_MBENET_ENABLED)
      cs->mbenet_enabled = TRUE;
   else
      cs->mbenet_enabled = FALSE;

   if(flags & FLAG_ABTCP_ENABLED)
      cs->abtcp_enabled = TRUE;
   else
      cs->abtcp_enabled = FALSE;

   if(flags & FLAG_PROFIBUS_ENABLED)
      cs->dual_port_comm_enabled = TRUE;
   else
      cs->dual_port_comm_enabled = FALSE;

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_user_interface_flags(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   unsigned int flags = 0;
   /*process_id p_id;*/

   flags = v->i;
   if(flags & FLAG_INTL_DATE_FORMAT)
      cs->intl_date_format = TRUE;
   else
      cs->intl_date_format = FALSE;

   if(flags & FLAG_SPD_IN_PERCENT)
   {
       cs->spd_in_percent = TRUE;

       cfgEXT.rs_num = 0;
       cfgGFLUFF.rs_num = 0;
       cfgGFLUFF.rs_num = 0;
       cfgHO.rs_num = 0;
       cfgSHO.rs_num = 0;

       cfgEXT.spd_factor = 100.0;
       cfgGFLUFF.spd_factor = 100.0;
       cfgHO.spd_factor[0] = 100.0;
       cfgSHO.spd_factor[0] = 100.0;
   }
   else
      cs->spd_in_percent = FALSE;

   if(flags & FLAG_HO_IN_RATIO)
      cs->ho_in_ratio = TRUE;
   else
      cs->ho_in_ratio = FALSE;
#if 0
   if(flags & FLAG_USE_ANSI_DISPLAY)
      cs->use_ANSI_display = TRUE;
   else
      cs->use_ANSI_display = FALSE;
#endif

   if (flags & FLAG_PRINT_SUMMARY_REPORT)
   {
       shr_global.print_report_option = PRINT_SUMMARY_REPORT;
      /*_os_fork((unsigned int)cs->sup_pr,3,"print","",1,0,&p_id,0,0);  */
   }

   if (flags & FLAG_PRINT_RECIPE_REPORT)
   {
       shr_global.print_report_option = PRINT_RECIPE_REPORT;
      /*_os_fork((unsigned int)cs->sup_pr,3,"print","",1,0,&p_id,0,0);  */
   }

   if (flags & FLAG_PRINT_ALARM_LOG_REPORT)
   {
       shr_global.print_report_option = PRINT_ALARM_REPORT;
      /*_os_fork((unsigned int)cs->sup_pr,3,"print","",1,0,&p_id,0,0);  */
   }

   if (flags & FLAG_PRINT_CONFIG_REPORT)
   {
       shr_global.print_report_option = PRINT_CONFIG_REPORT;
      /*_os_fork((unsigned int)cs->sup_pr,3,"print","",1,0,&p_id,0,0);  */
   }

   if (flags & FLAG_PRINT_RESIN_REPORT)
   {
       shr_global.print_report_option = PRINT_CONFIG_REPORT;
      /*_os_fork((unsigned int)cs->sup_pr,3,"print","",1,0,&p_id,0,0);  */
   }

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_set_alarm_relay_type(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->set_alarm_relay_type = v->i;
   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_1(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[0] = v->c[0];
   cs->aux_alarm_msg[1] = v->c[1];
   cs->aux_alarm_msg[2] = v->c[2];
   cs->aux_alarm_msg[3] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_2(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[4] = v->c[0];
   cs->aux_alarm_msg[5] = v->c[1];
   cs->aux_alarm_msg[6] = v->c[2];
   cs->aux_alarm_msg[7] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_3(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[8] = v->c[0];
   cs->aux_alarm_msg[9] = v->c[1];
   cs->aux_alarm_msg[10] = v->c[2];
   cs->aux_alarm_msg[11] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_4(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[12] = v->c[0];
   cs->aux_alarm_msg[13] = v->c[1];
   cs->aux_alarm_msg[14] = v->c[2];
   cs->aux_alarm_msg[15] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_5(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[16] = v->c[0];
   cs->aux_alarm_msg[17] = v->c[1];
   cs->aux_alarm_msg[18] = v->c[2];
   cs->aux_alarm_msg[19] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_6(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[20] = v->c[0];
   cs->aux_alarm_msg[21] = v->c[1];
   cs->aux_alarm_msg[22] = v->c[2];
   cs->aux_alarm_msg[23] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_7(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[24] = v->c[0];
   cs->aux_alarm_msg[25] = v->c[1];
   cs->aux_alarm_msg[26] = v->c[2];
   cs->aux_alarm_msg[27] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_8(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[28] = v->c[0];
   cs->aux_alarm_msg[29] = v->c[1];
   cs->aux_alarm_msg[30] = v->c[2];
   cs->aux_alarm_msg[31] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_aux_alarm_msg_9(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;

   cs->aux_alarm_msg[32] = v->c[0];
   cs->aux_alarm_msg[33] = v->c[1];
   cs->aux_alarm_msg[34] = v->c[2];
   cs->aux_alarm_msg[35] = v->c[3];

   return OK;

}
/****************************************************************************/
/****************************************************************************/
int wv_recipe_flags(fic *v, point_data *pd)
{
   config_super *cs = &cfg_super;
   unsigned int flags = 0;

   flags = v->i;

   if(flags & FLAG_JOB_NAME_USED)
      cs->job_name_used = TRUE;
   else
      cs->job_name_used = FALSE;

   if(flags & FLAG_CLEAR_REC_TOTAL)
      cs->clear_rec_total = TRUE;
   else
      cs->clear_rec_total = FALSE;

   if(flags & FLAG_ENABLE_RESIN_CODES)
      cs->enable_resin_codes = TRUE;
   else
      cs->enable_resin_codes = FALSE;

   if(flags & FLAG_ENABLE_PRODUCT_CODE)
      cs->enable_product_code = TRUE;
   else
      cs->enable_product_code = FALSE;

   if(flags & FLAG_FORCE_DENSITY)
      cs->force_density = TRUE;
   else
      cs->force_density = FALSE;

   if(flags & FLAG_JOB_WT_IN_RECIPE)
      cs->job_wt_in_recipe = TRUE;
   else
      cs->job_wt_in_recipe = FALSE;

   if(flags & FLAG_MIX_TIME_IN_RECIPE)
      cs->mix_time_in_recipe = TRUE;
   else
      cs->mix_time_in_recipe = FALSE;

   if(flags & FLAG_GATE_ORDER_IN_RECIPE)
      cs->gate_order_in_recipe = TRUE;
   else
      cs->gate_order_in_recipe = FALSE;

   if(flags & FLAG_RECIPE_TYPE)
      cs->recipe_type = TRUE;
   else
      cs->recipe_type = FALSE;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp1
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp1(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[0] = v->c[0];
	cs->device_name[1] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp2
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp2(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[2] = v->c[0];
	cs->device_name[3] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp3
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp3(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[4] = v->c[0];
	cs->device_name[5] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp4
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp4(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[6] = v->c[0];
	cs->device_name[7] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp5
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp5(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[8] = v->c[0];
	cs->device_name[9] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp6
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp6(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[10] = v->c[0];
	cs->device_name[11] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp7
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp7(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[12] = v->c[0];
	cs->device_name[13] = v->c[1];

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_temp8
 *                                                                          *
 ****************************************************************************/
int wv_device_name_temp8(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

	cs->device_name[14] = v->c[0];
	cs->device_name[15] = '\0';

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_1
 *                                                                          *
 ****************************************************************************/
int wv_device_name_1(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->device_name[0] = v->c[0];
   cs->device_name[1] = v->c[1];
   cs->device_name[2] = v->c[2];
   cs->device_name[3] = v->c[3];

   return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_2
 *                                                          *
 ****************************************************************************/
int wv_device_name_2(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->device_name[4] = v->c[0];
   cs->device_name[5] = v->c[1];
   cs->device_name[6] = v->c[2];
   cs->device_name[7] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_3
 *                                                                          *
 ****************************************************************************/
int wv_device_name_3(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->device_name[8] = v->c[0];
   cs->device_name[9] = v->c[1];
   cs->device_name[10] = v->c[2];
   cs->device_name[11] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_device_name_4
 *                                                                          *
 ****************************************************************************/
int wv_device_name_4(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->device_name[12] = v->c[0];
   cs->device_name[13] = v->c[1];
   cs->device_name[14] = v->c[2];
   cs->device_name[15] = v->c[3];

   return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_1
 *                                                                          *
 ****************************************************************************/
int wv_job_name_1(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->job_name[0] = v->c[0];
   cs->job_name[1] = v->c[1];
   cs->job_name[2] = v->c[2];
   cs->job_name[3] = v->c[3];

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_2
 *                                                                          *
 ****************************************************************************/
int wv_job_name_2(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->job_name[4] = v->c[0];
   cs->job_name[5] = v->c[1];
   cs->job_name[6] = v->c[2];
   cs->job_name[7] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_3
 *                                                                          *
 ****************************************************************************/
int wv_job_name_3(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->job_name[8] = v->c[0];
   cs->job_name[9] = v->c[1];
   cs->job_name[10] = v->c[2];
   cs->job_name[11] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_4
 *                                                                          *
 ****************************************************************************/
int wv_job_name_4(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->job_name[12] = v->c[0];
   cs->job_name[13] = v->c[1];
   cs->job_name[14] = v->c[2];
   cs->job_name[15] = v->c[3];

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_out_of_spec_limit
 *                                                                          *
 ****************************************************************************/
int wv_out_of_spec_limit(fic * v,point_data * pd)
{
    int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
            cfgGATE[pd->loop].out_of_spec_limit =  v->f/100.0;
            break;
        case TYPE_LIW:
            cfgEXT.out_of_spec_limit = v->f/100.0;
            break;
        case TYPE_HO:
            cfgHO.out_of_spec_limit = v->f/100.0;
            break;
        case TYPE_WTH:
            cfgWTH.out_of_spec_limit = v->f/100.0;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_receiver_fill_time
 *                                                                          *
 ****************************************************************************/
int wv_bad_feeds_before_alarm(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].num_bad_feeds_before_alarm = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_receiver_fill_time
 *                                                                          *
 ****************************************************************************/
int wv_receiver_fill_time(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].creceiver.fill_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_receiver_dump_time
 *                                                                          *
 ****************************************************************************/
int wv_receiver_dump_time(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].creceiver.dump_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_primary_gate_channel
 *                                                                          *
 ****************************************************************************/
int wv_second_gate_channel(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].secondary_gate.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_primary_gate_channel
 *                                                                          *
 ****************************************************************************/
int wv_primary_gate_channel(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].primary_gate.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_hopper_type
 *                                                                          *
 ****************************************************************************/
int wv_hopper_type(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].hopper_type = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_algorithm
 *                                                                          *
 ****************************************************************************/
int wv_algorithm(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].algorithm = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_minimum_gate_time
 *                                                                          *
 ****************************************************************************/
int wv_minimum_gate_time(fic * v,point_data * pd)
{
#if 0
   if (v->i != cfgGATE[pd->loop].minimum_gate_time)
      cfgGATE[pd->loop].clear_gate_cal = TRUE;

   cfgGATE[pd->loop].minimum_gate_time = v->i;
#endif /* really don't want outside data changing min gate time GTF */
   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_tableresolution
 *                                                                          *
 ****************************************************************************/
int wv_tableresolution(fic * v,point_data * pd)
{
   if (v->i != cfgGATE[pd->loop].time_wt_resolution)
      shrGATE[pd->loop].clear_gate_cal = TRUE;

/*   cfgGATE[pd->loop].time_wt_resolution = v->i; This should not change time_wt_resolution */

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_primary_proxy_channel
 *                                                                          *
 ****************************************************************************/
int wv_primary_proxy_channel(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].creceiver.prox_primary.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_primary_output_channel
 *                                                                          *
 ****************************************************************************/
int wv_second_output_channel(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].out_secondary.chan = v->i;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_primary_output_channel
 *                                                                          *
 ****************************************************************************/
int wv_primary_output_channel(fic * v,point_data * pd)
{
   cfgGATE[pd->loop].creceiver.out_primary.chan = v->i;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_set_batch_size
 *                                                                          *
 ****************************************************************************/
int wv_set_batch_size(fic * v,point_data * pd)
{
   cfgBWH.set_batch_size = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_ad_filter
 *                                                                          *
 ****************************************************************************/
int wv_ad_filter(fic * v,point_data * pd)
{

   int retcode = OK;
   int temp;

    switch(pd->dev_type)
    {
        case TYPE_BWH:
            temp = cfgBWH.ad_filter;
            cfgBWH.ad_filter = v->f;
            if(temp != (char)v->i)
               shr_global. remote_system_restart = TRUE;
            break;
        case TYPE_LIW:
            cfgEXT.ad_filter = v->f;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_max_coast_time
 *                                                                          *
 ****************************************************************************/
int wv_max_coast_time(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_BWH:
            cfgBWH.max_coast_time = v->i*TICKSPERSEC;
            break;
        case TYPE_LIW:
            cfgEXT.max_coast_time = v->i*TICKSPERSEC;
            break;
        case TYPE_HO:
            cfgHO.max_coast_time = v->i*TICKSPERSEC;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_ad_filter_table
 *                                                                          *
 ****************************************************************************/
int wv_ad_filter_table(fic * v,point_data * pd)
{

    int temp;

   temp = cfgBWH.ad_filter_table;
   cfgBWH.ad_filter_table = v->i;
   if(temp != (char)v->i)
      shr_global. remote_system_restart = TRUE;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_adloop_time_factor
 *                                                                          *
 ****************************************************************************/
int wv_adloop_time_factor(fic * v,point_data * pd)
{
   cfgBWH.ad_loop_timer_factor = v->i;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_min_dump_time
 *                                                                          *
 ****************************************************************************/
int wv_min_dump_time(fic * v,point_data * pd)
{
   cfgBWH.min_bwh_dump_time = v->i*TICKSPERSEC;  /* in mS GTF */

   return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_mixer_setup_flags
 *                                                                          *
 ****************************************************************************/
int wv_mixer_setup_flags(fic * v,point_data * pd)
{
   unsigned int flags = 0;

   flags = v->i;
   if( flags & FLAG_AUX_STARTER_USED)
      cfgMIXER.aux_starter_used = TRUE;
   else
      cfgMIXER.aux_starter_used = FALSE;

   if(flags & FLAG_MATERIAL_REQUEST_INVERT)
      cfgMIXER.material_request_invert = TRUE;
   else
      cfgMIXER.material_request_invert = FALSE;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_mixer_motor_mode
 *                                                                          *
 ****************************************************************************/
int wv_mixer_motor_mode(fic * v,point_data * pd)
{
   cfgMIXER.mixer_motor_mode = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_mixer_gate_mode
 *                                                                          *
 ****************************************************************************/
int wv_mixer_gate_mode(fic * v,point_data * pd)
{
   cfgMIXER.mixer_gate_mode = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_material_request_source
 *                                                                          *
 ****************************************************************************/
int wv_material_request_source(fic * v,point_data * pd)
{
   cfgMIXER.material_request_source = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_dc_pump_relay_chan
 *                                                                          *
 ****************************************************************************/
int wv_dc_pump_relay_chan(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.dc_pump_relay.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_cont_run_relay_chan
 *                                                                          *
 ****************************************************************************/
int wv_cont_run_relay_chan(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.cont_run_relay.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_anx_cont_chan
 *                                                                          *
 ****************************************************************************/
int wv_anx_cont_chan(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.anx_cont.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_dust_a_relay_chan
 *                                                                          *
 ****************************************************************************/
int wv_dust_a_relay_chan(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.dust_a_relay.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_dust_b_relay_chan
 *                                                                          *
 ****************************************************************************/
int wv_dust_b_relay_chan(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.dust_b_relay.chan = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_flags
 *                                                                          *
 ****************************************************************************/
int wv_pump_flags(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;
   int flags = 0;

   flags = v->i;
   if(flags & FLAG_DUST_USED)
      cs->cpump.dust_used = TRUE;
   else
      cs->cpump.dust_used = FALSE;

   if(flags & FLAG_CONT_USED)
      cs->cpump.cont_used = TRUE;
   else
      cs->cpump.cont_used = FALSE;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_clean_time
 *                                                                          *
 ****************************************************************************/
int wv_pump_clean_time(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.clean_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_continuous_run_time
 *                                                                          *
 ****************************************************************************/
int wv_pump_continuous_run_time(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.continuous_run_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_restart_time
 *                                                                          *
 ****************************************************************************/
int wv_pump_restart_time(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.restart_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_start_delay_time
 *                                                                          *
 ****************************************************************************/
int wv_pump_start_delay_time(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->cpump.start_delay_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_alarm_wt
 *                                                                          *
 ****************************************************************************/
int wv_alarm_wt(fic * v,point_data * pd)
{
   cfgEXT.alarm_wt = v->f;

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_critical_weight(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.crit_wt = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;


}
/****************************************************************************
****************************************************************************/
int wv_min_dump_weight(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.min_dump_wt = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_rs_maxchg
 *                                                                          *
 ****************************************************************************/
int wv_rs_maxchg(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.rs_maxchg = v->f;
            break;
        case TYPE_HO:
            cfgHO.rs_maxchg = v->f;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }

    return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_rate_speed_table_reset(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.rs_resetchg = v->f/100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_demo_max_wtp
 *                                                                          *
 ****************************************************************************/
int wv_demo_max_wtp(fic * v, point_data * pd)
{
   int retcode = OK;

   switch(pd->dev_type)
   {
      case TYPE_SYSTEM:
      case TYPE_LIW:
         cfgEXT.demo_max_wtp = v->f;
      case TYPE_HO:
         cfgHO.demo_max_ltp = v->f;
         break;
      case TYPE_MIX:
         cfgMIXER.demo_max_wtp = v->f; //drt?
         break;
      default:
          retcode = INVALID_POINT;
          break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_max_bad
 *                                                                          *
 ****************************************************************************/
int wv_max_bad(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.max_bad = v->i;
            break;
        case TYPE_HO:
            cfgHO.max_bad = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }

    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_zero_crossing
 *                                                                          *
 ****************************************************************************/
int wv_zero_crossing(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (cfgEXT.zero_crossing != v->f)
               cfgEXT.rs_num = 0;
            cfgEXT.zero_crossing = v->f;
            break;
        case TYPE_HO:
            if (cfgHO.zero_crossing != v->f)
               cfgHO.rs_num = 0;
            cfgHO.zero_crossing = v->f;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }

    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_max_spd
 *                                                                          *
 ****************************************************************************/
int wv_max_spd(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (cfgEXT.max_spd != v->f)
               cfgEXT.rs_num = 0;
            cfgEXT.max_spd = v->f;
            break;
        case TYPE_HO:
            if (cfgHO.max_spd != v->f)
               cfgHO.rs_num = 0;
            cfgHO.max_spd = v->f;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }

    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_spd_factor
 *                                                                          *
 ****************************************************************************/
int wv_spd_factor(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (cfgEXT.spd_factor != v->f)
               cfgEXT.rs_num = 0;
            cfgEXT.spd_factor = v->f;
            break;
        case TYPE_HO:
            if (cfgHO.spd_factor[0] != v->f)
               cfgHO.rs_num = 0;
            cfgHO.spd_factor[0] = v->f;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }

    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_min_wtp
 *                                                                          *
 ****************************************************************************/
int wv_min_wtp(fic * v,point_data * pd)
{
   cfgEXT.min_wtp = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_load_wt_on
 *                                                                          *
 ****************************************************************************/
int wv_load_wt_on(fic * v,point_data * pd)
{
   cfgEXT.load_wt_on = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_load_wt_off
 *                                                                          *
 ****************************************************************************/
int wv_load_wt_off(fic * v,point_data * pd)
{
   cfgEXT.load_wt_off = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_load_time
 *                                                                          *
 ****************************************************************************/
int wv_load_time(fic * v,point_data * pd)
{
   cfgEXT.load_time = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_drive_mod_address
 *                                                                          *
 ****************************************************************************/
int wv_drive_mod_address(fic * v,point_data * pd)
{
   int retcode = OK;


   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.drive_mod.addr = v->i;
         cfgEXT.drive_mod.chan = v->i >> 16;
         break;
      case TYPE_HO:
         cfgHO.drive_mod.addr = v->i;
         cfgHO.drive_mod.chan = v->i >> 16;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_weigh_mod_address
 *                                                                          *
 ****************************************************************************/
int wv_weigh_mod_address(fic * v,point_data * pd)
{

   cfgEXT.weigh_mod.addr = v->i;
   cfgEXT.weigh_mod.chan = v->i >> 16;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_load_relay
 *                                                                          *
 ****************************************************************************/
int wv_load_relay(fic * v,point_data * pd)
{

   if(!cfgEXT.use_internal_loading_signal_only)
   {
      if(!cfgEXT.use_secondary_weight_card)
      {
         cfgEXT.load_relay.addr = cfgEXT.weigh_mod.addr;
         cfgEXT.load_relay.chan = (unsigned char)(v->i)-1;
      }
      else
      {
         cfgEXT.load_relay.addr = 1;
         cfgEXT.load_relay.chan = (unsigned char)(v->i);
      }
   }

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_alarm_relay
 *                                                                          *
 ****************************************************************************/
int wv_alarm_relay(fic * v,point_data * pd)
{

   if(cfgEXT.use_secondary_weight_card)
   {
      cfgEXT.alarm_relay.addr = cfgEXT.weigh_mod.addr;
   }
   else
   {
      cfgEXT.alarm_relay.addr = 1;
   }

   cfgEXT.alarm_relay.chan = (unsigned char)(v->i)-1;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_drive_fail_error
 *                                                                          *
 ****************************************************************************/
int wv_drive_fail_error(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.drive_fail_error = v->i;
            break;
        case TYPE_HO:
            cfgHO.drive_fail_error = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_max_drive_fail
 *                                                                          *
 ****************************************************************************/
int wv_max_drive_fail(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.max_drive_fail = v->i;
            break;
        case TYPE_HO:
            cfgHO.max_drive_fail = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_movepts
 *                                                                          *
 ****************************************************************************/
int wv_movepts(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            if (cfgEXT.movepts != v->i)
               cfgEXT.rs_num = 0;
            cfgEXT.movepts = v->i;
            break;
        case TYPE_HO:
            if (cfgHO.movepts != v->i)
               cfgHO.rs_num = 0;
            cfgHO.movepts = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_calctime
 *                                                                          *
 ****************************************************************************/
int wv_calctime(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.calctime = v->i;
            break;
        case TYPE_HO:
            cfgHO.calctime = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_initial_coast
 *                                                                          *
 ****************************************************************************/
int wv_initial_coast(fic * v,point_data * pd)
{
   int retcode = OK;

    switch(pd->dev_type)
    {
        case TYPE_LIW:
            cfgEXT.initial_coast = v->i;
            break;
        case TYPE_HO:
            cfgHO.initial_coast = v->i;
            break;
        default:
            retcode = INVALID_POINT;
            break;
    }
    return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_gate_flags(fic * v, point_data * pd)
{

   unsigned int flags = 0;

   flags =  v->i;

   if(flags & ENABLE_PREPULSE) /* value 1 */
      cfgGATE[pd->loop].prepulse_enabled = TRUE;
   else
      cfgGATE[pd->loop].prepulse_enabled = FALSE;

   if(flags & FLAG_CLEAR_RECIPE_ON_CRIT_LOW) /* value 1 */
      cfgGATE[pd->loop].clear_recipe_on_crit_low = TRUE;
   else
      cfgGATE[pd->loop].clear_recipe_on_crit_low = FALSE;

   if(flags & USE_SECONDARY_GATE)/* value 2 */
      cfgGATE[pd->loop].secondary_gate_enabled = TRUE;
   else
      cfgGATE[pd->loop].secondary_gate_enabled = FALSE;

   if(flags & CLEAR_GATE_CAL_AFTER_OUTOFSPEC)  /* value 4 */
      cfgGATE[pd->loop].clear_gate_cal_after_out_of_spec = TRUE;
   else
      cfgGATE[pd->loop].clear_gate_cal_after_out_of_spec = FALSE;

   if(flags & USE_POWDER_RECEIVER)  /* value 8 */
      cfgGATE[pd->loop].use_powder_receiver = TRUE;
   else
      cfgGATE[pd->loop].use_powder_receiver = FALSE;

   if(flags & USE_AUGER_TO_FEED)  /* value 16 */
      cfgGATE[pd->loop].auger_used = TRUE;
   else
      cfgGATE[pd->loop].auger_used = FALSE;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_min_ltp
 *                                                                          *
 ****************************************************************************/
int wv_min_ltp(fic * v,point_data * pd)
{
   cfgHO.min_ltp = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_demo_max_ltp
 *                                                                          *
 ****************************************************************************/
int wv_demo_max_ltp(fic * v,point_data * pd)
{
   cfgHO.demo_max_ltp = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_ho_flags
 *                                                                          *
 ****************************************************************************/
int wv_ho_flags(fic * v,point_data * pd)
{
   unsigned int flags = 0;
   flags = v->i;

   if(flags & FLAG_STOP_INSTANTLY)
      cfgHO.stop_instantly = TRUE;
   else
      cfgHO.stop_instantly = FALSE;
   if(flags & FLAG_DI_SIGNAL_ACTIVE_ON)
      cfgHO.drive_inhibit_invert = TRUE;
   else
      cfgHO.drive_inhibit_invert = FALSE;

   if(flags & FLAG_LIMIT_SPEED_CHANGE)
      cfgHO.rs_clip = TRUE;
   else
      cfgHO.rs_clip = FALSE;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_GearInputMethod
 *                                                                          *
 ****************************************************************************/
int wv_GearInputMethod(fic * v,point_data * pd)
{
   cfgHO.GearInputMethod = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_alarm_wth_min
 *                                                                          *
 ****************************************************************************/
int wv_alarm_wth_min(fic * v,point_data * pd)
{
   cfgWTH.alarm_wth_min = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_alarm_wth_max
 *                                                                          *
 ****************************************************************************/
int wv_alarm_wth_max(fic * v,point_data * pd)
{
   cfgWTH.alarm_wth_max = v->f;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_mode
 *                                                                          *
 ****************************************************************************/
int wv_mode(fic * v,point_data * pd)
{
   cfgWTH.mode = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_wth_type
 *                                                                          *
 ****************************************************************************/
int wv_wth_type(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->wth_type = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_width_port_number
 *                                                                          *
 ****************************************************************************/
int wv_width_port_number(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->width_port.number = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_width_address
 *                                                                          *
 ****************************************************************************/
int wv_width_address(fic * v,point_data * pd)
{
   cfgWTH.width_address = v->i;

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_1
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_1(fic * v, point_data * pd)
{
   int i;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   for (i = 0;i < 4 ;i++ ) 
     r->product_code[i] = v->c[i];
   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_2
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_2(fic * v, point_data * pd)
{
   int i;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   for (i = 0;i < 4 ;i++ )   
      r->product_code[i+4] = v->c[i];
   
   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_3
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_3(fic * v, point_data * pd)
{
   int i;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   for (i = 0;i < 4 ;i++ )   
      r->product_code[i+8] = v->c[i];
     
   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_4
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_4(fic * v, point_data * pd)
{
   int i;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   for (i = 0;i < 3 ;i++ )
      r->product_code[i+12] = v->c[i];
   r->product_code[15] = '\0';   /* make sur last char is null */
    
   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp1
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp1(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[0] = v->c[0];
		cs->job_name[1] = v->c[1];
     

	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp2
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp2(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[2] = v->c[0];
		cs->job_name[3] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp3
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp3(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[4] = v->c[0];
		cs->job_name[5] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp4
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp4(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[6] = v->c[0];
		cs->job_name[7] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp5
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp5(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[8] = v->c[0];
		cs->job_name[9] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp6
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp6(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[10] = v->c[0];
		cs->job_name[11] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp7
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp7(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[12] = v->c[0];
		cs->job_name[13] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp8
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp8(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[14] = v->c[0];
		cs->job_name[15] = v->c[1];
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_name_temp9
 *                                                                          *
 ****************************************************************************/
int wv_recipe_name_temp9(fic * v, point_data * pd)
{
	config_super *cs = &cfg_super;

		cs->job_name[16] = v->c[0];
		cs->job_name[17] = '\0';
     
	return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_1
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_1(fic * v, point_data * pd)
{
   recipe_struct *r = &BATCH_RECIPE_EDIT;
   config_super *cs = &cfg_super;

   short resin_num;

   if(OPER_MODE != PAUSE_MODE)
      resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
   else
      resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[0] = v->c[0];
   cs->resin_table[resin_num].resin_desc[1] = v->c[1];
   cs->resin_table[resin_num].resin_desc[2] = v->c[2];
   cs->resin_table[resin_num].resin_desc[3] = v->c[3];

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_2
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_2(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   short resin_num;

   if(OPER_MODE != PAUSE_MODE)
      resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
   else
      resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[4] = v->c[0];
   cs->resin_table[resin_num].resin_desc[5] = v->c[1];
   cs->resin_table[resin_num].resin_desc[6] = v->c[2];
   cs->resin_table[resin_num].resin_desc[7] = v->c[3];

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_3
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_3(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   short resin_num;

   if(OPER_MODE != PAUSE_MODE)
      resin_num = BATCH_RECIPE_NOW->resin_recipe_num[pd->hop];
   else
      resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[8] = v->c[0];
   cs->resin_table[resin_num].resin_desc[9] = v->c[1];
   cs->resin_table[resin_num].resin_desc[10] = '\0';

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp1
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp1(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[0] = v->c[0];
	cs->resin_table[resin_num].resin_desc[1] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp2
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp2(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[2] = v->c[0];
	cs->resin_table[resin_num].resin_desc[3] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp3
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp3(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[4] = v->c[0];
	cs->resin_table[resin_num].resin_desc[5] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp4
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp4(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[6] = v->c[0];
	cs->resin_table[resin_num].resin_desc[7] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp5
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp5(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[8] = v->c[0];
	cs->resin_table[resin_num].resin_desc[9] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp6
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp6(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[10] = v->c[0];
	cs->resin_table[resin_num].resin_desc[11] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp7
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp7(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[12] = v->c[0];
	cs->resin_table[resin_num].resin_desc[13] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp8
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp8(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[14] = v->c[0];
	cs->resin_table[resin_num].resin_desc[15] = v->c[1];

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_temp9
 *                                                                          *
 ****************************************************************************/
int wv_resin_name_temp9(fic * v, point_data * pd)
{
	recipe_struct *r = &BATCH_RECIPE_EDIT;
	config_super *cs = &cfg_super;
	short resin_num;
   
	resin_num = r->resin_recipe_num[pd->hop];

	cs->resin_table[resin_num].resin_desc[16] = v->c[0];
	cs->resin_table[resin_num].resin_desc[17] = '\0';

	return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_1
 *                                                                          *
 ****************************************************************************/
int wv_temp_resin_name_1(fic * v, point_data * pd)
{
   recipe_struct *r = &BATCH_RECIPE_EDIT;
   config_super *cs = &cfg_super;
   short resin_num;
   
   resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[0] = v->c[0];
   cs->resin_table[resin_num].resin_desc[1] = v->c[1];
   cs->resin_table[resin_num].resin_desc[2] = v->c[2];
   cs->resin_table[resin_num].resin_desc[3] = v->c[3];

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_2
 *                                                                          *
 ****************************************************************************/
int wv_temp_resin_name_2(fic * v, point_data * pd)
{
   recipe_struct *r = &BATCH_RECIPE_EDIT;
   config_super *cs = &cfg_super;
   short resin_num;
   
   resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[4] = v->c[0];
   cs->resin_table[resin_num].resin_desc[5] = v->c[1];
   cs->resin_table[resin_num].resin_desc[6] = v->c[2];
   cs->resin_table[resin_num].resin_desc[7] = v->c[3];

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_resin_name_3
 *                                                                          *
 ****************************************************************************/
int wv_temp_resin_name_3(fic * v, point_data * pd)
{
   recipe_struct *r = &BATCH_RECIPE_EDIT;
   config_super *cs = &cfg_super;
   short resin_num;
   
   resin_num = r->resin_recipe_num[pd->hop];

   cs->resin_table[resin_num].resin_desc[8] = v->c[0];
   cs->resin_table[resin_num].resin_desc[9] = v->c[1];
   cs->resin_table[resin_num].resin_desc[10] = '\0';

   return(OK);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_status
 *                                                                          *
 ****************************************************************************/
int wv_status(fic * v,point_data * pd)
{

   int retcode = OK;
   int status_word_writes = 0;
   config_super *cs = &cfg_super;

   if (pd->sixteen_bit_mode)
   {
      if (pd->reg == R_STATUS_WORD_HI)  /* upper word           */
         status_word_writes = (unsigned int)(v->i << 16);   /* mask off lower bits,shift to lo word */
      else                      /* lower word           */
         status_word_writes = (unsigned int)v->i;   /* mask off upper bits  */
   }
   else
      status_word_writes = v->i;
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if(status_word_writes & R_RUN_RECIPE_CHANGED)
            shr_global.remote_run_recipe_changed = FALSE;

         if(status_word_writes & R_BATCH_COMPLETED)
            shrBWH.remote_batch_completed = FALSE;

         if (status_word_writes & R_RECIPE_CLEAR)
         {
            shr_global.remote_recipe_status &= ~R_RECIPE_INVALID;
            shr_global.remote_recipe_status &= ~R_RECIPE_VALID;
         }

         if(status_word_writes & R_CANCEL_JOB)
         {
            if (BLN_OPER_MODE != PAUSE_MODE)
            {
               shr_global.pause_pending = FALSE;
               /*_os_send(shr_global.super_pid, BLN_PAUSE_SIGNAL);*/
            }
            cs->job_wt_subtotal = 0.0;
            cs->job_wt_complete = FALSE;
         }
         break;
      case TYPE_GATE:
          if(status_word_writes & W_PRIMARY_SOLINOID)
             shrGATE[pd->loop].sreceiver.out_relay_on = TRUE;
          else
             shrGATE[pd->loop].sreceiver.out_relay_on = FALSE;

         if(status_word_writes & W_PUMP)
            shr_global.spump.dc_pump_on = TRUE;
         else
            shr_global.spump.dc_pump_on = FALSE;
         if(status_word_writes & W_CONT_RUN)
            shr_global.spump.cont_run_on = TRUE;
         else
            shr_global.spump.cont_run_on = FALSE;
         if(status_word_writes & W_DUST_COLL_A)
            shr_global.spump.dust_a_on = TRUE;
         else
            shr_global.spump.dust_a_on = FALSE;
         if(status_word_writes & W_DUST_COLL_B)
            shr_global.spump.dust_b_on = TRUE;
         else
            shr_global.spump.dust_b_on = FALSE;
         break;
      default:
         retcode = INVALID_POINT;
         break;
    }

    return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_control_register(fic * v, point_data * pd)
{
   int retcode = OK;
   unsigned int flags;

   recipe_struct empty_recipe;
   int i;
   stored_recipe *ptr;
   double *inven;
   config_super *cs = &cfg_super;

   flags = v->i;
   brsmemset((UDINT)&empty_recipe, 0, sizeof(recipe_struct)); /* clear recipe */
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         //if (flags & FLAG_RESTART_MINOP)                  /* 0x00000001 */
            
         //if (flags & FLAG_CLEAR_CTL_LOOP_DIAG_DATA)       /* 0x00000002 */
            
         //if (flags & FLAG_CLEAR_NET_DIAG_DATA)            /* 0x00000004 */
            
         //if (flags & FLAG_RESET_TO_FACTORY_DEFAULT)       /* 0x00000008 */
            
         //if (flags & FLAG_DEFAULT_ALARMS)                 /* 0x00000010 */
            
         //if (flags & FLAG_CLEAR_ALARMS_CONDITIONS)                             */
            
         //if (flags & FLAG_DEFAULT_RECIPES)                /* 0x00000040 */
            
         //if (flags & FLAG_RESTART_REMOTE_COMM)            /* 0x00000080 */
            
         //if (flags & FLAG_DISABLE_REMOTE_WRITES)          /* 0x00000200 */
            
         //if (flags & FLAG_SAVE_CONFIG)
         
         //if (flags & FLAG_SAVE_RECIPE)
         
         //if (flags & FLAG_LOAD_CONFIG)
         
         //if (flags & FLAG_LOAD_RECIPE)
         

         if (flags & FLAG_CLEAR_RESIN_INVENTORY)            /* 0x00000800 */
         {
             for (i=0; i<MAX_RESINS; ++i)
               PMEM.resin_inven[i] = 0.0;
         }

         if (flags & FLAG_CLEAR_RECIPE_INVENTORY)           /* 0x00000400 */
         {
            for (i = 0; i < MAX_RECIPES; ++i)
            {
               ptr = pd->rec + (i * cs->wordsperrecipe);
               inven = (double *) (ptr + cs->recipe_inven_offset);
               *inven = 0.0;
            }
         }
         //if (flags & FLAG_FORCE_RESTART)                  /* 0x00004000 */
            
         //if (flags & FLAG_STORE_CONFIG)                   /* 0x00008000 */
            
         if (flags & FLAG_VALIDATE_RECIPE)                  /* 0x00010000 */
         {
            shr_global.remote_recipe_status = 0;
            shr_global.invalid_remote_recipe = (unsigned int) rem_validate_recipe(pd, &BATCH_RECIPE_EDIT, shr_global.recipe_errors);
            if (shr_global.invalid_remote_recipe)
               shr_global.remote_recipe_status |= R_RECIPE_INVALID;
            else
               shr_global.remote_recipe_status |= R_RECIPE_VALID;
         }
         if (flags & FLAG_STORE_RECIPE)                     /* 0x00020000 */
            store_recipe(cs,pd->rec,cs->current_temp_recipe_num, (recipe_struct*)&BATCH_RECIPE_EDIT);
         if (flags & FLAG_DELETE_RECIPE)                    /* 0x00000020 */ 
            store_recipe(cs,pd->rec,cs->current_temp_recipe_num, &empty_recipe);

         //if (flags & FLAG_PROFACE)                          /* 0x00002000 */
            
         //if (flags & FLAG_DISABLE_SERVICE_CODES)            /* 0x00040000 */
            
         if (flags & FLAG_DISPENSE_TEST_AUTO_REPEAT)        /* 0x00000100 */
            shr_global.auto_repeat = TRUE;

         if (flags & FLAG_DISPENSE_TEST_CLEAR_CALIB)        /* 0x00000800 */
            shr_global.clear_gate_cal = TRUE;

         if (flags & FLAG_DISPENSE_TEST_METHOD_WEIGHT)      /* 0x00001000 */
            shr_global.dispense_test_method = DISPENSE_TEST_WEIGHT;

         if (flags & FLAG_DISPENSE_TEST_METHOD_TIME)        /* 0x00080000 */
            shr_global.dispense_test_method = DISPENSE_TEST_TIME;

         if (flags & FLAG_DISPENSE_TEST_START)              /* 0x00100000 */
         {
            shr_global.dispense_test_done = FALSE;
         }
         if (flags & FLAG_DISPENSE_TEST_MAN_REPEAT)         /* 0x00200000 */
         {
            shr_global.auto_repeat = FALSE;
         }
         //if (flags & FLAG_COM_PORT_TEST)                    /* 0x00800000 */
         
         if (flags & FLAG_IO_TEST_1)                        /* 0x01000000 */
         {
            shr_global.if_had_sec_io_card = FALSE;
            shr_global.io_test_done = FALSE;
         }
         if (flags & FLAG_IO_TEST_2)                        /* 0x02000000 */
         {
            shr_global.if_had_sec_io_card = TRUE;
            shr_global.io_test_done = FALSE;
         }
         break;
      case TYPE_LIW:
         if(flags & FLAG_CALIBRATION_ZERO_WT)
         {
            shrEXT.remote_cal_zero_done = FALSE;
         }
         if(flags & FLAG_CALIBRATION_TEST_WT)
         {
            shrEXT.remote_cal_all_done = FALSE;
         }
         if(flags & FLAG_CALIBRATION_CLEAR_STATUS)
         {
            shrEXT.remote_cal_all_done = FALSE;
         }
       break;
      case TYPE_BWH:
         if(flags & FLAG_CALIBRATION_ZERO_WT)
         {
            shrBWH.remote_cal_zero_done = FALSE;
         }
         if(flags & FLAG_CALIBRATION_TEST_WT)
         {
            shrBWH.remote_cal_all_done = FALSE;
         }
         if(flags & FLAG_CALIBRATION_CLEAR_STATUS)
         {
            shrBWH.remote_cal_all_done = FALSE;
         }
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_receiver_status
 *                                                                          *
 ****************************************************************************/
int wv_receiver_status(fic * v, point_data * pd)
{
   int flags = 0;

   flags = v->i;

   if(flags & FLAG_LEFT_SOLENOID)
      shrGATE[pd->hop].sreceiver.receiving_on = TRUE;
   else
      shrGATE[pd->hop].sreceiver.receiving_on = FALSE;

   if(flags & FLAG_DUMP_STATUS)
      shrGATE[pd->hop].sreceiver.dumping_on = TRUE;
   else
      shrGATE[pd->hop].sreceiver.dumping_on = FALSE;

   if(flags & FLAG_PROXY_SWITCH)
      shrGATE[pd->hop].sreceiver.prox_on = TRUE;
   else
      shrGATE[pd->hop].sreceiver.prox_on = FALSE;

   if(flags & FLAG_FLAPPER_VALVE)
      shrGATE[pd->hop].sreceiver.out_relay_on = TRUE;
   else
      shrGATE[pd->hop].sreceiver.out_relay_on = FALSE;

   return OK;


}
/****************************************************************************
 *                                                                          *
 * Function: wv_pump_status
 *                                                                          *
 ****************************************************************************/
int wv_pump_status(fic * v, point_data * pd)
{

   char flags = 0;

   flags = v->i;

   if(flags & FLAG_PUMP_STATUS)
      shr_global.spump.dc_pump_on = TRUE;
   else
      shr_global.spump.dc_pump_on = FALSE;

   if(flags & FLAG_DUST_A)
      shr_global.spump.dust_a_on = TRUE;
   else
      shr_global.spump.dust_a_on = FALSE;

   if(flags & FLAG_DUST_B)
      shr_global.spump.dust_b_on = TRUE;
   else
      shr_global.spump.dust_b_on = FALSE;

   if(flags & FLAG_CONTINUOUS_RUN)
      shr_global.spump.cont_run_on = TRUE;
   else
      shr_global.spump.cont_run_on = FALSE;


   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_manual_backup_status
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_manual_backup_status(fic * v, point_data * pd)
{

   unsigned int flags = 0;
   int i;

   flags = v->i;

   for(i=0; i<16; i++)
   {
      if(flags & 1 << i)
         shr_global.manual_backup_state[i] = TRUE;
      else
         shr_global.manual_backup_state[i] = FALSE;
   }
    /*_os_send(shr_global.super_pid, BLN_MANUAL_SIGNAL); */
   return OK;
}
 /****************************************************************************/
/****************************************************************************/
void if_run_system(point_data *pd)
{

    config_super *cs = &cfg_super;

   /* only for AUTO */
    if(shr_global.new_recipe == TRUE)
       shr_global.remote_run_recipe_changed = TRUE;
    if (shr_global.new_recipe && cs->autorun_after_rate_changed)
    {
      /*_os_send(shr_global.super_pid, EXT_AUTO_SIGNAL);*/
      shr_global.new_recipe = FALSE;
    }
}
/****************************************************************************
****************************************************************************/
int wv_dm_network_add(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.drive_mod.addr = v->i;
         break;
      case TYPE_HO:
         if (pd->loop == HO_LOOP)
            cfgHO.drive_mod.addr = v->i;
         else
            cfgSHO.drive_mod.addr = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_dm_network_chan(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.drive_mod.chan = v->i-1;
         break;
      case TYPE_HO:
         if (pd->loop == HO_LOOP)
            cfgHO.drive_mod.chan = v->i-1;
         else
            cfgSHO.drive_mod.chan = v->i-1;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_ho_gear_num
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_ho_gear_num(fic * v, point_data * pd)
{
   int retcode = OK;
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_HO:
         if ((cs->ltp_mode == CONTROL) &&
             (cfgHO.GearInputMethod != NO_GEAR_INPUT))
            shr_global.NewGearNum = (unsigned char) v->i;  /* ho gear number */
         break;

      default:
         break;
   }
   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_time_between_updates(fic * v, point_data * pd)
{

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.calctime = v->i*100;
         break;
      case TYPE_HO:
         cfgHO.calctime = v->i*100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_num_max_error_before_updates(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.max_bad = v->i;
         break;
      case TYPE_HO:
         cfgHO.max_bad = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_coast_tolerance(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.coast_tolerance = v->i;
         break;
      case TYPE_HO:
         cfgHO.coast_tolerance = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_initial_coast_value(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.initial_coast = v->i;
         break;
      case TYPE_HO:
         cfgHO.initial_coast = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_drive_fail_tolerance(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.drive_fail_error = v->i;
         break;
      case TYPE_HO:
         cfgHO.drive_fail_error = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;


}
/****************************************************************************
****************************************************************************/
int wv_max_drive_fail_before_alarm(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.max_drive_fail = v->i;
         break;
      case TYPE_HO:
         cfgHO.max_drive_fail = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_simulation(fic * v, point_data * pd)
{
   int retcode = OK;
   config_super *cs = &cfg_super;
   unsigned int flags;
   int i;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
        flags = v->i;
       if (flags & FLAG_SIMU_SYSTEM)
       {
             cs->demo = TRUE;
             /* set all liw loops into demo mode */
             dummy_bwh_loop(pd);

             for(i=0;i<cs->total_gates;++i)
                 dummy_gate_loop(pd,i);

             dummy_mix_loop(pd,MIXER_LOOP);

             if(cs->wtp_mode != UNUSED)
                dummy_liw_loop(pd,EXT_LOOP,DEMO_BOTH_MODS);

             if (cs->refeed == GRAVIFLUFF)
             {
                 dummy_liw_loop(pd,GRAVIFLUFF_LOOP,DEMO_BOTH_MODS);
                 dummy_liw_loop(pd,GRAVIFLUFF_FDR_LOOP,DEMO_BOTH_MODS);
             }

             /* dummy up the haul off */
             if(cs->ltp_mode != UNUSED)
                dummy_ho_loop(pd, HO_LOOP,DEMO_BOTH_MODS);

             if(cs->sltp_mode != UNUSED)
                dummy_ho_loop(pd, SHO_LOOP,DEMO_BOTH_MODS);

             dummy_wth_loop(pd);
       }
       else
       {
             cs->demo = FALSE;

             cfgBWH.demo = FALSE;

             for(i=0;i<cs->total_gates;++i)
             {
                 cfgGATE[i].demo = FALSE;
             }

             cfgEXT.demo = FALSE;
             cfgGFLUFF.demo = FALSE;
             cfgGFLUFF.demo = FALSE;
             cfgHO.demo = FALSE;
             cfgSHO.demo = FALSE;
             cfgWTH.demo = FALSE;
             cfgMIXER.demo = FALSE;
       }
         break;

     case TYPE_GATE:
        flags = v->i;
       if (flags & FLAG_SIMU_GATE)
            dummy_gate_loop(pd, pd->loop);
       else
            cfgGATE[pd->loop].demo = FALSE;

       break;
     case TYPE_BWH:
        flags = v->i;
       if (flags & FLAG_SIMU_BWH)
            dummy_bwh_loop(pd);
       else
            cfgBWH.demo = FALSE;
       break;
     case TYPE_MIX:
        flags = v->i;
       if (flags & FLAG_SIMU_MIXER)
             dummy_mix_loop(pd,MIXER_LOOP);
       else
            cfgMIXER.demo = FALSE;

        break;
      case TYPE_HO:
         flags = v->i;
         if(flags & FLAG_SIMU_DRIVE_MODULE)
            cfgHO.demo |= DEMO_DRIVE_MOD;
         else
            cfgHO.demo &= ~DEMO_DRIVE_MOD;

         if(flags & FLAG_SIMU_PULSES)
            cfgHO.demo |= DEMO_PULSES;
         else
            cfgHO.demo &= ~DEMO_PULSES;

          dummy_ho_loop(pd, pd->loop,(unsigned int) cfgHO.demo);

         break;
      case TYPE_WTH:
         flags = v->i;
         if (flags & FLAG_SIMU_WIDTH)
         	cfgWTH.demo = DEMO_BOTH_MODS;
      	else
         	cfgWTH.demo = FALSE;

         break;
      case TYPE_LIW:
      default:
         flags = v->i;

         if(flags & FLAG_SIMU_DRIVE_MODULE)
            cfgEXT.demo |= DEMO_DRIVE_MOD;
         else
            cfgEXT.demo &= ~DEMO_DRIVE_MOD;

         if(flags & FLAG_SIMU_WEIGH_MODULE)
            cfgEXT.demo |= DEMO_WEIGH_MOD;
         else
            cfgEXT.demo &= ~DEMO_WEIGH_MOD;

          dummy_liw_loop(pd, pd->loop,(unsigned int) cfgEXT.demo);
         break;
   }

   return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_set_density
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_set_density(fic * v, point_data * pd)
{
   int retcode = OK;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         if (pd->loop == ILLEGAL_LIW)
            return (INVALID_POINT);
         else
         {
	          r->density.s[pd->loop] = v->f;
              pd->recipe_changed = TRUE;
         }
         break;

      case TYPE_SYSTEM:
         r->avg_density = v->f;
         pd->recipe_changed = TRUE;
         break;

      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_wm_network_add (fic * v, point_data * pd)
{
   int retcode = OK;
   int temp;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         temp = cfgEXT.weigh_mod.addr;
         cfgEXT.weigh_mod.addr = v->i;
         if(temp != (char)v->i)
            shr_global. remote_system_restart = TRUE;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_wm_network_chan (fic * v, point_data * pd)
{
   int retcode = OK;
   int temp;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         temp = cfgEXT.weigh_mod.chan;
         cfgEXT.weigh_mod.chan = v->i-1;
         if(temp != (char)(v->i-1))
            shr_global. remote_system_restart = TRUE;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_lm_network_add(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.load_relay.addr = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_lm_network_chan(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.load_relay.chan = v->i-1;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_zerowt_bits(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.zerowt = v->i;
         break;
      case TYPE_BWH:
         cfgBWH.zerowt = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_testwt_bits(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.testwt = v->i;
         break;
      case TYPE_BWH:
         cfgBWH.testwt = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_test_wt
 *                                                                          *
 ****************************************************************************/
int wv_test_wt(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.test_wt = v->f;
         break;
      case TYPE_BWH:
         cfgBWH.test_wt = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_calibrate_status(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
    /*     pd->shr_LOOP[pd->loop].LIW->remote_calibrate_status = v->i ;*/

         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_percent_error_to_coast(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.coast_error = v->f/100;
         break;
      case TYPE_HO:
         cfgHO.coast_error = v->f/100;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_hopper_flags(fic * v, point_data * pd)
{
   unsigned int flags = 0;

   flags =  v->i;

   if(flags & FLAG_LIMIT_SPEED_CHANGE)
      cfgEXT.rs_clip = TRUE;
   else
      cfgEXT.rs_clip = FALSE;

   if(flags & FLAG_DI_SIGNAL_ACTIVE_ON)
      cfgEXT.drive_inhibit_invert = TRUE;
   else
      cfgEXT.drive_inhibit_invert = FALSE;

   if(flags & FLAG_SYNC_WEIGHT_TO_EXT_SCREW)
      cfgEXT.sync = TRUE;
   else
      cfgEXT.sync = FALSE;

   if(flags & FLAG_USE_SECOND_WM)
   {
      if ((cfgEXT.use_secondary_weight_card & FLAG_USE_SECOND_WM)==0)
            shr_global.remote_system_restart = TRUE;
      cfgEXT.use_secondary_weight_card = TRUE;
   }
   else
   {
      if (cfgEXT.use_secondary_weight_card & FLAG_USE_SECOND_WM)
            shr_global.remote_system_restart = TRUE;
      cfgEXT.use_secondary_weight_card = FALSE;
   }

   if(flags & FLAG_USE_INTERNAL_LOADING)
      cfgEXT.use_internal_loading_signal_only = TRUE;
   else
      cfgEXT.use_internal_loading_signal_only = FALSE;

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_current_mode
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_current_mode(fic * v, point_data * pd)
{
#if 0
   switch (pd->dev_type)
   {
      case TYPE_LIW:
         if (pd->loop != ILLEGAL_LIW)
         {
            pd->s_LIW->alarm_latch_bits = pd->s_LIW->alarm_bits;
            if (v->i & R_AUX_CLR_INVEN)
            {
               pd->c_LIW->inven_wt = 0.0;
            }

            if (v->i & R_AUX_CLR_SHIFT)
            {
               pd->c_LIW->shift_wt = 0.0;
            }
         }
         else
            return INVALID_POINT;
         break;

      case TYPE_HO:
         if (v->i & R_AUX_CLR_ALARMS)
         {
            shrHO.alarm_latch_bits = shrHO.alarm_bits;
         }
         if (v->i & R_AUX_CLR_INVEN)
         {
            cfgHO.inven_len = 0.0;
            cfgHO.inven_area = 0.0;
         }

         if (v->i & R_AUX_CLR_SHIFT)
         {
            cfgHO.shift_len = 0.0;
            cfgHO.shift_area = 0.0;
         }
         break;
#if 0
      case TYPE_SHO:
         if (v->i & R_AUX_CLR_ALARMS)
         {
            cfgSHO.alarm_latch_bits = cfgSHO.alarm_bits;
         }
         if (v->i & R_AUX_CLR_INVEN)
         {
            cfgSHO.inven_len = 0.0;
            cfgSHO.inven_area = 0.0;
         }

         if (v->i & R_AUX_CLR_SHIFT)
         {
            cfgSHO.shift_len = 0.0;
            cfgSHO.shift_area = 0.0;
         }
         break;
#endif
   }
#endif

   if (v->i & R_AUX_CLR_ALARMS)
   {
      shr_global.alarm_latch_bits = 0;
      shr_global.alarm_bits = 0;
      /*os_send(shr_global.super_pid, ALARM_SIGNAL);*/
   }

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_device_number
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_device_number(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cs->rem_device_defs[pd->ext].id = v->i;
         break;

      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_loading_on_weight(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.load_wt_on = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_loading_off_weight(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.load_wt_off = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_loading_time(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.load_time = v->f * FTICKSPERSEC;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_hopper_rs_factor(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.rs_fact[0] = v->f;
         cfgEXT.rs_sum = v->f;
         cfgEXT.rs_ave = v->f;

         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_low_alarm_weight(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         cfgEXT.alarm_wt = v->f;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_language(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->language = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_security_flags(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   if (v->i == RESET_TO_MIN_SECURITY)
   {
      cs->current_security_level = cs->minimum_security_level;
     shr_global.password_validation = RESET_TO_MIN_SECURITY;
   }
   else
      shr_global.password_validation = v->i;
   return OK;
}
/****************************************************************************
****************************************************************************/
int wv_set_time(fic * v, point_data * pd)
{
#if 0

   unsigned int32 date;
   unsigned int32 time;
   unsigned int16 day;
   unsigned int32 ticks;

   _os9_getime(0,&time, &date, &day, &ticks);

   time = v->i;

    _os9_setime(time, date);
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_set_date(fic * v, point_data * pd)
{
#if 0

   unsigned int32 date;
   unsigned int32 time;
   unsigned int16 day;
   unsigned int32 ticks;

   _os9_getime(0,&time, &date, &day, &ticks);

   date = v->i;

   _os9_setime(time, date);
#endif
   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_gate_to_test(fic * v, point_data * pd)
{
   shr_global.gate_to_test = v->i;

   return OK;

}

/****************************************************************************
****************************************************************************/
int wv_num_of_cycles(fic * v, point_data * pd)
{
   shr_global.num_of_cycles = v->i;

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_debug_who(fic * v, point_data * pd)
{
   shr_global.dbg_who = v->i;

   return OK;

}

/****************************************************************************
****************************************************************************/
int wv_debug_what(fic * v, point_data * pd)
{
   shr_global.dbg_what = v->i;

   return OK;
}
/****************************************************************************
****************************************************************************/
int wv_debug_remote_low_pt(fic * v, point_data * pd)
{
   struct port_diag_data_str *r_diag;
   r_diag = &(shr_global.rem_diag);

   r_diag->dbg_remote_low_pt = v->i;

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_debug_remote_high_pt(fic * v, point_data * pd)
{
   struct port_diag_data_str *r_diag;
   r_diag = &(shr_global.rem_diag);

   r_diag->dbg_remote_high_pt = v->i;

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_recipe_mode(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   cs->recipe_mode[0] = (v->i >> 16) & 255;
   cs->recipe_mode[1] = (v->i >> 8) & 255;
   cs->recipe_mode[2] = (v->i) & 255;

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_1
 *                                                                          *
 ****************************************************************************/
int wv_new_job_name_1(fic * v, point_data * pd)
{
   shr_global.new_job_name[0] = v->c[0];
   shr_global.new_job_name[1] = v->c[1];
   shr_global.new_job_name[2] = v->c[2];
   shr_global.new_job_name[3] = v->c[3];

   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_2
 *                                                                          *
 ****************************************************************************/
int wv_new_job_name_2(fic * v, point_data * pd)
{
   shr_global.new_job_name[4] = v->c[0];
   shr_global.new_job_name[5] = v->c[1];
   shr_global.new_job_name[6] = v->c[2];
   shr_global.new_job_name[7] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_3
 *                                                                          *
 ****************************************************************************/
int wv_new_job_name_3(fic * v, point_data * pd)
{
   shr_global.new_job_name[8] = v->c[0];
   shr_global.new_job_name[9] = v->c[1];
   shr_global.new_job_name[10] = v->c[2];
   shr_global.new_job_name[11] = v->c[3];

   return OK;

}

/****************************************************************************
 *                                                                          *
 * Function: wv_job_name_4
 *                                                                          *
 ****************************************************************************/
int wv_new_job_name_4(fic * v, point_data * pd)
{
   shr_global.new_job_name[12] = v->c[0];
   shr_global.new_job_name[13] = v->c[1];
   shr_global.new_job_name[14] = v->c[2];
   shr_global.new_job_name[15] = v->c[3];

   return OK;
}
/****************************************************************************
****************************************************************************/
#define NOT_USED  0
#define PRINTER   1
#define REMOTE    2
#define REMOTE_OP 3
#define WIDTH_CONTROLLER   4
#define GRAVILINK 5

int wv_port_1_device_baud_rate(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[1])
   {
      case REMOTE:
         cs->remote_port.baud = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.baud = v->i;
         break;
      case WIDTH_CONTROLLER:
         cs->width_port.baud = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.baud = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_port_3_device_baud_rate(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[3])
   {
      case REMOTE:
         cs->remote_port.baud = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.baud = v->i;
         break;
      case WIDTH_CONTROLLER:
         cs->width_port.baud = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.baud = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_port_1_device_parity(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[1])
   {
      case REMOTE:
         cs->remote_port.parity = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.parity = v->i;
         break;
      case WIDTH_CONTROLLER:
         cs->width_port.parity = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.parity = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_port_3_device_parity(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[3])
   {
      case REMOTE:
         cs->remote_port.parity = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.parity = v->i;
         break;
      case WIDTH_CONTROLLER:
         cs->width_port.parity = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.parity = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_port_1_device_port_type(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[1])
   {
      case REMOTE:
         cs->remote_port.options = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.options = v->i;
         break;
      case WIDTH_CONTROLLER:
            cs->width_port.options = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.options = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_port_3_device_port_type(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch ((unsigned int) cs->device_on_port[3])
   {
      case REMOTE:
         cs->remote_port.options = v->i;
         break;
      case REMOTE_OP:
         cs->remote_int_port.options = v->i;
         break;
      case WIDTH_CONTROLLER:
            cs->width_port.options = v->i;
         break;
      case GRAVILINK:
         cs->remote_gravitrol_link.options = v->i;
         break;
      default: /* NOT_USED */
         break;
   }

   return retcode;

}


/****************************************************************************
****************************************************************************/
int wv_ip_address(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   unsigned char temp_ipaddress[4];
   int i;

   for(i = 0; i<4; i++)
      temp_ipaddress[i] = cs->ip_address[i];

   cs->ip_address[0] = (unsigned char)v->c[0];
   cs->ip_address[1] = (unsigned char)v->c[1];
   cs->ip_address[2] = (unsigned char)v->c[2];
   cs->ip_address[3] = (unsigned char)v->c[3];

   for(i = 0; i<4; i++)
   {
      if(temp_ipaddress[i] != cs->ip_address[i])
         shr_global.remote_system_restart  = TRUE;
   }

   return OK;


}
/****************************************************************************
****************************************************************************/
int wv_subnet_mask(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->subnet_mask[0] = (unsigned char)v->c[0];
   cs->subnet_mask[1] = (unsigned char)v->c[1];
   cs->subnet_mask[2] = (unsigned char)v->c[2];
   cs->subnet_mask[3] = (unsigned char)v->c[3];

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_gateway(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   cs->gateway[0] = (unsigned char)v->c[0];
   cs->gateway[1] = (unsigned char)v->c[1];
   cs->gateway[2] = (unsigned char)v->c[2];
   cs->gateway[3] = (unsigned char)v->c[3];

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_pe_hop_a(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[0] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_b(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[1] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_c(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[2] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_d(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[3] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_e(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[4] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_f(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[5] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_g(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[6] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_h(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[7] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_hop_i(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[8] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_j(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[9] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_k(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[10] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_hop_l(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->hopper_parts_lock[11] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_setup(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_SETUP] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_calibrate(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CALIBRATE] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_change_recip(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CHANGE_RECIPE] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_store_recipe(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_STORE_RECIPE] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_stop(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_STOP] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_run(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_RUN] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}

/****************************************************************************
****************************************************************************/
int wv_pe_manual(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_EXT_MANUAL] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_clear_shift(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CLEAR_SHIFT] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_clear_inven(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CLEAR_INVEN] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_clear_resin(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CLEAR_RESIN] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_clear_recipe(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_CLEAR_RECIPE] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_pe_man_backup(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->pe_needed[PE_BLN_MANUAL] = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_super_priority(fic * v, point_data * pd)
{
#if 0
   config_super *cs = &cfg_super;


   cs->sup_pr = v->i;
   shr_global.sup_pr = cs->sup_pr;
#endif

   return OK;

}

/****************************************************************************
****************************************************************************/
int wv_sub_priority(fic * v, point_data * pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->pro_pr = v->i;
   shr_global.pro_pr = cs->pro_pr;
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_device_priority(fic * v, point_data * pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->dev_pr = v->i;
   shr_global.dev_pr = cs->dev_pr;
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_remote_priority(fic * v, point_data * pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->rem_pr = v->i;
   shr_global.rem_pr = cs->rem_pr;
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_web_server_priority(fic * v, point_data * pd)
{
#if 0
    config_super *cs = &cfg_super;


   cs->rptask_pr = v->i;
   shr_global.rptask_pr = cs->rptask_pr;
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_high_priority(fic * v, point_data * pd)
{
#if 0
   config_super *cs = &cfg_super;

   cs->high_pr = v->i;
   shr_global.high_pr = cs->high_pr;
#endif

   return OK;

}
/****************************************************************************
****************************************************************************/
char verify_password(char *string_1, char* string_2)
{
   int i;
   char retcode = TRUE;
   int len1=0, len2=0;

   while (string_1[len1] != '\0')
      len1++;

   while (string_2[len2] != '\0')
      len2++;

   if (len1 != len2)
        return (FALSE);

   for(i = 0; i<len1; i++)
   {
      if(string_1[i] != string_2[i])
      {
         retcode = FALSE;
         return retcode;
      }
   }
   return retcode;

}
/****************************************************************************
****************************************************************************/
int wv_enter_password_1(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int operator_psw_len=0, supervisor_psw_len=0,maintenance_psw_len=0;

   while (cs->operator_code[operator_psw_len] != '\0')
      operator_psw_len++;

    while (cs->operator_code[supervisor_psw_len] != '\0')
      supervisor_psw_len++;

    while (cs->operator_code[maintenance_psw_len] != '\0')
      maintenance_psw_len++;

   shr_global.verify_password[0] = v->c[0];
   shr_global.verify_password[1] = v->c[1];
   shr_global.verify_password[2] = v->c[2];
   shr_global.verify_password[3] = v->c[3];

   if (operator_psw_len <= 4 || maintenance_psw_len <= 4 || supervisor_psw_len <= 4)
   {
      shr_global.password_validation = VALID_PASSWORD;

      if (operator_psw_len <= 4 && verify_password(shr_global.verify_password,cs->operator_code))
            cs->current_security_level = OPERATOR_LOCK;
      else if (maintenance_psw_len <= 4 && verify_password(shr_global.verify_password,cs->maintenance_code))
            cs->current_security_level = MAINTENANCE_LOCK;
       else if (supervisor_psw_len <= 4 && verify_password(shr_global.verify_password,cs->supervisor_code))
            cs->current_security_level = SUPERVISOR_LOCK;
       else
       {
           cs->current_security_level = cs->minimum_security_level;
           shr_global.password_validation = INVALID_PASSWORD;
       }
   }
   else
   {
      cs->current_security_level = cs->minimum_security_level;
      shr_global.password_validation = INVALID_PASSWORD;
   }
   return OK;

}

/****************************************************************************
****************************************************************************/
int wv_enter_password_2(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   shr_global.verify_password[4] = v->c[0];
   shr_global.verify_password[5] = v->c[1];

   shr_global.password_validation  = VALID_PASSWORD;

   if (verify_password(shr_global.verify_password,"852456"))
      cs->current_security_level = SERVICE_LOCK;
   else if (verify_password(shr_global.verify_password,cs->supervisor_code))
      cs->current_security_level = SUPERVISOR_LOCK;
   else if (verify_password(shr_global.verify_password,cs->maintenance_code))
      cs->current_security_level = MAINTENANCE_LOCK;
   else if (verify_password(shr_global.verify_password,cs->operator_code))
      cs->current_security_level = OPERATOR_LOCK;
   else    /* invalid password */
   {
      cs->current_security_level = cs->minimum_security_level;
      shr_global.password_validation = INVALID_PASSWORD;
   }

   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_current_alarm_num(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         shr_global.current_alarm_num = (char)v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_alarm_setting(fic * v, point_data * pd)
{
   int retcode = OK;
#if 0
   config_super *cs = &cfg_super;

   int i;
   shr_global.reset_alarms = TRUE;

   switch (pd->dev_type)
   {
      case TYPE_LIW:
         for (i = 0; i < MAX_ALARMS; i++)
         {
               if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_LIW) && (cs->alarms[i].device == pd->hop))
               {
               #if 0
                  cs->alarms[i].pause_action = v->c[3];
                  cs->alarms[i].man_action = v->c[2];
                  cs->alarms[i].auto_action = v->c[1];
               #endif
               }
         }
         break;
      case TYPE_SYSTEM:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if (v->i == 1)
            {
               shr_global.current_alarm_status = 55;
            }
            else if (v->i == 2)
            {
               shr_global.current_alarm_status = 0;
            }
            else
            {
               if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_SYSTEM))
               {
                #if 0
                  cs->alarms[i].pause_action = v->c[3];
                  cs->alarms[i].man_action = v->c[2];
                  cs->alarms[i].auto_action = v->c[1];
				#endif
               }
            }
         }
         break;
      case TYPE_HO:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_HO) && (!cs->alarms[i].device))
            {
            #if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      case TYPE_BWH:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_BWH) && (!cs->alarms[i].device))
            {
            #if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      case TYPE_MIX:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_MIX) && (!cs->alarms[i].device))
            {
            #if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      case TYPE_GATE:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_GATE) && (cs->alarms[i].device == pd->hop))
            {
            #if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      case TYPE_WTH:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_WTH) && (!cs->alarms[i].device))
            {
			#if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      case TYPE_FLF:
         for (i = 0; i < MAX_ALARMS; i++)
         {
            if((cs->alarms[i].alarm_type == shr_global.current_alarm_num) && (cs->alarms[i].device_type == TYPE_FLF) && (!cs->alarms[i].device))
            {
            #if 0
               cs->alarms[i].pause_action = v->c[3];
               cs->alarms[i].man_action = v->c[2];
               cs->alarms[i].auto_action = v->c[1];
			#endif
            }

         }
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }
#endif
   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_backup_restore_control(fic * v, point_data * pd)
{
#if 0
   if ((unsigned)(v->i & 0x00010000) == 0x00010000)
   _os_send(shr_global.proc_mon_pid, REMOTE_TEST_PCMCIA_PRESENT_SIGNAL);
   else if ((unsigned)(v->i & 0x00200000) == 0x00200000)
   {
     shr_global.backup_restore_pcmcia_block_num = (unsigned char)((v->i >> 8) & 0xff);
     /*_os_send(shr_global.proc_mon_pid, REMOTE_TEST_IF_BLOCKNUM_EXIST_SIGNAL);*/
   }
  else   if ((unsigned)(v->i & 0x00800000) == 0x00800000) /* GO */
   {
        shr_global.backup_restore_done = FALSE;
      if ((unsigned)(v->i & 0x00020000) == 0)
         shr_global.backup_restore_destination =BACKUP_RESTORE_THRU_PCMCIA;
      else
         shr_global.backup_restore_destination =BACKUP_RESTORE_THRU_PORT;

      /*if (shr_global.backup_restore_who == BACKUP)*/
      if ((unsigned)(v->i & 0x00080000) == 0) /* BACKUP */
      {
          shr_global.backup_restore_who = BACKUP;

         if ((unsigned)(v->i & 0x00040000) == 0) /* CONFIG */
         {
             shr_global.backup_restore_what = BACKUP_RESTORE_CONFIG;
            shr_global.backup_restore_pcmcia_block_num = (char)((v->i >> 8) & 0xff);
            /*_os_send(shr_global.proc_mon_pid, REMOTE_SAVE_CONFIG_SIGNAL); */
         }
         else                                   /* BACKUP RECIPE */
         {
             shr_global.backup_restore_what = BACKUP_RESTORE_RECIPE;
            shr_global.backup_restore_port_number = (char)(v->i & 0x03);
            shr_global.backup_restore_port_type = (char)((v->i >> 2) & 0x03);
            shr_global.backup_restore_baud_rate = (char)((v->i >> 4) & 0x0f);
            /*_os_send(shr_global.proc_mon_pid, REMOTE_SAVE_RECIPE_SIGNAL); */
         }
      }
      else  /* RESTORE */
      {
          shr_global.backup_restore_who = RESTORE;

         if ((unsigned)(v->i & 0x00040000) == 0)  /* CONFIG */
         {
             shr_global.backup_restore_what = BACKUP_RESTORE_CONFIG;
             shr_global.backup_restore_what = BACKUP_RESTORE_CONFIG;
            shr_global.backup_restore_pcmcia_block_num = (char)(v->i >> 8) & 0xff;
            /*_os_send(shr_global.proc_mon_pid, REMOTE_LOAD_CONFIG_SIGNAL); */
         }
         else
         {                                      /* RESTORE RECIPE */
             shr_global.backup_restore_what = BACKUP_RESTORE_RECIPE;
            shr_global.backup_restore_port_number = (char)(v->i & 0x03);
            shr_global.backup_restore_port_type = (char)((v->i >> 2) & 0x03);
            shr_global.backup_restore_baud_rate = (char)((v->i >> 4) & 0x0f);
            /*_os_send(shr_global.proc_mon_pid, REMOTE_LOAD_RECIPE_SIGNAL); */
         }
      }
   }
#endif
   return OK;

}
/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_entry_mode
 *                                                                          *
 ****************************************************************************/
int wv_recipe_entry_mode(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->recipe_entry_mode = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_recipe_entry_mode
 *                                                                          *
 ****************************************************************************/
int wv_config_relay_num(fic * v, point_data * pd)
{
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         shr_global.config_relay_num = v->i;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
 ****************************************************************************/
int wv_remote_stop_out(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->ok_to_run_output_address.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_config_relay_location(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;
   relay_desc *relay;

   int retcode = OK;

   relay = &cs->pio_relays[shr_global.config_relay_num];
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         relay->location.chan = (unsigned char) v->i ;
         relay->location.addr = (unsigned char) v->i >> 8;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_config_relay_flags(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;
   relay_desc *relay;
   unsigned int flags = 0;

   int retcode = OK;

   relay = &cs->pio_relays[shr_global.config_relay_num];
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         flags =  v->i;

         if(flags & 1)
            relay->active = TRUE;
         else
         {
            relay->active = FALSE;
            if(shr_global.config_relay_num == 5)
               shr_global.powder_clean  = FALSE;
         }

         if(flags & 2)
            relay->invert = TRUE;
         else
            relay->invert = FALSE;

         if(flags & 4)
            cs->remote_relay_stop_end_batch  = TRUE;
         else
            cs->remote_relay_stop_end_batch  = FALSE;
         break;

         if(flags & 8)
            cs->ok_to_run_output_signal  = TRUE;
         else
            cs->ok_to_run_output_signal  = FALSE;
         break;

         if(flags & 16)
            cs->remote_purge  = TRUE;
         else
            cs->remote_purge  = FALSE;
         break;

      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/************************************************************************/
/************************************************************************/
int wv_clean_pulse_duration(fic *v,point_data *pd)
{
	config_super *cs = &cfg_super;

    switch(pd->dev_type)
    {
        case TYPE_SYSTEM:
            cs->purge_duration = v->f;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/************************************************************************/
/************************************************************************/
int wv_clean_pulse_cyletime(fic *v,point_data *pd)
{
	config_super *cs = &cfg_super;

    switch(pd->dev_type)
    {
        case TYPE_SYSTEM:
            cs->purge_cycle_time = v->f;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/************************************************************************/
/************************************************************************/
int wv_clean_toppurge_time(fic *v,point_data *pd)
{
	config_super *cs = &cfg_super;

    switch(pd->dev_type)
    {
        case TYPE_SYSTEM:
            cs->top_purge_time = v->f;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/****************************************************************************
 ****************************************************************************/
int wv_selfclean_purge1(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->purge_valve_1.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_selfclean_purge2(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->purge_valve_2.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_selfclean_purge3(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->purge_valve_3.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_remote_purge_startrelay(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->remote_purge_start_relay.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************
 ****************************************************************************/
int wv_remote_purge_stoprelay(fic * v,point_data * pd)
{
   config_super *cs = &cfg_super;

   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->remote_purge_complete_relay.chan = (unsigned char) v->i ;
         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/************************************************************************/
/* wv_accel()                                                           */
/************************************************************************/
int wv_recipe_use(fic *v,point_data *pd)
{
    switch(pd->dev_type)
    {
        case TYPE_GATE:
            cfgGATE[pd->loop].recipe_use = v->i;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/***********************************************************************/
/*   dummy_ho_loop() - sets loop into demo mode                        */
/***********************************************************************/
void dummy_ho_loop(point_data *pd, int liw, unsigned char DemoMask)
{
   if ((DemoMask & DEMO_PULSES) && (!cfgHO.cal_done))
   {
      cfgHO.nip_circumference = 2.0; /* ft */
      cfgHO.pulses_per_rev = 100.0;
      cfgHO.lenperbit = (cfgHO.nip_circumference) / cfgHO.pulses_per_rev;
      cfgHO.cal_done = TRUE;
      cfgHO.demo_max_ltp = 500.0;
   }
   cfgHO.demo |= DemoMask;
}

/***********************************************************************/
/*   dummy_wth_loop() - sets loop into demo mode                       */
/***********************************************************************/
void dummy_wth_loop(point_data *pd)
{
  cfgWTH.demo = DEMO_BOTH_MODS;
}

/***********************************************************************/
/*   dummy_liw_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_liw_loop(point_data *pd,int i, unsigned char DemoMask)
{
   register config_LIW_device *c_LIW;

   c_LIW = pd->cnf_LOOP[i].LIW;

   if ((DemoMask & DEMO_WEIGH_MOD) && (!c_LIW->cal_done))
   {
      /* do calc time             */
      c_LIW->calctime = 10 * TICKSPERSEC;

      /* do cal coefficients      */
      if (!c_LIW->cal_done)
      {
        if(c_LIW->use_secondary_weight_card)
        {
           c_LIW->zerowt = (int)BWH_DEMO_ZEROWT;
           c_LIW->testwt = (int)BWH_DEMO_TESTWT;
           c_LIW->test_wt = (float)BWH_DEMO_TEST_WT;
        }
        else
        {
           c_LIW->zerowt = (int)LIW_DEMO_ZEROWT;
           c_LIW->testwt = (int)LIW_DEMO_TESTWT;
           c_LIW->test_wt = (float)LIW_DEMO_TEST_WT;
        }

        c_LIW->wtperbit = c_LIW->test_wt / (float)(c_LIW->testwt - c_LIW->zerowt);
        c_LIW->cal_done = TRUE;
      }
      if (i == GRAVIFLUFF_FDR_LOOP)
         c_LIW->demo_max_wtp = 250.0;
      else if (i == GRAVIFLUFF_LOOP)
         c_LIW->demo_max_wtp = 500.0;
      else
         c_LIW->demo_max_wtp = 1000;

      /* do alarm, load wts         */
      if (i == GRAVIFLUFF_LOOP)
      {
         c_LIW->load_wt_on = 50.0;
         c_LIW->load_wt_off = 60.0;
         c_LIW->alarm_wt = 40.0;
         c_LIW->crit_wt = 20.0;
      }
      else
      {
         c_LIW->load_wt_on = (float)LIW_DEMO_LOAD_WT_ON;
         c_LIW->load_wt_off = (float)LIW_DEMO_LOAD_WT_OFF;
         c_LIW->alarm_wt = (float)LIW_DEMO_ALARM_WT;
         c_LIW->crit_wt = (float)LIW_DEMO_CRIT_WT;
      }
      /* set demo mode flag */
      c_LIW->cal_done = TRUE;
   }

   /* set drive fail error tolerance */
   c_LIW->drive_fail_error = 5;

   c_LIW->demo |= DemoMask;
}


/***********************************************************************/
/*   dummy_bwh_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_bwh_loop(point_data *pd)
{

  if(!cfgBWH.cal_done)
  {
    /* do cal coefficients      */
    cfgBWH.zerowt = (int)BWH_DEMO_ZEROWT;
    cfgBWH.testwt = (int)BWH_DEMO_TESTWT;
    cfgBWH.test_wt = (float)BWH_DEMO_TEST_WT;
    cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
    cfgBWH.resolution = ((float)MAX_AD_VALUE * cfgBWH.test_wt)/
                         (10000.0 * ((float)(cfgBWH.testwt - cfgBWH.zerowt)));
    /*c_BWH->offsetbits = 0;*/
    cfgBWH.cal_done = TRUE;
  }
  /* set demo mode flag */
  cfgBWH.demo = DEMO_BOTH_MODS;
}

/***********************************************************************/
/*   dummy_gate_loop() - sets loop i into demo mode                     */
/***********************************************************************/
void dummy_gate_loop(point_data *pd,int i)
{
    register config_GATE_device *c_gate;

    c_gate = &cfgGATE[i];
    /* set demo mode flag */
    c_gate->demo = DEMO_BOTH_MODS;
}


/*********************************************************************/
/*dummy_mix_loop(i)                                                  */
/*********************************************************************/
void dummy_mix_loop(point_data *pd, int i)
{
  register config_MIXER_device *c;

  c = &cfgMIXER;

  c->demo_max_wtp = 500.0;
  c->demo = DEMO_BOTH_MODS;
}
/****************************************************************************
****************************************************************************/
int wv_restore_recipe(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   int retcode = OK;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         restore_recipe(cs,pd->rec,BATCH_RECIPE_NEW,v->i); 
         cs->current_temp_recipe_num = (int) v->i;
         PMEM.recipe_num = cs->current_temp_recipe_num;
         pd->recipe_changed = TRUE;
         break;

      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}
/****************************************************************************
****************************************************************************/
int wv_store_recipe(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         store_recipe(cs,pd->rec,v->i,&BATCH_RECIPE_EDIT); 
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_num_recipe_inven
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_num_recipe_inven(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;
   stored_recipe *rec;
   double *inven;

   if(cs->enable_product_code || (cs->current_temp_recipe_num > 0))
   {
      rec = pd->rec + ((shr_global.num_recipe_inven-1) * cs->wordsperrecipe);
      rec +=  cs->recipe_inven_offset;
      inven = (double *)rec;
      *inven = 0.0;
   }

   return OK;
}
/****************************************************************************
 *                                                                          *
 * Function: wv_num_recipe_inven
 * Filename: writefun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_number_for_recipe_inven(fic * v, point_data * pd)
{
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         shr_global.num_recipe_inven = (int)v->i;
         break;

      default:
         return INVALID_POINT;
   }
   return OK;
}
/****************************************************************************
****************************************************************************/
int wv_print_id(fic * v, point_data * pd)
{
   switch (pd->dev_type)
   {
      default:
         return INVALID_POINT;
   }
   return OK;

}
/****************************************************************************
****************************************************************************/
int wv_device_on_port_1(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->device_on_port[1] = v->i;
         if(cs->device_on_port[1] == 1 || cs->device_on_port[1] == 5)
            shr_global.remote_system_restart = TRUE;
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}

/****************************************************************************
 ****************************************************************************/
int wv_device_on_port_3(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->device_on_port[3] = v->i;
         if(cs->device_on_port[3] == 1 || cs->device_on_port[3] == 5)
            shr_global.remote_system_restart = TRUE;
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}
/****************************************************************************
****************************************************************************/
int wv_remote_protocol(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->remote_protocol = v->i;
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}

/****************************************************************************
 ****************************************************************************/
int wv_remote_address(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->remote_address = v->i;
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}
/****************************************************************************
 ****************************************************************************/
int wv_new_gate_order(fic * v, point_data * pd)
{
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   switch (pd->dev_type)
   {
      case TYPE_GATE:            
         r->gate_order[pd->loop].number = v->i;
         break;
      default:
         return INVALID_POINT;
   }
   return OK;
}
/************************************************************************/
/* wv_stretch_update()                                                       */
/************************************************************************/
int wv_stretch_update(fic *v,point_data *pd)
{
    if (pd->dev_type != TYPE_HO)
      return(INVALID_POINT);

    cfgSHO.stretch_update_delay = v->f;

    return(OK);
}
/****************************************************************************
 ****************************************************************************/
int wv_dual_port_byte_order(fic * v, point_data * pd)
{
   config_super *cs = &cfg_super;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         cs->dual_port_byte_order = v->i;
         break;
      default:
         return INVALID_POINT;
   }

   return OK;
}
/****************************************************************************
****************************************************************************/
int wv_temp_resin_num(fic * v, point_data * pd)
{
   int retcode = OK;
   recipe_struct *r = &BATCH_RECIPE_EDIT;

   switch (pd->dev_type)
   {
      case TYPE_GATE:
            r->resin_recipe_num[pd->hop] = v->i;
          break;        
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;
}

int wv_loading_controlled_remotely(fic *v, point_data *pd) /* BMM 1*/
{
	config_super *cs = &cfg_super;
	cs->loading_controlled_remotely = v->i;

	return OK;
}

int wv_loadout_dig_io(fic *v, point_data *pd) /* BMM 1*/
{
	config_super *cs = &cfg_super;

	if (cs->loading_controlled_remotely)
	{
		if (v->i == 1)
			IO_dig[0].channel[pd->hop + 55] = ON;
		else
			IO_dig[0].channel[pd->hop + 55] = OFF;
	}
	return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_special_devices64()
 * Filename: writefun.c    Date: 11/19/10   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_special_devices64(fic v, point_data * pd)
{
   int resin_num, recipe_num, resin_offset, recipe_offset, i;
   stored_recipe *rec;
   config_super *cs = &cfg_super;
   char *recipe_name;

   switch (pd->dev_type)
   {
      case TYPE_RESIN_NAME:
         resin_num = (pd->reg - 1) / REGS_PER_STRING;
         resin_offset = ((pd->reg - 1) % REGS_PER_STRING) * 4;
         for (i = 0; i < 4; i++)
         {
            if (resin_offset + i < RESIN_LENGTH)
               cs->resin_table[resin_num].resin_desc[resin_offset + i] = v.c[i];
            else
               v.c[i] = 0;
         }
         break;
      case TYPE_RESIN_DATA:
         resin_num = (pd->reg - 1) / REGS_PER_STRING;
         resin_offset = ((pd->reg - 1) % REGS_PER_STRING);
         switch (resin_offset)
         {
            case R2_RESIN_DENSITY:
               cs->resin_table[resin_num].density = v.f;
               break;
            case R2_RESIN_INVEN_WT:
               PMEM.dresin_inven[resin_num] = 0.0;
               break;
         }
         break;
      case TYPE_RECIPE_NAME:
         recipe_num = (pd->reg / REGS_PER_STRING);
         recipe_offset = (((pd->reg ) % REGS_PER_STRING)-1) * 4;
         if(cs->enable_product_code)
         {
            rec = pd->rec + (recipe_num * cs->wordsperrecipe);
            rec +=  cs->recipe_inven_offset + 2;
            recipe_name = (char *)rec;
            for (i = 0; i < 4; i++)
            {
               if (recipe_offset + i < 15)
                  *(recipe_name + recipe_offset + i) = v.c[i];
            }
         }

         break;

      case TYPE_RECIPE_DATA:
         recipe_num = (pd->reg / REGS_PER_STRING);
         if(cs->enable_product_code)
            RECIPE_INVEN_WT64(recipe_num) = 0.0;			
         break;

      case TYPE_SYSTEM:
         break;

      default:
         break;
   }

	return OK;
}

/****************************************************************************
 *                                                                          *
 * Function: wv_current_recipe_inven64()
 * Filename: readfun.c    Date: 11/8/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_current_recipe_inven64(fic * v, point_data * pd)
{
   int retcode = OK;

   config_super *cs = &cfg_super;

   if(cs->enable_product_code)
      RECIPE_INVEN_WT64(shr_global.current_recipe_num) = 0.0;

   return (retcode);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_next_recipe_inven64()
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_next_recipe_inven64(fic * v, point_data * pd)
{
   int retcode = OK;
   config_super *cs = &cfg_super;

   if(cs->enable_product_code)
      RECIPE_INVEN_WT64(cs->current_temp_recipe_num) = 0.0;

   return (retcode);
}
/****************************************************************************
 *                                                                          *
 * Function: wv_inven_wt64()
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
int wv_inven_wt64(fic *v,point_data *pd)
{
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
		    GATE_LAST_INVEN_WT64(loop) = GATE_INVEN_WT64(loop);
            GATE_INVEN_WT64(loop) = 0.0;
            break;

        case TYPE_SYSTEM:
            clear_all_inven_wt(pd);
            break;

        case TYPE_LIW:
            EXT_LAST_INVEN_WT64 = EXT_INVEN_TOTAL64;
            PMEM.EXT.InvTotal = 0.0;
            break;

        default:
            return(INVALID_POINT);
     }

    return(OK);
}

/************************************************************************/
/* wv_shift_wt()                                                        */
/************************************************************************/
int wv_shift_wt64(fic *v,point_data *pd)
{
    int loop = pd->loop;

    switch(pd->dev_type)
    {
        case TYPE_GATE:
            GATE_LAST_SHIFT_WT64(loop) = GATE_SHIFT_WT64(loop);
            GATE_SHIFT_WT64(loop) = 0.0;			
            break;

        case TYPE_SYSTEM:
            clear_all_shift_wt(pd);
            break;

        case TYPE_LIW:
            EXT_LAST_SHIFT_WT64 = EXT_SHIFT_TOTAL64;
            EXT_SHIFT_TOTAL64 = 0.0;
            break;

        default:
            return(INVALID_POINT);
    }

    return(OK);
}
/************************************************************************/
/* wv_inven_len64()                                                       */
/************************************************************************/
int wv_inven_len64(fic *v,point_data *pd)
{
    HO_INVEN_TOTAL64 = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_shift_len64()                                                       */
/************************************************************************/
int wv_shift_len64(fic *v,point_data *pd)
{
    HO_SHIFT_TOTAL64 = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_inven_area64()                                                       */
/************************************************************************/
int wv_inven_area64(fic *v,point_data *pd)
{
    HO_INVEN_AREA64 = 0.0;

    return(OK);
}

/************************************************************************/
/* wv_shift_area64()                                                       */
/************************************************************************/
int wv_shift_area64(fic *v,point_data *pd)
{
    if (pd->dev_type != TYPE_HO)
      return(INVALID_POINT);

    HO_SHIFT_AREA64 = 0.0;

    return(OK);
}
/************************************************************************/
/************************************************************************/
void clear_all_inven_wt64(point_data *pd)
{
    int i;
    config_super *cs = &cfg_super;
	
    for(i=0; i<cs->total_gates;++i)
    {
      GATE_LAST_INVEN_WT64(i) = GATE_INVEN_WT64(i);
      GATE_INVEN_WT64(i) = 0.0;
    }

    HO_LAST_INVEN_LEN64 = HO_INVEN_TOTAL64;
    HO_INVEN_TOTAL64 = 0.0;
    EXT_LAST_INVEN_WT64 = EXT_INVEN_TOTAL64;
    EXT_INVEN_TOTAL64 = 0.0;
    SYS_LAST_INVEN_WT64 = SYS_INVEN_WT64;
    SYS_INVEN_WT64 = 0.0;

  /*_os_send(shr_global.super_pid, VARS_SIGNAL);*/
}

/************************************************************************/
/************************************************************************/
void clear_all_shift_wt64(point_data *pd)
{
  int i;
  config_super *cs = &cfg_super;
 
  for(i=0; i<cs->total_gates;++i)
  {
     GATE_LAST_SHIFT_WT64(i) = GATE_SHIFT_WT64(i);
     GATE_SHIFT_WT64(i) = 0.0;
  }
  HO_LAST_SHIFT_LEN64 = HO_SHIFT_TOTAL64;
  HO_SHIFT_TOTAL64 = 0.0;
  EXT_LAST_SHIFT_WT64 = EXT_SHIFT_TOTAL64;
  EXT_SHIFT_TOTAL64 = 0.0;
  SYS_LAST_SHIFT_WT64 = SYS_SHIFT_WT64;
  SYS_SHIFT_WT64 = 0.0;
}
/****************************************************************************
****************************************************************************/
int wv_control_register64(fic * v, point_data * pd)
{
   int retcode = OK;
   unsigned int flags;

   recipe_struct empty_recipe;
   int i;

   flags = v->i ;
   brsmemset((UDINT)&empty_recipe, 0, sizeof(recipe_struct)); /* clear recipe */
   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         if (flags & FLAG_CLEAR_RESIN_INVENTORY)         /* 0x00000800 */
         {
             for (i=0; i<MAX_RESINS; ++i)
               RESIN_INVEN_WT64(i) = 0.0;
         }

         if (flags & FLAG_CLEAR_RECIPE_INVENTORY)        /* 0x00000400 */
         {
            for (i = 0; i < MAX_RECIPES; ++i)
            {
               RECIPE_INVEN_WT64(i) = 0.0;
            }
         }

         break;
      default:
         retcode = INVALID_POINT;
         break;
   }

   return retcode;

}
/****************************************************************************                                                                         *
 * Function: wv_num_recipe_inven64()
 ****************************************************************************/
int wv_num_recipe_inven64(fic * v, point_data * pd)
{
   int retcode = OK;
   config_super *cs = &cfg_super;

   if(cs->enable_product_code || (cs->current_temp_recipe_num > 0))
   {
      RECIPE_INVEN_WT64(shr_global.num_recipe_inven) = 0.0;
   }

   return (retcode);
}
