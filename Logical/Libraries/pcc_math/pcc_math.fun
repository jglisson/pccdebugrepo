
FUNCTION find_table_idx : INT
	VAR_INPUT
		table : data_table;
		xval : REAL;
		hi_idx : REFERENCE TO INT;
		low_idx : REFERENCE TO INT;
	END_VAR
END_FUNCTION

FUNCTION remove_from_table : INT
	VAR_INPUT
		table : data_table;
		data : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION remove_from_table_by_index : INT
	VAR_INPUT
		table : data_table;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION table_insert_space : INT
	VAR_INPUT
		table : data_table;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION replace_table_entry : INT
	VAR_INPUT
		table : data_table;
		idx : INT;
		data : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION add_to_table : INT
	VAR_INPUT
		table : data_table;
		data : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION clear_table : INT
	VAR_INPUT
		table : data_table;
		initx : REAL;
		inity : REAL;
	END_VAR
END_FUNCTION

FUNCTION link_table : INT
	VAR_INPUT
		table : data_table;
		data : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION init_table : INT
	VAR_INPUT
		table : data_table;
		length : INT;
		data : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION incr_sample_index : INT
	VAR_INPUT
		s_data : samples;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION decr_sample_index : INT
	VAR_INPUT
		s_data : samples;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION get_sample : INT
	VAR_INPUT
		s_data : samples;
		idx : INT;
		x : REFERENCE TO REAL;
		y : REFERENCE TO REAL;
	END_VAR
END_FUNCTION

FUNCTION get_sample_index : INT
	VAR_INPUT
		s_data : samples;
	END_VAR
END_FUNCTION

FUNCTION clear_samples : INT
	VAR_INPUT
		s_data : samples;
	END_VAR
END_FUNCTION

FUNCTION remove_sample : INT
	VAR_INPUT
		s_data : samples;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION add_sample : INT
	VAR_INPUT
		s_data : samples;
		xdata : REAL;
		ydata : REAL;
	END_VAR
END_FUNCTION

FUNCTION init_samples : INT
	VAR_INPUT
		s_data : samples;
		length : INT;
		d : ordered_pair;
	END_VAR
END_FUNCTION

FUNCTION calc_rms_error : REAL
	VAR_INPUT
		error_data : samples;
	END_VAR
END_FUNCTION

FUNCTION calc_rms_error_p : REAL
	VAR_INPUT
		error_data : samples;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_std_dev_p : REAL
	VAR_INPUT
		sample_data : samples;
		mean : REFERENCE TO REAL;
		num : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_std_dev : REAL
	VAR_INPUT
		sample_data : samples;
		mean : REFERENCE TO REAL;
	END_VAR
END_FUNCTION

FUNCTION calc_mean_p : REAL
	VAR_INPUT
		sample_data : samples;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_mean : REAL
	VAR_INPUT
		sample_data : samples;
	END_VAR
END_FUNCTION

FUNCTION calc_variance_p : REAL
	VAR_INPUT
		sample_data : samples;
		mean : REFERENCE TO REAL;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_variance : REAL
	VAR_INPUT
		sample_data : samples;
		mean : REFERENCE TO REAL;
	END_VAR
END_FUNCTION

FUNCTION get_current_avg : REAL
	VAR_INPUT
		ma_ptr : moving_avg_str;
	END_VAR
END_FUNCTION

FUNCTION clear_moving_avg : INT
	VAR_INPUT
		ma_ptr : moving_avg_str;
	END_VAR
END_FUNCTION

FUNCTION simple_ma_add_pt : INT
	VAR_INPUT
		ma_ptr : moving_avg_str;
		data : REAL;
	END_VAR
END_FUNCTION

FUNCTION ma_add_pt : INT
	VAR_INPUT
		ma_ptr : moving_avg_str;
		data : REAL;
	END_VAR
END_FUNCTION

FUNCTION init_mvg_avg : INT
	VAR_INPUT
		ma_ptr : moving_avg_str;
		size : INT;
		data : REFERENCE TO REAL;
		change_limit : REAL;
	END_VAR
END_FUNCTION

FUNCTION iroundf : INT
	VAR_INPUT
		f : REAL;
	END_VAR
END_FUNCTION

FUNCTION dpercent_error : REAL
	VAR_INPUT
		a : REAL;
		b : REAL;
	END_VAR
END_FUNCTION

FUNCTION fpercent_error : REAL
	VAR_INPUT
		a : REAL;
		b : REAL;
	END_VAR
END_FUNCTION

FUNCTION calc_irms_error : INT
	VAR_INPUT
		error_data : isamples;
	END_VAR
END_FUNCTION

FUNCTION calc_irms_error_p : INT
	VAR_INPUT
		error_data : isamples;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_istd_dev_p : INT
	VAR_INPUT
		sample_data : isamples;
		mean : REFERENCE TO INT;
		num : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_istd_dev : INT
	VAR_INPUT
		sample_data : isamples;
		mean : REFERENCE TO INT;
	END_VAR
END_FUNCTION

FUNCTION calc_imean : INT
	VAR_INPUT
		sample_data : isamples;
	END_VAR
END_FUNCTION

FUNCTION calc_imean_p : INT
	VAR_INPUT
		sample_data : isamples;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_ivariance_p : INT
	VAR_INPUT
		sample_data : isamples;
		mean : REFERENCE TO INT;
		length : INT;
	END_VAR
END_FUNCTION

FUNCTION calc_ivariance : INT
	VAR_INPUT
		sample_data : isamples;
		mean : REFERENCE TO INT;
	END_VAR
END_FUNCTION

FUNCTION incr_isample_index : INT
	VAR_INPUT
		s_data : isamples;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION decr_isample_index : INT
	VAR_INPUT
		s_data : isamples;
		idx : INT;
	END_VAR
END_FUNCTION

FUNCTION get_isample : INT
	VAR_INPUT
		s_data : isamples;
		idx : INT;
		x : REFERENCE TO INT;
		y : REFERENCE TO INT;
	END_VAR
END_FUNCTION

FUNCTION get_isample_index : INT
	VAR_INPUT
		s_data : isamples;
	END_VAR
END_FUNCTION

FUNCTION remove_isample : INT
	VAR_INPUT
		s_data : isamples;
		index : INT;
	END_VAR
END_FUNCTION

FUNCTION add_isample : INT
	VAR_INPUT
		s_data : isamples;
		xdata : INT;
		ydata : INT;
	END_VAR
END_FUNCTION

FUNCTION clear_isamples : INT
	VAR_INPUT
		s_data : isamples;
	END_VAR
END_FUNCTION

FUNCTION init_isamples : INT
	VAR_INPUT
		s_data : isamples;
		length : INT;
		d : ordered_ipair;
	END_VAR
END_FUNCTION

FUNCTION linear_interp : REAL
	VAR_INPUT
		x1 : REAL;
		y1 : REAL;
		x2 : REAL;
		y2 : REAL;
		x3 : REAL;
	END_VAR
END_FUNCTION

FUNCTION clipd : REAL
	VAR_INPUT
		limit : REAL;
		value : REAL;
	END_VAR
END_FUNCTION

FUNCTION tenpt_ma_fclear : USINT
	VAR_INPUT
		x1 : ma_tp_f_struct;
	END_VAR
END_FUNCTION

FUNCTION tenpt_ma_float : USINT
	VAR_INPUT
		x1 : ma_tp_f_struct;
		x2 : REAL;
	END_VAR
END_FUNCTION

FUNCTION tenpt_ma_iclear : USINT
	VAR_INPUT
		x1 : ma_tp_i_struct;
	END_VAR
END_FUNCTION

FUNCTION tenpt_ma_int : USINT
	VAR_INPUT
		x1 : ma_tp_i_struct;
		x2 : INT;
	END_VAR
END_FUNCTION

FUNCTION clip : REAL
	VAR_INPUT
		limit : REAL;
		value : REAL;
	END_VAR
END_FUNCTION
