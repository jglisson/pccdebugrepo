#include "pcc_math.h"

signed short decr_sample_index(samples *s_data,signed short idx)
{
    if(--idx < 0)
    {
        idx = s_data->length - 1;
    }
    return idx;
}


