#include<stdio.h>
#include <string.h>
#include "pcc_math.h"


/***********************************************************************/
/*   tenpt_ma_int (int, int) - adds a point to a 10 pt MA of ints      */
/*     written by:   Greg Fisher                                       */
/*     notes:        count must be at least 1                          */
/***********************************************************************/
unsigned char tenpt_ma_int (ma_tp_i_struct *array, signed short new_point)
{
   int i;
   int sum;

   array->count += 1; //moved inside function DRT
   sum = 0;
   if (array->count == 0)
      array->count = 1;

   if(array->count < 10)
   {
      array->data[array->count - 1] = new_point;
      for(i=0;i<array->count;i++)
         sum += array->data[i];
      array->avg = sum / array->count;
      /* do I need to float these?  ZZZ GTF */
   }
   else
   {
      array->count = 10;
      /* move the data down one place */
      for(i=0;i<array->count;i++)
         array->data[i] = array->data[i+1];
      array->data[array->count - 1] = new_point;
      for(i=0;i<array->count;i++)
         sum += array->data[i];
      array->avg = sum / array->count;
      /* do I need to float these?  ZZZ GTF */
   }
   return 1;
}

