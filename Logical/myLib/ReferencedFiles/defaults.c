#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  defaults.c

   Description: This file will set the system back to it's default state.


      $Log:   F:\Software\BR_Guardian\defaults.c_v  $
 *
 *    Rev 1.22   Jun 08 2016 14:55:00   zcl
 *    changed default auxiliary timer from 60 secs to 4 secs
 *
 *Rev 1.21   Mar 24 2016  DRT
 * edit of default UCB entrys
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.defaults to toggle the debug information in this file
 *
 *    Rev 1.20   Sep 15 2008 13:13:10   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.19   Sep 03 2008 10:37:44   gtf
 * Finalized bug fix to fire user input channel number of gate instead of default gate relay #
 *
 *    Rev 1.18   Aug 05 2008 09:38:46   gtf
 * Added default for stretch_update_delay
 *
 *    Rev 1.17   Jul 16 2008 09:14:00   vidya
 * Changed the default recipe mode from NONE
 * to WTP.
 *
 *    Rev 1.15   May 09 2008 15:45:50   FMK
 * Removed defaulting PMEM for totals.
 *
 *    Rev 1.14   May 06 2008 15:07:30   FMK
 * Setup remote port settings in visualization as
 * well as config_super.
 *
 *    Rev 1.13   May 01 2008 10:26:36   FMK
 * Fixed problem with default values not being set. The
 * brsmemset command was erasing memory of configured
 * parameters. Removed memsets.
 *
 *    Rev 1.12   Apr 14 2008 13:38:20   FMK
 * Changed subnet mask to 255.255.255.0
 *
 *    Rev 1.11   Apr 11 2008 08:56:20   FMK
 * Added code for the setting up and changing
 * of the ethernet ip address and subnet mask.
 *
 *    Rev 1.10   Apr 04 2008 11:37:20   vidya
 * Changed cs->temp_recipe to PMEM.RECIPE->
 *
 *    Rev 1.9   Apr 02 2008 15:06:06   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.8   Mar 24 2008 14:30:10   FMK
 * Added drive type for the various B&R drive interface
 * possiblities. This also affected the diagnostics pages
 * and io rack alarms based on what type of drive is
 * present.
 *
 *    Rev 1.7   Mar 24 2008 13:01:10   FMK
 * Removed references to HMI structure. Not used
 * here only in INTERFACE task.
 *
 *    Rev 1.6   Feb 27 2008 14:31:00   FMK
 * Printer is no longer connected to
 * serial port. Removed from defaults.
 *
 *    Rev 1.5   Jan 30 2008 13:49:02   FMK
 * Removed dyn-data file from storeing to cf card. This
 * information should be put into bbram instead.
 *
 *    Rev 1.4   Jan 23 2008 14:17:54   FMK
 * Changed path location for default configuration
 * files on cf card.
 *
 *    Rev 1.3   Jan 18 2008 09:01:54   FMK
 * Changed batch oversize percentage so that it
 * no longer needs to be divided by 100.
 *
 *    Rev 1.2   Jan 15 2008 15:21:04   FMK
 * Corrected initialization of Filelist table. The Path
 * was incorrectly set.
 *
 *    Rev 1.1   Jan 15 2008 08:43:16   FMK
 * Found problem with building of the filelist. The
 * dynamic data filename was wrong and the EOL
 * entry was missing.
 *
 *    Rev 1.0   Jan 11 2008 10:33:32   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <string.h>
#include <asstring.h>
#include "mem.h"
#include "bwh_pcc.h"
#include "hmi.h"
#include "plc.h"
#include "sys.h"
#include "relaydef.h"
#include "units.h"
#include "version.h"
#include "alarm.h"
#include "aldeflt.h"
#include "file_rw.h"
#include "libbgdbg.h"
#include "recipe.h"
#include "gate_pcc.h"

_LOCAL char tempstr[4];           /* The string representation of the value */

void BuildFileReference(char *Fname, int FileIndex);
void default_filelist();
void default_internet();
void default_smtp();
int  default_system();
void default_gate(int loop);
void default_mix(int loop);
void default_bwh(int loop);
void default_liw(int loop);
void default_ho(int loop);
void default_sho(int loop);
void default_wth(int loop);
void default_grf(int loop);
void default_alarms();
void default_recipes();
void calc_k_factor(void);
void default_units();
void copy_alarm(config_super *cs_, unsigned char device_type, unsigned char device_id, unsigned char alarm_type, int *ptr);

int words_per_recipe(config_super *cs_);

extern void copy_recipe(recipe_struct *to, recipe_struct *from, int what);

/* table of default multipliers used for integer remote protocols */
float default_mult[100] =
{
/*  0        1        2        3        4        5        6        7        8        9  */

    1.0,     100.0,   100.0,   10.0,    10.0,    10.0,    10.0,    100.0,   100.0,   100.0,     /* 0  -  9 */
    100.0,   1.0,     1.0,     10.0,    10.0,    100.0,   100.0,   100.0,   100.0,   100.0,     /* 10 - 19 */
    100.0,   100.0,   100.0,   100.0,   100.0,   100.0,   1000.0,  1000.0,  100.0,   100.0,     /* 20 - 29 */
    100.0,   10.0,    10.0,    100.0,   100.0,   100.0,   100.0,   100.0,   100.0,   100.0,     /* 30 - 39 */
    1000.0,  100.0,   100.0,   1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 40 - 49 */
    100.0,   100.0,   1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 50 - 59 */
    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    1.0,       /* 60 - 69 */
    1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 70 - 79 */
    1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 80 - 89 */
    1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0        /* 90 - 99 */
};

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

UDINT next_byte;

/************************************************************************/
/*
   Function:  default_units(loop)

   Description:  defaults units settings
                                                                        */
/************************************************************************/
void default_units()
{
   config_super *cs = &cfg_super;

   if (cs->units == ENGLISH)
   {
      cs->id_unit = INCH;
      cs->od_unit = INCH;
      cs->width_unit = INCH;
      cs->length_unit = FEET;
      cs->thick_unit = MILS;
      cs->weight_unit = LBS;
      cs->wpa_unit = LBS_REAM;
      cs->ltp_unit = FT_MIN;
      cs->wpl_unit = LBS_FT;
      cs->wtp_unit = LB_HR;
      cs->temp_unit = FAHR;
   }
   else if (cs->units == METRIC)
   {
      cs->id_unit = CM;
      cs->od_unit = CM;
      cs->width_unit = CM;
      cs->length_unit = METER;
      cs->thick_unit = uM;
      cs->weight_unit = KGS;
      cs->wpa_unit = KG_SQM;
      cs->ltp_unit = M_MIN;
      cs->wpl_unit = KG_M;
      cs->wtp_unit = KG_HR;
      cs->temp_unit = CENT;
   }
	
   calc_k_factor();
}


/************************************************************************/
/*
   Function:  calc_k_factor()

   Description:  Calculates K factors
                                                                        */
/************************************************************************/
void calc_k_factor(void)
{
	config_super *cs = &cfg_super;
	
	if (cs->units == ENGLISH)
	{
		cs->t_k = E_T_K;
		cs->width_k = EL_WIDTH_K; // LARGE
      
		if (cs->wtp_unit == OZ_HR)
			cs->wtp_k = ES_WTP_K; // SMALL WTP
		if (cs->wtp_unit == LB_HR)
			cs->wtp_k = EL_WTP_K; // LARGE WTP

		if (cs->ltp_unit == FT_MIN)
		{
			cs->ltp_k = EL_LTP_K; // LARGE AREA
			cfgHO.visual_max_ltp = 2000.0;
		}
		else if (cs->ltp_unit == FT_HR)
		{
			cs->ltp_k = ES_LTP_K; // SMALL AREA
			cfgHO.visual_max_ltp = 120000.0;
		}

		if (cs->wpa_unit == LBS_SQFT)
		{
			cs->wpa_k = EL_WPA_K; // LARGE AREA
			cs->area_k = EL_AREA_K; // LARGE AREA
		}
		else if (cs->wpa_unit == LBS_REAM)
		{
			cs->wpa_k = ES_WPA_K; // SMALL AREA
			cs->area_k = ES_AREA_K; // SMALL AREA
		}
         
		if (cs->wpl_unit == LBS_FT)
			cs->wpl_k = EL_WPL_K; // LARGE AREA
		else if (cs->wpl_unit == LBS_1000FT)
			cs->wpl_k = ES_WPL_K; // SMALL AREA
         
		if (cs->thick_unit == MILS)
			cs->ea_k = ES_EA_K; // SMALL
		else
			cs->ea_k = EL_EA_K; // LARGE
      
		if (cs->id_unit == INCH)
			cs->id_k = EL_ID_K; // LARGE
		else
			cs->id_k = ES_ID_K; // SMALL

		if (cs->od_unit == INCH)
			cs->od_k = EL_OD_K; // LARGE
		else
			cs->od_k = ES_OD_K; // SMALL
	}
	else if (cs->units == METRIC)
	{
		cs->t_k = M_T_K;
		cs->width_k = ML_WIDTH_K; // LARGE
      
		if (cs->wtp_unit == KG_HR)
			cs->wtp_k = ML_WTP_K; // LARGE WTP
		else if (cs->wtp_unit == G_HR)
			cs->wtp_k = MS_WTP_K; // SMALL WTP
		
		if (cs->ltp_unit == M_MIN)
			cs->ltp_k = ML_LTP_K; // LARGE AREA
		else if (cs->ltp_unit == M_HR)
			cs->ltp_k = MS_LTP_K; // SMALL AREA

		if (cs->wpa_unit == KG_SQM)
		{
			cs->wpa_k = ML_WPA_K; // LARGE AREA
			cs->area_k = ML_AREA_K; // LARGE AREA
		}
		else if (cs->wpa_unit == G_SQM)
		{
			cs->wpa_k = MS_WPA_K; // SMALL AREA
			cs->area_k = MS_AREA_K; // SMALL AREA
		}
      
		if (cs->wpl_unit == KG_M)
			cs->wpl_k = ML_WPL_K; // LARGE AREA
		else if (cs->wpl_unit == G_M)
			cs->wpl_k = MS_WPL_K; // SMALL AREA

		if (cs->thick_unit == uM)
			cs->ea_k = MS_EA_K; // SMALL
		else
			cs->ea_k = ML_EA_K; // LARGE

		if (cs->id_unit == CM)
			cs->id_k = ML_ID_K; // LARGE
		else
			cs->id_k = MS_ID_K; // SMALL

		if (cs->od_unit == CM)
			cs->od_k = ML_OD_K; // LARGE
		else
			cs->od_k = MS_OD_K; // SMALL
	}
}

/************************************************************************/
/*
   Function:  default_system()

   Description:  defaults system settings
                                                                        */
/************************************************************************/
int default_system()
{
   int RetCode;
   int i;

   config_super *cs = &cfg_super;
   config_LIW_device *cnf_ext = ((config_loop_ptr*)(&cnf_LOOP[EXT_LOOP]))->LIW;
   config_LIW_device *cnf_gflf = ((config_loop_ptr*)(&cnf_LOOP[GRAVIFLUFF_FDR_LOOP]))->LIW;

   RetCode = NOTDONE;

   if (HMI.Load_status[SYS_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfg_super, 0, sizeof(config_super));
      cfg_super.language = ENGLISH_LANGUAGE;
      cfg_super.MultiLingual = FALSE;
      cfg_super.pccweb_language = ENGLISH_LANGUAGE;

      cfg_super.scrBrightness = 50;
      cfg_super.scrContrast   = 50;

      cfg_super.scrBacklightSaver   = TRUE; 
      cfg_super.scrBacklightTimeout = 300;
      cfg_super.scrPageTimeout      = 300;

      /* default_internet(cs); */
      /**********************************************************************/
      /* clear the supervisor data to factory default settings              */
      /* the default is one extruder w/o line speed control.                */
      /**********************************************************************/
      /* initialize the version info data structure */
      brsstrcpy((UDINT)cfg_super.Info.VersionMarker, (UDINT)"VERSION");
      brsstrcpy((UDINT)cfg_super.Info.VersionString, (UDINT)SW_VERSION);
	   //brsstrcpy((UDINT)shr_global.sw_version, (UDINT)SW_VERSION);

	  if (Blender_Type == GUARDIAN) 
        brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION);
	  else if (Blender_Type == AUTOBATCH)
	     brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION_AB);
	  
     brsstrcpy((UDINT)cfg_super.Info.DateString, (UDINT)SW_VERSION_DATE);
	  if (Blender_Type == AUTOBATCH)
	  {
	  	  brsstrcpy((UDINT)cfg_super.device_name, (UDINT)"AUTOBATCH");
		  cfg_super.gate_order_in_recipe = 0;
	  }
	  else
	     brsstrcpy((UDINT)cfg_super.device_name, (UDINT)"GUARDIAN");
		
		if (Blender_Type == GUARDIAN)
		   cfg_super.current_blender_type = 0;
		else 
		   cfg_super.current_blender_type = 1;

	   cfg_super.demo = FALSE;
      cfg_super.num_ext = 0;                      /* default to 0 extruder        */

      if (PLC.DigOut[1].ModuleOK)         /* is module 5 present          */
	   {
         cfg_super.total_gates = 6;
      } 
	   else 
      {
         cfg_super.total_gates = 4;
      }
		
		cfg_super.first_gate_normal_tolerance = 1;
		
		cfgEXT.movepts = 5;
      
      calc_k_factor();
      cfg_super.Flood_Feed_Timer = 15.0;
//		cfg_super.G_u16ProfibusInputBuf  = 216;
//		cfg_super.G_u16ProfibusOutputBuf = 152;  
   }
   /**********************************************************************/
   /* clear batch hopper data to factory default settings                */
   /**********************************************************************/
   if (HMI.Load_status[BWH_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgBWH, 0, (UDINT)sizeof(config_BWH_device));
      default_bwh(WEIGH_HOP_LOOP);
   }

   /**********************************************************************/
   /* clear gate feeder data to factory default settings                 */
   /**********************************************************************/
   for (i=0; i<MAX_GATES; ++i)
   {
      /* gate num is 0-11 status is 10-21 GTF */
      if (HMI.Load_status[i+GATE_A_STATUS] == STRUCTURE_DEFAULT)
      {
         /* this line clears out the structure before it is re-initialized GTF */
         /* does this work? GTF */
         next_byte = (UDINT)brsmemset((UDINT)&(cfgGATE[i]), 0, (UDINT)(sizeof(config_GATE_device)));
         default_gate(i);
      }
   }
   /**********************************************************************/
   /* clear extruder data to factory default settings                    */
   /**********************************************************************/
   if (HMI.Load_status[EXT_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgEXT, 0, (UDINT)sizeof(config_LIW_device));
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)cnf_ext, 0, (UDINT)sizeof(config_LIW_device));

      default_liw(EXT_LOOP);
      cnf_ext->ext = (char) 0;
      cnf_ext->hop = (char) 0;
      cnf_ext->extruder_with_additives = 0;
   }
   /**********************************************************************/
   /* clear nip/puller data to factory default settings                  */
   /**********************************************************************/
   if (HMI.Load_status[HO_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgHO, 0, (UDINT)sizeof(config_HO_device));
      default_ho(HO_LOOP);
   }
   if (HMI.Load_status[SHO_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgSHO, 0, (UDINT)sizeof(config_HO_device));
      default_sho(SHO_LOOP);
   }
   /**********************************************************************/
   /* clear mixer data to factory default settings                       */
   /**********************************************************************/
   if (HMI.Load_status[MIX_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgMIXER, 0, (UDINT)sizeof(config_MIXER_device));
      default_mix(MIXER_LOOP);
   }
   /**********************************************************************/
   /* clear width data to factory default settings                       */
   /**********************************************************************/
   if (HMI.Load_status[WTH_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgWTH, 0, (UDINT)sizeof(config_WTH_device));
      default_wth(WTH_LOOP);
   }
   /**********************************************************************/
   /* clear gravifluff data to factory default settings                       */
   /**********************************************************************/
   if (HMI.Load_status[GRF_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)cnf_gflf, 0, (UDINT)sizeof(config_LIW_device));
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgGFLUFF, 0, (UDINT)sizeof(config_LIW_device));
      default_grf(GRAVIFLUFF_LOOP);
      cnf_gflf->ext = (char) 0;
      cnf_gflf->hop = (char) REFEED;
      cnf_gflf->extruder_with_additives = 0;
   }

   if (HMI.Load_status[LIW_STATUS] == STRUCTURE_DEFAULT)
   {
      /* this line clears out the structure before it is re-initialized GTF */
      next_byte = (UDINT)brsmemset((UDINT)&cfgGFEEDER, 0, (UDINT)sizeof(config_LIW_device));
      default_liw(GRAVIFLUFF_FDR_LOOP);
   }

   if (HMI.Load_status[SYS_STATUS] == STRUCTURE_DEFAULT)
   {
      cfg_super.application = BLOWN_FILM;         /* default to BLOWN FILM        */
      cfg_super.wtp_mode = UNUSED;                /* set control modes            */
      cfg_super.ltp_mode = UNUSED;
      cfg_super.wth_mode = UNUSED;
      cfg_super.wth_type = KUNDIG;
      cfg_super.default_wtp = 100.0;
      cfg_super.default_ltp = 20.0;
      cfg_super.default_wpa = .02;
      cfg_super.default_wpl = .02;
      cfg_super.default_width = 24.0;
      cfg_super.default_id = 12.0;
      cfg_super.default_od = 12.0;
      cfg_super.default_thk = 8.0;
      
      cfg_super.target_accuracy = .0002;         /* .02% percent fixed and removed from setup this is min val for gate tgt_acc GTF*/
      cfg_super.recipe_mode[0] = WTP;
      cfg_super.recipe_mode[1] = NONE;
      cfg_super.recipe_mode[2] = NONE;
      cfg_super.gate_order_in_recipe = FALSE;     /* default for this is NO! */
      cfg_super.monitor_sys[0] = 17;              /* show set vs act wtp          */
      cfg_super.monitor_sys[1] = 6;               /* show inven wt, shift wt      */
      cfg_super.monitor_sys[2] = 0;               /* no other system stuff        */
      cfg_super.monitor_sys[3] = 0;
      cfg_super.recipe_entry_mode = PERCENT;
      cfg_super.layers_by = FALSE;
      cfg_super.density_in_recipe = FALSE;
      cfg_super.job_wt_in_recipe = FALSE;
      cfg_super.job_wt = 0.0;
      cfg_super.job_wt_subtotal = 0.0;
      cfg_super.refeed = FALSE;
      cfg_super.ho_in_ratio = TRUE;               /* nip part of ratio            */
      cfg_super.no_auto_from_pause = FALSE;
      cfg_super.spd_in_percent = FALSE;            /* speeds in %s                 */
      cfg_super.control_mode = (char) ((cfg_super.wtp_mode+1)*3+(cfg_super.ltp_mode+1));

      cfg_super.net_retry_time = 5*TICKSPERSEC;   /* default net timer in ticks   */
      cfg_super.net_max_tries = 10;               /* default net max tries bfr bad*/
      cfg_super.net_timeout_time = 20;            /* default timeout time in ticks*/
      cfg_super.accel = 0.02;                     /* system accel/decel as %'s    */
      cfg_super.decel = 0.02;
      cfg_super.blend_control = FALSE;                    /* loading style                 */
      cfg_super.blend_control_remote = FALSE;             /* self loading remote */
      cfg_super.cpump.restart_time = 10;
      cfg_super.cpump.clean_time = 6;                     /* 10 seconds                   */
      cfg_super.cpump.continuous_run_time = 5;            /* 5 minutes                    */
      /* cfg_super.cpump.starter_fail_time = 10;    this variable does not exist*/         /* 1 second                     */
      cfg_super.cpump.start_delay_time = 0;               /* 0.5 seconds                   */
      cfg_super.cpump.dust_used = FALSE;
      cfg_super.cpump.cont_used = FALSE;
	   cfg_super.cpump.vfd_high_relay.addr = X20;
		cfg_super.cpump.vfd_high_relay.chan = VFD_HIGH;
		cfg_super.cpump.vfd_low_relay.addr = X20;
		cfg_super.cpump.vfd_low_relay.chan = VFD_LOW;		
		cfg_super.cpump.dc_pump_relay.addr = X20;
		cfg_super.cpump.dc_pump_relay.chan = PUMP_OUT;
      cfg_super.cpump.cont_run_relay.addr = X20;
      cfg_super.cpump.cont_run_relay.chan = CONT_RUN_OUT;
      cfg_super.cpump.anx_cont.addr = X20;
      cfg_super.cpump.anx_cont.chan = AUX_CONT;
      cfg_super.cpump.dust_a_relay.addr = X20;
      cfg_super.cpump.dust_a_relay.chan = DUST_A_OUT;
      cfg_super.cpump.dust_b_relay.addr = X20;
      cfg_super.cpump.dust_b_relay.chan = DUST_B_OUT;
      cfg_super.total_receivers  = 4;
      cfg_super.DC_num_loading_before_cleaning = 6;  //G2-464 
      cfg_super.set_alarm_relay_type = 2;         /* shutdown & general only */
      cfg_super.intl_date_format = FALSE;  /* use U.S. date format by default */
      cfg_super.wordsperrecipe = (unsigned short) words_per_recipe(cs);
	   cfg_super.self_cleaning = FALSE;
	   cfg_super.self_cleaning_flag = FALSE;	
		
      if (cfg_super.wordsperrecipe != 0)
         cfg_super.num_recipes = ((int)RECIPE_SIZE / ((int)cfg_super.wordsperrecipe * (int)sizeof(stored_recipe)));
      else
         cfg_super.num_recipes = 0;

      cfg_super.DisplayLanguage = 0;      //drt          
      cfg_super.print_verbose = TRUE;
      cfg_super.print_job_wt_report = FALSE;
      cfg_super.log_alarms = FALSE;
      cfg_super.auto_reports = FALSE;
      cfg_super.report_interval = 8*60*60;    /* every 8 hrs  */
      cfg_super.report_start = 0;             /* midnight     */
      cfg_super.remote_protocol = 0;  // none by default
      //cfg_super.remote_protocol = Modbus_Serial;
      cfg_super.ab_use_crc_chksum = TRUE;
      cfg_super.remote_address = 1;
      cfg_super.remote_port.address = 1;
      cfg_super.remote_port.number = 1;                     /* slot number */
      cfg_super.remote_port.parity = Comm_Parity_None;      /* no parity  */
      cfg_super.remote_port.baud = Comm_Baud_192k;          /* 19.2k */
      cfg_super.remote_port.stop_bits = Comm_Stop_1;        /* 1 stop bit */
      cfg_super.remote_port.data_bits = Comm_Data_8;        /* 8 data bits */
      cfg_super.width_port.number = 0;
      cfg_super.width_port.baud = 15;         /* 19.2k */
      cfg_super.width_port.parity = 0x00;
      cfg_super.remote_int_port.number = 0;
      cfg_super.remote_int_port.baud = 15;    /* 19.2k */
      cfg_super.remote_int_port.parity = 0x00;
      cfg_super.compatibility_mode = FALSE;
      /* Jon Phelps said this should default to FALSE GTF */
      cfg_super.silence_old_alarms = FALSE;
      cfg_super.mbenet_enabled = TRUE;
      cfg_super.using_32bit_mode_holding_reg = TRUE;
      cfg_super.abtcp_enabled = FALSE;
      cfg_super.profibus_enabled = FALSE;
      cfg_super.clear_rec_total = FALSE;
      cfg_super.report_change_recipe = FALSE;
      cfg_super.use_batch_report = FALSE;
      cfg_super.remote_relay_stop_end_batch = TRUE;
      cfg_super.ok_to_run_output_signal = FALSE;
      cfg_super.ok_to_run_output_address.chan = 16;
      cfg_super.recipe_type = 0;
	   cfg_super.fRegrindStep=2.0;
	   cfg_super.clear_shift_total_flag = FALSE;
		cfg_super.new_wtp_smooth_clip = 3.00;
		cs->new_wtp_smooth = 5;
      cfg_super.pg201_self_clean = 1;  //G2-465 1=not active
	   remote_OneTimeAuto_write_flag = TRUE;
	   debugFlags.InvalidRemoteAutoTest = 0;
	   debugFlags.ValidRemoteAutoTest = 0;

	   cfg_super.G_u16ProfibusInputBuf  = 216;
      cfg_super.G_u16ProfibusOutputBuf = 152;
	
	   // test flags remote comms for Americhem
		shr_global.remote_build_TWT = FALSE;
		
      /* default all the register multipliers */
      for (i=0; i<100; ++i)
      {
         cfg_super.register_multiplier[i] = default_mult[i];
      }
      /* default resin strings */
      for (i=0; i<MAX_RESINS; ++i)
      {
         /*fmk    sprintf(cfg_super.resin_table[i].resin_desc,"%s_%d","RESIN",i); */
         cfg_super.resin_table[i].density = 1.0;    /* default so math won't bomb   */
      }
      /* default need key to */
      for (i=0; i<PE_SIZE; i++)             /* key needed for all functs    */
         cfg_super.pe_needed[i] = SUPERVISOR_LOCK;

      cfg_super.pe_needed[PE_SETUP]         = SUPERVISOR_LOCK;
      cfg_super.pe_needed[PE_CALIBRATE]     = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_CHANGE_RECIPE] = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_STORE_RECIPE]  = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_SELECT_RECIPE] = OPERATOR_LOCK; //G2-662
      cfg_super.pe_needed[PE_STOP]          = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_RUN]           = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_EXT_MANUAL]    = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_CLEAR_SHIFT]   = OPERATOR_LOCK;
      cfg_super.pe_needed[PE_CLEAR_INVEN]   = SUPERVISOR_LOCK;
      cfg_super.pe_needed[PE_CLEAR_RESIN]   = SUPERVISOR_LOCK;
      cfg_super.pe_needed[PE_CLEAR_RECIPE]  = SUPERVISOR_LOCK;
      cfg_super.pe_needed[PE_BLN_MANUAL]    = MAINTENANCE_LOCK;
      cfg_super.pe_needed[PE_ALARM_CONTROL] = OPERATOR_LOCK;

      for (i=0; i<(MAX_GATES+1); i++)
         cfg_super.hopper_parts_lock[i]     = OPERATOR_LOCK;

      cfg_super.current_security_level      = SERVICE_LOCK;
      cfg_super.minimum_security_level      = OPERATOR_LOCK;
      cfg_super.minimum_web_security_level  = OBSERVER_LOCK;

      brsstrcpy((UDINT)cfg_super.supervisor_code,  (UDINT)"111111");
      brsstrcpy((UDINT)cfg_super.maintenance_code, (UDINT)"222222");
      brsstrcpy((UDINT)cfg_super.operator_code,    (UDINT)"0");
      brsstrcpy((UDINT)cfg_super.observer_code,	   (UDINT)"");
      brsstrcpy((UDINT)cfg_super.job_name,	      (UDINT)"");

      cfg_super.FeatureFlag = 0x00;
      cfg_super.purge_valve_1.addr = cfg_super.purge_valve_2.addr = cfg_super.purge_valve_3.addr = 0;
      cfg_super.purge_valve_1.chan = PurgeValve_R;
      cfg_super.purge_valve_2.chan = PurgeValve_L;
      cfg_super.purge_valve_3.chan = PurgeValve_T;
      cfg_super.purge_duration = 45.0;
      cfg_super.purge_cycle_time = 1.0;
      cfg_super.top_purge_time = 10.0;
      cfg_super.remote_purge = FALSE;
      cfg_super.remote_purge_start_relay.chan = 15;
      cfg_super.remote_purge_complete_relay.chan = 1;
      cfg_super.loading_controlled_remotely = 0; /* BMM 1 */
     
      brsstrcpy((UDINT)cfg_super.u8XmlFilename,	(UDINT)"xconfig"); // G2-362 drt
      default_alarms();
      default_internet();
      default_smtp();		
      cfg_super.units = ENGLISH;  /* set up units */		
      default_units();
      RetCode = SUCCESS;
   }
   return RetCode;
}

/************************************************************************/
/*
   Function:  default_smtp()

   Description:  defaults the system email/smtp settings.
                                                                        */
/************************************************************************/
void default_smtp()
{
   strncpy((char *)smtp_info.ip, "ServerIPName", 12);         
   strncpy((char *)smtp_info.user_name, "UserName", 8);      
   strncpy((char *)smtp_info.user_pass, "UserPassword", 12);      
   strncpy((char *)smtp_info.sender_name, "SenderAddress", 13);
   strncpy((char *)smtp_info.receiver_name, "ReceiverAddress", 16);
}
/************************************************************************/
/*
   Function:  default_internet()

   Description:  defaults the system tcp/ip settings.
                                                                        */
/************************************************************************/
void default_internet()
{
   int i, j;
   int pointone;
   int pointtwo;
   int pointthree;

   pointone = pointtwo = pointthree = 0;
   cfg_super.ip_address[0] = 192;
   cfg_super.ip_address[1] = 168;
   cfg_super.ip_address[2] = 200;
   cfg_super.ip_address[3] = 105;
   for (i=0; i<16; i++)
   {
      if (gIPAddress[i] == '.')
      {
         if (!pointone)
         {                  /* found first decimal */
            pointone = i;
            for (j=0; j<4; j++)
            {
               /* 4 is filed size of tempstr */
               if (j < pointone)
                  tempstr[j] = gIPAddress[j];
               else
                  tempstr[j] = '\0';
            }
            cfg_super.ip_address[0] = brsatoi((UDINT)&tempstr);
         }
         else if (!pointtwo)
         {
            pointtwo = i;
            for (j=pointone+1; j<(pointone + 5); j++)
            {    /* j starts just past first period and reads 4 chars */
               if (j < pointtwo)
                  tempstr[j-(pointone+1)] = gIPAddress[j];  /* if not at next decimal */
               else
                  tempstr[j-(pointone+1)] = '\0';
            }
            cfg_super.ip_address[1] = brsatoi((UDINT)&tempstr);
         }
         else if (!pointthree)
         {
            pointthree = i;
            for (j=pointtwo+1; j<(pointtwo+5); j++)
            {
               if (j < pointthree)
                  tempstr[j-(pointtwo+1)] = gIPAddress[j];
               else
                  tempstr[j-(pointtwo+1)] = '\0';
            }
            cfg_super.ip_address[2] = brsatoi((UDINT)&tempstr);
            for (j=(pointthree+1); j<(pointthree+5); j++)
            {
               if (gIPAddress[j] >0)
               {
                  tempstr[j-(pointthree+1)] = gIPAddress[j];
               }
               else
                  tempstr[j-(pointthree+1)] = '\0';
            }
            cfg_super.ip_address[3] = brsatoi((UDINT)&tempstr);//cfg_super.ip_address[3] = brsatoi((unsigned long)&tempstr);
         }
      }
   }
   cfg_super.subnet_mask[0] = 255;
   cfg_super.subnet_mask[1] = 255;
   cfg_super.subnet_mask[2] = 255;
   cfg_super.subnet_mask[3] = 0;

   cfg_super.gateway[0] = 0;
   cfg_super.gateway[1] = 0;
   cfg_super.gateway[2] = 0;
   cfg_super.gateway[3] = 0;
}
/************************************************************************/
/*
   Function:  default_alarms()

   Description:  defaults the alarm conditions for all devices
                                                                        */
/************************************************************************/
void default_alarms()
{
   config_super *cs = &cfg_super;
   int j;
   int i = 0;

   shr_global.halt_alarm_processing = TRUE; /* no alarms processing here */

   brsstrcpy((UDINT)cfg_super.aux_alarm_msg, (UDINT)"Auxiliary Alarm");  /* copy in default msg */

   brsmemset((UDINT)&cfg_super.AlarmAction[0], 0, (UDINT)(MAX_ALARMS*sizeof(structAlarmAction)));

   /*
      The AlarmAction table must be initialized for each alarm condition.
      The following code will fill in this table. The index into this table
      is indicated by the variable i. It is incremented with each addition
      into the table. The device type, alarm condition and id number of the
      device is also put into the table. The severity of the alarm is
      determined in the copy_alarm function and also placed into the table.
   */

   /* system alarms    */
   copy_alarm(cs,TYPE_SYSTEM,0,CHECK_PRINTER,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,POWER_SUPPLY_FAILURE,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,HARDWARE_FAILURE,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,LOW_BATTERY,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,AUXILIARY,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,PUMP_FAILURE,&i);
   copy_alarm(cs,TYPE_SYSTEM,0,EXD_PLC_LOW_BATTERY, &i);

   copy_alarm(cs,TYPE_SYSTEM,0,RESV_INPUT1_ALARM, &i);
   cfg_super.AlarmAction[--i].pause_action = GENERAL_ALARM;
   cfg_super.AlarmAction[i].man_action = SYSTEM_SHUTDOWN;
   cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;

   copy_alarm(cs,TYPE_SYSTEM,0,RESV_INPUT2_ALARM, &i);
   cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i++].auto_action = INFORMATION_ONLY;

   copy_alarm(cs,TYPE_SYSTEM,0,RESV_INPUT3_ALARM, &i);
   cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i++].auto_action = INFORMATION_ONLY;

   /* batch hopper alarms */
   /* manual backup stops blender */
   copy_alarm(cs,TYPE_BWH,0,HOPPER_IN_MANUAL,&i);
   cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;

   /* unstable weigh system stops blender */
   copy_alarm(cs,TYPE_BWH,0,EXCESSIVE_COASTING,&i);
   cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;

   /* weigh hopper over max weight stops blender */
   copy_alarm(cs,TYPE_BWH,0,HOPPER_OVERWEIGHT,&i);
   cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;

   copy_alarm(cs,TYPE_BWH,0,HOPPER_UNDERWEIGHT,&i);
   copy_alarm(cs,TYPE_BWH,0,HOPPER_FAILURE,&i);
   copy_alarm(cs,TYPE_BWH,0,HOPPER_NOT_DUMPING,&i);

   copy_alarm(cs,TYPE_BWH,0,OUT_OF_SPEC,&i);
   cfg_super.AlarmAction[i-1].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i-1].man_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i-1].auto_action = SYSTEM_SHUTDOWN;

   copy_alarm(cs,TYPE_BWH,0,SWD_OUT_OF_SPEC_FAILURE, &i);
   cfg_super.AlarmAction[i-1].pause_action = GENERAL_ALARM;
   cfg_super.AlarmAction[i-1].man_action = GENERAL_ALARM;
   cfg_super.AlarmAction[i-1].auto_action = GENERAL_ALARM;

   copy_alarm(cs,TYPE_BWH,0,SPD_OUT_OF_SPEC_FAILURE, &i);
   cfg_super.AlarmAction[i-1].pause_action = GENERAL_ALARM;
   cfg_super.AlarmAction[i-1].man_action = GENERAL_ALARM;
   cfg_super.AlarmAction[i-1].auto_action = GENERAL_ALARM;

   /* mixer alarms */
   copy_alarm(cs, TYPE_MIX, 0, INTERLOCK_OPEN, &i);
   cfg_super.AlarmAction[i-1].auto_action  = SYSTEM_SHUTDOWN;
   cfg_super.AlarmAction[i-1].pause_action = INFORMATION_ONLY;
   cfg_super.AlarmAction[i-1].man_action   = SYSTEM_SHUTDOWN;

   copy_alarm(cs, TYPE_MIX, 0, MIXER_MOTOR_FAILURE, &i);
   cfg_super.AlarmAction[i-1].auto_action  = SYSTEM_SHUTDOWN;    //G2-404
   cfg_super.AlarmAction[i-1].pause_action = INFORMATION_ONLY;   //G2-404
   cfg_super.AlarmAction[i-1].man_action   = SYSTEM_SHUTDOWN;    //G2-404

   copy_alarm(cs, TYPE_MIX, 0, VALVE_FAILURE, &i);
   copy_alarm(cs, TYPE_MIX, 0, MAT_REQ_UNCOVERED_FAILURE, &i);
	
	/*	per ingredient hopper switch alarms for 1Kg Guardian 	*/
	for (j=0; j<cfg_super.total_gates; j++)		
	{
		copy_alarm(cs, TYPE_GATE, j, HOPPER_SWITCH_FAILURE_ALARM, &i);     
		cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
		cfg_super.AlarmAction[i].man_action     = SYSTEM_SHUTDOWN;
		cfg_super.AlarmAction[i++].auto_action  = SYSTEM_SHUTDOWN;
	}

  /* per ingredient hopper alarms */
   for (j=0; j<cfg_super.total_gates; j++)
   {
      copy_alarm(cs,TYPE_GATE,j,OUT_OF_SPEC,&i);
      copy_alarm(cs,TYPE_GATE,j,HOPPER_LEVEL_LOW_WARNING,&i);   /* for AutoBatch */
      copy_alarm(cs,TYPE_GATE,j,HOPPER_LOW,&i);                 /* for AutoBatch */
      copy_alarm(cs,TYPE_GATE,j,HOPPER_CRIT_LOW_TIME,&i);
      copy_alarm(cs,TYPE_GATE,j,LOW_DUMP_SIZE,&i);
	   copy_alarm(cs,TYPE_GATE,j,HOPPER_CRITICAL_LOW,&i);        /* Guardian */
	   copy_alarm(cs,TYPE_GATE,j,HOPPER_PROX_HIGH_LEVEL,&i);     /* Guardian */
	   copy_alarm(cs,TYPE_GATE,j,HOPPER_PROX_LOW_LEVEL,&i);      /* Guardian */
   }

   /* extruder alarms */
   if (cfg_super.wtp_mode != UNUSED)
   {
      //copy_alarm(cs,TYPE_LIW,0,HOPPER_FAILURE,&i);
      //copy_alarm(cs,TYPE_LIW,0,HOPPER_RESET,&i);
      //copy_alarm(cs,TYPE_LIW,0,HOPPER_SW_INCOMPATABLE,&i);
      //copy_alarm(cs,TYPE_LIW,0,HOPPER_NOT_RESPONDING,&i);
      //copy_alarm(cs,TYPE_LIW,0,HOPPER_IN_MANUAL,&i);

      copy_alarm(cs,TYPE_LIW,0,HOPPER_OVERWEIGHT,&i);
      cfg_super.AlarmAction[i-1].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i-1].man_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i-1].auto_action = SYSTEM_SHUTDOWN;
      
      copy_alarm(cs,TYPE_LIW,0,LOW_DUMP_SIZE,&i);
		cfg_super.AlarmAction[i-1].pause_action = INFORMATION_ONLY;
		cfg_super.AlarmAction[i-1].man_action = INFORMATION_ONLY;
		cfg_super.AlarmAction[i-1].auto_action = INFORMATION_ONLY;
		
		copy_alarm(cs,TYPE_LIW,0,HOPPER_LOW,&i);
		cfg_super.AlarmAction[i-1].pause_action = GENERAL_ALARM; //G2-638
		cfg_super.AlarmAction[i-1].man_action = GENERAL_ALARM;   //G2-638
		cfg_super.AlarmAction[i-1].auto_action = GENERAL_ALARM;  //G2-638

      /* do critical low as a shutdown here */
      copy_alarm(cs,TYPE_LIW,0,HOPPER_CRITICAL_LOW,&i);
      cfg_super.AlarmAction[--i].pause_action = GENERAL_ALARM; //G2-638
      cfg_super.AlarmAction[i].man_action = GENERAL_ALARM;     //G2-638
      cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;

      //copy_alarm(cs,TYPE_LIW,0,LOSS_OF_SYNC,&i);
      copy_alarm(cs,TYPE_LIW,0,HOPPER_UNDERWEIGHT,&i);

      copy_alarm(cs,TYPE_LIW,0,EXCESSIVE_COASTING,&i);
      cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i++].auto_action = INFORMATION_ONLY;

      //copy_alarm(cs,TYPE_LIW,0,DRIVE_FAILURE,&i);
      //copy_alarm(cs,TYPE_LIW,0,DRIVE_RESET,&i);
      //copy_alarm(cs,TYPE_LIW,0,DRIVE_SW_INCOMPATABLE,&i);

      copy_alarm(cs,TYPE_LIW,0,DRIVE_NOT_RESPONDING,&i);
      cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i].man_action = SYSTEM_SHUTDOWN;
      cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;
      
      copy_alarm(cs,TYPE_LIW,0,DRIVE_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_LIW,0,DRIVE_INHIBIT,&i);
      copy_alarm(cs,TYPE_LIW,0,DRIVE_OVERSPEED,&i);
      copy_alarm(cs,TYPE_LIW,0,OUT_OF_SPEC,&i);
      copy_alarm(cs,TYPE_LIW,0,DRIVE_UNDERSPEED,&i);
      copy_alarm(cs,TYPE_LIW,0,DRIVE_SYSTEM_FAILURE,&i);
   }

   /* line speed alarms  */
   if (cfg_super.ltp_mode != UNUSED)
   {
      copy_alarm(cs,TYPE_HO,0,EXCESSIVE_COASTING,&i);
      cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i].man_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i++].auto_action = INFORMATION_ONLY;

      //copy_alarm(cs,TYPE_LIW,0,DRIVE_FAILURE,&i);
      //copy_alarm(cs,TYPE_LIW,0,DRIVE_RESET,&i);
      //copy_alarm(cs,TYPE_LIW,0,DRIVE_SW_INCOMPATABLE,&i);

      copy_alarm(cs,TYPE_HO,0,DRIVE_NOT_RESPONDING,&i);
      cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i].man_action = SYSTEM_SHUTDOWN;
      cfg_super.AlarmAction[i++].auto_action = SYSTEM_SHUTDOWN;
      
      copy_alarm(cs,TYPE_HO,0,DRIVE_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_HO,0,DRIVE_INHIBIT,&i);
      copy_alarm(cs,TYPE_HO,0,DRIVE_OVERSPEED,&i);
      copy_alarm(cs,TYPE_HO,0,OUT_OF_SPEC,&i);
      copy_alarm(cs,TYPE_HO,0,DRIVE_UNDERSPEED,&i);
      copy_alarm(cs,TYPE_HO,0,DRIVE_SYSTEM_FAILURE_HO,&i);
      
      //copy_alarm(cs,TYPE_HO,0,DRIVE_FAILURE,&i);
      //copy_alarm(cs,TYPE_HO,0,DRIVE_RESET,&i);
      //copy_alarm(cs,TYPE_HO,0,DRIVE_SW_INCOMPATABLE,&i);
      //copy_alarm(cs,TYPE_HO,0,DRIVE_NOT_RESPONDING,&i);
      //copy_alarm(cs,TYPE_HO, 0, EXCESSIVE_COASTING, &i);
      //
      //if (cfg_super.ltp_mode == CONTROL)
      //{
      //   copy_alarm(cs,TYPE_HO,0,DRIVE_IN_MANUAL,&i);
      //   copy_alarm(cs,TYPE_HO,0,DRIVE_INHIBIT,&i);
      //   copy_alarm(cs,TYPE_HO,0,DRIVE_OVERSPEED,&i);
      //   copy_alarm(cs,TYPE_HO,0,OUT_OF_SPEC,&i);
      //   copy_alarm(cs,TYPE_HO,0,DRIVE_UNDERSPEED,&i);
      //   copy_alarm(cs,TYPE_HO,0,DRIVE_SYSTEM_FAILURE,&i);
      //}
   }
   if (cfg_super.sltp_mode != UNUSED)
   {
      copy_alarm(cs,TYPE_HO,1,DRIVE_FAILURE,&i);
      copy_alarm(cs,TYPE_HO,1,DRIVE_RESET,&i);
      copy_alarm(cs,TYPE_HO,1,DRIVE_SW_INCOMPATABLE,&i);
      copy_alarm(cs,TYPE_HO,1,DRIVE_NOT_RESPONDING,&i);

      if (cfg_super.sltp_mode == CONTROL)
      {
         copy_alarm(cs,TYPE_HO,1,DRIVE_IN_MANUAL,&i);
         copy_alarm(cs,TYPE_HO,1,DRIVE_INHIBIT,&i);
         copy_alarm(cs,TYPE_HO,1,DRIVE_OVERSPEED,&i);
         copy_alarm(cs,TYPE_HO,1,OUT_OF_SPEC,&i);
         copy_alarm(cs,TYPE_HO,1,DRIVE_UNDERSPEED,&i);
         copy_alarm(cs,TYPE_HO,1,DRIVE_SYSTEM_FAILURE,&i);
      }
   }

   /* gravifluff alarms */
   if (cfg_super.scrap && (cfg_super.refeed == GRAVIFLUFF))
   {
      /* gravifluff loader alarms */
      copy_alarm(cs,TYPE_FLF,0,HOPPER_FAILURE,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_RESET,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_SW_INCOMPATABLE,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_NOT_RESPONDING,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_OVERWEIGHT,&i);
      copy_alarm(cs,TYPE_FLF,0,HOPPER_LOW,&i);

      copy_alarm(cs,TYPE_FLF,0,HOPPER_CRITICAL_LOW,&i);
      cfg_super.AlarmAction[--i].pause_action = INFORMATION_ONLY;
      cfg_super.AlarmAction[i].man_action = GENERAL_ALARM;
      cfg_super.AlarmAction[i++].auto_action = GENERAL_ALARM;

      copy_alarm(cs,TYPE_FLF,0,HOPPER_UNDERWEIGHT,&i);

      /* gravifluff feeder alarms */
      copy_alarm(cs,TYPE_LIW,1,HOPPER_FAILURE,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_RESET,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_SW_INCOMPATABLE,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_NOT_RESPONDING,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_OVERWEIGHT,&i);
      copy_alarm(cs,TYPE_LIW,1,LOW_DUMP_SIZE,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_LOW,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_CRITICAL_LOW,&i);

      copy_alarm(cs,TYPE_LIW,1,LOSS_OF_SYNC,&i);
      copy_alarm(cs,TYPE_LIW,1,HOPPER_UNDERWEIGHT,&i);
      copy_alarm(cs,TYPE_LIW,1,EXCESSIVE_COASTING,&i);

      copy_alarm(cs,TYPE_LIW,1,DRIVE_FAILURE,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_RESET,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_SW_INCOMPATABLE,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_NOT_RESPONDING,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_INHIBIT,&i);

      copy_alarm(cs,TYPE_LIW,1,DRIVE_OVERSPEED,&i);

      copy_alarm(cs,TYPE_LIW,1,OUT_OF_SPEC,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_UNDERSPEED,&i);
      copy_alarm(cs,TYPE_LIW,1,DRIVE_SYSTEM_FAILURE,&i);
   }

   /* width controller alarms  */
   if (cfg_super.wth_mode != UNUSED)
   {
      copy_alarm(cs,TYPE_WTH,0,WIDTH_FAILURE,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_SW_INCOMPATIBLE,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_NOT_RESPONDING,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_IN_MANUAL,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_PRODUCT_BREAK,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_MEASUREMENT_ERROR,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_OVER_MAXIMUM,&i);
      copy_alarm(cs,TYPE_WTH,0,WIDTH_UNDER_MINIMUM,&i);
      copy_alarm(cs,TYPE_WTH,0,OUT_OF_SPEC,&i);
   }

   shr_global.halt_alarm_processing = FALSE; /* restart alarms processing here */
   shr_global.ack_all_alarms = TRUE; /* since reset the alarms database, go clear the ack bits */
}

/************************************************************************/
/*
   Function:  copy_alarm()

   Description:  copies a single alarm into the database
                                                                        */
/************************************************************************/
  void copy_alarm(config_super *cs_, unsigned char device_type, unsigned char device_id, unsigned char alarm_type, int *ptr)
  {
     int i, l;
     char opermode;
     unsigned char severity;

     i = *ptr;

  /*
     An entry is placed into the AlarmAction table which indicates the device,
     device id #, alarm type, and the severity of the alarm based on the
     operating mode of the blender. aldeflt.h is used to determine the
     severity level of the alarm.

     A severity of 4 is a shutdown action
                   2 is a general action
                   1 is an information action
  */
     cfg_super.AlarmAction[i].device_type = device_type;
     cfg_super.AlarmAction[i].device = device_id;
     cfg_super.AlarmAction[i].alarm_type = alarm_type;
     for (l=2; l>=0; l--)
     {
        opermode = alarm_defaults[alarm_type][l];
        if (opermode) 
        {
           severity = 1<<l;
           if (opermode & PAUSE_MASK)
              cfg_super.AlarmAction[i].pause_action = severity;
           if (opermode & MAN_MASK)
              cfg_super.AlarmAction[i].man_action = severity;
           if (opermode & AUTO_MASK)
              cfg_super.AlarmAction[i].auto_action = severity;
        }
     }
     (*ptr)++;
  }
  /************************************************************************/
  /*
     Function:  default_bwh(loop)

     Description:  defaults batch weigh hopper settings
                                                                          */
  /************************************************************************/
  void default_bwh(int loop)
  {
     /* set default network addresses */
     cfgBWH.weigh_hopper.addr = 0;
     cfgBWH.weigh_hopper.chan = 1;   /* what is this and what should it be set to? */
     cfgBWH.dump_valve.addr = 0;
     cfgBWH.dump_valve.chan = WEIGH_HOPPER_DUMP_OUT;
     cfgBWH.cal_done = FALSE;
     cfgBWH.ad_filter = 200.0;

	  if (Blender_Type == AUTOBATCH) //current_blender_type
	  { 
        cfg_super.pg2180_aux5_invert = 0; 
        cfg_super.pg175_System_Size = 11; /* 15kg */
	     cfgBWH.set_batch_size = 33.0;
        cfgBWH.max_batch_size = cfgBWH.set_batch_size * 1.10;
        cfgBWH.min_batch_size = 10.0; //cfgBWH.set_batch_size * 0.50; //drt set to 10.0 now
	  }
	  else 
	  {
	     cfg_super.pg175_System_Size = 3;    /* 5kg */
	     cfgBWH.set_batch_size = 10.0; //G2-399
        cfgBWH.max_batch_size = 11.5; //cfgBWH.set_batch_size * 1.10;
        cfgBWH.min_batch_size = 5.0;  //cfgBWH.set_batch_size * 0.50;
        cfg_super.pg2180_aux5_invert = 1; 
	  }
	
     cfgBWH.max_coast_time = 10 * TICKSPERSEC;
     cfgBWH.min_bwh_dump_time = 5 * TICKSPERSEC; //G2-405
     cfgBWH.ad_filter_table = 1;   /* was 0(99) but 1(58) has a better response curve GTF */
     cfgBWH.ad_loop_timer_factor = 2;
     cfgBWH.batch_oversize = 0.10;   /* 10 % */
     cfgBWH.autozero_on = TRUE;      /* enable auto zero function */
     cfgBWH.hop_wt_rezero_thres = 0.15; //G2-405 
	  cfgBWH.load_cell_tolerance = 2000; 
	  cfgBWH.check_zero_wt_flag  = FALSE;		 /* enable BWH calibration check */
	
	  // test per Scott
	  cfgGATE[loop].firstshot_percentage_accuracy  = 0.8;
	  cfgGATE[loop].secondshot_percentage_accuracy = 0.85;
	  cfgGATE[loop].thirdshot_percentage_accuracy  = 0.9;
     cfgGATE[loop].fourthshot_percentage_accuracy = 0.95;
     
     WeightDeviationThreshold[0] = 12.0/453.59;      /* 12gm default per Tim Peyton */
     WeightDeviationThreshold[1] = 12.0/453.59;      /* 12gm default per Tim Peyton */
     WeightDeviationThreshold[2] = 12.0/453.59;      /* 12gm default per Tim Peyton */
     WeightDeviationThreshold[3] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[4] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[5] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[6] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[7] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[8] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[9] = 12.0/453.59;      /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[10] = 12.0/453.59;     /* 12gm default per Tim Peyton */
	  WeightDeviationThreshold[11] = 12.0/453.59;     /* 12gm default per Tim Peyton */
	
	  MaxAllowedWeightDeviations[0] = 5;         /* no default given           */
     MaxAllowedWeightDeviations[1] = 5;         /* no default given           */
     MaxAllowedWeightDeviations[2] = 5;         /* no default given           */
     MaxAllowedWeightDeviations[3] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[4] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[5] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[6] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[7] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[8] = 5;         /* no default given           */
  	  MaxAllowedWeightDeviations[9] = 5;         /* no default given           */
	  MaxAllowedWeightDeviations[10] = 5;        /* no default given           */
     MaxAllowedWeightDeviations[11] = 5;        /* no default given           */
  }
  /************************************************************************/
  /*
     Function:  default_gate(loop)

     Description:  defaults gate settings
                                                                          */
  /************************************************************************/
  void default_gate(int loop)
  {
     cfgGATE[loop].primary_gate.addr = X20;
     cfgGATE[loop].creceiver.prox_primary.addr = X20;
     cfgGATE[loop].creceiver.out_primary.addr = X20;
     cfgGATE[loop].primary_gate.addr = X20;
     cfgGATE[loop].primary_gate.chan = loop + GATE_A_OUT;               /* 21 to 32 */
     cfgGATE[loop].creceiver.prox_primary.chan = loop + LOAD_A_IN;      /* 37 to 48 */
     cfgGATE[loop].creceiver.out_primary.chan = loop + LOAD_A_OUT;      /* 55 to 66 */
     cfgGATE[loop].secondary_gate_enabled = FALSE;
     cfgGATE[loop].secondary_gate.addr = X20;                           /* NO DEFAULT DEFINED */
     cfgGATE[loop].secondary_gate.chan = loop + GATE_A_OUT;             /* NO DEFAULT DEFINED 21 to 32*/
     cfgGATE[loop].out_secondary.chan = loop + LOAD_A_OUT; 			      /* NO DEFAULT DEFINED 55 to 66*/
     cfgGATE[loop].time_wt_resolution = TIME_WT_RESOLUTION;             /* in mS this should exceed cycle time on TC 1 (opengate) */
     cfgGATE[loop].hopper_type = NORMAL;
     cfgGATE[loop].algorithm = STD_ALGORITHM;
     cfgGATE[loop].out_of_spec_limit = 0.005;
     cfgGATE[loop].target_accuracy = 0.0005;   /* .05% percent */
     cfgGATE[loop].alarm_factor = 0;
     cfgGATE[loop].auger_used = FALSE;
	  cfgGATE[loop].firstshot_percentage = 0.92;
	  cfgGATE[loop].firstshot_percentage_accuracy = 0.8;
	  cfgGATE[loop].allowdump_proxoff = 0;       //G2-284 was 1
	  cfgGATE[loop].OOM_Alarm_Time = 30000;      //30s
	 
	  if (cfgGATE[loop].hopper_type == NORMAL)        /* Autobatch or Guardian uses different minimum gate time */
	  	  cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME;
	  else if (cfgGATE[loop].hopper_type == MEDIUM)
	     cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_MEDIUM;
	  else if  (cfgGATE[loop].hopper_type == SMALL) 
		  cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_SMALL;
     else if (cfgGATE[loop].hopper_type == REGRIND)
	     cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_REGRIND;
	  else if (cfgGATE[loop].hopper_type == STANDARD_MAIN)  
		 cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_STANDARD;
	  else if (cfgGATE[loop].hopper_type == SCR_MEDIUM)
	     cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME;
	  else if (cfgGATE[loop].hopper_type == SCR_SMALL)
	     cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME; 
	 
	  if ((Blender_Type == GUARDIAN) || (loop == 0))
	     cfgGATE[loop].weight_settle_time = 1000;              /* 1000ms */
	  else 
	     cfgGATE[loop].weight_settle_time = 2000;              /* 2000ms */
	 
	  cfgGATE[loop].min_gate_time_index = 0;
     cfgGATE[loop].tuning_method = TUNE_MANUAL;
     cfgGATE[loop].clear_recipe_on_crit_low = FALSE;
	  cfgGATE[loop].ucRegrind3ProxEnabled = FALSE;
	  cfgGATE[loop].use_powder_receiver = FALSE;
     cfgGATE[loop].creceiver.fill_time = 30;
     cfgGATE[loop].creceiver.dump_time = 10;
     cfgGATE[loop].creceiver.enabled = FALSE;    //G2-401
     IO_dig[0].channel[LOAD_A_OUT+loop] = FALSE; //G2-401
     cfgGATE[loop].gate_pulse_number = 0;
     cfgGATE[loop].num_bad_feeds_before_alarm = 3; // max is 500 now from HMI
	  cfgGATE[loop].num_before_dump_low = 3; // max is 500 now from HMI
	  cfgGATE[loop].num_no_feeds_before_out_of_mat = 3;
     cfgGATE[loop].clear_gate_cal_after_out_of_spec = FALSE;
     cfgGATE[loop].recipe_use = 0;
	  cfgGATE[loop].step_size_zc = 0;
	  cfgGATE[loop].proximity_switch.chan = 115+loop;
	  cfgGATE[loop].PreFeed_Enabled = FALSE;
		
	  if (Blender_Type == AUTOBATCH) 
	  {
	     cfgGATE[loop].proximityswitch_exist = TRUE;
		  cfgGATE[loop].prepulse_enabled = FALSE;
       	
		  if (loop == 0)
			  cfgGATE[loop].hopper_type = STANDARD_MAIN;
		  else
		     cfgGATE[loop].hopper_type = MEDIUM;
     }
	  else
	  {
		  cfgGATE[loop].proximityswitch_exist = FALSE;
        cfgGATE[loop].prepulse_enabled = TRUE;
	  }
	 
	  cfgGATE[loop].wt_sz_mux_1 = 1; 
	  cfgGATE[loop].wt_sz_mux_2 = 1; 
	  cfgGATE[loop].wt_sz_mux_3 = 10; 
     cfgGATE[loop].wt_sz_mux_4 = 20; 
  }


  /************************************************************************/
  /*
     Function:  default_mix()

     Description:  defaults mixer settings
                                                                          */
  /************************************************************************/
  void default_mix(int loop)
  {
	  if (Blender_Type == AUTOBATCH)
  	  {
		  cfgMIXER.dump_cycle_time = 60.0;
		  cfgMIXER.gate_close_delay = 0;         /* in ms */ 
		  cfgMIXER.bypass_mixer_prox_switch = TRUE;
		  cfgMIXER.material_request_source = MATERIAL_REQUEST_EXTERNAL;
		  cfgMIXER.mixer_gate_mode_autobatch = 0;
	  }
	  else 
	  {
		  cfgMIXER.dump_cycle_time = 15.0;
		  cfgMIXER.gate_close_delay = 0;
		  cfgMIXER.material_request_source = MATERIAL_REQUEST_MIXER_PROX;
		  cfgMIXER.bypass_mixer_prox_switch = FALSE;
		  cfgMIXER.mixer_gate_mode_autobatch = 0;
		  cfgMIXER.keep_mixer_empty_ext_prox = 0;
	  }

	  cfgMIXER.demo = FALSE;
     cfgMIXER.mix_time = 10000; /* time is mS GTF */
     cfgMIXER.max_dump_time = 20.0;
     cfgMIXER.mixer_low_level_time = 0.2;
     cfgMIXER.demo_max_wtp = 2000.0;
     /* Rich ALmeada requested we change this to TRUE GTF */
     cfgMIXER.aux_starter_used = TRUE;
     cfgMIXER.aux_starter_input_steady = TRUE;
     cfgMIXER.aux_alarm_time  = 4;  // zcl [06/08/16] - 4 secs default, not 60 /* 60 seconds */

     cfgMIXER.UncoveredMatReqSwitchAlarmTime = 450;
	  cfgMIXER.material_request_invert = FALSE;
  }

  /************************************************************************/
  /*
     Function:  default_liw(int loop)

     Description:  defaults liw settings ,extruder, gravifluff
                                                                          */
  /************************************************************************/
  void default_liw(int loop)
  {
     unsigned char wm_address;
     unsigned char exd_address;

     config_LIW_device *cnf;

     if (loop == EXT_LOOP)
     {
        wm_address = 16;
        exd_address = 32;
        cnf = &cfgEXT;
     }
     else
     {
        wm_address = 17;
        exd_address = 33;
        cnf= &cfgGFEEDER;
     }

     /* set default network addresses */
     cnf->drive_type = PCC_BR_AO;
     cnf->weigh_mod.addr = wm_address;
     cnf->weigh_mod.chan = 0;
     cnf->drive_mod.addr = exd_address;
     cnf->drive_mod.chan = 0;
     cnf->load_relay.addr = wm_address;
     cnf->load_relay.chan = 18;
     cnf->alarm_relay.addr = wm_address;
     cnf->alarm_relay.chan = 2;
     cnf->drive_inhibit_invert = 0;
     cnf->receiver.type = 0;
     cnf->receiver.clean_time = 50;            /* units are 0.1 sec    */
     cnf->receiver.dump_time = 100;            /* units are 0.1 sec    */
     cnf->receiver.fill_time = 100;            /* units are 0.1 sec    */
     cnf->receiver.priority = 1;

     /* set rate speed info */
     cnf->algorithm = STATISTIC;
     cnf->movepts = 5;
     cnf->rs_clip = FALSE;
     //cnf->rs_clip = TRUE;
     cnf->rs_maxchg = 0.05;
     cnf->rs_resetchg = 0.25;
     cnf->zero_crossing = 0.0;
     /*   cnf->rs_offset = 0.0; */

     /* set control defaults for running */
     cnf->calctime = 10 * TICKSPERSEC;
     cnf->dead_band = 0.0025;
     cnf->max_error = 0.25;
     cnf->max_bad = 3;
     cnf->coast_tolerance = 400; //G2-514 
     cnf->coast_error = 4.0;     //G2-514
     cnf->initial_coast = 5;     //G2-514
     cnf->max_coast_time = 30;
     cnf->max_drive_fail = 20;
     cnf->drive_fail_error = 1;
     cnf->accel = 0.02;
     cnf->decel = 0.02;
     cnf->out_of_spec_limit = 0.02;
     cnf->ad_filter = 0.20;
     cnf->cal_done = FALSE;
     cnf->demo_max_wtp = 500.0;
     cnf->ad_filter_table = 2;
     cnf->enable_multi_rs_factor = 0; 

     /* set default load & alarm wts       */
     cnf->load_wt_on = 10.0;
     cnf->load_wt_off = 0.0;
     cnf->alarm_wt = 5.0;
     cnf->high_alarm_wt = 70;
     cnf->crit_wt = 1.0;
     cnf->min_dump_wt = 4.0;
     cnf->warning_high_spd = 0.98; //G2-514      /* recipe warning speed */
     cnf->warning_low_spd  = 0.04; //G2-514
     cnf->alarm_high_spd = 0.999;  //G2-514      /* alarm speeds         */
     cnf->alarm_low_spd  = 0.02;   //G2-514
     cnf->max_spd = 1.0;
     cnf->spd_factor = 100.0;
     cnf->use_secondary_weight_card = FALSE;
     cnf->use_internal_loading_signal_only = TRUE;

     /* default ad parameters */
     cnf->skip = FALSE;
     cnf->fast = FALSE;
     cnf->ac = TRUE;
     cnf->chop = TRUE;
     cnf->delay = 15;
     cnf->sec_ad_offset = 0x800000;
     cnf->sec_ad_gain = 0x59AEE7;

     cnf->drive_ip_address[0]= 100;
     cnf->drive_ip_address[1]= 2;
     cnf->drive_ip_address[2]= 168;
     cnf->drive_ip_address[3]= 192;
  }

  /************************************************************************/
  /*
     Function:  default_ho(loop)

     Description:  defaults haul off settings
                                                                          */
  /************************************************************************/
void default_ho(int loop)
{
	config_HO_device *cnf = &cfgHO;
	int i;

	/* set default network addresses */
	cnf->drive_type = PCC_BR_AO;
	cnf->drive_mod.addr = 0x2F;
	cnf->drive_mod.chan = 1;
	cnf->drive_inhibit_invert = 0;
	
	/* set default calibration */
	cnf->nip_circumference = 0.178;
	cnf->pulses_per_rev = 1000.0;
	cnf->lenperbit = cnf->nip_circumference / cnf->pulses_per_rev;
	cnf->cal_done = TRUE;
	
	/* set rate speed info */
	cnf->movepts = 5;
	cnf->rs_clip = TRUE;
	cnf->rs_maxchg = 0.05;
	cnf->rs_num = 0;
	cnf->demo_max_ltp = 100.0;

	/* set control defaults for running */
	cnf->calctime = 10 * TICKSPERSEC;
	cnf->dead_band = 0.0025;
	cnf->max_error = 0.25;
	cnf->max_bad = 3;
	cnf->coast_tolerance = 400; //G2-514
	cnf->coast_error = 4.0;     //G2-514
	cnf->initial_coast = 5;     //G2-514
	cnf->max_coast_time = 30;
	cnf->max_drive_fail = 20;
	cnf->drive_fail_error = 1;
	cnf->accel = 0.02;
	cnf->decel = 0.02;
	cnf->out_of_spec_limit = 0.02;
	cnf->warning_high_spd = 0.98;  //G2-514     /* recipe warning speed */
	cnf->warning_low_spd  = 0.04;  //G2-514
	cnf->alarm_high_spd = 0.999;         /* alarm speeds         */
	cnf->alarm_low_spd = 0.03;
	cnf->max_spd = 1;		 /* default to be 100% of full spd */
	cnf->GearInputMethod = NO_GEAR_INPUT;
     
	cnf->drive_ip_address[0]= 100;
	cnf->drive_ip_address[1]= 2;
	cnf->drive_ip_address[2]= 168;
	cnf->drive_ip_address[3]= 192;
     
	for (i=0; i<MAX_GEARS; i++)
		cnf->spd_factor[i] = 100.0;
}

  /************************************************************************/
  /*
     Function:  default_sho(loop)

     Description:  defaults secondary haul off settings
                                                                          */
  /************************************************************************/
  void default_sho(int loop)
  {
     config_HO_device *cnf = &cfgSHO;
	  int i;

	/* set default network addresses */
	cnf->drive_type = PCC_BR_AO;
	cnf->drive_mod.addr = 0x2F;
	cnf->drive_mod.chan = 1;
	cnf->drive_inhibit_invert = 0;
	
	/* set default calibration */
	cnf->nip_circumference = 0.178;
	cnf->pulses_per_rev = 1000.0;
	cnf->lenperbit = cnf->nip_circumference / cnf->pulses_per_rev;
	cnf->cal_done = TRUE;
	
	/* set rate speed info */
	cnf->movepts = 5;
	cnf->rs_clip = TRUE;
	cnf->rs_maxchg = 0.05;
	cnf->rs_num = 0;
	cnf->demo_max_ltp = 100.0;

	/* set control defaults for running */
	cnf->calctime = 10 * TICKSPERSEC;
	cnf->dead_band = 0.0025;
	cnf->max_error = 0.25;
	cnf->max_bad = 3;
	cnf->coast_tolerance = 400; //G2-514
	cnf->coast_error = 4.0;     //G2-514
	cnf->initial_coast = 5;     //G2-514
	cnf->max_coast_time = 30;
	cnf->max_drive_fail = 20;
	cnf->drive_fail_error = 1;
	cnf->accel = 0.02;
	cnf->decel = 0.02;
	cnf->out_of_spec_limit = 0.02;
	cnf->warning_high_spd = 0.98;  //G2-514     /* recipe warning speed */
	cnf->warning_low_spd  = 0.04;  //G2-514
	cnf->alarm_high_spd = 0.999;         /* alarm speeds         */
	cnf->alarm_low_spd = 0.03;
	cnf->max_spd = 1;		 /* default to be 100% of full spd */
	cnf->GearInputMethod = NO_GEAR_INPUT;
     
	cnf->drive_ip_address[0]= 100;
	cnf->drive_ip_address[1]= 2;
	cnf->drive_ip_address[2]= 168;
	cnf->drive_ip_address[3]= 192;
     
	for (i=0; i<MAX_GEARS; i++)
		cnf->spd_factor[i] = 100.0;
  }

  /************************************************************************/
  /*
     Function:  default_wth(loop)

     Description:  defaults width settings
                                                                          */
  /************************************************************************/
 void default_wth(int loop)
 {
     config_WTH_device *cnf = &cfgWTH;
     int i;

     cnf->mode = 1;
     cnf->alarm_wth_min = WIDTH_MIN;
     cnf->alarm_wth_max = WIDTH_MAX;
     cnf->out_of_spec_limit = 0.02;

     /* for DR Joseph Ethernet */
     cnf->dr_joseph_ethernet = FALSE;
     cnf->dr_joseph_ethernet_port = (unsigned short)502;
     for (i=0; i<4; i++)
     {
        cnf->dr_joseph_ip_address[i] = 0;
     }
}

   /************************************************************************/
   /*
      Function:  default_grf(loop)

      Description:  defaults gravifluff settings
                                                                           */
   /************************************************************************/
   void default_grf(int loop)
   {
     config_LIW_device *cnf = &cfgGFLUFF;

     /* set default network addresses */
     cnf->drive_type = PCC;
     cnf->weigh_mod.addr = 18;
     cnf->weigh_mod.chan = 0;
     cnf->load_relay.addr = 18;
     cnf->load_relay.chan = 2;
     cnf->alarm_relay.addr = 18;
     cnf->alarm_relay.chan = 1;

     /* set rate speed info */
     cnf->movepts = 5;
     cnf->rs_clip = TRUE;
     cnf->rs_maxchg = 0.05;

     /* set control defaults for running */
     cnf->calctime = 60 * TICKSPERSEC;
     cnf->dead_band = 0.0025;
     cnf->max_error = 0.25;
     cnf->max_bad = 3;
     cnf->coast_tolerance = 20;
     cnf->coast_error = 4.0;  //G2-514
     cnf->initial_coast = 5;
     cnf->max_coast_time = 30 ;
     cnf->max_drive_fail = 20;
     cnf->drive_fail_error = 1;
     cnf->accel = 0.02;
     cnf->decel = 0.02;
     cnf->out_of_spec_limit = 0.02;

     /* set default load & alarm wts       */
     cnf->load_wt_on = 10.0;
     cnf->load_wt_off = 20.0;
     cnf->alarm_wt = 5.0;
     cnf->crit_wt = 1.0;
     cnf->min_dump_wt = 4.0;
     cnf->warning_high_spd = 0.98;        /* recipe warning speed */
     cnf->warning_low_spd = 0.04;
     cnf->alarm_high_spd = 0.999;         /* alarm speeds         */
     cnf->alarm_low_spd = 0.02;
     cnf->max_spd = 1.0;
     cnf->spd_factor = 1.0;
  }

  /************************************************************************/
  /*
     Function:  words_per_recipe(loop)

     Description:  calculates the size of recipe
                                                                          */
  /************************************************************************/
  int words_per_recipe(config_super *cs_)
  {
     int total;

     total = 1;                            /* used flag                    */

     if (((cfg_super.wtp_mode == CONTROL) || (cfg_super.ltp_mode == CONTROL)) && (cfg_super.layers_by == FALSE))
         total++;                            /* first value                  */
     if (cfg_super.recipe_mode[1] != NONE)       /* need secondary value         */
         total++;
     if (cfg_super.recipe_mode[2] != NONE)       /* need third value             */
         total++;

     if (cfg_super.density_in_recipe)            /* density.s's  */
     {
         total += cfg_super.total_gates;
     }

#if 0
      if ((cfg_super.wtp_mode == CONTROL) && cfg_super.refeed)
          total += cfg_super.num_ext;               /* scrap percentages            */
#endif

     total++;                          /* parts.t      */
     if (cfg_super.total_gates)
     {
        total += cfg_super.total_gates;    /* parts.s's    */
        if (cfg_super.scrap)
           total++;
     }
     else if (cfg_super.scrap)            /* 2 parts.s's  */
        total+= 2;

     if (cfg_super.refeed == GRAVIFLUFF)
        total++;

     if (cfg_super.mix_time_in_recipe)
        total++;
     if (cfg_super.job_wt_in_recipe) 
        total++;

     if (cfg_super.current_temp_recipe_num > 0)
     {
        cfg_super.recipe_inven_offset = total;    /* save the offset */
        total += 2;                         /* product inventory */
        total += PRODUCT_CODE_LEN/4;        /* product code */
     }
     if(cfg_super.enable_resin_codes)
     {
        total += cfg_super.total_gates;        /* resin codes */
     }
     if (cfg_super.gate_order_in_recipe)
        total += cfg_super.total_gates;        /* gate order */

     return total;
  }

  /************************************************************************/
  /*
     Function:  BuildFileReference()

     Description: This function will build the file entry in the file list.

     Returns:  VOID
                                                                          */
  /************************************************************************/
void BuildFileReference(char *Fname, int FileIndex)
{
   brsstrcpy((UDINT)FileList[FileIndex].Filename, (UDINT)Fname);
   FileList[FileIndex].bRead = FALSE;
   FileList[FileIndex].bWrite = FALSE;
   FileList[FileIndex].NumBlocks = 0;
}
/************************************************************************/
/*
   Function:  default_recipes()

   Description:  defaults the recipe structure
                                                                        */
/************************************************************************/
void default_recipes()
{
   int i;

   EMPTY_RECIPE->used  = FALSE;
   EMPTY_RECIPE->ho_gear_num = 0;
   EMPTY_RECIPE->wpl   = 0.0;
   EMPTY_RECIPE->thk   = 0.0;
   EMPTY_RECIPE->ltp   = 0.0;
   EMPTY_RECIPE->width = 0.0;
   EMPTY_RECIPE->id    = 0.0;
   EMPTY_RECIPE->od    = 0.0;
   EMPTY_RECIPE->ea    = 0.0;
   EMPTY_RECIPE->avg_density = 0.0;
   EMPTY_RECIPE->total_parts = 0.0;
   EMPTY_RECIPE->ho_man_spd  = 0.0;
   EMPTY_RECIPE->ext_man_spd = 0.0;
   EMPTY_RECIPE->refeed_man_spd = 0.0;
   EMPTY_RECIPE->total_wtp   = 0.0;
   EMPTY_RECIPE->refeed_wtp  = 0.0;
   EMPTY_RECIPE->max_scrap_parts = 0.0;
   EMPTY_RECIPE->mix_time    = 0; /* time is mS GTF */
   EMPTY_RECIPE->job_wt      = 0;
   EMPTY_RECIPE->parts.t     = 0;
   EMPTY_RECIPE->parts.ts    = 0;
   EMPTY_RECIPE->parts.ext   = 0;
   EMPTY_RECIPE->parts_pct.t = 0;
   EMPTY_RECIPE->parts_pct.ts = 0;
   EMPTY_RECIPE->parts_pct.ext = 0;
   EMPTY_RECIPE->density.t   = 0;
   EMPTY_RECIPE->density.ts  = 0;
   EMPTY_RECIPE->density.ext = 0;

   for (i=0; i<MAX_GATES; i++)
   {
      EMPTY_RECIPE->parts.s[i]     = 0;
      EMPTY_RECIPE->parts_pct.s[i] = 0;
      EMPTY_RECIPE->density.s[i]   = 0;
      EMPTY_RECIPE->resin_recipe_num[i] = 0;
      EMPTY_RECIPE->gate_order[i].number = i;
   }
   EMPTY_RECIPE->wpa         = 0;
   EMPTY_RECIPE->step_size   = 0;
   EMPTY_RECIPE->recipe_inven_wt = 0;
   EMPTY_RECIPE->stretch_factor = 0;
   brsmemcpy((UDINT)EMPTY_RECIPE->product_code, (UDINT)" ", (UDINT)PRODUCT_CODELEN);
   copy_recipe(&UI_RECIPE, EMPTY_RECIPE, BLENDER_RECIPE);
   for (i=0; i<MAX_RECIPES; i++)
   {
      copy_recipe(&RECIPES->entry[i], EMPTY_RECIPE, BLENDER_RECIPE);
   }
}


/************************************************************************/
/*
   Function:  visible()

   Description:  modifies the passed runtime variable to either show or
        hide the object the runtime variable is assigned to.
                                                                        */
/************************************************************************/
void visible(unsigned char *Runtime, unsigned char state)
{
   if (state == TRUE)
      *Runtime &= ~gcVisibleBit;    /* shown */
   else
      *Runtime |= gcVisibleBit;     /* hidden */
}
