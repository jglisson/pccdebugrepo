#ifndef _BG_DEBUG_H
#define _BG_DEBUG_H

#ifdef WIN32
#define DBG printf
#define DBGMSG printf


#define DBGCRT printf
#define DBGERR printf
#define DBGWAR printf
#define DBGINF printf
#define DBGDBG printf

#else

#include <bur/plc.h>

/*
Public functions:
-----------------
*/

#define BG_DEBUG_FORMAT_FILENAME	0x1
#define BG_DEBUG_FORMAT_FUNCTION	0x2
#define BG_DEBUG_FORMAT_TIMESTAMP	0x4
#define BG_DEBUG_FORMAT_LEVEL		0x8
#define BG_DEBUG_FORMAT_LINENUMBER	0x16


/*
Public data types:
-----------------
*/
typedef struct bgRingBuffer_typ{

    int     bufSize;
    int     init;
    int     overrun;
    unsigned int     dbgLevel;
    int     dbgFormat;

    char    *ptrCircularBufferBegin;
    char    *ptrCircularBufferEnd;

    char    *ptrDataIn;
    char    *ptrDataOut;

}bgRingBuffer_typ;

typedef struct {
  unsigned char ucBuffer[256];
  unsigned char ucUsed;
} DBG_MSG_QUEUE;

#define MAX_DBG_QUEUE_MSGS  8
_GLOBAL DBG_MSG_QUEUE DbgQueue[MAX_DBG_QUEUE_MSGS];

/*
- debug specific function for entering data into the ring buffer
*/
void bgDbgInfo(bgRingBuffer_typ *rb, const char *file, const char *function, unsigned int level, unsigned int context, const char *format, ...);

/*
- put formated message (i.e. printf) into the ring buffer
*/
void bgDbgPrintf(bgRingBuffer_typ *rb, char *format, ...);


#define BG_DEBUG_LEVEL_CRITICAL		0
#define BG_DEBUG_LEVEL_ERROR		1
#define BG_DEBUG_LEVEL_WARNING		2
#define BG_DEBUG_LEVEL_INFO			3
#define BG_DEBUG_LEVEL_DEBUG		4

#define BG_DEBUG_UNUSED	0x0


/*
- To completly remove debug msgs from the source code use:
#define BG_DEBUG_ENABLE 0
*/
#define BG_DEBUG_ENABLE 1


#if BG_DEBUG_ENABLE == 1
/*BG_DEBUG_ENABLE == 1 => debug messages present in the source code*/
#define DBG(gRingBuffer, format, args...)		bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_CRITICAL, BG_DEBUG_UNUSED, format, ## args);
#define DBGMSG(gRingBuffer, format, args...)	bgDbgPrintf(gRingBuffer, format, ## args);


#define DBGCRT(gRingBuffer, format, args...)	bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_CRITICAL, BG_DEBUG_UNUSED, format, ## args);
#define DBGERR(gRingBuffer, format, args...)	bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_ERROR, BG_DEBUG_UNUSED, format, ## args);
#define DBGWAR(gRingBuffer, format, args...)	bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_WARNING, BG_DEBUG_UNUSED, format, ## args);
#define DBGINF(gRingBuffer, format, args...)	bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_INFO, BG_DEBUG_UNUSED, format, ## args);
#define DBGDBG(gRingBuffer, format, args...)	bgDbgInfo(gRingBuffer, __FILE__, __FUNCTION__, BG_DEBUG_LEVEL_DEBUG, BG_DEBUG_UNUSED, format, ## args);


#else
/*BG_DEBUG_ENABLE == 0 => all debug messages removed from the source code*/
#define DBG(gRingBuffer, format, args...)  ;
#define DBGMSG(gRingBuffer, format, args...)  ;

#define DBGCRT(gRingBuffer, format, args...)	;
#define DBGERR(gRingBuffer, format, args...)	;
#define DBGWAR(gRingBuffer, format, args...)	;
#define DBGINF(gRingBuffer, format, args...)	;
#define DBGDBG(gRingBuffer, format, args...)	;


#endif /*#if BG_DEBUG_ENABLE*/

#endif /*#ifdef WIN32*/

#endif /*#ifndef _BG_DEBUG_H*/



