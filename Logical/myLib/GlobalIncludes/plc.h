/************************************************************************/
/*
   File:  plc.h

   Description: This file defines the PLC i/o rack modules. Each Module
      type has a structure which is used to control the i/o of the module
      as well as the status of the module itself.


      $Log:   F:\Software\BR_Guardian\plc.h_v  $
 *
 *    Rev 1.6   Jul 17 2008 16:38:56   gtf
 * Added support for 4PP045.0571-K15 PCC specific panel version
 *
 *    Rev 1.06   July 17,  2008 15:05:34   GTF
 * Added support for 4PP045.0571-K15 PCC version of B&R Panel
 *    Rev 1.5   Mar 07 2008 10:31:16   FMK
 * Added define for pcc custom module. Have not
 * as yet recieved proper value yet but code in
 * place for when value does arrive.
 *
 *    Rev 1.4   Feb 27 2008 14:34:30   FMK
 * Added a module for 4 dry contact outputs. This
 * is used for switching the pot/internal output
 * to the external drive.
 *
 *    Rev 1.3   Feb 04 2008 13:19:48   FMK
 * Fixed variable declaration for analog output module.
 *
 *    Rev 1.2   Feb 04 2008 09:43:30   FMK
 * Corrected variable names for analog input
 * module 0-10v.
 *
 *    Rev 1.1   Jan 23 2008 13:17:20   FMK
 * Added structures for boards used in the EXD
 * and EXL systems
 *
 *    Rev 1.0   Jan 11 2008 10:33:38   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#ifndef PLC_H
#define PLC_H

#include <bur/plctypes.h>

#define ModID_BR9300       7105
#define ModID_4PP045       9498
#define ModID_4PP065       57733 //added DRT 1/12/16 
#define ModID_4PPC70       58752 //added DRT 3/2/18
         
/* changed modid value for ppc modules  GTF 07/17/2008*/
#define ModID_4PP045PCC    42409
#define ModID_AI1744       7390
#define ModID_AI1744_3     42223
#define ModID_DI9371       7061
#define ModID_DO9321       7067
#define ModID_DO9322       7066
#define ModID_D04529       8409
#define ModID_CS1030       8144
#define ModID_AO2632       7076
#define ModID_AI2632       7072
#define ModID_DI2377       7054


#define MaxDigOutModules   5

#define MaxDigInModules    4
#define MaxStrainModules   2

#define BWH_ADNum	0
#define EXT_ADNum	1

/* ModuleOK definition */
#define sts_notpresent  0
#define sts_present     1

/* IOchannels #defines GTF                                                   */
#define IOmaxaddress 2             /* Maximum Network network address for IO */
#define IOmaxchannel 150           /* Maximum number of IO channels          */
#define X20          0             /* x20 rach is element 0 of array */

struct Ethernet_IOMapping
{
   USINT NodeSwitch;
};

struct X20_IOMapping
{
   DINT CycleCount;
   DINT BreakCount;
   DINT SyncErrorCount;
   DINT SyncSramErrorCount;
   DINT SyncBusyErrorCount;
   DINT SyncNoRxErrorCount;
   DINT SyncFormatErrorCount;
   DINT SyncPendingErrorCount;
   DINT AsyncErrorCount;
   DINT AsyncSramErrorCount;
   DINT AsyncBusyErrorCount;
   DINT AsyncNoRxErrorCount;
   DINT AsyncFormatErrorCount;
   DINT AsyncPendingErrorCount;
};

struct X20ModuleInfo
{
   UDINT SerialNum;
   UINT  ModuleID;
   UINT  HardwareVariant;
   UINT  FirmwareVersion;
};

struct PP045_Touch
{
   UDINT SerialNum;
   DINT  SystemTime;
   UINT  ModuleID;
   UINT  TemperatureCPU;
   UINT  TemperatureENV;
   USINT BatteryStatus;
   USINT spare;
};

struct x20ao2632
{
    struct X20ModuleInfo Info;
    BOOL ModuleOK;
    INT  AnalogOutput1;
    INT  AnalogOutput2;
};

struct X20AI2632Channel_Struct
{
    INT  Value;
    BOOL OK;
    BOOL Underflow;
    BOOL Overflow;
    BOOL OutOfRange;
};

struct x20ai2632
{
    struct X20ModuleInfo Info;
    BOOL ModuleOK;
    BOOL StaleData;
    BOOL SyncStatus;
    BOOL ConvertionCycle;
    struct X20AI2632Channel_Struct AnaInp[2];
};

struct x20di2377
{
    struct X20ModuleInfo Info;
    BOOL ModuleOK;
    BOOL StaleData;
 /*   BOOL DigInput[2]; */
    UINT CounterValue[2];
    BOOL CounterReset[2];
};

struct x20cs1030
{
   struct X20ModuleInfo Info;
   BOOL ModuleOK;
};


struct x20br9300
{
   struct X20ModuleInfo Info;
   BOOL  ModuleOK;
   USINT SupplyCurrent;
   USINT SupplyVoltage;
   BOOL  Status1;
   BOOL  Status2;
   BOOL  spare;
};

struct x20do9321
{
   struct X20ModuleInfo Info;

   BOOL ModuleOK;
   BOOL spare;
/*   BOOL Out[12];*/
   BOOL OutStatus[12];
};

struct x20do9322
{
   struct X20ModuleInfo Info;

   BOOL ModuleOK;
   BOOL spare;

/*   BOOL Out[12]; */

   BOOL OutStatus[12];
};

struct x20di9371
{
   struct X20ModuleInfo Info;

   BOOL ModuleOK;
   BOOL spare;

/*   BOOL In[12]; */
};

struct x20do4529
{
   struct X20ModuleInfo Info;

   BOOL ModuleOK;
   BOOL spare;

   BOOL Out[4];
};

/* module status bit definitions for AI1744 */
#define sts_good     0
#define sts_busy     1
#define sts_broken   2
struct x20ai1744
{
   DINT  AnalogInput01;
   struct X20ModuleInfo Info;
   USINT Status01;
   USINT ConfigOut01;
   BOOL  ModuleOK;
   BOOL  spare;
};

struct PLC_Map
{
   struct Ethernet_IOMapping Ethernet_IOMap;
   struct X20_IOMapping X20_IOMap;
   struct PP045_Touch TouchPanel;
   struct x20br9300 BusRecvr;
   struct x20do9322 DigOut[MaxDigOutModules];
   struct x20do4529 RelayOut;
   struct x20di9371 DigIn[MaxDigInModules];
   struct x20ai1744 StrainGauge[MaxStrainModules];
   struct x20cs1030 Comm422Module;
   struct x20ao2632 AnaOut;
   struct x20ai2632 AnaIn;
   struct x20di2377 PulseCounter;
};

_GLOBAL struct PLC_Map PLC;

/* IOchannels #defines GTF                                                   */
struct IOchan_Map
{
	BOOL channel[IOmaxchannel];
};

_GLOBAL struct IOchan_Map IO_dig[IOmaxaddress]; /* array for mapping IO to hardware GTF*/

#endif

