#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  ab_com.c

   Description: common functions for Allen-Bradley (Serial and EtherNet).


      $Log:   F:\Software\BR_Guardian\ab_com.c_v  $
 *
 * Apr 12, 2011 BEB
 *  // if user wants PCC2 "6400001" in a UCB, user inputs "17001", and we will map it here.
 *  // if user wants PCC2 "96001" in a UCB, user inputs "18001", and we will map it here.
 *  // if user wants PCC2 "97001" in a UCB, user inputs "19001", and we will map it here.
 *
 *
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.abCom to turn on/off the debug in this file
 *
 *    Rev 1.1   Apr 30 2008 14:00:30   YZS
 * Fixed compile warnings.
 *
 *    Rev 1.0   Apr 29 2008 14:34:42   YZS
 * Initial revision.
 *
 *

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include "comminc.h"

#include <stdio.h>
#include <string.h>
#include "sys.h"
#include "mem.h"
#include "register.h"
#include "abdhp.h"
#include "remote.h"
#include "libbgdbg.h"

/* functions defined in abdhp_tx.c */
extern void debug_rem_write(point_data * pd, int point, fic * value, int type);
extern void debug_rem_read(point_data * pd, int point, fic * value, int type);

extern void read_register_data(unsigned int,int);
extern void read_pt_into_data(unsigned int);
extern void write_register_data(unsigned int RegPoint,int RegSpan,unsigned char DataType);
int  write_data_point(unsigned int,unsigned char);
extern int get_plc2_element_size(int,int);
extern unsigned int adjust_plc2_address(unsigned int);
extern unsigned int decode_plc5_address(unsigned int *file);
unsigned int plc5_to_pcc2(unsigned int file, unsigned int element, unsigned int sub_element);
extern int  get_plc5_write_type(unsigned char *,int *);
extern unsigned char get_plc5_read_type(int,int);
extern int write_remote_var(point_data *, unsigned int , fic );
extern void read_horizontal_data(unsigned int RegPoint,int RegSpan,int ElementSize);
extern int write_remote_var_2(point_data *pd, unsigned int p,fic v);
extern int read_remote_var(point_data *pd, unsigned int p,fic *v,int *pt_type);
extern int read_remote_var_2(point_data *pd, unsigned int p,fic *v,int *type);

unsigned int findstr(char str[], char find_str[]);

extern config_super *cs_;
extern global_struct *g_;

extern unsigned char MsgIn[512];     /* msg received from master        */
extern unsigned char MsgOut[512];    /* reply message */
extern int MsgIndex;                 /* length of command message in bytes */
extern int MsgOutIndex;              /* current reply message index     */
int point_type;

#ifdef TRACE_ROUTINES
int func_num;
#endif

extern point_data pd;
extern struct port_diag_data_str *r_diag;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/*************************************************************************/
/* read_register_data - this function reads data from the internal regs. */
/*  It will also stuff data bytes into the MsgOut[] array                  */
/*************************************************************************/
void read_register_data(unsigned int RegPoint,int RegSpan)
{
int i;
config_super *cs_ = &cfg_super;

   /* loop to build outgoing data buffer */
   for (i=0; i<RegSpan; i++)
   {
      /* fetch value for this register */
      read_pt_into_data(RegPoint);

      if (RegPoint < 30000 && (!cs_->use_pcc2_address))
      {
         /* assemble next register point */
         if ((RegPoint % 100) == 99)                 /* last integer point */
            RegPoint = RegPoint - (99-R_INTEGER);  /* wrap to 1st integer */
         else if ((RegPoint % 100) == (R_INTEGER-1)) /* last floating pt */
            RegPoint = RegPoint - (R_INTEGER-2);    /* wrap to 1st float */
         else
            RegPoint++;
      }
      else
         RegPoint++;

   } /*endfor*/

}

/*************************************************************************/
/* read_pt_into_data - this function read the specified point from the   */
/*  internal registers and formats the result bytes into the MsgOut[] array*/
/*  It will also keep track of the byte count of message data            */
/*************************************************************************/
void read_pt_into_data (unsigned int RegPoint)
{
fic value;                     /* value read from reg point  */
unsigned char *val_bytes;     /* pointer to value bytes     */
unsigned char Function;
unsigned char Cmd;
int status;
UINT hold_sixteen_bit_mode;

config_super *cs_ = &cfg_super;
   Cmd = MsgIn[CMD];
   Function = MsgIn[FNC];

   /* set byte pointer to point to fic union */
   val_bytes = (unsigned char *)&value;

   /* fetch value for this register */
   if (cs_->use_pcc2_address)
      status = read_remote_var_2(&pd, RegPoint, (fic *) &value, &point_type);
   else
   {
      hold_sixteen_bit_mode = pd.sixteen_bit_mode;
      pd.sixteen_bit_mode = 1;
      status = read_remote_var(&pd, RegPoint,&value,&point_type);
      pd.sixteen_bit_mode = hold_sixteen_bit_mode;
   }

   if (status == INVALID_POINT)
   {
      if (point_type == FLOAT)
         value.f = 0.0;
      else
         value.i = 0;
   }

   debug_rem_read(&pd, RegPoint, &value, point_type);
   if (point_type == FLOAT)
   {
      HMI.pg212_fRegisterReturn = value.f;
      HMI.pg212_uiRegisterReturn = 0;
   }
   else
   {
      HMI.pg212_uiRegisterReturn = value.i;
      HMI.pg212_fRegisterReturn = 0.0;
   }

   /* place reg value bytes into outgoing data, note byte swapping */
   /* add upper bytes of "fi" union if floating point data only    */
   if (point_type == FLOAT)
   {
      if ((Cmd != EXTENDED_COMMAND) || ((Cmd == EXTENDED_COMMAND)
            && (Function != PLC5_TYPED_READ)))
      {
         MsgOut[(MsgOutIndex)++] = val_bytes[2];
         MsgOut[(MsgOutIndex)++] = val_bytes[3];
      }
   }

   /* add lower bytes to the data stream                           */
   MsgOut[(MsgOutIndex)++] = val_bytes[0];
   MsgOut[(MsgOutIndex)++] = val_bytes[1];

   /* place reg value bytes into outgoing data, note byte swapping */
   /* add upper bytes of "fi" union if floating point data only    */
   if ((point_type == FLOAT) && (Cmd == EXTENDED_COMMAND)
         && (Function == PLC5_TYPED_READ))
   {
      MsgOut[(MsgOutIndex)++] = val_bytes[2];
      MsgOut[(MsgOutIndex)++] = val_bytes[3];
   }
}

/*************************************************************************/
/* write_register_data - this function writes data from the input stream */
/*  of data bytes into the internal registers/points.  It will check for */
/*  changes to the recipe points and "re-punch" the AUTO button if in    */
/*  AUTO mode already                                                    */
/*************************************************************************/
void write_register_data(unsigned int RegPoint,int RegSpan,unsigned char DataType)
{
   config_super *cs_ = &cfg_super;
   int i;

   /* loop to get bytes from msg and write to registers */
   for (i=0; i<RegSpan; i++)
   {
      write_data_point(RegPoint, DataType);
      if (RegPoint < 30000 && (!cs_->use_pcc2_address))
      {
         /* assemble next register point */
         if ((RegPoint % 100) == 99)           /* if last bit mapped point   */
            RegPoint = RegPoint - 19;         /*   set to 1st bit mapped pt */
         else if ((RegPoint % 100) == 79)      /* else if last float point   */
            RegPoint = RegPoint - 78;         /*   set to 1st float point   */
         else                                   /* else                       */
            RegPoint++;                        /*   increment point by 1     */
      }
      else                                   /* else                       */
         RegPoint++;                        /*   increment point by 1     */

   } /*endfor */
}
/************************************************************************/
/* write_data_point - writes data into a register                       */
/*  data is pointed to by **val_bytes, this pointer is incremented after*/
/*  each write                                                          */
/************************************************************************/
int write_data_point(unsigned int RegPoint,unsigned char DataType)
{
   config_super *cs_ = &cfg_super;
	fic value;
	unsigned char Function;
	unsigned char Cmd;
	int retcode;

   Cmd = MsgIn[CMD];
   Function = MsgIn[FNC];

   /* arrange the msg data bytes into a float/int union */
   /* 4 bytes for floating point data, 2 bytes for int data */

   if ((Cmd == EXTENDED_COMMAND) && (Function == PLC5_TYPED_WRITE))
   {
      if ( DataType == FLOAT_TYPE )
      {
         value.i = (unsigned int)(MsgIn[MsgIndex]);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 1] << 8);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 2] << 16);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 3] << 24);
         MsgIndex += 4;
      }
      else  /* int data */
      {
         value.i =  (unsigned int)(MsgIn[MsgIndex]);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 1] << 8);
         // if user wants "6400001" in a UCB, user inputs "17001", and we will map it here.
         // if user wants "96001" in a UCB, user inputs "18001", and we will map it here.
         // if user wants "97001" in a UCB, user inputs "19001", and we will map it here.
         if ( ( value.i > 17000 ) && (value.i < 17305) )
         {
           value.i -= 17000;
           value.i += 6400000;
         }
         else if ( ( value.i > 18000 ) && (value.i < 18305) )
         {
           value.i -= 18000;
           value.i += 96000;
         }
         else if ( ( value.i > 19000 ) && (value.i < 19305) )
         {
           value.i -= 19000;
           value.i += 97000;
         }
         MsgIndex += 2;
      }
   }

   else if ((Cmd == EXTENDED_COMMAND) && (Function == WORD_RANGE_WRITE))
   {
      if ( DataType == FLOAT_TYPE )
      {
         value.i = (unsigned int)(MsgIn[MsgIndex + 2]);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 3] << 8);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 0] << 16);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 1] << 24);
         
         MsgIndex += 4;
      }
      else  /* int data */
      {
         value.i =  (unsigned int)(MsgIn[MsgIndex]);
         value.i |= (unsigned int)(MsgIn[MsgIndex + 1] << 8);
         // if user wants "6400001" in a UCB, user inputs "17001", and we will map it here.
         // if user wants "96001" in a UCB, user inputs "18001", and we will map it here.
         // if user wants "97001" in a UCB, user inputs "19001", and we will map it here.
         if ( ( value.i > 17000 ) && (value.i < 17305) )
         {
           value.i -= 17000;
           value.i += 6400000;
         }
         else if ( ( value.i > 18000 ) && (value.i < 18305) )
         {
           value.i -= 18000;
           value.i += 96000;
         }
         else if ( ( value.i > 19000 ) && (value.i < 19305) )
         {
           value.i -= 19000;
           value.i += 97000;
         }
         MsgIndex += 2;
      }
   }
   else if ( (RegPoint % 100) < R_INTEGER )
   {
      value.i =  (unsigned int)(MsgIn[MsgIndex + 2]);
      value.i |= (unsigned int)(MsgIn[MsgIndex+3] << 8);
      value.i |= (unsigned int)(MsgIn[MsgIndex]  << 16);
      value.i |= (unsigned int)(MsgIn[MsgIndex+1] << 24);
      MsgIndex += 4;
   }

   else  /* int data */
   {
      value.i =  (unsigned int)(MsgIn[MsgIndex]);
      value.i |= (unsigned int)(MsgIn[MsgIndex+1] << 8);
      MsgIndex += 2;
   }

   /* write data */
   if (cs_->use_pcc2_address)
      retcode = write_remote_var_2(&pd, RegPoint, value);
   else
      retcode = write_remote_var(&pd, RegPoint, value);

   if (DataType == FLOAT_TYPE)   
   {
      debug_rem_write(&pd, RegPoint, &value, FLOAT);
      HMI.pg212_fRegisterReturn = value.f;
      HMI.pg212_uiRegisterReturn = 0;
   }
   else
   {
      debug_rem_write(&pd, RegPoint, &value, INT);
      HMI.pg212_uiRegisterReturn = value.i;
      HMI.pg212_fRegisterReturn = 0.0;
   }
   return (retcode);

}
/*
*****************************************************************************
*                       FIND NUMBER OF BYTES PER ELEMENT
*****************************************************************************
*  Function:      get_plc2_element_size();
*  Input:         void
*  Output:        void
*  Description:   This function determines the size of each element (in bytes)
*                 for the current command.
*  Notes:
*
*****************************************************************************
*/
int get_plc2_element_size(int Point,int ReadWrite)
/*
    int Point;                 address of first point
    int ReadWrite;             flag for read or write message
*/
{
int ElementSize;

   /* check for horizontal reads of interger values */
   if (Point < 10000 && Point >= 9000 &&
         (Point % 100) > R_INTEGER && ReadWrite == READ)
         ElementSize = 4;

   else  /* vertical read or write, or horiz float write */
   {
      if ( (Point % 100) >= R_INTEGER )       /* if bitmapped data   */
         ElementSize = 2;                  /*   2 bytes per point */
      else                                        /* else                */
         ElementSize = 4;                  /*   4 bytes per point */

   } /* endelse vert read... */

   return ElementSize;
}

/**************************************************************************/
/* adjust_plc2_address - this processes the address field received by the */
/*   PROTECTED/UNPROTECTED READ,WRITE, commands and re-maps               */
/*   it into the internal PCC register numbering system as follows        */
/*                                                                        */
/*       - if address value received is doubled (BYTE addressing) it      */
/*         must be halved to convert to WORD addressing                   */
/*                                                                        */
/*       Received WORD value          Translates to:                      */
/*                                                                        */
/*         00000 - 10099               00000 - 10099                      */
/*         15000 - 15099               20000 - 20099                      */
/*                                                                        */
/**************************************************************************/
unsigned int adjust_plc2_address(unsigned int RegPoint)
{
   /* cut the register point value in half */
   /*   if (cs_->plc2_word_or_byte == PLC2_BYTE)*/

   RegPoint = RegPoint >> 1;

   /* check for system registers */
   if (RegPoint >= 15000 && RegPoint < 16000)
      RegPoint += 5000;

   /* check for user definable registers */
   else if (RegPoint >= 9100 && RegPoint < 10000)
      RegPoint += 30000 + (RegPoint % 9100);

   /*
   map addresses 16000->16099 to 00->99 to allow plc2 to read register
   points 0 -> 7.
   */
   else if (RegPoint >= 16000)
      RegPoint -= 16000;

   return RegPoint;
}

/* not sure why these aren't used */
/*lint -esym(550,sub_element,dta_table)*/
/*
*****************************************************************************
*                 GET PLC-5 ADDRESS
*****************************************************************************
*  Function:      decode_plc5_address();
*  Input:         Pointer to variable for file number.
*  Output:        1. The register point addressed is returned.
*                 2. The file number is set in the supplied pointer.
*  Description:   This function will decode the PLC-5 address supplied in
*                 the current message. It will decode logical and binary
*                 addresses
*
*  Notes:
*        It expects the plc5 following format for logical addressing:
*           Fabc:xx = Grav Reg pt (ab,cxx - 10000)
*
*****************************************************************************
*/
/*lint -esym(715, sub_element) */
unsigned int decode_plc5_address(unsigned int *file)
{
unsigned int RegPoint;              /* returns grav point              */
int Offset;

unsigned char *msg;          /* ptr to address field            */

int Index;                 /* returns # of bytes in the field */
int dta_table = 0;
unsigned int sub_element = 0;
unsigned int element;
int  delNdx;            /* index of separators in ascii addr */
char *value;            /* finds value in ascii addressing */
char *delimit;          /* separators in ascii addressing  */
char ascii_addr[20];    /* temp string for ascii decode    */
config_super *cs_ = &cfg_super;
char chr[10];

   msg = &MsgIn[PLC5_ADDR];
   /* check for logical ascii addressing */
   if (msg[0] == '\0' && msg[1] == '$')
   {
      Index = brsstrlen((UDINT)&msg[1]) + 2;    /* count bytes in field */
      strncpy(&ascii_addr[0], (char *)&msg[3], 19);     /* rmv null,$ F or N  */
      value = ascii_addr;                       /* extract the file   */
		brsstrcpy((UDINT)&chr[0], (UDINT)":");
      delNdx = findstr(ascii_addr, chr);
      delimit = &value[delNdx];             /* terminate file     */
      *delimit = '\0';                          /*  bytes with null   */
      *file = (UDINT)brsatoi((UDINT)value);                       /* get the file value */

      value = delimit + 1;                       /* pt to element bytes*/
		brsstrcpy((UDINT)&chr[0], (UDINT)"/");
      delNdx = findstr(ascii_addr, chr);            /* check for subelmt  */
      if (delNdx != 0)
      {                  /* slash was found    */
         delimit = &value[delNdx];          /* terminate element  */
         *delimit = '\0';                       /*  bytes with null   */
      }
      element = (unsigned int)brsatoi((UDINT)value);                    /* get element value  */

      if (delNdx != 0)
      {                  /* subelement present */
         value = delimit + 1;                   /* pt to subelmt bytes*/
         sub_element = (unsigned int)brsatoi((UDINT)value);             /* get subelmt value  */
      }
   }
   else   /* using binary addressing */
   {
      Index = 1;

      /* process level 1 values(data table area) if present   */
      if (msg[ADDR_FLAG] & LEVEL_1)    /* bit 1 data table  */
      {
         if (msg[Index] != DELIMITER)
            dta_table = msg[Index++];
         else
         {
            /* get 2 byte value (low byte first) */
            dta_table = (int)(msg[(Index)+2] * 0x100) + msg[(Index)+1];
            Index += 3;
         }
      } /* endif level 1 values present  */
      else                      /* use default data table vals */
         dta_table = 0;
      /* process level 2 value (File number if present   */
      if (msg[ADDR_FLAG] & LEVEL_2)   /* bit 2 file values */
      {
         if (msg[(Index)] != DELIMITER)
            *file = msg[(Index)++];
         else
         {
            /* get 2 byte value (low byte first) */
            *file = (int)(msg[(Index)+2] * 0x100) + msg[(Index)+1];
            Index += 3;
         }
      } /* endif level 2 values present */

      else                      /* use default file value      */
         *file = 1;
      if (msg[ADDR_FLAG] & LEVEL_3) /* bit 3 element values */
      {
         if (msg[Index] != DELIMITER)
            element = msg[(Index)++];
         else
         {
            /* get 2 byte value (low byte first) */
            element = (int)(msg[(Index)+2] * 0x100) + msg[(Index)+1];
            Index += 3;
         }
      } /* endif leve 3 values present */

      else                      /* use default element value  */
         element = 0;

      if (msg[ADDR_FLAG] & LEVEL_4)  /* subelement values */
      {
         if (msg[Index] != (unsigned char)0xFF)
            sub_element = msg[(Index)++];
         else
         {
            /* get 2 byte value (low byte first) */
            sub_element = (int)(msg[(Index)+2] * 0x100) + msg[(Index)+1];
            Index += 3;
         }
      } /* endif level 4 values present*/

      else                      /* use default subelement val  */
         sub_element = 0;

   } /* endelse binary addressing */

   if (cs_->use_pcc2_address)
   {
      RegPoint = plc5_to_pcc2 (*file, element, sub_element);
   }
   else
   {
      /* determine reg point, check for horizontal file */
      if (*file >= 400 && *file < 500)
         RegPoint = 9000 + (*file % 400);
      /* check for UCBs */
      else if (*file >= 500 && *file < 510)
         RegPoint = 30000 + ((*file % 500) * 100) + element;
      else if (*file >= 510 && *file < 600)
         RegPoint = 30000 + ((*file % 510) * 100) + element;
      else
         RegPoint = ((*file - 100) * 100) + element;
   }

   Offset = (int)((MsgIn[OFFSET_HI] * 0x100) + MsgIn[OFFSET_LO]);
   MsgIndex = PLC5_ADDR + Index;

   RegPoint += Offset;
   return RegPoint;
}

unsigned int plc5_to_pcc2(unsigned int file, unsigned int element, unsigned int sub_element)
{
   unsigned int point=0;

   switch (file)
   {
      case 200:                        /* HO Float File */
         point = PCC2_HO_OFFSET + element;
         break;
      case 201:                        /* HO Integer File */
         point = PCC2_HO_OFFSET + element + 299;
         break;
      case 202:                        /* SHO Float File */
         point = PCC2_SHO_OFFSET + element;
         break;
      case 203:                        /* SHO Integer File */
         point = PCC2_SHO_OFFSET + element + 299;
         break;
      case 204:                        /* WIDTH Float File */
         point = PCC2_WTH_OFFSET + element;
         break;
      case 205:                        /* WIDTH Integer File */
         point = PCC2_WTH_OFFSET + element + 299;
         break;
#if 0
      case 206:                        /* FLF Float File */
         point = PCC2_FLF_OFFSET + element;
         break;
      case 207:                        /* FLF Integer File */
         point = PCC2_FLF_OFFSET + element + 299;
         break;
#endif
      case 300:                        /* SYSTEM Float File */
         point = PCC2_SYSTEM_OFFSET + element;
         break;
      case 301:                        /* SYSTEM Integer File */
         point = PCC2_SYSTEM_OFFSET + element + 299;
         break;
      default:
         if (file >= 100 && file < 120)      /* EXTRUDERS(0-19) Float Files      */
            point = (PCC2_EXTRUDER_OFFSET * (file % 100)) + element;
         else if (file >= 120 && file < 139) /* EXTRUDERS(0-19) Interger Files   */
            point = (PCC2_EXTRUDER_OFFSET * (file % 100)) + (element + 299);
         if (file >= 140 && file < 160)      /* SUB_DEVICES (0-19) Float Files      */
            point = (PCC2_EXTRUDER_OFFSET * (file % 100)) + element;
         else if (file >= 170 && file < 199) /* SUB_DEVICES (0-19) Interger Files   */
            point = (PCC2_EXTRUDER_OFFSET * (file % 100)) + (element + 299);
         if (file >= 500 && file < 509)      /* UCB DEFINITION Integer Files      */
            point = PCC2_UCB_DEF_OFFSET + ((file % 500)* UCB_SIZE) + element;
         else if (file >= 520 && file < 529) /* UCB DATA Integer Files   */
            point = PCC2_UCB_DATA_OFFSET + ((file % 520) * UCB_SIZE) + (element);
         else if (file >= 540 && file < 549) /* UCB DATA Float Files   */
            point = PCC2_UCB_DATA_OFFSET + ((file % 540) * UCB_SIZE) + (element);
         break;
   }

   return(point);

}
/**********************************************************************/
/* get_plc5_write_type - this function decodes the plc5 type data       */
/*  parameter as follows:                                             */
/*                                                                    */
/**********************************************************************/
int get_plc5_write_type(unsigned char *DataType,int *RegSpan)
{
unsigned char FlagByte;
unsigned char DataTypeId;
unsigned char Descriptor;
int BytesPerElement;
int Size;

   FlagByte = MsgIn[MsgIndex++];

   if (FlagByte & 0x80)
   {
      DataTypeId = MsgIn[MsgIndex++];
   }
   else
      DataTypeId = (FlagByte & 0x70) >> 4;

   if (FlagByte & 0x08)
      Size = (int)MsgIn[MsgIndex++];
   else
      Size = (int)(FlagByte & 0x07);

   switch (DataTypeId)
   {
      case ARRAY_TYPE:
         Descriptor = MsgIn[MsgIndex++];
         if (Descriptor & 0x80)
         {
            Size--;     /* subtract descriptor byte from total bytes */
            *DataType = MsgIn[MsgIndex++];
         }
         else
            *DataType = (Descriptor & 0x70) >> 4;

         if (Descriptor & 0x08)
            BytesPerElement = MsgIn[MsgIndex++];
         else
            BytesPerElement = Descriptor & 0x07;
         break;
      case FLOAT_TYPE:
         BytesPerElement = 4;
         break;
      case INTEGER_TYPE:
         BytesPerElement = 2;
         break;
      case BIT_TYPE:
         BytesPerElement = 2;
         break;
      default:
         BytesPerElement = 2;
         break;
   }

   *RegSpan = Size / BytesPerElement;

   return BytesPerElement;

}
/**********************************************************************/
/* get_plc5_read_type - this function decides what type of data is being   */
/*  read.                                                             */
/*                                                                    */
/**********************************************************************/
unsigned char get_plc5_read_type(int Point,int File)
{
   unsigned char DataType = FLOAT_TYPE;
   int pcc2_device;
   config_super *cs_ = &cfg_super;

   if (cs_->use_pcc2_address)
   {
      pcc2_device = Point / PCC2_DEVICE_OFFSET;
      switch (pcc2_device)
      {
         case REM_UCB_DEF_ID:
            DataType = INTEGER_TYPE;
            break;
         case REM_UCB_DATA_ID:
            if ((File >= 520) && (File < 529))
               DataType = INTEGER_TYPE;
            if ((File >= 540) && (File < 549))
               DataType = FLOAT_TYPE;

            break;
         case REM_RECIPE_NAME_ID:
         case REM_RESIN_NAME_ID:
            break;
         case REM_RECIPE_DATA_ID:
            break;
         case REM_RESIN_DATA_ID:
            break;
         default:
            if ( (Point % PCC2_TYPES_PER) >= REG_INTEGER )     /* if integer data   */
               DataType = INTEGER_TYPE;
            else
               DataType = FLOAT_TYPE;
      }
   }
   else  /* original pcc addressing */
   {
      if ((Point >= 30000) && (Point < 31000))   /* user definable blocks */
      {
         if ((File >= 500) && (File < 510))
            DataType = INTEGER_TYPE;
         if ((File >= 510) && (File < 520))
            DataType = FLOAT_TYPE;
      }
      else
      {
         if ( (Point % 100) >= R_INTEGER )     /* if integer data   */
            DataType = INTEGER_TYPE;
         else
            DataType = FLOAT_TYPE;
      }
   }

   return DataType;
}

/*#if ((MACH==BATCH)||(MACH==WX_BATCH)||(MACH==WE_BATCH))*/
#if 1

/*
*****************************************************************************
*                  READ HORIZONTAL BATCH DATA
*****************************************************************************
*  Function:      read_horizontal_data();
*  Input:         1. RegPoint - register point requested
*                 2. RegSpan - # of registers to read
*                 3. ElementSize - # of bytes per element
*  Output:        void
*  Description:   This function reads data from the internal regs. Data is
*                 read as a block horizontally across devices, ie. read all
*                  alarm words or hopper wts in one block. Note that horizontal reads will
*                 access data in the following order
*
*      SYSTEM REGISTER        200xx     xx = modulo 100 of register pt
*      WIDTH REGISTER         110xx     (if wth_mode != UNUSED)
*      HAUL OFF REGISTER      100xx     (if ltp_mode != UNUSED)
*      EXTRUDER REGISTER        9xx     (if wtp_mode != UNUSED)
*      HOPPER REGISTER          hxx        h = hopper number 0 = A, 1 = B
*  Notes:
*        Requested data is placed in output buffer.
*****************************************************************************
*/

void read_horizontal_data(unsigned int RegPoint,int RegSpan,int ElementSize)
{
	register int num_pts_returned = 0;
	register int i,j;
	int FillBytes;
    config_super *cs_ = &cfg_super;

   RegPoint = RegPoint % 100;

   /* check for bitmapped word reads, pts 90-99 */
   if (RegPoint > R_INTEGER)
   {
      if(RegPoint > 90)
      {
         /* fetch upper word values for system integer register */
         read_pt_into_data(SYSTEM_OFFSET+RegPoint);

         /* fetch lower word */
         read_pt_into_data(WTH_OFFSET+RegPoint-5);
      }
      else
      {
         MsgOut[(MsgOutIndex)++] = 0;
         MsgOut[(MsgOutIndex)++] = 0;
         read_pt_into_data(WTH_OFFSET+RegPoint);
      }
      num_pts_returned++;

      /* read register for take off device if any, check cntl/monitor mode */
      /* also, figure how many pts were read to satisfy horiz read         */
      if (cs_->ltp_mode != UNUSED)
      {
         if(RegPoint > 90)
         {
            read_pt_into_data(HO_OFFSET+RegPoint);
            read_pt_into_data(HO_OFFSET+RegPoint-5);
         }
         else
         {
            MsgOut[(MsgOutIndex)++] = 0;
            MsgOut[(MsgOutIndex)++] = 0;
            read_pt_into_data(HO_OFFSET+RegPoint);
         }
         num_pts_returned++;
      }

      /* read register for extruder if any, check cntl/monitor mode */
      /* also, figure how many pts were read to satisfy horiz read         */
      if (cs_->wtp_mode != UNUSED)
      {
         if(RegPoint > 90)
         {
               read_pt_into_data(BATCH_EXT_OFFSET+RegPoint);
               read_pt_into_data(BATCH_EXT_OFFSET+RegPoint-5);
         }
         else
         {
               MsgOut[(MsgOutIndex)++] = 0;
               MsgOut[(MsgOutIndex)++] = 0;
               read_pt_into_data(BATCH_EXT_OFFSET+RegPoint);
         }
         num_pts_returned++;
      }

      if (cs_->scrap != UNUSED)
      {
         if(RegPoint > 90)
         {
               read_pt_into_data(REFEED_OFFSET+RegPoint);
               read_pt_into_data(REFEED_OFFSET+RegPoint-5);
         }
         else
         {
               MsgOut[(MsgOutIndex)++] = 0;
               MsgOut[(MsgOutIndex)++] = 0;
               read_pt_into_data(REFEED_OFFSET+RegPoint);
         }
         num_pts_returned++;
      }
/*#if ((MACH==WX_BATCH)||(MACH==WE_BATCH)) */
#if 1
      for(j = 0; j < cs_->total_gates; ++j)
      {
         if(RegPoint > 90)
         {
               read_pt_into_data((j * HOPPER_OFFSET) + RegPoint);
               read_pt_into_data((j * HOPPER_OFFSET) + RegPoint-5);
         }
         else
         {
               MsgOut[(MsgOutIndex)++] = 0;
               MsgOut[(MsgOutIndex)++] = 0;
               read_pt_into_data((j * HOPPER_OFFSET) + RegPoint);
         }
         num_pts_returned++;
      }
#endif
   }
   else
   {
      /* floating point */

      /* fetch value for system register */
      read_pt_into_data(SYSTEM_OFFSET+RegPoint);
      ++num_pts_returned;

      /* read register for width device if any, check cntl/monitor mode */
      /* also, figure how many pts were read to satisfy horiz read         */
      if (cs_->wth_mode != UNUSED)
      {
         read_pt_into_data(WTH_OFFSET + RegPoint);
         num_pts_returned++;
      }

      /* read register for take off device if any, check cntl/monitor mode */
      /* also, figure how many pts were read to satisfy horiz read         */
      if (cs_->ltp_mode != UNUSED)
      {
         read_pt_into_data(HO_OFFSET + RegPoint);
         num_pts_returned++;
      }

      /* read register for extruder if any, check cntl/monitor mode */
      /* also, figure how many pts were read to satisfy horiz read         */
      if (cs_->wtp_mode != UNUSED)
      {
         read_pt_into_data(BATCH_EXT_OFFSET + RegPoint);
         num_pts_returned++;
      }

      if (cs_->scrap != UNUSED)
      {
         read_pt_into_data(REFEED_OFFSET + RegPoint);
         num_pts_returned++;
      }
/*#if ((MACH==WX_BATCH)||(MACH==WE_BATCH))*/
#if 1
      for(j=0;j<cs_->total_gates;++j)
      {
         read_pt_into_data((j * HOPPER_OFFSET) + RegPoint);
         num_pts_returned++;
      }
#endif
   }

   /* see if pts returned by system doesn't match RegSpan requested in msg*/
   if (num_pts_returned > RegSpan)
      MsgOutIndex -= (num_pts_returned - RegSpan) * ElementSize;
   else if (num_pts_returned < RegSpan)
   {
      FillBytes = (RegSpan - num_pts_returned) * ElementSize;
      for (i = FillBytes; i > 0; i--)
         MsgOut[(MsgOutIndex)++] = 0;
   }
}

/****************************************************************************/
/* write_horizontal_data - this function writes # of registers given by     */
/*   RegSpan into a horizontal set of registers, i.e. will access all      */
/*   xx03 or xx99 registers for every device in system. Registers will be   */
/*   accessed in the following order:                                       */
/*                                                                          */
/*      SYSTEM REGISTER        200xx     xx = modulo 100 of register pt     */
/*      WIDTH REGISTER         110xx     (if wth_mode != UNUSED)            */
/*      HAUL OFF REGISTER      100xx     (if ltp_mode != UNUSED)            */
/*      EXTRUDER REGISTER        9xx     (if wtp_mode != UNUSED)            */
/*      HOPPER REGISTER          hxx        h = hopper number 0 = A, 1 = B  */
/****************************************************************************/
void write_horizontal_data(unsigned int RegPoint,unsigned int RegSpan,unsigned char DataType)
{
   config_super *cs_ = &cfg_super;
    int i;

    /* write the system register point exit if only point written is last one*/
    write_data_point( SYSTEM_OFFSET+RegPoint, DataType);
    if (!(--RegSpan))
    {
        return;
    }

    /* write width data if is configured with one */
    if (cs_->wth_mode != UNUSED)
    {
        write_data_point( (WTH_OFFSET+RegPoint), DataType);
        if (!(--RegSpan))
        {
            return;
        }
    }

    /* write haul off data if is configured with one */
    if (cs_->ltp_mode != UNUSED)
    {
        write_data_point( (HO_OFFSET+RegPoint), DataType);
        if (!(--RegSpan))
        {
            return;
        }
    }

    /* write extruder data if is configured with one */
    if (cs_->wtp_mode != UNUSED)
    {
        write_data_point( (BATCH_EXT_OFFSET+RegPoint), DataType);
        if (!(--RegSpan))
        {
            return;
        }
    }

    /* write scrap data if is configured with one */
    if (cs_->scrap != UNUSED)
    {
        write_data_point( (REFEED_OFFSET+RegPoint), DataType);
        if (!(--RegSpan))
        {
            return;
        }
    }
/*#if ((MACH==WX_BATCH)||(MACH==WE_BATCH))*/
#if 1
    for (i=0; i<cs_->total_gates; ++i)
    {
        write_data_point(i*HOPPER_OFFSET + RegPoint, DataType);
        if (!(--RegSpan))
        {
            return;
        }
    }
#endif
}
#endif

unsigned int findstr(char str[], char chr[])
{
	unsigned int i; /* BMM AB */
	unsigned char tmp1;
	unsigned char tmp2;

	tmp2 = chr[0];
	for (i=0; i<brsstrlen((UDINT)str); i++)
	{
		tmp1 = str[i];
		if (tmp1 == tmp2)
			return (i);
	}
	return (0);
}

