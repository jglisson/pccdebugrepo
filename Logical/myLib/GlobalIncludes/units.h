/************************************************************************/
/*
   File:  units.h

   Description: This file contains defines used for determining the units
      currently used in the system. WIth the B&R system the visualization
      manipulates the process variables to display the desired units.
      Some of the definitions defined here are used to specify menu
      selections in the visualization units configuration page.


      $Log:   F:\Software\B&R_Guardian\units.h_v  $
 *
 *    Rev 1.0   Jan 11 2008 10:33:42   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#define ENGLISH             0       /* units are english                */
#define METRIC              1       /* units are metric                 */
#define USER_DEFINED        2       /* units are user defined           */

/* min & max's for above formatting codes */
#define WIDTH_MIN       (float)2.0e-2
#define WIDTH_MAX       (float)9.99999e3

/* NOTE: The following values are based on the index value used */
/*       in the BR automation Unit Group. They must match in order */
/*       for the conversion on the display to match */
#define LBS_FT       0
#define LBS_1000FT   1
#define G_M          2
#define KG_M         3

#define OZ_HR        0
#define LB_HR        1
#define KG_HR        2
#define G_HR         3
#define G_MIN        4

#define LBS_SQFT     0
#define LBS_REAM     1
#define G_SQM        2
#define KG_SQM       3

#define INCH   0
#define MILS   1
#define CM     2
#define MM     3
#define uM     4
#define FEET   5
#define METER  6

#define OZS    0
#define LBS    1
#define KGS    2
#define GRAMS  3
#define MGRAMS 4

#define FAHR   0
#define CENT   1

#define FT_MIN 0
#define FT_HR  1
#define M_MIN  2
#define M_HR   3

/* k factors                    */
#define ES_WTP_K (float) 6.25e-2                /* english, small wtp   */
#define EL_WTP_K (float) 1.0                    /* english, large wtp   */
#define MS_WTP_K (float) 1.666666e1             /* metric, small wtp    */
#define ML_WTP_K (float) 2.204623               /* metric, large wtp   */

#define ES_LTP_K (float) 1.666666e-2            /* english, small ltp   */
#define EL_LTP_K (float) 1.0                    /* english, large ltp   */
#define MS_LTP_K (float) 1.666666e-2            /* metric, small ltp    */
#define ML_LTP_K (float) 3.28084                /* metric, large ltp    */

#define ES_WPL_K (float) 1.666666e1             /* english, small wpl   */
#define EL_WPL_K (float) 1.666666e-2            /* english, large wpl   */
#define MS_WPL_K (float) 1.666666e1             /* metric, small wpl    */
#define ML_WPL_K (float) 1.666666e-2            /* metric, large wpl    */

#define ES_WPA_K (float) 6.0e2                  /* english, small WPA   */
#define EL_WPA_K (float) 2.0e-1                 /* english, large WPA   */
#define MS_WPA_K (float) 1.666666e3             /* metric, small WPA    */
#define ML_WPA_K (float) 1.666666               /* metric, large WPA    */

#define ES_AREA_K (float) 2.777777e-5           /* english, small AREA  */
#define EL_AREA_K (float) 8.333333e-2           /* english, large AREA  */
#define MS_AREA_K (float) 1.0e-2                /* metric, small AREA   */
#define ML_AREA_K (float) 1.0e-2                /* metric, large AREA   */

#define ES_EA_K  (float) 1.0e-3                 /* english, mils to in  */
#define EL_EA_K  (float) 1.0                    /* english, in to in    */
#define MS_EA_K  (float) 1.0e-4                 /* metric, um to cm     */
#define ML_EA_K  (float) 1.0                    /* metric, cm to cm     */

#define ES_OD_K  (float) 1.0e-3                 /* english, mils to in  */
#define EL_OD_K  (float) 1.0                    /* english, in to in    */
#define MS_OD_K  (float) 1.0e-1                 /* metric, mm to cm     */
#define ML_OD_K  (float) 1.0                    /* metric, cm to cm     */

#define ES_ID_K  (float) 1.0e-3                 /* english, mils to in  */
#define EL_ID_K  (float) 1.0                    /* english, in to in    */
#define MS_ID_K  (float) 1.0e-1                 /* metric, mm to cm     */
#define ML_ID_K  (float) 1.0                    /* metric, cm to cm     */

/* note - if changes made here, then kundig.c needs updating    */
#define ES_WIDTH_K  (float) 1.0                 /* english, in to in    */
#define EL_WIDTH_K  (float) 1.0                 /* english, in to in    */
#define MS_WIDTH_K  (float) 1.0e-1              /* metric, mm to cm     */
#define ML_WIDTH_K  (float) 1.0                 /* metric, cm to cm     */

#define E_T_K    (float) 2.601165e1             /* english thickness    */
#define M_T_K    (float) 6.0                    /* metric thickness     */

