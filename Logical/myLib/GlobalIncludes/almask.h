/************************************************************************/
/*
   File:  almask.h

   Description: This file sets up arrays which define which alarm texts
      are available for each device in the system.


      $Log:   F:\Software\BR_Guardian\almask.h_v  $
 *
 *    Rev 1.1   Jan 30 2008 11:39:46   FMK
 * Changed reference for PVCS check in comment path.
 *
 *    Rev 1.0   Jan 11 2008 10:33:30   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
static unsigned int liw_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  DEV_OVERSPEED,                        /* 1            */
  DEV_EXCESSIVE_COAST,                  /* 2            */
  DEV_OUT_OF_SPEC,                      /* 3            */
  DEV_UNDERSPEED,                       /* 4            */
  DEV_DRV_SYS_FAILURE,                  /* 5            */
  0,0,0,0,                              /* 6-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,0,0,0,0,0,0,                  /* 20-29        */
  0,0,0,0,0,0,0,0,0,0,                  /* 30-39        */
  0,                                    /* 40           */
  0,                                    /* 41           */
  0,                                    /* 42           */
  0,                                    /* 43           */
  0,                                    /* 44           */
  WM_OVERWEIGHT,                        /* 45           */
  0,                                    /* 46 - not used*/
  DUMP_LOW,                             /* 47           */
  WT_LOW,                               /* 48           */
  WT_CRIT,                              /* 49           */
  WM_LOSS_OF_SYNC,                      /* 50           */
  WM_UNDERWEIGHT,                       /* 51           */
  0,0,0,0,0,0,0,0,                      /* 52-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,                                    /* 70           */
  0,                                    /* 71           */
  0,                                    /* 72           */
  EXD_TIMEOUT,                          /* 73           */
  EXD_IN_MANUAL,                        /* 74           */
  EXD_INHIBIT,                          /* 75           */
  0,                                    /* 76           */
  0,0,0,                                /* 77,78,79     */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int gate_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  0,                                    /* 1            */
  0,                                    /* 2            */
  DEV_OUT_OF_SPEC,                      /* 3            */
  0,                                    /* 4            */
  0,                                    /* 5            */
  0,0,WT_CRIT_TIME,0,                   /* 6-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,0,0,0,0,0,0,                  /* 20-29        */
  0,0,0,0,0,0,0,					          /* 30-36        */
  HOPPER_SWITCH_FAILURE,    		       /* 37           */
  HOPPER_PROX_HIGH,                     /* 38           */
  HOPPER_PROX_LOW,                      /* 39           */
  0,                                    /* 40           */
  0,                                    /* 41           */
  0,                                    /* 42           */
  0,                                    /* 43           */
  0,                                    /* 44           */
  0,                                    /* 45           */
  HOPPER_LEVEL_LOW,                     /* 46           */
  DUMP_LOW,                             /* 47           */
  WT_LOW,                               /* 48           */
  WT_CRIT,                              /* 49           */
  0,                                    /* 50           */
  0,                                    /* 51           */
  0,                                    /* 52           */
  0,                                    /* 53           */
  DEV_DRV_SYS_FAILURE,                  /* 54           */
  0,                                    /* 55           */
  0,                                    /* 56           */
  0,                                    /* 57           */
  0,0,                                  /* 58-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,0,0,0,0,0,0,0,0,0,                  /* 70-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int bwh_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  0,                                    /* 1            */
  DEV_EXCESSIVE_COAST,                  /* 2            */
  DEV_OUT_OF_SPEC,                      /* 3            */
  0,                                    /* 4            */
  0,                                    /* 5            */
  0,0,0,0,                              /* 6-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,0,0,0,                        /* 20-26        */
  0,                                    /* 27           */
  0,0,                                  /* 28-29        */
  0,0,0,0,0,0,0,0,0,0,                  /* 30-39        */
  WM_BOARD_FAIL,                        /* 40           */
  0,                                    /* 41           */
  0,                                    /* 42           */
  0,                                    /* 43           */
  WM_IN_MANUAL,                         /* 44           */
  WM_OVERWEIGHT,                        /* 45           */
  0,                                    /* 46           */
  0,                                    /* 47           */
  0,                                    /* 48           */
  0,                                    /* 49           */
  0,                                    /* 50           */
  WM_UNDERWEIGHT,                       /* 51           */
  0,                                    /* 52           */
  DEV_NOT_DUMPING,                      /* 53           */
  0,                                    /* 54           */
  SWD_OUT_OF_SPEC,                      /* 55           */
  SPD_OUT_OF_SPEC,                      /* 56           */
  0,                                    /* 57           */
  0,0,                                  /* 58-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,0,0,0,0,0,0,0,0,0,                  /* 70-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};


static unsigned int mix_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  0,                                    /* 1            */
  0,                                    /* 2            */
  0,                                    /* 3            */
  0,                                    /* 4            */
  0,                                    /* 5            */
  0,0,0,0,                              /* 6-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,                              /* 20-23        */
  INTERLOCK_OPEN_ALARM,                 /* 24           */
  0,0,0,                                /* 25-27        */
  MIXER_MOTOR_ALARM,                    /* 28           */
  0,                                    /* 29           */
  0,0,0,0,0,0,0,0,                      /* 30-37 */
  0,0,                                  /* 38-39        */
  0,0,0,0,0,0,0,0,0,0,                  /* 40-49        */
  0,0,0,0,                              /* 50-53        */
  MIX_VALVE_FAILURE,                    /* 54           */
  0,0,0,0,0,                            /* 55-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,0,0,0,0,0,0,                        /* 70-76        */
  MAT_REQ_UNCOVERED,                    /* 77           */
  0,0,                                  /* 78-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int ho_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  DEV_OVERSPEED,                        /* 1            */
  DEV_EXCESSIVE_COAST,                  /* 2            */
  DEV_OUT_OF_SPEC,                      /* 3            */
  DEV_UNDERSPEED,                       /* 4            */
  DEV_DRV_SYS_FAILURE,                  /* 5            */
  0,0,0,0,                              /* 6-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,0,0,0,0,0,0,                  /* 20-29        */
  0,0,0,0,0,0,0,0,0,0,                  /* 30-39        */
  0,0,0,0,0,0,0,0,0,0,                  /* 40-49        */
  0,0,0,0,0,0,0,0,0,0,                  /* 50-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  EXD_BOARD_FAIL,                       /* 70           */
  EXD_RESET,                            /* 71           */
  EXD_INCOMPATABILITY,                  /* 72           */
  EXD_TIMEOUT,                          /* 73           */
  EXD_IN_MANUAL,                        /* 74           */
  EXD_INHIBIT,                          /* 75           */
  EXD_AD_OVERLOAD,                      /* 76           */
  0,0,0,                                /* 77,78,79     */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int wth_alarm_mask[100] =
{
  0,                                    /* 0 not used   */
  0,0,                                  /* 1-2          */
  DEV_OUT_OF_SPEC,                      /* 3            */
  0,0,0,0,0,0,                          /* 4-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,0,0,0,0,0,0,                  /* 20-29        */
  0,0,0,0,0,0,0,0,0,0,                  /* 30-39        */
  0,0,0,0,0,0,0,0,0,0,                  /* 40-49        */
  0,0,0,0,0,0,0,0,0,0,                  /* 50-59        */
  WTH_FAIL,                             /* 60           */
  WTH_INCOMPATABILITY,                  /* 61           */
  WTH_TIMEOUT,                          /* 62           */
  WTH_IN_MANUAL,                        /* 63           */
  WTH_PRODUCT_BREAK,                    /* 64           */
  WTH_MEASUREMENT_ERROR,                /* 65           */
  WTH_OVER_MAXIMUM,                     /* 66           */
  WTH_UNDER_MINIMUM,                    /* 67           */
  0,0,                                  /* 68-69        */
  0,0,0,0,0,0,0,0,0,0,                  /* 70-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int sys_alarm_mask[100] =
{
  0,0,0,0,0,0,0,0,0,0,                  /* 0-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,                                    /* 20           */
  0,                                    /* 21           */
  0,                                    /* 22           */
  CHECK_PRINTER_ALARM,                  /* 23           */
  0,                                    /* 24           */
  PS_FAILURE_ALARM,                     /* 25           */
  HARDWARE_FAILURE_ALARM,               /* 26           */
  BATTERY_LOW_ALARM,                    /* 27           */
  0,                                    /* 28           */
  AUXILIARY_ALARM,                      /* 29           */
  PUMP_FAILURE_ALARM,                   /* 30           */
  EXD_PLC_LOW_BATTERY_ALARM,            /* 31           */
  RESERVE_INPUT1_ALARM,                 /* 32           */
  RESERVE_INPUT2_ALARM,                 /* 33           */
  RESERVE_INPUT3_ALARM,                 /* 34           */
  0,                					       /* 35           */
  0,0,0,0,                              /* 36-39        */
  WM_BOARD_FAIL,                        /* 40, for fluff*/
  WM_RESET,                             /* 41  loader   */
  WM_INCOMPATABILITY,                   /* 42     "     */
  WM_TIMEOUT,                           /* 43     "     */
  WM_IN_MANUAL,                         /* 44     "     */
  WM_OVERWEIGHT,                        /* 45     "     */
  DEV_SETTLE_TIME,                      /* 46     "     */
  0,                                    /* 47     "     */
  WT_LOW,                               /* 48     "     */
  WT_CRIT,                              /* 49     "     */
  0,                                    /* 50     "     */
  WM_UNDERWEIGHT,                       /* 51     "     */
  0,0,0,0,0,0,0,0,                      /* 52-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,0,0,0,0,0,0,0,0,0,                  /* 70-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};

static unsigned int flf_alarm_mask[100] =
{
  0,0,0,0,0,0,0,0,0,0,                  /* 0-9          */
  0,0,0,0,0,0,0,0,0,0,                  /* 10-19        */
  0,0,0,0,                              /* 20-23        */
  INTERLOCK_OPEN_ALARM,                 /* 24           */
  0,                                    /* 25           */
  0,                                    /* 26           */
  0,                                    /* 27           */
  0,                                    /* 28           */
  0,                                    /* 29           */
  0,0,0,0,0,0,0,0,0,0,                  /* 30-39        */
  WM_BOARD_FAIL,                        /* 40,          */
  WM_RESET,                             /* 41           */
  WM_INCOMPATABILITY,                   /* 42           */
  WM_TIMEOUT,                           /* 43           */
  WM_IN_MANUAL,                         /* 44           */
  WM_OVERWEIGHT,                        /* 45           */
  0,                                    /* 46           */
  0,                                    /* 47           */
  WT_LOW,                               /* 48           */
  WT_CRIT,                              /* 49           */
  0,                                    /* 50           */
  WM_UNDERWEIGHT,                       /* 51           */
  0,0,0,0,0,0,0,0,                      /* 52-59        */
  0,0,0,0,0,0,0,0,0,0,                  /* 60-69        */
  0,0,0,0,0,0,0,0,0,0,                  /* 70-79        */
  0,0,0,0,0,0,0,0,0,0,                  /* 80-89        */
  0,0,0,0,0,0,0,0,0,0                   /* 90-99        */
};


