/************************************************************************/
/*                                                                      */
/*   File:  gate_pcc.h                                                  */
/*                                                                      */
/*   Description: This file defines the Vars and functions.             */
/*                                                                      */
/************************************************************************/

/* statistics structures */
#define INIT_UCL             0x01
#define VALID_STATS          0x02
#define HAS_FED              0x04
#define STD_DEV_MULTIPLIER   ((float)4.0)
#define INITIAL_ERROR        ((float)0.03)
#define STD_MEASUREMENTS     15
#define MOD_SINGLE_SHOT      1.00
#define MOD_DUAL_SHOT        0.96
#define MOD_STD_ALGORITHM    0.92

/* zero cross            */
/*#define MIN_GATE_TIME_HI 200 per len's request */
#define MIN_GATE_TIME_HI 700 
#define MIN_GATE_TIME_LO 20
#define MIN_GATE_TIME_SMALL 220                /* Small hopper for AutoBatch  */
#define MIN_GATE_TIME_MEDIUM 180               /* Medium hopper for AutoBatch  */
#define MIN_GATE_TIME_STANDARD 25              /* Standard hopper for AutoBatch  */
#define MIN_GATE_TIME_NORMAL 25
#define MIN_GATE_TIME_REGRIND 25               /* Regrind hopper for AutoBatch, Tendative */  

#define ZC_NUM_SAMPLES 5  /* # of of samples used while detecting first real feed. */
#define ZC_OFFSET 1*TC_ONE_CYCLE_TIME /* # of mS to offset the gate minimum firing time from the first real feed. */
#define ZC_DELTA_MS TC_ONE_CYCLE_TIME /* # of mS to change the gate minimum firing time while detecting first real feed. */
#define ZC_START 0        /* State Machine Variable */
#define ZC_CHECKMIN 1     /* State Machine Variable */
#define ZC_UP 2           /* State Machine Variable */
#define ZC_DOWN 3         /* State Machine Variable */
#define ZC_DONE 4         /* State Machine Variable */

#define TRUNCATE 1        /* if not 0 we truncate gate times to nearest TIME_WT_RESOLUTION mS GTF */

/* store_measurement     */
#define WT_CK_PERCENT 0.50 /* modify delta time in Store_measurement by this percent GTF*/

/* WEIGHT STABLE */
#define USE_SETTLE_TIME_WEIGHT_THRESHOLD 0.5   /* was .5 gtf */
   /* the weight for each gate below which we will start using the settle   */
   /* time calculation instead of relying solely on BWH.wt_stable  JLR      */
#define SETTLE_TIME 500             /* was (TICKSPERSEC) aka 1000 in mS GTF */

/* CALC_GATE_TIME */
#define SINGLE_SHOT_FACTOR	1.0
#define DUAL_SHOT_FACTOR	1.0
#define TIME_CHECK_FACTOR  0.75
#define ZC_COUNT_MAX       200

/************************** moved from mem.h *********************************/
typedef struct  
{
  char  flags;          	      /* flags for initialization, etc                     */
  unsigned int numpts;  	      /* number of measurements for this feed time         */
  unsigned int nominal_time;   	/* nominal (center) feed time for this bin in mS GTF */
  float avg_wt;				      /* average weight for this point's measurements      */
  float expected_wt;             /* expected wt for this this point's measurements    */
  float fWeight[STD_MEASUREMENTS]; 		   /* stores the last 30 measurements for this point     */
  unsigned int uiTime[STD_MEASUREMENTS];	/* stores the desired gate time values for this point */
  unsigned int avg_time;
  unsigned char u8PositionIndex; /* here the next measurement will be stored 	  */
  float saved_std_error;	      /* saved std dev of error from LAST 30 SAMPLES */
} time_wt_str;

typedef struct  
{
   char structureName[15];
   char  VersionString[16];
} time_wt_header_str;


typedef struct 
{
   time_wt_header_str header;
   time_wt_str entry[SIZE_TIME_WT_ARRAY];

   /* turned this into a two dimensional array so that it can be stored/restored with the config files */
   /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition below.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
  unsigned int CRC16;
  /*
   * do NOT... under any circumstances define ANY variables below the
   * CRC definition above.  If you do, the world will end, the universe
   * will implode, and hell will freeze over.  Well, that and the CRC
   * calculations will always be wrong.
   */
} time_wt_line;

_GLOBAL time_wt_line *TIME_WT;


