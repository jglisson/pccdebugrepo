#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_istd_dev(isamples *sample_data, signed short *mean)
{
    int std_dev;
    int var;

    var = calc_ivariance(sample_data,mean);
    std_dev = (int)(sqrt((double)var)+.5);

    return std_dev;
}


