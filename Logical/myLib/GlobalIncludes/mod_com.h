/************************************************************************/
/*
   File:  mod_com.h

   Description: Modbus routines.


      $Log:   F:\Software\BR_Guardian\mod_com.h_v  $
 *
 *    Rev 1.0   Feb 11 2008 11:15:22   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#define READ_HOLD_REG      3
#define READ_INPUT_REG     4
#define PRESET_SINGLE_REG  6
#define PRESET_MULT_REG    16
#define READ_GENERAL_REF   20
#define WRITE_GENERAL_REF  21

/* define message states */
#define S_WAIT_FOR_BEGIN 0
#define S_GET_ADDR       1
#define S_GET_FCN        2
#define S_GET_DATA       3
#define S_FILL_ADDR_FCN  5
#define S_FILL_DATA      6
#define S_SEND_RESPONSE  7

/* define the two transmission protocols */
#define MODBUS_ASCII  0
#define MODBUS_RTU    1

#define MODBUS_TCP    0
#define MODBUS_SERIAL 1

#define BFR_SIZE  730
void fill_addr_fcn(int);

typedef struct
{
   unsigned char reference_type;
   unsigned int file_number;
   unsigned int start_reg;
   unsigned int num_regs;
}ref_group_struct;

struct msg_struct
{
   int addr;
   int function;
   unsigned char byte_count;
   ref_group_struct ref_group[100];
   unsigned int start_reg;
   int num_regs;
   unsigned int err_chk;
};

void get_data_block(void);
void fill_data_packet(unsigned int start_reg, int num_regs);
void send_response(void);


