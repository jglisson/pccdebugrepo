#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  writevar.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\writevar.c_v  $
 *
 * 2008/12/01 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.writeVar to toggle the debug information in this file
 *
 *    Rev 1.0   Feb 11 2008 11:06:12   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <math.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "register.h"
#include "remote.h"
#include "libbgdbg.h"
#include "recipe.h"

int write_remote_var_2(point_data *pd, unsigned int p,fic v);

extern int identify_user_block(point_data *pd, unsigned int point,unsigned int *block_num,unsigned int *base_point);
int user_block_write(point_data *pd, unsigned int *point,unsigned int val);
extern int rem_parse_register(point_data *pd, unsigned int i);
extern int rem_validate_recipe(point_data *pd,recipe_struct *r,int *validation_errors);
extern int rem_calc_recipe(point_data *pd, recipe_struct *r_);
extern void rem_change_recipe(point_data *pd, recipe_struct *r_);
extern void rem_copy_recipe(point_data *pd, recipe_struct *, recipe_struct *);

extern int (*fp_write_var_2[4000])(fic *, point_data *);
extern int wv_special_devices(fic v, point_data * pd);
extern int wv_special_devices64(fic v, point_data * pd);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/***************************************************************************
                     WRITE VAR
***************************************************************************/
int write_remote_var_2(point_data *pd, unsigned int p,fic v)
{
  int retcode;
  int fn_offset=0;
  char use_fn_tbl = TRUE;

   /*if(shr_global.disable_remote_writes)
	  return(INVALID_POINT);*/

   shr_global.rem_diag.last_point_processed = p; /* Copy for service diagnostic displays */

   /*if (shr_global.remote_lockout)
    return(EWARNING);*/

   /*pd->recipe_changed = FALSE;*/
   if( user_block_write(pd,&p, v.i) == DEFINED_POINT_REG )
   {
      return OK;
   }
   
   if (p == NOT_USED)
     return (INVALID_POINT);

   retcode = OK;

   pd->dev_type = rem_parse_register(pd,p);

   if (pd->reg < REG_INTEGER)
   {
    if (v.f < 0.000001)
      v.f = 0.0;
  }
  fn_offset = MAX_REG;

  /* setup offset into function table based on device type*/
  switch (pd->dev_type)
  {
     case TYPE_SYSTEM:
        fn_offset *= SYS_FUNC_OFFSET;
        break;
     case TYPE_GATE:
        fn_offset *= GATE_FUNC_OFFSET;
        break;
     case TYPE_LIW:
        fn_offset *= LIW_FUNC_OFFSET;
        break;
     case TYPE_HO:
        fn_offset *= HO_FUNC_OFFSET;
        break;
     case TYPE_BWH:
        fn_offset *= BWH_FUNC_OFFSET;
        break;
     case TYPE_MIX:
        fn_offset *= MIX_FUNC_OFFSET;
        break;
     case TYPE_WTH:
        fn_offset *= WTH_FUNC_OFFSET;
        break;
     case TYPE_FLF:
        fn_offset *= FLF_FUNC_OFFSET;
        break;
     default:
        use_fn_tbl = FALSE;
        break;
  }
  use_fn_tbl = TRUE; /* YYY */

  if(use_fn_tbl)
  {
     if((pd->reg > 0) && (pd->reg <= MAX_REG))
     {
        (*fp_write_var_2[fn_offset + pd->reg])(&v, pd);
        Signal.EnteredSetup = TRUE;
        shr_global.in_remote = TRUE;
     }
     else
        retcode = INVALID_POINT;
  }
  else
  {
     Signal.EnteredSetup = TRUE;
     shr_global.in_remote = TRUE;
     retcode = wv_special_devices(v, pd);  
     retcode = wv_special_devices64(v, pd);
  }

  if(pd->recipe_changed)
  {
     shr_global.new_recipe = TRUE;
     rem_copy_recipe(pd, &BATCH_RECIPE_EDIT, BATCH_RECIPE_NEW);
     rem_copy_recipe(pd, &BATCH_RECIPE_EDIT, &RECIPES->entry[HMI.pg453_RecipeNum]);
     rem_calc_recipe(pd,BATCH_RECIPE_NEW);
     
     shr_global.invalid_remote_recipe = rem_validate_recipe(pd,&BATCH_RECIPE_EDIT,shr_global.recipe_errors);
     if (shr_global.invalid_remote_recipe == 0)
     {
        shr_global.remote_run_recipe_changed = TRUE;
        Signal.StoreRecipes = TRUE;
     }
  }

  return (retcode);
}

/****************************************************************************/
/* int user_block_write()                                                   */
/*      if point is a normal register or part of a user defined data block  */
/*      then returns translated register. If trying to defined a new        */
/*      translation then stores the translation and returns -1              */
/****************************************************************************/
int user_block_write(point_data *pd, unsigned int *point,unsigned int val)
{
    config_super *cs = &cfg_super;
    unsigned int base_point;
    unsigned int block_num;
    int retcode = NORMAL_REG;    
    
    
    switch(retcode = identify_user_block(pd,*point,&block_num,&base_point))
    {
        case DEFINED_DATA_REG:
            *point = cs->user_def_blk[block_num].points[base_point];
            break;

        case DEFINED_POINT_REG:
            cs->user_def_blk[block_num].points[base_point] = val;
            break;
    }
    return retcode;
}

