
TYPE
	data_table : 	STRUCT 
		current_length : INT;
		data : REFERENCE TO ordered_pair;
		max_length : INT;
	END_STRUCT;
	isamples : 	STRUCT 
		next : INT;
		num_taken : INT;
		pts : REFERENCE TO ordered_ipair;
		length : INT;
	END_STRUCT;
	ma_tp_f_struct : 	STRUCT 
		data : ARRAY[0..9]OF REAL;
		count : INT;
		avg : REAL;
	END_STRUCT;
	ma_tp_i_struct : 	STRUCT 
		data : ARRAY[0..9]OF INT;
		count : INT;
		avg : REAL;
	END_STRUCT;
	moving_avg_str : 	STRUCT 
		num_valid : INT;
		num_pts : INT;
		data : REFERENCE TO REAL;
		curr_avg : REAL;
		limit_change : REAL;
		bad_value : REAL;
		num_clipped : SINT;
		curr_sum : REAL;
		idx : INT;
	END_STRUCT;
	ordered_ipair : 	STRUCT 
		x : INT;
		y : INT;
	END_STRUCT;
	ordered_pair : 	STRUCT 
		x : REAL;
		y : REAL;
	END_STRUCT;
	samples : 	STRUCT 
		length : INT;
		next : INT;
		num_taken : INT;
		pts : REFERENCE TO ordered_pair;
	END_STRUCT;
END_TYPE
