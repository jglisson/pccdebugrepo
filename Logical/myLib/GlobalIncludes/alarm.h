/************************************************************************/
/*
   File:  alarm.h

   Description: This file defines bit positions for each alarm/device.


      $Log:   F:\Software\BR_Guardian\alarm.h_v  $
 *
 *    Rev 1.1   Jan 30 2008 11:39:46   FMK
 * Changed reference for PVCS check in comment path.
 *
 *    Rev 1.0   Jan 11 2008 10:33:30   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#define  NUM_ALARM_STRINGS   100
#define  AlarmTextGroupIndex   5
/************************************************************************/
/* alarm device numbers for pulldown on pg181                           */
/************************************************************************/

#define BWH_ALM_DEV_NUM      13
#define MIXER_ALM_DEV_NUM    18


/************************************************************************/
/* alarm operating mode definitions (are bit defined)                   */
/************************************************************************/
#define PAUSE_MASK              0x01
#define MAN_MASK                0x04
#define AUTO_MASK               0x08
#define ALL_MASK                0x0f
#define RUNNING_MASK            (MAN_MASK|AUTO_MASK)

/************************************************************************/
/* alarm severity definitions (are bit defined)                         */
/************************************************************************/
#define INFORMATION_ONLY        ((unsigned char)(1<<0))  /* 1  */
#define GENERAL_ALARM           ((unsigned char)(1<<1))  /* 2  */
#define SYSTEM_SHUTDOWN         ((unsigned char)(1<<2))  /* 4  */
#define PRINT_ALARM             ((unsigned char)(1<<3))  /* 8  */
#define SUSPEND_SYSTEM          ((unsigned char)(1<<4))  /* 16 */
#define ALARM_OLD_CURRENT       ((unsigned char)(1<<5))  /* 32 */
#define ALARM_ACK               ((unsigned char)(1<<6))  /* 64 */
#define ALARM_OCCURRED          ((unsigned char)(1<<7))  /* 128*/

/************************************************************************/
/* alarm number definitions (NOTE - cannot have a #define xxxx 0)       */
/* these are used as OFFSET+DATA#, where OFFSET is an offset defined    */
/* in sys.h                                                             */
/************************************************************************/
/*                                     sys bwh mix gat liw ho  sho wth flf  description */
#define NO_ALARM                0

#define DRIVE_OVERSPEED         1   /*                  x   x               when drive goes over alarm_high_spd           */
#define EXCESSIVE_COASTING      2   /*      x           x   x               when device cannot control in auto            */
#define OUT_OF_SPEC             3   /*              x   x   x       x       when device goes outside out_of_spec_limit    */
#define DRIVE_UNDERSPEED        4   /*                  x   x               when drive goes under alarm_low_spd           */
#define DRIVE_SYSTEM_FAILURE    5   /*          x       x   x               when cannot detect drive running              */
#define DRIVE_SYSTEM_FAILURE_HO 6   /*                    x                 when cannot detect drive running              */
#define HOPPER_CRIT_LOW_TIME    8  /*              x                       out of material timeout alarm for autobatch      */

#define SYSTEM_SHUTDOWN_TYPE    20
#define GENERAL_ALARM_TYPE      21
#define INFORMATION_ONLY_TYPE   22
#define CHECK_PRINTER           23  /*  x                                   printer buffer is full                        */
#define INTERLOCK_OPEN          24  /*          x                           mixer door/user interlock is open             */
#define POWER_SUPPLY_FAILURE    25  /*  x                                   +24V monitor not detected                     */
#define HARDWARE_FAILURE        26  /*  x                                   misc catch all for system use                 */
#define LOW_BATTERY             27  /*  x                                   battery is low & must be changed              */
#define MIXER_MOTOR_FAILURE     28  /*          x                           mixer motor overload tripped                  */
#define AUXILIARY               29  /*  x                                   aux input alarm                               */
#define PUMP_FAILURE            30
#define EXD_PLC_LOW_BATTERY     31  /*  x                                   battery is low & must be changed              */
#define RESV_INPUT1_ALARM       32  /*  x                                                                                 */
#define RESV_INPUT2_ALARM       33  /*  x                                                                                 */
#define RESV_INPUT3_ALARM       34  /*  x                                                                                 */

#define HOPPER_SWITCH_FAILURE_ALARM  37  /*      x           x               x   hopper safety switch    */
#define HOPPER_PROX_HIGH_LEVEL  38  /*      x           x               x   hardware failure bit set in wm status word    */
#define HOPPER_PROX_LOW_LEVEL   39  /*      x           x               x   hardware failure bit set in wm status word    */
#define HOPPER_FAILURE          40  /*      x           x               x   hardware failure bit set in wm status word    */
#define HOPPER_RESET            41  /*                  x               x   sw reset bit set in wm status word            */
#define HOPPER_SW_INCOMPATABLE  42  /*                  x               x   sw in weigh module too old                    */
#define HOPPER_NOT_RESPONDING   43  /*                  x               x   no comm with wm                               */
#define HOPPER_IN_MANUAL        44  /*      x           x               x   wm is in manual backup                        */
#define HOPPER_OVERWEIGHT       45  /*      x           x               x   a/d is floored high                           */
#define HOPPER_LEVEL_LOW_WARNING     46  /*              x                       when material falls below proximity switch,  */
#define LOW_DUMP_SIZE           47  /*                  x                   avg dump size for hopper below alarm limit    */
#define HOPPER_LOW              48  /*              x   x               x   hopper weight below alarm_wt                  */
#define HOPPER_CRITICAL_LOW     49  /*              x   x               x   hopper weight below crit_wt                   */
#define LOSS_OF_SYNC            50  /*                  x                   no sync signal detected for 30 sec            */
#define HOPPER_UNDERWEIGHT      51  /*      x           x               x   a/d is floored low                            */

#define HOPPER_NOT_DUMPING      53  /*      x                               weigh hopper is not dumping                   */
#define VALVE_FAILURE           54  /*              x                       hopper not low & no weight gain detected      */
#define SWD_OUT_OF_SPEC_FAILURE 55  /* 28 UFS/Tim Peyton specific */
#define SPD_OUT_OF_SPEC_FAILURE 56  /* 29 UFS/Tim Peyton specific */

#define WIDTH_FAILURE           60  /*                              x       hardware failure bit set in width status word */
#define WIDTH_SW_INCOMPATIBLE   61  /*                              x       width controller sw is not compatable         */
#define WIDTH_NOT_RESPONDING    62  /*                              x       no comm with width controller                 */
#define WIDTH_IN_MANUAL         63  /*                              x       width controller in manual backup             */
#define WIDTH_PRODUCT_BREAK     64  /*                              x       width controller reports a product break      */
#define WIDTH_MEASUREMENT_ERROR 65  /*                              x       width controller has a meaurement error       */
#define WIDTH_OVER_MAXIMUM      66  /*                              x       width is over alarm_wth_max                   */
#define WIDTH_UNDER_MINIMUM     67  /*                              x       width is under alarm_wth_min                  */

#define DRIVE_FAILURE           70  /*                  x   x               hardware failure bit set in exd status word   */
#define DRIVE_RESET             71  /*                  x   x               sw reset bit set in exd status word           */
#define DRIVE_SW_INCOMPATABLE   72  /*                  x   x               sw in drive module is too old                 */
#define DRIVE_NOT_RESPONDING    73  /*                  x   x               no comm with exd                              */
#define DRIVE_IN_MANUAL         74  /*                  x   x               exd is in manual backup                       */
#define DRIVE_INHIBIT           75  /*                  x   x               drive is inhibited (no run light)             */
#define DRIVE_AD_OVERLOAD       76  /*                  x   x               analog input is over max or under min         */
#define MAT_REQ_UNCOVERED_FAILURE 77 /* 30 UFS/Tim Peyton specific */

/* The following are not alarms but events that can be logged when the operating mode changes                             */
#define RECIPE_CHANGED          90
#define DEVICE_IN_AUTOMATIC     91
#define DEVICE_IN_MANUAL        92
#define DEVICE_STOPPED          93
#define POWER_ON                94
#define POWER_OFF               95
#define MONITORING              96
#define DEVICE_BATCH_DONE       97
#define DEV_UNABLE_TO_RUN       98

/************************************************************************/
/* Status Bit Definitions for system wide alarms                        */
/*  shr_global.alarm_bits                                                       */
/************************************************************************/
#define SYS_SHUTDOWN                0x80000000    /* bit 32 */
#define GEN_ALARM                   0x40000000    /* bit 31 */
#define INFO_ALARM                  0x20000000    /* bit 30 */
#define CHECK_PRINTER_ALARM         0x10000000    /* bit 29 */
#define INTERLOCK_OPEN_ALARM        0x08000000    /* bit 28 */
#define PS_FAILURE_ALARM            0x04000000    /* bit 27 */
#define HARDWARE_FAILURE_ALARM      0x02000000    /* bit 26 */
#define BATTERY_LOW_ALARM           0x01000000    /* bit 25 *//* NOTE - conflicts with drive sys fail */
#define PUMP_FAILURE_ALARM          0x00800000    /* bit 24 */
#define AUXILIARY_ALARM             0x00400000    /* bit 23 */
#define EXD_PLC_LOW_BATTERY_ALARM   0x00200000    /* bit 22 *//* NOTE - exd plc only */
#define RESERVE_INPUT1_ALARM        0x00100000    /* bit 21 */
#define RESERVE_INPUT2_ALARM        0x00080000    /* bit 20 */
#define RESERVE_INPUT3_ALARM        0x00040000    /* bit 19 */

/************************************************************************/
/* Status Bit Definitions for bwh alarms                                */
/*  shr_LOOP[WEIGH_HOP_LOOP].BWH->alarm_bits                            */
/************************************************************************/

/************************************************************************/
/* Status Bit Definitions for width controller alarms                   */
/*  shr_LOOP[WTH_LOOP].WTH->alarm_bits                                  */
/************************************************************************/
#define WTH_FAIL                0x00000001
#define WTH_INCOMPATABILITY     0x00000004
#define WTH_TIMEOUT             0x00000008
#define WTH_IN_MANUAL           0x00000010
#define WTH_PRODUCT_BREAK       0x00000020
#define WTH_MEASUREMENT_ERROR   0x00000040
#define WTH_OVER_MAXIMUM        0x00001000
#define WTH_UNDER_MINIMUM       0x00002000
/*   this bit is also defined
#define DEV_OUT_OF_SPEC         0x00200000
*/

/************************************************************************/
/* Status Bit Definitions for per device alarms. Not all alarms are used*/
/*   for each device                                                    */
/************************************************************************/

#define EXD_BOARD_FAIL			0x00000001    /* 1 */
#define EXD_RESET             0x00000002    /* 2 */
#define EXD_INCOMPATABILITY	0x00000004    /* 3 */
#define EXD_TIMEOUT           0x00000008    /* 4 */ /* EXD has timed out    */
#define EXD_AD_OVERLOAD       0x00000010    /* 5 */
#define EXD_INHIBIT           0x00000020    /* 6 */
#define EXD_IN_MANUAL         0x00000040    /* 7 */
#define WT_CRIT_TIME          0x00000080    /* 8 */  /* WM CRITICAL LOW ALARM for AutoBatch */

#define WM_BOARD_FAIL         0x00000100     /* 9 */
#define WM_RESET              0x00000200     /* 10 */
#define WM_INCOMPATABILITY    0x00000400     /* 11 */
#define WM_TIMEOUT            0x00000800     /* 12 */ /* WM has timed out     */
#define WM_OVERWEIGHT         0x00001000     /* 13 */
#define DEV_SETTLE_TIME       0x00002000     /* 14 */
#define WM_IN_MANUAL          0x00004000     /* 15 */
#define WM_LOSS_OF_SYNC       0x00008000     /* 16 */

#define DUMP_LOW              0x00010000     /* 17 */ /* dump size low        */
#define WT_CUTBACK            0x00010000     /* 17 */ /* for fluff, same bit! */
#define WT_LOW                0x00020000     /* 18 */ /* below low alarm wt   */
#define WT_CRIT               0x00040000     /* 19 */ /* below crit low wt    */
#define WM_UNDERWEIGHT        0x00080000     /* 20 */ /* wm is under min wt   */

#define HOPPER_PROX_HIGH      0x00400000     /* 23 */ /* HOPPER PROX HIGH     */
#define HOPPER_PROX_LOW       0x00800000     /* 24 */ /* HOPPER PROX LOW      */
#define HOPPER_SWITCH_FAILURE 0x01000000     /* 25 */ /* HOPPER_SWITCH_FAILURE */
#define DEV_UNDERSPEED        0x00100000     /* 21 */ /* EXD underspeed       */
#define DEV_OUT_OF_SPEC       0x00200000     /* 22 */ /* out of spec alarm    */
#define DEV_EXCESSIVE_COAST   0x00400000     /* 23 */ /* too much coasting    */
#define DEV_OVERSPEED         0x00800000     /* 24 */ /* EXD overspeed        */
#define MIXER_MOTOR_ALARM     0x00800000     /* 24 */ /* mixer motor failure  */
#define DEV_DRV_SYS_FAILURE   0x01000000     /* 25 */ /* drive system failure */
#define MIX_VALVE_FAILURE     0x02000000     /* 26 */ /* valve failure        */
#define DEV_NOT_DUMPING       0x04000000     /* 27 */ /* batch hopper not dumping */
#define SWD_OUT_OF_SPEC       0x08000000     /* 28 UFS/Tim Peyton specific */
#define SPD_OUT_OF_SPEC       0x10000000     /* 29 UFS/Tim Peyton specific */
#define MAT_REQ_UNCOVERED     0x20000000     /* 30 UFS/Tim Peyton specific */
#define HOPPER_LEVEL_LOW      0x40000000     /* 31 Hopper Prox Level Low   */
#define ALL_ALARM_BITS        0xffffffff

/************************************************************************/
/* Status Bit Definitions for per card status bits                      */
/************************************************************************/
#define NETNOISE      0x8000                     /* generic              */
#define LASTRXERR     0x4000                     /* generic              */
#define NOPOLL        0x2000                     /* generic              */
#define INVALID_CMD   0x1000                     /* generic              */
#define INVALID_OP    0x0800                     /* generic              */
#define BOARD_FAIL    0x0400                     /* generic              */
#define BOARD_RESET   0x0200                     /* generic              */
#define AD_OVERLOAD   0x0080                     /* generic              */
#define IN_MANUAL     0x0040                     /* generic              */
#define AD_TIMEOUT    0x0020                     /* A/D Not responding   */
#define HOP_OVERFILL  0x0010                     /* wm only              */
#define HOP_LOWLEVEL  0x0008                     /* Low level on BG downcomer */
#define AD_SYNC_LEV   0x0004                     /* Hopper SYNC_LEVel input status */
#define AD_CAL_FLAG   0x0002                     /* Indicates calibration in progress */
#define EXT_SPEED_REF 0x0001                     /* speed reference control 1=ext  0=int */
#define DRV_INHIBIT   0x0020                     /* exd only             */

#define SYSALARM_TABLEOFFSET     0
#define BWHALARM_TABLEOFFSET     32
#define MIXALARM_TABLEOFFSET     64
#define HOPALARM_TABLEOFFSET     96
#define EXDALARM_TABLEOFFSET     480
#define LIWALARM_TABLEOFFSET     512
#define HO_ALARM_TABLEOFFSET     544
#define SHOALARM_TABLEOFFSET     576
#define FLFALARM_TABLEOFFSET     608
#define GFLFALARM_TABLEOFFSET    640
#define WTHALARM_TABLEOFFSET     672


