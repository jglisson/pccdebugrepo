#include "pcc_math.h"
#include <AsBrStr.h>

signed short remove_sample(samples *s_data, signed short idx)
{
    ordered_pair *start;
    unsigned long len;

    if (idx < s_data->length)
    {
        start = s_data->pts + idx;
        len = (unsigned long)(s_data->length - idx - 1) * sizeof(ordered_pair);
        (void)brsmemmove((UDINT)start, (UDINT)(start+1), (UDINT)len);

        if (--s_data->next < 0) 
           s_data->next = 0;
        --s_data->num_taken;
    }

    return(0);
}


