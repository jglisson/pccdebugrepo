#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  interface.c

   Description: This file controls the visualization and is the main
      interface to the visualization.


      $Log:   F:\Software\BR_Guardian\interface.c_v  $
 *
 *    Rev 1.30   Jun 08 2016 14:57:00   zcl
 * Auxiliary timer should cap at 99 secs instead of (mixer time - 1000) / 1000
 *
 *    Rev 1.29   Sep 15 2008 13:16:44   gtf
 * Fixed debug layer visibility
 *
 *    Rev 1.28   Sep 03 2008 10:41:02   gtf
 * Finalized bug fix to fire gate secondary (if enabled) in manual backup screen
 *
 *    Rev 1.27   Aug 25 2008 08:30:12   gtf
 * added range reset on debug layer language display variable
 *
 *    Rev 1.26   Jul 17 2008 16:39:18   gtf * Added support for 4PP045.0571-K15 PCC specific panel version
 *
 *    Rev 1.11   July 17,  2008 15:05:34   GTF
 * Added support for 4PP045.0571-K15 PCC version of B&R Panel
 *
 *    Rev 1.25   May 16 2008 10:27:06   vidya
 * Made changes to display recipe panel depending on
 * extruder mode.
 *
 *    Rev 1.24   May 16 2008 09:41:44   Barry
 * Added setting of EXT_HO_MODE
 *
 *    Rev 1.23   May 09 2008 15:46:38   FMK
 * Changed page references for monitor.
 *
 *    Rev 1.22   May 07 2008 08:59:22   Barry
 * Fixed a problem with stop at end of batch
 *
 *    Rev 1.21   Apr 22 2008 09:03:02   Barry
 * Modified calibration to set the calibrating flag
 * so that a/d errors will not be reported during calibration.
 *
 *    Rev 1.20   Apr 18 2008 08:27:54   vidya
 * Changed the sequence in which BWH receives
 * AUTO command.
 *
 *
 *    Rev 1.19   Apr 16 2008 13:09:24   vidya
 * Added code to display pop-up windows when
 * Auto is pressed.
4 *
 *    Rev 1.17   Apr 02 2008 13:22:00   FMK
 * Finished reconcile for simulation pages and
 * popups not showing up on main page after boot.
 *
 *    Rev 1.16   Apr 02 2008 10:16:38   FMK
 * HOCommand accel decel parameters were
 * seperated into two seperate values.
 *
 *    Rev 1.15   Apr 01 2008 13:43:28   Barry
 * Fixed the Pause at end of batch
 *
 *    Rev 1.14   Mar 24 2008 13:04:32   FMK
 * Added code for starting and stopping the extruder
 * haul off devices.
 *
 *    Rev 1.13   Mar 19 2008 14:54:54   FMK
 *
 *
 *    Rev 1.12   Mar 18 2008 14:53:30   FMK
 * Broke out the processing of the mode buttons,
 * to their own function. This will allow common
 * processing of these buttons from multiple pages.
 *
 *    Rev 1.12   Mar 18 2008 14:44:18   FMK
 * Made seperate function for processing the
 * system mode buttons. Now this processing
 * can be common among several pages. Also
 *
 *    Rev 1.11   Mar 13 2008 13:08:30   vidya
 * Modified the page number for the last recipe page.
 *
 *    Rev 1.10   Mar 12 2008 14:25:28   Barry
 * Modified to send oper mode commands to the BWH loop
 *
 *    Rev 1.9   Mar 07 2008 10:34:38   FMK
 * Changed reference of wtp_mode and ltp_mode.
 * Code cleaning.
 *
 *    Rev 1.8   Feb 29 2008 14:14:00   FMK
 * Added check for printer attached to usb. Affects
 * the visualization for the printer enable button.
 *
 *    Rev 1.7   Feb 11 2008 14:59:54   FMK
 * Modified code to correctly acknowledge the alarms
 * and clear the alarm history.
 *
 *    Rev 1.6   Feb 06 2008 10:42:42   FMK
 * Got the calibration working for the extruder
 * weigh hopper.
 *
 *    Rev 1.5   Jan 28 2008 13:52:56   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.4   Jan 25 2008 14:07:28   FMK
 * Made changes to the setup pages to allow the
 * user to store/restore configuration files to a thumb
 * drive installed in the USB ports.
 *
 *    Rev 1.3   Jan 18 2008 09:03:24   FMK
 * Changed global variables names to account for naming
 * system enacted.
 *
 *    Rev 1.2   Jan 15 2008 15:25:14   FMK
 * Added signal to store_config. The entered setup
 * signal now does not start the store config process.
 * When returned to the main page it will then trigger
 * the store_config signal which will start the config
 * store process.
 *
 *
 *    Rev 1.1   Jan 14 2008 11:02:50   FMK
 * Changed the signal variables to a structure format.
 *
 *    Rev 1.0   Jan 11 2008 10:33:36   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include "globalinc.h"
#include "hmi.h"
#include "units.h"
#include "plc.h"
#include "signal_pcc.h"
#include "alarm.h"
#include "vars.h"
#include "relaydef.h"
#include "rangedef.h"
#include "printerdef.h"
#include "version.h"
#include "recipe.h"
#include "remote.h"
#include "pcc_math.h"
#include "gate_pcc.h"
#include "AsRfbExt.h"

extern void run_ext();
extern void Setup_Main();
extern void Totals_Main();
extern void Monitor_Main();
extern void MonitorHistogram();
extern void Security_Main();
extern void Print_Main();
extern void Calibrate_Main();
extern void Recipe_Main();
extern void Alarm_Main();
extern void init_manual();
extern void ExtruderManual();
extern void MonitorVisibility();
extern void clear_recipe(recipe_struct *r, int what);
extern TIME start_time(void);
extern void Recipe_Init();
extern void default_alarms();

void MonitorVisibilityIcons(void);
void clear_recipe_totals(void);
void clear_shift_totals(void);
void io_reset();
void Regrind_Main(void);
void stop_ext(void);
void SetContrast_Brightness();
void process_leds(void);
void link_to_data();
void SetSecurity();
void SetSystemVisibility();
void SetDisplayUnits();
void AcknowledgeAlarms();
void ClearAlarmHistory();
void set_manual(void);
void pg000_Func_MainPanel();
#ifdef CRYPT_ENABLED
void vPg007_Func_LicenseKey_Main();
void vCalculateLicenseKey();
#endif
void vPg007_Func_InitPage();
#ifdef TEST_BUTTONS
void vPg999_InHouseTesting_Main();
#endif
int  pg004_Func_InitPage();
void vPg990_SetupWizzardMain();
void pg005_Manual_BackupPage();
void process_mode_buttons();
void usb_init();
void process_Estop_alarm(); //G2-662
USINT iVal(char);
BOOL bValidJump(UINT);

//void process_reserve_input_alarms();
void process_relay();
void extrusion_interface();
void hopper_visibility_update();
void manual_backup_keyswitch_check();
void clear_manual_backup_PopUp();
void run_ext_remote();
// G2-337 4/19/17 added functions
void page_completion(unsigned int);				//Dragan
void buttons_manual_reset();					   //Dragan
void close_all_gates_GTF();						//Dragan

extern char CannotRunRecipeBig_Ack;
extern char CannotRunRecipeSmall_Ack;
extern char CannotRunRecipeSmall_Change;
extern char CannotRunRecipeBig_Change;
extern int  err_ext;
/* extern int ROnly_File(void); AdBi not needed any longer; calculation is done in this file*/

extern void visible(unsigned char *Runtime, unsigned char state);
extern void Diagnostics_Main();
extern void copy_recipe(recipe_struct *, recipe_struct *, int);
extern int  save_time_weight_table_to_csv(void);
extern char *crypt_r(const char *key, const char *salt, char *buf);

extern void Change_Pause_Remote();
extern void Change_Auto_Remote();
extern void Change_Manual_Remote();
extern void Change_Recipe_Remote();
extern void pg453_Hopper_Visibility(void);
extern void pg450_Hopper_VisibilityIcons(void);
extern void Pop_InValid(int);

extern void Change_Ext_Pause_Remote();
extern void Change_Ext_Auto_Remote();
extern void Change_Ext_Manual_Remote();
extern void Change_Ext_Recipe_Remote();

extern int Store_Recipes(void);
extern int Restore_Recipes(void);
extern void set_recipe_gate_order(recipe_struct* current_recipe);

struct PopupActiveStruct Popup; /* make sure popups only for specific pages are allowed */

unsigned char gVC_ChangeDisplay = FALSE;
_GLOBAL int PageNow;
_LOCAL  int Interlock_Open_Ack;
_LOCAL  unsigned char pg005_ShowAck;
USINT   u8MacAddressTimeOut;
unsigned char recipe_confirmed;    /* too big or too small recipe has been confirmed to implement anyway */

_GLOBAL int pg990FirstInit;
_GLOBAL CfgGetMacAddr_typ Mac;
_GLOBAL plcstring DevName[20+1];
_GLOBAL unsigned char MacAdr[6];
#ifdef CRYPT_ENABLED
_GLOBAL unsigned char sCalculatedKey[31];
#endif
RTCtime_typ gRTCStartTime,gRTCNextTime;  /* elapsed time variables for status popup */
_GLOBAL char purge_button_visible; 

UDINT  gVCHandle;
UDINT  VCHandle;
UINT   State_ClearAlarmList;
unsigned char gFlag_SetHMIConfig;
unsigned char gFlag_AckAlarms;
unsigned char gFlag_ClearAlarmHistory;
unsigned char gFlag_ShowInitPage;

static int    pg024Flashctr; //G2-401

_GLOBAL  UINT    Status_RfbExtSetClientCaption;
_LOCAL   AsRfbExtType   pLib;
_LOCAL   UINT    Status_RfbExtInit, Status_RfbExtConnect;
_LOCAL   UDINT   uiTimeOut;
_GLOBAL unsigned char NoRateSpd_Ack;
_LOCAL unsigned char NoCalEXT_Ack;
_LOCAL unsigned char internal_loading_flag;  /* indicate whether internal loading selection has been changed */
_LOCAL unsigned char keyswitch_pause_mode_flag;	/* indicate whether we need the pg000_PopupGateSwitchRuntime */
_LOCAL unsigned char keyswitch_manual_mode_flag;	/* indicate whether we need the pg000_PopupGateSwitchRuntime */
//_GLOBAL int gFinalSpeed;
_GLOBAL unsigned char density_may_changed; 
_GLOBAL unsigned char BlenderRecipeChanged; 

_LOCAL unsigned char regring_back_in_batch;  /* indicate whether regrind will get back into the recipe if low level prox covered */

/* global variable for debug information */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;
_GLOBAL char gIPAddressMainPage[16];

void SET_ALARM(int Device, unsigned int Alarm);
void CLEAR_ALARM(int Device, unsigned int Alarm);

static BOOL pg034First; //G2-596

// test to see what var. will be unused
#ifdef C70_003
_GLOBAL int c70_003_flag;
#endif
#ifdef C70
_GLOBAL int c70_flag;
#endif
#ifdef PP65
_GLOBAL int pp65_flag;
#endif

/************************************************************************/
/*                                                                      */
/*   Function:  intrface_init()

   Description:  defaults the system and brings it out of reset.
                                                                        */
/************************************************************************/
_INIT void interface_init(void)
{
	USINT i;

   pg034First = 0; //G2-596

   //G2-401
	for (i=0; i<12; i++)
   {
		G_au8LoadingSignal[i]=0xff;
      visible(&HMI.pg024_loadreqvis[i], FALSE); //G2-401
      visible(&HMI.pg024_load[i], FALSE);       //G2-401
   }
   pg024Flashctr = 0;

	for (i=0; i<20; i++)
   {
		HMI.pg990_au8BlenderSizeAvailability[i] = 2;
   }
	HMI.pg990_au8BlenderSizeAvailability[0] = 0;
	HMI.pg990_au8BlenderSizeAvailability[1] = 0;
	HMI.pg990_au8BlenderSizeAvailability[3] = 0;
	HMI.pg990_au8BlenderSizeAvailability[9] = 0;
	HMI.pg990_au8BlenderSizeAvailability[14] = 0;
	HMI.pg990_au8BlenderSizeAvailability[19] = 0;
   	
	gFlag_ShowInitPage = TRUE;
	gFlag_AckAlarms = FALSE;
	gFlag_ClearAlarmHistory = FALSE;
	HMI.ChangeDevice = gNeutralDevice;
	HMI.CurrentDevice = 0;
	HMI.Request_SYSReset = 0;
	visible(&HMI.pg000_RestartRequiredRuntime,FALSE);
	visible(&HMI.pg000_PopupNoRateSpd,FALSE);
	visible(&HMI.pg000_PopupNoCalEXT,FALSE);
	visible(&HMI.pg000_CannotRunRecipe_Big,FALSE);
	visible(&HMI.pg000_CannotRunRecipe_Small,FALSE);
   
	HMI.pg435_bGoPressed = FALSE;
	for (i=0; i<6; i++)
		HMI.pg435_bGatesToTest[i] = FALSE;
	HMI.pg435_u16FirstDelay = 0;
	HMI.pg435_u16LastDelay = 400;
	HMI.pg435_u8DelayStep = 100;
	HMI.pg435_u16FirstTime = 200;
	HMI.pg435_u16LastTime = 500;
	HMI.pg435_u8TimeStep = 100;
	HMI.pg435_u8AveragePoints = 3;
	HMI.pg000_PopupRecipeUseRuntime = 1;
	HMI.pg000_PopupGateSwitchRuntime = 1;
	HMI.pg000_CalNeedDoneRuntime = 1;

	gVCHandle = 0;
	gFlag_SetHMIConfig = FALSE;
	HMI.InvalidTouchColor = gGreen;
	HMI.pg004_InitTextIndex = 0;
	usb_init();
	u16PageToGoTo = 0xffff;
   
	u8MacAddressTimeOut  = 0;
	u8ReadPanelIdTimeout = 0;

#ifdef  CRYPT_ENABLED
	CryptCheck = FALSE; /* set to FALSE to go through checking the crypt key */ 
#else
	CryptCheck = TRUE;
#endif
   
	debugFlags.time_weight_to_csv = 0;
   
	pg175_sys_size_change = 0;

	remote_manual_write_flag = FALSE;	
	remote_recipe_write_flag = FALSE;
	remote_auto_write_flag = FALSE;
	remote_pause_write_flag = FALSE;
	remote_OneTimeAuto_write_flag = FALSE;
	remote_pause_batch_write_flag = FALSE;
	u8ButtonsTestState = 0;
	
	pg991StartButtonTest = 0;
	pg991StopButtonTest = 0;
	
	remote_ext_manual_write_flag = FALSE;	
	remote_ext_recipe_write_flag = FALSE;
	remote_ext_auto_write_flag = FALSE;
	remote_ext_pause_write_flag = FALSE;
   
	uiTimeOut = 500;
	Status_RfbExtInit = RfbExtInit((UDINT)&pLib, uiTimeOut);
	keyswitch_manual_mode_flag = TRUE;
	keyswitch_pause_mode_flag = TRUE;
} 

/************************************************************************/
/*
   Function:  main_cycle()

   Description:  main cyclic task for the interface function
                                                                        */
/************************************************************************/
_CYCLIC void interface_cycle(void)
{
	int Result;
	int loop; 
	int i = 0;
#ifdef CRYPT_ENABLED
	char cCryptOK;
#endif
	config_super *cs = &cfg_super;
	unsigned char TmpStr[6];	
	
	// restart button
	
	if ((BLN_OPER_MODE == PAUSE_MODE)) 
	{
		if ((BLN_OPER_MODE != MAN_MODE) && (BLN_OPER_MODE != AUTO_MODE))
			HMI.u8LockedIfBlndNotStopped = 0;
	}
	else
		HMI.u8LockedIfBlndNotStopped = 1;
	
	if ((Signal.XML_read == FALSE) && (Signal.XML_store == FALSE))       /* only do this when not in XML r/w mode */
	{
		if (Blender_Type != cfg_super.current_blender_type) 
		{
			if (pg175_sys_size_change)
			{
				if (cfg_super.current_blender_type == AUTOBATCH)
				{
					brsstrcpy((UDINT)cfg_super.device_name, (UDINT)"AUTOBATCH");
					cfg_super.pg175_System_Size = 11; /* 15kg */ 
				   
					for (loop = 0; loop<cfg_super.total_gates; loop++)
					{
						cfgGATE[loop].prepulse_enabled = FALSE;
						cfgGATE[loop].proximity_switch.chan = 115+loop;
						cfgGATE[loop].proximityswitch_exist = TRUE;
					 
						if (loop == 0)
							cfgGATE[loop].hopper_type = STANDARD_MAIN;       
						else
							cfgGATE[loop].hopper_type = MEDIUM;
					 
						cfgGATE[loop].OOM_Alarm_Time = 30000;
					  
						if (loop == 0)
							cfgGATE[loop].weight_settle_time = 1000;              /* 1000ms */
						else 
							cfgGATE[loop].weight_settle_time = 2000;              /* 2000ms */
					 
						if (cfgGATE[loop].hopper_type == MEDIUM)
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_MEDIUM;
						else if (cfgGATE[loop].hopper_type == SMALL)  
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_SMALL;
						else if (cfgGATE[loop].hopper_type == REGRIND)
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_REGRIND;
						else if (cfgGATE[loop].hopper_type == STANDARD_MAIN)
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_STANDARD;
						else if (cfgGATE[loop].hopper_type == SCR_MEDIUM)
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME;
						else if (cfgGATE[loop].hopper_type == SCR_SMALL)
							cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME; 
					 
						cfgGATE[loop].PreFeed_Enabled   = 0;	 
						cfgGATE[loop].allowdump_proxoff = 1;
					}		 
				 
					cfg_super.gate_order_in_recipe = 0;
					cfgMIXER.bypass_mixer_prox_switch = TRUE;
					cfgMIXER.material_request_source = MATERIAL_REQUEST_EXTERNAL; 		 
					cfgMIXER.dump_cycle_time = 60.0;
					cfgMIXER.gate_close_delay = 5000;
					cfg_super.gate_order_in_recipe = 0;
               cfg_super.pg2180_aux5_invert = 0;
				}
				else
				{
					brsstrcpy((UDINT)cfg_super.device_name, (UDINT)"GUARDIAN");
					cfg_super.pg175_System_Size = 3; /* 15kg */
					/*pg175_sys_size_change = 1;*/
				 
					for (loop = 0; loop<cfg_super.total_gates; loop++)
					{
						cfgGATE[loop].prepulse_enabled = TRUE;
						cfgGATE[loop].proximity_switch.chan = 115+loop;
						cfgGATE[loop].proximityswitch_exist = FALSE;
						cfgGATE[loop].hopper_type = NORMAL;  
					}
				 
					cfgGATE[loop].weight_settle_time = 1000;              /* 1000ms */
				 
					cfgMIXER.bypass_mixer_prox_switch = FALSE;
					cfgMIXER.material_request_source = MATERIAL_REQUEST_MIXER_PROX;
				 
					if (cfgGATE[loop].hopper_type == NORMAL)
						cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME;
					else if (cfgGATE[loop].hopper_type == REGRIND)
						cfgGATE[loop].minimum_gate_time = MIN_GATE_TIME_REGRIND;
				 
					cfgMIXER.dump_cycle_time = 15.0;
					cfgMIXER.gate_close_delay = 0;
					cfgGATE[loop].hopper_type = NORMAL;
               cfg_super.pg2180_aux5_invert = 1;
				}
				/*default_alarms();*/
			}
			Blender_Type = cfg_super.current_blender_type;    
		}
	}
	
	clear_manual_backup_PopUp();
	manual_backup_keyswitch_check();
	process_leds();
	
	if (cfg_super.clear_shift_total_flag == 1)
		cfg_super.job_wt_in_recipe = 1;
	
	if (PMEM.recipe_num == 0) 
	{
		PMEM.recipe_num = 1;
		BATCH_RECIPE_NEW->index = 1;
		BATCH_RECIPE_NOW->index = 1;
		strcpy(PMEM.recipe_name, "Recipe");
		HMI.pg453_RecipeNum = 1;
	}
	
#if 0	
   //process_reserve_input_alarms();
#endif

   if (!cfg_super.demo)
   {
      process_Estop_alarm(); //G2-662
  	   // per Rich A. for John Beal gate alarm door normally-closed relay
      if (cfg_super.pg990_HopperDoorSwitch) //G2-558
	   {
		   if (IO_dig[0].channel[RESV_IN_5] == ON)
			   CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
		   else
			   SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
	   }
   }
   else
   {
	   CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
   }
   
	
	process_relay();
	extrusion_interface();

	if (Signal.Store_Timewt_as_CSV == TRUE)
	{
		Result = save_time_weight_table_to_csv();
		switch (Result)
		{
			case SUCCESS:
				Signal.Store_Timewt_as_CSV = FALSE;
				break;
			case NOTDONE:
				break; 				
			default:
				Signal.Store_Timewt_as_CSV = FALSE;
				break;
		}
	}
   
	if (u16PageToGoTo != 0xffff) /* someone is using the shortcut button */
	{
		if (bValidJump(u16PageToGoTo)) 
         HMI.ChangePage = u16PageToGoTo; /* check if ok to jump to this page*/
		u16PageToGoTo = 0xffff; /* set to invalid jump until someone uses the shortcut button again*/
	}

	if (Signal.StoreRecipes)
	{
		Result = Store_Recipes();
		switch (Result)
		{
			case SUCCESS:
				Signal.StoreRecipes = FALSE;
				Signal.FILEIO_INUSE = FALSE;
				break;
			case NOTDONE:
				break;
			case FAIL:
			default:
				Signal.StoreRecipes = FALSE;
				Signal.FILEIO_INUSE = FALSE;
				break;
		}
	}
   
	if (Signal.RestoreRecipes == TRUE)
	{
		Result = Restore_Recipes();
		switch (Result)
		{
			case SUCCESS:
				Signal.RestoreRecipes = FALSE;
				HMI.Load_status[RECIPE_STATUS] = STRUCTURE_LOADED;
				HMI.pg453_Recipe_Action = RECIPE_NOACTION;
				copy_recipe(&BATCH_RECIPE_EDIT, &RECIPES->entry[PMEM.recipe_num], BLENDER_RECIPE);
				Signal.FILEIO_INUSE = FALSE;
				break;
			case NOTDONE:
				break;
			case FAIL:
			default:
				Signal.RestoreRecipes = FALSE;
				HMI.Load_status[RECIPE_STATUS] = STRUCTURE_DEFAULT;
				HMI.pg453_Recipe_Action = RECIPE_NOACTION;
				Signal.FILEIO_INUSE = FALSE;
				break;
		}
	}
	
	if (debugFlags.manualPgTest == TRUE)
	{
		debugFlags.manualPgTest = FALSE;
		shr_global.pause_pending = FALSE;
		process_leds();
	}
	
	if ((shr_global.pause_pending == 0) && (remote_pause_batch_write_flag))
	{
		remote_pause_batch_write_flag = FALSE;
		shr_global.bln_oper_mode = PAUSE_MODE;
	}

	if (remote_pause_write_flag == TRUE)
	{
		remote_pause_write_flag = FALSE;
		Change_Pause_Remote();
	}

	if (remote_manual_write_flag == TRUE)
	{
		remote_manual_write_flag = FALSE;
		Change_Manual_Remote();
	}
	
	if (remote_recipe_write_flag == TRUE)
	{
		remote_recipe_write_flag = FALSE;
		Change_Recipe_Remote();
	}
	
	if (remote_auto_write_flag == TRUE)
	{
		remote_auto_write_flag = FALSE;
		Change_Auto_Remote();
	}

	if (remote_ext_pause_write_flag == TRUE)
	{
		remote_ext_pause_write_flag = FALSE;
		Change_Ext_Pause_Remote();
	}

	if (remote_ext_manual_write_flag == TRUE)
	{
		remote_ext_manual_write_flag = FALSE;
		Change_Ext_Manual_Remote();
	}

	if (remote_ext_recipe_write_flag == TRUE)
	{
		remote_ext_recipe_write_flag = FALSE;
		Change_Ext_Recipe_Remote();
	}

	if (remote_ext_auto_write_flag == TRUE)
	{
		remote_ext_auto_write_flag = FALSE;
		Change_Ext_Auto_Remote();
	}	
   
	if (Blender_Type == AUTOBATCH)
	{
		visible((unsigned char *)&HMI.pg017_AutoBatch_display, TRUE);
		visible((unsigned char *)&HMI.pg017_Guardian_display, FALSE);
		if ((Signal.XML_read == FALSE) && (Signal.XML_store == FALSE))
		{
			cfgGATE[0].hopper_type = STANDARD_MAIN;  
			brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION_AB); 
		}
	}	
	else
	{
		visible((unsigned char *)&HMI.pg017_Guardian_display, TRUE);
		visible((unsigned char *)&HMI.pg017_AutoBatch_display, FALSE);
		if ((Signal.XML_read == FALSE) && (Signal.XML_store == FALSE))
			brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION);
	}
   
	/* GTF reset key index for Lang_next to 0 after cycling through list */
	if (HMI.pg000_DisplayLanguage == 7)
		HMI.pg000_DisplayLanguage = 0;

	if (reboot_SetSecurity == TRUE)
   {
		visible(&HMI.pg000_Debug_Layer_On, FALSE);   
		visible(&HMI.pg000_Lang_Layer_On, FALSE); /* Turn off language layer GTF */   
		SetSecurity();  /* Visibility might change as a result of security change */
		reboot_SetSecurity = FALSE;
	}

	//if (PLC.TouchPanel.ModuleID == 0x00)
	//{
	//   HMI.ChangePage = pg004_InitPage;
	//	  HMI.InvalidTouchColor = gYellow;
	//	  HMI.pg004_InitTextIndex = 1;
	//	  return;
	//}
   //   else if((PLC.TouchPanel.ModuleID != ModID_4PP045PCC)&&(HMI.CurrentPage != pg210_Setup_System_Ethernet))
	//{
	//   HMI.ChangePage = pg004_InitPage;
	//	  HMI.InvalidTouchColor = gRed;
	//	  HMI.pg004_InitTextIndex = 1;
	//	  return;
	//}

	/* if the pp045 code does not match the PCC defined code, then the system
	will stay on the init screen and display 'InValid Touch Panel' message */

	if (HMI.CurrentPage == pg004_InitPage)
	{
		Result = pg004_Func_InitPage();

		//if (Result == ERROR)
		//   return;  //for now just let the blender run even if the pannel ID is not a match
		
		/* go to main page */
		HMI.CurrentPage = pg000_MainPage;
		HMI.ChangePage  = pg000_MainPage;
	}
#ifdef CRYPT_ENABLED
	if (HMI.CurrentPage == pg007_LicenseKey)
	{
		/*the device may come back to this page but without going through init phase.
		** The following line checks for that and in that particular case calls the init first*/
		if ((brsstrlen((UDINT)sSerialNumber) < 1) || (brsstrlen((UDINT)sMacAddr) < 1)) 
			vPg007_Func_InitPage();
		else
			vPg007_Func_LicenseKey_Main();
	}
#endif
	
	/* Demo mode on the main page */
	if (cfg_super.demo == TRUE)
		visible(&HMI.SimulationPopupRuntime, TRUE);
	else
		visible(&HMI.SimulationPopupRuntime, FALSE);
	/* end Demo mode*/
	
	/* When use external material_request_source we need to bypass the mixer_prox_switch */
	//if (cfgMIXER.material_request_source == MATERIAL_REQUEST_EXTERNAL)
	//   cfgMIXER.bypass_mixer_prox_switch = 1;
	
	/* If use external material_request_source and bypass the mixer_prox_switch we need to add a delay to close gate */
	if ((cfgMIXER.material_request_source == MATERIAL_REQUEST_EXTERNAL) && (cfgMIXER.bypass_mixer_prox_switch == 1) && (cfg_super.current_blender_type != AUTOBATCH))
   {
      //G2-675
      if (cfgMIXER.gate_close_delay <= 0)
		   cfgMIXER.gate_close_delay = 5000;
   }
	
	/* AdBi setup wizard begin */
	if (HMI.CurrentPage == pg990_SetupWizzard)
	{
      if (HMI.pg990_HopperDoorSwitchSet) // == 'S') //G2-558
      {
         cfg_super.pg990_HopperDoorSwitch = HMI.pg990_HopperDoorSwitch;
         Signal.EnteredSetup = TRUE;
         Signal.StoreConfig  = TRUE;
         HMI.pg990_HopperDoorSwitchSet = 0;
      }
      if (pg990FirstInit == 0) //G2-558
      {
         pg990FirstInit = 1;
         HMI.pg990_HopperDoorSwitch = cfg_super.pg990_HopperDoorSwitch;
      }
		vPg990_SetupWizzardMain(); 
	}
	/* AdBi setup wizard end */
	
	/* per Holger in Germany */
	
	if ((cfg_super.self_cleaning) && (HMI.CurrentPage == pg005_BlendManualBackup1))
		HMI.pg005_SelfCleaningBtn = 0;
	else
		HMI.pg005_SelfCleaningBtn = 1;
	
	if (cfg_super.self_cleaning == FALSE)
		cfg_super.self_cleaning_flag = FALSE;
	
	/* Main button pg005 disable when using blender in manual */
	if ((hmi_pg005_controls[16] == 1) && (HMI.CurrentPage == pg005_BlendManualBackup1)) 
		HMI.MainRuntime = 1;
	else if ((hmi_pg005_controls[16] == 0) && (HMI.CurrentPage == pg005_BlendManualBackup1))
		HMI.MainRuntime = 0;
  
	/* Main button pg005 disable when using self cleaning in manual */
	if ((cfg_super.self_cleaning_flag == TRUE) && (HMI.CurrentPage == pg005_BlendManualBackup1))
	{
		HMI.MainRuntime = 1;
		HMI.SelfCleanLockRuntime = 1;
      HMI.SelfCleanPopup = 0;
	}
	else if ((cfg_super.self_cleaning_flag == FALSE) && (HMI.CurrentPage == pg005_BlendManualBackup1))
	{
		HMI.MainRuntime = 0;
		HMI.SelfCleanLockRuntime = 0;
      HMI.SelfCleanPopup = 1;
	}
	
	/* if blender in operator mode hide recipe order from recipe page per Tim Payton */
	if (HMI.pg310_SecurityLvlIndex == 1)
	{	
		if (cfg_super.gate_order_in_recipe)	
		{
			for (i=0; i < MAX_GATES; i++)
			{
				if (i < cfg_super.total_gates)
					HMI.pg450_RecipeOptionalOrder[i] = 0;
				//HMI.pg453_Gate_Order_Index[BATCH_RECIPE_EDIT.gate_order[i].number] = 0;				
			}
		}	
	}
	
	if (cfg_super.self_correct_TWT == 0)
		visible(&HMI.pg411_TWTSubstitutionRuntime, FALSE);
	else
		visible(&HMI.pg411_TWTSubstitutionRuntime, TRUE);
	
	if (pg991StopButtonTest == 1)
	{
		pg991StartButtonTest = 0;
		pg991StopButtonTest  = 0;
	}
	
	if (debugFlags.numberOfOverShoot == 10000)
	{
		debugFlags.numberOfOverShoot = 0;
	}
	
	if (debugFlags.numberOfUnderShoot == 10000)
	{
		debugFlags.numberOfUnderShoot = 0;
	}
	
	// only for Autobatch per Jon
	
	if (cfg_super.current_blender_type == AUTOBATCH)
	{
		visible(&HMI.pg032_MixerGateAutobatchRuntime, TRUE);
		visible(&pg005_ShowAck, FALSE);
	}
	else
		visible(&HMI.pg032_MixerGateAutobatchRuntime, FALSE);
	
	if ((HMI.pg350_BWH_calibration_flag) && (BLN_OPER_MODE == PAUSE_MODE))
	{
		HMI.pg350_BWH_calibration_flag = 0;
		Signal.StoreConfig = TRUE;
	}
	
	
   // zcl [06-08-16] - commented the two lines of code below out. Auxiliary timer should be between
   // 0 and 99 secs, but these lines are capping the higher limit in relation to mix time.
   //	if (cfgMIXER.aux_alarm_time >= (cfgMIXER.mix_time / 1000))
   //		cfgMIXER.aux_alarm_time = (cfgMIXER.mix_time  - 1000) / 1000;
	
	
	// to test load cell calibration .... 
	
	if ((shrBWH.BWH_count_bad_calib > 2000) && (cfgBWH.check_zero_wt_flag))
	{
		shrBWH.BWH_count_bad_calib = 0;
		visible(&HMI.pg000_CalNeedDoneRuntime, TRUE);
	}
	
	if (HMI.pg000_PopupBWHCalibration_ACK)
	{
		HMI.pg000_PopupBWHCalibration_ACK = 0;
		visible(&HMI.pg000_CalNeedDoneRuntime, FALSE);
	}
	
	//end test
			
#ifdef TEST_BUTTONS
	if (u8ButtonsTestState > TEST_NOT_STARTED) 
      vPg999_InHouseTesting_Main();
#endif

   if (cfg_super.blend_control == 2)  //Tex-Trude
      HMI.pg024_remoteSLvis = 1;
   else 
      HMI.pg024_remoteSLvis = 0;

   //G2-401
   if ((cfg_super.blend_control > 0) && ((HMI.CurrentPage == pg024_Loading) || (HMI.CurrentPage == 88)))
	{
      if (cfg_super.blend_control == 1)
      {
         if (HMI.pg024_LoadAll)
         {
            for (i=0; i<cfg_super.total_gates; i++)
            {
               visible(&HMI.pg024_load[i], TRUE);  //G2-401
               cfgGATE[i].creceiver.enabled = TRUE;
            }
            HMI.pg024_LoadAll = FALSE;
         }

         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (IO_dig[0].channel[LOAD_A_IN+i] && cfgGATE[i].creceiver.enabled  && shrGATE[i].sreceiver.receiving_on)
            { 
               if (IO_dig[0].channel[LOAD_A_OUT+i])             //Do blinking
               {
                  if (pg024Flashctr < 10)
                     visible(&HMI.pg024_loadreqvis[i], TRUE);   //G2-401

                  if (pg024Flashctr > 10)
                  {
                     visible(&HMI.pg024_loadreqvis[i], FALSE);  //G2-401
                     if (pg024Flashctr > 20)
                        pg024Flashctr = 0;
                  }
                  pg024Flashctr++;
               }
               else
                   visible(&HMI.pg024_loadreqvis[i], FALSE);  //G2-401
            }
            else
               visible(&HMI.pg024_loadreqvis[i], FALSE);  //G2-401
            visible(&HMI.pg024_load[i], TRUE);            //G2-401
         }
      }
      else if (cfg_super.blend_control == 2)  //Tex-Trude
      {
         if (HMI.pg024_LoadAll)
         {
            for (i=0; i<cfg_super.total_gates; i++)
            {
               visible(&HMI.pg024_load[i], TRUE);  //G2-401
               cfgGATE[i].creceiver.enabled = TRUE;
               IO_dig[0].channel[LOAD_A_OUT+i] = TRUE;
            }
            HMI.pg024_LoadAll = FALSE;
         }

         for (i=0; i<cfg_super.total_gates; i++) //G2-401
         {
            IO_dig[0].channel[LOAD_A_OUT+i] = cfgGATE[i].creceiver.enabled;
         }

         for (i=0; i<cfg_super.total_gates; i++)
         {
               if (IO_dig[0].channel[LOAD_A_IN+i])              //Do blinking
               {
                  if (pg024Flashctr < 10)
                     visible(&HMI.pg024_loadreqvis[i], TRUE);   //G2-401

                  if (pg024Flashctr > 10)
                  {
                     visible(&HMI.pg024_loadreqvis[i], FALSE);  //G2-401
                     if (pg024Flashctr > 20)
                        pg024Flashctr = 0;
                  }
                  pg024Flashctr++;
               }
               else
                  visible(&HMI.pg024_loadreqvis[i], FALSE);  //G2-401

            visible(&HMI.pg024_load[i], TRUE);            //G2-401
         }
      }
   }
   else // reset all off page not active
   {
      HMI.pg024_firstpass = FALSE;   //reset
      for (i=0; i<12; i++)           //clear all this is necessary if # gates is changed
      {
         visible(&HMI.pg024_loadreqvis[i], FALSE); //G2-401
         visible(&HMI.pg024_load[i], FALSE);       //G2-401
      }
   }
	
   if (HMI.CurrentPage == pg405_Diag_Gate)
	{
      for (i=0; i < MAX_GATES; i++)
   	{
   		 shrGATE[i].set_wt2  = shrGATE[i].set_wt; 
          shrGATE[i].rem_wt2  = shrGATE[i].rem_wt;
          shrGATE[i].set_tol2 = shrGATE[i].set_tol;
      }
   }

	if (HMI.CurrentPage == pg005_BlendManualBackup1)
	{
		pg005_Manual_BackupPage();
	}
	
	if (Signal.SystemLoaded)
	{
		if (Status_RfbExtSetClientCaption != 0) // also triggerd by pg183     
		{
			Status_RfbExtSetClientCaption = RfbExtSetClientCaption((void *)&pLib, (UDINT)cfg_super.device_name);
		}

		if (Status_RfbExtInit == 0)
		{
			Status_RfbExtConnect = RfbExtConnect((UDINT)&pLib, (UDINT)"visual");
			if (!Status_RfbExtConnect)
			{
				Status_RfbExtInit = 99;
				Status_RfbExtSetClientCaption = 99;
			}
		}

		if (gFlag_AckAlarms == TRUE)
			AcknowledgeAlarms();

		if (gFlag_ClearAlarmHistory == TRUE)
			ClearAlarmHistory();

		if (gFlag_SetHMIConfig == FALSE)
		{
			gFlag_SetHMIConfig = TRUE;
			SetSystemVisibility();
		}

		/* System loaded is now loaded, if init page shown then hide it forever */
		if (gFlag_ShowInitPage == TRUE && HMI.CurrentPage != pg990_SetupWizzard) // SS
		{
			gFlag_ShowInitPage = FALSE;
			visible(&HMI.pg000_PopupClearAlarms, FALSE);
			visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
			visible(&HMI.pg000_PopupFinishBatchRuntime, FALSE);
			visible(&HMI.pg000_RestartRequiredRuntime, FALSE);
			/* go to main page */
			HMI.CurrentPage = pg000_MainPage;
			HMI.ChangePage = pg000_MainPage;
		}

		SetDisplayUnits();

      /* The global version of current page is used by other task to determine which page the
         current visualization is using */
		shr_global.CurrentPage = HMI.CurrentPage;
		/* Process the individual pages of the visualization */
		if (HMI.CurrentPage == pg000_MainPage)
		{   
#ifdef CRYPT_ENABLED
			cCryptOK = TRUE; /* init this to true before checking; inocent until proven guilty*/
			if (CryptCheck == TRUE) 
			{
#ifdef CHECK_LICENSE_DURING_OPERATION
				if (brsstrlen((UDINT)sCalculatedKey) < 1)
					vCalculateLicenseKey(); /* if first time here then calculate */		
				
				for (i=0; i<17; i++)
				   sPannelLicenceKey[i] = "";	/* no need for the first 17 char from the license key keep pnly last 13 char	*/
				
				if (brsstrcmp((UDINT)sCalculatedKey, (UDINT)sPannelLicenceKey) == 0)
               ; /*cCryptOK is TRUE already */
				else 
				{
					cCryptOK = FALSE; /* got damaged */
				}
#endif
			}
			else 
            cCryptOK = FALSE;
		
			if (cCryptOK == TRUE)
			{
				/* call main page function - the call is done the code*/
				pg000_Func_MainPanel();
			}
			else
			{
				if ((brsstrlen((DINT)sSerialNumber) > 1) && (brsstrlen((UDINT)sMacAddr) > 1))
				{
					/* Serial and MacAddress are read now */
					u8MacAddressTimeOut = 0; /* reset timeout counter for reading MAC Address*/ 
					vCalculateLicenseKey(); /* will fill sCalculatedKey */
					sCalculatedKey[30] = '\0';
					
					for (i=0; i<17; i++)
						sPannelLicenceKey[i] = "";	/* no need for the first 17 char from the license key keep pnly last 13 char	*/
					
					/* Check the calculated key against the one stored on the flash */
					if (brsstrcmp((UDINT)sCalculatedKey, (UDINT)sPannelLicenceKey) == 0)
					{
						/* calculated and stored are the same
						** so go to main page if it is a match*/
						CryptCheck = TRUE;
						pg000_Func_MainPanel();
					}
					else
					{
						/* stored is blank or wrong so go to License page to input a new one*/
#ifdef CHECK_LICENSE_DURING_OPERATION
						CryptCheck = FALSE; /* just in case we got here because the stored key was damaged */
#endif
						HMI.ValidateKey = FALSE;
						HMI.CurrentPage = pg007_LicenseKey;
						HMI.ChangePage  = pg007_LicenseKey;
					}
				}
				else
				{
					/* MacAddress was not ready so keep reading a few times before going to error state */
					u8MacAddressTimeOut++;
					if (u8MacAddressTimeOut < 40)
						vPg007_Func_InitPage();
					else
					{
						u8MacAddressTimeOut = 0;
						u8PanelErrorCode = 0x07;
						pg000_Func_MainPanel();
						/* HMI.ChangePage = pg008_PanelGotOld; ignore for now */
					}
				}
			}
#else /* CRYPT_ENABLED is FALSE so just call main page function - the call is down the code*/
			pg000_Func_MainPanel();	
#endif /*  end CRYPT_ENABLED */
		}
		else if (((HMI.CurrentPage >= pg451_Recipe_Check) && (HMI.CurrentPage <= pg457_Resin_Editor)) || (HMI.CurrentPage == pg454_Recipe_Entry))
		{
			Recipe_Main();
		}
		else if (HMI.CurrentPage == pg404_Diag_System)
		{
			if ((brsstrlen((UDINT)sSerialNumber) > 1) && (brsstrlen((UDINT)sMacAddr) > 1)) 
				u8MacAddressTimeOut = 0;/* ever read? */
			else
			{
				/* if empty read again */
				u8MacAddressTimeOut++;
				if (u8MacAddressTimeOut<40)
					vPg007_Func_InitPage();
				else
				{
					u8MacAddressTimeOut = 0;
					u8PanelErrorCode = 0x07;
					/* HMI.ChangePage = pg008_PanelGotOld; ignore for now */
				}
			}
		}
		else if (HMI.CurrentPage == pg408_Diag_Regrind)
		{
			Regrind_Main();
		}
		else if ((HMI.CurrentPage >= pg305_AlarmSelect) && (HMI.CurrentPage <= pg307_AlarmHistory))
			Alarm_Main();
		else if ((HMI.CurrentPage >= pg320_Totals_System) && (HMI.CurrentPage <= pg329_Totals_ExtNip64))
			Totals_Main();
		else if (((HMI.CurrentPage >= pg330_Monitor_Sys) && (HMI.CurrentPage <= pg339_Monitor_Line)) || (HMI.CurrentPage == pg075_select_config_set))
			Monitor_Main();
		else if (HMI.CurrentPage == pg310_Security)
			Security_Main();
      else if (HMI.CurrentPage == pg025_histogram)
          MonitorHistogram();
		else if ((HMI.CurrentPage == pg002_Setup_Main) ||  (HMI.CurrentPage == pg006_Setup_Gate_CLoop4)
			|| ((HMI.CurrentPage >= pg010_Setup_GateSel) && (HMI.CurrentPage <= pg233_Setup_System_Sim4)) || (HMI.CurrentPage == pg990_SetupWizzard))
			Setup_Main();
		else if (HMI.CurrentPage == pg300_Print)
			Print_Main();
		else if ((HMI.CurrentPage >= pg350_Calibrate) && (HMI.CurrentPage <= pg352_Calibrate_Weigh))
		{
			shr_global.calibrating = TRUE;
			Calibrate_Main();
			shr_global.calibrating = FALSE;
		}
		else if ((HMI.CurrentPage >= pg399_Diag) && (HMI.CurrentPage <= pg446_Env_Temp))
			Diagnostics_Main();
		else if (HMI.CurrentPage == pg009_ExtManual)
			set_manual();
     
		/* if the current page has changed restart the security timer GTF... */
      // G2-337 4/19/17
		if (HMI.CurrentPage != PageNow)
      {				                              //Dragan
			cfg_super.EnteredPassword = start_time();
			/*... and run page_completion */			//Dragan
			page_completion(PageNow);					//Dragan
		}												      //Dragan
		PageNow = HMI.CurrentPage;
		
		if (Signal.ProcessAuto)
		{
			HMI.Action_BlendMode = 3;
			process_mode_buttons();
			Signal.ProcessAuto = FALSE;
		}
		
		if (Signal.ProcessExtManChange)
		{			
			HMI.Action_ExtruderMode = 2;
			process_mode_buttons();
			
			HMI.Action_Input2 = gcActive;
			HMI.Action_Input3 = gcActive;
			set_manual();
			
			Signal.ProcessExtManChange = 0;
		}
		
		if (Signal.ProcessShutdown)
		{
			HMI.Action_BlendMode = 1;
			//HMI.Action_ExtruderMode = 1;
			HMI.Action_Input7 = 1;
			process_mode_buttons();
			Signal.ProcessShutdown = FALSE;
		}

		if (Signal.StopExtr)
		{
			/* go into paused mode */
			HMI.Action_ExtruderMode = 1;
			stop_ext();
			process_mode_buttons();
			Signal.StopExtr = FALSE;
		}
		if (Signal.InitManExtr)
		{
			HMI.Action_ExtruderMode = 2;
			process_mode_buttons();
			Signal.InitManExtr = 0;
		}
		if (Signal.RunExtr)
		{
			HMI.Action_ExtruderMode = 3;
			run_ext_remote();
			process_mode_buttons();
			Signal.RunExtr = FALSE;
		}
	}
	
	if (cfg_super.profibus_enabled == 1)
	{
		visible(&HMI.SetupRuntime10, TRUE);
	}
	else
	{
		visible(&HMI.SetupRuntime10, FALSE);
	}

   // for testing - Do not display in released code
   #ifndef RELEASE_CODE
		visible((unsigned char *)&HMI.pg212_OPCvis, TRUE);
	#else
		visible((unsigned char *)&HMI.pg212_OPCvis, FALSE);
	#endif
   
   //drt 03-09-16 flood feed
   if (HMI.CurrentPage == pg034_Setup_Mixer_CLoop2) //G2-596
   {
      if (pg034First == 0)
      {
         HMI.pg034_Flood_Feed_Timer = cfg_super.Flood_Feed_Timer;
         HMI.Flood_Feed_Timer_Flag  = cfg_super.Flood_Feed_Timer_Flag;
         pg034First = 1;
      }

      if (HMI.Flood_Feed_Timer_Flag) 
      {
         if (BLN_OPER_MODE != AUTO_MODE)
         {
            visible(&HMI.Flood_Feed_Timer_visible, TRUE);
         }
         else
         {
            visible(&HMI.Flood_Feed_Timer_visible, FALSE); 
         }
      }
      else
      {
         visible(&HMI.Flood_Feed_Timer_visible, FALSE);
         HMI.pg034_Flood_Feed_Timer = cfg_super.Flood_Feed_Timer = 15.0; //change back to default if not used.
      }	

      if (HMI.pg034_FloodFeedSet)
      {
         HMI.pg034_FloodFeedSet = 0;
         cfg_super.Flood_Feed_Timer = HMI.pg034_Flood_Feed_Timer; 
      }
      if (HMI.pg034_FloodFeedModeSet)
      {
         HMI.pg034_FloodFeedModeSet = 0;
         cfg_super.Flood_Feed_Timer_Flag = HMI.Flood_Feed_Timer_Flag;
         Signal.EnteredSetup = 1;
      }
   }
   else
      pg034First = 0; //reset for next time

   if (HMI.OPCenable) 
   {
      HMI.OPCenableVis = 0;
   }
   else
   {
      HMI.OPCenableVis   = 1;
      HMI.OPCenableRead  = 0;
      HMI.OPCenableWrite = 0;
   }	

	// for regrind per Jon 

	for (i=0; i<MAX_GATES; i++)
	{
		if ((cfgGATE[i].hopper_type == REGRIND) /*&& (BATCH_RECIPE_NOW->parts_pct.s[i] != 0.0)*/ 
			&& (cfgGATE[i].proximityswitch_exist == ON) && (cfgGATE[i].allowdump_proxoff == OFF)
			&& (IO_dig[0].channel[cfgGATE[i].proximity_switch.chan] == OFF) && (regring_back_in_batch == TRUE)) 
		{
			regring_back_in_batch     = FALSE;	// DF
			NEW_Recipe_after_batch    = TRUE;
			HMI.Action_BlendMode      = 3;
			HMI.pg451_HopperRateSpeed = 3;
			process_mode_buttons();
		}
		else if ((cfgGATE[i].hopper_type == REGRIND) /*&& (BATCH_RECIPE_NOW->parts_pct.s[i] != 0.0) */
			&& (cfgGATE[i].proximityswitch_exist == ON) && (cfgGATE[i].allowdump_proxoff == OFF)
			&& (IO_dig[0].channel[cfgGATE[i].proximity_switch.chan] == ON) && (regring_back_in_batch == FALSE))	
			   regring_back_in_batch = TRUE;	// DF
	}
	
	brsitoa((DINT)cs->ip_address[0],     (UDINT)TmpStr);
	brsstrcpy((UDINT)gIPAddressMainPage, (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)".");
	brsitoa((DINT)cs->ip_address[1],     (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)".");
	brsitoa((DINT)cs->ip_address[2],     (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)".");
	brsitoa((DINT)cs->ip_address[3],     (UDINT)TmpStr);
	brsstrcat((UDINT)gIPAddressMainPage, (UDINT)TmpStr);

   if (cfg_super.blend_control == 0)  //G2-590
      visible(&HMI.pg012_loadingON, FALSE); 
   else
      visible(&HMI.pg012_loadingON, TRUE); 
}
/************************************************************************/
/*
   Function:  pg004_Func_InitPage()

   Description:  This function process the actions and visualization of
         the initial startup page. If the module id does not match the
         desired moduled id then the system will not boot.
                                                                        */
/************************************************************************/
int pg004_Func_InitPage()
{
	if ((PLC.TouchPanel.ModuleID == ModID_4PP045PCC) || (PLC.TouchPanel.ModuleID == ModID_4PP045) || (PLC.TouchPanel.ModuleID == ModID_4PP065) || (PLC.TouchPanel.ModuleID == ModID_4PPC70))
	{
      HMI.pg004_InitTextIndex = 0;
		return(SUCCESS);
	}
	else
	{
		HMI.InvalidTouchColor   = gRed;
		HMI.pg004_InitTextIndex = 1;
		return(ERROR);
	}
}

/************************************************************************/
/*
   Function:  vPg007_Func_InitPage()

   Description:  This function inits the License page and sets the global variables
   for serial number and MacAddress. 
                                                                        */
/************************************************************************/
void vPg007_Func_InitPage()
{
	int  temp, i;
	char sTempBuff[12];

	/* init Mac */
#ifdef PP65
	brsstrcpy((UDINT)DevName, (UDINT)"IF5");
#endif
#ifdef C70
	brsstrcpy((UDINT)DevName, (UDINT)"IF2");
#endif
#ifdef C70_003
	brsstrcpy((UDINT)DevName, (UDINT)"IF2");
#endif

	Mac.enable   = 1;
	Mac.pDevice  = (UDINT)&DevName;
	Mac.pMacAddr = (UDINT)&MacAdr;
	Mac.Len      = 6;
   
	/*generate the MacAddress*/
	CfgGetMacAddr(&Mac);

	if (Mac.status == 0)/* no error reading the MacAddress */
	{
		/* generate the serial number string */
		brsitoa((DINT)PLC.TouchPanel.SerialNum, (UDINT)&sTempBuff);
		brsstrcpy((UDINT)sSerialNumber, (UDINT)"E1850");
		brsstrcat((UDINT)sSerialNumber, (UDINT)sTempBuff);
	   
		/*store MAC-Address for display needs*/
		brsstrcpy((UDINT)sMacAddr, (UDINT)"00:00:00:00:00:00");
		for (i=0; i<6; i++)
		{
			temp = (MacAdr[i]&0xF0)>>4;
			if (temp > 9) 
            temp += 7; /* add an offset to reach the hex chars A, B, ... F */
			sMacAddr[3*i] += temp;
			temp = (MacAdr[i]&0x0F);
			if (temp > 9) 
            temp += 7; /* add an offset to reach the hex chars A, B, ... F */
			sMacAddr[3*i+1] += temp;
		}
	}
}

/************************************************************************/
/*
   Function:  pg005_Manual_BackupPage()

   Description:  This function process the actions and visualization of
         the manual backup page. Need to process gate buttons based on
         gate-network setting pg005_manual_GateControl will be in the
         range 1-12 if a gate button is pressed and set to 13 when released.
         13 is used to close gate.
                                                                        */
/************************************************************************/
void pg005_Manual_BackupPage()
{
	int i;

	BLN_OPER_MODE = MAN_MODE;

	if (!Interlock_Open_Ack && !shr_global.interlock_closed)/*interlock message has not been displayed */
		visible(&pg005_ShowAck, TRUE);  /* show ack layer */
	if ((Interlock_Open_Ack == 2) && !shr_global.interlock_closed)/*interlock message has been acknowledged */
		visible(&pg005_ShowAck, FALSE);  /* hide ack layer */
	if (!Interlock_Open_Ack && shr_global.interlock_closed)/*interlock message has not been acknowledged but interlock has closed */
		visible(&pg005_ShowAck, FALSE);  /* hide ack layer */
	if ((Interlock_Open_Ack == 2) && shr_global.interlock_closed)/*interlock message has been acknowledged and interlock has closed */
		Interlock_Open_Ack = 1;
#if 0
	//if ((Interlock_Open_Ack == 1) && !shr_global.interlock_closed)/*interlock message was acknowledged and interlock has re-opened */
	//   visible(&pg005_ShowAck, TRUE);  /* show ack layer */
#endif

	shrMIXER.mixer_mode = PAUSE_MODE; 
	
		if ((hmi_pg005_controls[ACTUATE]) && (cfg_super.self_cleaning_flag == FALSE))
		{
			for (i=0; i<cfg_super.total_gates; i++)
			{
				if (hmi_pg005_controls[i])
				{
					IO_dig[X20].channel[cfgGATE[i].primary_gate.chan] = ON;
					if (cfgGATE[i].secondary_gate_enabled == TRUE) /* if secondary enabled, open it GTF */
						IO_dig[X20].channel[cfgGATE[i].secondary_gate.chan] = ON;
				}
				else
					IO_dig[X20].channel[cfgGATE[i].primary_gate.chan] = OFF;
			}
			if (hmi_pg005_controls[12])
				IO_dig[0].channel[MIXER_MOTOR_OUT] = (shr_global.interlock_closed ? ON : OFF); //G2-348
			else
				IO_dig[0].channel[MIXER_MOTOR_OUT] = OFF;
			if (hmi_pg005_controls[13])
				IO_dig[0].channel[MIXER_GATE_OUT]  = (shr_global.interlock_closed ? ON : OFF); //G2-348
			else
            if (cfgMIXER.mixer_motor_mode != NO_MIXER) //G2-333
				   IO_dig[0].channel[MIXER_GATE_OUT] = OFF;
			if (hmi_pg005_controls[14])
				IO_dig[0].channel[WEIGH_HOPPER_DUMP_OUT] = ON;
			else
				IO_dig[0].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
			if (hmi_pg005_controls[15])
			{
				IO_dig[0].channel[ALARM_OUT] = ON;
				if (GravitrolHopperAlone)
					IO_dig[0].channel[ALARM_OUT_GRAV] = ON;
			}
			else
				IO_dig[0].channel[ALARM_OUT] = OFF;
		
			/* make next/prev page buttons invisible GTF */
			hmi_pg005_controls[17] = ON;
		}
		else if ((!hmi_pg005_controls[ACTUATE]) && (cfg_super.self_cleaning_flag == FALSE))
		{   /* Actuate not pressed turn everything off GTF */
			close_all_gates_GTF();							// G2-337 4/19/17 Dragan
			IO_dig[0].channel[MIXER_MOTOR_OUT] = OFF;
         if (cfgMIXER.mixer_motor_mode != NO_MIXER) //G2-333
			   IO_dig[0].channel[MIXER_GATE_OUT] = OFF;
			IO_dig[0].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
			IO_dig[0].channel[ALARM_OUT] = OFF;
			IO_dig[0].channel[ALARM_OUT_GRAV] = OFF;
			/* make next/prev page buttons invisible GTF */
			hmi_pg005_controls[17] = ON;
		}

		if (cfg_super.self_cleaning_flag == TRUE)
		{
		   // per Holger in Germany		
			if (!IO_dig[X20].channel[SAFETY_SWITCH_IN])
			{	
				shrBWH.oper_mode = PAUSE_MODE;
				shrMIXER.oper_mode = PAUSE_MODE;
			 
				IO_dig[X20].channel[cfg_super.purge_valve_3.chan] = OFF;
			
				if (cfg_super.remote_purge)
				{
					IO_dig[X20].channel[cfg_super.remote_purge_start_relay.chan]    = OFF;
					IO_dig[X20].channel[cfg_super.remote_purge_complete_relay.chan] = OFF;
				}
				else
				{
					IO_dig[X20].channel[cfg_super.purge_valve_1.chan] = OFF;
					IO_dig[X20].channel[cfg_super.purge_valve_2.chan] = OFF;
				}

			   shr_global.purge_pending = FALSE;
		      cfg_super.self_cleaning_flag = FALSE;
			}
         else 
            shr_global.purge_pending = TRUE;
		}
	
	return;
}
/************************************************************************/
/*
   Function: Regrind_Main()

   Description:  This function process the actions and visualization of
         the 3 Prox Regrind Hopper. 
 */
/************************************************************************/
void Regrind_Main()
{
	int   i;
	USINT ucGateNum;
	
	ucGateNum = 99;
	for (i=0; i<cfg_super.total_gates; i++)
	{
		if ((cfgGATE[i].ucRegrind3ProxEnabled == TRUE) && (cfgGATE[i].hopper_type == REGRIND))
			ucGateNum = i;
	}
	if (ucGateNum == 99)
		return;
	
	copy_recipe(&BATCH_RECIPE_EDIT, BATCH_RECIPE_NOW, BLENDER_RECIPE);

	if (HMI.pg408_Simulation == 0) // Let's Control the real BLender Values
	{
		visible(&HMI.pg450_RecipeCurrentRuntime[11], FALSE); // controls 3 buttons for simulation
		HMI.pg408_ucRegrindLowCovered    =  !(IO_dig[0].channel[RESV_IN_1]) ; // LowCovered
		HMI.pg408_ucRegrindMiddleCovered =  !(IO_dig[0].channel[RESV_IN_2]) ; // MiddleCovered
		HMI.pg408_ucRegrindHighCovered   =  !(IO_dig[0].channel[RESV_IN_3]) ; // HighCovered		
	}
	else
	{		
		visible(&HMI.pg450_RecipeCurrentRuntime[11], TRUE);

		if (HMI.Action_Input3 == gcActive) // one of the change buttons pushed
		{
			shrGATE[ucGateNum].ucRegrindHighCovered   =  HMI.pg408_ucRegrindHighCovered;
			shrGATE[ucGateNum].ucRegrindMiddleCovered =  HMI.pg408_ucRegrindMiddleCovered;
			shrGATE[ucGateNum].ucRegrindLowCovered    =  HMI.pg408_ucRegrindLowCovered;
			HMI.Action_Input3 = gcInActive;				
		}
	}

	HMI.pg408_GraphIndex = 20;
	if (HMI.pg408_ucRegrindHighCovered == ON)
	{
		HMI.pg408_GraphIndex += 60;		
		HMI.pg330_BatchStsColor = gGreen;
	}
	else
	{
		HMI.pg330_BatchStsColor = gBlack;
	}
		
	if (HMI.pg408_ucRegrindMiddleCovered == ON)
	{
		HMI.pg408_GraphIndex += 60;
		HMI.pg330_MixerStsColor = gGreen;
	}
	else
	{
		HMI.pg330_MixerStsColor = gBlack;
	}

	if (HMI.pg408_ucRegrindLowCovered == ON)
	{
		HMI.pg408_GraphIndex += 40;
		HMI.pg330_MixerGateStsColor = gGreen;
	}
	else
	{
		HMI.pg330_MixerGateStsColor = gBlack;
	}
}

/************************************************************************/
/*
   Function:  pg000_Func_MainPanel()

   Description:  This function process the actions and visualization of
         the main page.
                                                                        */
/************************************************************************/
void pg000_Func_MainPanel()
{
	config_super *cs = &cfg_super;
	//int  Result;
	UINT status;
   
	if (!Signal.SystemLoaded)
		return;	
      
	if ((Blender_Type == GUARDIAN) && (!cfg_super.wtp_mode))
		brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION);
	else if ((Blender_Type == GUARDIAN) && (cfg_super.wtp_mode))
		brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)WE_VERSION);
	else 
		brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)FW_VERSION_AB);
	  
	SetSystemVisibility();

   if (debugFlags.drt) 
	{
      bgDbgInfo(&g_RingBuffer1, "interface.c", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
      "%d \r\n", HMI.Request_SYSReset);
   }

	if (BLN_OPER_MODE == MAN_MODE)
	{
		BLN_OPER_MODE = PAUSE_MODE;
	}

	/* This is needed because if current device is invalid it causes a page fault in the OS GTF */
	if (HMI.CurrentDevice == gNeutralDevice)
      HMI.CurrentDevice = 0;

	if (cs->current_security_level < SUPERVISOR_LOCK)
	{
		HMI.pg000_Debug_Layer_On = 1; /* Turn off debug layer, not in Service Mode GTF */
		visible(&HMI.pg000_Debug_Layer_On, FALSE); /* Turn off debug layer, not in Service Mode GTF */
		visible(&HMI.BlendMainRestartRuntime, FALSE);
	}
	else
  	 	visible(&HMI.BlendMainRestartRuntime, TRUE);
	
	visible(&HMI.PrintRuntime, TRUE);
 
	if (HMI.Request_SYSReset == 1) /* the setup has changed and a system reset is required */
	{
		if (HMI.pg000_RestartRequiredRuntime == 1)   /* the layer for requesting restart is invisible */
		{
			status = RTC_gettime(&gRTCStartTime);        /* get current time and store it into time struct */
			visible(&HMI.pg000_RestartRequiredRuntime, TRUE);
		}
		else
		{
			/* timeout before restart the system */
			status = RTC_gettime(&gRTCNextTime);        /* get current time and store it into time struct */
			if (gRTCNextTime.second >= gRTCStartTime.second)  /* calculate elapsed seconds the popup has shown */
				HMI.PopupElapTime = gRTCNextTime.second - gRTCStartTime.second;
			else
				HMI.PopupElapTime = (gRTCNextTime.second+60) - gRTCStartTime.second;

			if (HMI.PopupElapTime >= 2)
			{  /* proc_mon will restart the system when/if the configuration is stored */
				Signal.RestartSystem = TRUE;
				HMI.Request_SYSReset = 0;
			}
		}
	}

	/* pressed totals */
	if (HMI.Action_Input1 == gcActive)
	{
		visible(&HMI.PopupRuntime1, FALSE);
		visible(&HMI.PopupRuntime2, FALSE);
		visible(&HMI.PopupRuntime3, FALSE);
		visible(&HMI.PopupRuntime4, FALSE);
		visible(&HMI.PopupRuntime5, FALSE);
		
		HMI.ChangePage = pg320_Totals_System;
		HMI.Action_Input1 = gcInActive;
	}
	
	/* pressed monitor */
	/* pressed monitor */
	if (HMI.Action_Input2 == gcActive)
	{
		if (cs->ucIconInterface)
			MonitorVisibilityIcons();
		else
			MonitorVisibility();
		if (GravitrolHopperAlone)
		   HMI.ChangePage = pg338_Monitor_Ext;
		else 
		   HMI.ChangePage = pg330_Monitor_Sys;
		HMI.Action_Input2 = gcInActive;
	}

	/* clear alarms before entering extruder manual */
	if (HMI.Action_Input0 == 1)
	{
		visible(&HMI.pg000_PopupClearAlarms, FALSE);
		HMI.Action_Input0 = gcInActive;
	}

	/* cannot go from pause to auto directly */
	if (HMI.Action_Input0 == 2)
	{
		visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
		HMI.Action_Input0 = gcInActive;
	}

	if (HMI.pg453_Recipe_Action == RECIPE_INITIALIZE) //recipe
	{         
		HMI.ChangePage = pg453_Recipe_Input;
	}

	if (BlenderRecipeChanged) 
	{
		HMI.pg000_PopupGateSwitchRuntime = 1;
		visible(&HMI.pg000_PopupRecipeUseRuntime, TRUE);
		if (HMI.pg000_PopupRecipeUsed_ACK)
		{
			BlenderRecipeChanged = 0;
			HMI.pg000_PopupRecipeUsed_ACK = 0;
			visible(&HMI.pg000_PopupRecipeUseRuntime, FALSE);
		}
	}
	
	// restart 
	if (HMI.Request_SYSReset == 1) /* the setup has changed and a system reset is required */
	{
		copy_recipe(BATCH_RECIPE_NEW, &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
		if (HMI.pg000_RestartRequiredRuntime == 1)   /* the layer for requesting restart is invisible */
		{
			status = RTC_gettime(&gRTCStartTime);        /* get current time and store it into time struct */
			visible(&HMI.pg000_RestartRequiredRuntime, TRUE);
		}
		else
		{
			/* timeout before restart the system */
			status = RTC_gettime(&gRTCNextTime);        /* get current time and store it into time struct */
			if (gRTCNextTime.second >= gRTCStartTime.second)  /* calculate elapsed seconds the popup has shown */
				HMI.PopupElapTime = gRTCNextTime.second - gRTCStartTime.second;
			else
				HMI.PopupElapTime = (gRTCNextTime.second+60) - gRTCStartTime.second;
	
			if (HMI.PopupElapTime >= 2)
			{  /* proc_mon will restart the system when/if the configuration is stored */
				Signal.RestartSystem = TRUE;
				HMI.Request_SYSReset = 0;
			}
		}
	}
	process_mode_buttons();
}

void vPg990_SetupWizzardMain()
{
#define WIZ_SETUP_INIT	 			   0
#define WIZ_SETUP_START 			   1
#define WIZ_SETUP_BLENDER_TYPE      2
#define WIZ_SETUP_BLENDER_SIZE 		3
#define WIZ_SETUP_SUMMARY			   4
#define WIZ_SETUP_STOP				   5
#define WIZ_SETUP_BLENDER_OPERATION	6 /* skiped for now */
	
	static USINT u8SetupState = 0;
	USINT i;
	
	/* these HMI variables show or hide the layers*/
	HMI.pg990_u8WizBlenderType           = !(u8SetupState == WIZ_SETUP_BLENDER_TYPE);
	HMI.pg990_u8WizBlenderSize_Guardian  = !((u8SetupState == WIZ_SETUP_BLENDER_SIZE) && (cfg_super.current_blender_type == GUARDIAN));
	HMI.pg990_u8WizBlenderSize_Autobatch = !((u8SetupState == WIZ_SETUP_BLENDER_SIZE) && (cfg_super.current_blender_type == AUTOBATCH));
	HMI.pg990_u8WizSummary               = !((u8SetupState == WIZ_SETUP_SUMMARY) && (!HMI.pg990_GravitrolHopperAlone));
	HMI.pg990_u8WizSummary1              = !((u8SetupState == WIZ_SETUP_SUMMARY) && (HMI.pg990_GravitrolHopperAlone));
	HMI.pg990_u8WizBlenderOperation      = !(u8SetupState == WIZ_SETUP_BLENDER_OPERATION);
	
	if (HMI.pg990_GravitrolHopperAlone) 
      GravitrolHopperAlone = TRUE;
	else 
      GravitrolHopperAlone = FALSE;
			
	if (HMI.SetupWizzardProceed == TRUE)
	{
		HMI.SetupWizzardProceed = FALSE;
		if (u8SetupState < WIZ_SETUP_STOP) 
         u8SetupState++;
	} 
	
	if (HMI.SetupWizzardBack == TRUE)
	{
		HMI.SetupWizzardBack = FALSE;
		if (u8SetupState > WIZ_SETUP_BLENDER_TYPE) 
         u8SetupState--;
	} 
	
	switch (u8SetupState)
	{
		case WIZ_SETUP_INIT:
			u8SetupState = WIZ_SETUP_START;
			HMI.pg990_u8WizBlndOpBtnPressed = 2; /* init value */
		case WIZ_SETUP_START:
			brsstrcpy((UDINT)sWizSetupFrameText, (UDINT)"Setup Wizard start page");			
			brsstrcpy((UDINT)sWizSetupInfoText,  (UDINT)"\nBefore using the blender, some critical parameters must be set!\n\nThis setup wizard will help you set these parameters.\n");
			break;
		case WIZ_SETUP_BLENDER_TYPE:
			brsstrcpy((UDINT)sWizSetupFrameText, (UDINT)"Blender Type");			
			brsstrcpy((UDINT)sWizSetupInfoText,  (UDINT)"\nSetup the blender type.");
         if (cfg_super.current_blender_type == AUTOBATCH)
            cfg_super.pg2180_aux5_invert = 0;
         else
            cfg_super.pg2180_aux5_invert = 1;
			break;
		case WIZ_SETUP_BLENDER_SIZE:
			brsstrcpy((UDINT)sWizSetupFrameText, (UDINT)"Blender Size");			
			brsstrcpy((UDINT)sWizSetupInfoText,  (UDINT)"\nSetup the blender size.");
			if (cfg_super.current_blender_type == AUTOBATCH)
				cfg_super.pg175_System_Size = 11;
			break;
		case WIZ_SETUP_BLENDER_OPERATION:
			brsstrcpy((UDINT)sWizSetupFrameText, (UDINT)"Blender Operation");			
			brsstrcpy((UDINT)sWizSetupInfoText,  (UDINT)"\nFast for higher blender rate.\nAccurate for higher accuracy.\nAll purpose for a mix between accuracy and speed.");
			if (HMI.pg990_u8WizBlndOpBtnPressed > 0)
			{
				for (i=0; i<3; i++)
					HMI.pg990_au8WizBlndOpBtnBitmap[i] = 0;
				HMI.pg990_au8WizBlndOpBtnBitmap[HMI.pg990_u8WizBlndOpBtnPressed-1] = 1;
				switch (HMI.pg990_u8WizBlndOpBtnPressed)
				{
					case 1:
						brsstrcpy((UDINT)HMI.pg990_sWizOpMode, (UDINT)"Fast");
						break;
					case 2:
						brsstrcpy((UDINT)HMI.pg990_sWizOpMode, (UDINT)"All purpose");
						break;
					case 3:
						brsstrcpy((UDINT)HMI.pg990_sWizOpMode, (UDINT)"Accurate");
						break;
				}
				HMI.pg990_u8WizBlndOpBtnPressed = 0;
			} /* end if (HMI.SetupWizzardProceed == TRUE) */
			break;			
		case WIZ_SETUP_SUMMARY:
			brsstrcpy((UDINT)sWizSetupFrameText, (UDINT)"Summary");			
			brsstrcpy((UDINT)sWizSetupInfoText,  (UDINT)"\nPress 'Back' to change the settings or 'Next' to continue");
			break;
		case WIZ_SETUP_STOP:
			gSetupWizzardHadRun = TRUE; 
			G_bWizCopyValuesToGlobal = TRUE;
			G_u8WizBlenderSize = cfg_super.pg175_System_Size;
			HMI.CurrentPage = pg000_MainPage;
			HMI.ChangePage  = pg000_MainPage;
			u8SetupState = WIZ_SETUP_INIT;
			break;
	} /* end switch (u8SetupState) */			
}
	
#ifdef CRYPT_ENABLED
/************************************************************************/
/*
   Function:  vPg007_Func_LicenseKey_Main()

   Description:  This function processes the LicenseKey page
                                                                        */
/************************************************************************/
void vPg007_Func_LicenseKey_Main()
{
	char sInputKey[31];
   int  j;
	
	if (HMI.ValidateKey == TRUE) /* button ValidateKey was pushed*/
	{	
		brsstrcpy((UDINT)sInputKey, (UDINT)"");
		brsstrcpy((UDINT)sInputKey, (UDINT)sLicensePart1);
		brsstrcat((UDINT)sInputKey, (UDINT)sLicensePart2);
		brsstrcat((UDINT)sInputKey, (UDINT)sLicensePart3);
		brsstrcpy((UDINT)sInputKey, (UDINT)"");
		brsstrcpy((UDINT)sInputKey, (UDINT)"xxxxxxxxxxxxxxxxx");
		brsstrcat((UDINT)sInputKey, (UDINT)sLicensePart4);
		HMI.ValidateKey = FALSE; /* prepare for the next push of the Validate Key button */
		/* Calculate License Key */
		vCalculateLicenseKey(); /* will fill sCalculatedKey */
		sInputKey[30] = '\0';
		sCalculatedKey[30] = '\0';
		
		for (j=0; j<17; j++)
			sInputKey[j] = "";	
		
		if (brsstrcmp((UDINT)sInputKey, (UDINT)sCalculatedKey) == 0) /* check against a new calculation based on serial# and MacAddress */
		{
			brsstrcpy((UDINT)sPannelLicenceKey, (UDINT)sInputKey); /* store the correct license key in config super */					
			CryptCheck = TRUE;/* set the flag that the cript key is ok */
			HMI.CurrentPage = pg000_MainPage;
			HMI.ChangePage = pg000_MainPage;
		}
		else
		{
			/* nothing to do */
			CryptCheck = FALSE;/* set the flag that the cript key is not ok*/
		}
	}
}

void vCalculateLicenseKey()
{
	char  sBuff[15], sTemp1[15], sTemp2[30], sCryptSalt[2];
	int   iMac[6];
	int   j;
	UDINT u32Key2, u32Serial, u32MacSum, u32SerialKeyResult;
	int   i;
	
	/* Pannel type */
	brsstrcpy((UDINT)sCalculatedKey, (UDINT)"b\0");
	
	/* Key2 */
	sTemp1[0] = sSerialNumber[1];
	sTemp1[1] = sSerialNumber[2];
	sTemp1[2] = sSerialNumber[3];
	sTemp1[3] = sSerialNumber[0];
	sTemp1[4] = sSerialNumber[1];
	sTemp1[5] = sSerialNumber[2];
	sTemp1[6] = sSerialNumber[3];
	
	/* convert to decimal */
	u32Key2 = 0;
	for (i=0; i<7; i++)
	{
		u32Key2 =  u32Key2 << 4;
		u32Key2 += iVal(sTemp1[i]);
	}
	u32Key2 = u32Key2 >> 1;

	/*  */
	for (i=0; i<7; i++)
	{
		sTemp1[i] = sSerialNumber[i+4];
	}
	
	sCryptSalt[0] = 'Z';
	sCryptSalt[1] = '8';

	sTemp1[7] = 0x0a;
	brsstrcpy((UDINT)sBuff, (UDINT)crypt_r(sTemp1, sCryptSalt, sBuff));

	u32Serial = (unsigned int)brsatoi((UDINT)sTemp1);
	u32SerialKeyResult = u32Serial ^ u32Key2;
	
	sprintf(sTemp2, "%8d", u32SerialKeyResult);
	brsstrcat((UDINT)sCalculatedKey, (UDINT)sTemp2);
	
	/* MacKeyResult */
	for (i=0; i<6; i++)
	{
		iMac[i] = iVal(sMacAddr[3*i])<<4;
		iMac[i] = iMac[i] + iVal(sMacAddr[3*i+1]);
	}
	/* scramble them */
	iMac[0] = iMac[0] ^ iMac[5];
	iMac[1] = iMac[1] ^ iMac[4];
	iMac[2] = iMac[2] ^ iMac[3];
	u32MacSum =  iMac[0]<<16;
	u32MacSum += iMac[1]<<8;
	u32MacSum += iMac[2];
	u32MacSum = u32MacSum ^ u32Key2;
	
	sprintf(sTemp2, "%8d", u32MacSum);
	brsstrcat((UDINT)sCalculatedKey, (UDINT)sTemp2);
	
	/* sz */
	
	brsstrcat((UDINT)sCalculatedKey, (UDINT)sBuff);
	
	/* Transmute to lower case and digits only */
	i = 0;
	while (sCalculatedKey[i] != '\0')
	{
		if (sCalculatedKey[i] < '0') /* character ascii code < than 0's code */
			sCalculatedKey[i] = '0' + (sCalculatedKey[i] % 10); /* convert to digits */
		else if ((sCalculatedKey[i] > '9') && (sCalculatedKey[i] < 'a')) /* char is between dgits and lower cases */
			sCalculatedKey[i] = 'a' + ((sCalculatedKey[i] - '9') % ('z'-'a')); /* convert to lower case */
		else if (sCalculatedKey[i] > 'z') /* char is after the lower cases */
			sCalculatedKey[i] = 'a' + ((sCalculatedKey[i] - 'z') % ('z'-'a')); /* convert to lower case */
		i++;
	}
	
	for (j=0; j<17; j++)
		sCalculatedKey[j] = "";	
}

USINT iVal(char c)
{
	if ((c>='0') && (c<='9'))
	{
		return (c-'0');
	}
	else
	{
		if ((c>='a') && (c<='f'))
		{
			return (10+c-'a');
		}
		else if ((c>='A') && (c<='F')) 
         return (10+c-'A');
	}
	return 20; /* here to avoid warning */
}
#endif /* CRYPT_ENABLED */	


/************************************************************************/
/*
   Function:  process_mode_buttons()

   Description:  This function processes the mode buttons. It is common
         code that can be called from multiple pages.
                                                                        */
/************************************************************************/
void process_mode_buttons()
{
	config_super *cs = &cfg_super;
	int i;

	if (HMI.Action_BlendMode == 1)  /* pause the blender, if running ask to end batch */
	{
		if (shr_global.bln_oper_mode == MAN_MODE)
		{
			shr_global.bln_oper_mode = PAUSE_MODE; 
		}
      
		/* commented out first two conditions so that pause could be interrupted after finish batch was selected GTF */
		if (shr_global.bln_oper_mode == PAUSE_MODE)
		{
			HMI.Action_Input7 = gcInActive;
			HMI.Action_BlendMode = 0;
			if (shrBWH.oper_mode != PAUSE_MODE)
			{
				BwhCommand.Command = PAUSE_MODE;
            if (shrBWH.oper_mode == MAN_MODE) //G2-588
				   BwhCommand.PauseAtEndOfBatch = FALSE;
            else
               BwhCommand.PauseAtEndOfBatch = TRUE;
				BwhCommand.CommandPresent = TRUE;
			}
			if (shrMIXER.oper_mode != PAUSE_MODE)
			{
				BwhCommand.Command = PAUSE_MODE;
				BwhCommand.PauseAtEndOfBatch = TRUE;
				BwhCommand.CommandPresent = TRUE;
			}
		}	
		else if (shr_global.bln_oper_mode != MAN_MODE)
		{
			visible(&HMI.pg000_PopupFinishBatchRuntime, TRUE);

			HMI.pg320_PopupTextIndex = 0;
			if (HMI.Action_Input6 == gcActive) // finish the current batch then stop
			{
				visible(&HMI.pg000_PopupFinishBatchRuntime, FALSE);
            
				HMI.Action_Input6 = gcInActive;
				HMI.Action_BlendMode = 0;
				/* send PAUSE command to BWH */
				BwhCommand.Command = PAUSE_MODE;
				BwhCommand.PauseAtEndOfBatch = TRUE;
				BwhCommand.CommandPresent = TRUE;
				HMI.pg450_GoIntoAuto = 0;
				
				visible((unsigned char *)&purge_button_visible, TRUE);
            
				if (debugFlags.time_weight_to_csv == TRUE) 
               Signal.Store_Timewt_as_CSV = TRUE;
				Signal.Store_Timewt = TRUE;
			}
			else if (HMI.Action_Input7 == gcActive) // stop immediately
			{
				HMI.Action_Input7 = gcInActive;
				visible(&HMI.pg000_PopupFinishBatchRuntime, FALSE);

				HMI.Action_BlendMode = 0;
				/* send PAUSE command to BWH */
				BwhCommand.Command = PAUSE_MODE;
				BwhCommand.PauseAtEndOfBatch = FALSE;
				BwhCommand.CommandPresent = TRUE;

				/* send PAUSE command to mixer */
				MixerCommand.Command = PAUSE_MODE;
				MixerCommand.PauseAtEndOfBatch = FALSE;
				MixerCommand.CommandPresent = TRUE;
				
				visible((unsigned char *)&purge_button_visible, TRUE);

				/* Close all gates GTF */
				close_all_gates_GTF();						// G2-337 4/19/17 Dragan
				if (debugFlags.time_weight_to_csv == TRUE) 
					Signal.Store_Timewt_as_CSV = TRUE;
				Signal.Store_Timewt = TRUE;
            if (cfg_super.current_blender_type == AUTOBATCH) //G2-651
		      {
               HMI.BatchStopFlg = TRUE;
            }
			}
			else if (HMI.Action_Input4 == gcActive) // CANCEL stop immediately
			{
				HMI.Action_Input4 = gcInActive;
				HMI.pg000_PopupFinishBatchRuntime = 1;
				HMI.Action_BlendMode = 0;
			}
		}
	}
	else if (HMI.Action_BlendMode == 2)
	{      
		/* go into manual mode */
		HMI.pg005_manual_GateControl = 12;
		Interlock_Open_Ack = FALSE;
		HMI.Action_BlendMode = 0;
		BwhCommand.CommandPresent = TRUE;
		BwhCommand.Command = MAN_MODE;
		
		if (cfg_super.current_blender_type == AUTOBATCH)
		{
         HMI.BatchStopFlg = FALSE; //G2-651
			HMI.ChangePage = pg005_BlendManualBackup1;
			Interlock_Open_Ack = 2;
		}
		else	
			HMI.ChangePage = pg005_BlendManualBackup1;
						
		HMI.ChangePage = pg005_BlendManualBackup1;
		BLN_OPER_MODE = MAN_MODE;
		buttons_manual_reset();					// G2-337 4/19/17 Dragan
	}
	else if (HMI.Action_BlendMode == 3)
	{
		if (debugFlags.greg)
		{
			// printing blender size, frame size, # of hoppers, units and recipe 
			bgDbgInfo(&g_RingBuffer1, "interface.c", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
            "New Test Conditions:,%d,%d,%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f,%f  \r\n",
				cfg_super.pg175_System_Size, cfg_super.total_gates,cfg_super.weight_unit,
				BATCH_RECIPE_NEW->parts.s[0], BATCH_RECIPE_NEW->parts.s[1], BATCH_RECIPE_NEW->parts.s[2], BATCH_RECIPE_NEW->parts.s[3],
				BATCH_RECIPE_NEW->parts.s[4], BATCH_RECIPE_NEW->parts.s[5], BATCH_RECIPE_NEW->parts.s[6], BATCH_RECIPE_NEW->parts.s[7],
				BATCH_RECIPE_NEW->parts.s[8], BATCH_RECIPE_NEW->parts.s[9], BATCH_RECIPE_NEW->parts.s[10], BATCH_RECIPE_NEW->parts.s[11]);
			for (i=0; i<cfg_super.total_gates; i++)
			{	
		      // printing gate settings: index, out of spec limits, #of bad feeds before alarm, clear gate calibration after out of spec
            // # of feeds before out of material, hopper type, algorithm, auger used, prepulse enabled, target accuracy
				bgDbgInfo(&g_RingBuffer1, "interface.c", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
					"Gate ,%d,%f,%d,%d,%d,%d,%d,%d,%d,%f \r\n", i, cfgGATE[i].out_of_spec_limit, cfgGATE[i].num_bad_feeds_before_alarm
					,cfgGATE[i].clear_gate_cal_after_out_of_spec, cfgGATE[i].num_no_feeds_before_out_of_mat, cfgGATE[i].hopper_type
					,cfgGATE[i].algorithm, cfgGATE[i].auger_used, cfgGATE[i].prepulse_enabled, cfgGATE[i].target_accuracy);
			}
		}
		// when we press AUTO. clear gate alarms.  BEB 
	
		shr_global.num_recipe_inven = PMEM.recipe_num;	// pg 320 will display the active recipe total 
		
		for (i=0; i<MAX_GATES; i++)
		{
			shrGATE[i].alarm_bits &= ((~((WT_CRIT|DEV_OUT_OF_SPEC|HOPPER_LEVEL_LOW|WT_CRIT_TIME|HOPPER_PROX_LOW|HOPPER_PROX_HIGH))) | shrGATE[i].alarm_force_bits);
		}
		shrMIXER.alarm_bits &= ((~(MIX_VALVE_FAILURE|MIXER_MOTOR_ALARM)) | shrMIXER.alarm_force_bits);
		cfg_super.uc3ProxyRegrindSleep = TRUE;
		HMI.Action_BlendMode = 0;
		
		// job wt 
		if ((cfg_super.job_wt_in_recipe) && (BATCH_RECIPE_EDIT.job_wt > .01))
			cfg_super.job_wt = BATCH_RECIPE_EDIT.job_wt;
		
		if ((cs->recipe_entry_mode == PERCENT && (!((BATCH_RECIPE_NEW->parts.ts > 99.9995) && (BATCH_RECIPE_NEW->parts.ts < 100.0005)))) ||
			(!cfgBWH.cal_done) || (shr_global.recipe_changed))
		{
			if (shr_global.recipe_changed)
			{
				gNewMaterial = TRUE;
				shrBWH.num_batches = 0;
				shrBWH.batch_target = 1;
			}
			recipe_start_state = RECIPE_INIT;
         //drt 040317 G2-324
         if (shr_global.recipe_changed)
			{
			   HMI.ChangePage = pg451_Recipe_Check;
			   gPage = pg000_MainPage;
         }
         else
         {
            HMI.ChangePage = pg000_MainPage;
			   gPage = pg000_MainPage;
         }
		}
		else
		{
			/* go into automatic mode */
			gPage = pg000_MainPage;
         copy_recipe(BATCH_RECIPE_NOW, BATCH_RECIPE_NEW, BLENDER_RECIPE); //G2-418
			for (i=0; i<MAX_GATES; i++)
			{
            //G2-418
            if (BATCH_RECIPE_NOW->parts_pct.s[i] == 0.0)  // not in recipe
            {
                GateCommand[i].Command = PAUSE_MODE;
                GateCommand[i].CommandPresent = TRUE;
            }
				shrGATE[i].set_wt = 0.0; //drt ?
			}

			BwhCommand.CommandPresent = TRUE;
			BwhCommand.Command = AUTO_MODE;
			if (debugFlags.jeff)
				bgDbgInfo(&g_RingBuffer1, "interface.c", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
				"AutoMode Engaged\r\n");
			if (cfgBWH.demo)
				shrBWH.wt_bits = cfgBWH.zerowt;
		}

		/* Auto just pressed clear data            */
		/* this is to do with WTP calculations GTF */
		batch_start_time = start_time();
		tenpt_ma_fclear (&shrBWH.avg_wtp_rate);
		tenpt_ma_fclear (&shrBWH.max_wtp_rate);
		tenpt_ma_fclear (&shrBWH.avg_batch_size);
		tenpt_ma_iclear (&shrBWH.avg_batch_time);
		shrBWH.max_wtp = 0.0;
		shrBWH.last_batch_time = 0.0;
		shrBWH.last_batch_size = 0.0;
		last_batch_start_time = start_time();
		// no need to clear the job_wt_subtotal each time the Auto mode started
		if (cfg_super.job_wt_complete == TRUE)
			cfg_super.job_wt_subtotal = 0.0;
		
		//cfg_super.job_wt_subtotal = 0.0;
		cfg_super.job_wt_complete = FALSE;
		visible((unsigned char *)&purge_button_visible, FALSE);
		// recipe name and number on main page
		brsstrcpy((UDINT)PMEM.recipe_name, (UDINT)BATCH_RECIPE_EDIT.product_code);
		//PMEM.recipe_num = debugFlags.recipeIdx;
      
		if (EXT_HO_OPER_MODE == AUTO_MODE)
			density_may_changed = TRUE;
	}

	if (HMI.Action_ExtruderMode == 1)
	{      
		/* go into paused mode */
		HMI.Action_ExtruderMode = gcInActive;
		stop_ext();
	}
	else if (HMI.Action_ExtruderMode == 2)
	{
		if (shr_global.alarm_bits & SYS_SHUTDOWN)
			/*  if (!in_remote)  */
		{
			visible(&HMI.pg000_PopupClearAlarms, TRUE);
		}
		else
		{
         if (Ext_running_mode != MONITOR_MODE)
         {
			   visible(&HMI.pg000_PopupClearAlarms, FALSE);
			   HMI.Action_ExtruderMode = gcInActive;
			   init_manual();
			   /* HMI.ChangePage = pg009_ExtManual;*/
         }
		}

		HMI.Action_ExtruderMode = gcInActive;
	}
	else if (HMI.Action_ExtruderMode == 3) 
	{    
		if (cfgEXT.rs_ave)
			err_ext = 0;
      
		if (((!cfgEXT.cal_done)&&(cs->wtp_mode == CONTROL)) || ((!cfgHO.cal_done) && (cs->ltp_mode == CONTROL)))
		{
			visible(&HMI.pg000_PopupNoCalEXT, TRUE);
			if (NoCalEXT_Ack)
			{
				HMI.Action_ExtruderMode = gcInActive;
				HMI.ChangePage = pg350_Calibrate;	
				visible(&HMI.pg000_PopupNoCalEXT, FALSE);
				NoCalEXT_Ack = 0;
			}
		}   
		else if (((cfgEXT.rs_ave <= 0.001) && (cs->wtp_mode == CONTROL)) || ((cfgHO.rs_ave <= 0.001)&&(cs->ltp_mode == CONTROL)) ||(err_ext == 256))
		{
			visible(&HMI.pg000_PopupNoRateSpd, TRUE);
			if (NoRateSpd_Ack)
			{
				HMI.Action_ExtruderMode = gcInActive;
				HMI.ChangePage = pg009_ExtManual;
				visible(&HMI.pg000_PopupNoRateSpd, FALSE);
				NoRateSpd_Ack = 0;
				err_ext = 0;
			}
		}
			/* go into automatic mode */
		else 
		{
			visible(&HMI.pg000_PopupNoRateSpd, FALSE);
			HMI.Action_ExtruderMode = gcInActive;
			if (shr_global.alarm_bits & SYS_SHUTDOWN)             /* have shutdown alarm  */
				visible(&HMI.pg000_PopupClearAlarms, TRUE);
			else
			{
				visible(&HMI.pg000_PopupClearAlarms, FALSE);
				if (cs->no_auto_from_pause && (EXT_HO_OPER_MODE == PAUSE_MODE))
					visible(&HMI.pg000_PopupCannotAutoPause, TRUE);
				else
				{
					visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
					run_ext();
				}
			}
		}
	}
	
	if ((cfg_super.clear_shift_total_flag == 1) && (shr_global.recipe_changed))
		clear_shift_totals();

   if ((cfg_super.clear_rec_total == 1) && (shr_global.recipe_changed)) //G2-660
		clear_recipe_totals();
}
/************************************************************************/
/*
   Function:  stop_ext()

   Description:  sends commands to the extruder, refeed, ho, width device
         to stop the devices. This is all based on the configuration of
         the system.
                                                                        */
/************************************************************************/
void stop_ext(void)
{
	config_super *cs = &cfg_super;
	float HO_ad;
   
	clear_recipe(BATCH_RECIPE_NOW, EXTRUDER_RECIPE);
	EXT_HO_OPER_MODE = PAUSE_MODE; 
	shrEXT.set_spd = 0;
	DriveCommand[0].SetSpeed = 0.0;
	shrEXT.init_man_extspd = 1;
	shrHO.init_man_extspd = 1;
	shrEXT.alarm_bits &= ((~(DEV_OVERSPEED))|shrEXT.alarm_force_bits);
	shrHO.alarm_bits  &= ((~(DEV_OVERSPEED))|shrHO.alarm_force_bits);
	shrSHO.alarm_bits  &= ((~(DEV_OVERSPEED))|shrSHO.alarm_force_bits);
	
	if (cs->wtp_mode == CONTROL)
	{
		LiwCommand[0].CommandPresent = TRUE;
		LiwCommand[0].Command = PAUSE_MODE;
		LiwCommand[0].SetSpeed = 0.0;
		LiwCommand[0].SetAccelDecel = cfgEXT.decel;
	}
	else if (cs->wtp_mode == MONITOR)
	{
		LiwCommand[0].CommandPresent = TRUE;
		LiwCommand[0].Command = MONITOR_MODE;
		LiwCommand[0].SetSpeed = 0.0;
		LiwCommand[0].SetAccelDecel = 0.0;
	}

	if (cs->refeed == GRAVIFLUFF)
	{
		LiwCommand[1].CommandPresent = TRUE;
		LiwCommand[1].Command = PAUSE_MODE;
		LiwCommand[1].SetSpeed = 0.0;
		LiwCommand[1].SetAccelDecel = cfgGFEEDER.decel;
	}

	if (cs->ltp_mode == CONTROL)
	{
		if (cfgHO.stop_instantly)
			HO_ad = 0.0;
		else
			HO_ad = cfgHO.decel;

		HOCommand.CommandPresent = TRUE;
		HOCommand.Command = PAUSE_MODE;
		HOCommand.SetSpeed = 0.0;
		HOCommand.SetAccelDecel = HO_ad;
	}
	else if (cs->ltp_mode == MONITOR)
	{
		HOCommand.CommandPresent = TRUE;
		HOCommand.Command = MONITOR_MODE;
		HOCommand.SetSpeed = 0.0;
		HOCommand.SetAccelDecel = 0.0;
	}

	if (cs->sltp_mode == MONITOR)
	{
		SHOCommand.CommandPresent = TRUE;
		SHOCommand.Command = MONITOR_MODE;
		SHOCommand.SetSpeed = 0.0;
		SHOCommand.SetAccelDecel = 0.0;
	}

	if (cs->wth_mode != UNUSED)
	{
		WTHCommand.CommandPresent = TRUE;
		WTHCommand.Command = PAUSE_MODE;
		WTHCommand.Wth = 0;
	}
}
/************************************************************************/
/*
   Function:  Print_Main()

   Description:  watches the HMI for the print page and appropriately calls
                 the correct page setup routines.
                                                                        */
/************************************************************************/
void Print_Main()
{
	switch (HMI.Action_Input0) 
	{
		case 1: // changed a print option. need to show button
			HMI.pg300_PrinterInitialized = 0;
			Signal.EnteredSetup = TRUE;
			break;
		case 2: // pushed the intialize print button
			HMI.pg300_PrinterInitialized = 1;	
			break;
		default:
			break;
	}
	HMI.Action_Input0 = 0;
   
	if (cfg_super.spare_port_1.connect_type == PRINT_TO_SERIAL_PRINTER)
		visible(&HMI.SetupRuntime2, TRUE);
	else
		visible(&HMI.SetupRuntime2, FALSE);
	
	if (HMI.pg300_PrinterInitialized == 0)
		visible(&HMI.SetupRuntime1, TRUE);
	else
		visible(&HMI.SetupRuntime1, FALSE);
}
/************************************************************************/
/*
   Function:  AcknowledgeAlarms()

   Description:  This function acknowledges the active alarms database.
         It must be run cyclically so that is why it is located here and
         not in alarms.c
                                                                        */
/************************************************************************/
void AcknowledgeAlarms()
{
	config_super *cs = &cfg_super;
	unsigned int status;
	int i;
	int group = 0xFFFF;

   //G2-698
#ifdef PP65
   gVCHandle = VA_Setup(1, "visual");
#endif
#ifdef C70
   gVCHandle = VA_Setup(1, "vis_fo");
#endif
#ifdef C70_003
   gVCHandle = VA_Setup(1, "vis_fo");
#endif
	if (gVCHandle)
	{
		if (!VA_Saccess(1, gVCHandle))
		{
			status = VA_QuitAlarms(1, gVCHandle, group);
			VA_Srelease(1, gVCHandle);

			if (status == 0)				
			{
				shr_global.alarm_latch_bits &= shr_global.alarm_bits;
				shrBWH.alarm_latch_bits &= shrBWH.alarm_bits;
				shrMIXER.alarm_latch_bits &= shrMIXER.alarm_bits;
				for (i=0; i<cs->total_gates; i++)
				{
					shrGATE[i].alarm_latch_bits &= shrGATE[i].alarm_bits;
				}
				if ((cs->wtp_mode == MONITOR) || (cs->wtp_mode == CONTROL))
					shrEXT.alarm_latch_bits &= shrEXT.alarm_bits;
				if ((cs->ltp_mode == MONITOR) || (cs->ltp_mode == CONTROL))
					shrHO.alarm_latch_bits &= shrHO.alarm_bits;

				gFlag_AckAlarms = FALSE;
			}
		}
	}
}
/************************************************************************/
/*
   Function:  ClearAlarmHistory()

   Description:  This functions clears the alarm history/log. It must run
         cyclically and that is why it is located here.
                                                                        */
/************************************************************************/
void ClearAlarmHistory()
{
	unsigned int status;

	if (!State_ClearAlarmList)
	{
//G2-698
#ifdef PP65
      VCHandle = VA_Setup(1, "visual");
#endif
#ifdef C70
      VCHandle = VA_Setup(1, "vis_fo");
#endif
#ifdef C70_003
      VCHandle = VA_Setup(1, "vis_fo");
#endif
		if (VCHandle)
			State_ClearAlarmList = 1;
	}

	if (State_ClearAlarmList)
	{
		if (!VA_Saccess(1, VCHandle))
		{
			status = VA_DelAlarmHistory(1, VCHandle);
			VA_Srelease(1, VCHandle);
			if (status == 0)
				gFlag_ClearAlarmHistory = FALSE;
		}
		shr_global.alarm_bits       = 0;
		shr_global.alarm_latch_bits = 0;
		shrEXT.alarm_bits           = 0;
		shrEXT.alarm_latch_bits     = 0;
		shrHO.alarm_bits            = 0;
		shrHO.alarm_latch_bits      = 0;
		shrSHO.alarm_bits           = 0;
		shrSHO.alarm_latch_bits     = 0;
	}
}
/************************************************************************/
/*
   Function:  SetSecurity()

   Description:  Based on the current and set security levels, there are
         HMI objects which should appear/disappear. This function sets the
         visibility of these objects based on security and setup.
                                                                        */
/************************************************************************/
void SetSecurity()
{
	//int i;
	config_super *cs = &cfg_super;

	switch (cs->current_security_level)
	{
		case SERVICE_LOCK:
			HMI.SecLevelColor = gRed;
			break;
		case SUPERVISOR_LOCK:
			HMI.SecLevelColor = gOrange;
			break;
		case MAINTENANCE_LOCK:
			HMI.SecLevelColor = gYellow;
			break;
		case OPERATOR_LOCK:
			HMI.SecLevelColor = gGreen;
			break;
		default:
			HMI.SecLevelColor = gBlue;
			break;
	}
	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_SETUP])
		visible(&HMI.SetupLock, TRUE);      /* shown */
	else
		visible(&HMI.SetupLock, FALSE);     /* hidden */

	if ((cfg_super.current_security_level >= cfg_super.pe_needed[PE_CALIBRATE]) || (shrMIXER.oper_mode == AUTO_MODE)) //65 is AUTO_MODE
		visible(&HMI.CalibrateLock, TRUE);      /* locked */
	else
		visible(&HMI.CalibrateLock, FALSE);     /* unlocked */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_CHANGE_RECIPE])
	{
//		for (i=0; i<cfg_super.total_gates; i++)
//			HMI.pg450_RecipeEditLock[i] = 1;
		visible(&HMI.ChangeRecipeLock, TRUE);   /* shown change recipe enable*/
	}
	else
	{
//		for (i=0; i<cfg_super.total_gates; i++)
//			HMI.pg450_RecipeEditLock[i] = 0;
		
		visible(&HMI.ChangeRecipeLock, FALSE);     /* hidden change recipe*/
	}

   // G2-662
   if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_SELECT_RECIPE])
		visible(&HMI.SelectRecipeLock, TRUE);      /* shown store recipe enable */
	else
		visible(&HMI.SelectRecipeLock, FALSE);     /* hidden store recipe enable */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_STORE_RECIPE])
		visible(&HMI.StoreRecipeLock, TRUE);      /* shown store recipe enable */
	else
		visible(&HMI.StoreRecipeLock, FALSE);     /* hidden store recipe enable */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_BLN_MANUAL]) 
		visible(&HMI.BlendManualRuntime, TRUE);       /* shown blender manual backup*/
	else
		visible(&HMI.BlendManualRuntime, FALSE);      /* hidden blender manual backup*/

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_EXT_MANUAL])
	{
      if (cfg_super.wtp_mode == -1)
         HMI.ExtManualRuntimelck = 0; //G2-641
      else
         HMI.ExtManualRuntimelck = 1; //G2-641

		//	if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED))
		visible(&HMI.ExtManualRuntime, TRUE);       /* shown extruder manual if system configured and security allows*/
	}
	else
		visible(&HMI.ExtManualRuntime, FALSE);      /* hidden extruder manual button */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_STOP])
	{
		visible(&HMI.BlendStopRuntime, TRUE);       /* shown blender stop button */
		if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED))
			visible(&HMI.ExtRunRuntime, TRUE);       /* shown extruder auto if configured and security allows */
	}
	else
	{
		visible(&HMI.ExtStopRuntime,   FALSE);     /* hidden extruder stop button*/
		visible(&HMI.BlendStopRuntime, FALSE);     /* hidden blender pause button */
	}
	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_RUN])
	{
		visible(&HMI.BlendRunRuntime, TRUE);       /* shown blender auto button */
		if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED))
			visible(&HMI.ExtStopRuntime, TRUE);     /* shown extruder stop if configured and security allows */
	}
	else
	{
		visible(&HMI.BlendRunRuntime, FALSE);     /* hidden auto button enable */
		visible(&HMI.ExtRunRuntime,   FALSE);     /* hidden extruder run button*/
	}

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_SHIFT])
		visible(&HMI.ClearShiftLock, TRUE);      /* shown clear shift */
	else
		visible(&HMI.ClearShiftLock, FALSE);     /* hidden clear shift */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_INVEN])
		visible(&HMI.ClearInvenLock, TRUE);      /* shown clear inventory */
	else
		visible(&HMI.ClearInvenLock, FALSE);     /* hidden clear inventory */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RESIN])
		visible(&HMI.ClearResinLock, TRUE);      /* shown clear resins */
	else
		visible(&HMI.ClearResinLock, FALSE);     /* hidden clear resins */

	if (cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RECIPE])
		visible(&HMI.ClearRecipeLock, TRUE);     /* shown clear recipes */
	else
		visible(&HMI.ClearRecipeLock, FALSE);    /* hidden clear recipes */

	if (cfg_super.current_security_level > SUPERVISOR_LOCK)
		visible(&HMI.ServiceLock, TRUE);      /* shown service level */
	else
		visible(&HMI.ServiceLock, FALSE);     /* hidden service level */

	/* the following settings enable/disable visiblity based on the system setup */
	HMI.NumDecimalPoints = 0;

	if (cfg_super.wth_mode > UNUSED)
		visible(&HMI.WTHRuntime, TRUE);      /* shown width available */
	else
		visible(&HMI.WTHRuntime, FALSE);     /* hidden width not available */

	if ((cfg_super.wtp_mode == MONITOR) || (cfg_super.wtp_mode == CONTROL))
		visible(&HMI.WTPRuntime, TRUE);      /* shown extruder */
	else
	{
		visible(&HMI.WTPRuntime, FALSE);     /* hidden extruder */
		visible(&HMI.ExtRunRuntime, FALSE); 
		visible(&HMI.ExtManualRuntime, FALSE); 
		visible(&HMI.ExtStopRuntime, FALSE); 
	}

	if (GravitrolHopperAlone)
		visible(&HMI.BLNRuntime, FALSE);	
	else 
		visible(&HMI.BLNRuntime, TRUE);
	
	if ((cs->ltp_mode == MONITOR) || (cs->ltp_mode == CONTROL))
		visible(&HMI.LTPRuntime, TRUE);      /* shown haul off */
	else
		visible(&HMI.LTPRuntime, FALSE);     /* hidden haul off */

	if ((cs->sltp_mode == MONITOR) || (cs->sltp_mode == CONTROL))
		visible(&HMI.SLTPRuntime, TRUE);     /* shown secondary haul off */
	else
		visible(&HMI.SLTPRuntime, FALSE);    /* hidden secondary haul off */

	if (cs->refeed == GRAVIFLUFF)
		visible(&HMI.GFLFRuntime, TRUE);     /* shown gravifluff feeder */
	else
		visible(&HMI.GFLFRuntime, FALSE);    /* hidden gravifluff feeder */

	if (cs->blend_control >= 1) //G2-456
		visible(&HMI.PUMPRuntime, TRUE);     /* shown pump configuration */
	else
		visible(&HMI.PUMPRuntime, FALSE);    /* hidden pump configuration */
}

/************************************************************************/
/*
   Function:  SetSystemVisibility()

   Description:  This routine sets visualization options for visibility
        based on the configuration of the system.
                                                                        */
/************************************************************************/
void SetSystemVisibility()
{
	config_super *cs = &cfg_super;
   
	visible(&HMI.HopARuntime, FALSE);
	visible(&HMI.HopBRuntime, FALSE);
	visible(&HMI.HopCRuntime, FALSE);
	visible(&HMI.HopDRuntime, FALSE);
	visible(&HMI.HopERuntime, FALSE);
	visible(&HMI.HopFRuntime, FALSE);
	visible(&HMI.HopGRuntime, FALSE);
	visible(&HMI.HopHRuntime, FALSE);
	visible(&HMI.HopIRuntime, FALSE);
	visible(&HMI.HopJRuntime, FALSE);
	visible(&HMI.HopKRuntime, FALSE);
	visible(&HMI.HopLRuntime, FALSE);

	if (cs->total_gates >= 1)
		visible(&HMI.HopARuntime, TRUE);

	if (cs->total_gates >= 2)
		visible(&HMI.HopBRuntime, TRUE);

	if (cs->total_gates >= 3)
		visible(&HMI.HopCRuntime, TRUE);

	if (cs->total_gates >= 4)
		visible(&HMI.HopDRuntime, TRUE);

	if (cs->total_gates >= 5)
		visible(&HMI.HopERuntime, TRUE);

	if (cs->total_gates >= 6)
		visible(&HMI.HopFRuntime, TRUE);

	if (cs->total_gates >= 7)
		visible(&HMI.HopGRuntime, TRUE);

	if (cs->total_gates >= 8)
		visible(&HMI.HopHRuntime, TRUE);

	if (cs->total_gates >= 9)
		visible(&HMI.HopIRuntime, TRUE);

	if (cs->total_gates >= 10)
		visible(&HMI.HopJRuntime, TRUE);

	if (cs->total_gates >= 11)
		visible(&HMI.HopKRuntime, TRUE);

	if (cs->total_gates >= 12)
		visible(&HMI.HopLRuntime, TRUE);

	/* setup parameters based only on security */
	SetSecurity();

	if (!((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED)) && (cfg_super.ucIconInterface) )
	{
		visible(&HMI.pg000_LayerRuntime1 ,TRUE);  // layer reference for Icons 
		visible(&HMI.pg000_LanguageRuntime, FALSE);
		visible(&HMI.BlendStopRuntime, FALSE);
		visible(&HMI.BlendRunRuntime, FALSE);
		visible(&HMI.BlendManualRuntime, FALSE);

		visible(&HMI.ExtControlRuntime1, FALSE);    /* hidden extruder manual if system configured and security allows*/
		visible(&HMI.ExtManualRuntime, FALSE);      /* hidden extruder manual if system configured and security allows*/
		visible(&HMI.ExtRunRuntime, FALSE);         /* hidden extruder manual if system configured and security allows*/
		visible(&HMI.ExtStopRuntime, FALSE);        /* hidden extruder manual if system configured and security allows*/      
	}
	else
	{
		visible(&HMI.PopupRuntime1, FALSE);
		visible(&HMI.PopupRuntime1, FALSE);
		visible(&HMI.PopupRuntime2, FALSE);
		visible(&HMI.PopupRuntime3, FALSE);
		visible(&HMI.PopupRuntime4, FALSE);
		visible(&HMI.PopupRuntime5, FALSE);
		visible(&HMI.pg000_LayerRuntime1, FALSE);  // layer reference for Icons 
		
		/* setup parameters based on configuration */
		if (cs->MultiLingual == TRUE)
			visible(&HMI.pg000_LanguageRuntime, TRUE);
		else
			visible(&HMI.pg000_LanguageRuntime, FALSE);

		visible(&HMI.MainRuntime, TRUE);   

		if (!((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED)))
		{
			visible(&HMI.ExtControlRuntime1, FALSE);     /* hidden extruder manual if system configured and security allows*/
		   //visible(&HMI.ExtManualRuntime, FALSE);     /* hidden extruder manual if system configured and security allows*/
			visible (&HMI.ExtRunRuntime, FALSE);         /* hidden extruder manual if system configured and security allows*/
			visible(&HMI.ExtStopRuntime, FALSE);         /* hidden extruder manual if system configured and security allows*/
		}
		else
		{
		   //visible(&HMI.ExtManualRuntime, TRUE);   /* shown extruder manual if system configured and security allows*/
			visible(&HMI.ExtRunRuntime, TRUE);        /* shown extruder manual if system configured and security allows*/
			visible(&HMI.ExtStopRuntime, TRUE);       /* shown extruder manual if system configured and security allows*/
			//cfgMIXER.material_request_source = MATERIAL_REQUEST_GRAVITROL;  /* make it default when using extrusion control */
		}
	}
	brsstrcpy((UDINT)cfg_super.Info.FirmwareNum, (UDINT)WE_VERSION);   
	// some HMI stuff to correctly show layers on startup
	visible(&HMI.pg335_LayerRuntime1, FALSE);
	HMI.pg345change_layer = 0;
	visible(&HMI.pg345_LayerRuntime1, FALSE);
	visible(&HMI.pg345_LayerRuntime2, FALSE);
	HMI.pg405change_layer = 0;
	visible(&HMI.pg405_LayerRuntime1, FALSE);
	visible(&HMI.pg405_LayerRuntime2, FALSE);
	HMI.pg406change_layer = 0;
	visible(&HMI.pg406_LayerRuntime1, FALSE);
	visible(&HMI.pg406_LayerRuntime2, FALSE);
	visible(&HMI.pg334_LayerRuntime1, FALSE);
	HMI.pg453change_layer = 0;
	visible(&HMI.pg453_LayerRuntime1, FALSE);
	visible(&HMI.pg453_LayerRuntime2, FALSE);
}

/************************************************************************/
/*
   Function:  SetDisplayUnits()

   Description:  This routine sets visualization options for units display
        based on the configuration of the system.
                                                                        */
/************************************************************************/
void SetDisplayUnits()
{
	HMI.UnitIDChange = cfg_super.id_unit;
	HMI.UnitODChange = cfg_super.od_unit;
	HMI.UnitWidthChange = cfg_super.width_unit;
	HMI.UnitODChange = cfg_super.length_unit;
	HMI.UnitThicknessChange = cfg_super.thick_unit;
	HMI.UnitMassChange = cfg_super.weight_unit;
	HMI.UnitMassAreaChange = cfg_super.wpa_unit;
	HMI.UnitLengthRateChange = cfg_super.ltp_unit;
	HMI.UnitMassLengthChange = cfg_super.wpl_unit;
	HMI.UnitMassRateChange = cfg_super.wtp_unit;
	HMI.UnitTempChange = cfg_super.temp_unit;
	HMI.UnitPercentChange = 0;
	HMI.UnitLengthChange = cfg_super.length_unit;
}
/************************************************************************/
/*
   Function:  SetContrast_Brightness()

   Description:  This routine sets visualization options for visibility
        based on the configuration of the system.
                                                                        */
/************************************************************************/
void SetContrast_Brightness()
{
	int success;
	config_super *cs = &cfg_super;

	if ((gVC_ChangeDisplay & gChangeContrast) == gChangeContrast)
	{
#ifdef PP65
         gVCHandle = VA_Setup(1, "visual");
#endif
#ifdef C70
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
#ifdef C70_003
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
		if (gVCHandle)
		{
			if (!VA_Saccess(1, gVCHandle))
			{
				success = VA_SetContrast(1, gVCHandle,cs->scrContrast);
				gVC_ChangeDisplay ^= gChangeContrast;
				VA_Srelease(1, gVCHandle);
			}
		}
	}

	if ((gVC_ChangeDisplay & gChangeBrightness) == gChangeBrightness)
	{
#ifdef PP65
         gVCHandle = VA_Setup(1, "visual");
#endif
#ifdef C70
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
#ifdef C70_003
         gVCHandle = VA_Setup(1, "vis_fo");
#endif
		if (gVCHandle)
		{
			if (!VA_Saccess(1, gVCHandle))
			{
				success = VA_SetBrightness(1, gVCHandle, cs->scrBrightness);
				gVC_ChangeDisplay ^= gChangeBrightness;
				VA_Srelease(1, gVCHandle);
			}
		}
	}
}

/************************************************************************/
/*
   Function:  process_leds()

   Description:  The following routine processes the status indicators on
            the main page as well as the monitor page.
                                                                        */
/************************************************************************/
void process_leds(void)
{
	config_super *cs = &cfg_super;
	static int count;

	if (gVC_ChangeDisplay > 0)
		SetContrast_Brightness();
	count += 1;

   /* the following code is used to change the color and visiblity of the
      alarm button on the page footer */
	if (shr_global.alarm_bits & SYS_SHUTDOWN)
	{
		visible(&HMI.AlarmRuntime, TRUE);
		/* togle between red and orange GTF */
		if (count < 3)
			HMI.AlarmBtnColor = gRed;
		else if (count < 6)
			HMI.AlarmBtnColor = gOrange;
		else
			count = 0;
	}
	else if (shr_global.alarm_bits & GEN_ALARM)
	{
		visible(&HMI.AlarmRuntime, TRUE);
		/* togle between red and orange GTF */
		if (count < 3)
			HMI.AlarmBtnColor = gRedOrange;
		else if (count < 6)
			HMI.AlarmBtnColor = gOrange;
		else
			count = 0;
	}
	else if (shr_global.alarm_bits & INFO_ALARM)
	{
		visible(&HMI.AlarmRuntime, TRUE);
		/* togle between red and orange GTF */
		if (count < 3)
			HMI.AlarmBtnColor = gDarkYellow;
		else if (count < 6)
			HMI.AlarmBtnColor = gOrange;
		else
			count = 0;
	}
	else
	{
		visible(&HMI.AlarmRuntime, FALSE);
		HMI.AlarmBtnColor = gOrange;
	}

	/* the following code for the blender stop led & extruder stop leds should be moved */
	/* to show_oper_mode when the led blink function is fixed in the driver             */
	if (shr_global.pause_pending)     /* should be blinking here for when it works */
	{
		HMI.BlendModeText = 0;
		HMI.BlendModeColor = gYellow;
		HMI.BlendManualLedColor = gWhite;
		HMI.BlendRunLedColor = gWhite;
		HMI.BlendStopLedColor = gYellow;
		HMI.pg330_BlendModeIndex  = 0;
		HMI.pg330_BlendModeColor  = gYellow;
		HMI.BlendManualLock = 0;  /* locked manual button unless paused */
		HMI.BlendMainBtnColor = gYellow;
	}
	else if (BLN_OPER_MODE == PAUSE_MODE)
	{
		HMI.BlendModeText = 0;
		HMI.BlendModeColor = gLtGreen;
		HMI.BlendManualLedColor = gWhite;
		HMI.BlendRunLedColor = gWhite;
		HMI.BlendStopLedColor = gRed;
		HMI.pg330_BlendModeIndex  = 1;
		HMI.pg330_BlendModeColor  = gRed;
		HMI.BlendMainBtnColor = gRed;
		HMI.BlendManualLock = 1;   /* paused so manual backup allowed */
	}
	else if (BLN_OPER_MODE == MAN_MODE)
	{
		HMI.BlendModeText = 1;
		HMI.BlendModeColor = gBlue;
		HMI.BlendStopLedColor = gWhite;
		HMI.BlendRunLedColor = gWhite;
		HMI.BlendManualLedColor = gBlue;
		HMI.pg330_BlendModeIndex  = 2;
		HMI.pg330_BlendModeColor  = gBlue;
		HMI.BlendMainBtnColor = gBlue;
		HMI.BlendManualLock = 1;   /* in manual backup so of course not locked */
	}
	else if (BLN_OPER_MODE == AUTO_MODE)
	{
		HMI.BlendModeText = 2;
		HMI.BlendModeColor = gLtRed;
		HMI.BlendStopLedColor = gWhite;
		HMI.BlendRunLedColor = gGreen;
		HMI.BlendManualLedColor = gWhite;
		HMI.pg330_BlendModeIndex  = 3;
		HMI.pg330_BlendModeColor  = gGreen;
		HMI.BlendMainBtnColor = gGreen;
		HMI.BlendManualLock = 0;   /* locked manual button unless paused */
	}

	if ((cs->wtp_mode == CONTROL) || (cs->ltp_mode == CONTROL))
	{
		if ((EXT_HO_OPER_MODE == PAUSE_MODE) && ((EXT_ACT_SPD > 0.0003) || (HO_ACT_SPD > 0.0003)))
		{
			HMI.ExtStopLedColor = gYellow;
			HMI.ExtManualLedColor = gWhite;
			HMI.ExtRunLedColor = gWhite;
			HMI.pg330_ExtModeIndex  = 0;
			HMI.pg330_ExtModeColor  = gYellow;
		}
		else if ((EXT_HO_OPER_MODE == PAUSE_MODE) && (EXT_ACT_SPD < 0.0003) && (HO_ACT_SPD < 0.0003))
		{
			HMI.ExtStopLedColor = gRed;
			HMI.ExtManualLedColor = gWhite;
			HMI.ExtRunLedColor = gWhite;
			HMI.pg330_ExtModeIndex  = 1;
			HMI.pg330_ExtModeColor  = gRed;
		}
		else if ((EXT_HO_OPER_MODE == MAN_MODE))
		{
			HMI.ExtStopLedColor = gWhite;
			HMI.ExtManualLedColor = gBlue;
			HMI.ExtRunLedColor = gWhite;
			HMI.pg330_ExtModeIndex  = 2;
			HMI.pg330_ExtModeColor  = gBlue;
		}
		else if ((EXT_HO_OPER_MODE == AUTO_MODE))
		{
			HMI.ExtStopLedColor = gWhite;
			HMI.ExtManualLedColor = gWhite;
			HMI.ExtRunLedColor = gGreen;
			HMI.pg330_ExtModeIndex  = 3;
			HMI.pg330_ExtModeColor  = gGreen;
		}
	}
}
#if 0
/************************************************************************/
/*
   Function:  IO_Semaphore_init()                                       ;

   Description:  The following routine initializes
            the main page as well as the monitor page.
   These vars are declared in mem.h for use with this
   _GLOBAL UINT  sm_FILEIO_C_status
   _GLOBAL UINT  sm_FILEIO_I_status
   _GLOBAL UINT  sm_FILEIO_R_status
   _GLOBAL UINT  sm_FILEIO_A_status
   _GLOBAL UINT  sm_FILEIO_D_status
   _GLOBAL UDINT sm_FILEIO_ident

                                                                        */
/************************************************************************/
void IO_Semaphore_init(void)
{
	/*get id-No.*/
	sm_FILEIO_I_status = SM_ident("sm_FILEIO", &sm_FILEIO_ident);
	/*if this semaphor does not extist=>*/
	if (sm_FILEIO_I_status == 3336)
		/*create Semaphor*/
		sm_FILEIO_C_status = SM_create("sm_FILEIO", 1, &sm_FILEIO_ident);
}
#endif

/************************************************************************/
/*
   Function:  bValidJump(UINT)                                       ;

   Description:  This function is called when the shortcut button has been 
   used to jump to a page number.
   The page number to jump to is the parameter.
   The function checks if the requested page is available for showing based
   on the current security level set in the pannel.
   It return TRUE if the page should be shown and FALSE otherwise
                   														*/
/************************************************************************/
BOOL bValidJump(UINT u16JumpPage)
{
	config_super *cs = &cfg_super;
	//USINT i;
	
	/* if jump to Manual page, first reset all buttons and the ack screen */
	if (u16JumpPage == pg005_BlendManualBackup1)
	{
		/* init the page items and allow jump */
		HMI.pg005_manual_GateControl = 12;
		Interlock_Open_Ack = FALSE;
		HMI.Action_BlendMode = 0;
		BwhCommand.CommandPresent = TRUE;
		BwhCommand.Command = MAN_MODE;
		buttons_manual_reset();					// G2-337 4/19/17 Dragan
		return TRUE;
	}

	/* is jump to twTable allowed? yes DF*/
	//if ((u16JumpPage == pg411_Diag_TimeWt) && (!Signal.display_timewt)) 
   //   return FALSE;
		
	/* no jump to License page */
	if (u16JumpPage == pg007_LicenseKey) 
      return FALSE;
	
	/* no jump to error page */
	if (u16JumpPage == pg008_PanelGotOld)
      return FALSE;
	
	/* no jump to monitor pages without going through Main Menu hopper_visibility()*/
	if (u16JumpPage == pg334_Monitor_Hop_AtoH) 
      return FALSE;
	
	/* no jump to InHouseTestPage if test is not enabled */
#ifndef TEST_BUTTONS
	if (u16JumpPage == pg999_In_House_Testing) 
		return FALSE;
#endif	
	
	/* is jump to extruder control setup or totals page allowed? */
	if ((cs->wtp_mode == UNUSED) && ((u16JumpPage == pg176_Setup_System_Config1) || (u16JumpPage == pg324_Totals_ExtNip) || (u16JumpPage == pg009_ExtManual))) 
      return FALSE;

#if 0 // caylen - adding extrusion control
	/* make extruder setup pages unavailable since there is no extruder control in the system */
	/* !!!!!!!!!!!!!!!!!!!!*/
	/* if you are enabling pages 40 to 52 then you must also fix the navigation inside. */ 
	/* See Bug B&R001-51 for details about what needs to be fixed */
	if ((u16JumpPage >= pg040_Setup_Ext) && (u16JumpPage <= pg052_Setup_Ext_Drive1)) 
      return FALSE;
#endif
	
	/* make haul off setup pages unavailable if lpt_mode is UNUSED */
	if ((cs->wtp_mode == UNUSED) && ((u16JumpPage >= pg080_Setup_HO) && (u16JumpPage <= pg087_Setup_HO_Drive2)))
      return FALSE;

	/* make secondary haul off setup pages unavailable if slpt_mode is UNUSED */
	if ((cs->sltp_mode == UNUSED) && ((u16JumpPage >= pg160_Setup_Winder) && (u16JumpPage <= pg169_Setup_Winder_Drive2)))
      return FALSE;
		
	/* make Gravifluff pages unavailable if HMI.GFLFRuntime is FALSE */
	if ((u16JumpPage >= pg110_Setup_GLoad) && (u16JumpPage <= pg140_Setup_GFluff_Drive)) 
      return FALSE;
		
	/* calibration pages not available by "**" button */
	if ((u16JumpPage == pg351_Calibrate_Edit) || (u16JumpPage == pg352_Calibrate_Weigh)) 
      return FALSE;
	
	/* page 400 is empty so no jump allowed*/
	if (u16JumpPage == pg400_Diag1) 
      return FALSE;
		
	/* if in AUTO_MODE some more featurs must be checked */
	if (BLN_OPER_MODE == AUTO_MODE)
	{
		if (u16JumpPage == pg001_ChangeLanguage) 
         return FALSE;
		if (u16JumpPage == pg200_Setup_System_Time)
         return FALSE;
		if (u16JumpPage == pg225_Setup_System_Backup) 
         return FALSE;
		if (u16JumpPage == pg226_Setup_System_Restore) 
         return FALSE;		
		if (u16JumpPage == pg350_Calibrate) 
         return FALSE;
	}
	/* if reached this far then jump is allowed so return TRUE */
	return TRUE;
}

#ifdef TEST_BUTTONS
void vPg999_InHouseTesting_Main()
{
	switch (u8ButtonsTestState)
	{
		case TEST_NOT_STARTED:
			/* wait for it to start */
			break;
		case OPEN_TEST_PLAN_FILE:
			/* wait for it to start */
			u8ButtonsTestState = TEST_CASE_INIT;
			break;
		case TEST_CASE_INIT:
			/* wait for it to start */
			u8ButtonsTestState = TEST_CASE_ACTION;
			break;
		case TEST_CASE_ACTION:
			/* wait for it to start */
			u8ButtonsTestState = TEST_CASE_READ_OUTPUT;
			break;
		case TEST_CASE_READ_OUTPUT:
			/* wait for it to start */
			u8ButtonsTestState = TEST_CASE_WRITE_RESULT;
			break;
		case TEST_CASE_WRITE_RESULT:
			/* wait for it to start */
			u8ButtonsTestState = NEXT_TEST_CASE;
			break;
		case NEXT_TEST_CASE:
			/* wait for it to start */
			u8ButtonsTestState = CLOSE_TEST_PLAN_FILE;
			break;
		case CLOSE_TEST_PLAN_FILE:
			/* wait for it to start */
			u8ButtonsTestState = TEST_FINISHED;
			break;
		case TEST_FINISHED:
			/* wait for it to start */
			u8ButtonsTestState = OPEN_TEST_PLAN_FILE;
			break;
	}
	/* finished so go back to main page */
}
#endif 

#if (0)
void process_reserve_input_alarms()
{
	if (IO_dig[0].channel[RESV_IN_1] == ON)
		SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
	else
		CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);

	if (IO_dig[0].channel[RESV_IN_2] == ON)
		SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT2_ALARM);
	else
		CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT2_ALARM);

   if (IO_dig[0].channel[RESV_IN_3] == ON)
	   SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT3_ALARM);
   else
	   CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT3_ALARM);
}
#endif

void process_relay()               /* check if purge input button pressed */
{
	config_super *cs = &cfg_super;
	//global_struct *g = &shr_global;	

	if (cs->purge_input)
	{
		//g->purge_pending = TRUE;
		cs->purge_input = 0;
		cfg_super.self_cleaning_flag = TRUE;
	}
}

void extrusion_interface()
{
   config_super *cs = &cfg_super;
   
   cs->scrap = (cs->refeed != NONE);

	if ((CannotRunRecipeSmall_Ack) || (CannotRunRecipeBig_Ack))
	{
		visible(&HMI.pg000_CannotRunRecipe_Big, FALSE);
		visible(&HMI.pg000_CannotRunRecipe_Small, FALSE);
		CannotRunRecipeBig_Ack = 0; 
		CannotRunRecipeSmall_Ack = 0;
		recipe_confirmed = 1;
		run_ext();
	}
	else if ((CannotRunRecipeSmall_Change) || (CannotRunRecipeBig_Change))
	{
		visible(&HMI.pg000_CannotRunRecipe_Big, FALSE);
		visible(&HMI.pg000_CannotRunRecipe_Small, FALSE);
		CannotRunRecipeSmall_Change = 0;
		CannotRunRecipeBig_Change = 0;
		HMI.ChangePage = pg454_Recipe_Entry;
	}
	
	if (internal_loading_flag)
	{
		internal_loading_flag = 0;
		shrEXT.loading = 0;
		IO_dig[0].channel[cfgEXT.load_relay.chan] = 0;
	}
	if (NoRateSpd_Ack)
	{
		HMI.Action_ExtruderMode = gcInActive;
		visible(&HMI.pg000_PopupNoRateSpd, FALSE);
		NoRateSpd_Ack = 0;
	}
		
	if (density_may_changed)	    /* density changed to cause recal extrusion recipe etc */
	{
		run_ext();
		density_may_changed = FALSE; 
	}
	 
	if (cs->wtp_mode == MONITOR)
	{   
		LiwCommand[0].CommandPresent = TRUE;
		LiwCommand[0].Command = MONITOR_MODE;
	}
   
	if (cs->ltp_mode == MONITOR)
	{
		HOCommand.CommandPresent = TRUE;
		HOCommand.Command = MONITOR_MODE;
	}
}

void hopper_visibility_update()
{
	config_super *cs = &cfg_super;	
	
	if (cs->total_gates >= 2)
		visible(&HMI.HopBRuntime, TRUE);
	else 
		visible(&HMI.HopBRuntime, FALSE);

	if (cs->total_gates >= 3)
		visible(&HMI.HopCRuntime, TRUE);
	else 
		visible(&HMI.HopCRuntime, FALSE);

	if (cs->total_gates >= 4)
		visible(&HMI.HopDRuntime, TRUE);
	else 
		visible(&HMI.HopDRuntime, FALSE);

	if (cs->total_gates >= 5)
		visible(&HMI.HopERuntime, TRUE);
	else 
		visible(&HMI.HopERuntime, FALSE);

	if (cs->total_gates >= 6)
		visible(&HMI.HopFRuntime, TRUE);
	else 
		visible(&HMI.HopFRuntime, FALSE);

	if (cs->total_gates >= 7)
		visible(&HMI.HopGRuntime, TRUE);
	else 
		visible(&HMI.HopGRuntime, FALSE);

	if (cs->total_gates >= 8)
		visible(&HMI.HopHRuntime, TRUE);
	else 
		visible(&HMI.HopHRuntime,FALSE);

	if (cs->total_gates >= 9)
		visible(&HMI.HopIRuntime, TRUE);
	else 
		visible(&HMI.HopIRuntime, FALSE);

	if (cs->total_gates >= 10)
		visible(&HMI.HopJRuntime, TRUE);
	else 
		visible(&HMI.HopJRuntime, FALSE);

	if (cs->total_gates >= 11)
		visible(&HMI.HopKRuntime, TRUE);
	else 
		visible(&HMI.HopKRuntime, FALSE);

	if (cs->total_gates >= 12)
		visible(&HMI.HopLRuntime, TRUE);
	else 
		visible(&HMI.HopLRuntime, FALSE);
} 

/************************************************************************/
/*
   Function:  SET_ALARM()

   Description:  This function will set the appropriate alarm bits in the
      correct device alarm word based on parameters sent in.
                                                                        */
/************************************************************************/
void SET_ALARM(int Device, unsigned int Alarm)
{
	switch (Device)
	{
		case TYPE_SYSTEM:
			shr_global.alarm_bits       |= ((Alarm)|shr_global.alarm_force_bits);
			shr_global.alarm_latch_bits |= ((Alarm)|shr_global.alarm_force_bits);
			break;
	}
}

/************************************************************************/
/*
   Function:  CLEAR_ALARM()

   Description:  This function will clear the alarm defined by the device
         and alarm bit passed in.
                                                                        */
/************************************************************************/
void CLEAR_ALARM(int Device, unsigned int Alarm)
{
	switch (Device)
	{
		case TYPE_SYSTEM:
			shr_global.alarm_bits &= ((~(Alarm))|shr_global.alarm_force_bits);
			break;
	}
}

/************************************************************************/
/*
   Function:  manual_backup_keyswitch_check()

   Description:  This function will check if Manual Backup Keyswitch 
						  is active in Pause_Mode, Manual_Mode and Auto_Mode.
                                                                        */
/************************************************************************/
void manual_backup_keyswitch_check()
{
	int i;

	//if ((debugFlags.dan) && (BLN_OPER_MODE == AUTO_MODE))
	if ((IO_dig[0].channel[KEY_SWITCH_IN] == GateKeyswitchON) 
		&& (cfg_super.demo == FALSE) && (BLN_OPER_MODE == AUTO_MODE))
	{
		keyswitch_manual_mode_flag = FALSE;
	}
	//if ((debugFlags.dan) && (BLN_OPER_MODE != AUTO_MODE)  && (keyswitch_manual_mode_flag))
	if ((IO_dig[0].channel[KEY_SWITCH_IN] == GateKeyswitchON) 
		&& (keyswitch_manual_mode_flag) && (cfg_super.demo == FALSE) && (BLN_OPER_MODE != AUTO_MODE))
	{
		if ((BLN_OPER_MODE != MAN_MODE) && (keyswitch_pause_mode_flag))
		{	
			visible(&HMI.pg000_PopupGateSwitchRuntime, TRUE);
			if (HMI.pg000_PopupGateSwitch_ACK)
			{
				keyswitch_pause_mode_flag = FALSE;
				HMI.pg000_PopupGateSwitch_ACK = 0;
				visible(&HMI.pg000_PopupGateSwitchRuntime, FALSE);
			}
		}		
		if (BLN_OPER_MODE == MAN_MODE)
		{
         visible(&HMI.pg005_PopupMixerGateRuntime, FALSE);
         // if it is not visible make sure it is not active
         hmi_pg005_controls[12] = 0; //G2-456
         hmi_pg005_controls[13] = 0; //G2-456
         hmi_pg005_controls[14] = 0; //G2-456
         close_all_gates_GTF();      //G2-456
         for (i=0; i<12; i++) 
         {
            hmi_pg005_controls[i] = 0; //G2-456
         }
			for (i=0; i<MAX_GATES; i++)
			{
				visible(&HMI.pg005_PopupGateSwitchRuntime[i], FALSE);
			}
		}
	}
	else
	{
		keyswitch_pause_mode_flag = TRUE;
		visible(&HMI.pg000_PopupGateSwitchRuntime, FALSE);
		visible(&HMI.pg005_PopupMixerGateRuntime, TRUE);
		for (i=0; i<MAX_GATES; i++)
		{
			visible(&HMI.pg005_PopupGateSwitchRuntime[i], TRUE);
		}
	}
}

/************************************************************************/
/*
   Function:  clear_manual_backup_PopUp();

   Description: This function will clear the Manual Backup Pop Up Keyswitch 
                                                                        */
/************************************************************************/
void clear_manual_backup_PopUp()
{
	if ((IO_dig[0].channel[KEY_SWITCH_IN] != GateKeyswitchON) && (cfg_super.demo == FALSE))
	{
		keyswitch_manual_mode_flag = TRUE;
	}
}

void run_ext_remote()
{
   config_super *cs = &cfg_super;
	//char in_remote;
	//char ho_decel = TRUE;                 /* flag for ho is decelerating  */
	//float max_eta;                        /* eta for system ramp          */

   // shrHO.set_ltp  = BATCH_RECIPE_NOW->ltp;            /* fill in for udata    */
   // shrEXT.set_wtp = BATCH_RECIPE_NOW->ext_wtp;        /* fill in for udata    */

  	/* send out auto messages to LIW's & HO                               */

   if (cs->ltp_mode == CONTROL)
	{
		HOCommand.CommandPresent = TRUE;
		HOCommand.Command = AUTO_MODE;
		HOCommand.SetLTP = BATCH_RECIPE_EDIT.ltp;
	   /*	HOCommand.SetAccelDecel = gHO_ad;  */
	}
	
	if (cs->wtp_mode == CONTROL)
	{
	   LiwCommand[0].CommandPresent = TRUE;
		LiwCommand[0].Command = AUTO_MODE;
		LiwCommand[0].SetWTP = BATCH_RECIPE_EDIT.total_wtp;
		/* LiwCommand[0].SetAccelDecel = gLIW_ad[EXT_LOOP]; */
	}
  
	if ((cfg_super.wtp_mode == CONTROL) || (cfg_super.ltp_mode == CONTROL))
		EXT_HO_OPER_MODE = AUTO_MODE;
	
	BATCH_RECIPE_NOW->ltp     = BATCH_RECIPE_EDIT.ltp;  
	BATCH_RECIPE_NOW->ext_wtp = BATCH_RECIPE_EDIT.total_wtp;  
}
// G2-337 4/19/17 added
/********************************************************************************
	Dragan
   Function:  page_completion();

   Description: This function runs code at leaving a page
   Note: At the moment handles leaving pages 1 and 5, but code for other may be
         added. 

*********************************************************************************/
void page_completion(unsigned int ui_Page)
{
   switch(ui_Page)
   {	
		case pg000_MainPage :			/* Main, page 1 */
			break; 

		case pg005_BlendManualBackup1 : /* Manual Backup Blender, page 5*/
			buttons_manual_reset();
			io_reset();
         if (shr_global.purge_pending || cfg_super.self_cleaning_flag)
         {
            shrBWH.oper_mode   = PAUSE_MODE;
		      shrMIXER.oper_mode = PAUSE_MODE; 
			   IO_dig[X20].channel[cfg_super.purge_valve_3.chan] = OFF;
			   if (cfg_super.remote_purge)
			   {
			 	   IO_dig[X20].channel[cfg_super.remote_purge_start_relay.chan]    = OFF;
				   IO_dig[X20].channel[cfg_super.remote_purge_complete_relay.chan] = OFF;
			   }
			   else
			   {
				   IO_dig[X20].channel[cfg_super.purge_valve_1.chan] = OFF;
				   IO_dig[X20].channel[cfg_super.purge_valve_2.chan] = OFF;
			   }
			   shr_global.purge_pending     = FALSE;
		      cfg_super.self_cleaning_flag = FALSE;
         }
			break; 		
	}
}

// G2-337 4/19/17
/********************************************************************************
	Dragan
   Function:  buttons_manual_reset();

   Description: Resets buttons on pg005_BlendManualBackup1

*********************************************************************************/
void buttons_manual_reset()
{
	short i;

	for (i=0; i<MANUAL_DEVICES_NO; i++) 
      hmi_pg005_controls[i] = 0;
}

// G2-337 4/19/17 added
/********************************************************************************
	Dragan
   Function:  io_reset();

   Description: Resets IOs when pg005_BlendManualBackup1 is left

*********************************************************************************/
void io_reset()
{
	short i;

	for (i=0; i<cfg_super.total_gates; i++)
	{
		IO_dig[X20].channel[cfgGATE[i].primary_gate.chan] = OFF;
	}
	if (hmi_pg005_controls[12])
		IO_dig[0].channel[MIXER_MOTOR_OUT] = ON;
	else
		IO_dig[0].channel[MIXER_MOTOR_OUT] = OFF;
	if (hmi_pg005_controls[13])
		IO_dig[0].channel[MIXER_GATE_OUT] = ON;
	else
		if (cfgMIXER.mixer_motor_mode != NO_MIXER) //G2-333
		   IO_dig[0].channel[MIXER_GATE_OUT] = OFF;
	if (hmi_pg005_controls[14])
		IO_dig[0].channel[WEIGH_HOPPER_DUMP_OUT] = ON;
	else
		IO_dig[0].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
	if (hmi_pg005_controls[15])
	{
		IO_dig[0].channel[ALARM_OUT] = ON;
		if (GravitrolHopperAlone)
			IO_dig[0].channel[ALARM_OUT_GRAV] = ON;
	}
	else
	   IO_dig[0].channel[ALARM_OUT] = OFF;
}

// G2-337 4/19/17 added 
/********************************************************************************
		Dragan
   Function:   close_all_gates_GTF();

   Description: 
*********************************************************************************/
void close_all_gates_GTF()
{
	short i;

	for (i=0; i<cfg_super.total_gates; i++)
	{
		IO_dig[X20].channel[cfgGATE[i].primary_gate.chan] = OFF;
		if (cfgGATE[i].secondary_gate_enabled == TRUE) /* if secondary enabled, close it GTF */
			IO_dig[X20].channel[cfgGATE[i].secondary_gate.chan] = OFF;
	}
}

void process_Estop_alarm() //G2-662
{
   // for Armacell estop alarm G2-662
   if (!cfg_super.pg990_HopperDoorSwitch) 
	{
      if (cfg_super.pg2180_aux5_invert == 0)
      {
         if (IO_dig[0].channel[RESV_IN_5] == ON)
         {
            SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
            //force blender into stop
            BLN_OPER_MODE &= ~MAN_MODE;
            remote_pause_write_flag = TRUE;
         }
   	   else
   		   CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
      }
      else
      {
         if (IO_dig[0].channel[RESV_IN_5] == OFF)
         {
            SET_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
            //force blender into stop
            BLN_OPER_MODE &= ~MAN_MODE;
            remote_pause_write_flag = TRUE;
         }
   	   else
   		   CLEAR_ALARM(TYPE_SYSTEM, RESERVE_INPUT1_ALARM);
      }
   }
}

