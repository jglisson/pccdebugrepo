
#ifndef READSUB_H
#define READSUB_H

extern int rv_set_manual(fic *v,point_data *pd, int *type);
extern int rv_config(fic *v,point_data *pd, int *type);
extern int rv_num_hop(fic *v,point_data *pd, int *type);
extern int rv_alternate_set_addr(fic *v,point_data *pd, int *type);
extern int rv_set_mode(fic *v,point_data *pd, int *type);
extern int rv_current_mode(fic *v,point_data *pd, int *type);
extern int rv_status(fic *v,point_data *pd, int *type);
extern int rv_current_alarm(fic *v,point_data *pd, int *type);
extern int rv_latched_alarm(fic *v,point_data *pd, int *type);
extern int rv_next_recipe(fic *v,point_data *pd, int *type);
extern int rv_set_parts(fic *v,point_data *pd, int *type);
extern int rv_std_dev(fic *v,point_data *pd, int *type);
extern int rv_temp_set_parts(fic *v,point_data *pd, int *type);
extern int rv_act_parts(fic *v,point_data *pd, int *type);
extern int rv_loading_controlled_remotely(fic *,point_data *, int *);
extern int rv_loadin_dig_io(fic *,point_data *, int *); /* BMM 1 */
extern int rv_loadout_dig_io(fic *,point_data *, int *); /* BMM 1 */
extern int rv_pump_aux_input(fic *,point_data *, int *); /* BMM 1 */
extern int rv_set_wtp(fic *v,point_data *pd, int *type);
extern int rv_temp_set_wtp(fic *v,point_data *pd, int *type);
extern int rv_act_wtp(fic *v,point_data *pd, int *type);
extern int rv_set_ltp(fic *v,point_data *pd, int *type);
extern int rv_temp_set_ltp(fic *v,point_data *pd, int *type);
extern int rv_act_ltp(fic *v,point_data *pd, int *type);
extern int rv_set_thk(fic *v,point_data *pd, int *type);
extern int rv_temp_set_thk(fic *v,point_data *pd, int *type);
extern int rv_act_thk(fic *v,point_data *pd, int *type);
extern int rv_set_wpl(fic *v,point_data *pd, int *type);
extern int rv_temp_set_wpl(fic *v,point_data *pd, int *type);
extern int rv_act_wpl(fic *v,point_data *pd, int *type);
extern int rv_set_od(fic *v,point_data *pd, int *type);
extern int rv_act_od(fic *v,point_data *pd, int *type);
extern int rv_set_id(fic *v,point_data *pd, int *type);
extern int rv_act_id(fic *v,point_data *pd, int *type);
extern int rv_set_spd(fic *v,point_data *pd, int *type);
extern int rv_act_spd(fic *v,point_data *pd, int *type);
extern int rv_inven_wt(fic *v,point_data *pd, int *type);
extern int rv_shift_wt(fic *v,point_data *pd, int *type);
extern int rv_inven_len(fic *v,point_data *pd, int *type);
extern int rv_shift_len(fic *v,point_data *pd, int *type);
extern int rv_last_batch_weight(fic *v,point_data *pd, int *type);
extern int rv_last_inven_wt(fic *v,point_data *pd, int *type);
extern int rv_last_shift_wt(fic *v,point_data *pd, int *type);
extern int rv_last_inven_len(fic *v,point_data *pd, int *type);
extern int rv_last_shift_len(fic *v,point_data *pd, int *type);
extern int rv_set_wpa(fic *v,point_data *pd, int *type);
extern int rv_new_set_wpa(fic *v,point_data *pd, int *type);
extern int rv_act_wpa(fic *v,point_data *pd, int *type);
extern int rv_inven_area(fic *v,point_data *pd, int *type);
extern int rv_shift_area(fic *v,point_data *pd, int *type);
extern int rv_hopper_weight(fic *v,point_data *pd, int *type);
extern int rv_man_spd(fic *v,point_data *pd, int *type);
extern int rv_temp_man_spd(fic *v,point_data *pd, int *type);
extern int rv_set_density(fic *v,point_data *pd, int *type);
extern int rv_act_density(fic *v,point_data *pd, int *type);
extern int rv_temp_set_density(fic *v,point_data *pd, int *type);
extern int rv_accel(fic *v,point_data *pd, int *type);
extern int rv_decel(fic *v,point_data *pd, int *type);
extern int rv_set_ratio_spd(fic *v,point_data *pd, int *type);
extern int rv_act_ratio_spd(fic *v,point_data *pd, int *type);
extern int rv_temp_set_od(fic *v,point_data *pd, int *type);
extern int rv_temp_set_id(fic *v,point_data *pd, int *type);
extern int rv_set_width(fic *v,point_data *pd, int *type);
extern int rv_temp_set_width(fic *v,point_data *pd, int *type);
extern int rv_act_width(fic *v,point_data *pd, int *type);
extern int rv_temp_stretch_factor(fic *v,point_data *pd, int *type);
extern int rv_stretch_factor(fic *v,point_data *pd, int *type);
extern int rv_mix_time_left(fic *v, point_data *pd, int *type);
extern int rv_mix_time(fic *v, point_data *pd, int *type);
extern int rv_temp_mix_time(fic *v, point_data *pd, int *type);
extern int rv_sys_batch_wt(fic *v, point_data *pd, int *type);
extern int rv_temp_sys_batch_size(fic *v,point_data *pd, int *type);
extern int rv_sys_batch_size(fic *v,point_data *pd, int *type);
extern int rv_avg_batch_size(fic *v,point_data *pd, int *type);
extern int rv_last_gate_feed_wt(fic *v,point_data *pd, int *type);
extern int rv_avg_batch_time(fic *v,point_data *pd, int *type);
extern int null_read_function(fic *,point_data *,int *);
extern int rv_target_accuracy(fic *,point_data *,int *);
extern int rv_frame_size(fic *,point_data *,int *);
extern int rv_total_gates(fic *,point_data *,int *);
extern int rv_wtp_mode(fic *,point_data *,int *);
extern int rv_ltp_mode(fic *,point_data *,int *);
extern int rv_sltp_mode(fic *,point_data *,int *);
extern int rv_application(fic *,point_data *,int *);
extern int rv_configuration_flags(fic *,point_data *,int *);
extern int rv_minimum_security_level(fic *,point_data *,int *);
extern int rv_hopper_parts_lock_1(fic *,point_data *,int *);
extern int rv_hopper_parts_lock_2(fic *,point_data *,int *);
extern int rv_hopper_parts_lock_3(fic *,point_data *,int *);
extern int rv_hopper_parts_lock_4(fic *,point_data *,int *);
extern int rv_pe_needed_flags(fic *,point_data *,int *);
extern int rv_supervisor_code_1(fic *,point_data *,int *);
extern int rv_supervisor_code_2(fic *,point_data *,int *);
extern int rv_maintenance_code_1(fic *,point_data *,int *);
extern int rv_maintenance_code_2(fic *,point_data *,int *);
extern int rv_operator_code_1(fic *,point_data *,int *);
extern int rv_operator_code_2(fic *,point_data *,int *);
extern int rv_units(fic *,point_data *,int *);
extern int rv_wpl_unit(fic *,point_data *,int *);
extern int rv_long_wpl_unit(fic *,point_data *,int *);
extern int rv_wpa_unit(fic *,point_data *,int *);
extern int rv_long_wpa_unit(fic *,point_data *,int *);
extern int rv_t_unit(fic *,point_data *,int *);
extern int rv_long_t_unit(fic *,point_data *,int *);
extern int rv_od_unit(fic *,point_data *,int *);
extern int rv_long_od_unit(fic *,point_data *,int *);
extern int rv_id_unit(fic *,point_data *,int *);
extern int rv_long_id_unit(fic *,point_data *,int *);
extern int rv_width_unit(fic *,point_data *,int *);
extern int rv_long_width_unit(fic *,point_data *,int *);
extern int rv_device_on_port_1(fic *,point_data *,int *);
extern int rv_device_on_port_3(fic *,point_data *,int *);
extern int rv_print_id(fic *,point_data *,int *);
extern int rv_remote_protocol(fic *,point_data *,int *);
extern int rv_remote_address(fic *,point_data *,int *);
extern int rv_remote_gravilink_address(fic *,point_data *,int *);
extern int rv_dual_port_byte_order(fic *,point_data *,int *);
extern int rv_report_start(fic *,point_data *,int *);
extern int rv_report_interval(fic *,point_data *,int *);
extern int rv_communication_flags(fic *,point_data *,int *);
extern int rv_user_interface_flags(fic *,point_data *,int *);
extern int rv_set_alarm_relay_type(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_1(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_2(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_3(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_4(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_5(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_6(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_7(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_8(fic *,point_data *,int *);
extern int rv_aux_alarm_msg_9(fic *,point_data *,int *);
extern int rv_recipe_entry_mode(fic *,point_data *,int *);
extern int rv_recipe_flags(fic *,point_data *,int *);
extern int rv_date_time_flags(fic *,point_data *,int *);
extern int rv_miscellaneous_flags(fic *,point_data *,int *);
extern int rv_job_name_1(fic *,point_data *,int *);
extern int rv_job_name_2(fic *,point_data *,int *);
extern int rv_job_name_3(fic *,point_data *,int *);
extern int rv_job_name_4(fic *,point_data *,int *);
extern int rv_device_name_1(fic *,point_data *,int *);
extern int rv_device_name_2(fic *,point_data *,int *);
extern int rv_device_name_3(fic *,point_data *,int *);
extern int rv_device_name_4(fic *,point_data *,int *);
extern int rv_out_of_spec(fic *,point_data *,int *);
extern int rv_primary_gate_channel(fic *,point_data *,int *);
extern int rv_hopper_type(fic *,point_data *,int *);
extern int rv_algorithm(fic *,point_data *,int *);
extern int rv_minimum_gate_time(fic *,point_data *,int *);
extern int rv_control_loop_flags(fic *,point_data *,int *);
extern int rv_primary_proxy_channel(fic *,point_data *,int *);
extern int rv_primary_output_channel(fic *,point_data *,int *);
extern int rv_receiver_fill_time(fic *,point_data *,int *);
extern int rv_receiver_dump_time(fic *,point_data *,int *);
extern int rv_set_batch_size(fic *,point_data *,int *);
extern int rv_ad_filter(fic *,point_data *,int *);
extern int rv_max_coast_time(fic *,point_data *,int *);
extern int rv_ad_filter_table(fic *,point_data *,int *);
extern int rv_mixer_setup_flags(fic *,point_data *,int *);
extern int rv_mixer_motor_mode(fic *,point_data *,int *);
extern int rv_mixer_gate_mode(fic *,point_data *,int *);
extern int rv_material_request_source(fic *,point_data *,int *);
extern int rv_dc_pump_relay_chan(fic *,point_data *,int *);
extern int rv_cont_run_relay_chan(fic *,point_data *,int *);
extern int rv_anx_cont_chan(fic *,point_data *,int *);
extern int rv_dust_a_relay_chan(fic *,point_data *,int *);
extern int rv_dust_b_relay_chan(fic *,point_data *,int *);
extern int rv_pump_flags(fic *,point_data *,int *);
extern int rv_pump_clean_time(fic *,point_data *,int *);
extern int rv_pump_continuous_run_time(fic *,point_data *,int *);
extern int rv_pump_restart_time(fic *,point_data *,int *);
extern int rv_pump_start_delay_time(fic *,point_data *,int *);
extern int rv_alarm_wt(fic *,point_data *,int *);
extern int rv_crit_wt(fic *,point_data *,int *);
extern int rv_min_dump_wt(fic *,point_data *,int *);
extern int rv_warning_high_spd(fic *,point_data *,int *);
extern int rv_warning_low_spd(fic *,point_data *,int *);
extern int rv_drive_high_speed_limit(fic *,point_data *,int *);
extern int rv_drive_low_speed_limit(fic *,point_data *,int *);
extern int rv_rs_maxchg(fic *,point_data *,int *);
extern int rv_dead_band(fic *,point_data *,int *);
extern int rv_rate_speed_table_reset(fic *,point_data *,int *);
extern int rv_max_error(fic *,point_data *,int *);
extern int rv_max_bad(fic *,point_data *,int *);
extern int rv_coast_error(fic *,point_data *,int *);
extern int rv_coast_tolerance(fic *,point_data *,int *);
extern int rv_zero_crossing(fic *,point_data *,int *);
extern int rv_max_spd(fic *,point_data *,int *);
extern int rv_spd_factor(fic *,point_data *,int *);
extern int rv_min_wtp(fic *,point_data *,int *);
extern int rv_demo_max_wtp(fic *,point_data *,int *);
extern int rv_load_wt_on(fic *,point_data *,int *);
extern int rv_load_wt_off(fic *,point_data *,int *);
extern int rv_load_time(fic *,point_data *,int *);
extern int rv_drive_mod_address(fic *,point_data *,int *);
extern int rv_weigh_mod_address(fic *,point_data *,int *);
extern int rv_load_relay(fic *,point_data *,int *);
extern int rv_alarm_relay(fic *,point_data *,int *);
extern int rv_drive_fail_error(fic *,point_data *,int *);
extern int rv_max_drive_fail(fic *,point_data *,int *);
extern int rv_movepts(fic *,point_data *,int *);
extern int rv_calctime(fic *,point_data *,int *);
extern int rv_initial_coast(fic *,point_data *,int *);
extern int rv_liw_flags(fic *,point_data *,int *);
extern int rv_min_ltp(fic *,point_data *,int *);
extern int rv_demo_max_ltp(fic *,point_data *,int *);
extern int rv_ho_flags(fic *,point_data *,int *);
extern int rv_GearInputMethod(fic *,point_data *,int *);
extern int rv_alarm_wth_min(fic *,point_data *,int *);
extern int rv_alarm_wth_max(fic *,point_data *,int *);
extern int rv_mode(fic *,point_data *,int *);
extern int rv_wth_type(fic *,point_data *,int *);
extern int rv_width_port_number(fic *,point_data *,int *);
extern int rv_width_address(fic *,point_data *,int *);
extern int rv_recipe_name_1(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_2(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_3(fic * v, point_data * pd, int *type);
extern int rv_recipe_name_4(fic * v, point_data * pd, int *type);
extern int rv_resin_name_1(fic * v, point_data * pd, int *type);
extern int rv_resin_name_2(fic * v, point_data * pd, int *type);
extern int rv_resin_name_3(fic * v, point_data * pd, int *type);
extern int rv_receiver_status(fic * v, point_data * pd, int *type);
extern int rv_pump_status(fic * v, point_data * pd, int *type);
extern int rv_manual_backup_status(fic * v, point_data * pd, int *type);
extern int rv_temp_set_wpa(fic *v,point_data *pd, int *type);
extern int rv_job_wt(fic *v, point_data *pd, int *type);;
extern int rv_job_wt_completed(fic *v, point_data *pd, int *type);
extern int rv_current_recipe_inven(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_inven(fic * v, point_data * pd, int *type);
extern int rv_temp_job_wt(fic *v, point_data *pd, int *type);
extern int rv_clean_pulse_duration(fic *v,point_data *pd, int *type);
extern int rv_clean_pulse_cyletime(fic *v,point_data *pd, int *type);
extern int rv_num_recipe_inven(fic * v, point_data * pd, int *type);
extern int rv_temp_step_size(fic *v, point_data *pd, int *type);
extern int rv_read_dc_ad(fic *v, point_data *pd, int *type); /* BMM 1*/
extern int rv_clean_toppurge_time(fic *v,point_data *pd, int *type);
extern int rv_step_size(fic *v, point_data *pd, int *type);
extern int rv_act_target_weight(fic *v,point_data *pd, int *type);
extern int rv_security_flags(fic * v, point_data * pd, int *type);
extern int rv_number_for_recipe_inven(fic *v,point_data *pd, int *type);
extern int rv_num_recipe_name_1(fic * v, point_data * pd, int *type);
extern int rv_num_recipe_name_2(fic * v, point_data * pd, int *type);
extern int rv_num_recipe_name_3(fic * v, point_data * pd, int *type);
extern int rv_num_recipe_name_4(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_name_1(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_name_2(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_name_3(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_name_4(fic * v, point_data * pd, int *type);
extern int rv_current_recipe_num(fic * v, point_data * pd, int *type);
extern int rv_recipe_errors(fic * v, point_data * pd, int *type);
extern int rv_sub_priority(fic * v, point_data * pd, int *type);
extern int rv_device_priority(fic * v, point_data * pd, int *type);
extern int rv_remote_priority(fic * v, point_data * pd, int *type);
extern int rv_web_server_priority(fic * v, point_data * pd, int *type);
extern int rv_high_priority(fic * v, point_data * pd, int *type);
extern int rv_super_priority(fic * v, point_data * pd, int *type);
extern int rv_debug_who(fic * v, point_data * pd, int *type);
extern int rv_debug_what(fic * v, point_data * pd, int *type);
extern int rv_debug_remote_low_pt(fic * v, point_data * pd, int *type);
extern int rv_debug_remote_high_pt(fic * v, point_data * pd, int *type);
extern int rv_recipe_mode(fic * v, point_data * pd, int *type);
extern int rv_recipe_menu(fic * v, point_data * pd, int *type);
extern int rv_port_1_device_baud_rate(fic * v, point_data * pd, int *type);
extern int rv_port_1_device_parity(fic * v, point_data * pd, int *type);
extern int rv_port_1_device_port_type(fic * v, point_data * pd, int *type);
extern int rv_port_3_device_baud_rate(fic * v, point_data * pd, int *type);
extern int rv_port_3_device_parity(fic * v, point_data * pd, int *type);
extern int rv_port_3_device_port_type(fic * v, point_data * pd, int *type);
extern int rv_ip_address(fic * v, point_data * pd, int *type);
extern int rv_subnet_mask(fic * v, point_data * pd, int *type);
extern int rv_gateway(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_i(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_j(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_k(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_l(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_a(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_b(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_c(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_d(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_e(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_f(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_g(fic * v, point_data * pd, int *type);
extern int rv_pe_hop_h(fic * v, point_data * pd, int *type);
extern int rv_pe_setup(fic * v, point_data * pd, int *type);
extern int rv_pe_change_recip(fic * v, point_data * pd, int *type);
extern int rv_pe_calibrate(fic * v, point_data * pd, int *type);
extern int rv_pe_store_recipe(fic * v, point_data * pd, int *type);
extern int rv_pe_stop(fic * v, point_data * pd, int *type);
extern int rv_pe_run(fic * v, point_data * pd, int *type);
extern int rv_pe_manual(fic * v, point_data * pd, int *type);
extern int rv_pe_clear_shift(fic * v, point_data * pd, int *type);
extern int rv_pe_clear_inven(fic * v, point_data * pd, int *type);
extern int rv_pe_clear_resin(fic * v, point_data * pd, int *type);
extern int rv_pe_clear_recipe(fic * v, point_data * pd, int *type);
extern int rv_pe_man_backup(fic * v, point_data * pd, int *type);
extern int rv_current_security_level(fic * v, point_data * pd, int *type);
extern int rv_current_alarm_num(fic * v, point_data * pd, int *type);
extern int rv_alarm_setting(fic * v, point_data * pd, int *type);
extern int rv_simulation(fic * v, point_data * pd, int *type);
extern int rv_backup_restore_control(fic * v, point_data * pd, int *type);
extern int rv_refeed_mode(fic * v, point_data * pd, int *type);
extern int rv_width_mode(fic * v, point_data * pd, int *type);
extern int rv_spawn_counts(fic * v, point_data * pd, int *type);
extern int rv_process_status(fic * v, point_data * pd, int *type);
extern int rv_last_process_spawned_1(fic * v, point_data * pd, int *type);
extern int rv_last_process_spawned_2(fic * v, point_data * pd, int *type);
extern int rv_last_process_spawned_3(fic * v, point_data * pd, int *type);
extern int rv_last_point_processed(fic * v, point_data * pd, int *type);
extern int rv_mbenet_receives(fic * v, point_data * pd, int *type);
extern int rv_mbenet_writes(fic * v, point_data * pd, int *type);
extern int rv_abtcp_receives(fic * v, point_data * pd, int *type);
extern int rv_abtcp_writes(fic * v, point_data * pd, int *type);
extern int rv_language(fic * v, point_data * pd, int *type);
extern int rv_wtp_unit(fic * v, point_data * pd, int *type);
extern int rv_config_relay_flags(fic * v, point_data * pd, int *type);
extern int rv_config_relay_location(fic * v, point_data * pd, int *type);
extern int rv_remote_stop_out(fic * v, point_data * pd, int *type);
extern int rv_remote_purge_startrelay(fic * v, point_data * pd, int *type);
extern int rv_selfclean_purge1(fic * v, point_data * pd, int *type);
extern int rv_selfclean_purge2(fic * v, point_data * pd, int *type);
extern int rv_selfclean_purge3(fic * v, point_data * pd, int *type);
extern int rv_remote_purge_stoprelay(fic * v, point_data * pd, int *type);
extern int rv_system_flags(fic * v, point_data * pd, int *type);
extern int rv_keep_alive_flag(fic * v, point_data * pd, int *type);
extern int rv_total_receivers(fic * v, point_data * pd, int *type);
extern int rv_mac_address_1(fic * v, point_data * pd, int *type);
extern int rv_mac_address_2(fic * v, point_data * pd, int *type);
extern int rv_serial_number_1(fic * v, point_data * pd, int *type);
extern int rv_serial_number_2(fic * v, point_data * pd, int *type);
extern int rv_sw_version_1(fic * v, point_data * pd, int *type);
extern int rv_sw_version_2(fic * v, point_data * pd, int *type);
extern int rv_sw_version_3(fic * v, point_data * pd, int *type);
extern int rv_sw_version_date_1(fic * v, point_data * pd, int *type);
extern int rv_sw_version_date_2(fic * v, point_data * pd, int *type);
extern int rv_sw_version_date_3(fic * v, point_data * pd, int *type);
extern int rv_fw_version_1(fic * v, point_data * pd, int *type);
extern int rv_fw_version_2(fic * v, point_data * pd, int *type);
extern int rv_fw_version_3(fic * v, point_data * pd, int *type);
extern int rv_number_of_recipes(fic * v, point_data * pd, int *type);
extern int rv_current_uptime(fic * v, point_data * pd, int *type);
extern int rv_control_status(fic * v, point_data * pd, int *type);
extern int rv_wt_per_bit(fic * v, point_data * pd, int *type);
extern int rv_test_wt(fic * v, point_data * pd, int *type);
extern int rv_zerowt_bits(fic * v, point_data * pd, int *type);
extern int rv_testwt_bits(fic * v, point_data * pd, int *type);
extern int rv_raw_wt_bits(fic * v, point_data * pd, int *type);
extern int rv_delta_bits(fic * v, point_data * pd, int *type);
extern int rv_coast(fic * v, point_data * pd, int *type);
extern int rv_dm_good_message(fic * v, point_data * pd, int *type);
extern int rv_dm_bad_message(fic * v, point_data * pd, int *type);
extern int rv_wm_good_message(fic * v, point_data * pd, int *type);
extern int rv_wm_bad_message(fic * v, point_data * pd, int *type);
extern int rv_dm_hardwareid(fic * v, point_data * pd, int *type);
extern int rv_dm_hardwarerev(fic * v, point_data * pd, int *type);
extern int rv_dm_softwarerev(fic * v, point_data * pd, int *type);
extern int rv_wm_hardwareid(fic * v, point_data * pd, int *type);
extern int rv_wm_hardwarerev(fic * v, point_data * pd, int *type);
extern int rv_wm_softwarerev(fic * v, point_data * pd, int *type);
extern int rv_wm_calibrates(fic * v, point_data * pd, int *type);
extern int rv_calibrate_status(fic * v, point_data * pd, int *type);
extern int rv_out_of_spec_limit(fic * v, point_data * pd, int *type);
extern int rv_stretch_update(fic * v, point_data * pd, int *type);
extern int rv_max_speed_change(fic * v, point_data * pd, int *type);
extern int rv_control_dead_band(fic * v, point_data * pd, int *type);
extern int rv_max_error_before_updates(fic * v, point_data * pd, int *type);
extern int rv_percent_error_to_coast(fic * v, point_data * pd, int *type);
extern int rv_recipe_high_speed_alarm(fic * v, point_data * pd, int *type);
extern int rv_recipe_low_speed_alarm(fic * v, point_data * pd, int *type);
extern int rv_zero_crossing_speed(fic * v, point_data * pd, int *type);
extern int rv_nip_circumference(fic * v, point_data * pd, int *type);
extern int rv_pulses_per_rev(fic * v, point_data * pd, int *type);
extern int rv_dm_network_add(fic * v, point_data * pd, int *type);
extern int rv_dm_network_chan(fic * v, point_data * pd, int *type);
extern int rv_ho_gear_num(fic * v, point_data * pd, int *type);
extern int rv_time_between_updates(fic * v, point_data * pd, int *type);
extern int rv_num_max_error_before_updates(fic * v, point_data * pd, int *type);
extern int rv_initial_coast_value(fic * v, point_data * pd, int *type);
extern int rv_drive_fail_tolerance(fic * v, point_data * pd, int *type);
extern int rv_max_drive_fail_before_alarm(fic * v, point_data * pd, int *type);
extern int rv_current_bits(fic * v, point_data * pd, int *type);
extern int rv_pulse_bits(fic * v, point_data * pd, int *type);
extern int rv_drive_fail_count(fic * v, point_data * pd, int *type);
extern int rv_coasts(fic * v, point_data * pd, int *type);
extern int rv_low_alarm_weight(fic * v, point_data * pd, int *type);
extern int rv_critical_weight(fic * v, point_data * pd, int *type);
extern int rv_min_dump_weight(fic * v, point_data * pd, int *type);
extern int rv_loading_time(fic * v, point_data * pd, int *type);
extern int rv_loading_on_weight(fic * v, point_data * pd, int *type);
extern int rv_loading_off_weight(fic * v, point_data * pd, int *type);
extern int rv_avg_dump_size(fic * v, point_data * pd, int *type);
extern int rv_percent_grav(fic * v, point_data * pd, int *type);
extern int rv_hopper_rs_factor(fic * v, point_data * pd, int *type);
extern int rv_hopper_flags(fic * v, point_data * pd, int *type);
extern int rv_wm_network_add(fic * v, point_data * pd, int *type);
extern int rv_wm_network_chan(fic * v, point_data * pd, int *type);
extern int rv_lm_network_add(fic * v, point_data * pd, int *type);
extern int rv_lm_network_chan(fic * v, point_data * pd, int *type);
extern int rv_loads(fic * v, point_data * pd, int *type);
extern int rv_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_batch_size(fic * v, point_data * pd, int *type);
extern int rv_last_max_wtp(fic * v, point_data * pd, int *type);
extern int rv_resolution(fic * v, point_data * pd, int *type);
extern int rv_batch_oversize(fic * v, point_data * pd, int *type);
extern int rv_offset_wt(fic * v, point_data * pd, int *type);
extern int rv_adloop_time_factor(fic * v, point_data * pd, int *type);
extern int rv_min_dump_time(fic * v, point_data * pd, int *type);
extern int rv_mixer_low_level_time(fic * v, point_data * pd, int *type);
extern int rv_mixer_dump_cycle_time(fic * v, point_data * pd, int *type);
extern int rv_max_dump_time(fic * v, point_data * pd, int *type);
extern int rv_total_mix_time(fic * v, point_data * pd, int *type);
extern int rv_mix_elap_time(fic * v, point_data * pd, int *type);
extern int rv_last_batch_time(fic * v, point_data * pd, int *type);
//extern int rv_last_batch_weight(fic * v, point_data * pd, int *type);
extern int rv_gate_delay_time(fic * v, point_data * pd, int *type);
extern int rv_set_wt(fic * v, point_data * pd, int *type);
extern int rv_act_wt(fic * v, point_data * pd, int *type);
extern int rv_curr_gate_time(fic * v, point_data * pd, int *type);
extern int rv_set_tol(fic * v, point_data * pd, int *type);
extern int rv_rem_weight(fic * v, point_data * pd, int *type);
extern int rv_remaining_wt(fic * v, point_data * pd, int *type);
extern int rv_bad_feeds_before_alarm(fic * v, point_data * pd, int *type);
extern int rv_second_output_channel(fic * v, point_data * pd, int *type);
extern int rv_second_gate_channel(fic * v, point_data * pd, int *type);
extern int rv_temp_resin_num(fic * v, point_data * pd, int *type);
extern int rv_resin_num(fic * v, point_data * pd, int *type);
extern int rv_tableresolution(fic * v, point_data * pd, int *type);
extern int rv_recipe_use(fic * v, point_data * pd, int *type);
extern int rv_new_gate_order(fic * v, point_data * pd, int *type);
extern int rv_num_self_recals(fic * v, point_data * pd, int *type);
extern int rv_gate_pulse_number(fic * v, point_data * pd, int *type);
extern int rv_gate_pulse_number(fic * v, point_data * pd, int *type);
extern int rv_gate_flags(fic * v, point_data * pd, int *type);
extern int rv_num_of_feeds(fic * v, point_data * pd, int *type);

extern int rv_current_recipe_inven64(fic * v, point_data * pd, int *type);
extern int rv_next_recipe_inven64(fic * v, point_data * pd, int *type);
extern int rv_inven_wt64(fic *v,point_data *pd, int *type);
extern int rv_last_inven_wt64(fic *v,point_data *pd, int *type);
extern int rv_shift_wt64(fic *v,point_data *pd, int *type);
extern int rv_inven_len64(fic *v,point_data *pd, int *type);
extern int rv_last_inven_len64(fic *v,point_data *pd, int *type);
extern int rv_shift_len64(fic *v,point_data *pd, int *type);
extern int rv_last_shift_len64(fic *v,point_data *pd, int *type);
extern int rv_inven_area64(fic *v,point_data *pd, int *type);
extern int rv_shift_area64(fic *v,point_data *pd, int *type);
extern int rv_num_recipe_inven64(fic * v, point_data * pd, int *type);

int (*fp_read_var_2[4000])(fic *, point_data *, int *)={
/**************** SYSTEM Remote Read Functions ( Offset 0)  *******************************************/
             null_read_function,             /* 0 */
             rv_set_wtp,                     /* 1 */
             rv_act_wtp,                     /* 2 */
             rv_set_density,                 /* 3 */
             rv_act_density,                 /* 4 */
             rv_set_wpl,                     /* 5 */
             rv_act_wpl,                     /* 6 */
             rv_set_thk,                     /* 7 */
             rv_act_thk,                     /* 8 */
             rv_set_wpa,                     /* 9 */
             rv_act_wpa,                     /* 10 */
             null_read_function,             /* 11 */
             null_read_function,             /* 12 */
             null_read_function,             /* 13 */
             null_read_function,             /* 14 */
             null_read_function,             /* 15 */
             null_read_function,             /* 16 */
             rv_set_ratio_spd,               /* 17 */
             rv_act_ratio_spd,               /* 18 */
             null_read_function,             /* 19 */
             null_read_function,             /* 20 */
             null_read_function,             /* 21 */
             null_read_function,             /* 22 */
             null_read_function,             /* 23 */
             null_read_function,             /* 24 */
             rv_shift_wt,                    /* 25 */
             rv_inven_wt,                    /* 26 */
             rv_last_shift_wt,               /* 27 */
             rv_last_inven_wt,               /* 28 */
             null_read_function,             /* 29 */
             rv_temp_set_wtp,                /* 30 */
             rv_temp_set_wpl,                /* 31 */
             rv_temp_set_thk,                /* 32 */
             rv_temp_set_wpa,                /* 33 */
             rv_job_wt,                      /* 34 */
             rv_job_wt_completed,            /* 35 */
             rv_current_recipe_inven,        /* 36 */
             rv_next_recipe_inven,           /* 37 */
             rv_num_recipe_inven,            /* 38 */
             null_read_function,             /* 39 */
             rv_temp_job_wt,                 /* 40 */
             rv_temp_step_size,              /* 41 */
             null_read_function,             /* 42 */
             null_read_function,             /* 43 */
             null_read_function,             /* 44 */
             null_read_function,             /* 45 */
             null_read_function,             /* 46 */
             null_read_function,             /* 47 */
             null_read_function,             /* 48 */
             null_read_function,             /* 49 */
             null_read_function,             /* 50 */
             null_read_function,             /* 51 */
             null_read_function,             /* 52 */
             null_read_function,             /* 53 */
             null_read_function,             /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             rv_clean_pulse_duration,         /* 90  */
             rv_clean_pulse_cyletime,         /* 91  */
             rv_clean_toppurge_time,          /* 92  */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             rv_accel,                        /* 100  */
             rv_decel,                        /* 101  */
             rv_step_size,                    /* 102  */
             rv_act_target_weight,            /* 103  */
             null_read_function,              /* 104  */
             null_read_function,              /* 105  */
             null_read_function,              /* 106  */
             null_read_function,              /* 107  */
             null_read_function,              /* 108  */
             null_read_function,              /* 109  */
             rv_target_accuracy,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             null_read_function,              /* 113 */
             null_read_function,              /* 114 */
             null_read_function,              /* 115  */
             null_read_function,              /* 116  */
             null_read_function,              /* 117  */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             rv_current_recipe_inven64,       /* 200 */
             rv_next_recipe_inven64,          /* 201 */
             rv_inven_wt64,                   /* 202 */
             rv_last_inven_wt64,              /* 203 */
             rv_shift_wt64,                   /* 204 */
             rv_inven_len64,                  /* 205 */
             rv_last_inven_len64,             /* 206 */
             rv_shift_len64,                  /* 207 */
             rv_inven_area64,                 /* 208 */
             rv_shift_area64,                 /* 209 */
             rv_num_recipe_inven64,           /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299  */
             rv_set_mode,                     /* 300  */
             rv_current_mode,                 /* 301  */
             rv_status,                       /* 302  */
             rv_current_alarm,                /* 303  */
             rv_latched_alarm,                /* 304  */
             rv_loading_controlled_remotely,  /* 305 BMM 1 */
             rv_security_flags,               /* 306  */
             rv_pump_aux_input,               /* 307 BMM 1 */
             rv_read_dc_ad,             	    /* 308 BMM 1*/
             rv_number_for_recipe_inven,      /* 309 */
             rv_num_recipe_name_1,            /* 310 */
             rv_num_recipe_name_2,            /* 311 */
             rv_num_recipe_name_3,            /* 312 */
             rv_num_recipe_name_4,            /* 313 */
             rv_next_recipe_name_1,           /* 314  */
             rv_next_recipe_name_2,           /* 315  */
             rv_next_recipe_name_3,           /* 316  */
             rv_next_recipe_name_4,           /* 317  */
             rv_next_recipe,                  /* 318 */
             rv_current_recipe_num,           /* 319 */
             rv_recipe_errors,                /* 320 */
             rv_sub_priority,                 /* 321  */
             rv_device_priority,              /* 322  */
             rv_remote_priority,              /* 323  */
             rv_web_server_priority,          /* 324  */
             rv_high_priority,                /* 325  */
             rv_super_priority,               /* 326  */
             rv_debug_who,                    /* 327  */
             rv_debug_what,                   /* 328  */
             rv_debug_remote_low_pt,          /* 329  */
             rv_debug_remote_high_pt,         /* 330  */
             rv_recipe_mode,                  /* 331 */
             rv_recipe_menu,                  /* 332 */
             rv_keep_alive_flag,              /* 333  */
             null_read_function,              /* 334  */
             rv_port_1_device_baud_rate,      /* 335  */
             rv_port_1_device_parity,         /* 336  */
             rv_port_1_device_port_type,      /* 337  */
             rv_port_3_device_baud_rate,      /* 338  */
             rv_port_3_device_parity,         /* 339  */
             rv_port_3_device_port_type,      /* 340  */
             rv_ip_address,                   /* 341  */
             rv_subnet_mask,                  /* 342  */
             rv_gateway,                      /* 343  */
             rv_set_manual,                   /* 344 */
             null_read_function,              /* 345 */
             rv_pe_hop_i,                     /* 346  */
             rv_pe_hop_j,                     /* 347  */
             rv_pe_hop_k,                     /* 348  */
             rv_pe_hop_l,                     /* 349  */
             rv_minimum_security_level,       /* 350  */
             rv_pe_hop_a,                     /* 351  */
             rv_pe_hop_b,                     /* 352  */
             rv_pe_hop_c,                     /* 353  */
             rv_pe_hop_d,                     /* 354  */
             rv_pe_hop_e,                     /* 355  */
             rv_pe_hop_f,                     /* 356  */
             rv_pe_hop_g,                     /* 357  */
             rv_pe_hop_h,                     /* 358  */
             rv_pe_setup,                     /* 359  */
             rv_pe_calibrate,                 /* 360  */
             rv_pe_change_recip,              /* 361  */
             rv_pe_store_recipe,              /* 362  */
             rv_pe_stop,                      /* 363  */
             rv_pe_run,                       /* 364  */
             rv_pe_manual,                    /* 365  */
             rv_pe_clear_shift,               /* 366  */
             rv_pe_clear_inven,               /* 367  */
             rv_pe_clear_resin,               /* 368  */
             rv_pe_clear_recipe,              /* 369  */
             rv_pe_man_backup,                /* 370  */
             rv_current_security_level,       /* 371  */
             null_read_function,              /* 372 */
             null_read_function,              /* 373  */
             rv_current_alarm_num,            /* 374  */
             rv_alarm_setting,                /* 375  */
             rv_simulation,                   /* 376  */
             rv_backup_restore_control,       /* 377  */
             rv_refeed_mode,                  /* 378 */
             rv_width_mode,                   /* 379 */
             rv_spawn_counts,                 /* 380  */
             rv_process_status,               /* 381  */
             rv_last_process_spawned_1,       /* 382  */
             rv_last_process_spawned_2,       /* 383  */
             rv_last_process_spawned_3,       /* 384  */
             rv_last_point_processed,         /* 385  */
             rv_mbenet_receives,              /* 386  */
             rv_mbenet_writes,                /* 387  */
             rv_abtcp_receives,               /* 388  */
             rv_abtcp_writes,                 /* 389  */
             rv_aux_alarm_msg_1,              /* 390  */
             rv_aux_alarm_msg_2,              /* 391  */
             rv_aux_alarm_msg_3,              /* 392  */
             rv_aux_alarm_msg_4,              /* 393  */
             rv_aux_alarm_msg_5,              /* 394  */
             rv_aux_alarm_msg_6,              /* 395  */
             rv_aux_alarm_msg_7,              /* 396  */
             rv_aux_alarm_msg_8,              /* 397  */
             rv_aux_alarm_msg_9,              /* 398  */
             rv_language,                     /* 399  */
             rv_total_gates,                  /* 400  */
             rv_frame_size,                   /* 401  */
             rv_wtp_mode,                     /* 402  */
             rv_ltp_mode,                     /* 403  */
             rv_sltp_mode,                    /* 404  */
             rv_application,                  /* 405  */
             rv_wtp_unit,                     /* 406  */
             rv_units,                        /* 407  */
             rv_wpl_unit,                     /* 408  */
             rv_long_wpl_unit,                /* 409  */
             rv_wpa_unit,                     /* 410  */
             rv_long_wpa_unit,                /* 411  */
             rv_t_unit,                       /* 412  */
             rv_long_t_unit,                  /* 413  */
             rv_od_unit,                      /* 414  */
             rv_long_od_unit,                 /* 415  */
             rv_id_unit,                      /* 416  */
             rv_long_id_unit,                 /* 417  */
             rv_width_unit,                   /* 418  */
             rv_long_width_unit,              /* 419  */
             rv_device_on_port_1,             /* 420  */
             rv_device_on_port_3,             /* 421  */
             rv_print_id,                     /* 422  */
             rv_remote_protocol,              /* 423  */
             rv_remote_address,               /* 424  */
             rv_remote_gravilink_address,     /* 425  */
             rv_dual_port_byte_order,         /* 426  */
             rv_report_start,                 /* 427  */
             rv_report_interval,              /* 428  */
             null_read_function,              /* 429  */
             rv_config_relay_flags,           /* 430  */
             rv_config_relay_location,        /* 431  */
             rv_remote_stop_out,              /* 432  */
             rv_set_alarm_relay_type,         /* 433 */
             rv_remote_purge_startrelay,      /* 434  */
             rv_selfclean_purge1,             /* 435  */
             rv_selfclean_purge2,             /* 436  */
             rv_remote_purge_stoprelay,       /* 437  */
             rv_selfclean_purge3,             /* 438  */
             null_read_function,              /* 439  */
             null_read_function,              /* 440  */
             null_read_function,              /* 441  */
             rv_system_flags,                 /* 442  */
             rv_pe_needed_flags,              /* 443  */
             rv_communication_flags,          /* 444  */
             rv_user_interface_flags,         /* 445  */
             null_read_function,              /* 446  */
             rv_recipe_entry_mode,            /* 447  */
             rv_recipe_flags,                 /* 448  */
             null_read_function,              /* 449  */
             rv_pump_restart_time,            /* 450  */
             rv_dust_b_relay_chan,            /* 451  */
             rv_pump_start_delay_time,        /* 452 */
             rv_dust_a_relay_chan,            /* 453 */
             rv_pump_clean_time,              /* 454  */
             rv_pump_continuous_run_time,     /* 455  */
             rv_total_receivers,              /* 456  */
             rv_job_name_1,                   /* 457  */
             rv_job_name_2,                   /* 458  */
             rv_job_name_3,                   /* 459  */
             rv_job_name_4,                   /* 460  */
             rv_device_name_1,                /* 461  */
             rv_device_name_2,                /* 462  */
             rv_device_name_3,                /* 463  */
             rv_device_name_4,                /* 464  */
             rv_recipe_name_1,                /* 465  */
             rv_recipe_name_2,                /* 466  */
             rv_recipe_name_3,                /* 467  */
             rv_recipe_name_4,                /* 468  */
             rv_mac_address_1,                /* 469  */
             rv_mac_address_2,                /* 470  */
             rv_serial_number_1,              /* 471  */
             rv_serial_number_2,              /* 472  */
             rv_sw_version_1,                 /* 473  */
             rv_sw_version_2,                 /* 474  */
             rv_sw_version_3,                 /* 475  */
             rv_sw_version_date_1,            /* 476  */
             rv_sw_version_date_2,            /* 477  */
             rv_sw_version_date_3,            /* 478  */
             rv_fw_version_1,                 /* 479  */
             rv_fw_version_2,                 /* 480  */
             rv_fw_version_3,                 /* 481  */
             rv_number_of_recipes,            /* 482  */
             rv_current_uptime,               /* 483  */
             rv_dc_pump_relay_chan,           /* 484  */
             rv_cont_run_relay_chan,          /* 485  */
             rv_anx_cont_chan,                /* 486  */
             rv_pump_flags,                   /* 487  */
             rv_manual_backup_status,         /* 488  */
             rv_pump_status,                  /* 489  */
             rv_supervisor_code_1,            /* 490  */
             rv_supervisor_code_2,            /* 491  */
             rv_maintenance_code_1,           /* 492  */
             rv_maintenance_code_2,           /* 493  */
             rv_operator_code_1,              /* 494  */
             rv_operator_code_2,              /* 495  */
             null_read_function,              /* 496  */
             rv_control_status,               /* 497  */
             null_read_function,              /* 498  */
             null_read_function,              /* 499  */

/**************** GRAVIFLUFF Remote Read Functions ( Offset 1)  ******************************************/

             null_read_function,              /* 0 */
             null_read_function,              /* 1 */
             rv_act_wtp,                      /* 2 */
             null_read_function,              /* 3 */
             null_read_function,              /* 4 */
             null_read_function,              /* 5 */
             null_read_function,              /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             null_read_function,              /* 9 */
             null_read_function,              /* 10 */
             null_read_function,              /* 11 */
             null_read_function,              /* 12 */
             null_read_function,              /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             null_read_function,              /* 17 */
             null_read_function,              /* 18 */
             null_read_function,              /* 19 */
             null_read_function,              /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             rv_hopper_weight,                /* 24 */
             null_read_function,              /* 25 */
             null_read_function,              /* 26 */
             null_read_function,              /* 27 */
             null_read_function,              /* 28 */
             rv_wt_per_bit,                   /* 29 */
             null_read_function,              /* 30 */
             null_read_function,              /* 31 */
             null_read_function,              /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             null_read_function,              /* 100 */
             null_read_function,              /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             null_read_function,              /* 104 */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             rv_test_wt,                      /* 113 */
             null_read_function,              /* 114 */
             null_read_function,              /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             null_read_function,              /* 305 */
             null_read_function,              /* 306 */
             null_read_function,              /* 307 */
             null_read_function,              /* 308 */
             null_read_function,              /* 309 */
             null_read_function,              /* 310 */
             null_read_function,              /* 311 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             null_read_function,              /* 314 */
             null_read_function,              /* 315 */
             null_read_function,              /* 316 */
             null_read_function,              /* 317 */
             null_read_function,              /* 318 */
             null_read_function,              /* 319 */
             null_read_function,              /* 320 */
             null_read_function,              /* 321 */
             null_read_function,              /* 322 */
             null_read_function,              /* 323 */
             null_read_function,              /* 324 */
             null_read_function,              /* 325 */
             null_read_function,              /* 326 */
             rv_zerowt_bits,                  /* 327 */
             rv_testwt_bits,                  /* 328 */
             rv_raw_wt_bits,                  /* 329 */
             null_read_function,              /* 330 */
             rv_delta_bits,                   /* 331 */
             null_read_function,              /* 332 */
             rv_coast,                        /* 333 */
             null_read_function,              /* 334 */
             null_read_function,              /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             null_read_function,              /* 373 */
             null_read_function,              /* 374  */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             rv_dm_good_message,              /* 378 */
             rv_dm_bad_message,               /* 379 */
             rv_wm_good_message,              /* 380 */
             rv_wm_bad_message,               /* 381 */
             rv_dm_hardwareid,                /* 382 */
             rv_dm_hardwarerev,               /* 383 */
             rv_dm_softwarerev,               /* 384 */
             rv_wm_hardwareid,                /* 385 */
             rv_wm_hardwarerev,               /* 386 */
             rv_wm_softwarerev,               /* 387 */
             rv_wm_calibrates,                /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             null_read_function,              /* 400 */
             null_read_function,              /* 401 */
             null_read_function,              /* 402 */
             null_read_function,              /* 403 */
             null_read_function,              /* 404 */
             null_read_function,              /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 */
             null_read_function,              /* 410 */
             null_read_function,              /* 411 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             rv_calibrate_status,             /* 496 */
             null_read_function,              /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/**************** WIDTH Remote Read Functions ( Offset 2)  ******************************************/

             null_read_function,              /* 0 */
             rv_set_width,                    /* 1 */
             rv_act_width,                    /* 2 */
             rv_set_id,                       /* 3 */
             rv_act_id,                       /* 4 */
             rv_set_od,                       /* 5 */
             rv_act_od,                       /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             null_read_function,              /* 9 */
             null_read_function,              /* 10 */
             null_read_function,              /* 11 */
             null_read_function,              /* 12 */
             null_read_function,              /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             null_read_function,              /* 17 */
             null_read_function,              /* 18 */
             null_read_function,              /* 19 */
             null_read_function,              /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             null_read_function,              /* 24 */
             rv_shift_area,                   /* 25 */
             rv_inven_area,                   /* 26 */
             null_read_function,              /* 27 */
             null_read_function,              /* 28 */
             null_read_function,              /* 29 */
             rv_temp_set_width,               /* 30 */
             rv_temp_set_id,                  /* 31 */
             rv_temp_set_od,                  /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             rv_alarm_wth_min,                /* 100 */
             rv_alarm_wth_max,                /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             rv_out_of_spec_limit,            /* 104  */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             null_read_function,              /* 113 */
             null_read_function,              /* 114 */
             null_read_function,              /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             null_read_function,              /* 305 */
             null_read_function,              /* 306 */
             null_read_function,              /* 307 */
             null_read_function,              /* 308 */
             null_read_function,              /* 309 */
             null_read_function,              /* 310 */
             null_read_function,              /* 311 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             null_read_function,              /* 314 */
             null_read_function,              /* 315 */
             null_read_function,              /* 316 */
             null_read_function,              /* 317 */
             null_read_function,              /* 318 */
             null_read_function,              /* 319 */
             null_read_function,              /* 320 */
             null_read_function,              /* 321 */
             null_read_function,              /* 322 */
             null_read_function,              /* 323 */
             null_read_function,              /* 324 */
             null_read_function,              /* 325 */
             null_read_function,              /* 326 */
             null_read_function,              /* 327 */
             null_read_function,              /* 328 */
             null_read_function,              /* 329 */
             null_read_function,              /* 330 */
             null_read_function,              /* 331 */
             null_read_function,              /* 332 */
             null_read_function,              /* 333 */
             null_read_function,              /* 334 */
             null_read_function,              /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             null_read_function,              /* 373 */
             null_read_function,              /* 374  */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             null_read_function,              /* 378 */
             null_read_function,              /* 379 */
             null_read_function,              /* 380 */
             null_read_function,              /* 381 */
             null_read_function,              /* 382 */
             null_read_function,              /* 383 */
             null_read_function,              /* 384 */
             null_read_function,              /* 385 */
             null_read_function,              /* 386 */
             null_read_function,              /* 387 */
             null_read_function,              /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             rv_mode,                         /* 400 */
             rv_wth_type,                     /* 401 */
             rv_width_port_number,            /* 402 */
             rv_width_address,                /* 403 */
             null_read_function,              /* 404 */
             null_read_function,              /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 */
             null_read_function,              /* 410 */
             null_read_function,              /* 411 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             null_read_function,              /* 496 */
             null_read_function,              /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/**************** HO Remote Read Functions ( Offset 3)  ******************************************/

             null_read_function,              /* 0 */
             rv_set_ltp,                      /* 1 */
             rv_act_ltp,                      /* 2 */
             null_read_function,              /* 3 */
             null_read_function,              /* 4 */
             null_read_function,              /* 5 */
             null_read_function,              /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             null_read_function,              /* 9 */
             null_read_function,              /* 10 */
             rv_stretch_factor,               /* 11 */
             null_read_function,              /* 12 */
             rv_man_spd,                      /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             rv_set_ratio_spd,                /* 17 */
             rv_act_ratio_spd,                /* 18 */
             rv_set_spd,                      /* 19 */
             rv_act_spd,                      /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             null_read_function,              /* 24 */
             rv_shift_len,                    /* 25 */
             rv_inven_len,                    /* 26 */
             rv_last_shift_len,               /* 27 */
             rv_last_inven_len,               /* 28 */
             null_read_function,              /* 29 */
             rv_temp_set_ltp,                 /* 30 */
             rv_temp_stretch_factor,          /* 31 */
             rv_temp_man_spd,                 /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             rv_stretch_update,               /* 61 */
             rv_max_speed_change,             /* 62 */
             rv_control_dead_band,            /* 63 */
             rv_max_error_before_updates,     /* 64 */
             rv_percent_error_to_coast,       /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             rv_out_of_spec_limit,            /* 71 */
             rv_drive_high_speed_limit,       /* 72 */
             rv_drive_low_speed_limit,        /* 73 */
             rv_recipe_high_speed_alarm,      /* 74 */
             rv_recipe_low_speed_alarm,       /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             rv_zero_crossing_speed,          /* 81 */
             rv_max_spd,                      /* 82 */
             rv_spd_factor,                   /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             rv_accel,                        /* 100 */
             rv_decel,                        /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             null_read_function,              /* 104  */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             rv_nip_circumference,            /* 111 */
             rv_pulses_per_rev,               /* 112 */
             null_read_function,              /* 113 */
             rv_rate_speed_table_reset,       /* 114 */
             rv_demo_max_wtp,                 /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             rv_min_ltp,                      /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             rv_dm_network_add,               /* 305 */
             rv_dm_network_chan,              /* 306 */
             rv_ho_flags,                     /* 307 */
             null_read_function,              /* 308 */
             null_read_function,              /* 309 */
             rv_ho_gear_num,                  /* 310 */
             null_read_function,              /* 311 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             rv_movepts,                      /* 314 */
             rv_time_between_updates,         /* 315 */
             rv_num_max_error_before_updates, /* 316 */
             rv_coast_tolerance,              /* 317 */
             rv_initial_coast_value,          /* 318 */
             null_read_function,              /* 319 */
             null_read_function,              /* 320 */
             rv_drive_fail_tolerance,         /* 321 */
             rv_max_drive_fail_before_alarm,  /* 322 */
             rv_max_coast_time,               /* 323 */
             null_read_function,              /* 324 */
             null_read_function,              /* 325 */
             null_read_function,              /* 326 */
             null_read_function,              /* 327 */
             null_read_function,              /* 328 */
             null_read_function,              /* 329 */
             rv_current_bits,                 /* 330 */
             rv_delta_bits,                   /* 331 */
             rv_pulse_bits,                   /* 332 */
             rv_coast,                        /* 333 */
             rv_drive_fail_count,             /* 334 */
             rv_coasts,                       /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             rv_recipe_errors,                /* 373 */
             null_read_function,              /* 374  */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             rv_dm_good_message,              /* 378 */
             rv_dm_bad_message,               /* 379 */
             null_read_function,              /* 380 */
             null_read_function,              /* 381 */
             rv_dm_hardwareid,                /* 382 */
             rv_dm_hardwarerev,               /* 383 */
             rv_dm_softwarerev,               /* 384 */
             null_read_function,              /* 385 */
             null_read_function,              /* 386 */
             null_read_function,              /* 387 */
             null_read_function,              /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             null_read_function,              /* 400 */
             null_read_function,              /* 401 */
             null_read_function,              /* 402 */
             null_read_function,              /* 403 */
             null_read_function,              /* 404 */
             null_read_function,              /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 */
             null_read_function,              /* 410 */
             null_read_function,              /* 411 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             null_read_function,              /* 496 */
             rv_control_status,               /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/**************** LIW Remote Read Functions ( Offset 4)  ******************************************/

             null_read_function,              /* 0 */
             rv_set_wtp,                      /* 1 */
             rv_act_wtp,                      /* 2 */
             rv_set_density,                  /* 3 */
             rv_act_density,                  /* 4 */
             null_read_function,              /* 5 */
             null_read_function,              /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             null_read_function,              /* 9  */
             null_read_function,              /* 10 */
             rv_set_parts,                    /* 11 */
             rv_act_parts,                    /* 12 */
             rv_man_spd,                      /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             rv_set_ratio_spd,                /* 17 */
             rv_act_ratio_spd,                /* 18 */
             rv_set_spd,                      /* 19 */
             rv_act_spd,                      /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             rv_hopper_weight,                /* 24 */
             rv_shift_wt,                     /* 25 */
             rv_inven_wt,                     /* 26 */
             rv_last_shift_wt,                /* 27 */
             rv_last_inven_wt,                /* 28 */
             rv_wt_per_bit,                   /* 29 */
             rv_temp_set_parts,               /* 30 */
             rv_temp_set_density,             /* 31 */
             rv_temp_man_spd,                 /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             rv_max_speed_change,             /* 62 */
             rv_control_dead_band,            /* 63 */
             rv_max_error_before_updates,     /* 64 */
             rv_percent_error_to_coast,       /* 65 */
             null_read_function,              /* 66 - bg = rv_rate_speed_table_dead_band */
             rv_rate_speed_table_reset,       /* 67 */
             rv_low_alarm_weight,             /* 68 */
             rv_critical_weight,              /* 69 */
             rv_min_dump_weight,              /* 70 */
             rv_out_of_spec_limit,            /* 71 */
             rv_drive_high_speed_limit,       /* 72 */
             rv_drive_low_speed_limit,        /* 73 */
             rv_recipe_high_speed_alarm,      /* 74 */
             rv_recipe_low_speed_alarm,       /* 75 */
             null_read_function,              /* 76 - bg = rv_max_output_voltage */
             rv_loading_time,                 /* 77 */
             null_read_function,              /* 78 - bg = rv_motor_speed_for_rate_speed */
             rv_loading_on_weight,            /* 79 */
             rv_loading_off_weight,           /* 80 */
             rv_zero_crossing_speed,          /* 81 */
             null_read_function,              /* 82 - bg = rv_rate_speed_table_resolution */
             null_read_function,              /* 83 - bg = rv_agitator_ratio */
             rv_avg_dump_size,                /* 84 */
             null_read_function,              /* 85 - bg = rv_shift_percent */
             null_read_function,              /* 86 - bg = rv_rate_std_dev */
             rv_percent_grav,                 /* 87 */
             rv_hopper_rs_factor,             /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             rv_accel,                        /* 100 */
             rv_decel,                        /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             null_read_function,              /* 104 */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             rv_test_wt,                      /* 113 */
             rv_ad_filter,                    /* 114 */
             rv_demo_max_wtp,                 /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             rv_max_spd,                      /* 121 */
             rv_spd_factor,                   /* 122 */
             rv_min_wtp,                      /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             rv_dm_network_add,               /* 305 */
             rv_dm_network_chan,              /* 306 */
             rv_hopper_flags,                 /* 307 */
             rv_wm_network_add,               /* 308 */
             rv_wm_network_chan,              /* 309 */
             rv_lm_network_add,               /* 310  */
             rv_lm_network_chan,              /* 311  */
             null_read_function,              /* 312- bx = rv_loading_relay_type */
             rv_hopper_type,                  /* 313- bx = rv_hopper_type */
             rv_movepts,                      /* 314 */
             rv_time_between_updates,         /* 315 */
             rv_num_max_error_before_updates, /* 316 */
             rv_coast_tolerance,              /* 317 */
             rv_initial_coast_value,          /* 318 */
             null_read_function,              /* 319 - bx = rv_num_of_dump_before_alarm */
             null_read_function,              /* 320 - bx = rv_max_out_of_spect_before_alarm */
             rv_drive_fail_tolerance,         /* 321 */
             rv_max_drive_fail_before_alarm,  /* 322 */
             rv_max_coast_time,               /* 323 */
             null_read_function,              /* 324 - bx = rv_loading_system */
             rv_receiver_fill_time,           /* 325 - bx = rv_receiver_fill_time */
             null_read_function,              /* 326 - bx = rv_hopper_dump_time */
             rv_zerowt_bits,                  /* 327 */
             rv_testwt_bits,                  /* 328 */
             rv_raw_wt_bits,                  /* 329 */
             rv_current_bits,                 /* 330 */
             rv_delta_bits,                   /* 331 */
             null_read_function,              /* 332 */
             rv_coast,                        /* 333 */
             rv_drive_fail_count,             /* 334 */
             rv_coasts,                       /* 335 */
             rv_loads,                        /* 336 - bx = rv_loads */
             null_read_function,              /* 337 - bx = rv_agitator_address */
             null_read_function,              /* 338 - bx = rv_agitator_duration */
             null_read_function,              /* 339 - bx = rv_agitator_interval */
             null_read_function,              /* 340 - bx = rv_substitute_hopper */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             rv_recipe_errors,                /* 373 */
             null_read_function,              /* 374  */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             rv_dm_good_message,              /* 378 */
             rv_dm_bad_message,               /* 379 */
             rv_wm_good_message,              /* 380 */
             rv_wm_bad_message,               /* 381 */
             rv_dm_hardwareid,                /* 382 */
             rv_dm_hardwarerev,               /* 383 */
             rv_dm_softwarerev,               /* 384 */
             rv_wm_hardwareid,                /* 385 */
             rv_wm_hardwarerev,               /* 386 */
             rv_wm_softwarerev,               /* 387 */
             rv_wm_calibrates,                /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             null_read_function,              /* 400 */
             null_read_function,              /* 401 */
             null_read_function,              /* 402 */
             null_read_function,              /* 403 */
             rv_alarm_relay,                  /* 404 */
             rv_load_relay,                   /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 - bx = rv_resin_name_1 */
             null_read_function,              /* 410 - bx = rv_resin_name_2 */
             null_read_function,              /* 411 - bx = rv_resin_name_3 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             rv_calibrate_status,             /* 496 */
             rv_control_status,               /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/**************** BWH Remote Read Functions ( Offset 5)  ******************************************/

             null_read_function,              /* 0 */
             rv_max_wtp,                      /* 1 */
             rv_act_wtp,                      /* 2 */
             rv_set_density,                  /* 3 */
             rv_act_density,                  /* 4 */
             null_read_function,              /* 5 */
             null_read_function,              /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             null_read_function,              /* 9 */
             null_read_function,              /* 10 */
             null_read_function,              /* 11 */
             null_read_function,              /* 12 */
             null_read_function,              /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             null_read_function,              /* 17 */
             null_read_function,              /* 18 */
             null_read_function,              /* 19 */
             null_read_function,              /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             rv_hopper_weight,                /* 24 */
             rv_batch_size,                   /* 25 */
             rv_shift_wt,                     /* 26 */
             rv_inven_wt,                     /* 27 */
             rv_last_max_wtp,                 /* 28 */
             rv_wt_per_bit,                   /* 29 */
             rv_avg_batch_size,               /* 30 */
             rv_avg_batch_time,               /* 31 */
             rv_resolution,                   /* 32 */
             rv_batch_oversize,               /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             rv_set_batch_size,               /* 100 */
             null_read_function,              /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             null_read_function,              /* 104 */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             rv_test_wt,                      /* 113 */
             null_read_function,              /* 114 */
             null_read_function,              /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             rv_ad_filter,                    /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             null_read_function,              /* 305 */
             null_read_function,              /* 306 */
             null_read_function,              /* 307 */
             null_read_function,              /* 308 */
             null_read_function,              /* 309 */
             null_read_function,              /* 310 */
             null_read_function,              /* 311 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             null_read_function,              /* 314 */
             null_read_function,              /* 315 */
             null_read_function,              /* 316 */
             null_read_function,              /* 317 */
             null_read_function,              /* 318 */
             null_read_function,              /* 319 */
             null_read_function,              /* 320 */
             null_read_function,              /* 321 */
             null_read_function,              /* 322 */
             null_read_function,              /* 323 */
             null_read_function,              /* 324 */
             null_read_function,              /* 325 */
             rv_offset_wt,                    /* 326 */
             rv_zerowt_bits,                  /* 327 */
             rv_testwt_bits,                  /* 328 */
             rv_raw_wt_bits,                  /* 329 */
             null_read_function,              /* 330 */
             null_read_function,              /* 331 */
             null_read_function,              /* 332 */
             null_read_function,              /* 333 */
             null_read_function,              /* 334 */
             null_read_function,              /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             rv_recipe_errors,                /* 373 */
             null_read_function,              /* 374 */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             null_read_function,              /* 378 */
             null_read_function,              /* 379 */
             null_read_function,              /* 380 */
             null_read_function,              /* 381 */
             null_read_function,              /* 382 */
             null_read_function,              /* 383 */
             null_read_function,              /* 384 */
             null_read_function,              /* 385 */
             null_read_function,              /* 386 */
             null_read_function,              /* 387 */
             rv_wm_calibrates,                /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             rv_max_coast_time,               /* 400 */
             rv_ad_filter_table,              /* 401 */
             rv_adloop_time_factor,           /* 402 */
             rv_min_dump_time,                /* 403 */
             null_read_function,              /* 404 */
             null_read_function,              /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 */
             null_read_function,              /* 410 */
             null_read_function,              /* 411 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             rv_calibrate_status,             /* 496 */
             rv_control_status,               /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/*********** MIXER Remote Read Functions ( Offset 6)  ******************************************/

             null_read_function,              /* 0 */
             rv_max_wtp,                      /* 1 */
             null_read_function,              /* 2 */
             rv_mix_time,                     /* 3 */
             rv_mix_time_left,                /* 4 */
             rv_mixer_low_level_time,         /* 5 */
             rv_mixer_dump_cycle_time,        /* 6 */
             rv_max_dump_time,                /* 7 */
             rv_total_mix_time,               /* 8 */
             rv_mix_elap_time,                /* 9 */
             rv_last_batch_time,              /* 10 */
             rv_gate_delay_time,              /* 11 */
             rv_last_batch_weight,            /* 12 */
             null_read_function,              /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             null_read_function,              /* 17 */
             null_read_function,              /* 18 */
             null_read_function,              /* 19 */
             null_read_function,              /* 20 */
             null_read_function,              /* 21 */
             null_read_function,              /* 22 */
             null_read_function,              /* 23 */
             null_read_function,              /* 24 */
             null_read_function,              /* 25 */
             null_read_function,              /* 26 */
             null_read_function,              /* 27 */
             rv_last_max_wtp,                 /* 28 */
             null_read_function,              /* 29 */
             rv_temp_mix_time,                /* 30 */
             null_read_function,              /* 31 */
             null_read_function,              /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             null_read_function,              /* 100 */
             null_read_function,              /* 101 */
             null_read_function,              /* 102 */
             null_read_function,              /* 103 */
             null_read_function,              /* 104 */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             null_read_function,              /* 113 */
             null_read_function,              /* 114 */
             rv_demo_max_wtp,                 /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             null_read_function,              /* 305 */
             null_read_function,              /* 306 */
             rv_mixer_setup_flags,            /* 307 */
             null_read_function,              /* 308 */
             null_read_function,              /* 309 */
             null_read_function,              /* 310 */
             null_read_function,              /* 311 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             null_read_function,              /* 314 */
             null_read_function,              /* 315 */
             null_read_function,              /* 316 */
             null_read_function,              /* 317 */
             null_read_function,              /* 318 */
             null_read_function,              /* 319 */
             null_read_function,              /* 320 */
             null_read_function,              /* 321 */
             null_read_function,              /* 322 */
             null_read_function,              /* 323 */
             null_read_function,              /* 324 */
             null_read_function,              /* 325 */
             null_read_function,              /* 326 */
             null_read_function,              /* 327 */
             null_read_function,              /* 328 */
             null_read_function,              /* 329 */
             null_read_function,              /* 330 */
             null_read_function,              /* 331 */
             null_read_function,              /* 332 */
             null_read_function,              /* 333 */
             null_read_function,              /* 334 */
             null_read_function,              /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             null_read_function,              /* 372 */
             null_read_function,              /* 373 */
             null_read_function,              /* 374  */
             rv_alarm_setting,                /* 375  */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             null_read_function,              /* 378 */
             null_read_function,              /* 379 */
             null_read_function,              /* 380 */
             null_read_function,              /* 381 */
             null_read_function,              /* 382 */
             null_read_function,              /* 383 */
             null_read_function,              /* 384 */
             null_read_function,              /* 385 */
             null_read_function,              /* 386 */
             null_read_function,              /* 387 */
             null_read_function,              /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             null_read_function,              /* 398 */
             null_read_function,              /* 399 */
             null_read_function,              /* 400 */
             rv_mixer_motor_mode,             /* 401 */
             rv_mixer_gate_mode,              /* 402 */
             rv_material_request_source,      /* 403 */
             null_read_function,              /* 404 */
             null_read_function,              /* 405 */
             null_read_function,              /* 406 */
             null_read_function,              /* 407 */
             null_read_function,              /* 408 */
             null_read_function,              /* 409 */
             null_read_function,              /* 410 */
             null_read_function,              /* 411 */
             null_read_function,              /* 412 */
             null_read_function,              /* 413 */
             null_read_function,              /* 414 */
             null_read_function,              /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,              /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             null_read_function,              /* 496 */
             null_read_function,              /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */

/**************** GATE Remote Read Functions ( Offset 7)  ******************************************/

             null_read_function,              /* 0 */
             rv_set_wt,                       /* 1 */
             rv_act_wt,                       /* 2 */
             rv_set_density,                  /* 3 */
             rv_act_density,                  /* 4 */
             null_read_function,              /* 5 */
             null_read_function,              /* 6 */
             null_read_function,              /* 7 */
             null_read_function,              /* 8 */
             rv_std_dev,                      /* 9 */
             null_read_function,              /* 10 */
             rv_set_parts,                    /* 11 */
             rv_act_parts,                    /* 12 */
             null_read_function,              /* 13 */
             null_read_function,              /* 14 */
             null_read_function,              /* 15 */
             null_read_function,              /* 16 */
             null_read_function,              /* 17 */
             null_read_function,              /* 18 */
             null_read_function,              /* 19 */
             null_read_function,              /* 20 */
             rv_curr_gate_time,               /* 21 */
             rv_set_tol,                      /* 22 */
             rv_rem_weight,                   /* 23 */
             rv_last_gate_feed_wt,            /* 24 */
             rv_shift_wt,                     /* 25 */
             rv_inven_wt,                     /* 26 */
             rv_last_shift_wt,                /* 27 */
             rv_last_inven_wt,                /* 28 */
             rv_remaining_wt,                 /* 29 */
             rv_temp_set_parts,               /* 30 */
             rv_temp_set_density,             /* 31 */
             null_read_function,              /* 32 */
             null_read_function,              /* 33 */
             null_read_function,              /* 34 */
             null_read_function,              /* 35 */
             null_read_function,              /* 36 */
             null_read_function,              /* 37 */
             null_read_function,              /* 38 */
             null_read_function,              /* 39 */
             null_read_function,              /* 40 */
             null_read_function,              /* 41 */
             null_read_function,              /* 42 */
             null_read_function,              /* 43 */
             null_read_function,              /* 44 */
             null_read_function,              /* 45 */
             null_read_function,              /* 46 */
             null_read_function,              /* 47 */
             null_read_function,              /* 48 */
             null_read_function,              /* 49 */
             null_read_function,              /* 50 */
             null_read_function,              /* 51 */
             null_read_function,              /* 52 */
             null_read_function,              /* 53 */
             null_read_function,              /* 54 */
             null_read_function,              /* 55 */
             null_read_function,              /* 56 */
             null_read_function,              /* 57 */
             null_read_function,              /* 58 */
             null_read_function,              /* 59 */
             null_read_function,              /* 60 */
             null_read_function,              /* 61 */
             null_read_function,              /* 62 */
             null_read_function,              /* 63 */
             null_read_function,              /* 64 */
             null_read_function,              /* 65 */
             null_read_function,              /* 66 */
             null_read_function,              /* 67 */
             null_read_function,              /* 68 */
             null_read_function,              /* 69 */
             null_read_function,              /* 70 */
             null_read_function,              /* 71 */
             null_read_function,              /* 72 */
             null_read_function,              /* 73 */
             null_read_function,              /* 74 */
             null_read_function,              /* 75 */
             null_read_function,              /* 76 */
             null_read_function,              /* 77 */
             null_read_function,              /* 78 */
             null_read_function,              /* 79 */
             null_read_function,              /* 80 */
             null_read_function,              /* 81 */
             null_read_function,              /* 82 */
             null_read_function,              /* 83 */
             null_read_function,              /* 84 */
             null_read_function,              /* 85 */
             null_read_function,              /* 86 */
             null_read_function,              /* 87 */
             null_read_function,              /* 88 */
             null_read_function,              /* 89 */
             null_read_function,              /* 90 */
             null_read_function,              /* 91 */
             null_read_function,              /* 92 */
             null_read_function,              /* 93 */
             null_read_function,              /* 94 */
             null_read_function,              /* 95 */
             null_read_function,              /* 96 */
             null_read_function,              /* 97 */
             null_read_function,              /* 98 */
             null_read_function,              /* 99 */
             null_read_function,              /* 100 */
             null_read_function,              /* 101 */
             null_read_function,              /* 102 */
             rv_target_accuracy,              /* 103 */
             rv_out_of_spec_limit,            /* 104  */
             null_read_function,              /* 105 */
             null_read_function,              /* 106 */
             null_read_function,              /* 107 */
             null_read_function,              /* 108 */
             null_read_function,              /* 109 */
             null_read_function,              /* 110 */
             null_read_function,              /* 111 */
             null_read_function,              /* 112 */
             null_read_function,              /* 113 */
             null_read_function,              /* 114 */
             null_read_function,              /* 115 */
             null_read_function,              /* 116 */
             null_read_function,              /* 117 */
             null_read_function,              /* 118 */
             null_read_function,              /* 119 */
             null_read_function,              /* 120 */
             null_read_function,              /* 121 */
             null_read_function,              /* 122 */
             null_read_function,              /* 123 */
             null_read_function,              /* 124 */
             null_read_function,              /* 125 */
             null_read_function,              /* 126 */
             null_read_function,              /* 127 */
             null_read_function,              /* 128 */
             null_read_function,              /* 129 */
             null_read_function,              /* 130 */
             null_read_function,              /* 131 */
             null_read_function,              /* 132 */
             null_read_function,              /* 133 */
             null_read_function,              /* 134 */
             null_read_function,              /* 135 */
             null_read_function,              /* 136 */
             null_read_function,              /* 137 */
             null_read_function,              /* 138 */
             null_read_function,              /* 139 */
             null_read_function,              /* 140 */
             null_read_function,              /* 141 */
             null_read_function,              /* 142 */
             null_read_function,              /* 143 */
             null_read_function,              /* 144 */
             null_read_function,              /* 145 */
             null_read_function,              /* 146 */
             null_read_function,              /* 147 */
             null_read_function,              /* 148 */
             null_read_function,              /* 149 */
             rv_status,                       /* 150 */
             rv_current_alarm,                /* 151 */
             rv_latched_alarm,                /* 152 */
             null_read_function,              /* 153 */
             null_read_function,              /* 154 */
             null_read_function,              /* 155 */
             null_read_function,              /* 156 */
             null_read_function,              /* 157 */
             null_read_function,              /* 158 */
             null_read_function,              /* 159 */
             null_read_function,              /* 160 */
             null_read_function,              /* 161 */
             null_read_function,              /* 162 */
             null_read_function,              /* 163 */
             null_read_function,              /* 164 */
             null_read_function,              /* 165 */
             null_read_function,              /* 166 */
             null_read_function,              /* 167 */
             null_read_function,              /* 168 */
             null_read_function,              /* 169 */
             null_read_function,              /* 170 */
             null_read_function,              /* 171 */
             null_read_function,              /* 172 */
             null_read_function,              /* 173 */
             null_read_function,              /* 174 */
             null_read_function,              /* 175 */
             null_read_function,              /* 176 */
             null_read_function,              /* 177 */
             null_read_function,              /* 178 */
             null_read_function,              /* 179 */
             null_read_function,              /* 180 */
             null_read_function,              /* 181 */
             null_read_function,              /* 182 */
             null_read_function,              /* 183 */
             null_read_function,              /* 184 */
             null_read_function,              /* 185 */
             null_read_function,              /* 186 */
             null_read_function,              /* 187 */
             null_read_function,              /* 188 */
             null_read_function,              /* 189 */
             null_read_function,              /* 190 */
             null_read_function,              /* 191 */
             null_read_function,              /* 192 */
             null_read_function,              /* 193 */
             null_read_function,              /* 194 */
             null_read_function,              /* 195 */
             null_read_function,              /* 196 */
             null_read_function,              /* 197 */
             null_read_function,              /* 198 */
             null_read_function,              /* 199 */
             null_read_function,              /* 200 */
             null_read_function,              /* 201 */
             null_read_function,              /* 202 */
             null_read_function,              /* 203 */
             null_read_function,              /* 204 */
             null_read_function,              /* 205 */
             null_read_function,              /* 206 */
             null_read_function,              /* 207 */
             null_read_function,              /* 208 */
             null_read_function,              /* 209 */
             null_read_function,              /* 210 */
             null_read_function,              /* 211 */
             null_read_function,              /* 212 */
             null_read_function,              /* 213 */
             null_read_function,              /* 214 */
             null_read_function,              /* 215 */
             null_read_function,              /* 216 */
             null_read_function,              /* 217 */
             null_read_function,              /* 218 */
             null_read_function,              /* 219 */
             null_read_function,              /* 220 */
             null_read_function,              /* 221 */
             null_read_function,              /* 222 */
             null_read_function,              /* 223 */
             null_read_function,              /* 224 */
             null_read_function,              /* 225 */
             null_read_function,              /* 226 */
             null_read_function,              /* 227 */
             null_read_function,              /* 228 */
             null_read_function,              /* 229 */
             null_read_function,              /* 230 */
             null_read_function,              /* 231 */
             null_read_function,              /* 232 */
             null_read_function,              /* 233 */
             null_read_function,              /* 234 */
             null_read_function,              /* 235 */
             null_read_function,              /* 236 */
             null_read_function,              /* 237 */
             null_read_function,              /* 238 */
             null_read_function,              /* 239 */
             null_read_function,              /* 240 */
             null_read_function,              /* 241 */
             null_read_function,              /* 242 */
             null_read_function,              /* 243 */
             null_read_function,              /* 244 */
             null_read_function,              /* 245 */
             null_read_function,              /* 246 */
             null_read_function,              /* 247 */
             null_read_function,              /* 248 */
             null_read_function,              /* 249 */
             null_read_function,              /* 250 */
             null_read_function,              /* 251 */
             null_read_function,              /* 252 */
             null_read_function,              /* 253 */
             null_read_function,              /* 254 */
             null_read_function,              /* 255 */
             null_read_function,              /* 256 */
             null_read_function,              /* 257 */
             null_read_function,              /* 258 */
             null_read_function,              /* 259 */
             null_read_function,              /* 260 */
             null_read_function,              /* 261 */
             null_read_function,              /* 262 */
             null_read_function,              /* 263 */
             null_read_function,              /* 264 */
             null_read_function,              /* 265 */
             null_read_function,              /* 266 */
             null_read_function,              /* 267 */
             null_read_function,              /* 268 */
             null_read_function,              /* 269 */
             null_read_function,              /* 270 */
             null_read_function,              /* 271 */
             null_read_function,              /* 272 */
             null_read_function,              /* 273 */
             null_read_function,              /* 274 */
             null_read_function,              /* 275 */
             null_read_function,              /* 276 */
             null_read_function,              /* 277 */
             null_read_function,              /* 278 */
             null_read_function,              /* 279 */
             null_read_function,              /* 280 */
             null_read_function,              /* 281 */
             null_read_function,              /* 282 */
             null_read_function,              /* 283 */
             null_read_function,              /* 284 */
             null_read_function,              /* 285 */
             null_read_function,              /* 286 */
             null_read_function,              /* 287 */
             null_read_function,              /* 288 */
             null_read_function,              /* 289 */
             null_read_function,              /* 290 */
             null_read_function,              /* 291 */
             null_read_function,              /* 292 */
             null_read_function,              /* 293 */
             null_read_function,              /* 294 */
             null_read_function,              /* 295 */
             null_read_function,              /* 296 */
             null_read_function,              /* 297 */
             null_read_function,              /* 298 */
             null_read_function,              /* 299 */
             null_read_function,              /* 300 */
             null_read_function,              /* 301 */
             rv_status,                       /* 302 */
             rv_current_alarm,                /* 303 */
             rv_latched_alarm,                /* 304 */
             rv_loadin_dig_io,      		  /* 305 BMM 1 */
             rv_loadout_dig_io,               /* 306 BMM 1 */
             rv_gate_flags,                   /* 307 */
             rv_gate_pulse_number,            /* 308 */
             rv_num_of_feeds,                 /* 309 */
             rv_num_self_recals,              /* 310 */
             rv_pump_aux_input,               /* 311 BMM 1 */
             null_read_function,              /* 312 */
             null_read_function,              /* 313 */
             null_read_function,              /* 314 */
             null_read_function,              /* 315 */
             null_read_function,              /* 316 */
             null_read_function,              /* 317 */
             null_read_function,              /* 318 */
             rv_bad_feeds_before_alarm,       /* 319 */
             null_read_function,              /* 320 */
             null_read_function,              /* 321 */
             null_read_function,              /* 322 */
             null_read_function,              /* 323 */
             null_read_function,              /* 324 */
             rv_receiver_fill_time,           /* 325 */
             rv_receiver_dump_time,           /* 326 */
             null_read_function,              /* 327 */
             null_read_function,              /* 328 */
             null_read_function,              /* 329 */
             null_read_function,              /* 330 */
             null_read_function,              /* 331 */
             rv_receiver_status,              /* 332 */
             null_read_function,              /* 333 */
             null_read_function,              /* 334 */
             null_read_function,              /* 335 */
             null_read_function,              /* 336 */
             null_read_function,              /* 337 */
             null_read_function,              /* 338 */
             null_read_function,              /* 339 */
             null_read_function,              /* 340 */
             null_read_function,              /* 341 */
             null_read_function,              /* 342 */
             null_read_function,              /* 343 */
             null_read_function,              /* 344 */
             null_read_function,              /* 345 */
             null_read_function,              /* 346 */
             null_read_function,              /* 347 */
             null_read_function,              /* 348 */
             null_read_function,              /* 349 */
             null_read_function,              /* 350 */
             null_read_function,              /* 351 */
             null_read_function,              /* 352 */
             null_read_function,              /* 353 */
             null_read_function,              /* 354 */
             null_read_function,              /* 355 */
             null_read_function,              /* 356 */
             null_read_function,              /* 357 */
             null_read_function,              /* 358 */
             null_read_function,              /* 359 */
             null_read_function,              /* 360 */
             null_read_function,              /* 361 */
             null_read_function,              /* 362 */
             null_read_function,              /* 363 */
             null_read_function,              /* 364 */
             null_read_function,              /* 365 */
             null_read_function,              /* 366 */
             null_read_function,              /* 367 */
             null_read_function,              /* 368 */
             null_read_function,              /* 369 */
             null_read_function,              /* 370 */
             null_read_function,              /* 371 */
             rv_new_gate_order,               /* 372 */
             null_read_function,              /* 373 */
             rv_recipe_use,                   /* 374  */
             rv_alarm_setting,                /* 375 */
             rv_simulation,                   /* 376 */
             null_read_function,              /* 377 */
             null_read_function,              /* 378 */
             null_read_function,              /* 379 */
             null_read_function,              /* 380 */
             null_read_function,              /* 381 */
             null_read_function,              /* 382 */
             null_read_function,              /* 383 */
             null_read_function,              /* 384 */
             null_read_function,              /* 385 */
             null_read_function,              /* 386 */
             null_read_function,              /* 387 */
             null_read_function,              /* 388 */
             null_read_function,              /* 389 */
             null_read_function,              /* 390 */
             null_read_function,              /* 391 */
             null_read_function,              /* 392 */
             null_read_function,              /* 393 */
             null_read_function,              /* 394 */
             null_read_function,              /* 395 */
             null_read_function,              /* 396 */
             null_read_function,              /* 397 */
             rv_second_output_channel,        /* 398 */
             rv_second_gate_channel,          /* 399 */
             rv_primary_gate_channel,         /* 400 */
             rv_hopper_type,                  /* 401 */
             rv_algorithm,                    /* 402 */
             rv_minimum_gate_time,            /* 403 */
             rv_tableresolution,              /* 404 */
             rv_primary_proxy_channel,        /* 405 */
             rv_primary_output_channel,       /* 406 */
             null_read_function,              /* 407 */
             rv_resin_num,                    /* 408 */
             rv_resin_name_1,                 /* 409 */
             rv_resin_name_2,                 /* 410 */
             rv_resin_name_3,                 /* 411 */
             null_read_function,              /* 412 - reserved */
             null_read_function,              /* 413 - reserved */
             null_read_function,              /* 414 - reserved */
             rv_temp_resin_num,               /* 415 */
             null_read_function,              /* 416 */
             null_read_function,              /* 417 */
             null_read_function,              /* 418 */
             null_read_function,              /* 419 */
             null_read_function,              /* 420 */
             null_read_function,              /* 421 */
             null_read_function,              /* 422 */
             null_read_function,              /* 423 */
             null_read_function,              /* 424 */
             null_read_function,              /* 425 */
             null_read_function,              /* 426 */
             null_read_function,              /* 427 */
             null_read_function,              /* 428 */
             null_read_function,              /* 429 */
             null_read_function,              /* 430 */
             null_read_function,              /* 431 */
             null_read_function,              /* 432 */
             null_read_function,              /* 433 */
             null_read_function,              /* 434 */
             null_read_function,              /* 435 */
             null_read_function,              /* 436 */
             null_read_function,              /* 437 */
             null_read_function,              /* 438 */
             null_read_function,              /* 439 */
             null_read_function,              /* 440 */
             null_read_function,              /* 441 */
             null_read_function,              /* 442 */
             null_read_function,              /* 443 */
             null_read_function,              /* 444 */
             null_read_function,              /* 445 */
             null_read_function,              /* 446 */
             null_read_function,              /* 447 */
             null_read_function,              /* 448 */
             null_read_function,              /* 449 */
             null_read_function,              /* 450 */
             null_read_function,              /* 451 */
             null_read_function,              /* 452 */
             null_read_function,              /* 453 */
             null_read_function,              /* 454 */
             null_read_function,              /* 455 */
             null_read_function,              /* 456 */
             null_read_function,              /* 457 */
             null_read_function,              /* 458 */
             null_read_function,              /* 459 */
             null_read_function,              /* 460 */
             null_read_function,              /* 461 */
             null_read_function,              /* 462 */
             null_read_function,              /* 463 */
             null_read_function,              /* 464 */
             null_read_function,              /* 465 */
             null_read_function,              /* 466 */
             null_read_function,              /* 467 */
             null_read_function,              /* 468 */
             null_read_function,              /* 469 */
             null_read_function,              /* 470 */
             null_read_function,              /* 471 */
             null_read_function,              /* 472 */
             null_read_function,              /* 473 */
             null_read_function,              /* 474 */
             null_read_function,              /* 475 */
             null_read_function,              /* 476 */
             null_read_function,              /* 477 */
             null_read_function,              /* 478 */
             null_read_function,              /* 479 */
             null_read_function,              /* 480 */
             null_read_function,      		  /* 481 */
             null_read_function,              /* 482 */
             null_read_function,              /* 483 */
             null_read_function,              /* 484 */
             null_read_function,              /* 485 */
             null_read_function,              /* 486 */
             null_read_function,              /* 487 */
             null_read_function,              /* 488 */
             null_read_function,              /* 489 */
             null_read_function,              /* 490 */
             null_read_function,              /* 491 */
             null_read_function,              /* 492 */
             null_read_function,              /* 493 */
             null_read_function,              /* 494 */
             null_read_function,              /* 495 */
             null_read_function,              /* 496 */
             null_read_function,              /* 497 */
             null_read_function,              /* 498 */
             null_read_function,              /* 499 */
};

#endif
