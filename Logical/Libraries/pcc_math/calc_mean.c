#include <stdio.h>
#include "pcc_math.h"


float calc_mean(samples *sample_data)
{
    int length;
    float mean;

    length = sample_data->num_taken;
    if(length > sample_data->length) length = sample_data->length;
    mean = calc_mean_p(sample_data,length);
    return mean;
}


