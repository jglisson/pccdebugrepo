/************************************************************************/
/*
   File:  mem.h

   Description: Defines the global structures used by the entire system.



      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>

#define NUM_FEEDS_MIN 1      /* number minimum gate feed range:  first found on page 14 in visualizaton */
#define NUM_FEEDS_MAX 20     /* number minimum gate feed range:  first found on page 14 in visualizaton */
#define OUT_OF_SPEC_MIN 0    /* minimum out of spec %:  first found on page 14 in visualizaton */
#define OUT_OF_SPEC_MAX 999  /* minimum out of spec %:  first found on page 14 in visualizaton divide by ten to yeild 99.9%*/


