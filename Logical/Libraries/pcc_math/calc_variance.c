#include "pcc_math.h"

float calc_variance(samples *sample_data,float *mean)
{
    int length;
    float var;

    length = sample_data->num_taken;
    if(length > sample_data->length) length = sample_data->length;

    var = calc_variance_p(sample_data,mean,length);
    return var;
}


