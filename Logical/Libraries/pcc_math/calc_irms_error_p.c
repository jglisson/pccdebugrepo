#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_irms_error_p(isamples *error_data, signed short length)
{
    int error;
    float derror;
    int i;
    ordered_ipair *data;
    int idx;

    if(length >= error_data->length) length = error_data->length;
    data = error_data->pts;

    idx = error_data->next - length;
    if(idx < 0)
        idx += error_data->length;

    error = 0;

    for(i=0; i<length; ++i)
    {
        error = error + data[idx].y * data[idx].y;

        if(++idx >= error_data->length)
            idx -= error_data->length;
    }
    if(length > 0)
        derror = (double)error / (double)length;        /* DIVIDE */
    else
        derror = 0.0;

    error = (int)(sqrt(derror)+.5);

    return error;
}


