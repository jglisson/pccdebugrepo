#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  rem_fun.c

   Description: Modbus routines.


      $Log:   F:\Software\BR_Guardian\rem_fun.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.remFun to toggle the debug information in this file
 *
 *    Rev 1.1   May 06 2008 13:18:28   YZS
 * Did some clean-up.
 *
 *
 *    Rev 1.0   Feb 11 2008 10:59:48   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <stdio.h>
//#include <stdlib.h>
#include <string.h>
#include "sys.h"
#include "mem.h"
#include "libbgdbg.h"

unsigned int calc_checksum(unsigned char *start,int len);
unsigned short CalcCrc_16(unsigned char *DataPtr,unsigned short Count, unsigned short Poly);
int pcc_write(int path,unsigned char *bfr,unsigned int cnt);
int pcc_read(int path,unsigned char *bfr,unsigned int cnt);
int pcc_gs_rdy(int path);
void intel_ipwrite(unsigned char *ptr,int value,int size);
unsigned int intel_ipread(unsigned char *ptr,int size);
unsigned int com_ipread(unsigned char *ptr,int size);
void com_ipwrite(register unsigned char *ptr,unsigned int value,int size);
unsigned short FlipBytes(unsigned short Value);

extern struct port_diag_data_str* r_diag;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/************************************************************************/
/* calc_checksum()                                                      */
/************************************************************************/
unsigned int calc_checksum(unsigned char *start,int len)
{
    register int i;
    register unsigned int sum = 0;

    for(i=0;i<len;++i)
    {
        sum += *start++;
    }
    return sum;
}

/*
*****************************************************************************
      Function:   CalcCrc_16
         Input:   address of data buffer and length of data
        Output:   unsigned int Crc_16 - calculated crc value of current MODBUS
                                        RTU message.
   Description:   This function will calcualate the Cyclic Redundancy Check
                  error check bytes (CRC-16) of a MODBUS RTU message using
                  the CRC16 algorithm.
         Notes:

*****************************************************************************
*/
unsigned short CalcCrc_16(unsigned char *DataPtr,unsigned short Count, unsigned short Poly)
{
    register unsigned short Crc = 0xffff;       /* initialize Crc */
    register int i;

    do
    {
        Crc ^= (unsigned short)(*DataPtr++);    /* XOR data byte with CRC */

        i = 8;
        do
        {
            if(Crc & 0x0001)
            {
                Crc >>= 1;              /* shift  reg 1 bit to right (step 3) */
                Crc ^= Poly;            /* exclusive or poly. with reg.  */
            }
            else
            {
                Crc >>= 1;              /* shift  reg 1 bit to right (step 3) */
            }
        } while(--i);

    } while(--Count);
    return FlipBytes(Crc);
}

/******************************************************************************
      Function:   FlipBytes (little endian <-> big endian)
         Input:   16 bit value
        Output:   16 bit value with bytes reversed.
   Description:   This function will flip the bytes of a 16 bit value.

         Notes:

******************************************************************************/
unsigned short FlipBytes(unsigned short Value)
{
unsigned char *cPtr;
unsigned char Temp;

   cPtr = (unsigned char*) &Value;
   Temp = *cPtr;
   *cPtr = *(cPtr + 1);
   *(cPtr + 1) = Temp;
   return Value;
}


/************************************************************************/
/* pcc_write()                                                          */
/************************************************************************/
/* should only be used when writing to the remote port */
int pcc_write(int path,unsigned char *bfr,unsigned int cnt)
{
#if 0
    register int data_cnt;

    data_cnt = write(path,(char *)bfr,cnt);
    r_diag->remote_bytes_written += data_cnt;
    return data_cnt;
#endif
	return 0;
}

/************************************************************************/
/* pcc_read()                                                           */
/************************************************************************/
int pcc_read(int path,unsigned char *bfr,unsigned int cnt)
{
#if 0
    register int data_cnt;

    data_cnt = read(path,(char *)bfr,cnt);
    r_diag->remote_bytes_read += data_cnt;
    return data_cnt;
#endif
	return 0;
}

/************************************************************************/
/* pcc_gs_rdy()                                                         */
/************************************************************************/
int pcc_gs_rdy(int path)
{
#if 0
      return(r_diag->bytes_in_bfr = _gs_rdy(path));
#endif
	return 0;
}

/************************************************************************/
/*  Function:  intel_ipwrite(&, value, size)                            */
/*                                                                      */
/* Description:  write size bytes from value into packet                */
/*              needs to be packed least significant byte first         */
/************************************************************************/
void intel_ipwrite(unsigned char *ptr,int value,int size)
{
    register unsigned char *cptr;
    register int i=size;

    cptr = (unsigned char *)(&value)+sizeof(int)-1;

    while(i-- > 0)
    {
        *(ptr++) = *(cptr--);
    }
}

/************************************************************************/
/*  Function:  intel_ipread(&, size)                                    */
/*                                                                      */
/* Description:  reads size bytes from packet & returns them as an int  */
/*              the values will be packed least significant byte first  */
/************************************************************************/
unsigned int intel_ipread(unsigned char *ptr,int size)
{
    register unsigned char *cptr;
    register int i=size;
    unsigned int ret = 0;

    cptr = (unsigned char *)(&ret) + sizeof(int) - 1;

    while(i-- > 0)
    {
        *(cptr--) = *(ptr++);
    }

    return(ret);
}

/************************************************************************/
/*  Function:  com_ipread(&, size)                                      */
/*                                                                      */
/* Description:  reads size bytes from packet & returns them as an int  */
/************************************************************************/
unsigned int com_ipread(unsigned char *ptr,int size)
{
    register char *cptr;
    register int i=size;
    unsigned int ret = 0;

    cptr = (char *)(&ret) + sizeof(int) - 1;
    ptr += size-1;

    while(i-- > 0)
    {
        *(cptr--) = *(ptr--);
    }

    return(ret);
}

/************************************************************************/
/*  Function:  com_ipwrite(&, value, size)                              */
/*                                                                      */
/* Description:  write size bytes from value into packet                */
/************************************************************************/
void com_ipwrite(unsigned char *ptr,unsigned int value,int size)
{
    register unsigned char *cptr;
    register int i=size;

    cptr = (unsigned char *)(&value)+sizeof(int)-1;
    ptr += size-1;
    while(i-- > 0)
    {
        *(ptr--) = *(cptr--);
    }
}


