#include <bur/plc.h>

_GLOBAL float WeightDeviationThreshold[12];
_GLOBAL int   MaxAllowedWeightDeviations[12];

struct wt_dev_struct
{
	float mavg_wt_deviation[15];
};

_GLOBAL struct wt_dev_struct wt_dev[12];
_GLOBAL unsigned int wt_dev_mavg_count;  

#define FINISH_BATCH_START        0
#define FINISH_BATCH_WAITING_FULL 1
#define FINISH_BATCH_DUMPING      2
#define FINISH_BATCH_CLOSING      3
#define FINISH_BATCH_CHECK_WT     4
#define FINISH_BATCH_JIGGLING     5
#define FINISH_BATCH_CALC_STATS   6 
#define FINISH_BATCH_DONE         7

typedef struct  
{
   float pcnt[12]; /* value 12 needs to parallel MAX_GATES */
   float wt[12];
} batch_hist_str;

_GLOBAL batch_hist_str batch_data[12];
