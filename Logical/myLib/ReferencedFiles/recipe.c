#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/*************************************************************************/
/*
   File:  recipe.c
   Process Control Corp,   Atlanta, GA
*/
/************************************************************************/
#include <bur/plctypes.h>
#include <asstring.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "vars.h"
#include "signal_pcc.h"
#include "file_rw.h"
#include "alarm.h"
#include "plc.h"
#include "libbgdbg.h"
#include "recipe.h"

#define RecipeModeSave 0
#define RecipeModeNew  1
#define RecipeParts    0
#define RecipeDensity  1
#define GateOrder      2
#define RecipeResin    3
#define CalFeedRate    4
#define MAX_RECIPE_VALUES MAX_LOOP+2

void Recipe_Init();
void Recipe_Main();
void input_bln_parts(recipe_struct *);
void Calc_txt_RecipeEntry1();
void Calc_txt_RecipeEntry2();
void Calc_txt_RecipeEntry3();

void Display_HopperRequestLayer(int);
void RunBlender(void);

void change_recipe(recipe_struct *);
void input_ext_parts(recipe_struct * );
void input_bln_recipe(recipe_struct *);
void input_bln_recipe_w_codes(recipe_struct *);
void input_bln_recipe_no_codes(recipe_struct *);
void input_bln_parts_molding(recipe_struct *);
void Resin_Edit();
void pg453_Hopper_Visibility(void);
void Set_Recipe(recipe_struct *);
void Hoppers_off(void);
void UpDownDataPoint(int *, unsigned char *, unsigned char *);

extern void set_recipe_gate_order(recipe_struct* current_recipe);
extern void visible(unsigned char *Runtime, unsigned char state);
extern void copy_recipe(recipe_struct *to, recipe_struct *from, int what);
void delete_recipe(recipe_struct *target, int what);
extern int validate_recipe(recipe_struct *r, int *validation_errors);
extern void stop_ext(void);
extern void run_ext(void);
extern int Store_Recipes(void);
extern int Restore_Recipes(void);
extern void calc_parts_percent(recipe_struct *);
extern void visible(unsigned char *Runtime, unsigned char state);
extern void set_recipe_gate_order(recipe_struct *);
extern int recipe_num_changed(float, float);
extern int calc_recipe(recipe_struct *r);

extern int gPage;
extern unsigned char gNewMaterial;
extern struct PopupActiveStruct Popup; /* make sure popups only for recipe are allowed */

void resin_visibility(void);
void Change_Pause_Remote();
void Change_Auto_Remote();
void Change_Manual_Remote();
void Change_Recipe_Remote();
void Recipe_Save(int);
void Recipe_Use(int);

void Pop_InValid(int);

void Change_Ext_Pause_Remote(void);
void Change_Ext_Auto_Remote(void);
void Change_Ext_Manual_Remote(void);
void Change_Ext_Recipe_Remote(void);

recipe_book recipes;
unsigned char init_recipe;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

_GLOBAL unsigned char recipe_gateorder[12];
_GLOBAL unsigned char recipe_density[12];
_LOCAL int recipe_not_good_ack;
_LOCAL unsigned int ext_recipe_init;
_LOCAL unsigned char RecipeNum_Changed; 
_GLOBAL int pg451_popup_invalid_recipe;
_GLOBAL int pg457save_status;
_GLOBAL int pg457resin_change;
_GLOBAL unsigned char BlenderRecipeChanged; 


/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Recipe_Init()
{   
   if (PMEM.recipe_num == 0) // really first bootup.
   {
      PMEM.recipe_num = 1;
      BATCH_RECIPE_NEW->index = 1;
      BATCH_RECIPE_NOW->index = 1;
      brsstrcpy((UDINT)PMEM.recipe_name, (UDINT)"Recipe");
      HMI.pg453_RecipeNum = 1;
   }
   else
   {
      HMI.pg453_RecipeNum = PMEM.recipe_num;
      cfg_super.current_temp_recipe_num = PMEM.recipe_num;
   }
   copy_recipe(&HMI_RECIPE1, &RECIPES->entry[PMEM.recipe_num], EXTRUDER_RECIPE); 	/* init pg454 and extrusion control recipe */
   change_recipe(BATCH_RECIPE_NOW);
   Set_Recipe(BATCH_RECIPE_NOW);
   ext_recipe_init = 0;
}   

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Recipe_Main()
{
   int i;
   int GateIndex;
   static int gAlarms = 0;

   switch (HMI.CurrentPage)
   {
      case pg451_Recipe_Check:
         switch (recipe_start_state)
         {
            case RECIPE_INIT:
               if (!cfgBWH.cal_done)
               {
                  Pop_InValid(POP_CALNOTDONE);
                  recipe_start_state = RECIPE_CAL_NOT_DONE;
               }
               else if ((cfg_super.recipe_entry_mode == PERCENT ) 
                  && (!(BATCH_RECIPE_NEW->parts.ts > 99.9995 && BATCH_RECIPE_NEW->parts.ts < 100.0005)))
               {
                  recipe_start_state = RECIPE_NOT_VALID;
                  Pop_InValid(POP_INVALIDRECIPE);
               }
               else if (shr_global.recipe_changed)
               {
                  recipe_start_state = RECIPE_WAS_CHANGED;
                  Pop_InValid(POP_RATEREQUEST);
               }
               else if (!HMI.pg000_PopupInValidRecipeRuntime)
               {
				      Pop_InValid(POP_INVALIDRECIPE);
				      recipe_start_state = RECIPE_NOT_VALID;
               }
               else if (!HMI.pg450_CalNotDoneRuntime)
               {
				      Pop_InValid(POP_CALNOTDONE);
                  recipe_start_state = RECIPE_CAL_NOT_DONE;
               }
               else if (!HMI.pg450_RecipeRateSpeedRequest)
               {
                  Pop_InValid(POP_RATEREQUEST);
                  recipe_start_state = RECIPE_WAS_CHANGED;
               }
               else if ((cfg_super.job_wt_in_recipe) && (BATCH_RECIPE_NEW->job_wt < .01))
               {
                  Pop_InValid(POP_JOBWEIGHT);
                  recipe_start_state = RECIPE_NOT_VALID;
               }
               else if ((cfg_super.mix_time_in_recipe) && !(BATCH_RECIPE_NEW->mix_time > 0))
               {
                  Pop_InValid(POP_MIXTIME);
                  recipe_start_state = RECIPE_NOT_VALID;
               }
               break;
            case RECIPE_CAL_NOT_DONE:
               if (cfgBWH.cal_done)
                  recipe_start_state = RECIPE_INIT;
               if (HMI.Action_Input5 ==1)/* want to send them to calib BWH when okay is pressed GTF*/
               {
                  recipe_start_state = RECIPE_INIT;
                  HMI.Action_Input5 = 0;
                  /* set current device to BWH GTF*/
                  HMI.CurrentDevice = 1;
                  HMI.ChangePage = pg352_Calibrate_Weigh;
                  gPage = pg352_Calibrate_Weigh;
               }
               break;
            case RECIPE_NOT_VALID:
               gPage = pg453_Recipe_Input;
               if (pg451_popup_invalid_recipe)
               {
                  HMI.ChangePage = pg453_Recipe_Input;
                  recipe_start_state = RECIPE_INIT;
                  pg451_popup_invalid_recipe = FALSE;
               }
               break;
            case RECIPE_WAS_CHANGED:
               /* All */
               if (HMI.pg451_HopperRateSpeed == 2)
               {
				      /* reset the counters on diagnostic pg 411*/
					   debugFlags.numberOfOverShoot  = 0;
					   debugFlags.numberOfUnderShoot = 0;		
                 /* if blendder not in auto copy now else copy in BWH_PCC in finish_batch GTF */
                  if (BLN_OPER_MODE != AUTO_MODE)
                  {
                     copy_recipe(BATCH_RECIPE_NOW,  BATCH_RECIPE_NEW, BLENDER_RECIPE);
                     NEW_Recipe_after_batch = FALSE;
                     Signal.StoreRecipes = TRUE;
                  }
                  else
                     NEW_Recipe_after_batch = TRUE;
                  /* end recipe copy */
                  HMI.pg451_HopperRateSpeed = 0;
                  for (i=0; i<cfg_super.total_gates; i++)
                  {
                     shrGATE[i].clear_gate_cal = TRUE;
                  }
                  visible(&HMI.pg450_RecipeRateSpeedRequest, FALSE);
                  recipe_start_state = RECIPE_INIT;
                  HMI.ChangePage = gPage;
                  shr_global.recipe_changed = FALSE;
                  shr_global.bln_oper_mode = AUTO_MODE;
                  shrBWH.oper_mode = AUTO_MODE;
                  RunBlender();
               }
               /* Some */
               else if (HMI.pg451_HopperRateSpeed == 1)
               {
                  HMI.pg451_HopperRateSpeed = 0;
                  visible(&HMI.pg450_RecipeHopperReqRateRuntime[12], TRUE);
                  visible(&HMI.pg450_RecipeRateSpeedRequest, FALSE);
                  recipe_start_state = RECIPE_SELECT_GATES;
                  HMI.pg450_GoIntoAuto = FALSE;
               }
               /* None */
               else if(HMI.pg451_HopperRateSpeed == 3)
               {
                  /* if blendder not in auto copy now else copy in BWH_PCC in finish_batch GTF */
                  if (BLN_OPER_MODE != AUTO_MODE)
                  {
                     copy_recipe(BATCH_RECIPE_NOW,  BATCH_RECIPE_NEW, BLENDER_RECIPE);
                     NEW_Recipe_after_batch = FALSE;
                     Signal.StoreRecipes = TRUE;
                  }
                  else
                     NEW_Recipe_after_batch = TRUE;
                  /* end recipe copy */
                  HMI.pg451_HopperRateSpeed = 0;
                  HMI.pg450_GoIntoAuto = 1;
                  gNewMaterial = TRUE;
                  HMI.ChangePage = gPage;
                  Display_HopperRequestLayer(TRUE);
                  recipe_start_state = RECIPE_INIT;
                  shr_global.recipe_changed = FALSE;
                  shr_global.bln_oper_mode = AUTO_MODE;
                  shrBWH.oper_mode = AUTO_MODE;
                  RunBlender();
               }
               break;
            case RECIPE_SELECT_GATES:
               Display_HopperRequestLayer(TRUE);
               if (HMI.pg450_GoIntoAuto)
               {
                  /* if blendder not in auto copy now else copy in BWH_PCC in finish_batch GTF */
                  if (BLN_OPER_MODE != AUTO_MODE)
                  {
                     copy_recipe(BATCH_RECIPE_NOW, BATCH_RECIPE_NEW, BLENDER_RECIPE);
                     NEW_Recipe_after_batch = FALSE;
                     Signal.StoreRecipes = TRUE;                     
                  }
                  else
                     NEW_Recipe_after_batch = TRUE;
                  /* end recipe copy */
                  gNewMaterial = TRUE;
                  HMI.ChangePage = gPage;
                  Display_HopperRequestLayer(FALSE);
                  recipe_start_state = RECIPE_INIT;
                  shr_global.recipe_changed = FALSE;
                  shr_global.bln_oper_mode = AUTO_MODE;
                  shrBWH.oper_mode = AUTO_MODE;
                  RunBlender();
               }
               break;
         }
      break;

      case pg453_Recipe_Input:
  		   ext_recipe_init = 0;  /* init pg454 */
		   if ((pg457resin_change) || (HMI.pg457_ResinSaveRuntime))
         {
            Resin_Edit();
            Recipe_Save(HMI.pg453_RecipeNum);				
         }
         gPage = HMI.CurrentPage;

		   switch (HMI.pg453_Recipe_Action)
         {
		      case RECIPE_INITIALIZE:
				   HMI.pg453_RecipeNum = PMEM.recipe_num;
				   RECIPES->entry[PMEM.recipe_num].index = PMEM.recipe_num;
				   copy_recipe(&BATCH_RECIPE_EDIT, &RECIPES->entry[PMEM.recipe_num], BLENDER_RECIPE);
               calc_recipe(&BATCH_RECIPE_EDIT);				
				   copy_recipe(&UI_RECIPE, BATCH_RECIPE_NOW, BLENDER_RECIPE); // current
				   pg453_Hopper_Visibility();
				   Pop_InValid(POP_NONE);
				   HMI.pg453_Recipe_Action = RECIPE_NOACTION;
			   break;
		      case RECIPE_USE:
               Recipe_Use(HMI.pg453_RecipeNum);
               if ((recipe_not_good_ack) || (pg451_popup_invalid_recipe))
               {
                  visible(&HMI.pg000_PopupInValidRecipeRuntime, FALSE);
                  HMI.pg453_Recipe_Action = RECIPE_NOACTION;
                  pg451_popup_invalid_recipe = FALSE;
               }
			      else
			      {
				      HMI.ChangePage = pg000_MainPage;	
				      BlenderRecipeChanged = 1; 
				   }
            break;
            case RECIPE_CHANGE:
               copy_recipe(&BATCH_RECIPE_EDIT, &RECIPES->entry[HMI.pg453_RecipeNum], BLENDER_RECIPE);
			      calc_recipe(&BATCH_RECIPE_EDIT);
				
			      for (i=0; i<cfg_super.total_gates; i++)
			      {
			         brsstrcpy((DINT)HMI.pg457_ResinDesc[i], (DINT)cfg_super.resin_table[BATCH_RECIPE_EDIT.resin_recipe_num[i]].resin_desc);
			      }		
			      HMI.pg453_Recipe_Action = RECIPE_NOACTION;
               pg453_Hopper_Visibility();				
            break;
            case RECIPE_DELETE:
               visible(&HMI.pg453_DeleteRecipeRunTime, TRUE);
               if (HMI.Action_Input7 ==1) /* yes was pressed */
               {
                  copy_recipe(&BATCH_RECIPE_EDIT, EMPTY_RECIPE, BLENDER_RECIPE);
                  copy_recipe(&RECIPES->entry[HMI.pg453_RecipeNum], EMPTY_RECIPE, BLENDER_RECIPE);
                  HMI.pg453_Recipe_Action = RECIPE_NOACTION;
                  HMI.Action_Input7 = 0;
                  visible(&HMI.pg453_DeleteRecipeRunTime, FALSE);
                  visible(&HMI.pg453_LayControlsRunTime, FALSE);
                  Signal.StoreRecipes = TRUE;
               }
               else if (HMI.Action_Input7 == 2) /* no was pressed */
               {
                  HMI.Action_Input7 = 0;
                  visible(&HMI.pg453_DeleteRecipeRunTime, FALSE);
                  visible(&HMI.pg453_LayControlsRunTime, FALSE);
                  HMI.pg453_Recipe_Action = RECIPE_NOACTION;
               }
            break;
            case RECIPE_NEXT_PREV:
				   if (++HMI.pg453change_layer > 3)
						HMI.pg453change_layer = 0;
               HMI.pg453_Recipe_Action = RECIPE_NOACTION;
			      pg453_Hopper_Visibility();
			   break; 
            case RECIPE_DELETE_ALL:
               visible(&HMI.pg453_DeleteRecipeRunTime, TRUE);
               if (HMI.Action_Input7 == 1) /* yes was pressed */
               {
				      HMI.Action_Input7 = 0;
					   copy_recipe(&BATCH_RECIPE_EDIT, EMPTY_RECIPE, BLENDER_RECIPE);
                  copy_recipe(BATCH_RECIPE_NEW, EMPTY_RECIPE, BLENDER_RECIPE);
                  for (i=0; i<(MAXRECIPE_S-11); i++)
                     copy_recipe(&RECIPES->entry[i], EMPTY_RECIPE, BLENDER_RECIPE);
                  HMI.pg453_Recipe_Action = RECIPE_NOACTION;
                  visible(&HMI.pg453_DeleteRecipeRunTime, FALSE);                 
                  visible(&HMI.pg453_LayControlsRunTime, FALSE);
                  Signal.StoreRecipes = TRUE;
				   }
               else if(HMI.Action_Input7 == 2) /* no was pressed */
               {
                  HMI.Action_Input7 = 0;
                  visible(&HMI.pg453_DeleteRecipeRunTime, FALSE);
                  visible(&HMI.pg453_LayControlsRunTime, FALSE);
                  HMI.pg453_Recipe_Action = RECIPE_NOACTION;
               }
            break;
            case RECIPE_CANCEL:
               Signal.RestoreRecipes = TRUE;
               HMI.pg453_Recipe_Action = RECIPE_NOACTION;               
            break;
			   case RECIPE_EDIT:
			      calc_recipe(&BATCH_RECIPE_EDIT);
			      if (HMI.pg453_RecipeUseRunTime == 0)
				      Recipe_Save(HMI.pg453_RecipeNum);
			      HMI.pg453_Recipe_Action = RECIPE_NOACTION;
			   break;
				case RECIPE_NOACTION:
               /* put nothing between here and default GTF */
            default:
               visible(&HMI.pg453_DeleteRecipeRunTime, FALSE);
               visible(&HMI.pg000_PopupInValidRecipeRuntime, FALSE);
            break;
         }
      break;

      case pg454_Recipe_Entry:
         if (!gAlarms)
         {
            visible(&HMI.pg450_RecipeOptionalRuntime, FALSE);
            Calc_txt_RecipeEntry1();
            visible(&HMI.SetupRuntime7, FALSE);
            Calc_txt_RecipeEntry2();
            visible(&HMI.SetupRuntime8, FALSE);
            Calc_txt_RecipeEntry3();
            
            if (cfg_super.scrap && (cfg_super.refeed == TRIM_ONLY)) 
               visible(&HMI.pg454_recipe_refeed_visible_1, TRUE);
            else
               visible(&HMI.pg454_recipe_refeed_visible_1, FALSE);
	  
            if ((!ext_recipe_init) || RecipeNum_Changed)		  /* only read original recipe when init or change recipe # */
	         {
	            copy_recipe(&BATCH_RECIPE_EDIT, &RECIPES->entry[HMI.pg453_RecipeNum], EXTRUDER_RECIPE);
	            ext_recipe_init = 1;
	            RecipeNum_Changed = 0;
	         }
	  
            // doing both saving and getting ready for auto when you edit field
	         if (HMI.pg450_StoredRecipeRuntime == 1)
	         {
  	            copy_recipe(&RECIPES->entry[HMI.pg453_RecipeNum], &BATCH_RECIPE_EDIT, EXTRUDER_RECIPE);  
				   copy_recipe(BATCH_RECIPE_NEW, &BATCH_RECIPE_EDIT, EXTRUDER_RECIPE);  
	            change_recipe(BATCH_RECIPE_NEW);
               Signal.StoreRecipes = TRUE;
	            HMI.pg450_StoredRecipeRuntime = 0;
	            ext_recipe_init = 0;
	         }
        }
         
        Signal.EnteredSetup = TRUE;
        if ((HMI.Action_ExtruderMode == 1) && !Signal.StopExtr)
        {
           Signal.StopExtr = TRUE;
        }
        else if (HMI.Action_ExtruderMode == 2)
        {
           if (shr_global.alarm_bits & SYS_SHUTDOWN)
           {
              visible(&HMI.pg000_PopupClearAlarms,TRUE);
           }
           else
           {
              visible(&HMI.pg000_PopupClearAlarms,FALSE);
              HMI.Action_ExtruderMode = gcInActive;
              HMI.ChangePage = pg009_ExtManual;
           }
           HMI.Action_ExtruderMode = gcInActive;
        }
        else if (HMI.Action_ExtruderMode == 3)
        {
           /* go into automatic mode */
           HMI.Action_ExtruderMode = gcInActive;
           if (shr_global.alarm_bits & SYS_SHUTDOWN)             /* have shutdown alarm  */
           {
              gAlarms = 1;
              visible(&HMI.pg000_PopupClearAlarms, TRUE);
              visible(&HMI.pg450_RecipeOptionalRuntime, FALSE);
              visible(&HMI.SetupRuntime6, FALSE);
              visible(&HMI.SetupRuntime7, FALSE);
              visible(&HMI.SetupRuntime8, FALSE);
           }
           else
           {
              visible(&HMI.pg000_PopupClearAlarms, FALSE);
              if (cfg_super.no_auto_from_pause && (EXT_HO_OPER_MODE == PAUSE_MODE))
              {
                gAlarms = 1;
                visible(&HMI.pg000_PopupCannotAutoPause, TRUE);
                visible(&HMI.pg450_RecipeOptionalRuntime, FALSE);
                visible(&HMI.SetupRuntime6, FALSE);
                visible(&HMI.SetupRuntime7, FALSE);
                visible(&HMI.SetupRuntime8, FALSE);
              }
              else
              {
                 visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
                 Signal.RunExtr = TRUE;
              }
           }
        }
        /* clear alarms before entering extruder manual */
        if (HMI.Action_Input0 == 1)
        {
           gAlarms = 0;
           visible(&HMI.pg000_PopupClearAlarms, FALSE);
           HMI.Action_Input0 = gcInActive;
        }
         /* cannot go from pause to auto directly */
        if (HMI.Action_Input0 == 2)
        {
           gAlarms = 0;
           visible(&HMI.pg000_PopupCannotAutoPause, FALSE);
           HMI.Action_Input0 = gcInActive;
        }
        break;

       case pg455_Change_Recipe:
       break;
       case pg456_Recipe_Resin:
         Resin_Edit();
         for (GateIndex=0; GateIndex<MAX_GATES; GateIndex++)
         {
            if (GateIndex < cfg_super.total_gates)
            {
               visible(&HMI.pg450_RecipeCurrentRuntime[GateIndex], TRUE);
            }
            else
            {
               visible(&HMI.pg450_RecipeCurrentRuntime[GateIndex], FALSE);
            }
         }
         break;

       case pg457_Resin_Editor:
          Resin_Edit();
          if (pg457resin_change)
          {
             pg457resin_change = 0;
             brsstrcpy((DINT)HMI.UI_ResinTable.resin_desc, (DINT)cfg_super.resin_table[HMI.pg457_ResinNum].resin_desc);
             HMI.UI_ResinTable.density = cfg_super.resin_table[HMI.pg457_ResinNum].density;
          }
          if (HMI.pg457_ResinSaveRuntime == 1)
          {
             pg457save_status = 2;
             HMI.pg457_ResinSaveRuntime = 0;
             brsstrcpy((UDINT)cfg_super.resin_table[HMI.pg457_ResinNum].resin_desc, (UDINT)HMI.UI_ResinTable.resin_desc);
             cfg_super.resin_table[HMI.pg457_ResinNum].density = HMI.UI_ResinTable.density;
             Signal.EnteredSetup = TRUE;
          }
          break;
       default:
       break;
   }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Calc_txt_RecipeEntry1()
{
  visible(&HMI.SetupRuntime6, TRUE);
  switch(cfg_super.recipe_mode[0])
  {
    case WTP:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"WTP");
      visible(&HMI.pg454_recipe_wtp_visible_1, TRUE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
    case WPL:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"WPL");
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, TRUE);
      visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
    case WPA:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"WPA");
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
       visible(&HMI.pg454_recipe_wpa_visible_1, TRUE);
    break;
    case THICKNESS:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"Thickness");
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, TRUE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
       visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
    case OD:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"OD");
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, TRUE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
       visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
    case ID:
      brsstrcpy((UDINT)HMI.pg450_txtFirstRecipeEntry, (UDINT)"ID");
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, TRUE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
       visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
    default:  /* NONE */
      visible(&HMI.SetupRuntime6, FALSE);
      visible(&HMI.pg454_recipe_wtp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_width_visible_1, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_1, FALSE);
      visible(&HMI.pg454_recipe_od_visible_1, FALSE);
      visible(&HMI.pg454_recipe_id_visible_1, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_1, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_1, FALSE);
    break;
  }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Calc_txt_RecipeEntry2()
{
  switch(cfg_super.recipe_mode[1])
  {
    case WIDTH:
      brsstrcpy((UDINT)HMI.pg450_txtSecRecipeEntry, (UDINT)"Width");
      visible(&HMI.SetupRuntime7, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_width_visible_2, TRUE);
      visible(&HMI.pg454_recipe_ltp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_od_visible_2, FALSE);
      visible(&HMI.pg454_recipe_id_visible_2, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_2, FALSE);
    break;
    case THICKNESS:
      brsstrcpy((UDINT)HMI.pg450_txtSecRecipeEntry, (UDINT)"Thickness");
      visible(&HMI.SetupRuntime7, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_width_visible_2, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_od_visible_2, FALSE);
      visible(&HMI.pg454_recipe_id_visible_2, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_2, TRUE);
      visible(&HMI.pg454_recipe_wpl_visible_2, FALSE);
       visible(&HMI.pg454_recipe_wpa_visible_2, FALSE);
    break;
    case OD:
      brsstrcpy((UDINT)HMI.pg450_txtSecRecipeEntry, (UDINT)"OD");
      visible(&HMI.SetupRuntime7, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_width_visible_2, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_od_visible_2, TRUE);
      visible(&HMI.pg454_recipe_id_visible_2, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_2, FALSE);
    break;
    case ID:
      brsstrcpy((UDINT)HMI.pg450_txtSecRecipeEntry, (UDINT)"ID");
      visible(&HMI.SetupRuntime7, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_width_visible_2, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_od_visible_2, FALSE);
      visible(&HMI.pg454_recipe_id_visible_2, TRUE);
      visible(&HMI.pg454_recipe_thickness_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_2, FALSE);
    break;
    default:  /* NONE */
      visible(&HMI.pg454_recipe_wtp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_width_visible_2, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_2, FALSE);
      visible(&HMI.pg454_recipe_od_visible_2, FALSE);
      visible(&HMI.pg454_recipe_id_visible_2, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_2, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_2, FALSE);
    break;
  }

}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Calc_txt_RecipeEntry3()
{
  switch(cfg_super.recipe_mode[2])
  {
    case LTP:
      brsstrcpy((UDINT)HMI.pg450_txtThirdRecipeEntry, (UDINT)"LTP");
      visible(&HMI.SetupRuntime8, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_3, FALSE);
      visible(&HMI.pg454_recipe_width_visible_3, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_3, TRUE);
      visible(&HMI.pg454_recipe_od_visible_3, FALSE);
      visible(&HMI.pg454_recipe_id_visible_3, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_3, FALSE);
    break;
    case WTP:
      brsstrcpy((UDINT)HMI.pg450_txtThirdRecipeEntry, (UDINT)"WTP");
      visible(&HMI.SetupRuntime8, TRUE);
      visible(&HMI.pg454_recipe_wtp_visible_3, TRUE);
      visible(&HMI.pg454_recipe_width_visible_3, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_3, FALSE);
      visible(&HMI.pg454_recipe_od_visible_3, FALSE);
      visible(&HMI.pg454_recipe_id_visible_3, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_3, FALSE);
    break;
    default:  /* NONE */
      visible(&HMI.pg454_recipe_wtp_visible_3, FALSE);
      visible(&HMI.pg454_recipe_width_visible_3, FALSE);
      visible(&HMI.pg454_recipe_ltp_visible_3, FALSE);
      visible(&HMI.pg454_recipe_od_visible_3, FALSE);
      visible(&HMI.pg454_recipe_id_visible_3, FALSE);
      visible(&HMI.pg454_recipe_thickness_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpl_visible_3, FALSE);
      visible(&HMI.pg454_recipe_wpa_visible_3, FALSE);
    break;
  }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void input_bln_parts(recipe_struct *r)
{
   int   i;
   int   CalParts = FALSE;
   float totals;

   if (cfg_super.recipe_entry_mode == PERCENT)
   {
      totals = 0;
      for (i=0; i<MAX_GATES; i++)
      {
         if (i < cfg_super.total_gates)
         {
            /* recipe parts limited to 2* target accuracy (expressed as %) GTF */
            if ((r->parts.s[i] > 0) && (r->parts.s[i] < (cfgGATE[i].target_accuracy*100.0*2.0)))
               r->parts.s[i] = (cfgGATE[i].target_accuracy*100.0*2.0);

            if (cfgGATE[i].hopper_type != REGRIND)
               totals += r->parts.s[i];
         } 
         else
         {
            r->parts.s[i] = 0; /* if i greater than num gates enabled GTF*/
         }
      }
      r->parts.t = totals;
      if (totals < 100)
         CalParts = TRUE;
      else
         CalParts = FALSE;
   }
   else /* recipe is in parts GTF */
   {
      totals = 0;
      for (i=0; i<cfg_super.total_gates; i++)
      {
         if (cfgGATE[i].hopper_type != REGRIND)
            totals += BATCH_RECIPE_NEW->parts.s[i];
      }
      for (i=0; i<cfg_super.total_gates; i++)
      {
         if ((BATCH_RECIPE_NEW->parts.s[i] > 0) && ((BATCH_RECIPE_NEW->parts.s[i]/totals) < (cfgGATE[i].target_accuracy*100.0*2.0)))
            BATCH_RECIPE_NEW->parts.s[i] = totals*(cfgGATE[i].target_accuracy*2.0);
      }
   }

   if (CalParts)
   {
      BATCH_RECIPE_NEW->parts.t = 0;
      for (i=0; i<MAX_GATES; i++)
      {
         if (cfgGATE[i].hopper_type != REGRIND)
         {
            BATCH_RECIPE_NEW->parts.t += BATCH_RECIPE_NEW->parts.s[i];
            totals = BATCH_RECIPE_NEW->parts.t;
            if (i > (cfg_super.total_gates - 1)) /*  this is intended to clear all gates above cfg_super.total_gates GTF */
               BATCH_RECIPE_NEW->parts.s[i] = 0;
         }
      }
   }
}

/************************************************************************************************************************/
/* Function to alter recipe parameters                                                                                  */
/************************************************************************************************************************/

void change_recipe(recipe_struct *r)
{
   if (BATCH_RECIPE_EDIT.total_wtp == 0.0)
      BATCH_RECIPE_EDIT.total_wtp = cfg_super.default_wtp;
   if (recipe_num_changed(r->total_wtp, BATCH_RECIPE_EDIT.total_wtp))
      shr_global.recipe_changed = TRUE;
   r->total_wtp = BATCH_RECIPE_EDIT.total_wtp;

   if (BATCH_RECIPE_EDIT.ltp == 0.0)
      BATCH_RECIPE_EDIT.ltp = cfg_super.default_ltp;
   if (recipe_num_changed(r->ltp, BATCH_RECIPE_EDIT.ltp))
       shr_global.recipe_changed = TRUE;
   r->ltp = BATCH_RECIPE_EDIT.ltp;

   if (BATCH_RECIPE_EDIT.wpl == 0.0)
      BATCH_RECIPE_EDIT.wpl = cfg_super.default_wpl;
   if (recipe_num_changed(r->wpl, BATCH_RECIPE_EDIT.wpl))
      shr_global.recipe_changed = TRUE;
   r->wpl = BATCH_RECIPE_EDIT.wpl;

   if (BATCH_RECIPE_EDIT.wpa == 0.0)
      BATCH_RECIPE_EDIT.wpa = cfg_super.default_wpa;
   if (recipe_num_changed(r->wpa, BATCH_RECIPE_EDIT.wpa))
      shr_global.recipe_changed = TRUE;
   r->wpa = BATCH_RECIPE_EDIT.wpa;

   if (BATCH_RECIPE_EDIT.thk == 0.0)
      BATCH_RECIPE_EDIT.thk = cfg_super.default_thk;
   if (recipe_num_changed(r->thk, BATCH_RECIPE_EDIT.thk))
      shr_global.recipe_changed = TRUE;
   r->thk = BATCH_RECIPE_EDIT.thk;
      
   if (BATCH_RECIPE_EDIT.od == 0.0)
      BATCH_RECIPE_EDIT.od = cfg_super.default_od;
   if (recipe_num_changed(r->od, BATCH_RECIPE_EDIT.od))
      shr_global.recipe_changed = TRUE;
   r->od = BATCH_RECIPE_EDIT.od;
      
   if (BATCH_RECIPE_EDIT.id == 0.0)
      BATCH_RECIPE_EDIT.id = cfg_super.default_id;
   if (recipe_num_changed(r->id, BATCH_RECIPE_EDIT.id))
      shr_global.recipe_changed = TRUE;
   r->id = BATCH_RECIPE_EDIT.id;

   if (BATCH_RECIPE_EDIT.width == 0.0)
     BATCH_RECIPE_EDIT.width = cfg_super.default_width;
   if (recipe_num_changed(r->width, BATCH_RECIPE_EDIT.width))
      shr_global.recipe_changed = TRUE;
   r->width = BATCH_RECIPE_EDIT.width;

   input_ext_parts(r);
   input_bln_recipe(r);

   if (cfg_super.ltp_mode == MONITOR)
      r->ltp = ACTUAL_RECIPE->ltp;
   if (cfg_super.wth_mode == MONITOR)
      r->width = ACTUAL_RECIPE->width;

   calc_recipe(r);               /* calculate all recipe stuff   */

   r->used = TRUE;               /* mark as used                 */
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void input_ext_parts(recipe_struct *r)
{
   if (cfg_super.scrap && (cfg_super.refeed == GRAVIFLUFF))
   {
      /* enter virgin parts */
      if (recipe_num_changed(r->parts.ext, UI_RECIPE.parts.ext))
         shr_global.recipe_changed = TRUE;
      r->parts.ext = UI_RECIPE.parts.ext;

      /* enter scrap parts */
      if (recipe_num_changed(r->max_scrap_parts, UI_RECIPE.max_scrap_parts))
         shr_global.recipe_changed = TRUE;
      r->max_scrap_parts = UI_RECIPE.max_scrap_parts;
      r->parts.s[REFEED] = r->max_scrap_parts;
   }
   else
   {
      r->parts.ext = 100;
      r->max_scrap_parts = 0;
   }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void input_bln_recipe(recipe_struct *r)
{
#if 0
   if (cfg_super.job_wt_in_recipe)
   {
       if (recipe_num_changed(r->job_wt, UI_RECIPE.job_wt))
          shr_global.recipe_changed = TRUE;
       r->job_wt = UI_RECIPE.job_wt;
   }

   if (cfg_super.mix_time_in_recipe)
   {
      if (recipe_num_changed(r->mix_time, UI_RECIPE.mix_time))
         shr_global.recipe_changed = TRUE;
      r->mix_time = UI_RECIPE.mix_time;

   }
#endif
   if (cfg_super.enable_resin_codes)
   {
       input_bln_recipe_w_codes(r);
   }
   else
   {
       input_bln_recipe_no_codes(r);
   }
   set_recipe_gate_order(r);
}

/***********************************************************************************************************************/
/* Funtion: input_bln_recipe_w_codes                                                                                   */
/***********************************************************************************************************************/
void input_bln_recipe_w_codes(recipe_struct *r)
{
   int j, i;

   if (cfg_super.recipe_type == NORMAL_RECIPE)
      input_bln_parts(r);
   else
      input_bln_parts_molding(r);

   for (j=0; j<cfg_super.total_gates; j++)
   {
      for (r->parts.t=0.0, i=0; i<cfg_super.total_gates; i++)  /* Keep total current incase user aborts */
         r->parts.t = r->parts.t + r->parts.s[i];

      for (i=0; i<cfg_super.total_gates; i++)
      {
         if (r->parts.t > 0.0)
            r->parts_pct.s[i] = r->parts.s[i]/r->parts.t;
         else
            r->parts_pct.s[i] = 0.0;
      }

      if (cfg_super.density_in_recipe || cfg_super.force_density)
      {
         r->density.s[j] = cfg_super.resin_table[r->resin_recipe_num[j]].density;
      }
   }
}

/***********************************************************************************************************************/
/* Funtion: input_bln_parts_molding                                                                                   */
/***********************************************************************************************************************/
void input_bln_parts_molding(recipe_struct *r)
{
   int i;

   if (cfg_super.recipe_entry_mode == PERCENT)
   {
      r->parts.t = 0;
      for (i=0; i<cfg_super.total_gates; i++)
      {
         if (cfgGATE[i].hopper_type != REGRIND && (cfgGATE[i].recipe_use == COLOR_HOP || cfgGATE[i].recipe_use == ADDITIVE_HOP))
         {
            r->parts.t += r->parts.s[i];
         }
      }
      for (i=0; i<cfg_super.total_gates; i++)
      {
         if (cfgGATE[i].hopper_type != REGRIND && cfgGATE[i].recipe_use == VIRGIN_HOP)
         {
            if (r->parts.t >= 100 )
               r->parts.s[i] = 0;
            r->parts.t += r->parts.s[i]; 
         }
      }
   }
}

/***********************************************************************************************************************/
/* Funtion: input_bln_recipe_no_codes                                                                                   */
/***********************************************************************************************************************/
void input_bln_recipe_no_codes(recipe_struct *r)
{
    int   i, j;
    float *values[MAX_RECIPE_VALUES];

    if (cfg_super.recipe_type == NORMAL_RECIPE)
       input_bln_parts(r);
    else
       input_bln_parts_molding(r);

    if (cfg_super.density_in_recipe || cfg_super.force_density)
    {
       j = 0;
       /* get densities now */

       /* do virgin ones first */
       if (cfg_super.recipe_type == NORMAL_RECIPE)
       {
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type != REGRIND)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
         }
         /* now do regrind */
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type == REGRIND)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
          }
       }
       else
       {
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type != REGRIND && cfgGATE[i].recipe_use == COLOR_HOP)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
         }
         /* now do additive */
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type != REGRIND && cfgGATE[i].recipe_use == ADDITIVE_HOP)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
         }
         /* now do regrind */
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type != REGRIND)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
         }
         /* now do virgin */
         for (i=0; i<cfg_super.total_gates; i++)
         {
            if (cfgGATE[i].hopper_type != REGRIND && cfgGATE[i].recipe_use == VIRGIN_HOP)
            {
               values[j] = &(r->density.s[i]);
               ++j;
            }
         }
      }
      r->parts.t = 1.0;
   }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Pop_InValid(int index)
{
   visible(&HMI.pg000_PopupInValidRecipeRuntime, FALSE);
   visible(&HMI.pg000_InValidJobWeightRuntine, FALSE);   
   visible(&HMI.pg000_PopupFinishBatchRuntime, FALSE);
   visible(&HMI.pg000_InValidMixTimeRuntine, FALSE);
   visible(&HMI.pg450_CalNotDoneRuntime, FALSE);
   visible(&HMI.pg450_BeginNewJobRunTime, FALSE);
   visible(&HMI.pg450_JobNameRuntime, FALSE);
   visible(&HMI.pg450_RecipeNewMaterialRunTime, FALSE);
   visible(&HMI.pg450_RecipeRateSpeedRequest, FALSE);
   Display_HopperRequestLayer(FALSE);

   switch (index)
   {
      case POP_NONE:
         break;
      case POP_INVALIDRECIPE:
         visible(&HMI.pg000_PopupInValidRecipeRuntime, TRUE);
         break;
      case POP_CALNOTDONE:
         visible(&HMI.pg450_CalNotDoneRuntime, TRUE);
         break;
      case POP_JOBWEIGHT:
         visible(&HMI.pg000_InValidJobWeightRuntine, TRUE);
         break;
      case POP_RECIPEJOBNAME:
         visible(&HMI.pg450_JobNameRuntime, TRUE);         
         break;
      case POP_NEWMATERIAL:
         visible(&HMI.pg450_RecipeNewMaterialRunTime, TRUE);
         break;
      case POP_RATEREQUEST:
         visible(&HMI.pg450_RecipeRateSpeedRequest, TRUE);
         break;
      case POP_MIXTIME:
         visible(&HMI.pg000_InValidMixTimeRuntine, TRUE);
         break;         
      case POP_DEFAULTLAYER:
         Display_HopperRequestLayer(TRUE);
         break;
   }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Display_HopperRequestLayer(int show)
{
   int i;

   for (i=0; i<12; i++)
   {
      if (i<cfg_super.total_gates)
      {
         visible(&HMI.pg450_RecipeHopperReqRateRuntime[i], show);
      }
      else
      {
         visible(&HMI.pg450_RecipeHopperReqRateRuntime[i], FALSE);
      }
   }
   visible(&HMI.pg450_RecipeHopperReqRateRuntime[12], show);
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void RunBlender(void)
{
   if (HMI.pg450_CalcRateSpeed)
   {   
       Pop_InValid(POP_RATEREQUEST);
       if (HMI.pg450_GoIntoAuto)
          HMI.pg450_CalcRateSpeed = 0;
   }
   else if (HMI.pg450_GoIntoAuto)
   {
      HMI.ChangePage = gPage;
   }
}

/************************************************************************/
/* Function:  Resin_Edit()                                             */
/************************************************************************/
void Resin_Edit()
{
   int i = 0;

   if (pg457resin_change == 1)// pg453
   {
	  for (i=0; i<cfg_super.total_gates; i++)
  	  {
	     brsstrcpy((UDINT)HMI.pg457_ResinDesc[i], (UDINT)cfg_super.resin_table[BATCH_RECIPE_EDIT.resin_recipe_num[i]].resin_desc);
	  }		
     Signal.StoreRecipes = TRUE;
	  Signal.EnteredSetup = TRUE;
   }		

   if (HMI.pg457_ResinSaveRuntime == 1)// pg453
   {
      for (i=0; i<cfg_super.total_gates; i++)
	   {
	      brsstrcpy((UDINT)cfg_super.resin_table[BATCH_RECIPE_EDIT.resin_recipe_num[i]].resin_desc, (UDINT)HMI.pg457_ResinDesc[i]);
	   }		
      Signal.EnteredSetup = TRUE;
   }
	pg457resin_change = 0;
	HMI.pg457_ResinSaveRuntime = 0;
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Extruder_Visibility(void)
{
   /* display extrusion recipe button */
   if (!((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED)))
   {
      visible(&HMI.ExtControlRuntime1, FALSE);
   }
   else
   {
      visible(&HMI.ExtControlRuntime1, TRUE); 
   }
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Hoppers_off(void)
{
   int i;
   
   visible(&HMI.SetupRuntime1, FALSE); // Mix_Time 
   visible(&HMI.SetupRuntime2, FALSE); // Job_Wt
   visible(&HMI.SetupRuntime3, FALSE); // Product_Code
   visible(&HMI.PopupRuntime1, FALSE); // Icons Layer 1
   visible(&HMI.PopupRuntime2, FALSE); // Icons Layer 2
   visible(&HMI.PopupRuntime3, FALSE); // Icons Layer 3
	
	visible(&Resin_HopARuntime, FALSE);
	visible(&Resin_HopBRuntime, FALSE);
	visible(&Resin_HopCRuntime, FALSE);
	visible(&Resin_HopDRuntime, FALSE);
	visible(&Resin_HopERuntime, FALSE);
	visible(&Resin_HopFRuntime, FALSE);
	visible(&Resin_HopGRuntime, FALSE);
	visible(&Resin_HopHRuntime, FALSE);
	visible(&Resin_HopIRuntime, FALSE);
	visible(&Resin_HopJRuntime, FALSE);
	visible(&Resin_HopKRuntime, FALSE);
	visible(&Resin_HopLRuntime, FALSE);
	for (i=0; i<MAX_GATES; i++)
	{
	   visible(&recipe_density[i], FALSE);
	   visible(&recipe_gateorder[i], FALSE);		
	   visible(&HMI.pg450_RecipeCurrentRuntime[i], FALSE);
	}
}

/**************************************************************************************************/
/*  Handles pages pg450, pg453, pg405, pg406, pg334, pg335 & pg345 UpDownDataPoint Logic          */
/**************************************************************************************************/
void UpDownDataPoint(int *ipChange_Layer, unsigned char *ucpLayerRuntime1, unsigned char *ucpLayerRuntime2)
{
	if (*ipChange_Layer == 3)
		*ipChange_Layer = 0;
	
	if ((cfg_super.total_gates < 5) && (*ipChange_Layer == 1))
		*ipChange_Layer = 0;
	else if ((cfg_super.total_gates <= 8) && (*ipChange_Layer == 2))
		*ipChange_Layer = 0;

	switch (*ipChange_Layer)
	{
		case 0:
			visible(ucpLayerRuntime1, FALSE);
			visible(ucpLayerRuntime2, FALSE);
			break;
		case 1:
			visible(ucpLayerRuntime1, TRUE);
			visible(ucpLayerRuntime2, FALSE);
			break;
		case 2:
			visible(ucpLayerRuntime1, FALSE);
			visible(ucpLayerRuntime2, TRUE);
			break;
	}
} 
/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void pg453_Hopper_Visibility(void)
{
	int i;
	int GateIndex;
	      
	Hoppers_off();

   visible(&HMI.pg450_Show_Next, FALSE); //G2-691

	if ( (cfg_super.ucIconInterface) && (!cfg_super.enable_resin_codes) &&
		 (!cfg_super.mix_time_in_recipe) && (!cfg_super.job_wt_in_recipe) &&
		 (!cfg_super.force_density) && (!cfg_super.gate_order_in_recipe) )
	{
		visible(&HMI.pg453_LayerRuntime1, FALSE); // pg453 layer 1
		visible(&HMI.pg453_LayerRuntime2, FALSE); // pg453 layer 2
		UpDownDataPoint(&HMI.pg453change_layer, &HMI.PopupRuntime2, &HMI.PopupRuntime3);
		visible(&HMI.PopupRuntime1, TRUE); // Icons Layer 1
	}
	else
	{
		if (cfg_super.recipe_entry_mode == PERCENT)
			visible(&HMI.pg453_LayerRuntime3, FALSE); // HEADER text is %
		else
		   visible(&HMI.pg453_LayerRuntime3, TRUE);  // HEADER text is PARTS
		
		UpDownDataPoint(&HMI.pg453change_layer, &HMI.pg453_LayerRuntime1, &HMI.pg453_LayerRuntime2);
		Extruder_Visibility();

		if (cfg_super.mix_time_in_recipe)
		{
			visible(&HMI.SetupRuntime1, TRUE);
		}

		if (cfg_super.job_wt_in_recipe)
		{
			visible(&HMI.SetupRuntime2, TRUE);
		}

		if (cfg_super.enable_resin_codes)
		{    
			resin_visibility();
			visible(&HMI.pg453_LayerRuntime4, FALSE);
			for (i=0; i<cfg_super.total_gates; i++)
			{
				brsstrcpy((UDINT)HMI.pg457_ResinDesc[i], (UDINT)cfg_super.resin_table[BATCH_RECIPE_EDIT.resin_recipe_num[i]].resin_desc);
			}		
		}
		else
		{
			if ((!cfg_super.force_density) && (!cfg_super.gate_order_in_recipe))
			{	
				visible(&HMI.pg453_LayerRuntime4, TRUE);
				visible(&HMI.pg453_LayerRuntime1, FALSE);
				visible(&HMI.pg453_LayerRuntime2, FALSE);

            visible(&HMI.pg450_Show_Next, FALSE); //G2-691
            
				for (GateIndex=0; GateIndex<MAX_GATES; GateIndex++)
				{
					if (GateIndex < cfg_super.total_gates)
					{
						visible(&HMI.pg450_RecipeCurrentRuntimeAll[GateIndex], TRUE);
					}
					else
					{
						visible(&HMI.pg450_RecipeCurrentRuntimeAll[GateIndex], FALSE);
					}
				}
			}
		}
	
		//if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED)) //G2-603
		{
			if (cfg_super.force_density)
			{
				visible(&HMI.pg453_LayerRuntime4, FALSE);
            visible(&HMI.pg450_Show_Next, TRUE); //G2-691
				for (i=0; i<MAX_GATES; i++)
				{
					if (i < cfg_super.total_gates)
						visible(&recipe_density[i], TRUE);
				}
			}
		}
			
		if (cfg_super.gate_order_in_recipe)	
		{
			visible(&HMI.pg453_LayerRuntime4, FALSE);
			for (i=0; i<MAX_GATES; i++)
			{
				if (i < cfg_super.total_gates)
					visible(&recipe_gateorder[i], TRUE);
			   //HMI.pg453_Gate_Order_Index[BATCH_RECIPE_EDIT.gate_order[i].number] = i+1;				
			}
		}		
	}
	
	//visible(&HMI.StoreRecipeLock, TRUE); 
	visible(&HMI.pg453_RecipeUseRunTime, TRUE);
	 
	for (i=0; i<cfg_super.total_gates; i++)
	{
		visible(&HMI.pg450_RecipeCurrentRuntime[i], TRUE);
		if (cfgGATE[i].hopper_type == REGRIND)
		{
			cfgGATE[i].recipe_use = 3;
			HMI.pg330_GateFeedingColor[i] = gYellow;
		}
		else
		{
			HMI.pg330_GateFeedingColor[i] = gGreen;
		}
		if (cfg_super.current_security_level > cfg_super.hopper_parts_lock[i])
		{
			HMI.pg450_RecipeEditLock[i] = SUPERVISOR_LOCK;
			HMI.pg450_RecipeOptionalOrder[i] = SUPERVISOR_LOCK;
		}
		else
		{
			HMI.pg450_RecipeEditLock[i] = OPERATOR_LOCK;
			HMI.pg450_RecipeOptionalOrder[i] = SUPERVISOR_LOCK;
		}
	}
	if (HMI.pg453_RecipeNum == 1)
	{
		for (i=0; i<cfg_super.total_gates; i++)
		{
			HMI.pg450_RecipeEditLock[i] = SUPERVISOR_LOCK;
			HMI.pg450_RecipeOptionalOrder[i] = SUPERVISOR_LOCK;
		}
	}
}

/*******************************************************************************/
/*                                                                             */
/*******************************************************************************/
void Set_Recipe(recipe_struct *r)
{
   int i, j;
   int rec_mode[3];

   for (i=0; i<3; ++i)
      rec_mode[i] = cfg_super.recipe_mode[i];

   if (cfg_super.layers_by != FALSE)  /* could by true if rec[0]=thickness or wpl or wpa */
      j = 1;
   else
      j = 0;

   for (i=j; i<3; ++i)
   {
      switch(rec_mode[i])                      /* go get recipe value          */
      {
         case WTP:
            UI_RECIPE.pg454_SetRecipeEntry[i]= r->total_wtp;
         break;
         case LTP:
            UI_RECIPE.pg454_SetRecipeEntry[i] = r->ltp;
         break;
         case WPL:
            UI_RECIPE.pg454_SetRecipeEntry[i]=r->wpl;
         break;
         case WPA:
            UI_RECIPE.pg454_SetRecipeEntry[i] = r->wpa;
         break;
         case THICKNESS:           /* note - no wire & cable               */

         switch (cfg_super.application)
         {
            case BLOWN_FILM:
               UI_RECIPE.pg454_SetRecipeEntry[i] = r->thk;
            break;
            case SHEET:
              UI_RECIPE.pg454_SetRecipeEntry[i] = r->thk;
            break;
            case PIPE_AND_TUBING:
               UI_RECIPE.pg454_SetRecipeEntry[i] = r->thk;
             break;
             case WIRE_AND_CABLE:
               UI_RECIPE.pg454_SetRecipeEntry[i] = r->thk;
             break;
         }
         break;
         case OD:
         if (r->od == 0.0)
            r->od = cfg_super.default_od;
         UI_RECIPE.pg454_SetRecipeEntry[i] = r->od;
         break;
         case ID:
         if (r->id == 0.0)
            r->id = cfg_super.default_id;
         switch (cfg_super.application)
         {
              case PIPE_AND_TUBING:
                 UI_RECIPE.pg454_SetRecipeEntry[i] = r->id;
              break;
              case WIRE_AND_CABLE:
                UI_RECIPE.pg454_SetRecipeEntry[i] = r->id;
              break;
         }
         break;
         case WIDTH:
         if (r->width == 0.0)
            r->width = cfg_super.default_width;
         switch (cfg_super.application)
         {
            case BLOWN_FILM:
               UI_RECIPE.pg454_SetRecipeEntry[i] = r->width;
            break;
            case SHEET:
              UI_RECIPE.pg454_SetRecipeEntry[i] = r->width;
            break;
         }
         break;
         default:  /* NONE */
         break;
      }
   }
}
                                                                       
void resin_visibility(void)
{  
   visible(&Resin_HopARuntime, TRUE);
   if (!HMI.HopBRuntime)
      visible(&Resin_HopBRuntime, TRUE);
   else
	  visible(&Resin_HopBRuntime, FALSE);
	if (!HMI.HopCRuntime)
		visible(&Resin_HopCRuntime, TRUE);
	else
		visible(&Resin_HopCRuntime, FALSE);
	if (!HMI.HopDRuntime)
		visible(&Resin_HopDRuntime, TRUE);
	else
		visible(&Resin_HopDRuntime, FALSE);
	if (!HMI.HopERuntime)
		visible(&Resin_HopERuntime, TRUE);
	else
		visible(&Resin_HopERuntime, FALSE);
	if (!HMI.HopFRuntime)
		visible(&Resin_HopFRuntime, TRUE);
	else
		visible(&Resin_HopFRuntime, FALSE);
	if (!HMI.HopGRuntime)
		visible(&Resin_HopGRuntime, TRUE);
	else
		visible(&Resin_HopGRuntime, FALSE);
	if (!HMI.HopHRuntime)
		visible(&Resin_HopHRuntime, TRUE);
	else
		visible(&Resin_HopHRuntime, FALSE);
	if (!HMI.HopIRuntime)
		visible(&Resin_HopIRuntime, TRUE);
	else
		visible(&Resin_HopIRuntime, FALSE);
	if (!HMI.HopJRuntime)
		visible(&Resin_HopJRuntime, TRUE);
	else
		visible(&Resin_HopJRuntime, FALSE);
	if (!HMI.HopKRuntime)
		visible(&Resin_HopKRuntime, TRUE);
	else
		visible(&Resin_HopKRuntime, FALSE);
	if (!HMI.HopLRuntime)
		visible(&Resin_HopLRuntime, TRUE);
	else
		visible(&Resin_HopLRuntime, FALSE); 
}

void Change_Pause_Remote()
{
   /* send PAUSE command to BWH */
   BwhCommand.Command = PAUSE_MODE;
   BwhCommand.PauseAtEndOfBatch = FALSE;   
   BwhCommand.CommandPresent = TRUE;

   /* send PAUSE command to mixer */
   MixerCommand.Command = PAUSE_MODE;
   MixerCommand.PauseAtEndOfBatch = FALSE;
   MixerCommand.CommandPresent = TRUE;

	if (HMI.CurrentPage == pg005_BlendManualBackup1)
	{
		HMI.ChangePage = pg000_MainPage;
		debugFlags.manualPgTest = TRUE;
	}
	Signal.ProcessShutdown = 1;
}

void Change_Manual_Remote()
{
   shr_global.pause_pending = FALSE;
   BwhCommand.CommandPresent = TRUE;
   BwhCommand.Command = MAN_MODE;

   HMI.CurrentPage = pg005_BlendManualBackup1;
   HMI.ChangePage = pg005_BlendManualBackup1;
}

void Change_Auto_Remote()
{
	Recipe_Use(1);
	shr_global.invalid_remote_recipe = validate_recipe(BATCH_RECIPE_NEW, shr_global.recipe_errors);
	
	if (!shr_global.recipe_errors[WEIGH_HOP_LOOP] && !shr_global.recipe_errors[MAX_LOOP]) // no errors
	{
		HMI.pg451_HopperRateSpeed = 3;//G2665 stop popup when Go auto from comms.
		Signal.ProcessAuto = 1;
	}
	
	/*
   calc_recipe(&BATCH_RECIPE_EDIT);
   set_recipe_gate_order(&BATCH_RECIPE_EDIT);
	
   if (!shr_global.recipe_errors[WEIGH_HOP_LOOP] && !shr_global.recipe_errors[MAX_LOOP]) // no errors
   {
      cfg_super.current_temp_recipe_num = 1;
      PMEM.recipe_num = 1;
      BATCH_RECIPE_EDIT.index = 1;
      copy_recipe(&RECIPES->entry[1], &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
      HMI.pg450_StoredRecipeRuntime = 0;
      Signal.StoreRecipes = TRUE;
      
      copy_recipe(BATCH_RECIPE_NEW, &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
      copy_recipe(BATCH_RECIPE_NOW,  BATCH_RECIPE_NEW, BLENDER_RECIPE);

	   copy_recipe(&UI_RECIPE, BATCH_RECIPE_NOW, BLENDER_RECIPE);
      HMI.pg451_HopperRateSpeed = 3;//G2665 stop popup when Go auto from comms.
      Signal.ProcessAuto = 1;
   }
	*/
}

void Change_Recipe_Remote()
{
   copy_recipe(&RECIPES->entry[1], &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
   change_recipe(BATCH_RECIPE_NEW);
}

void Recipe_Save(int index)
{
	int i, j;

	if ((cfg_super.current_security_level >= cfg_super.pe_needed[PE_STORE_RECIPE]) || (index == 1))
	{		
		calc_recipe(&BATCH_RECIPE_EDIT);
		for(i=0; i<cfg_super.total_gates; i++)
		{
			cfg_super.resin_table[BATCH_RECIPE_EDIT.resin_recipe_num[i]].density = BATCH_RECIPE_EDIT.density.s[i];
			
			if (cfg_super.gate_order_in_recipe)
			{
				j = HMI.pg453_Gate_Order_Index[i]-1; // one based
				if (j >= 0) 
               BATCH_RECIPE_EDIT.gate_order[j].number = i;				
			}
		}	
		BATCH_RECIPE_EDIT.index = index;
		BATCH_RECIPE_EDIT.used = 1;
		copy_recipe(&RECIPES->entry[index], &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
		Signal.StoreRecipes = TRUE;
	}
	else // security is not sufficient, read from stored recipes
	{
		copy_recipe(&BATCH_RECIPE_EDIT, &RECIPES->entry[index], BLENDER_RECIPE);                  
	}
}

void Recipe_Use(int index)
{
   int i;
   
	if ((cfg_super.job_wt_in_recipe) && (BATCH_RECIPE_EDIT.job_wt > .01))
      cfg_super.job_wt = BATCH_RECIPE_EDIT.job_wt;

	for (i=0; i<cfg_super.total_gates; i++)
	{
	   if ((cfgGATE[i].ucRegrind3ProxEnabled == TRUE) && (cfgGATE[i].hopper_type == REGRIND) && (BATCH_RECIPE_EDIT.parts.s[i] > 0.0))
			BATCH_RECIPE_EDIT.fRegrindInitialPct = BATCH_RECIPE_EDIT.parts.s[i];

      if (cfg_super.blend_control == 1)
      {
         if ((BATCH_RECIPE_EDIT.parts.s[i] > 0.0)) //G2-401
         {
            visible(&HMI.pg024_load[i], TRUE);     //G2-401
            cfgGATE[i].creceiver.enabled = TRUE;
            //IO_dig[0].channel[55+i] = TRUE;        //LOAD_A_OUT
         }
         else
         {
            visible(&HMI.pg024_load[i], FALSE);    //G2-401
            cfgGATE[i].creceiver.enabled = FALSE;
            //IO_dig[0].channel[55+i] = FALSE;       //LOAD_A_OUT
         }
      }

      //G2-401
      if (cfg_super.blend_control == 2)
      {
         if ((BATCH_RECIPE_EDIT.parts.s[i] > 0.0)) //G2-401
         {
            visible(&HMI.pg024_load[i], TRUE);     //G2-401
            cfgGATE[i].creceiver.enabled = TRUE;
            IO_dig[0].channel[55+i] = TRUE;        //LOAD_A_OUT
         }
         else
         {
            visible(&HMI.pg024_load[i], FALSE);    //G2-401
            cfgGATE[i].creceiver.enabled = FALSE;
            IO_dig[0].channel[55+i] = FALSE;       //LOAD_A_OUT
         }
      }
	}

   copy_recipe(BATCH_RECIPE_NEW, &BATCH_RECIPE_EDIT, BLENDER_RECIPE);
   change_recipe(BATCH_RECIPE_NEW);

	//   recipe name and number on main page	
   brsstrcpy((UDINT)cfg_super.job_name, (UDINT)BATCH_RECIPE_EDIT.product_code);
   //brsstrcpy(PMEM.recipe_name, BATCH_RECIPE_EDIT.product_code);
   cfg_super.current_temp_recipe_num = index;
   //debugFlags.recipeIdx = index;
   PMEM.recipe_num = index;
   HMI.pg450_StoredRecipeRuntime = 0;
   shr_global.recipe_changed = TRUE;
   HMI.pg453_Recipe_Action = RECIPE_NOACTION;
}

void Change_Ext_Pause_Remote()
{
	/* send PAUSE command to EXT */
	if (cfg_super.wtp_mode == CONTROL)
	{
		LiwCommand[0].Command = PAUSE_MODE;
		LiwCommand[0].CommandPresent = TRUE;
	}

	/* send PAUSE command to HO */
	if (cfg_super.ltp_mode == CONTROL)
	{
		HOCommand.Command = PAUSE_MODE;
		HOCommand.CommandPresent = TRUE;
	}

	Signal.StopExtr = 1;
}

void Change_Ext_Manual_Remote()
{
	if (cfg_super.wtp_mode == CONTROL)
	{
		LiwCommand[0].CommandPresent = TRUE;
		LiwCommand[0].Command = MAN_MODE;
	}
	
	if (cfg_super.ltp_mode == CONTROL)
	{
		HOCommand.CommandPresent = TRUE;
		HOCommand.Command = MAN_MODE;
	}
	
	Signal.InitManExtr = 1;
}

void Change_Ext_Auto_Remote()
{
	Recipe_Use(1);
	shr_global.invalid_remote_recipe = validate_recipe(BATCH_RECIPE_NEW, shr_global.recipe_errors);

	if (!shr_global.recipe_errors[EXT_LOOP] && !shr_global.recipe_errors[HO_LOOP])
		Signal.RunExtr = 1;
}

void Change_Ext_Recipe_Remote()
{
	change_recipe(BATCH_RECIPE_NEW);
}

