#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  super.c

   Description:


      $Log:   F:\Software\BR_Guardian\super.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.super to toggle the debug information in this file
 *
 *    Rev 1.10   Jul 01 2008 10:06:10   vidya
 * Added StoreResin();
 *
 *    Rev 1.9   Apr 14 2008 13:36:38   FMK
 * Changed the way the system responds when
 * and IP address has been changed.
 *
 *    Rev 1.8   Apr 11 2008 08:56:20   FMK
 *
 *
 *    Rev 1.7   Mar 26 2008 10:24:54   FMK
 * I don't want the configuration to automatically
 * store on a periodic basis. it could wear out the
 * cf card. It should only store if the configuration
 * changes.
 *
 *    Rev 1.6   Mar 13 2008 12:50:12   vidya
 * Added signal to create recipe file on CF card.
 *
 *    Rev 1.5   Mar 07 2008 10:31:44   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.4   Jan 30 2008 13:54:50   FMK
 * Changed the path for PVCS comments.
 *
 *    Rev 1.3   Jan 18 2008 09:44:34   FMK
 * Changed global variables to new naming structure.
 *
 *    Rev 1.2   Jan 15 2008 15:25:14   FMK
 * Added signal to store_config. The entered setup
 * signal now does not start the store config process.
 * When returned to the main page it will then trigger
 * the store_config signal which will start the config
 * store process.
 *
 *
 *    Rev 1.2   Jan 15 2008 15:23:08   FMK
 *
 *
 *    Rev 1.1   Jan 14 2008 10:56:50   FMK
 * Changed the signal variables to a structure format.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include "globalinc.h"

#include <string.h>
#include "alarm.h"
#include "file_rw.h"
#include "signal_pcc.h"
#include "hmi.h"
#include "constants.h"

#define SET_ALARM(A) {shr_global.alarm_bits|=((A)|shr_global.alarm_force_bits);shr_global.alarm_latch_bits|=((A)|shr_global.alarm_force_bits);}
#define CLEAR_ALARM(A) {shr_global.alarm_bits&=((~(A))|shr_global.alarm_force_bits);}

TON_typ gAlarmTimer;           /* instance variable of TON */
BOOL gAlarmTimerEnable;

TON_typ gVarsTimer;            /* instance variable of TON */
BOOL gVarsTimerEnable;

CfgSetIPAddr_typ gSetIP;              /* instance variable for setting the ip address */
CfgSetSubnetMask_typ gSetSubNet;      /* instance variable for setting the subnet mask */
CfgSetDefaultGateway_typ gSetGateway; /* instance variable for setting the subnet mask */
CfgSetHostNameIf_typ gSetHostnameIf;      //G2-620
CfgGetHostNameIf_typ gGetHostnameIf;      //G2-620

unsigned char gEthDevice[16];

extern void calc_vars(void);
extern void check_alarms(void);

void AlarmCycle(int PresetTime);
void VarsCycle(int PresetTime);
void BuildEthAddress();

unsigned char gProcessAlarms;
unsigned char gProcessVars;
unsigned char gprocess_store_recipe;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/************************************************************************/
/*
   Function:  super_init()

   Description: This function is the INIT function for the super task.

   Returns:  VOID
                                                                        */
/************************************************************************/
_INIT void super_init(void)
{
   gProcessAlarms  = TRUE;
   gProcessVars    = TRUE;
   gAllAlarmsAcked = FALSE;

   Signal.EnteredSetup  = FALSE;
   Signal.RestartSystem = FALSE;
   Signal.ChangeIPAddr  = FALSE;
   Signal.ChangeSubNet  = FALSE;
   Signal.GetHostName   = TRUE; //G2-620
}

/************************************************************************/
/*
   Function:  super_cycle()

   Description: This function is the main cyclic task for the super task.

   Returns:  VOID
                                                                        */
/************************************************************************/
_CYCLIC void super_cycle(void)
{
   int Result; /* Return value of function call */

   if (Signal.SystemLoaded)
   {
      AlarmCycle(200);    /* setup Alarm processing every 200 ms */
      VarsCycle(500);     /* setup VARS processing every 500 ms */

      if (gProcessAlarms) /* process alarms first for remote priority     */
      {
         check_alarms();  /* check on alarms              */
         gProcessAlarms = FALSE;
         gAlarmTimerEnable = TRUE;
      }

      if ( (gProcessVars) ) //&& (Signal.RecipesLoaded == TRUE) ) /* process alarms first for remote priority     */
      {
         calc_vars();
         gProcessVars = FALSE;
         gVarsTimerEnable = TRUE;
      }

      if ( (Signal.EnteredSetup) && (BLN_OPER_MODE == PAUSE_MODE) )
      {
         Signal.EnteredSetup = FALSE;
         Signal.StoreConfig = TRUE;
      }

      if (Signal.GetHostName == TRUE) /* G2-620 */
      { 
         #ifdef PP65
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF5");
         #endif
         #ifdef C70
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
         #endif
         #ifdef C70_003
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
         #endif
         gGetHostnameIf.enable    = 1;
         gGetHostnameIf.pDevice   = (UDINT)&gEthDevice[0];
         gGetHostnameIf.pHostName = (UDINT)&gHostname[0];
         gGetHostnameIf.Len       = (USINT)16;

         CfgGetHostNameIf(&gGetHostnameIf);
         if ((gGetHostnameIf.status == 0) || !(gGetHostnameIf.status == ERR_FUB_BUSY))
         {
            brsstrcpy((UDINT)&cfg_super.Hostname[0], (UDINT)&gHostname[0]);
            Signal.GetHostName = FALSE;
         }
      }

      if (Signal.ChangeHostName == TRUE) /* G2-620 */
      { 
         #ifdef PP65
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF5");
         #endif 
         #ifdef C70
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
         #endif
         #ifdef C70_003
         brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
         #endif
         brsstrcpy((UDINT)&gHostname[0], (UDINT)&cfg_super.Hostname[0]); //G2-620
         gSetHostnameIf.Option    = 1;  /* non volatile */
         gSetHostnameIf.enable    = 1;
         gSetHostnameIf.pDevice   = (UDINT)&gEthDevice[0];
         gSetHostnameIf.pHostName = (UDINT)&gHostname[0];

         CfgSetHostNameIf(&gSetHostnameIf);
         if ((gSetHostnameIf.status == 0) || !(gSetHostnameIf.status == ERR_FUB_BUSY))
         {
            brsstrcpy((UDINT)&cfg_super.device_name[0], (UDINT)&gHostname[0]);
            Signal.ChangeHostName = FALSE;
            Signal.GetHostName = TRUE;
         }
      }

      if (Signal.RestartSystem)
      {
         /* the ip address of the system has changed. Changed the ip address and restart
            the system. Restarting the system will insure that all of the remote communications
            bindings are correctly set. */
         if (Signal.ChangeIPAddr == TRUE)
         {
            #ifdef PP65
            brsstrcpy((UDINT)gEthDevice, (UDINT)"IF5");
            #endif
            #ifdef C70
            brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
            #endif
            #ifdef C70_003
            brsstrcpy((UDINT)gEthDevice, (UDINT)"IF2");
            #endif
            BuildEthAddress();

            if (Signal.ChangeGateway == TRUE) /* this must be changed first */
            {
               gSetGateway.Option = 1;  /* non volatile */
               gSetGateway.enable = 1;
               gSetGateway.pDevice  = (UDINT)&gEthDevice[0];
               gSetGateway.pGateway = (UDINT)&gGateway[0];

               CfgSetDefaultGateway(&gSetGateway);
               if ((gSetGateway.status == 0) || !(gSetGateway.status == ERR_FUB_BUSY))
               {
                  Signal.ChangeGateway = FALSE;
               }
            }
            else if (Signal.ChangeSubNet == TRUE) /* this must be changed first */
            {
               gSetSubNet.Option = 1;  /* non volatile */
               gSetSubNet.enable = 1;
               gSetSubNet.pDevice     = (UDINT)&gEthDevice[0];
               gSetSubNet.pSubnetMask = (UDINT)&gSubNetMask[0];

               CfgSetSubnetMask(&gSetSubNet);
               if ((gSetSubNet.status == 0) || !(gSetSubNet.status == ERR_FUB_BUSY))
               {
                  Signal.ChangeSubNet = FALSE;
               }
            }
            else
            {
               gSetIP.Option = 1;  /* non volatile */
               gSetIP.enable = 1;
               gSetIP.pDevice = (UDINT)&gEthDevice[0];
               gSetIP.pIPAddr = (UDINT)&gIPAddress[0];
               CfgSetIPAddr(&gSetIP);
               if ((gSetIP.status == 0) || !(gSetIP.status == ERR_FUB_BUSY))
               {
                  Signal.ChangeIPAddr = FALSE;
               }
            }
         }
         else
         {
            Signal.EnteredSetup = TRUE;          
            Result = Signal.DataObj_read;
              
            if((Result == FAIL) || (Result == SUCCESS))
            {
               Result = SYSreset(1, 1);    /* perform WARM RESTART */
               Signal.RestartSystem = FALSE; /* make sure signal is reset */
               Signal.FILEIO_INUSE = FALSE;
               Signal.DataObj_read = NOTDONE;
            }
            if (Result != 0)
            {
               /* generate an system error */
            }
         }
      }
   }
}
/************************************************************************/
/*
   Function:  BuildEthAddress()

   Description: This function creates a string representation of the ip
         address and the subnet mask used for ethernent communications.
         Internally these values are stored as 4 characters. This function
         converts this into the string.

   Returns:  VOID
                                                                        */
/************************************************************************/
void BuildEthAddress()
{
	config_super *cs = &cfg_super;   /* pointer to config super structure */
	unsigned char TmpStr[6];

	brsitoa(cs->ip_address[0], (UDINT)&TmpStr);
	brsstrcpy((UDINT) gIPAddress, (UDINT)TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)".");
	brsitoa(cs->ip_address[1], (UDINT)&TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)".");
	brsitoa(cs->ip_address[2], (UDINT)&TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)".");
	brsitoa(cs->ip_address[3], (UDINT)&TmpStr);
	brsstrcat((UDINT) gIPAddress, (UDINT)TmpStr);

	brsitoa(cs->subnet_mask[0], (UDINT)&TmpStr);
	brsstrcpy((UDINT) gSubNetMask, (UDINT)TmpStr);
	brsstrcat((UDINT) gSubNetMask, (UDINT)".");
	brsitoa(cs->subnet_mask[1], (UDINT)&TmpStr);
	brsstrcat((UDINT) gSubNetMask, (UDINT)TmpStr);
	brsstrcat((UDINT) gSubNetMask, (UDINT)".");
	brsitoa(cs->subnet_mask[2], (UDINT)&TmpStr);
	brsstrcat((UDINT) gSubNetMask, (UDINT)TmpStr);
	brsstrcat((UDINT) gSubNetMask, (UDINT)".");
	brsitoa(cs->subnet_mask[3], (UDINT)&TmpStr);
	brsstrcat((UDINT) gSubNetMask,(UDINT)TmpStr);

	brsitoa(cs->gateway[0], (UDINT)&TmpStr);
	brsstrcpy((UDINT) gGateway, (UDINT)TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)".");
	brsitoa(cs->gateway[1], (UDINT)&TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)".");
	brsitoa(cs->gateway[2], (UDINT)&TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)".");
	brsitoa(cs->gateway[3], (UDINT)&TmpStr);
	brsstrcat((UDINT) gGateway, (UDINT)TmpStr);
}
/************************************************************************/
/*
   Function:  AlarmCycle()

   Description: This function handles the timer for the Alarm timer. This
        timer will start when the .IN value is 1 and count in milliseconds
        until the preset time is reached. Then the output .Q will be TRUE.

        The timer will be turned off until the alarms are processed and
        at that time the timer .IN value will be set to TRUE and the
        cycle will repeat.

   Returns:  VOID
                                                                        */
/************************************************************************/
void AlarmCycle(int PresetTime)
{
   /* set input parameters */
   gAlarmTimer.IN = gAlarmTimerEnable;
   gAlarmTimer.PT = PresetTime;

   /* call TON with address of instance variable */
   TON(&gAlarmTimer);

   /* read output parameters */
   if (gAlarmTimer.Q == TRUE)
   {
      gProcessAlarms = TRUE;
      gAlarmTimerEnable = FALSE;

      /* set input parameters */
      gAlarmTimer.IN = gAlarmTimerEnable;

      /* call TON with address of instance variable */
      TON(&gAlarmTimer);
   }
}

/************************************************************************/
/*
   Function:  VarsCycle()

   Description: This function handles the timer for the Vars timer. This
        timer will start when the .IN value is 1 and count in milliseconds
        until the preset time is reached. Then the output .Q will be TRUE.

        The timer will be turned off until the Vars are processed and
        at that time the timer .IN value will be set to TRUE and the
        cycle will repeat.

   Returns:  VOID
                                                                        */
/************************************************************************/
void VarsCycle(int PresetTime)
{
   /* set input parameters */
   gVarsTimer.IN = gVarsTimerEnable;
   gVarsTimer.PT = PresetTime;

   /* call TON with address of instance variable */
   TON(&gVarsTimer);

   /* read output parameters */
   if (gVarsTimer.Q == TRUE)
   {
      gProcessVars = TRUE;
      gVarsTimerEnable = FALSE;

      /* set input parameters */
      gVarsTimer.IN = gVarsTimerEnable;

      /* call TON with address of instance variable */
      TON(&gVarsTimer);
   }
}

