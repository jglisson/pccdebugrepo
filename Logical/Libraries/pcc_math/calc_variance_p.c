#include <stdio.h>
#include "pcc_math.h"

float calc_variance_p(samples *sample_data,float *mean,signed short length)
{
    float avg;
    float err;
    int i;
    float var;
    ordered_pair *d;
    int idx;

    avg = calc_mean_p(sample_data,length);
    if(mean != NULL)
        *mean = avg;

    if(length > sample_data->length) length = sample_data->length;
    d = sample_data->pts;

    idx = sample_data->next - length;
    if(idx < 0)
        idx += sample_data->length;

    var = 0.0;
    for(i=0;i<length;++i)
    {
        err = d[idx].y - avg;
        var = var + err * err;

        if(++idx >= sample_data->length)
            idx -= sample_data->length;
    }
    if(length > 1)
        var = var / (length - 1);
    else
        var = 0.0;

    return var;
 }


