/************************************************************************/
/*
   File:  signal_pcc.h

   Description: This file defines signals that can be used to trigger
      actions across tasks and states within a task.


      $Log:   F:\Software\BR_Guardian\signal.h_v  $
 *
 *    Rev 1.7   Apr 11 2008 08:56:20   FMK
 *
 *
 *    Rev 1.6   Mar 13 2008 12:52:58   vidya
 * Added StoreRecipe and CreateStoreRecipe
 * members to Signal structure.
 *
 *    Rev 1.5   Feb 06 2008 10:42:06   FMK
 * Changed state definition for calibration.
 *
 *    Rev 1.4   Jan 23 2008 14:17:10   FMK
 * Added two signals for saving and retrieving
 * the configuration via user request in setup.
 *
 *    Rev 1.3   Jan 18 2008 09:05:32   FMK
 * Added signal to restart the system.
 *
 *    Rev 1.2   Jan 15 2008 15:23:08   FMK
 * Added signal to store_config. The entered setup
 * signal now does not start the store config process.
 * When returned to the main page it will then trigger
 * the store_config signal which will start the config
 * store process.
 *
 *    Rev 1.1   Jan 14 2008 15:01:10   FMK
 * Added signalling from independent global variables
 * into the signal structure.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/

#ifndef SIGNAL_H
#define SIGNAL_H

#define SIG_NONE           0
#define Sig_Calib_Zero_Bit 1
#define Sig_Calib_Test_Bit 2
#define Sig_Calib_Error    3
#define Sig_Calib_Done     4

struct signal_struct
{
	unsigned char SystemLoaded;  /* Signal to inform the system is now ready for operation when TRUE */
	unsigned char LoadTimeWt;    /* start loading stime wt */
	unsigned char TimeWtLoaded;  /* Signal to inform the system is now ready for operation when TRUE */
	unsigned char RecipesLoaded; /* Signal to inform the system is now ready for operation when TRUE */
	unsigned char DefaultLoaded; /* Signal to show if we loaded the default system */
	unsigned char DefaultSystem; /* Signal to reset system to default values when TRUE */
	unsigned char EnteredSetup;  /* Signal to inform the system that setup was entered when TRUE */
	unsigned char EnteredLicensePage; /* Signal to inform system that a new license key was entered */
	unsigned char Store_Timewt;  /* Signal to inform the Time_wt Table */
	unsigned char Restore_Timewt;/* Signal to inform the Time_wt Table */
	unsigned char Store_Timewt_as_CSV; /* Signal to trigger storing of time weight as csv file */
	unsigned char OpenGate;      /* Signal to task to run the open gate process */ 
	unsigned char StoreGate;     /* HMI gate has changed so store to cfgGATE GTF */
	unsigned char StoreConfig;   /* Signal to store the configuration to the cf card */
	unsigned char ChangeIPAddr;  /* Signal to change the system IP address */
	unsigned char ChangeSubNet;  /* Signal to change the subnet mask */
	unsigned char ChangeGateway; /* Signal to change the Gateway */
	unsigned char ChangeHostName;/* Signal to change the hostname */ //G2-620
	unsigned char GetHostName;   /* Signal to change the hostname */ //G2-620
	unsigned char StoreRecipe;
	unsigned char Calibrate;     /* Signal to start the calibration process */
	unsigned char RestartSystem; /* Signal to perform a restart of the system */
	unsigned char ProcessGateA;
	unsigned char ProcessGateB;
	unsigned char ProcessGateC;
	unsigned char ProcessGateD;
	unsigned char ProcessGateE;
	unsigned char ProcessGateF;
	unsigned char ProcessGateG;
	unsigned char ProcessGateH;
	unsigned char ProcessGateI;
	unsigned char ProcessGateJ;
	unsigned char ProcessGateK;
	unsigned char ProcessGateL;
	unsigned char USB_store;
	unsigned char UserBackupTimeWt;
	unsigned char UserRetrieveConfig;
	unsigned char UserRetrieveSys;
	unsigned char IPLoaded;
	unsigned char shouldRezeroHopperWeight; /* should we call rezero_hopper_wt */
	unsigned char startbatch_rezero;        /* rezero_hopper_wt before first gate GTF*/
	BOOL          doMaterialRequestAlarm;   /* Tim Peyton Special - Flag */
	BOOL          display_timewt;           /* time wt display page - Flag */
	unsigned char StopExtr;
	unsigned char InitManExtr;
	unsigned char RunExtr;
	unsigned char DefaultRecipes;
	unsigned char UserBackupRecipe;
	unsigned char StoreRecipes;
	unsigned char RestoreRecipes;
	unsigned char FILEIO_INUSE;
	unsigned char diag_line_ready;
	unsigned char diag_file_done;
	unsigned char XML_store;
	unsigned char XML_read;
	unsigned char DataObj_read;
	unsigned char Recipe_Special;
	unsigned char ProcessShutdown;
	unsigned char ProcessAuto;
	unsigned char ProcessExtManChange;
	unsigned char Kundig_read;
	unsigned char Kundig_write;
	unsigned char Binary_Store;
	unsigned char Binary_Store_Status;
	unsigned char Binary_Restore;
	unsigned char Binary_Restore_Status;
};

_GLOBAL struct signal_struct Signal;

#endif
