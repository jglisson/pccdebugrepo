/************************************************************************/
/*
   File:  recipe.h

   Description: recipe modes file - used by config to set recipe mode
                based upon application and control mode.
                [0] is application, [1] is control mode, [2] is primary
                recipe entry, [3] is a constant, and [4] is a secondary
                recipe entry or constant. [5] - [8] are used by monitor
                to select data to be shown on the system pages. [5]
                corresponds to sys page 0, [6] to page 1, etc.

      $Log:   F:\Software\B&R_Guardian\recipe.h_v  $
 *
 *    Rev 1.0   Jan 11 2008 10:33:38   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/

#ifndef RECIPE_H
#define RECIPE_H

#include "sys.h"

#define MAXRECIPE_S          MAX_RECIPES     /* parallels MAX_RECIPES */
#define MAXGATE_S            12              /* equals MAX_GATES      */
#define MAXHOPPER_S          (MAXGATE_S+1)   /* equals MAX_GATES      */
#define PRODUCT_CODELEN      16
#define MAXRESIN_S           100             /* equals MAXRESIN_S     */

#define POP_NONE           0
#define POP_INVALIDRECIPE  1
#define POP_CALNOTDONE     2
#define POP_JOBWEIGHT      3
#define POP_RECIPEALARM    4
#define POP_RECIPEJOBNAME  5
#define POP_NEWMATERIAL    6
#define POP_RATEREQUEST    7
#define POP_MIXTIME        8
#define POP_DEFAULTLAYER   9

#define pg450_EditNumField  1
#define pg450_Initialize    2
#define pg450_ChangePage    3
#define pg450_EditParts     4
#define pg450_RecipeUse     5

/* Page 453 switch actions */
#define RECIPE_NOACTION      0
#define RECIPE_SAVE          1
#define RECIPE_USE           2
#define RECIPE_CHANGE        3
#define RECIPE_DELETE        4
#define RECIPE_NEXT_PREV     5
#define RECIPE_DELETE_ALL    6
#define RECIPE_CANCEL        7
#define RECIPE_BACK          8
#define RECIPE_EDIT          9
#define RECIPE_INITIALIZE    10

#define RECIPE_INIT         0
#define RECIPE_CAL_NOT_DONE 1
#define RECIPE_NOT_VALID    2
#define RECIPE_WAS_CHANGED  3
#define RECIPE_SELECT_GATES 4

_GLOBAL int recipe_start_state;

/************************** moved from mem.h *********************************/
typedef struct                  /* "parts" description                  */
{
  float t;                      /* amount of total this "part" is       */
  float ts;                     /* sum of .s subparts                   */
  float s[MAXHOPPER_S];         /* parts for hoppers and refeed         */
  float ext;                    /* parts for ext                        */
                                /* REFEED is end part                   */
} partsdesc_struct;

typedef struct                   /* full address on net of any device    */
{
  unsigned char number;          /* Numeric representation of gate order */
} recipe_order_struct;

typedef struct                   /* recipe                               */
{
  char product_code[PRODUCT_CODELEN];
  int used;                      /* flag set when recipe valid           */
  int   ho_gear_num;             /* haul off gear number */
  float wpl;                     /* desired wt/length for product        */
  float thk;                     /* desired thickness for product        */
  float ltp;                     /* desired hauloff speed (ft/min)       */
  float width;                   /* width of sheet or layflat            */
  float id;                      /* inside diameter for tube/wire        */
  float od;                      /* outside diameter for tube/wire       */
  float ea;                      /* application dependant, total         */
  float avg_density;             /* entire structure avg density         */
  float total_parts;             /* total of parts.ext & parts.s[REFEED] */
  float ho_man_spd;              /* hauloff manual speed                 */
  float ext_man_spd;             /* extruder manual start speed          */
  float refeed_man_spd;          /* extruder manual start speed          */
  float total_wtp;               /* Entire blender wtp                   */
  float ext_wtp;
  float refeed_wtp;
  float max_scrap_parts;         /* Maximum scrap parts allowed          */
  int  mix_time;                 /* mix time in mS GTF */
  float job_wt;
  partsdesc_struct parts;        /* parts - entered by user in recipe    */
  partsdesc_struct parts_pct;    /* parts converted to percents          */
  partsdesc_struct density;      /* solid densities of each ingredient   */
  float wpa;                     /* weight per area                      */
  float step_size;               /* step size for ramping                */
  float recipe_inven_wt;
  float stretch_factor;          /* stretch factor                       */
  short resin_recipe_num[MAXHOPPER_S];   /* either the resin type for this hopper or the recipe number for the remote blender to run */
  /*char clear_gate_cal[MAXGATE_S]; moving this to shrGATE GTF */
  recipe_order_struct gate_order[MAXGATE_S];
  /*fic special[3];                space for per recipe specials      */
  float pg450_RecipeEntry[3];
  float pg454_SetRecipeEntry[3];		       /* move from HMI struct */
  short index;
  float fRegrindInitialPct;
} recipe_struct;

typedef struct
{
   float  InvTotal;
   float  ShftTotal;
   double dInvTotal;
   double dShftTotal;
} pmem_gate_struct;

typedef struct
{
   float InvTotal;
   float InvArea;
   float ShftTotal;
   float ShftArea;
   double dInvTotal;
   double dInvArea;
   double dShftTotal;
   double dShftArea;
} pmem_ho_struct;

typedef struct
{
   float InvTotal;
   float ShftTotal;
   double dInvTotal;
   double dShftTotal;
} pmem_liw_struct;

typedef struct
{
   double last_inven_wt;
   double last_shift_wt;
} pmem_shrGATE_struct;

typedef struct
{
   pmem_gate_struct     Gate[MAXGATE_S];
   pmem_ho_struct       HO; 
   pmem_ho_struct       SHO; 
   pmem_liw_struct      EXT;  
   int                  recipe_num;
   char                 recipe_name[PRODUCT_CODELEN];
   float                recipe_inven[MAXRECIPE_S];
   float                resin_inven[MAXRESIN_S];  
   double               drecipe_inven[MAXRECIPE_S];
   double               dresin_inven[MAXRESIN_S];  
   double               dshr_global_sys_inven_wt;  
   double               dshr_global_sys_shift_wt;  
   double               dshr_global_last_sys_inven_wt;  
   double               dshr_global_last_sys_shift_wt;  
   double               dshr_global_recipe_inven_wt;
   double               dshr_global_recipe_shift_wt;
   double               dshrHO_last_inven_len;  
   double               dshrHO_last_shift_len;  
   double               dshrEXT_last_inven_wt;  
   double               dshrEXT_last_shift_wt; 
   pmem_shrGATE_struct  dshrGATE[MAX_GATES];
} pmem_struct;

typedef struct  
{
   char  structureName[16];
   char  VersionString[16];
} recipe_header_str;

typedef struct  
{
   recipe_header_str header;
   recipe_struct entry[MAXRECIPE_S];

   // add items above the CRC var
   unsigned int CRC16; 
} recipe_book;

_GLOBAL recipe_book   *RECIPES;
_GLOBAL recipe_book   recipes; 
_GLOBAL recipe_struct *BATCH_RECIPE_LAST;
_GLOBAL recipe_struct *BATCH_RECIPE_NEW;
_GLOBAL recipe_struct *BATCH_RECIPE_NOW;
_GLOBAL recipe_struct BATCH_RECIPE_EDIT;
_GLOBAL recipe_struct *EMPTY_RECIPE;
/* ACTUAL_RECIPE in not set to anything in code GTF */
_GLOBAL recipe_struct *ACTUAL_RECIPE;
_GLOBAL recipe_struct *LAST_ACTUAL_RECIPE;
_GLOBAL_RETAIN pmem_struct PMEM; //G2-243
_GLOBAL recipe_struct UI_RECIPE;
_GLOBAL recipe_struct HMI_RECIPE1;
_GLOBAL recipe_struct HMI_RECIPE2;
_GLOBAL recipe_struct HMI_RECIPE3;
_GLOBAL unsigned char Resin_HopARuntime;
_GLOBAL unsigned char Resin_HopBRuntime;
_GLOBAL unsigned char Resin_HopCRuntime;
_GLOBAL unsigned char Resin_HopDRuntime;
_GLOBAL unsigned char Resin_HopERuntime;
_GLOBAL unsigned char Resin_HopFRuntime;
_GLOBAL unsigned char Resin_HopGRuntime;
_GLOBAL unsigned char Resin_HopHRuntime;
_GLOBAL unsigned char Resin_HopIRuntime;
_GLOBAL unsigned char Resin_HopJRuntime;
_GLOBAL unsigned char Resin_HopKRuntime;
_GLOBAL unsigned char Resin_HopLRuntime;
#endif
