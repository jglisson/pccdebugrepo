#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short decr_isample_index(isamples *s_data,signed short idx)
{
    if(--idx < 0) idx = 0;
    return idx;
}


