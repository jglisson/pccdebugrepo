#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  readvar.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\readvar.c_v  $
 *
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.readVar to toggle debug on/off in this file
 *
 *    Rev 1.0   Feb 11 2008 11:02:18   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <math.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "register.h"
#include "remote.h"
#include "libbgdbg.h"

int read_remote_var_2(point_data *pd, unsigned int p,fic *v,int *type);
int identify_user_block(point_data *pd, unsigned int point,unsigned int *block_num,unsigned int *base_point);
int translate_remote_register(point_data *pd, unsigned int * point);
int rem_parse_register(point_data *pd, unsigned int i);
extern int (*fp_read_var_2[4000])(fic *, point_data *, int *); /* YYY */
extern int read_remote_var_2(point_data *pd, unsigned int p,fic *v,int *type);
extern int rv_special_devices(fic * v, point_data * pd, int *type);
extern int rv_special_devices64(fic * v, point_data * pd, int *type);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/*
***************************************************************************
                     READ VAR
***************************************************************************
*/
int read_remote_var_2(point_data *pd, unsigned int p,fic *v,int *type)
{
   int retcode;
   int fn_offset=0;
   char use_fn_tbl = TRUE;
   
   v->i = 0; /* initialize return value */

   /* if return value is < 0 then was reading the block definition */
   /* register number is returned in p                             */
   /* correct value needs to be place in v.i                       */

   if( translate_remote_register(pd,&p) == DEFINED_POINT_REG )
   {
      v->i = (unsigned int)p;
      *type = INT;
      return OK;
   }
 
   shr_global.rem_diag.last_point_processed = p; /* Copy for service diagnostic displays */

   if (p == NOT_USED)
      return (INVALID_POINT);

   retcode = OK;
   
   pd->dev_type = rem_parse_register(pd,p);
   fn_offset = MAX_REG;

   switch (pd->dev_type)
   {
      case TYPE_SYSTEM:
         fn_offset *= SYS_FUNC_OFFSET;
         break;
      case TYPE_GATE:
         fn_offset *= GATE_FUNC_OFFSET;
         break;
      case TYPE_LIW:
         fn_offset *= LIW_FUNC_OFFSET;
         break;
      case TYPE_HO:
         fn_offset *= HO_FUNC_OFFSET;
         break;
      case TYPE_BWH:
         fn_offset *= BWH_FUNC_OFFSET;
         break;
      case TYPE_MIX:
         fn_offset *= MIX_FUNC_OFFSET;
         break;
      case TYPE_WTH:
         fn_offset *= WTH_FUNC_OFFSET;
         break;
      case TYPE_FLF:
         fn_offset *= FLF_FUNC_OFFSET;
         break;
      default:
         use_fn_tbl = FALSE;
         break;
   }

   /* set up return data type */
   if(( pd->reg < REG_INTEGER ) && (pd->reg >= REG_DOUBLE) )
      *type = DOUBLE;
   else if( pd->reg < REG_INTEGER )
      *type = FLOAT;
   else
      *type = INT;

   if(use_fn_tbl)
   {
      if((pd->reg > 0) && (pd->reg <= MAX_REG))
      {
         (*fp_read_var_2[fn_offset + pd->reg])(v, pd, type);
      }
      else
      {
         v->i = 0;
         retcode = INVALID_POINT;
      }
   }
   else
   {
      retcode = rv_special_devices64(v,pd,type);
      retcode = rv_special_devices(v,pd,type);
   }

   return (retcode);
}


/************************************************************************/
/* int  identify_user_block()                                           */
/*      determines if the register is part of a user defined block      */
/*      fills in the block number and base point, returns register type */
/************************************************************************/
int identify_user_block(point_data *pd, unsigned int point,unsigned int *block_num,unsigned int *base_point)
{
   int device;
   unsigned int reg;
   int retcode = NORMAL_REG;

   device = point / PCC2_EXTRUDER_OFFSET;
   reg = (point % PCC2_TYPES_PER) - 1;      /* data element number  */

   switch (device)
   {
      case REM_UCB_DEF_ID:
         *block_num = reg / UCB_SIZE;
         *base_point = reg % UCB_SIZE;    /* there are only 50 registers in cs for now */
         retcode = DEFINED_POINT_REG;
         break;
      case REM_UCB_DATA_ID:
         *block_num = reg / UCB_SIZE;
         *base_point = reg % UCB_SIZE;
         retcode = DEFINED_DATA_REG;
         break;
   }
   return retcode;
}

/****************************************************************************/
/* int translate_remote_register()                                          */
/*      takes block_number and base_point and translates to real register   */
/****************************************************************************/
int translate_remote_register(point_data *pd, unsigned int * point)
{
    config_super *cs = &cfg_super;
    unsigned int block_num;
    unsigned int base_point;
    int retcode = NORMAL_REG;

    switch( (retcode=identify_user_block(pd,*point,&block_num,&base_point)) )
    {
        case DEFINED_DATA_REG:
            *point = cs->user_def_blk[block_num].points[base_point];
            break;
        case DEFINED_POINT_REG:
            *point = cs->user_def_blk[block_num].points[base_point];
            break;
    }
    return retcode;
}

/************************************************************************/
/************************************************************************/
int rem_parse_register(point_data *pd, unsigned int point)
{
   config_super *cs = &cfg_super;
   int device,sub_device;
   int type= TYPE_NONE;

   pd->ext = pd->hop = pd->loop = 0;
   pd->total = FALSE;

   device = point / PCC2_DEVICE_OFFSET;
   sub_device = ((int)point % PCC2_DEVICE_OFFSET)/PCC2_SUBDEVICE_OFFSET;
   pd->reg = point % PCC2_TYPES_PER;      /* data element number  */

   switch (device)
   {
     case 0:
         switch (sub_device)
         {
            case TOTALS_SUBDEVICE:
               if (cs->wtp_mode == UNUSED)
               {
                 type = TYPE_BWH;
                 pd->loop = (int)(WEIGH_HOP_LOOP);
               }
               else
               {
                 type = TYPE_LIW;
                 pd->loop = (int)(EXT_LOOP);
               }
               break;
            case BWH_SUBDEVICE:
               type = TYPE_BWH;
               pd->loop = (int)(WEIGH_HOP_LOOP);
               break;
            case MIXER_SUBDEVICE:
               type = TYPE_MIX;
               pd->loop = (int)(MIXER_LOOP);
               break;
            case REFEED_SUBDEVICE:
               type = TYPE_LIW;
               pd->loop = (int)(GRAVIFLUFF_FDR_LOOP);          /* get liw # for refeed */
               pd->hop = REFEED;
               break;
            default:
               if (sub_device <= MAX_GATES)
               {
                 type = TYPE_GATE;
                 pd->loop = pd->hop = sub_device-1;             /* hopper number*/
               }
               break;
         }
         break;

    case REM_HO_ID:
         switch (sub_device)
         {
            case 0:
              type = TYPE_HO;
              pd->loop = HO_LOOP;
              break;
           case 1:
             type = TYPE_HO;
             pd->loop = SHO_LOOP;
             break;
            default:
              break;
         }

         break;
	  case REM_SHO_ID:
		type = TYPE_HO;
		pd->loop = SHO_LOOP;
		break;
      case REM_WIDTH_ID:
         type = TYPE_WTH;
         pd->loop = WTH_LOOP;
         break;
      case REM_FLUFF_ID:
         type = TYPE_FLF;
         pd->loop = GRAVIFLUFF_LOOP;           /* get liw #            */
         break;
      case REM_UCB_DEF_ID:
         break;
      case REM_UCB_DATA_ID:
         break;
      case REM_RECIPE_DATA_ID:
         type = TYPE_RECIPE_DATA;
         pd->ext = pd->hop = pd->loop = 0;
         break;
      case REM_RECIPE_NAME_ID:
         type = TYPE_RECIPE_NAME;
         pd->ext = pd->hop = pd->loop = 0;
         break;
      case REM_RESIN_DATA_ID:
         type = TYPE_RESIN_DATA;
         pd->ext = pd->hop = pd->loop = 0;
         break;
      case REM_RESIN_NAME_ID:
         pd->ext = pd->hop = pd->loop = 0;
         type = TYPE_RESIN_NAME;
         break;
      case REM_SYSTEM_ID:
         pd->loop = (int)(MAX_LOOP);
         type = TYPE_SYSTEM;
         break;

      default:
         break;
   }
   return(type);
}
