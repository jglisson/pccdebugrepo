#include <stdio.h>
#include <math.h>
#include "pcc_math.h"
#include <AsBrStr.h>
extern int abs(int);

signed short ma_add_pt(struct moving_avg_str *ma_ptr,float data)
{
    register float percent_change;
    float clipped_data;
    float last_clipped;

	if ((ma_ptr == NULL) ||(ma_ptr->num_pts <= 0))
		return 0;

    clipped_data = data;

    /* limit the value being entered if limit_change set and allowable */
    if(ma_ptr->limit_change > 0.0001 && ma_ptr->num_valid >= ma_ptr->num_pts)
    {
        if(ma_ptr->curr_avg > .0001)
            percent_change = (clipped_data - ma_ptr->curr_avg) / ma_ptr->curr_avg;      /* DIVIDE */
        else
            percent_change = 0.0;

        /* need to clip the incoming data value */
        if( fabs((float)percent_change) > ma_ptr->limit_change )
        {
            if(percent_change > 0.0)
            {
                clipped_data = (float)(ma_ptr->curr_avg * ( 1.0 + ma_ptr->limit_change ));
                ++ma_ptr->num_clipped;
            }
            else
            {
                clipped_data = (float)(ma_ptr->curr_avg * ( 1.0 - ma_ptr->limit_change ));
                --ma_ptr->num_clipped;
            }

            if(abs(ma_ptr->num_clipped) >= 3)
            {
                /* reset the moving point average and insert a new avg. */
                last_clipped = ma_ptr->bad_value;
                clear_moving_avg(ma_ptr);
                simple_ma_add_pt(ma_ptr,last_clipped);
                simple_ma_add_pt(ma_ptr,data);
                ma_ptr->num_clipped = 0;
                return(0);                         /* do not allow to continue because variables no longer set-up correctly */
            }
            else
                ma_ptr->bad_value = data;
        }
        else
            ma_ptr->num_clipped = 0;

    }
    else
        ma_ptr->num_clipped = 0;

    simple_ma_add_pt(ma_ptr,clipped_data);

    return(0);
}


