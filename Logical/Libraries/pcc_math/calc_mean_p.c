#include <stdio.h>
#include "pcc_math.h"

float calc_mean_p(samples *sample_data, signed short length)
{
    int i;
    float mean = 0.0;
    ordered_pair *d;
    int idx;

    if(length > sample_data->length) length = sample_data->length;
    d = sample_data->pts;

    idx = sample_data->next - length;
    if(idx < 0)
        idx += sample_data->length;

    mean = 0.0;
    for(i=0;i<length;++i)
    {
        mean = mean + d[idx].y;
        if(++idx >= sample_data->length)
            idx -= sample_data->length;
    }

    if(length > 0)
        mean = mean / (float)length;

    return mean;
}


