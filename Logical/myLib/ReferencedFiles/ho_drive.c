/********************************************************************
 * COPYRIGHT -- Process Control Corporation
 ********************************************************************
 * Package: ReferencedFiles
 * File: ho_drive.c
 * Author: LYang
 * Created: November 22, 2011
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif


 #include <bur/plctypes.h>
#include "plc.h"
#include "relaydef.h"
#include "sys.h"
#include "exd_drive.h"
#include "mem.h"
#include "libbgdbg.h"
#include "hmi.h"

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/* globals we'll need so we can do debug stuff */
extern global_struct *g;  /* the super global */


/* on the current EXD the external clock rate is 4.9152 Mhz. The internal clock
   rate is 1/4 of that = 1.2288 Mhz. This equates to 122.88 ticks/ms. The default
   accel/decel value is 15003 ticks/bit. This equates to 122 ms/bit accel rate.
   This task will run at an increment of 20 ms cycle time. The accel rate needs
   to reflect the cycle time / bit value */

_LOCAL unsigned char gDev_Init;

unsigned char gDriveType;

unsigned int gDefaultAccel;
unsigned int gDefaultDecel;
unsigned int gPulses;
TIME gPulseTime;
unsigned short gDriveInhibitCounter;
unsigned char gRampDir;
unsigned int gRampTime;
TIME gRampStartTime;
unsigned char gRampFlag;
unsigned char gIntExtControl;
unsigned char demo = FALSE;

extern TIME get_time(void);
extern unsigned int elap_time(TIME initial_DTime);

void CardLoop();
void UpdateDriveOutput();
void EmergencyShutdown();
void AdjustSpeed();
int  Check_DriveInh();
void Check_AutoMan();
void ControlDrive();
int  calc_demo_relay(int relay);

shared_HO_device *s;
struct drive_command_struct DriveCommand[MAX_DRIVES];
/************************************************************************/
/*
   Function:  ControlDrive()

   Description:  This function is the main cyclic task for the driver. It
         processes the initial state and the drive commands.
                                                                        */
/************************************************************************/
void ControlDrive()
{
  if (gDev_Init == FALSE)
  {
    gDriveInhibitCounter = 0;
    gCurrentSpeed[HO_DRIVE] = 0;
    gFinalSpeed[HO_DRIVE] = 0;
    gDefaultDecel = 50;
    gDefaultAccel = 50;
    gIntExtControl = Bit_IntDrv_Ctrl;
    gRampDir = RAMPNONE;
    gRampFlag = FALSE;

    UpdateDriveOutput();

    gDev_Init = TRUE;

    if (debugFlags.exdDrive)
      bgDbgInfo (&g_RingBuffer1, "ho_drive.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "drive init'ed\r\n");
   }
   
   DriveCommand[HO_DRIVE].CMD = SCP_SETSPEED;	       /* only adjust speed */

   if (DriveCommand[HO_DRIVE].CMDRdy)
   {
      switch (DriveCommand[HO_DRIVE].CMD)
      {
         case SCP_ID:
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETACCEL:
            gDefaultAccel = DriveCommand[HO_DRIVE].AccelDecelValue;
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETDECEL:
            gDefaultDecel = DriveCommand[HO_DRIVE].AccelDecelValue;
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_SETSPEED:
            gFinalSpeed[HO_DRIVE] = DriveCommand[HO_DRIVE].SetSpeed;
            if (DriveCommand[HO_DRIVE].AccelDecelValue < 0)
            {
               if (gFinalSpeed[HO_DRIVE] < gCurrentSpeed[HO_DRIVE])
                  gRampTime = gDefaultDecel;
               else
                  gRampTime = gDefaultAccel;
            }
            else
	            gRampTime = DriveCommand[HO_DRIVE].RampTime;   /* in seconds */
			  
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_GETSPEED:
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_GETPULSES:
            DriveCommand[HO_DRIVE].Pulses = gPulses;
            DriveCommand[HO_DRIVE].Time = gPulseTime;
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
         case SCP_EMERGENCY:
            EmergencyShutdown();
            DriveCommand[HO_DRIVE].CMDRdy = FALSE;
            DriveCommand[HO_DRIVE].CMDReturn = TRUE;
            DriveCommand[HO_DRIVE].Status  = SCP_ACK;
            break;
      }
   }
   if (cfgHO.drive_type != AB_PLC_DRIVE)
      CardLoop();
   shrHO.act_spd_bits = ANALOG_OUT_HAULOFF;

   if (cfgHO.drive_type != AB_PLC_DRIVE)
      shrHO.act_spd = (float) shrHO.act_spd_bits/OVF_INT;
   
   if (EXT_HO_OPER_MODE == PAUSE_MODE)
     gFinalSpeed[HO_DRIVE] = 0;	
	
}

/************************************************************************/
/*
   Function:  CardLoop()

   Description:  This function applies the correct output algorithm based
         on the drive type. It also monitors key digital inputs.
                                                                        */
/************************************************************************/
void CardLoop()
{
   int Result_DI;

   if (cfgHO.drive_type == PCC_BR_AO)
   {
      if (gFinalSpeed[HO_DRIVE] >= 0)
      {
		   if (gCurrentSpeed[HO_DRIVE] != gFinalSpeed[HO_DRIVE])
            AdjustSpeed();
         else 
            s->ramping = FALSE;
      }
 
      Result_DI = Check_DriveInh();
	   if(shr_global.ext_ho_oper_mode != PAUSE_MODE) /* gext_ho_oper_mode  ? Extruder mode is not Pause (80) GTF */
	   {
         Check_AutoMan();
      }
   }
}
/************************************************************************/
/*
   Function:  Check_AutoMan()

   Description:  This function looks at the auto manual input relay for
         the device defined by HO_DRIVE and sets the appropriate status
         bit in the device specific status word.

         if auto/manual bit is low then in auto.
         if auto/manual bit is high then in manual
                                                                        */
/************************************************************************/
void Check_AutoMan()
{
   unsigned short RelayState;

   RelayState = (IO_dig[X20].channel[AutoMan_HO]);

   /* if input is high then auto light on panel is lit, so then in automatic,
      clear the auto/manual bit to signal in auto */
   if (RelayState == TRUE)
   {
      /* drive in auto */
      DriveCommand[HO_DRIVE].IOStatus &= ~SCP_BIT_DriveMan;
      IO_dig[X20].channel[HOManual_Out_0] = ON;
      IO_dig[X20].channel[HOManual_Out_1] = ON;
   }
   else
   {
      /* drive in manual */
      DriveCommand[HO_DRIVE].IOStatus |= SCP_BIT_DriveMan;
      IO_dig[X20].channel[HOManual_Out_0] = OFF;
      IO_dig[X20].channel[HOManual_Out_1] = OFF;
   }
}
/************************************************************************/
/*
   Function:  Check_DriveInh()

   Description:  This function looks at the drive inhibit input relay for
         the device defined by HO_DRIVE and set the appropriate status bit
         in the device specific status word. It will also if inhibited
         reset the drive output to zero.  Because this can be a dangerous
         situation with noise spikes, the drive inhibit must be tested
         at least five times before triggerine the actual drive inhibit.
                                                                        */
/************************************************************************/
int Check_DriveInh()
{
   unsigned short RelayState;
   int Result;

   Result = FALSE;
   RelayState = (IO_dig[X20].channel[DriveInh_HO]);

   if (RelayState == TRUE)
   {
      if (gDriveInhibitCounter <= DRIVEINH_TIMEOUT)
         gDriveInhibitCounter++;
   }
   else
      gDriveInhibitCounter = 0;

   if (gDriveInhibitCounter > DRIVEINH_TIMEOUT)
   {
      DriveCommand[HO_DRIVE].IOStatus |= SCP_BIT_DriveInh;
      Result = TRUE;
   }
   else
      DriveCommand[HO_DRIVE].IOStatus &= ~SCP_BIT_DriveInh;

   return(Result);
}
/************************************************************************/
/*
   Function:  AdjustSpeed()

   Description:  This function looks at the current and final speed values
         and appropriately sets the output in the correct direction. A
         new ramp start time is also retrieved so the calucation for the
         next bit change can be accurately made.
                                                                        */
/************************************************************************/
void AdjustSpeed()
{
	USINT AccelOutputTolerance;
	
	if (DriveCommand[HO_DRIVE].AccelDecelValue <= 0.001)    
		DriveCommand[HO_DRIVE].AccelDecelValue = 0.02;
	
	AccelOutputTolerance = 16 * DriveCommand[HO_DRIVE].AccelDecelValue * 100;
	s->ramping = gCurrentSpeed[HO_DRIVE] != gFinalSpeed[HO_DRIVE];

	if ((gCurrentSpeed[HO_DRIVE] > gFinalSpeed[HO_DRIVE] - AccelOutputTolerance) && 
		(gCurrentSpeed[HO_DRIVE] < gFinalSpeed[HO_DRIVE] + AccelOutputTolerance))
	{
		gCurrentSpeed[HO_DRIVE] = gFinalSpeed[HO_DRIVE];  
	}
	else
	{
		if (gCurrentSpeed[HO_DRIVE] < (gFinalSpeed[HO_DRIVE]))
		{
			if (gCurrentSpeed[HO_DRIVE] <= OVF_INT-32)
				gCurrentSpeed[HO_DRIVE] += AccelOutputTolerance;   /* default, increment by 0.1% speed per 50ms namely 2%/s */
			else
				gCurrentSpeed[HO_DRIVE] = OVF_INT;
		}
		else if (gCurrentSpeed[HO_DRIVE] > gFinalSpeed[HO_DRIVE])
		{
			if (gCurrentSpeed[HO_DRIVE] >= 32)
				gCurrentSpeed[HO_DRIVE] -= AccelOutputTolerance;   /* decrement by 0.1% speed */
			else
				gCurrentSpeed[HO_DRIVE] = 0;
		}
	}
   
	if  ((gFinalSpeed[HO_DRIVE] == 0) && (gCurrentSpeed[HO_DRIVE] <= AccelOutputTolerance))
		gCurrentSpeed[HO_DRIVE] = 0;
   
	UpdateDriveOutput();
}

/************************************************************************/
/*
   Function:  EmergencyShutdown()

   Description:  This function retruns the system to a know initial safe
         state. the outputs that were turned on are reset and the speed
         values are immediately returned to zero as well.
                                                                        */
/************************************************************************/
void EmergencyShutdown()
{
   gCurrentSpeed[HO_DRIVE] = 0;
   gFinalSpeed[HO_DRIVE] = 0;
   gRampDir = RAMPNONE;
   gRampFlag = FALSE;

   UpdateDriveOutput();

   IO_dig[X20].channel[EXTManual_Out_0] = OFF;
   IO_dig[X20].channel[EXTManual_Out_1] = OFF;
   IO_dig[X20].channel[HOManual_Out_0] = OFF;
   IO_dig[X20].channel[HOManual_Out_1] = OFF;
}

/************************************************************************/
/*
   Function:  UpdateDriveOutput()

   Description:  This function set the analog output on the x20 output
         module. The HO_DRIVE variable is used to determine which output to
         set.
                                                                        */
/************************************************************************/
void UpdateDriveOutput()
{
	if (cfg_super.ltp_mode == CONTROL) 
	{
   	ANALOG_OUT_HAULOFF = gCurrentSpeed[HO_DRIVE];
	}
}

/************************************************************************/
/*
   Function:  calc_demo_relay(void)

   Description:


        Returns:  VOID
                                                                        */
/************************************************************************/
int calc_demo_relay(int relay)
{
  return(FALSE);
}


