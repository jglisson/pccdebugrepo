#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short clear_isamples(isamples *s_data)
{
    ordered_ipair *data;
    int i;

    s_data->next = 0;

    if(s_data->num_taken > 0)
    {
        data = s_data->pts;
        for(i=0;i<s_data->length;++i)
        {
            data->x = 0;
            data->y = 0;
            ++data;
        }
    }
    s_data->num_taken = 0;

    return(0);
}


