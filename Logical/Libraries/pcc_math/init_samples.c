#include "pcc_math.h"

signed short init_samples(samples *s_data,signed short length,ordered_pair *d)
{
    s_data->length = length;
    s_data->pts = d;
    s_data->num_taken = length;  /* force clear samples to zero out data */
    clear_samples(s_data);

    return(0);

}


