#include<bur/plc.h>
#include <sys_lib.h>
#include <astime.h>

DATE_AND_TIME get_date_time(void);
TIME get_time(void);
TIME start_time(void);
unsigned int elap_time(TIME iTime);
unsigned int elapsed_time(TIME Time1,TIME Time2);


