#ifdef ALLEN_BRADLEY_DRIVE
#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************
 * FILE:        plc_exd.c
 *
 * DESCRIPTION: This file is for PLC EXD
 * 2008/12/01  JLR
 *   added/changed debug telnet stuff
 *   use debugFlags.plcExd to toggle debug information in this file
 *
 *
 *  Process Control Corp,   Atlanta, GA
 ************************************************************************/
/*#include "globalinc.h"*/
#include <stdio.h>
//#include <stdlib.h>
#include <astime.h>
#include <string.h>
#include <timer.h>
#include <math.h>
#include <ethsock.h>
#include "sys.h"
#include "mem.h"
#include "libbgdbg.h"
#include "remote.h"
#include "hmi.h"

/*#include <Global/timer.c>*/

#define ERROR_NET -1
#define REG_SESSION_CMD       0x65
#define REG_SESSION_LENGTH    4
#define SendRRData_CMD        0x6f
#define SendUnitData_CMD      0x70
#define FWD_OPEN_REQUEST      0x54
#define PCCC_SERVICE_REQUEST  0x4b

#define ENCAP_HEADER_LENGTH   24
#define RESPONSE_TIMEOUT      3000   /*  (1 sec: 1 tick = 10ms) */
#define PLC_5_READ   0x68
#define PLC_5_WRITE  0x67
#define INT             0
#define FLOAT           1

#define PLC_FLOAT_DATA  0x08
#define PLC_INT_DATA    0x04
#define ARRAY_TYPE      9

#define DEBUG_PLC_EXD 1

void OpenRemoteSocketPLC(int PortVal, unsigned long HostAddr, unsigned char unit_id, int chan);
void RecvRegSessionResponse(int chan);
void SendRegSessionCommand(int chan);
void SendRRData(int chan);
void RecvSendRRDataResponse(int chan);
void SendUnitDataCommand(int chan, int FNC, short FileNum, short RegNum, short ElementSize, int DataType);
void RecvSendUnitDataResponse(int chan, int FNC);
void ClosePLCRemoteSocket(void);
void BuildPLCEthAddress();

int  errno;

char connectedPLC;
char ConnectedPLC_Flag;

unsigned char inbfr[256], outbfr[256], session_handle[4], O_T_CONN_ID[4], T_O_CONN_ID[4];
int counter = 1;
extern global_struct *g;
extern config_super *cs_;
short SendUnitData_Serial_Number=0, SendUnitData_TNS=0;
int   PLC_WRITTEN_INT_DATA[10], PLC_READ_INT_DATA[10];
float PLC_WRITTEN_FLOAT_DATA[10], PLC_READ_FLOAT_DATA[10];

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;
int  SocketNewPLC, ethnetpnPLC;
UINT EthError;
//_GLOBAL UINT State;									
/************************************************************************/
/***********************************************************************/

void OpenRemoteSocketPLC(int PortVal, unsigned long HostAddr, unsigned char unit_id, int chan)
{
    char bad = TRUE;
    struct ETH_sockaddr_typ ls_addr;

    connectedPLC = FALSE;
    brsmemset((UDINT)&ls_addr, 0, sizeof(ls_addr));
    brsmemcpy((UDINT)&ls_addr.sin_addr, (UDINT)&HostAddr, (UDINT)sizeof(HostAddr));   
 
    ls_addr.sin_family = ETH_AF_INET;
    ls_addr.sin_addr = H_TO_NUDINT(HostAddr);
    ls_addr.sin_port = N_TO_HUINT(PortVal); 
        
    bad = ((SocketNewPLC = socket(ETH_AF_INET, ETH_SOCK_STREAM, 0)) == ERROR_NET);
    
    EthError = EthGetError(); 

    if (bad)
    {
    /*    return (ERROR);*/
        return;
    }

     
   bad = (connect(SocketNewPLC, (UDINT)(struct sockaddr *)&ls_addr, sizeof(ls_addr)) == -1);

   if (bad)
   {
     if (errno == ERR_ETH_WOULDBLOCK)
     {
       bad = FALSE;
     }
     //ClosePLCRemoteSocket();*/
   }

   if (!bad)
      connectedPLC = TRUE;

   if (bad)
   {
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
      /*return(ERROR);*/
      return;
   }
   else
   {
      ethnetpnPLC = SocketNewPLC;
      /*return(NOERROR);*/
      return;
   }
   return;
}

/************************************************************************/
/************************************************************************/
void SendRegSessionCommand(int chan)
{
   DINT i, length;
   char bad = TRUE;
   unsigned int ResponseTimer;

   ResponseTimer = start_time();

   for (i=0; i< ENCAP_HEADER_LENGTH; i++)
      outbfr[i] = 0;

   outbfr[0] = REG_SESSION_CMD;     /* Encap CMD */
   outbfr[2] = REG_SESSION_LENGTH;  /* length of Encap data */

   i = ENCAP_HEADER_LENGTH;

   outbfr[i++] = 0x01;
   outbfr[i++] = 0x00;
   outbfr[i++] = 0x00;
   outbfr[i++] = 0x00;

   length = i;

   while (bad)
   {
      bad = (send(ethnetpnPLC, (UDINT)outbfr, length, 0) != length);
         if (elap_time(ResponseTimer) > RESPONSE_TIMEOUT)
         {
            bad = TRUE;
            break;
         }
   /*   tsleep(3);*/
   }

   if (bad)
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
   }
   else
   {
      shr_global.stats[chan+32].good_msgs++;
      shr_global.stats[chan+32].alive = TRUE;
   }
}

/************************************************************************/
/************************************************************************/
void RecvRegSessionResponse(int chan)
{
   int i, length;
   char bad=TRUE;
   unsigned int ResponseTimer;

   ResponseTimer = start_time();

   while (bad)
   {
      length = recv(ethnetpnPLC, (UDINT)inbfr, sizeof(inbfr), 0);
      if (length < 0)
         bad = TRUE;
      else
         bad = FALSE;

      if (elap_time(ResponseTimer) > RESPONSE_TIMEOUT)
      {
         bad = TRUE;
         break;
      }
     /* tsleep(3);*/
   }

   if (bad)
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
   }
   else
   {
      for (i = 4; i < 8; i++)
         session_handle[i-4] = inbfr[i];
      shr_global.stats[chan+32].good_msgs++;
      shr_global.stats[chan+32].alive = TRUE;
   }
}

/************************************************************************/
/************************************************************************/
void SendRRData(int chan)
{
   int i;
   int length;
   char bad=TRUE;
   unsigned int ResponseTimer;

   ResponseTimer = start_time();

   outbfr[0] = SendRRData_CMD;

   /* length of encap data */
   outbfr[2] = 0x3e;

   length = 4;

   /* session handle */
   for (i = 4; i < 8; i++)
      outbfr[length++] = session_handle[i - 4];

   /* status */
   for (i=0; i<4;i++)
      outbfr[length++] = 0x00;

   /* sender context */
   for (i = 0; i < 4; i++)
      outbfr[length++] = (unsigned char)cfg_super.ip_address[i];
   outbfr[length++] = chan-1;   /* 0x00;*/
   outbfr[length++] = 0x00;
   outbfr[length++] = 0xb1;
   outbfr[length++] = 0x53;

   length = ENCAP_HEADER_LENGTH;

   for (i=0; i<4; i++)      /* CIP protocol -- 4 bytes */
      outbfr[length++] = 0;

   /* timeout */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x04;

   /* item count */
   outbfr[length++] = 0x02;
   outbfr[length++] = 0x00;

   /* Addr ID */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;

   /* Addr length */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;

   /* Data ID */
   outbfr[length++] = 0xb2;
   outbfr[length++] = 0x00;

   /* Data Length */
   outbfr[length++] = 0x2e;
   outbfr[length++] = 0x00;

   /* T_PDU */
   /*Service code */
   outbfr[length++] = FWD_OPEN_REQUEST;

   /* size of request path */
   outbfr[length++] = 0x02;

   /* Request path */
   outbfr[length++] = 0x20;
   outbfr[length++] = 0x06;
   outbfr[length++] = 0x24;
   outbfr[length++] = 0x01;

   /* connection pariority/tick time */
   outbfr[length++] = 0x0a;

   /* connection timeout ticks */
   outbfr[length++] = 0x0e;

   /* O-T Conn ID , this will be set from response*/
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;

   /* T-O Conn ID, don't have to be unique */
   outbfr[length++] = T_O_CONN_ID[0] = 0xe8;
   outbfr[length++] = T_O_CONN_ID[1] = 0x89;
   outbfr[length++] = T_O_CONN_ID[2] = 0x90;
   outbfr[length++] = T_O_CONN_ID[3] = (chan-1)*10;


   /* Conn S/N , hass to be unique */
   outbfr[length++] = 0x12;
   outbfr[length++] = 0x34 + chan-1;

   /* Originator Vendor ID */
   outbfr[length++] = 0x01;
   outbfr[length++] = 0x00;

   /* originator S/N (CIP serial number) */
   for (i=0; i< 4; i++)
      /*outbfr[length++] = shr_global.mac_address[i+2];*/
       outbfr[length++] = HostMacAddr[i+2];
       
   /* Conn Timeout Multiplier */
   outbfr[length++] = 0x01;

   /* Reserved */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;

   /* O-T RPI */
   outbfr[length++] = 0x40;
   outbfr[length++] = 0x82;
   outbfr[length++] = 0x1f;
   outbfr[length++] = 0x00;

   /* O-T connection parameters */
   outbfr[length++] = 0x02;
   outbfr[length++] = 0x43;

   /* T-O RPI */
   outbfr[length++] = 0x40;
   outbfr[length++] = 0x82;
   outbfr[length++] = 0x1f;
   outbfr[length++] = 0x00;

   /* T-O connection parameters */
   outbfr[length++] = 0x02;
   outbfr[length++] = 0x43+(chan-1);   /* Number has to be positive */

  /* Transport Class */
  outbfr[length++] = 0xa3;

  /* Size of connection path */
  outbfr[length++] = 0x02;

  /* Connection Path */
  outbfr[length++] = 0x20;
  outbfr[length++] = 0x02;
  outbfr[length++] = 0x24;
  outbfr[length++] = 0x01;

   while (bad)
   {
      bad = (send(ethnetpnPLC, (UDINT)outbfr, length, 0) != length);
         if (elap_time(ResponseTimer) > RESPONSE_TIMEOUT)
         {
            bad = TRUE;
            break;
         }
    /*  tsleep(3);*/
   }

   if (bad)
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
   }
   else
   {
      shr_global.stats[chan+32].good_msgs++;
      shr_global.stats[chan+32].alive = TRUE;
   }
}

/************************************************************************/
/************************************************************************/
void RecvSendRRDataResponse(int chan)
{
   int  i, j, length;
   char bad = TRUE;
   unsigned int ResponseTimer;

   ResponseTimer = start_time();

   while (bad)
   {
      length = recv(ethnetpnPLC, (UDINT)inbfr, sizeof(inbfr), 0);
      if (length < 0)
         bad = TRUE;
      else
         bad = FALSE;

      if (elap_time(ResponseTimer) > RESPONSE_TIMEOUT)
      {
         bad = TRUE;
         break;
      }
      /*tsleep(3);*/
   }

   if (!bad)
   {
      /* make sure this is the return for SendRRData */
      if (inbfr[0] != SendRRData_CMD)
      {
         bad = TRUE;
      }
      /* check session handle if match */

      for (i = 0; i< 4; i++)
      {
         if (session_handle[i] != inbfr[i + 4])
         {
            bad = TRUE;
            break;
         }
      }

      /* check status */
      for (i = 8; i < 12; i++)
      {
         if (inbfr[i] != 0)
         {
            bad = TRUE;
            break;
         }
      }

      /* make sure this is response to Fwd_Open_Request */
      if (inbfr[40] != 0xd4)
      {
         bad = TRUE;
      }
      if (bad)
      {
         shr_global.stats[chan+32].timeouts++;
         shr_global.stats[chan+32].alive = FALSE;
      }
      else
      {
         /* to get O-T Conn ID */
         j = 0;
         for (i=44; i<48; i++)
         {
            O_T_CONN_ID[j] = inbfr[i];
            j++;
         }
         shr_global.stats[chan+32].good_msgs++;
         shr_global.stats[chan+32].alive = TRUE;
      }
   }
   else
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
   }
}

/************************************************************************/
/************************************************************************/
void SendUnitDataCommand(int chan, int FNC, short FileNum, short RegNum, short ElementSize, int DataType)
{
   int  i;
   int  length;
   char bad = TRUE;
   unsigned int ResponseTimer;
   unsigned char int_str[2]; //float_str[4];
   unsigned int ind_data_size, whole_data_size;
   unsigned char f[4];
   int EncapLength, b1_data_length, df1_data_type_length ;

   ResponseTimer = start_time();

   outbfr[0] = SendUnitData_CMD;

   /* length of encap data */ /* need to be variable for WRITE */
   outbfr[2] = 0x32;

   length = 4;

   /* session handle */
   for (i=4; i<8; i++)
      outbfr[length++] = session_handle[i - 4];

   /* status(4), sender context(8), Option(4), CIP Protocol(4)*/
   for (i=0; i<20; i++)
      outbfr[length++] = 0x00;

   /* timeout */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x04;

   /* Item count */
   outbfr[length++] = 0x02;
   outbfr[length++] = 0x00;

   /* Address ID */
   outbfr[length++] = 0xa1;
   outbfr[length++] = 0x00;

   /* Address length */
   outbfr[length++] = 0x04;
   outbfr[length++] = 0x00;

   for (i = 0; i < 4; i++)
      outbfr[length++] = O_T_CONN_ID[i];

   /* Data ID */
   outbfr[length++] = 0xb1;
   outbfr[length++] = 0x00;

   /* Data length */  /* !!!! need to be variable for WRITE */
   outbfr[length++] = 0x1e;
   outbfr[length++] = 0x00;

   /* S/N */
   if (SendUnitData_Serial_Number++ == 0)
      SendUnitData_Serial_Number = 1;
   brsmemcpy((UDINT)&int_str[0], (UDINT)&SendUnitData_Serial_Number, 2);
   outbfr[length++] = int_str[0];   /* need to swap order */
   outbfr[length++] = int_str[1];

   /* Execute PCCC Service request */
   outbfr[length++] = PCCC_SERVICE_REQUEST;

   /* size of request path */
   outbfr[length++] = 0x02;

   /* Request path */
   outbfr[length++] = 0x20;
   outbfr[length++] = 0x67;
   outbfr[length++] = 0x24;
   outbfr[length++] = 0x01;

   /* Length of Request ID */
   outbfr[length++] = 0x07;

   /* CIP Vendor ID */
   outbfr[length++] = 0x01;
   outbfr[length++] = 0x00;

   /* CIP serial number */
   for (i=0; i<4; i++)
      outbfr[length++] = HostMacAddr[i+2];

   /* CMD */
   outbfr[length++] = 0x0f;

   /* STS */
   outbfr[length++] = 0x00;

   /* TNS */
   if (SendUnitData_TNS++ == 0)
      SendUnitData_TNS = 1;
   brsmemcpy((UDINT)&int_str[0], (UDINT)&SendUnitData_TNS, 2);
   outbfr[length++] = int_str[0];   /* need to swap order */
   outbfr[length++] = int_str[1];

   /* FNC */
   outbfr[length++] = FNC;

   /* packet offset */
   outbfr[length++] = 0x00;
   outbfr[length++] = 0x00;

  /* total trans */
   brsmemcpy((UDINT)&int_str[0], (UDINT)&ElementSize, 2);
   outbfr[length++] = int_str[0];   /* need to swap order */
   outbfr[length++] = int_str[1];

  /* plc-5 system address */
  outbfr[length++] = 0x07;
  outbfr[length++] = 0x00;
  if (FileNum > 255)
  {
      outbfr[length++] = 0xff;
      brsmemcpy((UDINT)&int_str[0], (UDINT)&FileNum, 2);
      outbfr[length++] = int_str[0];
      outbfr[length++] = int_str[1];
      df1_data_type_length = 7; /* include 99, 09 */
  }
  else
  {
    outbfr[length++] = FileNum;
    df1_data_type_length = 5; /* include 99, 09 */
  }
  outbfr[length++] = RegNum;

   if (FNC == PLC_5_READ) /* if PLC 5 READ, then followed by Elements size */
   {
     /* total trans */
      brsmemcpy((UDINT)&int_str[0], (UDINT)&ElementSize, 2);
      outbfr[length++] = int_str[0];   /* need to swap order */
      outbfr[length++] = int_str[1];
   }
   else  /* PLC_5_WRITE, followed by A + B, which is Data TYPE ID and SIZE */
   {
      if (DataType == INT)
         ind_data_size = 2;
      else if (DataType == FLOAT)
         ind_data_size = 4;

      whole_data_size = ElementSize * ind_data_size;
      if (whole_data_size < 256)
      {
         outbfr[length++] = 0x99;
         outbfr[length++] = ARRAY_TYPE;
         if (DataType == FLOAT)
         {
            whole_data_size += 2;
            outbfr[length++] = (unsigned char)(whole_data_size);
         }
         else
         {
            whole_data_size += 1;
            outbfr[length++] = (unsigned char)(whole_data_size);
         }
      }
      else
      {
         outbfr[length++] = 0x9a;
         outbfr[length++] = ARRAY_TYPE;

         whole_data_size += 3;
         outbfr[length++] = (unsigned char)(whole_data_size);
         outbfr[length++] = (unsigned char)((whole_data_size) >> 8);
      }

      if (DataType == FLOAT)
      {
         outbfr[length++] = 0x94;
         outbfr[length++] = PLC_FLOAT_DATA;
      }
      else
         outbfr[length++] = 0x42;

      for (i = 0; i < ElementSize; i++)
      {
         if (DataType == INT)
         {
            outbfr[length++] = (unsigned char)(PLC_WRITTEN_INT_DATA[i] & 0x00ff);
            outbfr[length++] = (unsigned char)((PLC_WRITTEN_INT_DATA[i] & 0xff00) >> 8);
         }
         else
         {
            brsmemcpy((UDINT)f, (UDINT)&PLC_WRITTEN_FLOAT_DATA[i], 4);
            outbfr[length++] = f[0];
            outbfr[length++] = f[1];
            outbfr[length++] = f[2];
            outbfr[length++] = f[3];
         }
      }

      /* to modify the Encap length and Data Item Length */
      b1_data_length = 24 + df1_data_type_length + whole_data_size + 2;
      outbfr[42] = (unsigned char)(b1_data_length & 0x00ff);
      outbfr[43] = (unsigned char)((b1_data_length & 0xff00) >> 8);

      EncapLength = 20 + b1_data_length;
      outbfr[2] = (unsigned char)(EncapLength & 0x00ff);
      outbfr[3] = (unsigned char)((EncapLength & 0xff00) >> 8);
   }

   while (bad)
   {
      bad = (send(ethnetpnPLC, (UDINT)outbfr, length, 0) != length);
      if (elap_time(ResponseTimer) > (unsigned int)RESPONSE_TIMEOUT)
      {
         bad = TRUE;
         break;
      }
     /* tsleep(3);*/
   }

   if (bad)
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
/*       printf("SendUnitDataCommand: chan = %d timeouts=%d alive=%d\n",chan,shr_global.stats[chan+32].timeouts,shr_global.stats[chan+32].alive); */
   }
   else
   {
      shr_global.stats[chan+32].good_msgs++;
      shr_global.stats[chan+32].alive = TRUE;
   }
}

/************************************************************************/
/************************************************************************/
void RecvSendUnitDataResponse(int chan, int FNC)
{
   int i, length, index;
   char bad = TRUE;
   unsigned int ResponseTimer;
   unsigned char Flag_Byte, Next_Byte;
   int whole_data_type, whole_data_size, ind_data_type, ind_data_size, data_type_offset, data_size_offset, next_byte_pos;
   int data_start_pos;
   int int_data[10];
   float float_data[10];
   char f[4];
   char *ptr;

   ResponseTimer = start_time();

   while (bad)
   {
      length = recv(ethnetpnPLC, (UDINT)inbfr, sizeof(inbfr), 0);
      if (length < 0)
         bad = TRUE;
      else
         bad = FALSE;

      if (elap_time(ResponseTimer) > (unsigned int)RESPONSE_TIMEOUT)
      {
         bad = TRUE;
         break;
      }
      /*tsleep(3);*/
   }

   if (!bad)
   {
      /* make sure this is the return for SendRRData */
      if (inbfr[0] != SendUnitData_CMD)
      {
         bad = TRUE;
      }
      /* check session handle if match */

      for (i = 0; i< 4; i++)
      {
         if (session_handle[i] != inbfr[i + 4])
         {
            bad = TRUE;
            break;
         }
      }

      /* check status */
      for (i = 8; i < 12; i++)
      {
         if (inbfr[i] != 0)
         {
            bad = TRUE;
            break;
         }
      }

      /* check O_T CID */
      for (i = 36; i< 40; i++)
      {
         if (T_O_CONN_ID[i-36] != inbfr[i])
         {
            bad = TRUE;
            break;
         }
      }

      /* make sure this is response to SendUnitData */
      if (inbfr[46] != 0xcb)
         bad = TRUE;

      /* check status */
      for (i = 47; i < 50; i++)
      {
         if (inbfr[i] != 0)
         {
            bad = TRUE;
            break;
         }
      }

      /* check if reply of 0x0f */
      if (inbfr[57] != 0x4f && inbfr[58] != 0)
         bad = TRUE;

      if (bad)
      {
         shr_global.stats[chan+32].alive = FALSE;
         shr_global.stats[chan+32].timeouts++;
      }
      else
      {
         if (FNC == PLC_5_READ)
         {
            /* inbfr[61] is the Flag Byte */
            index = 61;
            Flag_Byte = inbfr[index];

            /* figure out data type */
            if ((Flag_Byte & 0xf0) > 0x70)
            {
               data_type_offset = (Flag_Byte & 0x70) >> 4;
               whole_data_type = inbfr[index + data_type_offset];
               next_byte_pos = index + data_type_offset + 1;
            }
            else
            {
               whole_data_type = (Flag_Byte & 0x70) >> 4;
               next_byte_pos = index + 1;
            }

            /* figure out data size */
            if ((Flag_Byte & 0x0f) > 0x07)
            {
               data_size_offset = Flag_Byte & 0x07;
               whole_data_size = inbfr[index + data_type_offset + data_size_offset];
               next_byte_pos += data_size_offset;
            }
            else
            {
               whole_data_size = Flag_Byte & 0x07;
               next_byte_pos += 1;
            }

            Next_Byte = inbfr[next_byte_pos];

            if ((Next_Byte & 0xf0) > 0x70)
            {
               ind_data_type = inbfr[next_byte_pos + ((Next_Byte & 0x70) >> 4)];
               data_start_pos = next_byte_pos + ((Next_Byte & 0x70) >> 4) + 1;
            }
            else
            {
               ind_data_type = (Next_Byte & 0xf0) >> 4;
               data_start_pos = next_byte_pos + 1;
            }

            ind_data_size = (Next_Byte & 0x0f);

            /* now we can retrieve the data */
            if (ind_data_type == PLC_INT_DATA)
            {
               ptr = &inbfr[data_start_pos];

               for (i = 0; i < whole_data_size / ind_data_size; i++)
               {
	               int_data[i] =  (unsigned char)*(ptr++);
	               int_data[i] |= (unsigned char)*ptr++ << 8;
                  PLC_READ_INT_DATA[i] = int_data[i];
               }
            }
            else if (ind_data_type == PLC_FLOAT_DATA)
            {
               ptr = &inbfr[data_start_pos];

               for (i = 0; i < whole_data_size / ind_data_size; i++)
               {
                   f[0] = *ptr++;
                   f[1] = *ptr++;
                   f[2] = *ptr++;
                   f[3] = *ptr++;
                   brsmemcpy((UDINT)&float_data[i], (UDINT)f, 4);
	                PLC_READ_FLOAT_DATA[i] = float_data[i];
               }
            }
         }

         shr_global.stats[chan+32].good_msgs++;
         shr_global.stats[chan+32].alive = TRUE;
      }
   }
   else
   {
      ClosePLCRemoteSocket();
      connectedPLC = FALSE;
      shr_global.stats[chan+32].timeouts++;
      shr_global.stats[chan+32].alive = FALSE;
/*       printf("RecvSendUnitDataResponse: chan = %d timeouts=%d alive=%d\n",chan,shr_global.stats[chan+32].timeouts,shr_global.stats[chan+32].alive); */
   }
}

/*********************************************************/
/*********************************************************/
void ClosePLCRemoteSocket(void)
{
   if (ethnetpnPLC != 0)
      close(ethnetpnPLC);
   if (SocketNewPLC != 0)
      close(SocketNewPLC);
   connectedPLC = FALSE;
   ConnectedPLC_Flag = FALSE;
}

void BuildPLCEthAddress()
{
}
#else
// get rid of warning

#endif


