#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  rec_fun.c

   Description: Remote routines.


      $Log:   F:\Software\BR_Guardian\rec_fun.c_v  $
 *
 * Dec 1, 2008 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.recFun to toggle on/off debug information in this file
 *
 *    Rev 1.8   Sep 15 2008 13:20:30   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.7   Jul 25 2008 15:22:22   vidya
 * Made changes.
 *
 *    Rev 1.6   Jul 01 2008 10:05:40   vidya
 * Changed the config gate pointer.
 *
 *
 *    Rev 1.5   Apr 04 2008 11:33:52   vidya
 * Changed cs->temp_recipe to PMEM_RECIPE->
 *
 *    Rev 1.4   Mar 24 2008 09:59:10   FMK
 * The function declaration for Validate_Recipe
 * did not include the correct number of arguments.
 *
 *    Rev 1.3   Mar 21 2008 15:01:18   FMK
 * HMI.h cannot be in here. HMI structure is LOCAL only to
 * to interface task.
 *
 *    Rev 1.2   Mar 21 2008 09:51:52   Barry
 * Changed number of parameters to calc_recipe,
 * validate_recipe, and calc_parts_percent.
 *
 *    Rev 1.1   Mar 13 2008 12:57:30   vidya
 * Modified calc_recipe, calc_parts_percent,
 * validate_recipe functions.
 *
 *
 *    Rev 1.0   Feb 11 2008 11:09:50   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <stdio.h>
//#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include "mem.h"
#include "hmi.h"
#include "sys.h"
#include "vars.h"
#include "remote.h"
#include "signal_pcc.h"
#include "file_rw.h"
#include "libbgdbg.h"
#include "recipe.h"
#include "units.h"

int calc_recipe(recipe_struct *r);
void calc_parts_percent(recipe_struct *r);
void store_recipe(config_super *cs_,stored_recipe *rec_,int to,recipe_struct *from);
void restore_recipe(config_super *cs_,stored_recipe *rec_,recipe_struct *to,int from);
float read_recipe_inven(config_super *cs_,stored_recipe *rec_,int from);
int is_recipe_used(void);
int read_recipe_desc(config_super *cs_,stored_recipe *rec_,int rnum);
int validate_recipe(recipe_struct *r,int *validation_errors);
void clear_recipe(recipe_struct *r, int what);
void copy_recipe(recipe_struct *to, recipe_struct *from, int what);
float get_rec_val(recipe_struct *rec_ptr,int mode);
void put_rec_val(recipe_struct *rec_ptr,float value,int mode);
void set_recipe_gate_order(recipe_struct* current_recipe);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;
_GLOBAL unsigned char gate_order[MAX_GATES];
/************************************************************************/
/*  Function:  calc_recipe(r)  **** FOR BLENDER ****                    */
/*                                                                      */
/* Description:  performs calculations on recipe entry to determine     */
/*                 pph's, etc.  .                                       */
/************************************************************************/
int calc_recipe(recipe_struct *r)
{
   config_super *cs = &cfg_super;
   int j;

   float f;
   float refeed_percent = 0.0;
   char rec0,rec1,rec2;
   char calc_od = FALSE;
   char calc_id = FALSE;
   char calc_thk = FALSE;
   
   //cfg_super.width_k = 0.0833;	      /* put it temporarily */
   //cfg_super.ea_k = 0.0833; 
   //cfg_super.t_k = 62.428;   
   //cs->wpa_k = 1/60;
   //cs->wpl_k = 1/60;
     
   //cs->force_density = TRUE;    

   //if (r->stretch_factor <= 0.0)
   //   r->stretch_factor = 1;

   rec0 = cs->recipe_mode[0];
   rec1 = cs->recipe_mode[1];
   rec2 = cs->recipe_mode[2];

//   if (cs->ltp_mode == MONITOR)
//   {
//      rec2 = LTP;
//      r->ltp = ACTUAL_RECIPE->ltp;
//   }
//   else if (cs->wtp_mode == MONITOR)
//   {
//      rec2 = WTP;
//      r->total_wtp = ACTUAL_RECIPE->total_wtp;
//      r->ext_wtp   = ACTUAL_RECIPE->ext_wtp;
//
//   }
//
   if (cs->sltp_mode == MONITOR)
   {
      if (ACTUAL_RECIPE->stretch_factor > .01)
          r->stretch_factor = ACTUAL_RECIPE->stretch_factor;
   }

   /* take the parts and make them percents */
   calc_parts_percent(r);

   if (cs->density_in_recipe)
   {
  //    if ((cs->t_k <= 0.0) || (cs->od_k <= 0.0) || (cs->id_k <= 0.0) || (cs->ea_k <= 0.0))
  //       return(ERROR);
      /* decide what need to calculate */

      if ((rec0 == THICKNESS) && (rec1 == ID)) calc_od = TRUE;

      if ((rec1 == THICKNESS) && (rec0 == ID)) calc_od = TRUE;

      if ((rec0 == THICKNESS) && (rec1 == OD)) calc_id = TRUE;

      if ((rec1 == THICKNESS) && (rec0 == OD)) calc_id = TRUE;

      if ((rec0 == ID) && (rec1 == OD)) calc_thk = TRUE;

      if ((rec1 == ID) && (rec0 == OD)) calc_thk = TRUE;

      if (calc_od && (cs->od_k > .01))
      {
         r->od = (float)((r->id * cs->id_k + 2.0 * r->thk * cs->ea_k)/cs->od_k);     /* DIVIDE */
      }

      if (calc_id && (cs->id_k > .01))
      {
         r->id = (float)((r->od * cs->od_k - 2.0 * r->thk * cs->ea_k)/cs->id_k);     /* DIVIDE */
      }

      if (calc_thk && (cs->ea_k > .01))
      {
         r->thk = (float)((r->od * cs->od_k - r->id * cs->id_k)/(2.0 * cs->ea_k));   /* DIVIDE */
      }

      /* calculate the final mixture density */
      r->density.ts = 0.0;

      for (j=0; j<cs->total_gates; j++)
         r->density.ts = r->density.ts + r->density.s[j] * r->parts.s[j];
      r->avg_density = r->density.ts/100;

      if (r->avg_density == 0)  
         r->avg_density = 1;

      /* now do effective area calcs                            */
      switch(cs->application)
      {
         case BLOWN_FILM:
            r->ea = (float)(r->width * cfg_super.width_k * r->thk * cfg_super.ea_k * 2.0);
            break;

         case SHEET:
            r->ea = (float)(r->width * cs->width_k * r->thk * cs->ea_k);
            break;

         case WIRE_AND_CABLE:
            case PIPE_AND_TUBING:
            r->ea = (float)(PI * ((r->od*r->od*cs->od_k*cs->od_k) - (r->id*r->id*cs->id_k*cs->id_k)) / 4.0); /* DIVIDE */
            break;

         default:
            break;
      }

      /* now calculate overall ltp,wtp  */
      switch(rec2)
      {
         case WTP:
            f = r->ea * r->avg_density * cs->t_k;
            if (f > 0.01)
               r->ltp = (r->total_wtp*cs->wtp_k) / f;              /* DIVIDE */
            else
               r->ltp = 0.0;
            break;

         case LTP:
            r->total_wtp = r->ea * (r->ltp*cs->ltp_k) * r->avg_density * cs->t_k;
            break;

         case NONE:
            break;
      }

      /* now calculate wpl (corrected for scrap)        */
      if ((f=(r->ltp*cs->ltp_k)) > 0.01)
      {
         refeed_percent = 0.0;

         if (cs->scrap && (cs->refeed != GRAVIFLUFF)) /* adjust for trim-only */
            refeed_percent = r->parts_pct.s[REFEED];
         r->wpl = (float) (((r->total_wtp*cs->wtp_k) * cs->wpl_k * (1.0 - refeed_percent)) / f);
         if (r->width > .001)
            r->wpa = (float) (((r->total_wtp*cs->wtp_k) * cs->wpa_k) * (1.0 - refeed_percent)) / (f * r->width * cs->width_k);
      }
      //else
      //   r->wpl = r->wpa = 0.0;

      /* adjust wpl for stretch factor */
      if (r->stretch_factor > 0.0)
      {
         r->wpl = r->wpl / r->stretch_factor;
         r->wpa = r->wpa / r->stretch_factor;
      }
   }
   else                                /* layers by wt                 */
   {
      if (cs->force_density)
      {
         /* calculate the final mixture density */
         r->density.ts = 0.0;

         for (j=0; j<cs->total_gates; j++)
             r->density.ts = r->density.ts + r->density.s[j] * r->parts.s[j];
         r->avg_density = r->density.ts/100;
      }

      if (r->avg_density == 0)  
         r->avg_density = 1;
      
      if (cs->recipe_mode[0] == WPA)
      {
         refeed_percent = 0.0;
         if (cs->scrap && (cs->refeed != GRAVIFLUFF)) /* adjust for trim-only */
            refeed_percent = r->parts_pct.s[REFEED];

         switch(cs->recipe_mode[2])
         {
            case WTP:
	         if ((f = r->wpa) > 0.0001)
               r->ltp = (float) ((r->total_wtp*cs->wtp_k) * cs->wpa_k) *  (1.0 - refeed_percent) / (f * r->width * cs->width_k); 
            else
               r->ltp = 0.0;
            break;

            case LTP:
            if ((f = cs->wpa_k ) > 0.0)
               r->total_wtp = (r->wpa * (r->ltp*cs->ltp_k) * r->width * cs->width_k) / f;
            else
               r->total_wtp = 0.0;    
            break;

            case NONE:
            break;
         }
      }
 
      if (rec0 == WPL)
      {
         refeed_percent = 0.0;
         if (cs->scrap && (cs->refeed != GRAVIFLUFF)) /* adjust for trim-only */
            refeed_percent = r->parts_pct.s[REFEED];

         switch(rec2)
         {
            case WTP:
                if ((f=r->wpl) > 0.001)
                   r->ltp = (r->total_wtp*cs->wtp_k) * cs->wpl_k * (1.0 - refeed_percent) / f;       
                else
                   r->ltp = 0.0;
            break;

            case LTP:
                if ((f = cs->wpl_k) > 0.0)
                   r->total_wtp = r->wpl * (r->ltp*cs->ltp_k) / f;
                else
                   r->total_wtp = 0.0;    
            break;

            case NONE:
            break;
         }
      }
   }

   /* now distribute the total WTP to the extruder and the refeed */
   if (cs->scrap && (cs->refeed == TRIM_ONLY)) /* adjust for trim-only */
   {
      r->ext_wtp = (float)(r->total_wtp * (1.0 - r->parts_pct.s[REFEED]));
      r->refeed_wtp = r->total_wtp * r->parts_pct.s[REFEED];
   }
   else
      r->ext_wtp = r->total_wtp;

   return(NOERROR);
}

/************************************************************************/
/*  Function:  calc_parts_percent(r)                                    */
/*                                                                      */
/* Description:  performs calculations on recipe entry to determine     */
/*                 percentages. .                                       */
/************************************************************************/
void calc_parts_percent(recipe_struct *r)
{
	config_super *cs = &cfg_super;
	int j;
	float f1;
	float tol_ts = 0.0, regrind_ts = 0.0;  /* including regrind parts*/

	r->parts.ts = 0.0;
	for (j=0; j<cs->total_gates; j++)
	{
		/*if (cnf_LOOP[j].GATE->hopper_type != REGRIND)*/
		if (cfgGATE[j].hopper_type != REGRIND)
			r->parts.ts = r->parts.ts + r->parts.s[j];
		else
			regrind_ts = regrind_ts + r->parts.s[j];
	}
	
	for (; j<MAX_GATES; j++)
	{
		r->parts.s[j] = 0.0;
		r->parts_pct.s[j] = 0.0;
	}
	
	tol_ts = r->parts.ts + regrind_ts;

	if (r->parts.ts > 0.000001)
	{
		for (j=0; j<cs->total_gates; j++)
		{
			/*if (cnf_LOOP[j].GATE->hopper_type != REGRIND)*/
			if (cfgGATE[j].hopper_type != REGRIND)
				r->parts_pct.s[j] = (r->parts.s[j] / r->parts.ts);
			else
			{
				if (cs->recipe_entry_mode == PERCENT)
					r->parts_pct.s[j] = r->parts.s[j] / 100.0;
				else
					r->parts_pct.s[j] = r->parts.s[j] / tol_ts;
			}
		}
	}
	else
	{
		for (j=0; j<MAX_GATES; j++)
		{
			/*if (cnf_LOOP[j].GATE->hopper_type != REGRIND)*/
			if (cfgGATE[j].hopper_type != REGRIND)
				r->parts_pct.s[j] = 0.0;
			else
			{
				if (cs->recipe_entry_mode == PERCENT)
					r->parts_pct.s[j] = r->parts.s[j] / 100.0;
				else
					r->parts_pct.s[j] = r->parts.s[j] / tol_ts;
			}
		}
	}

	r->parts_pct.ts = 0.0;
	for(j=0; j<MAX_GATES; j++)
	{
		if (cfgGATE[j].hopper_type != REGRIND)
			r->parts_pct.ts = r->parts_pct.ts + r->parts_pct.s[j];
	}

   
	if (cs->scrap)
	{
      r->parts.ext = 100.0 - r->parts.s[REFEED];
		r->total_parts = f1 = 100.0;
		if(f1 > .01)
		{
			r->parts_pct.s[REFEED] = r->parts.s[REFEED] / f1;
			r->parts_pct.ext = 1.0 - r->parts_pct.s[REFEED];
		}
		else
		{
			r->parts_pct.s[REFEED] = 0.0;
			r->parts_pct.ext = 0.0;
		}
	}
	else
		r->total_parts = 100.0;

	if (cfg_super.recipe_entry_mode == PERCENT)
	{
		if ((r->parts.ts > 99.9995) && (r->parts.ts < 100.0005))
		{
			HMI.pg450_RecipeEntryColor[12] = 221;
			HMI.pg453_RecipeUseRunTime = 0;
		}
		else
		{
			HMI.pg450_RecipeEntryColor[12] = 45;
			HMI.pg453_RecipeUseRunTime = 2;
		}
	}
	else
	{
		HMI.pg450_RecipeEntryColor[12] = 221;
		HMI.pg453_RecipeUseRunTime = 0;
	}
	set_recipe_gate_order(r);	
}

/************************************************************************/
/*  Function:  store_recipe(to,from)                                    */
/*                                                                      */
/* Description:  copies recipe pointed to by from into recipe storage   */
/*                 location to.                                         */
/************************************************************************/
void store_recipe(config_super *cs_, stored_recipe *rec_, int to, recipe_struct *from)
{
   register config_super *cs = cs_;
   register int i, j;
   register stored_recipe *rec = rec_;

   if (to > 0)                           /* storage starts w/ 0          */
      to--;
   else
      to = 0;

   rec = rec + (to * cs->wordsperrecipe);

   (rec++)->i = (unsigned int) 1;       /* force 'used' true, since you are storing it     */

   if (((cs->wtp_mode == CONTROL) || (cs->ltp_mode == CONTROL)) && !cs->layers_by)
      (rec++)->f = get_rec_val(from,cs->recipe_mode[0]);

   if (cs->recipe_mode[1] != NONE)
      (rec++)->f = get_rec_val(from,cs->recipe_mode[1]);
   if (cs->recipe_mode[2] != NONE)
      (rec++)->f = get_rec_val(from,cs->recipe_mode[2]);

   if (cs->density_in_recipe)
   {
      for (j=0; j<cs->total_gates; ++j)
      {
         (rec++)->f = from->density.s[j];
      }
   }

   (rec++)->f = from->parts.t;
   if (cs->total_gates)
   {
      for (j=0; j<cs->total_gates; ++j)
         (rec++)->f = from->parts.s[j];
      if (cs->scrap)
         (rec++)->f = from->parts.s[REFEED];
   }
   else if (cs->scrap)
   {
      (rec++)->f = from->parts.s[0];
      (rec++)->f = from->parts.s[REFEED];
   }
   if (cs->refeed == GRAVIFLUFF)
      (rec++)->f = from->max_scrap_parts;

   if (cs->mix_time_in_recipe)
      (rec++)->f = from->mix_time;

   if (cs->job_wt_in_recipe)
      (rec++)->f = from->job_wt;

   /* leave room for the recipe inventory weight to be stored */
   rec += 2;

   /* store the product description */
   for (i=0; i<PRODUCT_CODE_LEN; ++i)
   {
      rec->c[i%4] = from->product_code[i];
      if(i%4 == 3) ++rec;
   }
   if (cs->enable_resin_codes)
   {
      /* store the resin codes for each hopper */
      for (i=0; i<cs->total_gates; ++i)
      {
         (rec++)->i = (unsigned short)(from->resin_recipe_num[i]);
      }
   }

   if (cs->gate_order_in_recipe)
   {
      for (i=0; i<cs->total_gates; ++i)
      {
         rec->c[i%4] = from->gate_order[i].number;
         if (i%4 == 3) ++rec;
      }
   }
}


/************************************************************************/
/*  Function:  restore_recipe(to,from)                                  */
/*                                                                      */
/* Description:  copies recipe from from storage to recipe pointed to   */
/*                 by to        .                                       */
/************************************************************************/
void restore_recipe(config_super *cs_,stored_recipe *rec_,recipe_struct *to,int from)
{
   register config_super *cs = cs_;
   register int i,j;
   register stored_recipe *rec = rec_;

   if (from > 0)                         /* storage starts w/ 0          */
       from--;
   else
       from = 0;

   rec = rec + (from * cs->wordsperrecipe);

   to->used = (int)(rec++)->i;         /* always included              */

   if (((cs->wtp_mode == CONTROL) || (cs->ltp_mode == CONTROL)) && !cs->layers_by)
       put_rec_val(to,(rec++)->f,cs->recipe_mode[0]);

   if (cs->recipe_mode[1] != NONE)
       put_rec_val(to,(rec++)->f,cs->recipe_mode[1]);
   if (cs->recipe_mode[2] != NONE)
       put_rec_val(to,(rec++)->f,cs->recipe_mode[2]);

   if (cs->density_in_recipe)
   {
       for(j=0;j<cs->total_gates;++j)
       {
           to->density.s[j] = (rec++)->f;
       }
   }

   to->parts.t = (rec++)->f;
   if (cs->total_gates)
   {
       for (j=0; j<cs->total_gates; ++j)
           to->parts.s[j] = (rec++)->f;
       if (cs->scrap)
           to->parts.s[REFEED] = (rec++)->f;
   }
   else if (cs->scrap)
   {
       to->parts.s[0] = (rec++)->f;
       to->parts.s[REFEED] = (rec++)->f;
   }
   if (cs->refeed == GRAVIFLUFF)
       to->max_scrap_parts = (rec++)->f;

   if (cs->mix_time_in_recipe) to->mix_time = (rec++)->f;
   if (cs->job_wt_in_recipe)   to->job_wt   = (rec++)->f;

   /* skip the recipe inventory weight */
   rec += 2;

   /* recall the product description */
   for (i=0; i<PRODUCT_CODE_LEN; ++i)
   {
       to->product_code[i] = rec->c[i%4];
       if (i%4 == 3) ++rec;
   }
   if (cs->enable_resin_codes)
   {
       /* recall the resin codes for each hopper */
       for (i=0; i<cs->total_gates; ++i)
       {
          to->resin_recipe_num[i] = (short)((rec++)->i);
       }
   }
   if (cs->gate_order_in_recipe)
   {
      if (to->used)
      {
         for (i=0; i<cs->total_gates; ++i)
         {
            to->gate_order[i].number = rec->c[i%4];
            if (i%4 == 3) ++rec;
         }
      }
      else
      {
         for (i=0; i<cs->total_gates; ++i)
         {
            to->gate_order[i].number = i;
         }
      }
   }
}

/************************************************************************/
/************************************************************************/
float read_recipe_inven(config_super *cs_, stored_recipe *rec_, int rnum)
{
    register config_super  *cs  = cs_;
    register stored_recipe *rec = rec_;
    double *wt;
    
    if (rnum > 0)
      --rnum;
    rec = rec + (rnum * cs->wordsperrecipe);
    wt = (double *)(rec + cs->recipe_inven_offset);

    return((float)*wt);

}

/************************************************************************/
/************************************************************************/
int is_recipe_used(void)
{
  return(BATCH_RECIPE_EDIT.used);
}
/************************************************************************/
/************************************************************************/

int recipe_num_changed(float num1, float num2)
{
  if (num1 != num2 )
     return(TRUE);
  else
    return(FALSE);
}
/************************************************************************/
/************************************************************************/
int read_recipe_desc(config_super *cs_, stored_recipe *rec_, int rnum)
{
    register config_super  *cs  = cs_;
    register stored_recipe *rec = rec_;

    if (rnum > 0)
      --rnum;

    rec = rec + (rnum * cs->wordsperrecipe);

    rec += cs->recipe_inven_offset + 2;
    return (int)rec;
}

/************************************************************************/
/************************************************************************/
int validate_recipe(recipe_struct *r, int *validation_errors)
{
    config_super *cs = &cfg_super;
    int   i;
    float total;
    config_HO_device *cnf_HO = &cfgHO;
 
    int   retval = 0;
    float newspeed;
    float HoSpeedFactor;

     /* clear recipe errors */
    for (i=0; i<=MAX_LOOP; i++)
       validation_errors[i] = 0;

    calc_parts_percent(r);
    /* make sure recipe totaled */
    total = 0.0;
    for (i=0; i<cs->total_gates; ++i)
    {
      /*if (cnf_LOOP[i].GATE->hopper_type!=REGRIND)*/
     	if (cfgGATE[i].hopper_type != REGRIND)
         	total += r->parts.s[i];
    }

    if ((cs->recipe_entry_mode == PERCENT) && (fabs(total - 100.0) > .1))
    {
        validation_errors[MAX_LOOP] |= RECIPE_NOT_100;
        retval |= RECIPE_NOT_100;
    }

    /*if (!cnf_LOOP[WEIGH_HOP_LOOP].BWH->cal_done)*/
    if (cfgGATE[i].hopper_type != REGRIND)
       if (!cfgBWH.cal_done)
       {
          validation_errors[WEIGH_HOP_LOOP] |= RECIPE_NO_CAL;
          retval |= RECIPE_NO_CAL;
       }

    switch(cs->ltp_mode)
    {
        case MONITOR:
            break;
        case CONTROL:
            if (r->ltp <= 0.0)
            {
                validation_errors[HO_LOOP] |= RECIPE_BAD_THROUGHPUT;
                retval |= RECIPE_BAD_THROUGHPUT;
            }

            if (!cfgHO.cal_done)
            {
                validation_errors[HO_LOOP] |= RECIPE_NO_CAL;
                retval |= RECIPE_NO_CAL;
            }

            /* check for rate speeds  */
            if (cfgHO.rs_num <= 0.0 )
            {
               validation_errors[HO_LOOP] |= RECIPE_NO_RATESPEED;
               retval |= RECIPE_NO_RATESPEED;
            }

            HoSpeedFactor = cfgHO.spd_factor[shrHO.GearNum];

            if (cfgHO.drive_has_control)
                newspeed = BATCH_RECIPE_NEW->ltp;
            else if (cfgHO.rs_ave != 0.0)
                newspeed = BATCH_RECIPE_NEW->ltp / cfgHO.rs_ave;
            else
                newspeed = 0.0;

            if (newspeed > 0.0)
            {
              if (newspeed > cnf_HO->max_spd * cfgHO.warning_high_spd)
              {
                validation_errors[HO_LOOP] |= RECIPE_OVERSPEED_AL;
                retval |= RECIPE_OVERSPEED_AL;
              }
              else if (newspeed < cnf_HO->max_spd * cfgHO.warning_low_spd)
              {
              //  validation_errors[HO_LOOP] |= RECIPE_UNDERSPEED_AL;
              //  retval |= RECIPE_UNDERSPEED_AL;
              }
            }
            break;

        case UNUSED:
            break;
    }

    switch(cs->wtp_mode)
    {
        case MONITOR:
            break;
        case CONTROL:
            if (r->total_wtp <= 0.1)
            {
               validation_errors[EXT_LOOP] |= RECIPE_BAD_THROUGHPUT;
               retval |= RECIPE_BAD_THROUGHPUT;
            }
            if (r->total_parts <= 0.0)
            {
               validation_errors[EXT_LOOP] |= RECIPE_BAD_PROD;
               retval |= RECIPE_BAD_PROD;
            }

            if (!cfgEXT.cal_done)
            {
               validation_errors[EXT_LOOP] |= RECIPE_NO_CAL;
               retval |= RECIPE_NO_CAL;
            }

            /* check for rate speeds  */
            if (cfgEXT.rs_num <= 0.0 )
            {
               validation_errors[EXT_LOOP] |= RECIPE_NO_RATESPEED;
               retval |= RECIPE_NO_RATESPEED;
            }

            if (cfgEXT.rs_ave > .003)
                newspeed = BATCH_RECIPE_NEW->ext_wtp / cfgEXT.rs_ave;       /* DIVIDE */
            else
                newspeed = 0.0;

            if (newspeed > 0.0)
            {
                if (newspeed > cfgEXT.max_spd * cfgEXT.warning_high_spd)
                {
                    validation_errors[EXT_LOOP] |= RECIPE_OVERSPEED_AL;
                    retval |= RECIPE_OVERSPEED_AL;
                }
                else if (newspeed < cfgEXT.max_spd * cfgEXT.warning_low_spd)
                {
                    validation_errors[EXT_LOOP] |= RECIPE_UNDERSPEED_AL;
                    retval |= RECIPE_UNDERSPEED_AL;
                }
            }
#if 0 /* bmh br */
            /*cnf_LIW = cnf_LOOP[GRAVIFLUFF_FDR_LOOP].LIW;*/
            cnf_LIW = ((config_loop_ptr*)(&cnf_LOOP[GRAVIFLUFF_FDR_LOOP]))->LIW;
            if(cs->scrap && (cs->refeed == GRAVIFLUFF))
            {
               /*if((BATCH_RECIPE_EDIT.refeed_wtp > 0.0) && !cnf_LOOP[GRAVIFLUFF_LOOP].LIW->cal_done)*/
               if((BATCH_RECIPE_EDIT.refeed_wtp > 0.0) && !((config_loop_ptr*)(&cnf_LOOP[GRAVIFLUFF_LOOP]))->LIW->cal_done)
               {
                  validation_errors[GRAVIFLUFF_LOOP] |= RECIPE_NO_CAL;
                  retval |= RECIPE_NO_CAL;
               }

               /* check for rate speeds  */
               if ((BATCH_RECIPE_EDIT.refeed_wtp > 0.0) && (cnf_LIW->rs_num <= 0.0))
               {
                  validation_errors[GRAVIFLUFF_FDR_LOOP] |= RECIPE_NO_RATESPEED;
                  retval |= RECIPE_NO_RATESPEED;
               }

               if (cnf_LIW->rs_ave != 0.0)
                 newspeed = BATCH_RECIPE_EDIT.refeed_wtp / cnf_LIW->rs_ave;
               else
                 newspeed = 0.0;
               if (newspeed > 0.0)
               {
                 if (newspeed > cnf_LIW->max_spd * cnf_LIW->warning_high_spd)
                 {
                   validation_errors[GRAVIFLUFF_LOOP] |= RECIPE_OVERSPEED_AL;
                   retval |= RECIPE_OVERSPEED_AL;
                 }
                 else if (newspeed < cnf_LIW->max_spd * cnf_LIW->warning_low_spd)
                 {
                   validation_errors[GRAVIFLUFF_LOOP] |= RECIPE_UNDERSPEED_AL;
                   retval |= RECIPE_UNDERSPEED_AL;
                 }
               }
            }
#endif
            break;

        case UNUSED:
            break;
    }

    return(retval);
}

/************************************************************************/
/*  Function:  clear_recipe(recipe *r, int what)                        */
/*                                                                      */
/* Description:  clears portions of the recipe specified by what        */
/************************************************************************/
void clear_recipe(recipe_struct *r, int what)
{
  int i;

  if (what & BLENDER_RECIPE)
  {
    r->parts.t = 0.0;
    r->parts.ts = 0.0;
    for (i=0; i<REFEED; i++)
       r->parts.s[i] = 0.0;
    r->fRegrindInitialPct = 0.0;

    r->parts_pct.t = 0.0;
    r->parts_pct.ts = 0.0;
    for (i=0; i<REFEED; i++)
       r->parts_pct.s[i] = 0.0;

    /*r->mix_time = cnf_LOOP[MIXER_LOOP].MIX->mix_time;*/
    r->mix_time = cfgMIXER.mix_time;
    r->job_wt = 0.0;
/*    for (i=0; i<MAX_GATES; i++)
      r->clear_gate_cal[i] = FALSE; */
  }

  if (what & (BLENDER_RECIPE|DENSITY_ONLY))
  {
    r->density.t = 0.0;
    r->density.ts = 0.0;
    for (i=0; i<MAX_HOPPER; i++)
       r->density.s[i] = 0.0;
    r->density.ext = 0.0;
    r->avg_density = 0.0;
  }
  if (what & EXTRUDER_RECIPE)
  {
    r->parts.ext = 0.0;
    r->parts_pct.ext = 0.0;
    r->parts.s[REFEED] = 0.0;
    r->parts_pct.s[REFEED] = 0.0;
    r->wpl = 0.0;
    r->thk = 0.0;
    r->ltp = 0.0;
    r->width = 0.0;
    r->id = 0.0;
    r->od = 0.0;
    r->ea = 0.0;
    r->total_parts = 0.0;
    r->ho_man_spd = 0.0;
    r->ext_man_spd = 0.0;
    r->refeed_man_spd = 0.0;
    r->ho_gear_num = 0;
    r->total_wtp = 0.0;
    r->ext_wtp = 0.0;
    r->refeed_wtp = 0.0;
    r->max_scrap_parts = 0.0;
    r->stretch_factor = 0.0;
    r->wpa = 0.0;
  }
}
/************************************************************************/
/*  Function:  copy_recipe(recipe *to, recipe *from, int what)          */
/*                                                                      */
/* Description:  copies portions of the recipe specified by what        */
/************************************************************************/
void copy_recipe(recipe_struct *to, recipe_struct *from, int what)
{
  int i;

  strncpy(to->product_code, from->product_code, 16);

  if (what & BLENDER_RECIPE)
  {
    to->fRegrindInitialPct = from->fRegrindInitialPct;
  
    to->index = from->index;     
    to->parts.t = from->parts.t;
    to->parts.ts = from->parts.ts;
    for (i=0; i<REFEED; i++)
       to->parts.s[i] = from->parts.s[i];
    to->parts_pct.t = from->parts_pct.t;
    to->parts_pct.ts = from->parts_pct.ts;
    for (i=0; i<REFEED; i++)
       to->parts_pct.s[i] = from->parts_pct.s[i];
    for (i=0; i<REFEED; i++)
       to->resin_recipe_num[i] = from->resin_recipe_num[i];
    to->mix_time = from->mix_time;
    to->job_wt = from->job_wt;
    to->recipe_inven_wt = from->recipe_inven_wt;
    for (i=0; i<MAX_GATES; i++)
    {
       to->gate_order[i].number = from->gate_order[i].number;
    }
  }

  if (what & (BLENDER_RECIPE|DENSITY_ONLY))
  {
    to->density.t = from->density.t;
    to->density.ts = from->density.ts;
    for (i=0; i<MAX_HOPPER; i++)
       to->density.s[i] = from->density.s[i];
    to->density.ext = from->density.ext;
    to->avg_density = from->avg_density;
  }

   if (what & EXTRUDER_RECIPE)
   {
    to->parts.ext = from->parts.ext;
    to->parts_pct.ext = from->parts_pct.ext;
    to->parts.s[REFEED] = from->parts.s[REFEED];
    to->parts_pct.s[REFEED] = from->parts_pct.s[REFEED];
    to->resin_recipe_num[REFEED] = from->resin_recipe_num[REFEED];
    to->wpl = from->wpl;
    to->wpa = from->wpa;
    to->thk = from->thk;
    to->ltp = from->ltp;
    to->width = from->width;
    to->id = from->id;
    to->od = from->od;
    to->ea = from->ea;
    to->total_parts = from->total_parts;
    to->ho_man_spd = from->ho_man_spd;
    to->ext_man_spd = from->ext_man_spd;
    to->refeed_man_spd = from->refeed_man_spd;
    to->ho_gear_num = from->ho_gear_num;
    to->total_wtp = from->total_wtp;
    to->ext_wtp = from->ext_wtp;
    to->refeed_wtp = from->refeed_wtp;
    to->max_scrap_parts = from->max_scrap_parts;
    to->stretch_factor = from->stretch_factor;
    
    to->pg450_RecipeEntry[0] = from->pg450_RecipeEntry[0];
    to->pg450_RecipeEntry[1] = from->pg450_RecipeEntry[1];
    to->pg450_RecipeEntry[2] = from->pg450_RecipeEntry[2];
        
    to->pg454_SetRecipeEntry[0] = from->pg454_SetRecipeEntry[0];
    to->pg454_SetRecipeEntry[1] = from->pg454_SetRecipeEntry[1];
    to->pg454_SetRecipeEntry[2] = from->pg454_SetRecipeEntry[2];
   }
}
/************************************************************************/
/*  Function:  get_rec_val(rec_ptr,mode)                                */
/*                                                                      */
/* Description:  gets recipe value of type mode from recipe pointed to  */
/*                 by rec_ptr.                                          */
/************************************************************************/
float get_rec_val(recipe_struct *rec_ptr,int mode)
{
  switch(mode)
  {
    case WTP:
      return(rec_ptr->total_wtp);
    case LTP:
      return(rec_ptr->ltp);
    case WPL:
      return(rec_ptr->wpl);
    case WPA:
      return(rec_ptr->wpa);
    case THICKNESS:
      return(rec_ptr->thk);
    case OD:
      return(rec_ptr->od);
    case ID:
      return(rec_ptr->id);
    case WIDTH:
      return(rec_ptr->width);
  }
  return((float)ILLEGAL_FLOAT);
}

/************************************************************************/
/*  Function:  put_rec_val(rec_ptr,value,mode)                          */
/*                                                                      */
/* Description:  puts recipe value 'value' of type mode into recipe     */
/*                 pointed to  by rec_ptr.                              */
/************************************************************************/
void put_rec_val(recipe_struct *rec_ptr,float value,int mode)
{
  switch(mode)
  {
    case WTP:
      rec_ptr->total_wtp = value;
      break;
    case LTP:
      rec_ptr->ltp = value;
      break;
    case WPL:
      rec_ptr->wpl = value;
      break;
    case WPA:
      rec_ptr->wpa = value;
      break;
    case THICKNESS:
      rec_ptr->thk = value;
      break;
    case OD:
      rec_ptr->od = value;
      break;
    case ID:
      rec_ptr->id = value;
      break;
    case WIDTH:
      rec_ptr->width = value;
      break;
  }
}

/************************************************************************/
void set_recipe_gate_order(recipe_struct* current_recipe)
{
	int i, j, k, gate_index = 0;
	int insert_gate = FALSE;
	unsigned char new_hopper_type, current_hopper_type;
	
	if (!cfg_super.gate_order_in_recipe)
	{   
		for (i=0; i<cfg_super.total_gates; i++)
		{
			gate_order[i] = i;
		}

		for (i=0; i<cfg_super.total_gates; i++)
		{
			new_hopper_type = cfgGATE[i].hopper_type;
			for (j=0; j < gate_index; j++)
			{
				if (i == gate_order[j])
					break;
            
				current_hopper_type = cfgGATE[gate_order[j]].hopper_type;

				/*put regrind hoppers before other hoppers */
				if (new_hopper_type == REGRIND && current_hopper_type != REGRIND)
				{
					insert_gate = TRUE;
					break;
				}
				if (new_hopper_type == REGRIND && current_hopper_type == REGRIND)
				{
					/* make regind hopper with highest percentage the first hopper */
					if (current_recipe->parts.s[i]>current_recipe->parts.s[gate_order[j]])
					{
						insert_gate = TRUE;
						break;
					}
				}
				if (current_hopper_type != REGRIND)
				{
					/* make vrgin hoppers with highest percentage go before other virgin hoppers  */
					if (current_recipe->parts.s[i]>current_recipe->parts.s[gate_order[j]])
					{
						insert_gate = TRUE;
						break;
					}
				}
			}
			/* end gate order j loop */
			if (insert_gate)
			{
				/* insert gate, shift all remainng gates down (to the right)*/
				for (k=gate_index; k > j; k--)
					gate_order[k] = gate_order[k-1];
				/* insert new gate */
				gate_order[j] = i;
				gate_index++;
			}
			else
			{
				/* add gate to end of gate order */
				gate_order[gate_index] = i;
				gate_index++;
			}
		}

		for (j=0; j < gate_index; j++)
		{
			current_recipe->gate_order[j].number = gate_order[j];
		}
	}
}   
