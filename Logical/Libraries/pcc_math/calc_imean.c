#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_imean(isamples *sample_data)
{
    int length;
    int mean;

    length = sample_data->num_taken;
    if(length > sample_data->length) length = sample_data->length;
    mean = calc_imean_p(sample_data,length);
    return mean;
}


