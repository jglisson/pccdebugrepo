#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  mod_com.c

   Description: Modbus common routines.


      $Log:   F:\Software\BR_Guardian\mod_com.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.modCom to toggle the debug information in this file
 *
 *    Rev 1.5   May 12 2008 12:43:50   YZS
 * Fixed broken Modbus serial communication caused by previous cleaning-ups.
 *
 *
 *    Rev 1.4   May 06 2008 13:16:16   YZS
 * Did some clean-up.
 *
 *
 *    Rev 1.3   Apr 30 2008 14:01:08   YZS
 * Fixed compile warnings and erase GLOBALs and LOCALs.
 *
 *
 *    Rev 1.2   Apr 11 2008 09:26:44   FMK
 * Fixed compiler warnings.
 *
 *    Rev 1.1   Mar 18 2008 10:54:52   YZS
 * Commented out some unused function calls.
 *
 *    Rev 1.0   Feb 11 2008 11:15:22   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include "comminc.h"

//#include <stdio.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include "mem.h"
#include "sys.h"
#include "remote.h"
#include "register.h"
#include "mod_com.h"
#include "vars.h"
#include "libbgdbg.h"
#include "AsBrStr.h"

#define MACH BATCH

extern void debug_rem_write(point_data *pd, int point, fic *value, int type);
extern void debug_rem_read(point_data *pd, int point, fic *value, int type);
void fill_sub_hdr(unsigned char group);
void fill_ref_data_packet(unsigned int file, unsigned int start_reg, int num_regs);
unsigned int modbus_2_epcc(unsigned int);
unsigned int modbus_to_pcc2(unsigned int file, unsigned int modbus_reg);
unsigned short read_int16(unsigned char *bfr);
unsigned int translate_value(int reg, unsigned int val);
void write_modbus_reg(unsigned int modbus_reg, int value);
float find_multiplier(int);
void read_device_definition(unsigned int file, unsigned int modbus_reg);
void set_device_definition(unsigned int file, unsigned int modbus_reg, unsigned char * data);
void map_device(unsigned int modbus_device, unsigned int * pcc2_device, unsigned int * pcc2_sub_device);
void write_ref_data(struct msg_struct * curr_msg, int group, unsigned char * data);
void get_data_block(void);
int ascii_to_byte(unsigned char *);
void fill_addr_fcn(int);
void fcn_read_reg(void);
void fcn_read_general_ref(void);
void fcn_write_general_ref(void);
void fcn_write_mult(void);
void fcn_write_reg(void);
void fill_data_packet(unsigned int start_reg, int num_regs);
void ascii_to_binary(unsigned char *ascii, unsigned char *binary, int number);
int check_msg(void);
void fill_error_check(void);
void get_byte_count(void);
void byte_to_ascii(unsigned char * byte_buf, unsigned char * ascii_buf, int byte_count);
void fill_read_reg_data_packet_32bit(int file, int start_reg, int num_regs);
void write_multi_data_32bit(int file, int start_reg, unsigned char * data, int num_regs);
extern unsigned int calc_checksum(unsigned char * start, int len);
extern void com_ipwrite(register unsigned char * ptr, unsigned int value, int size);
extern unsigned short CalcCrc_16(unsigned char *DataPtr, unsigned short Count, unsigned short Poly);
extern int read_remote_var_2(point_data *pd, unsigned int p,fic *v,int *type);
extern int write_remote_var_2(point_data *pd, unsigned int p,fic v);
extern int write_remote_var(point_data *, unsigned int , fic );
extern int read_remote_var(point_data *pd, unsigned int p,fic *v,int *pt_type);

extern point_data pd;
extern struct msg_struct msg;
extern int msg_state;
extern int in_count;
extern unsigned short out_count;
extern unsigned char temp_bfr[BFR_SIZE];
extern unsigned char *inbfr;
extern unsigned char outbfr[BFR_SIZE];
extern unsigned char *out_packet;
extern struct port_diag_data_str *r_diag;
extern unsigned int timeout_ticks;
extern int MODBUS_CONN_TYPE;   /* serial or ethernet */
extern int modbus_type;        /* RTU or ASCII */
extern char reset_port;

UINT file_number, start_register, num_reg, num_group;
extern FRM_gbuf_typ FrameGetBufferStruct;
extern UDINT Ident;
extern UDINT* SendBuffer;
extern UINT SendBufferLength;
extern UINT StatusGetBuffer, StatusWrite, StatusReleaseOutputBuffer;
extern FRM_write_typ FrameWriteStruct;
extern USINT Error, Start, StartRead, Close, OKAY;
extern FRM_robuf_typ FrameReleaseOutputBufferStruct;
extern USINT ReadData[1024];               /* arrays for write and read data */

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

float default_mult[100] =
	{
	/*  0        1        2        3        4        5        6        7        8        9  */

	1.0,     100.0,   100.0,   10.0,    10.0,    10.0,    10.0,    100.0,   100.0,   100.0,     /* 0  -  9 */
	100.0,   1.0,     1.0,     10.0,    10.0,    100.0,   100.0,   100.0,   100.0,   100.0,     /* 10 - 19 */
	100.0,   100.0,   100.0,   100.0,   100.0,   100.0,   1000.0,  1000.0,  100.0,   100.0,     /* 20 - 29 */
	100.0,   10.0,    10.0,    100.0,   100.0,   100.0,   100.0,   100.0,   100.0,   100.0,     /* 30 - 39 */
	1000.0,  100.0,   100.0,   1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 40 - 49 */
	100.0,   100.0,   1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 50 - 59 */
	10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    10.0,    1.0,       /* 60 - 69 */
	1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 70 - 79 */
	1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,       /* 80 - 89 */
	1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0,     1.0        /* 90 - 99 */
	};

/****************************************************************************
 * Function: modbus_to_pcc2
 ****************************************************************************/
unsigned int modbus_to_pcc2(unsigned int file, unsigned int modbus_reg)
{
	unsigned int reg, pcc2_reg = 0;
	unsigned int addr = 0;
	unsigned int mod_device;
	unsigned int pcc2_device, pcc2_sub_device = 0;

	addr = ((file - 1) * 10000) + modbus_reg;
	reg = ((modbus_reg % 1000) / 2) + 1;
	mod_device = addr / 1000;


	/* map the modbus device to pcc2 device */
	map_device(mod_device, &pcc2_device, &pcc2_sub_device);

	/* map the modbus register to pcc2 register */
	pcc2_reg = (pcc2_device * 100000) + (pcc2_sub_device * 1000) + (reg);
	return (pcc2_reg);
}


#if ((MACH==BATCH)||(MACH==WX_BATCH)||(MACH==WE_BATCH))

/****************************************************************************
 * Function: map_device
 ****************************************************************************/
void map_device(unsigned int modbus_device, unsigned int * pcc2_device, unsigned int * pcc2_sub_device)
{

	if (modbus_device <= REM_MAX_SUB_DEVICE_ID)
	{
		*pcc2_device = 0;
		switch (modbus_device)
		{
			case MOD_BWH_DEVICE:
				*pcc2_sub_device = BWH_SUBDEVICE;
				break;
			case MOD_MIXER_DEVICE:
				*pcc2_sub_device = MIXER_SUBDEVICE;
				break;
			case MOD_REFEED_DEVICE:
				*pcc2_sub_device = REFEED_SUBDEVICE;
				break;
			default:
				*pcc2_sub_device = modbus_device;
				break;
		}
	}
	else
	{
		*pcc2_device = modbus_device;
		switch (modbus_device)
		{
			case MOD_SHO_DEVICE:
				*pcc2_sub_device = 1;
				break;
			default:
				*pcc2_sub_device = 0;
				break;
		}
	}
}

#endif

#if (MACH==BE_BLENDER)||(MACH==BGBC_BLENDER)

/****************************************************************************
 * Function: map_device
 ****************************************************************************/
void map_device(unsigned int modbus_device, unsigned int * pcc2_device, unsigned int * pcc2_sub_device)
{
	if (modbus_device <= REM_MAX_SUB_DEVICE_ID)
	{
		*pcc2_device = 0;
		switch (modbus_device)
		{
			case MOD_DC_DEVICE:
				*pcc2_sub_device = BWH_SUBDEVICE;
				break;
			case MOD_REFEED_DEVICE:
				*pcc2_sub_device = REFEED_SUBDEVICE;
				break;
			default:
				*pcc2_sub_device = modbus_device;
				break;
		}
	}
	else
	{
		*pcc2_device = modbus_device;
		switch (modbus_device)
		{
			case MOD_SHO_DEVICE:
				*pcc2_sub_device = 1;
				break;
			default:
				*pcc2_sub_device = 0;
				break;
		}
	}
}
#endif

/****************************************************************************
 * Function: modbus_2_epcc
 ****************************************************************************/
unsigned int modbus_2_epcc(unsigned int modbus_reg)
{
	unsigned int epcc_reg = 0;
	int group;
	unsigned int point;
	unsigned int temp;

	/* per hopper device */
	if (modbus_reg < 9000)
	{
		epcc_reg = modbus_reg + 1;/* take care of zero offset of the modbus register */
#if ((MACH==BATCH)||(MACH==WX_BATCH)||(MACH==WE_BATCH))
		if (epcc_reg >= 8000)
		{
			temp = epcc_reg - 8000;
			group = temp / 100;
			point = temp % 100;

			switch (group)
			{
				case 0:             /* Batch hopper */
					epcc_reg = BATCH_HOP_OFFSET + point;
					break;

				case 1:             /* Mixer */
					epcc_reg = MIX_OFFSET + point;
					break;

				default:
					epcc_reg = 0;
					break;
			}
		}
#endif
	}
	else
	{
		temp = modbus_reg - 9000 + 1;
		group = temp / 100;
		point = temp % 100;

		switch (group)
		{
			case 0:                /* nip */
				epcc_reg = HO_OFFSET + point;
				break;
			case 1:                /* width */
				epcc_reg = WTH_OFFSET + point;
				break;
			case 2:                /* system */
				epcc_reg = SYSTEM_OFFSET + point;
				break;
			case 3:
				break;
			case 4:
				break;
			case 5:
				epcc_reg = 30000 + point;
				break;
			case 6:
				epcc_reg = 30100 + point;
				break;
			case 7:
				epcc_reg = 30200 + point;
				break;
			case 8:
				epcc_reg = 30300 + point;
				break;
			case 9:
				epcc_reg = 30400 + point;
				break;
			default:
				epcc_reg = 0;
				break;
		}
	}
	return epcc_reg;
}

/****************************************************************************
 * Function: read_int16
 ****************************************************************************/
unsigned short read_int16(unsigned char *bfr)
{
	typedef union
	{
	unsigned short s;
	char c[2];
	} sc;
sc val;
/*
   sc.c[0] = *bfr++;
   sc.c[1] = *bfr;
*/
val.c[1] = *bfr++;
val.c[0] = *bfr;

return val.s;
}

/****************************************************************************
 * Function: find_multiplier
****************************************************************************/
float find_multiplier(int reg)
{
	register float mult;

	if (reg == 0)
		return(1.0);

	/* strip off all but the point */
	reg = reg % HOPPER_OFFSET;
	mult = default_mult[reg];

	return (mult);
}

/****************************************************************************
 * Function: fill_sub_hdr
 ****************************************************************************/
void fill_sub_hdr(unsigned char group)
{
	unsigned char count;

	count = (2 * msg.ref_group[group].num_regs) + 1;

	if (out_count >= BFR_SIZE-2)
	{
		return;
	}
	out_packet[out_count++] = count; /* set sub-response count */
	out_packet[out_count++] = (unsigned char) msg.ref_group[group].reference_type;

	/* add sub-response count to total count */
	out_packet[2] += count + 1;
}


/****************************************************************************
 * Function: fill_ref_data_packet
 ****************************************************************************/
void fill_ref_data_packet(unsigned int file, unsigned int start_reg, int num_regs)
{
	register unsigned int curr_reg;
	register int i;
	unsigned int epcc_reg;
	fic val;
	int point_type;
	int status=0, dev;
	int read_string = FALSE;

	/* convert 6 digit address to PCC type 2 register */
	curr_reg = start_reg;

	for (i = 0; i < num_regs; i++)
	{
		dev = (((file - 1) * 10000) + curr_reg) / 1000;
		if ((dev == REM_RESIN_NAME_ID) || (dev == REM_RECIPE_NAME_ID))
			read_string = TRUE;
		else
			read_string = FALSE;

		if (((curr_reg % 2) && (curr_reg > 0)) || read_string)
		{
			if (((curr_reg % 1000) == 999) || ((curr_reg % 1000) == 997))
			{
				read_device_definition(file, curr_reg);
			}
			else
			{
				epcc_reg = modbus_to_pcc2(file, curr_reg);
				status = read_remote_var_2(&pd, epcc_reg, &val, &point_type);
				debug_rem_read(&pd, epcc_reg, &val, point_type);

				switch (point_type)
				{
					case FLOAT:
						if (status == INVALID_POINT)
							val.f = 0.0;

						if (out_count >= BFR_SIZE-4)
						{
						   return;
						}

						/* pack the floating point response */
						out_packet[out_count++] = (unsigned char) val.c[1];  /* this is OK! */
						out_packet[out_count++] = (unsigned char) val.c[0];
						out_packet[out_count++] = (unsigned char) val.c[3];
						out_packet[out_count++] = (unsigned char) val.c[2];
						break;
					case INT:
						if (status == INVALID_POINT)
						{
							val.i = 0;
						}

						if (out_count >= BFR_SIZE-4)
						{
						   return;
						}

						out_packet[out_count++] = (unsigned char) val.c[3];	/* this is OK! */
						out_packet[out_count++] = (unsigned char) val.c[2];
						out_packet[out_count++] = (unsigned char) val.c[1];
						out_packet[out_count++] = (unsigned char) val.c[0];
						break;
					case RESIN_STRING:
						/* 2 chars. per register */
						if (out_count >= BFR_SIZE-2)
						{
							return;
						}
						if ((curr_reg % 2) == 0)
						{
							out_packet[out_count++] = (unsigned char) val.c[0];
							out_packet[out_count++] = (unsigned char) val.c[1];
						}
						else
						{
							out_packet[out_count++] = (unsigned char) val.c[2];
							out_packet[out_count++] = (unsigned char) val.c[3];
						}
						break;

					default:
						break;
				}
			}
		}
		curr_reg++;
	} /* end of for loop */
}
/****************************************************************************
 * Function: write_ref_data
 ****************************************************************************/
void write_ref_data(struct msg_struct * curr_msg, int group, unsigned char * data)
{
	unsigned int num_regs = curr_msg->ref_group[group].num_regs;
	unsigned int file = curr_msg->ref_group[group].file_number;
	register unsigned int curr_reg;
	register unsigned int i;
	unsigned int epcc_reg, temp_reg;
	fic val;
	int dev,point_type;
	int write_string = FALSE;
	char resin_or_recipe_data=FALSE;
	config_super *cs=&cfg_super;
   
	curr_reg = curr_msg->ref_group[group].start_reg;

	for (i=0; i<num_regs; ++i)
	{
		dev = (((file - 1) * 10000) + curr_reg) / 1000;
		if ((dev == REM_RESIN_NAME_ID) || (dev == REM_RECIPE_NAME_ID))
			write_string = TRUE;
		else
			write_string = FALSE;

		if ((dev == REM_RESIN_DATA_ID) || (dev == REM_RECIPE_DATA_ID))
			resin_or_recipe_data = TRUE;

		if (((curr_reg % 2) && (curr_reg > 0)) || write_string)
		{
			if (((curr_reg % 1000) == 999) || ((curr_reg % 1000) == 997))
			{
				/* handle device definition commands here */
				set_device_definition(curr_msg->ref_group[group].file_number, curr_reg, data);
				data += 4;
			}
			else
			{
				/* translate modbus 6-digit address to pcc address */
				epcc_reg = modbus_to_pcc2(file, curr_reg);

				temp_reg = epcc_reg;
				if (!write_string)
				{
					/* if translate_register_2(&epcc_reg) != 2)*/
					{
						if (((epcc_reg % 1000) < REG_INTEGER) && ((epcc_reg % 1000) >= REG_DOUBLE))
							point_type = DOUBLE;
						else if ((epcc_reg % 1000) < REG_INTEGER)
							point_type = FLOAT;
						else
							point_type = INT;

						/* normal register, read in data value for this register */
						if (point_type == INT)
						{
							if (cs->ucModbusSwapBytes)  
							{
								val.c[1] = *data++;
								val.c[0] = *data++;
								val.c[3] = *data++;
								val.c[2] = *data++;
							}
							else
							{
								val.c[3] = *data++;
								val.c[2] = *data++;
								val.c[1] = *data++;
								val.c[0] = *data++;
							}
						}
						else if (point_type == DOUBLE)
						{
							val.c[1] = *data++;
							val.c[0] = *data++;
							val.c[3] = *data++;
							val.c[2] = *data++;

							val.c[5] = *data++;
							val.c[4] = *data++;
							val.c[7] = *data++;
							val.c[6] = *data++;
						}
						else
						{
							val.c[1] = *data++;
							val.c[0] = *data++;
							val.c[3] = *data++;
							val.c[2] = *data++;
						}
					}
#if 0
					else /* defining UCB data is always integer */
					{
						point_type = INT;

							val.c[0] = *data++;
						val.c[1] = *data++;
						val.c[2] = *data++;
						val.c[3] = *data++;
						epcc_reg = temp_reg;
					}
#endif
					write_remote_var_2(&pd, epcc_reg, val);
				}
				else
				{
					point_type = STRINGS;
					if ((curr_reg % 2) == 0)
					{
						val.c[0] = *data++;
						val.c[1] = *data++;
					}
					else
					{
						val.c[2] = *data++;
						val.c[3] = *data++;
						write_remote_var_2(&pd, epcc_reg, val);
					}
				}
				debug_rem_write(&pd, epcc_reg, &val, point_type);
			}

		}
		curr_reg++;
	}
}

/****************************************************************************
 * Function: translate_value
 ****************************************************************************/
unsigned int translate_value(int reg, unsigned int val)
{
	/* make any modifications to return values */
	switch (reg)
	{
		case R_INVEN_WT:
		case R_SHIFT_WT:
		case R_INVEN_LEN:
		case R_SHIFT_LEN:
			/* only return the lower word */
			val &= 0xffff;
			break;

		case R_INVEN_WT_HI:
		case R_SHIFT_WT_HI:
		case R_INVEN_LEN_HI:
		case R_SHIFT_LEN_HI:
			/* only return upper word */
			val = (val & 0xffff0000) / 0x10000;
			break;
	}
	return (val);
}

/****************************************************************************
 * Function: set_device_definition
 ****************************************************************************/
void set_device_definition(unsigned int file, unsigned int modbus_reg, unsigned char * data)
{
	unsigned int addr = 0;
	fic val;
	config_super *cs = &cfg_super;
	unsigned int mod_device;

	addr = ((file - 1) * 10000) + modbus_reg;
	mod_device = addr / 1000;
	val.c[0] = *data++;
	val.c[1] = *data++;
	val.c[2] = *data++;
	val.c[3] = *data++;

	if ((modbus_reg % 1000) == 999)
		cs->rem_device_defs[mod_device].id = val.i;
	else
		cs->rem_device_defs[mod_device].sub_device = val.i;

}

/****************************************************************************
 * Function: read_device_definition
 ****************************************************************************/
void read_device_definition(unsigned int file, unsigned int modbus_reg)
{
	unsigned int addr = 0;
	fic val;
	config_super *cs = &cfg_super;
	unsigned int mod_device;

	addr = ((file - 1) * 10000) + modbus_reg;
	mod_device = addr / 1000;

	if ((modbus_reg % 1000) == 999)
		val.i = cs->rem_device_defs[mod_device].id;
	else
		val.i = cs->rem_device_defs[mod_device].sub_device;

	out_packet[out_count++] = (unsigned char) val.c[0];
	out_packet[out_count++] = (unsigned char) val.c[1];
	out_packet[out_count++] = (unsigned char) val.c[2];
	out_packet[out_count++] = (unsigned char) val.c[3];
}


/****************************************************************************
 * Function: write_modbus_reg
 ****************************************************************************/
void write_modbus_reg(unsigned int modbus_reg, int value)
{
	unsigned int epcc_reg;
	unsigned int temp_reg;
	register float format_mult;
	fic val;

	epcc_reg = modbus_2_epcc(modbus_reg);  /* change to epcc register */
	temp_reg = epcc_reg;

	format_mult = find_multiplier(epcc_reg);

	if ((epcc_reg % TYPES_PER) >= R_INTEGER)
	{
		val.i = (unsigned int) (((float) value / format_mult) + .5);
	}
	else
	{
		val.f = (float) value / format_mult;
	}

	write_remote_var(&pd, epcc_reg, val);
	val.i = (unsigned int)value;/* only here for debug_rem_write statement */
	debug_rem_write(&pd, epcc_reg, &val, INT);
}

/****************************************************************************
 * Function: get_byte_count
 ****************************************************************************/
void get_byte_count(void)
{
	msg.byte_count = ReadData[2];

	inbfr[in_count++] = (unsigned char) msg.byte_count;
	msg_state = S_GET_DATA;
}

/****************************************************************************
 * Function: get_data_block
 ****************************************************************************/
void get_data_block(void)
{
	config_super *cs = &cfg_super;

	switch (msg.function)
	{
		case READ_HOLD_REG:
			if (cs->using_32bit_mode_holding_reg)
				pd.sixteen_bit_mode = FALSE;
			else
				pd.sixteen_bit_mode = TRUE;
			fcn_read_reg();
			break;

		case PRESET_SINGLE_REG:
			if (cs->using_32bit_mode_holding_reg)
				pd.sixteen_bit_mode = FALSE;
			else
				pd.sixteen_bit_mode = TRUE;
			fcn_write_reg();
			break;

		case READ_GENERAL_REF:	/* tested */
			pd.sixteen_bit_mode = FALSE;
			fcn_read_general_ref();
			break;

		case WRITE_GENERAL_REF:		/* tested */
			pd.sixteen_bit_mode = FALSE;
			fcn_write_general_ref();
			break;

		case PRESET_MULT_REG:
			if (cs->using_32bit_mode_holding_reg)
				pd.sixteen_bit_mode = FALSE;
			else
				pd.sixteen_bit_mode = TRUE;
			fcn_write_mult();
			break;

		default:
			break;
	}
}

/****************************************************************************
 * Function: fcn_read_reg
 ****************************************************************************/
void fcn_read_reg(void)
{
	register int len;
	unsigned char *ptr;
	int byte_count;
	int file_num, start_reg;
	config_super *cs=&cfg_super;

#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL; /* YYY */
#endif

	modbus_type = MODBUS_RTU;

	if (!cs->using_32bit_mode_holding_reg) /* 16 bit mode HOLDING REGISTERS */
	{
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
				len = 6;
			else
				len = 12;

			brsmemcpy ((UDINT)&temp_bfr, (UDINT)&ReadData[2], len); /* YYY */

			ptr = &inbfr[in_count];

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT)&inbfr[in_count], (UDINT)temp_bfr, 6);
				in_count += 6;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}


			if (in_count >= BFR_SIZE)
			{
				return;
			}

			msg.start_reg = (int) read_int16(ptr); /* read start register */
			ptr += 2;


			msg.num_regs = (int) read_int16(ptr);  /* read number of registers */
			ptr += 2;

			if (modbus_type == MODBUS_RTU)
			{
				byte_count = msg.num_regs * 2;
				msg.err_chk = (int) read_int16(ptr);   /* read 16 bit CRC */
				ptr += 2;
			}
			else
			{
				byte_count = msg.num_regs * 2;
				msg.err_chk = (int) inbfr[6]; /* read 8 bit checksum */
			}

			if (check_msg())
			{
				in_count = 0;
				out_count = 0;
				fill_addr_fcn(byte_count);

				fill_data_packet(msg.start_reg, msg.num_regs);
				fill_error_check();
			}
		}
		else	   /* TCP */
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;

			if (in_count >= BFR_SIZE)
			{
				return;
			}

			msg.start_reg = *ptr * 256 + *(ptr+1); /* read start register */
			ptr += 2;
			msg.num_regs = *ptr * 256 + *(ptr+1);  /* read number of registers */
			ptr += 2;

			byte_count = msg.num_regs * 2;

			in_count = 0;
			out_count = 0;

			fill_addr_fcn(byte_count);

			fill_data_packet(msg.start_reg, msg.num_regs);
		}
	}	/* end of 16 bit mode */
	else  /* start of 32 bit mode */
	{
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
				len = 6;
			else
				len = 12;

			brsmemcpy ((UDINT)&temp_bfr,(UDINT) &ReadData[2], len); /* YYY */
			ptr = &inbfr[in_count];

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT)&inbfr[in_count], (UDINT)temp_bfr, 6);
				in_count += 6;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}
			if (in_count >= BFR_SIZE)
			{
				return;
			}
			msg.start_reg = (int) read_int16(ptr); /* read start register */
			ptr += 2;
			msg.num_regs = (int) read_int16(ptr);  /* read number of registers */
			ptr += 2;

			if (modbus_type == MODBUS_RTU)
			{
				byte_count = msg.num_regs * 2;
				msg.err_chk = (int) read_int16(ptr);   /* read 16 bit CRC */
				ptr += 2;
			}
			else
			{
				byte_count = msg.num_regs * 2;
				msg.err_chk = (int) inbfr[6]; /* read 8 bit checksum */
			}

			if (check_msg())
			{
				in_count = 0;
				out_count = 0;

				file_num = msg.start_reg / 10000 + 1;
				start_reg = msg.start_reg % 10000;
				fill_addr_fcn(byte_count);
				fill_read_reg_data_packet_32bit(file_num, start_reg, msg.num_regs);
				fill_error_check();
			}
		}
		else   /* TCP */
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;

			if (in_count >= BFR_SIZE)
			{
				return;
			}

			msg.start_reg = *ptr * 256 + *(ptr+1); /* read start register */
			ptr += 2;
			msg.num_regs = *ptr * 256 + *(ptr+1);  /* read number of registers */
			ptr += 2;
			byte_count = msg.num_regs * 2;

			file_num = msg.start_reg / 10000 + 1;
			start_reg = msg.start_reg % 10000;

			in_count = 0;
			out_count = 0;
			fill_addr_fcn(byte_count);
			fill_read_reg_data_packet_32bit(file_num, start_reg, msg.num_regs);
		}
	}  /* end of 32 bit mode */
}

/****************************************************************************
 * Function: fill_read_reg_data_packet_32bit
 ****************************************************************************/
void fill_read_reg_data_packet_32bit(int file, int start_reg, int num_regs)
{
	config_super *cs = &cfg_super;

	register unsigned int curr_reg;
	register int i;
	unsigned int epcc_reg;
	fic val;
	int point_type;
	int status=0, dev;
	int read_string = FALSE;

	/* convert 6 digit address to PCC type 2 register */
	curr_reg = start_reg;
	for (i = 0; i < num_regs; i++)
	{
		dev = (((file - 1) * 10000) + curr_reg) / 1000;
		if ((dev == REM_RESIN_NAME_ID) || (dev == REM_RECIPE_NAME_ID))
			read_string = TRUE;
		else
			read_string = FALSE;

		if (((curr_reg % 2) && (curr_reg > 0)) || read_string)
		{
			if (((curr_reg % 1000) == 999) || ((curr_reg % 1000) == 997))
			{
				read_device_definition(file, curr_reg);
			}
			else
			{
				epcc_reg = modbus_to_pcc2(file, curr_reg);
				status = read_remote_var_2(&pd, epcc_reg, &val, &point_type);
				shr_global.rem_diag.last_point_processed = epcc_reg; /* Copy for service diagnostic displays */            
				debug_rem_read(&pd, epcc_reg, &val, point_type);

				switch (point_type)
				{
#if 0
					case DOUBLE: // 64 bit
						if (status == INVALID_POINT)
							val.d = 0.0;

						if (out_count >= BFR_SIZE-8)
						{
							return;
						}
						/* this is OK 11/4/2010 beb */
						out_packet[out_count++] = (unsigned char) val.c[1];
						out_packet[out_count++] = (unsigned char) val.c[0];
						out_packet[out_count++] = (unsigned char) val.c[3];
						out_packet[out_count++] = (unsigned char) val.c[2];

						out_packet[out_count++] = (unsigned char) val.c[5];
						out_packet[out_count++] = (unsigned char) val.c[4];
						out_packet[out_count++] = (unsigned char) val.c[7];
						out_packet[out_count++] = (unsigned char) val.c[6];
						break;
#endif
					case FLOAT:
						if (status == INVALID_POINT)
							val.f = 0.0;

						if (out_count >= BFR_SIZE-4)
						{
							return;
						}
						/* this is OK 1/31/2008 */
						out_packet[out_count++] = (unsigned char) val.c[1];
						out_packet[out_count++] = (unsigned char) val.c[0];
						out_packet[out_count++] = (unsigned char) val.c[3];
						out_packet[out_count++] = (unsigned char) val.c[2];
						HMI.pg212_uiRegisterReturn = 0;
						HMI.pg212_fRegisterReturn = val.f;

						break;
					case INT:
						if (status == INVALID_POINT)
						{
							val.i = 0;
						}

						if (out_count >= BFR_SIZE-4)
						{
						return;
						}
					//	 This is OK 1/31/2008 
						if (cs->ucModbusSwapBytes) 
						{
							out_packet[out_count++] = (unsigned char) val.c[1];
							out_packet[out_count++] = (unsigned char) val.c[0];
							out_packet[out_count++] = (unsigned char) val.c[3];
							out_packet[out_count++] = (unsigned char) val.c[2];

						}
						else
						{
							out_packet[out_count++] = (unsigned char) val.c[3];
							out_packet[out_count++] = (unsigned char) val.c[2];
							out_packet[out_count++] = (unsigned char) val.c[1];
							out_packet[out_count++] = (unsigned char) val.c[0];
						}
						HMI.pg212_uiRegisterReturn = val.i;
						HMI.pg212_fRegisterReturn = 0.0;
						break;
					case RESIN_STRING:
						/* 2 chars. per register */
						if (out_count >= BFR_SIZE-2)
						{
							return;
						}
						if ((curr_reg % 2) == 0)
						{
							out_packet[out_count++] = (unsigned char) val.c[0];
							out_packet[out_count++] = (unsigned char) val.c[1];
						}
						else
						{
							out_packet[out_count++] = (unsigned char) val.c[2];
							out_packet[out_count++] = (unsigned char) val.c[3];
						}

						break;

					default:
						break;
				}
			}
		}
		curr_reg++;
	}
}


/****************************************************************************
 * Function: ascii_to_binary
 ****************************************************************************/
void ascii_to_binary(unsigned char *ascii, unsigned char *binary, int number)
{
	register int j;

	for (j = 0; j < number; ++j)
	{
		*binary++ = (unsigned char) ascii_to_byte(ascii);
		ascii += 2;
	}
}

/****************************************************************************
 * Function: check_msg
 ****************************************************************************/
int check_msg()
{
	register unsigned int chk;
	int ret_val = FALSE;

	if (modbus_type == MODBUS_RTU)
	{
		chk = (unsigned int) CalcCrc_16(inbfr, in_count - 2, 0xA001);

		if (chk == msg.err_chk)
			ret_val = TRUE;
	}
	else
	{
		chk = calc_checksum(inbfr, in_count);

		chk &= 0xff;

		if (chk == 0x0)
			ret_val = TRUE;
	}

	return ret_val;
}

/****************************************************************************
 * Function: fill_error_check
 ****************************************************************************/
void fill_error_check(void)
{
	int temp;

	if (modbus_type == MODBUS_RTU)
	{
		temp = (int) CalcCrc_16(out_packet, out_count, 0xA001);
		out_packet[out_count++] = temp / 256; /* this is OK! */
		out_packet[out_count++] = temp % 256;
	}
	else   /* ? */
	{
		temp = (int) calc_checksum(out_packet, out_count);
		com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);   /* LRC is two's complement of checksum */
	}

	if (out_count >= BFR_SIZE)
	{
		return;
	}
	msg_state = S_SEND_RESPONSE;
}

/****************************************************************************
 * Function: ascii_to_byte
 ****************************************************************************/
int ascii_to_byte(unsigned char *ascii)
{
	register int i;
	register int x;

	/* convert the ascii character to a number */
	i = (int) (*ascii++ - '0');
	if (i > 9)
		i -= 7;

	x = i * 0x10;

	/* convert the ascii character to a number */
	i = (int) (*ascii++ - '0');
	if (i > 9)
		i -= 7;

	x += i;

	return x;
}

/****************************************************************************
 * Function: fcn_read_general_ref
 ****************************************************************************/
void fcn_read_general_ref(void)
{
	register int len = 0;
	unsigned char *ptr;
	int num_groups, group;

	out_count = 0;

#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL;
#endif

	modbus_type = MODBUS_RTU;

	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		/* get total number of bytes in reference groups */
		get_byte_count();

		if (modbus_type == MODBUS_RTU)
		{
			brsmemcpy ((UDINT)&inbfr[in_count], (UDINT)&ReadData[3], msg.byte_count + 2); /* YYY */
		}

		ptr = &inbfr[in_count];

		if (modbus_type == MODBUS_RTU)
		{
			in_count += msg.byte_count + 2;
			msg.err_chk = (int) read_int16(&inbfr[in_count - 2]); /* read 16 bit CRC */
		}
		else
		{
			in_count += msg.byte_count + 1;
			msg.err_chk = (int) inbfr[in_count - 1]; /* read 8 bit checksum */
		}


	}
	else
	{
		ptr = &inbfr[3];
		in_count += msg.byte_count;
	}
	if (in_count >= BFR_SIZE)
	{
		return;
	}

	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		/* msg arrived ok, now process it */
		if (modbus_type == MODBUS_RTU)
			len = 7;
		else
			len = 14;
	}

	/* fill in the addr, func bytes of response and initialize count to 0 */
	fill_addr_fcn(0);

	num_groups = msg.byte_count / 7;
	num_group = num_groups; /* YYY */

	for (group = 0; group < num_groups; group++)
	{
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type != MODBUS_RTU)
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], len);
			}
		}

		/* get reference type (1 byte) - must be 6 */
		msg.ref_group[group].reference_type = *ptr;  /* read reference type */
		ptr += 1;
		if (msg.ref_group[group].reference_type != 6)
		{
			return;
		}
		/* get file number    (2 bytes) */
		msg.ref_group[group].file_number = (int) read_int16(ptr);   /* read file number */
		ptr += 2;

		file_number = msg.ref_group[group].file_number; /* YYY */

		/* get starting addr. (2 bytes) */
		msg.ref_group[group].start_reg = (int) read_int16(ptr);  /* read start register */
		ptr += 2;

		start_register = msg.ref_group[group].start_reg;

		/* get register count (2 bytes) */
		msg.ref_group[group].num_regs = (int) read_int16(ptr);   /* read number of registers */
		ptr += 2;

		num_reg = msg.ref_group[group].num_regs;

		/* fill in the sub-response(group) byte count and reference type */
		fill_sub_hdr(group);
		fill_ref_data_packet(msg.ref_group[group].file_number, msg.ref_group[group].start_reg, msg.ref_group[group].num_regs);
		ptr += msg.ref_group[group].num_regs * 2;
	}

	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		fill_error_check();

	in_count = 0;
}

/****************************************************************************
 * Function: fcn_write_general_ref
 ****************************************************************************/
void fcn_write_general_ref(void)
{
	int group;
	register int len=0;
	unsigned char *ptr;
	unsigned int bytes_left;

	out_count = 0;
	/* YYY */
#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL;
#endif

	modbus_type = MODBUS_RTU;

	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		/* get total number of bytes in reference groups */
		get_byte_count();

		if (modbus_type == MODBUS_RTU)
		{
			brsmemcpy((UDINT) &inbfr[in_count],(UDINT) &ReadData[3], msg.byte_count + 2); /* YYY */
		}

		ptr = &inbfr[in_count];

		if (modbus_type == MODBUS_RTU)
		{
			in_count += msg.byte_count + 2;
		}
		else
		{
			in_count += msg.byte_count + 1;
		}
	}
	else
	{
		ptr = &inbfr[3];
		in_count += msg.byte_count;
	}

	if (in_count >= BFR_SIZE)
	{
		return;
	}

	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		/* read crc */
		if (modbus_type == MODBUS_RTU)
			msg.err_chk = (int) read_int16(&inbfr[in_count - 2]); /* read 16 bit CRC */
		else
			msg.err_chk = (int) inbfr[in_count - 1]; /* read 8 bit checksum */

		/* msg arrived ok, now process it */
		if (modbus_type == MODBUS_RTU)
			len = 7;
		else
			len = 14;

		bytes_left = msg.byte_count;
	}
	else
		bytes_left = msg.byte_count - 3;

	group = 0;

	while (bytes_left)
	{
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type != MODBUS_RTU)
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], len);
			}
		}

		/* get reference type (1 byte) - must be 6 */
		msg.ref_group[group].reference_type = *ptr;  /* read reference type */
		ptr += 1;

		if (msg.ref_group[group].reference_type != 6)
		{
			return;
		}

		/* get file number    (2 bytes) */
		msg.ref_group[group].file_number = (int) read_int16(ptr);   /* read file number */
		ptr += 2;

		/* get starting addr. (2 bytes) */
		msg.ref_group[group].start_reg = (int) read_int16(ptr);  /* read start register */
		ptr += 2;

		/* get register count (2 bytes) */
		msg.ref_group[group].num_regs = (int) read_int16(ptr);   /* read number of registers */
		ptr += 2;

		/* fill in the sub-response(group) byte count and reference type */
		/*fill_sub_hdr(group); */
		write_ref_data(&msg, group, ptr);
		ptr += msg.ref_group[group].num_regs * 2;
		bytes_left -= 7 + (msg.ref_group[group].num_regs * 2);
		group++;
	}

	/* echo the msg here */
	brsmemcpy((UDINT) out_packet, (UDINT)inbfr, (unsigned int) in_count);
	out_count = in_count;
	/*fill_error_check(); */

	in_count = 0;

	msg_state = S_SEND_RESPONSE;
}

/****************************************************************************
 * Function: fill_addr_fcn
 ****************************************************************************/
void fill_addr_fcn(int count)
{
	if (out_count >= BFR_SIZE-3)
	{
		return;
	}
	out_packet[out_count++] = (unsigned char) msg.addr;
	out_packet[out_count++] = (unsigned char) msg.function;
	out_packet[out_count++] = (unsigned char) count;

	msg_state = S_FILL_DATA;
}

/****************************************************************************
 * Function: fill_data_packet
 ****************************************************************************/
void fill_data_packet(unsigned int start_reg, int num_regs)
{
	unsigned int curr_reg;
	register int i;
	int temp;
	float ftemp;
	unsigned int epcc_reg;
	fic val;
	int point_type;
	int status=0;
	float format_mult;
	unsigned char *ptr;

#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL; /* YYY */
#endif

	modbus_type = MODBUS_RTU;

	curr_reg = start_reg;
	for (i = 0; i < num_regs; ++i)
	{
		epcc_reg = modbus_2_epcc(curr_reg); /* change to epcc register */
		status = read_remote_var(&pd, epcc_reg, &val, &point_type);
		debug_rem_read(&pd, epcc_reg, &val, point_type);

		format_mult = find_multiplier(epcc_reg);

		switch (point_type)
		{
			case FLOAT:
				if (status == INVALID_POINT)
					val.f = 0.0;
				ftemp = val.f * format_mult;
				temp = (int) (ftemp + .5);
				break;

			case INT:
				if (status == INVALID_POINT)
					val.i = 0;
				temp = (int) val.i * (int) (format_mult + .5);
				break;

			default:
				temp = 0;
		}

		temp = (int) translate_value(epcc_reg, (unsigned int) temp);
		ptr = &(out_packet[out_count]);
		out_packet[out_count++] = (temp >> 8) & 0xff; /* tested OK */
		out_packet[out_count++] = temp & 0xff;
		if (out_count >= BFR_SIZE)
		{
			return;
		}
		curr_reg++;
	}
	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		if (modbus_type == MODBUS_RTU)
		{
			temp = (int) CalcCrc_16(out_packet, out_count, 0xA001);
			out_packet[out_count++] = (temp >> 8) & 0xff;
			out_packet[out_count++] = temp & 0xff;

			if (out_count >= BFR_SIZE)
			{
				return;
			}
		}
		else
		{
			temp = (int) calc_checksum(out_packet, out_count);

			/* LRC is two's complement of checksum */
			com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);
		}
	}
	brsmemcpy((UDINT) &ReadData, (UDINT)out_packet, out_count); /* YYY */

	msg_state = S_SEND_RESPONSE;
}
/****************************************************************************
 * Function: fcn_write_mult
 ****************************************************************************/
void fcn_write_mult(void)
{
	register int i;
	unsigned char *ptr;
	register unsigned int temp;
	int byte_count;
	int item_count;
	int data_start;
	int quantity_of_output;
	int file_num, start_reg;
	int num_regs;
	config_super *cs=&cfg_super;

#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL;
#endif

	modbus_type = MODBUS_RTU;
	if (!cs->using_32bit_mode_holding_reg) /* 16 bit mode HOLDING REGISTERS */
	{
		ptr = &inbfr[in_count];
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
				i = 5;
			else
				i = 10;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[2], i); /* YYY tested OK */

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, 5);
				in_count += 5;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}
		}
		else
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;
		}

		if (in_count >= BFR_SIZE)
		{
			return;
		}

		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			msg.start_reg = (int) read_int16(ptr);
			ptr += 2;
			msg.num_regs = (int) read_int16(ptr);
			ptr += 2;
			item_count = (int) inbfr[6];
		}
		else
		{
			msg.start_reg = *ptr * 256 + *(ptr+1);
			ptr += 2;
			msg.num_regs = *ptr * 256 + *(ptr+1);
			item_count = inbfr[6];
		}

		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
				byte_count = item_count + 2;
			else
				byte_count = item_count * 2 + 2;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[2], byte_count); /* YYY */
		}
		else
		{
			byte_count = item_count + 2;
		}

		data_start = in_count;

		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, (UDINT) byte_count);
				in_count += byte_count;
				msg.err_chk = (int) read_int16(&inbfr[in_count - 2]);
			}
			else
			{
				/* convert all the data and error check field to binary */
				ascii_to_binary(temp_bfr, &inbfr[in_count], item_count + 1);
				in_count += item_count;
				msg.err_chk = inbfr[in_count];
				++in_count;
			}

			ptr = &inbfr[data_start];
			temp = msg.start_reg;
			for (i = 0; i < msg.num_regs; ++i)
			{
				write_modbus_reg(temp++, (unsigned int) read_int16(ptr));
				ptr += 2;
			}

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			out_packet[out_count++] = (msg.start_reg >> 8) & 0xff;
			out_packet[out_count++] = msg.start_reg & 0xff;
			out_packet[out_count++] = (msg.num_regs >> 8) & 0xff;
			out_packet[out_count++] = msg.num_regs & 0xff;

			if (modbus_type == MODBUS_RTU)
			{
				temp = (int) CalcCrc_16(out_packet, out_count, (unsigned short) 0xA001);
				out_packet[out_count++] = (temp >> 8) & 0xff;
				out_packet[out_count++] = temp & 0xff;
			}
			else
			{
				temp = calc_checksum(out_packet, out_count);
				com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);   /* LRC is two's complement of checksum */
			}
			msg_state = S_SEND_RESPONSE;
		}
		else
		{
			in_count += msg.byte_count;
			ptr = &inbfr[7];

			temp = msg.start_reg;
			for (i = 0; i < msg.num_regs; ++i)
			{
				write_modbus_reg(temp++, (unsigned int) read_int16(ptr));
				ptr += 2;
			}

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			out_packet[out_count++] = (msg.start_reg >> 8) & 0xff;
			out_packet[out_count++] = msg.start_reg & 0xff;
			out_packet[out_count++] = (msg.num_regs >> 8) & 0xff;
			out_packet[out_count++] = msg.num_regs & 0xff;

			msg_state = S_SEND_RESPONSE;

		}
	}	/* end of 16 bit mode */
	else	/* 32 bit mode */
	{
		/* start of 32 bit mode for SERIAL */
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			ptr = &inbfr[in_count];
			if (modbus_type == MODBUS_RTU)
				i = 5;
			else
				i = 10;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[2], i); /* YYY tested OK */

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, 5);
				in_count += 5;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}

			msg.start_reg = (int) read_int16(ptr);
			ptr += 2;
			quantity_of_output = (int)read_int16(ptr);
			item_count = (int) inbfr[6];
			if (modbus_type == MODBUS_RTU)
				byte_count = item_count;
			else
				byte_count = item_count * 2;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[7], byte_count); /* tested OK 2/1/2008 */
			data_start = in_count;

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, (UDINT) byte_count);
				in_count += byte_count;
				msg.err_chk = (int) read_int16(&inbfr[in_count - 2]);
			}
			else
			{
				/* convert all the data and error check field to binary */
				ascii_to_binary(temp_bfr, &inbfr[in_count], item_count + 1);
				in_count += item_count;
				msg.err_chk = inbfr[in_count];
				++in_count;
			}

			ptr += 3;
			file_num = msg.start_reg / 10000 + 1;
			start_reg = msg.start_reg % 10000;
			num_regs = ceil((float)quantity_of_output/2);
			write_multi_data_32bit(file_num, start_reg, ptr, num_regs);

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			out_packet[out_count++] = (msg.start_reg >> 8) & 0xff;
			out_packet[out_count++] = msg.start_reg & 0xff;
			out_packet[out_count++] = (quantity_of_output >> 8) & 0xff;
			out_packet[out_count++] = quantity_of_output & 0xff;

			if (modbus_type == MODBUS_RTU)
			{
				temp = (int) CalcCrc_16(out_packet, out_count, (unsigned short) 0xA001);
				out_packet[out_count++] = (temp >> 8) & 0xff;
				out_packet[out_count++] = temp & 0xff;
			}
			else
			{
				temp = calc_checksum(out_packet, out_count);
				com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);   /* LRC is two's complement of checksum */
			}
			msg_state = S_SEND_RESPONSE;
		}   /* end of 32 bit serial mode */
		else  /* start of 32 bit TCP mode */
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;

			msg.start_reg = (int) read_int16(ptr);
			ptr += 2;
			quantity_of_output = (int)read_int16(ptr);

			ptr += 3;

			file_num = msg.start_reg / 10000 + 1;
			start_reg = msg.start_reg % 10000;
			num_regs = ceil((float)quantity_of_output/2);
			write_multi_data_32bit(file_num, start_reg, ptr, num_regs);
			
			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) msg.start_reg, 2);
			out_count += 2;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) quantity_of_output, 2);
			out_count += 2;

			msg_state = S_SEND_RESPONSE;
		}
	}	/* end of 32 bit mode */
}

/****************************************************************************
 * Function: write_multi_data_32bit
 ****************************************************************************/
void write_multi_data_32bit(int file, int start_reg, unsigned char * data, int num_regs)
{
	register unsigned int curr_reg;
	register unsigned int i;
	unsigned int epcc_reg, temp_reg;
	fic val;
	int dev,point_type;
	int write_string = FALSE;
	char resin_or_recipe_data=FALSE;
	config_super *cs=&cfg_super;

	curr_reg = start_reg;

	for (i = 0; i < (unsigned)num_regs; ++i)
	{
		dev = (((file - 1) * 10000) + curr_reg) / 1000;

		if ((dev == REM_RESIN_NAME_ID) || (dev == REM_RECIPE_NAME_ID))
			write_string = TRUE;
		else
			write_string = FALSE;

		if ((dev == REM_RESIN_DATA_ID) || (dev == REM_RECIPE_DATA_ID))
			resin_or_recipe_data = TRUE;

      	/*if (((curr_reg % 2) && (curr_reg > 0)) || write_string)
      	{	*/
		if (((curr_reg % 1000) == 999) || ((curr_reg % 1000) == 997))
		{
			set_device_definition(file, curr_reg, data);
			data += 4;
		}
		else
		{
			/* translate modbus 6-digit address to pcc address */
			epcc_reg = modbus_to_pcc2(file, curr_reg);
			shr_global.rem_diag.last_point_processed = epcc_reg; /* Copy for service diagnostic displays */            
               
			temp_reg = epcc_reg;
			if (!write_string)
			{
				if (translate_remote_register(&pd, &epcc_reg) != 2)
				{
					if( (epcc_reg % 1000) == 21)
						point_type = DOUBLE;
					if (((epcc_reg % 1000) >= REG_INTEGER) && !resin_or_recipe_data)
						point_type = INT;
						else
						point_type = FLOAT;

					/* normal register, read in data value for this register */
					if (point_type == INT)
					{
						if (cs->ucModbusSwapBytes)  
						{
							val.c[1] = *data++;
							val.c[0] = *data++;
							val.c[3] = *data++;
							val.c[2] = *data++;
						}
						else
						{
							val.c[3] = *data++;
							val.c[2] = *data++;
							val.c[1] = *data++;
							val.c[0] = *data++;
						}
						HMI.pg212_uiRegisterReturn = val.i;
						HMI.pg212_fRegisterReturn = 0.0;
					}
					else if (point_type == DOUBLE)
					{
						val.c[1] = *data++;
						val.c[0] = *data++;
						val.c[3] = *data++;
						val.c[2] = *data++;
						val.c[5] = *data++;
						val.c[4] = *data++;
						val.c[7] = *data++;
						val.c[6] = *data++;
					}
					else
					{
						val.c[1] = *data++;
						val.c[0] = *data++;
						val.c[3] = *data++;
						val.c[2] = *data++;
						HMI.pg212_fRegisterReturn = val.f;
						HMI.pg212_uiRegisterReturn = 0.0;
					}
				}
				else // UCB
				{
					epcc_reg = temp_reg;
					if (cs->ucModbusSwapBytes)  
					{
						val.c[1] = *data++;
						val.c[0] = *data++;
						val.c[3] = *data++;
						val.c[2] = *data++;
					}
					else
					{
						val.c[3] = *data++;
						val.c[2] = *data++;
						val.c[1] = *data++;
						val.c[0] = *data++;
					}
					HMI.pg212_uiRegisterReturn = val.i;
					HMI.pg212_fRegisterReturn = 0.0;
				}
				write_remote_var_2(&pd, epcc_reg, val);
			}
			else
			{
				point_type = STRINGS;
				val.c[0] = *data++;
				val.c[1] = *data++;
				val.c[2] = *data++;
				val.c[3] = *data++;
				write_remote_var_2(&pd, epcc_reg, val);
			}
			debug_rem_write(&pd, epcc_reg, &val, point_type);
		}
		/* }      */
		curr_reg += 2;
	}
}

/****************************************************************************/
/****************************************************************************/
void fcn_write_reg(void)
{
	register int len;
	unsigned char *ptr;
	int value;
	register int temp;
	int byte_count;
	int item_count;
	int data_start;
	int i;
	int file_num, start_reg;
	config_super *cs=&cfg_super;

#if 0
	MODBUS_CONN_TYPE = MODBUS_SERIAL;
#endif

	modbus_type = MODBUS_RTU;

	if (!cs->using_32bit_mode_holding_reg) /* 16 bit mode HOLDING REGISTERS */
	{
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
				len = 6;
			else
				len = 12;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[2], (UDINT)len); /* YYY tested OK */
		}

		ptr = &inbfr[in_count];

		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, 6);
				in_count += 6;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}
		}
		else
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;
		}

		msg.start_reg = (int) read_int16(ptr);
		ptr += 2;
		msg.num_regs = 1;
		value = (int) read_int16(ptr);
		ptr += 2;

		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			if (modbus_type == MODBUS_RTU)
			{
				msg.err_chk = (int) read_int16(ptr);
				ptr += 2;
			}
			else
			{
				msg.err_chk = (int) inbfr[6];
			}
			/*if (check_msg())*/
			{
				write_modbus_reg(msg.start_reg, value);
				out_count = 0;
				out_packet[out_count++] = (unsigned char) msg.addr;
				out_packet[out_count++] = (unsigned char) msg.function;
				out_packet[out_count++] = (msg.start_reg >> 8) & 0xff;
				out_packet[out_count++] = msg.start_reg & 0xff;
				out_packet[out_count++] = (value >> 8) & 0xff;
				out_packet[out_count++] = value & 0xff;

				if (modbus_type == MODBUS_RTU)
				{
					temp = (int) CalcCrc_16(out_packet, out_count, 0xA001);
					out_packet[out_count++] = (temp >> 8) & 0xff;
					out_packet[out_count++] = temp & 0xff;
				}
				else
				{
					temp = (int) calc_checksum(out_packet, out_count);
					com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);   /* LRC is two's complement of checksum */
				}

				msg_state = S_SEND_RESPONSE;
			}
		}
		else
		{

			write_modbus_reg(msg.start_reg, value);

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) msg.start_reg, 2);
			out_count += 2;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) value, 2);
			out_count += 2;

			msg_state = S_SEND_RESPONSE;
		}
	} /* end of 16 bit mode */
	else	/* start of 32 bit mode */
	{
		/* start of 32 bit mode SERIAL */
		if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
		{
			ptr = &inbfr[in_count];

			if (modbus_type == MODBUS_RTU)
				i = 5;
			else
				i = 10;

			brsmemcpy((UDINT) &temp_bfr, (UDINT)&ReadData[2], (UDINT)i); /* YYY tested OK */

			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, 5);
				in_count += 5;
			}
			else
			{
				ascii_to_binary(temp_bfr, &inbfr[in_count], 5);
				in_count += 5;
			}
			if (in_count >= BFR_SIZE)
			{
				return;
			}

			msg.start_reg = (int) read_int16(ptr);
			ptr += 2;
			value = (int)read_int16(ptr);

			item_count = (int) inbfr[6];
			if (modbus_type == MODBUS_RTU)
				byte_count = item_count;
			else
				byte_count = item_count * 2;

			data_start = in_count;
			if (modbus_type == MODBUS_RTU)
			{
				brsmemcpy((UDINT) &inbfr[in_count], (UDINT)temp_bfr, (UDINT) byte_count);
				in_count += byte_count;
				msg.err_chk = (int) read_int16(&inbfr[in_count - 2]);
			}
			else
			{
				/* convert all the data and error check field to binary */
				ascii_to_binary(temp_bfr, &inbfr[in_count], item_count + 1);
				in_count += item_count;
				msg.err_chk = inbfr[in_count];
				++in_count;
			}

			if (in_count >= BFR_SIZE)
			{
				return;
			}

			/*ptr += 3;*/
			file_num = msg.start_reg / 10000 + 1;
			start_reg = msg.start_reg % 10000;
			write_multi_data_32bit(file_num, start_reg, ptr, 1);

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			out_packet[out_count++] = (msg.start_reg >> 8) & 0xff;
			out_packet[out_count++] = msg.start_reg & 0xff;
			out_packet[out_count++] = (value >> 8) & 0xff;
			out_packet[out_count++] = value & 0xff;

			if (modbus_type == MODBUS_RTU)
			{
				temp = (int) CalcCrc_16(out_packet, out_count, (unsigned short) 0xA001);
				out_packet[out_count++] = (temp >> 8) & 0xff;
				out_packet[out_count++] = temp & 0xff;
			}
			else
			{
				temp = calc_checksum(out_packet, out_count);
				com_ipwrite((unsigned char *) &out_packet[out_count++], (unsigned int) (-1 * temp), 1);
			}
			msg_state = S_SEND_RESPONSE;
		}  /* end of 32 bit mode SERIAL */
		else	/* start of 32 bit mode TCP */
		{
			ptr = &inbfr[2];
			in_count += msg.byte_count;

			msg.start_reg = (int) read_int16(ptr);
			ptr += 2;
			file_num = msg.start_reg / 10000 + 1;
			start_reg = msg.start_reg % 10000;

			msg.num_regs = 1;
			value = (int) read_int16(ptr);

			write_multi_data_32bit(file_num, start_reg, ptr, 1);

			out_count = 0;
			out_packet[out_count++] = (unsigned char) msg.addr;
			out_packet[out_count++] = (unsigned char) msg.function;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) msg.start_reg, 2);
			out_count += 2;
			com_ipwrite((unsigned char *) &out_packet[out_count], (unsigned) value, 2);
			out_count += 2;

			msg_state = S_SEND_RESPONSE;
		}	/* end of 32 bit mode TCP */
	}	/* end of 32 bit mode */
}

/****************************************************************************
 *                                                                          *
 * Function: send_response
 * Filename: modbus.c    Date: 8/17/00   Author:
 * Input:
 * Returns:
 * Description:
 *                                                                          *
 ****************************************************************************/
void send_response(void)
{
	if (MODBUS_CONN_TYPE == MODBUS_SERIAL)
	{
		if (modbus_type == MODBUS_RTU)
		{
			if ((Error == 0) && (Start == 1))
			{
				Start = 0;

				/* initialize get buffer structure */
				FrameGetBufferStruct.enable = 1;
				FrameGetBufferStruct.ident = Ident;

				FRM_gbuf(&FrameGetBufferStruct);           /* get send buffer */

				SendBuffer = (UDINT*) FrameGetBufferStruct.buffer; /* get adress of send buffer */
				SendBufferLength = FrameGetBufferStruct.buflng; /* get length of send buffer */
				StatusGetBuffer = FrameGetBufferStruct.status;  /* get status */

				if (StatusGetBuffer == 0)                             /* check status */
				{
					brsmemcpy((UDINT) SendBuffer, (UDINT)out_packet, out_count * sizeof (USINT)); /* copy write data into send buffer */
					SendBufferLength = out_count;

					/* initialize write structure */
					FrameWriteStruct.ident = Ident;
					FrameWriteStruct.buffer = (UDINT) SendBuffer;
					FrameWriteStruct.buflng = SendBufferLength;
					FrameWriteStruct.enable = 1;

					FRM_write(&FrameWriteStruct);            /* write data to interface */

					StatusWrite = FrameWriteStruct.status;   /* get status */
					StartRead = 1;

					if (StatusWrite != 0)                          /* check status */
					{
						/* initialize release output buffer structure */
						FrameReleaseOutputBufferStruct.enable = 1;
						FrameReleaseOutputBufferStruct.ident = Ident;
						FrameReleaseOutputBufferStruct.buffer = (UDINT) SendBuffer;
						FrameReleaseOutputBufferStruct.buflng = SendBufferLength;
						FRM_robuf(&FrameReleaseOutputBufferStruct); /* release output buffer */

						StatusReleaseOutputBuffer = FrameReleaseOutputBufferStruct.status;

						Error = 3;                    /* set error level for FRM_write */

						if (StatusReleaseOutputBuffer != 0) /* check status */
						{
							Error = 4;            /* set error level for FRM_robuf */
						}
					}
				}
				else
					Error = 2;                            /* set error level for FRM_gbuf */
			}
		}
		msg_state = 0;
		in_count = 0;
		out_count = 0;
	}
	else
		msg_state++;
}

/****************************************************************************
 * Function: byte_to_ascii
 ****************************************************************************/
void byte_to_ascii(unsigned char * byte_buf, unsigned char * ascii_buf, int byte_count)
{
	register unsigned char nibble;

	while (byte_count-- > 0)
	{
		nibble = (unsigned char) ((*byte_buf & 0xf0) >> 4);
		if (nibble <= 9)
			*ascii_buf++ = (unsigned char) (nibble + '0');
		else
			*ascii_buf++ = (unsigned char) (nibble + 'A' - 10);   /* get upper nibble and store */

		nibble = *byte_buf & 0x0f;
		if (nibble <= 9)
			*ascii_buf++ = (unsigned char) (nibble + '0');  /* get lower nibble and store */
		else
			*ascii_buf++ = (unsigned char) (nibble + 'A' - 10);

		byte_buf++;
	}

}


