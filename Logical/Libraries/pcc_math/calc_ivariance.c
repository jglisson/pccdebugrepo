#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_ivariance(isamples *sample_data, signed short *mean)
{
    int length;
    int var;

    length = sample_data->num_taken;
    if(length > sample_data->length) length = sample_data->length;

    var = calc_ivariance_p(sample_data,mean,length);
    return var;
}


