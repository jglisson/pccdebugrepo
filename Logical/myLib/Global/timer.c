#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  timer.c

   Description: This file contains functions for maintaining timers


      $Log:   F:\Software\BR_Guardian\timer.c_v  $

 * Dec 1, 2008 JLR
 *   Added/changed debug telnet stuff
 *   use debugFlags.timer to turn on/off debug information in this file
 *
 *    Rev 1.2   Feb 27 2008 14:32:18   FMK
 * Timer functions were not working with milliseconds.
 * Time functions now only act on time and not date
 * and time.
 *
 *    Rev 1.1   Jan 24 2008 13:30:32   FMK
 * Fixed problem with Get_Time. The enable value
 * was set to 0 not allowing the FBK to work.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plc.h>
#include <sys_lib.h>
#include <astime.h>
#include "libbgdbg.h"
#include "timer.h"

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

unsigned int error;

/************************************************************************/
/*
   Function:  get_time()

   Description: This function fetches the current time in the DATE_AND_TIME
      structure. It DOES NOT include milliseconds or microseconds
                                                                        */
/************************************************************************/
DATE_AND_TIME get_date_time(void)
{
   /* note, that for the record, DATE_AND_TIME type is an unsigned long */
   unsigned char enable;
   int status;
   DTGetTime_typ DTGetTime_1;

   enable = 1;
   DTGetTime_1.enable = enable;

   DTGetTime(&DTGetTime_1);

   status = DTGetTime_1.status;
   if (status != 0)
      error = status;

   return(DTGetTime_1.DT1);
}
/************************************************************************/
/*
   Function:  get_time()

   Description: This function fetches the current time in the TIME structure
         Which does include the microseconds and milliseconds.
                                                                         */
/************************************************************************/
TIME get_time()
{
   return(clock_ms());
}
/************************************************************************/
/*
   Function:  start_time()

   Description: This function fetches the current time in the TIME
      structure. Which does include the microseconds and milliseconds.
                                                                        */
/************************************************************************/
TIME start_time(void)
{
   return(get_time());
}

/************************************************************************/
/*
   Function:  elap_time()

   Description: This function calculates the difference between the
      initial time passed in and the current time.

   The DiffT function manipulates the TIME structure.
                                                                        */
/************************************************************************/
unsigned int elap_time(TIME iTime)
{
   //unsigned int elapsed_time;
   //TIME cTime;
   //cTime = get_time();
   //elapsed_time = DiffT(cTime, iTime);
   //return(elapsed_time);

   return((unsigned int)DiffT(get_time(), iTime));
}

/************************************************************************/
/*
   Function:  elapsed_time()

   Description: This function calculates the difference between the
      first and second time values.

   The DiffT function manipulates the TIME structure.
                                                                        */
/************************************************************************/
unsigned int elapsed_time(TIME Time1, TIME Time2)
{
   //unsigned int elapsed_time;
   //elapsed_time = DiffT(Time1, Time2);
   //return(elapsed_time);

   return((unsigned int)DiffT(Time1, Time2));
}


