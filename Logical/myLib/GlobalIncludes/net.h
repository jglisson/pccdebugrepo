/************************************************************************/
/* FILE:        net.h                                                   */
/*                                                                      */
/* DESCRIPTION: network definitions file                                */
/*              has network definitions                                 */
/*                                                                      */
/*                                                                      */
/*                                                                      */
/*  Process Control Corp,   Atlanta, GA                                 */
/************************************************************************/

#define PCC_BROADCAST 0x00         /* broadcast address                    */
#define CC_ADDR      0x01          /* central computer address             */
#define NCC_ADDR     0x7e          /* 1's compliment                       */

#ifndef CMD
#define CMD             0 
#endif

#define ARG_0           2       /* Location for data start */

#ifndef STX
#define STX             0x02
#endif

#ifndef ETX
#define ETX             0x03
#endif

/************************************************************************/
/* Network device commands                                              */
/************************************************************************/
#ifndef ENQ
#define ENQ           0x05
#endif

#ifndef ACK
#define ACK           0x06
#endif

#ifndef NAK
#define NAK           0x15
#endif

#define RESET_BOARD   0x20
#define FACTORY       0x21
#define SELFTEST      0x22
#define SHUTDOWN      0x23
#define EMERGENGY     0x24
#define READ_NVM      0x25
#define WRITE_NVM     0x26
#define ID_VECTOR     0x27
#define SET_TIMEOUT   0x28
#define TURNAROUND    0x29
#define INIT_DIO      0x2C
#define SET_DO        0x2D
#define RESET_DO      0x2E
#define READ_DI       0x2F
#define SYNC          0x31
#define GET_WEIGHT    0x40
#define SYNC_WT_ON    0x41
#define SYNC_WT_OFF   0x42
#define SET_SPEED     0x43
#define GET_SPEED     0x44
#define SET_ACCEL     0x45
#define SET_DECEL     0x46
#define SET_TRIM_DAC  0x47
#define GET_AI        0x48
#define GET_PULSES    0x49
#define SET_FSPEED    0x4a
#define GET_FSPEED    0x4b
#define GET_FWEIGHT   0x4c
#define GET_FWTP      0x4d
#define READ_EPCC     0x4e
#define WRITE_EPCC    0x4f
#define FORCE_CAL     0x50      /* Force weight module to self calibrate */
#define TEMP_READ     0x51      /* Read one byte of temperature from WM */
#define AD_CLK_DIV    0x52      /* Cut-off filter divider (2 bytes) */
#define READ_EPCC_ARRAY     0x53
#define SET_FEATURE_VERSION 0x54
#define DSP_FILTER_NUM      0x55
#define DSP_ID        0x56
#define DSP_DIAG      0x57

/* the following is for self loader setup       */
#define LOADER_SETUP     0x58
#define PUMP_NUMBER      0x0003
#define NO_PUMP          0x0003
#define RECEIVER_TYPE    0x000c
#define FLAPPER          0x0000
#define KNIFEGATE        0x0004
#define POSITIVE_SHUTOFF 0x0008
#define DUST_BAGS        0x0010
#define PROPORTIONAL     0x0020
#define LOW_LEVEL_PROX   0x0040
#define RECEIVER_PROX    0x0080
#define CONTINUOUS_RUN   0x0100
#define STATION_ACTIVE   0x4000
#define RECEIVER_OR_PUMP 0x8000 /* receiver is 0, pump is 1 */

/************************************************************************/
/* Network addresses                                                    */
/************************************************************************/
#define WM_ADD_OFFSET   0x10
#define EXD_ADD_OFFSET  0x20
#define LOAD_ADD_OFFSET 0x30

/************************************************************************/
#if 0
       Following are the current 'smart cards' (C1172, C1310_revA, . . .)
       used by PCC equipment for device control.  The label after the
       define is used by software to identify the card if it is the base
       card (with no '_revx' appended), or to identify the current software
       version (code release) for specific boards if the base board name
       has a rev letter/number appended.  If the code reads the software
       version from the card and determines that its version is less
       than the number assigned to the label for that card, it will
       generate a 'software needs updating' alarm.
#endif
/************************************************************************/
#define C1172           1       /* weigh module         */
#define C1172_rev0      7
#define C1172_revA      7
#define C1181           4       /* user interface       */
#define C1181_rev0      6
#define C1181_revA      6
#define C1181_revB      6
#define C1200           2       /* exd drive            */
#define C1200_rev0      5
#define C1200_revA      5
#define C1219           3       /* quad weigh module    */
#define C1219_rev0      0
#define C1219_revA      0
#define C1247           5       /* octal drive          */
#define C1247_rev0      4
#define C1247_revA      4
#define C1247_revB      4
#define C1247_revC      4
#define C1310           6       /* quint weigh module   */
#define C1310_rev0      4
#define C1310_revA      4
#define C1310_revB      4
#define C1336           7       /* single chan weigh mod*/
#define C1336_rev0      6
#define C1336_revA      6
#define C1359           8       /* loading module       */
#define C1359_rev0      6
#define C1388           9       /* batch blender weigh/drive card */
#define C1388_rev0      1
#define C1388_revA      1
#define GRAV_BLENDER    10      /* Continuous gravimetric blender */
#define LOADING_BOARD   11      /* Saves a place for generic loading boards */
#define C1450           12      /* Single channel PWM drive */
#define C1450_rev0      2
#define C1450_revA      2
#define C1450_revB      2
#define C1532           13      /* Replacement for quint weigh module */
#define C1532_rev0      8
#define C1532_revA      8
#define C1532_revB      8
#define C1538           14      /* Replacement for single chan weigh module */
#define GUARDIAN_BLENDER 15
#define C1813           16
#define C1815           17
#define C1814           18

#define SIM_WEIGH       98 /* For any simulated weigh module */
#define SIM_WEIGH_rev0   1
#define SIM_DRIVE       99 /* For any simulated drive module */
#define SIM_DRIVE_rev0   1

/* Following find some defines for data locations in board id_vectors */
/* NOTE:  Offsets are from beginning of id vector, so have no offset for header */
#define ID_OFSET_DATE            0
#define ID_OFSET_HARDWARE_ID     9
#define ID_OFSET_HARDWARE_REV    11
#define ID_OFSET_SOFTWARE_REV    13
#define ID_OFSET_NUM_WEIGH       15
#define ID_OFSET_NUM_DIGITAL     17
#define ID_OFSET_NUM_ANALOG_IN   19
#define ID_OFSET_NUM_ANALOG_OUT  21
#define ID_OFSET_NUM_PULSE       23
#define ID_OFSET_TIMEBASE        25
#define ID_OFSET_GRAVILINK       30

/* Following are some defines for smart card ID VECTOR 'labels' */
//#define ID_VECT_HARDWARE_ID    'H'  /* Hardware ID */
//#define ID_VECT_HARDWARE_REV   'R'  /* Hardware revision */
//#define ID_VECT_SOFTWARE_REV   'S'  /* SW Version */
//#define ID_VECT_NUM_WEIGH      'W'  /* Nuber of weigh inputs */
//#define ID_VECT_NUM_DIGITAL    'D'  /* Number of drive channels */
//#define ID_VECT_NUM_ANALOG_IN  'I'  /* Number inputs */
//#define ID_VECT_NUM_ANALOG_OUT 'O'  /* Number of outputs */
//#define ID_VECT_NUM_PULSE      'P'  /* Number of pulse pickups */
//#define ID_VECT_TIMEBASE       'T'  /* Timebase */
//#define ID_VECT_GRAVILINK      'G'  /* Gravilink version/revision follows */
//#define ID_VECT_DRIVE_TYPE     'E'  /* Exd drive type 0=normal 1=up/down interface*/

/* Some bits used in second status byte for network commands.    */
/* NOTE: Do not re-use bits, even if bit is specific for only one command */
//#define NEW_FWTP 1  /* Bit to set when liw_dc has new ext rate, clear when read */



