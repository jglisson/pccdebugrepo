#include "pcc_math.h"
#include <AsBrStr.h>

signed short table_insert_space(data_table *table, signed short idx)
{
    ordered_pair *start;
    unsigned int len;

    if (idx < table->max_length && idx >= 0)
    {
        /* move all entries down by one space */
        start = &table->data[idx];
        len = (unsigned int)(table->max_length - idx - 1) * sizeof(ordered_pair);
        if (len != 0)
           brsmemmove((UDINT)(start+1), (UDINT)(start), (UDINT)len);
    }

    return(0);
}


