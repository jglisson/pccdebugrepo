#include <math.h>
#include "pcc_math.h"

float calc_std_dev(samples *sample_data,float *mean)
{
    float std_dev;
    float var;

    var = calc_variance(sample_data,mean);
    std_dev = (float)sqrt((double)var);

    return std_dev;
}


