/************************************************************************/
/*
   File:  remote.h

   Description: Remote used status definitions.


      $Log:   F:\Software\BR_Guardian\remote.h_v  $
 *
 *    Rev 1.2   Mar 30 2011 07:53 BEB
 * Removed duplicate #define TYPE_XXX, they are in mem.h

 *    Rev 1.1   Mar 21 2008 09:53:16   Barry
 * Removed FTICKSPERSEC
 *
 *    Rev 1.0   Feb 11 2008 11:19:08   YZS
 * Initial revision.


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#ifndef REMOTE_H
#define REMOTE_H

#include "mem.h"
#include "sys.h"

#define E_DEST          0       /* byte positions for euro protocol     */
#define E_SOURCE        1
#define E_BYTECOUNT     2
#define E_CMD           3
#define E_STATUS        4

#define READ            1       /* commands                             */
#define BRA             2
#define BRB             3

#ifndef WRITE
#define WRITE           4
#endif

#define BWA             5
#define BWB             6
#define ENTER           13
#define REM_STOP        14
#define MANUAL          16
#define AUTO            17

#define R_PAUSE_MODE    1       /* MODE bit masks                       */
#define R_MANUAL_MODE   4
#define R_AUTO_MODE     8
#define R_MANUAL_BACKUP 0x0010

        /* New MODE button masks */
#define R_EXT_PAUSE     0x01
#define R_EXT_MANUAL    0x04
#define R_EXT_AUTO      0x08
#define R_BLN_PAUSE     0x10
#define R_BLN_PAUSE_PENDING 0x20
#define R_BLN_MANUAL    0x40
#define R_BLN_AUTO      0x80
#define R_ALL_PAUSE     (R_BLN_PAUSE|R_EXT_PAUSE)
#define R_ALL_MANUAL    (R_BLN_MANUAL|R_EXT_MANUAL)
#define R_ALL_AUTO      (R_BLN_AUTO|R_EXT_AUTO)
// per Holger to use in Germany
#define R_BLN_NOT_CALIBRATED 0x200 
#define R_BLN_CALIBRATED 0x400

#define R_EXT_BITS      0x0f
#define R_BLN_BITS      0xf0

                                /* AUXILIARY Mode bit masks             */
#define R_AUX_CLR_ALARMS 0x8000
#define R_AUX_CLR_INVEN  0x4000
#define R_AUX_CLR_SHIFT  0x2000
#define R_RAMP_HOLD      0x1000
#define R_RATE_SPEED     0x0800
#define R_PURGE          0x0400

#define R_HO            1       /* config bit masks                     */
#define R_WIDTH         2
#define R_SCRAP         4
#define R_GRAVIFLUFF    8       /* if R_SCRAP & !R_GRAVIFLUFF, trim only*/
#define R_ADDITIVES     16
#define R_EXT           32

#define OK              0       /* return status codes                  */
#define INVALID_POINT   1
#define READ_ONLY       2
#define EWARNING        3
#define EERROR          4
#define INVALID_COMMAND 5

#define INT             0       /* type definitions for ascii printout  */
#define FLOAT           1
#define STRINGS         2
#define RESIN_STRING    3
#define RECIPE_STRING   4
#define DOUBLE          5 
 /************************************************************************************/
/* system status bit definitions (common) */
#define R_DIAG_DEBUG_PORT_UNAVAIL   0x00000001
#define R_DISPENSE_TEST_DONE        0x00000002
#define R_BAD_RECIPE                0x00000010                          /* Bit 4 */
#define R_RECIPE_VALID              0x00000020                          /* Bit 5 */
#define R_RECIPE_INVALID            0x00000040                          /* Bit 6 */
#define R_JOB_COMPLETED             0x00000080                          /* Bit 7 */
#define R_CANCEL_JOB                0x00000100                          /* Bit 8 */
#define R_RECIPE_CLEAR              0x00000200                          /* Bit 9 */
#define R_DISPENSE_TEST_ERROR       0x00001000                          /* Bit 12 */
#define R_RUN_RECIPE_CHANGED        0x00002000                          /* Bit 13 */

/* system status bit definitions (WX Code) */
#define R_BATCH_COMPLETED           0x00004000                          /* Bit 14 */
#define R_PURGE_PENDING             0x00008000                          /* Bit 15 */
#define R_PUMP_FAIL                 0x00020000
#define R_PUMP_RESTING              0x00040000

 /************************************************************************************/
/* Define Read Status Bits (common loops)*/
#define R_LOADING                   0x00000001                          /* Bit 0 */
#define R_SELF_LOADING              0x00020000                          /* Bit 17 */
#define R_LOADING_PROX_SWITCH       0x00040000                          /* Bit 18 */
#define W_PUMP                      0x00080000                          /* Bit 19 */
#define W_CONT_RUN                  0x00100000                          /* Bit 20 */
#define W_DUST_COLL_A               0x00200000                          /* Bit 21 */
#define W_DUST_COLL_B               0x00400000                          /* Bit 22 */
#define W_PRIMARY_SOLINOID          0x00800000                          /* Bit 23 */
#define W_SECONDARY_SOLINOID        0x01000000                          /* Bit 24 */
#define W_SWING_GATE                0x02000000                          /* Bit 25 */

/* Define Read Status Bits (liw loops)*/
#define R_RAMPING                   0x00000002                          /* Bit 1 */
#define R_COASTING                  0x00000004                          /* Bit 3 */
#define R_DEMO_BOTH_MODS            0x00000018
#define R_DEMO_DRIVE_MOD            0x00000008
#define R_DEMO_WEIGH_MOD            0x00000010
#define R_NO_RATE_SPEED             0x00000020                          /* Bit 5 */
#define R_NO_CALIBRATION            0x00000040                          /* Bit 6 */
#define R_DEMO_PULSES               0x00000010
#define R_LIW_OPER_MODE_AUTO        0x00000100
#define R_LIW_OPER_MODE_MAN         0x00000200
#define R_LIW_OPER_MODE_PAUSE       0x00000400
#define R_LIW_OPER_MODE_MONITOR     0x00000800

/* for FLF loop */
#define R_FLF_MAX_SCRAP             0x00001000
#define R_FLF_CUT_BACK              0x00002000
#define R_FLF_MAX_VIRGIN            0x00004000

/* Define Read Status Bits (Gate Loop)*/
#define R_GATE_RECEIVING_ON         0x00000001
#define R_GATE_DUMPING_ON           0x00000002
#define R_GATE_DONE                 0x00000004
#define R_GATE_IN_DEMO              0x00000008
#define R_GATE_LAST_RECAL_CAUSE_P   0x00000010
#define R_GATE_LAST_RECAL_CUASE_F   0x00000020
#define R_GATE_LAST_RECAL_CAUSE_NO  0x00000040
#define R_HOPPER_GATE_STATUS        0x00000080                          /* Bit 7 */
#define R_GATE_FEED_STATE_start                 0x00000100
#define R_GATE_FEED_STATE_prepulse_gate         0x00000200
#define R_GATE_FEED_STATE_checking_prepulse     0x00000400
#define R_GATE_FEED_STATE_pulse_gate            0x00000800
#define R_GATE_FEED_STATE_waiting_for_gate      0x00001000
#define R_GATE_FEED_STATE_waiting_for_settling  0x00002000
#define R_GATE_FEED_STATE_check_wt              0x00004000
#define R_GATE_FEED_STATE_done                  0x00008000
#define R_AUX_CONTACT               0x00010000                          /* Bit 16 */
#define R_GATE_AUTO_MODE            0x00020000
#define R_GATE_PAUSE_MODE           0x00040000
#define R_GATE_MAN_MODE             0x00080000
#define R_GATE_CALIBRATE_MODE       0x00100000
#define R_GATE_DISPENSING_MODE      0x00200000
#define R_GATE_RUNNING_MODE_NEW     0x00400000
#define R_GATE_RUNNING_MODE_AUTO    0x00800000
#define R_GATE_RUNNING_MODE_MAN     0x01000000
#define R_GATE_RUNNING_MODE_PAUSE   0x02000000
#define R_GATE_RUNNING_MODE_SUSPEND 0x04000000
#define R_GATE_RUNNING_MODE_WAITING 0x08000000

/* used for new manual page controls BEB */
#define MANUAL_DEVICES_NO	17			//number of devices to follow	 G2-337 4/19/17	Dragan
#define MANUAL_GATE_A       0x00000001  // hmi_pg005_controls[0]
#define MANUAL_GATE_B       0x00000002  // hmi_pg005_controls[1]
#define MANUAL_GATE_C       0x00000004  // hmi_pg005_controls[2]
#define MANUAL_GATE_D       0x00000008  // hmi_pg005_controls[3]
#define MANUAL_GATE_E       0x00000010  // hmi_pg005_controls[4]
#define MANUAL_GATE_F       0x00000020  // hmi_pg005_controls[5]
#define MANUAL_GATE_G       0x00000040  // hmi_pg005_controls[6]
#define MANUAL_GATE_H       0x00000080  // hmi_pg005_controls[7]
#define MANUAL_GATE_I       0x00000100  // hmi_pg005_controls[8]
#define MANUAL_GATE_J       0x00000200  // hmi_pg005_controls[9]
#define MANUAL_GATE_K       0x00000400  // hmi_pg005_controls[10]
#define MANUAL_GATE_L       0x00000800  // hmi_pg005_controls[11]

#define MANUAL_MIXER_MOTOR  0x00001000  // hmi_pg005_controls[12]
#define MANUAL_MIXER_GATE   0x00002000  // hmi_pg005_controls[13]
#define MANUAL_BWH          0x00004000  // hmi_pg005_controls[14]
#define MANUAL_ALARM_OUT    0x00008000  // hmi_pg005_controls[15]
#define MANUAL_ACTUATE      0x00010000  // hmi_pg005_controls[16]


/* status bit definitions for Downcomer */
/* Only extruder rate for DC, which can't coast, so overload bit */
#define R_NEW_EXTRUDER_INSTANT_RATE 0x00000008                          /* Bit 3 */
#define R_HIGH_LEVEL_EXCEEDED       0x00000004                          /* Bit 2 */

/* status bits for LIW, DC and HO, which are used for "new set spd available", "drive inhiited" and "manual backup" */
#define R_DRIVE_INHIBIT  			0x00000100							/* Bit 8 */
#define R_EXD_IN_MANUAL				0x00000200							/* Bit 9 */
#define R_NEW_SET_SPD_READY 		0x00000400							/* Bit 10 */

/* Define Read Status Bits (Mixer Loop)*/
#define R_EXT_NEED_MATERIAL                  0x00000001
#define R_MIXER_NEED_MATERIAL                0x00000002
#define R_MIXER_PROX_SWITCH                  0x00000004
#define R_MIXER_INTERLOCK_CLOSED             0x00000008
#define R_MIXER_IN_DEMO                      0x00000010
#define R_MIXER_MODE_PAUSE                   0x00000020
#define R_MIXER_MODE_MAN                     0x00000040
#define R_MIXER_MODE_PURGE                   0x00000080
#define R_MIXER_MODE_WAITING_EMPTY           0x00000100
#define R_MIXER_MODE_TIME                    0x00000800
#define R_MIXER_MODE_WAITING_FULL            0x00002000
#define R_MIXER_MODE_DUMPING                 0x00004000
#define R_MIXER_OPER_MODE_PAUSE              0x00008000
#define R_MIXER_OPER_MODE_AUTO               0x00010000
#define R_MIXER_OPER_MODE_MAN                0x00020000
#define R_MIXER_OPER_MODE_PURGE              0x00040000
#define R_MIXER_AUX_PLUS                     0x00080000
#define R_MIXER_AUX_MINUS                    0x00100000
#define R_MIXER_AUX_NONE                     0x00200000

#define R_MIXER_GATE_OPEN                    0x00000200                 /* Bit 9 */
#define R_MIXER_MOTOR_ON                     0x00000400                 /* Bit 10 */
#define R_EXTERNAL_MATERIAL_REQUEST_STATUS   0x00001000                 /* Bit 12 */
/* Define Read Status Bits (BWH Loop)*/
#define R_BATCH_GATE_STATUS         0x00000100                          /* Bit 8 */
#define R_BWH_WEIGHT_STABLE         0x00000001
#define R_BWH_IN_DEMO               0x00000002
#define R_BWH_LAST_CAL_REASON_R     0x00000004
#define R_BWH_LAST_CAL_REASON_I     0x00000008
#define R_BWH_LAST_CAL_REASON_NONE  0x00000010
#define R_BWH_OPER_MODE_PURGE       0x00000020
#define R_BWH_OPER_MODE_AUTO        0x00000040
#define R_BWH_OPER_MODE_PAUSE       0x00000200
#define R_BWH_OPER_MODE_MAN         0x00000400
#define R_DEV_LOW_LEVEL             0x00000800                          /* Bit 11 */

/* define Write Status Bits */

/* Defines for the batch blender R_DEVICE_CONTROL registers */
#define R_DEV_MAIN_GATE_CLOSE       0x00000001                          /* Bit 0 */
#define R_DEV_MAIN_GATE_OPEN        0x00000002                          /* Bit 1 */
#define R_DEV_BATCH_GATE_CLOSE      0x00000004                          /* Bit 2 */
#define R_DEV_BATCH_GATE_OPEN       0x00000008                          /* Bit 3 */
#define R_DEV_MIXER_GATE_CLOSE      0x00000001                          /* Bit 4 */
#define R_DEV_MIXER_GATE_OPEN       0x00000002                          /* Bit 5 */
#define R_DEV_MIXER_MOTOR_OFF       0x00000004                          /* Bit 6 */
#define R_DEV_MIXER_MOTOR_ON        0x00000008                          /* Bit 7 */

#define DEMO_BOTH_MODS  0xFF        /* demo drive and weigh module      */
#define DEMO_DRIVE_MOD  0xF0        /* demo drive module                */
#define DEMO_WEIGH_MOD  0x0F        /* demo weigh module                */
#define DEMO_PULSES     0x0F        /* demo pulse module                */

/* old remote protocol (pcc addressing) common definitions        */
#define NOT_USED                0
#define SYSTEM_OFFSET           20000
#define EXTRUDER_OFFSET         1000
#define HOPPER_OFFSET           100
#define HOPPER_OFFSET_END       800
#define BATCH_EXT_OFFSET        900
#define EXT_TOT_OFFSET          900             /* totals for an extruder       */
#define REFEED_OFFSET           1200
#define HO_OFFSET               14400
#define SHO_OFFSET              10100
#define WTH_OFFSET              11000
#define MIX_OFFSET              13000
#define BATCH_HOP_OFFSET        14000
#define GRAVIFLUFF_OFFSET       15000
#define TYPES_PER               100
#define DEV_OFFSET              1200
#define DESCRIP_OFFSET          22000

#define ILLEGAL_LIW     127     /* illegal liw# for tests               */
#define ILLEGAL_LOOP    127     /* illegal loop# for tests, same as LIW */
#define ILLEGAL_FLOAT   -1.0e9

/************************************************************************/
/* Recipe error definitions                                             */
/************************************************************************/
#define RECIPE_UNDERSPEED_AL    0x0001    /* recipe may cause underspeed*/
#define RECIPE_OVERSPEED_AL     0x0002    /* recipe may cause ovespeed  */
#define RECIPE_SMALL_AUGER      0x0004    /* auger too small for recipe */
#define RECIPE_LARGE_AUGER      0x0008    /* auger too large for recipe */
#define RECIPE_NO_CAL           0x0010    /* this hopper not calibrated */
#define RECIPE_BAD_THROUGHPUT   0x0020    /* total LTP or WTP is bad    */
#define RECIPE_BAD_PROD         0x0040    /* bad production recipe msg  */
#define RECIPE_NOT_100          0x0080    /* recipe does not add to 100 */
#define RECIPE_NO_RATESPEED     0x0100    /* device has no rate vs speed*/

#define DISPENSE_TEST_WEIGHT     1
#define DISPENSE_TEST_TIME       2

/* these values are for copying and clearing parts of the recipe */
#define BLENDER_RECIPE      0x00000001
#define EXTRUDER_RECIPE     0x00000002
#define DENSITY_ONLY        0x00000004
#define ALL_RECIPE          0xffffffff

#define NOERROR 0               /* return codes for subroutines         */
#define ERROR -1

/* Flag used for remote printing */
#define PRINT_NOT_REQUESTED    0
#define PRINT_SUMMARY_REPORT   1
#define PRINT_RECIPE_REPORT    2
#define PRINT_ALARM_REPORT     3
#define PRINT_CONFIG_REPORT    4
#define PRINT_RESIN_REPORT     5

#define DP_COMMS_1_PRESENT 0x00000001

#define TIME_MIXER            'T'     /* mixing length by time          */

/************************************************************************/
/* Operating Mode definitions for shr_LIW[GRAVIFLUFF]->scrap_mode       */
/************************************************************************/
#define MAX_SCRAP             'S'     /* used for liw_flf only          */
#define MAX_VIRGIN            'V'     /* used for liw_flf only          */
#define CUT_BACK              'C'     /* used for liw_flf only          */

/************************************************************************/
/* Operating Mode definitions for oper_mode                             */
/************************************************************************/
#define BLN_OPER_MODE         shr_global.bln_oper_mode
#define EXT_HO_OPER_MODE      shr_global.ext_ho_oper_mode
#define OPER_MODE             (shr_global.bln_oper_mode|shr_global.ext_ho_oper_mode)

#define PAUSE_MODE            'P'     /* system paused, no drives on    */
#define MAN_MODE              'M'     /* manual mode on                 */
                                      /* idividual gate speeds          */
#define AUTO_MODE             'A'     /* auto mode                    */
#define WAITING_FOR_NEW_FEED  'W'     /* waiting for next feed command    */
#define FAST_AUTO_MODE        'F'     /* fast auto mode                    */
#define MONITOR_MODE          'B'     /* monitor wtp only               */
#define CALIBRATE_MODE        'C'     /* calibrate feeders, batch       */
#define PURGE_MODE            'X'     /* purge mode, batch       */
                                      /* blender only                   */
#define DISPENSING_MODE       'H'
#define NEW_MODE              'Z'
#define SUSPEND               's'     /* Suspend loop                   */
#define RESUME                'e'     /* Resume loop                    */
#define REINIT                'J'     /* reinitialize hardware settings */

#define DUMP_DEBUG_DATA       'd'     /* command to dump debug data     */

/************************************************************************/
/* Definitions for shr_LOOP[].GATE->feed_state                          */
/************************************************************************/
#define START                 'I'
#define PULSE_GATE_DELAY      'd' //drt delay 
#define PULSE_GATE            'P'
#define WAITING_FOR_GATE      'G'
#define WAITING_FOR_SETTLING  'S'
#define CHECK_WT              'W'
//#define DONE                'D'
#define PREPULSING			   'R' /* all the p's were taken */	
#define PREPULSE_GATE         'p'
#define WAITING_FOR_PREPULSE  'g'
#define CHECKING_PREPULSE     'c'

/************************************************************************/
/* Mixer Definitions                                                    */
/************************************************************************/
                                      /* counting the mixer revolutions */
#define TIME_MIXER            'T'     /* mixing length by time          */
#define MIXER_DUMPING         'D'
#define MIXER_WAITING_EMPTY   'E'     /* mixer empty & waiting for batch*/
#define MIXER_WAITING_FULL    'F'     /* mixer is full and waiting for  */
                                      /* material request               */
#define MIXER_UNKNOWN_STATE   '?'     /* mixer is in an unknow condition*/


typedef struct remote_point_data_str
{
    int loop;
    int dev_type;
    int ext;
    int hop;
    int total;
    unsigned int reg;
    config_LIW_device *c_LIW;               /* ptr to config data           */
    shared_LIW_device *s_LIW;               /* ptr to data shared w/ super  */
    char recipe_changed;
    char sixteen_bit_mode;                 /* 16/32 bit int xfers */

    int    (*read_remote_var)(struct remote_point_data_str *,unsigned int, fic *,int *);
    int    (*write_remote_var)(struct remote_point_data_str *,unsigned int, fic);
    int    (*translate_register)(struct remote_point_data_str *,unsigned int *);
    int    (*read_remote_var_2)(struct remote_point_data_str *,unsigned int, fic *,int *);
    int    (*write_remote_var_2)(struct remote_point_data_str *,unsigned int, fic);
    int    (*translate_register_2)(struct remote_point_data_str *,unsigned int *);

    void    *read_point_fn;
    void    *write_point_fn;
    void    *read_point_fn_2;
    void    *write_point_fn_2;

    unsigned int read_var_modhead;
    unsigned int write_var_modhead;
    unsigned int rem_vars_modhead;

    /* these functions cannot be in the subroutine module because they */
    /* referenced globals that cannot be used in a subroutine module. */
    /* things such as sprintf,printf etc */
    void    (*set_liw_spd)(int, float, float);
    void    (*set_ho_spd)(float, float);
    void    (*debug_print)(unsigned int, fic);

    config_super *cs;
    global_struct *g;
    stored_recipe *rec;

	config_loop_ptr cnf_LOOP[MAX_LOOP];
	shared_loop_ptr shr_LOOP[MAX_LOOP];


/* globals for ramp sync */
    float LIW_ad[MAX_LOOP];               /* accel / decel factors        */
    float HO_ad;
    float LIW_eta[MAX_LOOP];              /* eta's using max accel/decel  */
    float HO_eta;
    float LIW_ns[MAX_LOOP];               /* new speeds from rs_avg       */
    float HO_ns;
    float max_eta;                        /* eta for system ramp          */

    unsigned char mode_button_stick[MAX_LOOP+1];
    unsigned int mode_button_value[MAX_LOOP+1];
    int next_recipe_num;
    unsigned int current_resin_idx;
    unsigned int current_recipe_idx;

    struct port_diag_data_str *r_diag;
} point_data;

/* these are substitutions for the functions that were in the old */
/* rem_vars. The new functions take more arguments */
#define translate_register(a)       pd.translate_register(&pd,a)

#define translate_register_2(a)     pd.translate_register_2(&pd,a)

#define SET_DIAGNOSTICS 1

int init_remote_subroutines(point_data *pd);


#endif



