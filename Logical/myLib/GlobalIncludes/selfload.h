/************************************************************************/
/*                                                                      */
/*   File:  gate_pcc.h                                                  */
/*                                                                      */
/*   Description: This file defines the Vars and functions.             */
/*                                                                      */
/************************************************************************/

/* Self load stat vars */
#define LOAD_MAIN_STATE      13
#define LOAD_GATE_A          0
#define LOAD_GATE_B          1
#define LOAD_GATE_C          2
#define LOAD_GATE_D          3
#define LOAD_GATE_E          4
#define LOAD_GATE_F          5
#define LOAD_GATE_G          6
#define LOAD_GATE_H          7
#define LOAD_GATE_I          8
#define LOAD_GATE_J          9
#define LOAD_GATE_K          10
#define LOAD_GATE_L          11
#define LOAD_TIME            3 /* # of seconds to run the loading system. */


