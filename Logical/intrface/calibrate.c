#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  calibrate.c

   Description: This file controls the visualization as it pertains to
      calibration of the system devices.


      $Log:   F:\Software\BR_Guardian\calibrate.c_v  $
 *
 *    Rev 1.4   May 13 2008 09:43:56   FMK
 *
 *
 *    Rev 1.3   Apr 30 2008 15:02:24   Barry
 * Changed resolution calculation to wtperbit * MAX_BIT_RANGE/2
 *
 *    Rev 1.2   Feb 06 2008 10:42:42   FMK
 * Got the calibration working for the extruder
 * weigh hopper.
 *
 *    Rev 1.1   Jan 18 2008 09:50:04   FMK
 * Added new naming structure for global variables.
 * Broke out the resolution calculation to a new function
 * so that it could be called from either the calibrate
 * process or from editing the calibration.
 *
 *    Rev 1.0   Jan 11 2008 10:33:32   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "signal_pcc.h"

#define State_WaitForStart          0
#define State_CalibZeroBits         1
#define State_WaitForTestWt         2
#define State_WaitForStartTestBits  3
#define State_CalibTestBits         4
#define State_CalibDone             5

#define DisplayTextState_ZeroWtbits    0
#define DisplayTextState_TestWt        1
#define DisplayTextState_TestWtbits    2

#define ZEROBITFACTOR 0
#define TestBitFactor 1

int CalculateResolution();
void Calibrate_Main();
void Calibrate_Weigh();
void Edit_Calibration();
int Get_Calib_Factor(int liw,int bittype);

extern void visible(unsigned char *Runtime,unsigned char state);

extern DATE_AND_TIME get_date_time(void);

unsigned char gCalib_Init = NOTDONE;   /* must run initialization when started */
unsigned char gCalibrateState;

/************************************************************************/
/*
   Function:  Calibrate_Init()

   Description:  Initialization function for the calibration process.
        Sets up both the visualization and the calibrate process.
                                                                        */
/************************************************************************/
void Calibrate_Init()
{

   shr_global.Calib_ADError = 0;
   gCalib_Init = DONE;
   visible(&HMI.PopupRuntime1,FALSE); /* reset layers and objects to init state */
   visible(&HMI.PopupRuntime2,FALSE);
   visible(&HMI.PopupRuntime3,FALSE);
   visible(&HMI.PopupRuntime4,FALSE);
   visible(&HMI.PopupRuntime5,FALSE);
   visible(&HMI.SetupRuntime1,FALSE);
   visible(&HMI.SetupRuntime2,FALSE);
   visible(&HMI.SetupRuntime3,FALSE);
   visible(&HMI.SetupRuntime4,FALSE);
   visible(&HMI.SetupRuntime5,FALSE);
   HMI.Action_Input0 = gcInActive;
   HMI.Action_Input1 = gcInActive;
   HMI.Action_Input2 = gcInActive;
   HMI.Action_Input3 = gcInActive;
   HMI.Action_Input4 = gcInActive;
   HMI.Action_Input5 = gcInActive;
   HMI.Action_Input6 = gcInActive;
   HMI.Action_Input7 = gcInActive;
   visible(&HMI.SetupRuntime1,FALSE);
   visible(&HMI.SetupRuntime2,FALSE);
   visible(&HMI.SetupRuntime3,FALSE);
   visible(&HMI.SetupRuntime4,FALSE);
   visible(&HMI.SetupRuntime5,FALSE);
#if 0 /* invalid number that cause a page fault in the OS GTF */
   HMI.CurrentDevice = 65532;        /* reset device selection */
#endif
   gCalibrateState = State_WaitForStart; /* calibrate process to start */
   Signal.Calibrate = SIG_NONE;         /* signal for cal_bwh task to wait state */
}

/************************************************************************/
/*
   Function:  Calibrate_Main()

   Description:  main cyclic task for the calibration process
                                                                        */
/************************************************************************/
void Calibrate_Main()
{
   int Result;

   /* initialize all popups, inputs and initital settings */
   if (gCalib_Init == NOTDONE)
   {
      Calibrate_Init();
   }

   switch (HMI.CurrentPage)
   {
      case pg350_Calibrate: /* device selection, select device to calibrate  */
         if (HMI.Action_Input3 == gcActive)     /* calibrating the touch screen */
         {
            HMI.Action_Input3 = gcInActive;
            gTouchCalib = 0;
            Signal.EnteredSetup = TRUE;
         }

         if (HMI.Action_Input4 == gcActive)     /* calibrating a weigh device */
         {
            HMI.Action_Input4 = gcInActive;
            visible(&HMI.PopupRuntime3,TRUE);
            Signal.EnteredSetup = TRUE;
         }

         if (HMI.Action_Input5 == gcActive)     /* calibrate nip type device */
         {
            Signal.EnteredSetup = TRUE;
            HMI.Action_Input5 = gcInActive;
	         HMI.UnitLengthChange = 5;
            switch (HMI.CurrentDevice)          /* Button press sets this */
            {
               case 5:     /* haul off device */
                  HMI.pg351_NipCircum = cfgHO.nip_circumference;
                  HMI.pg351_NipPulses = cfgHO.pulses_per_rev;
                  break;
               case 3:     /* winder device */
                  HMI.pg351_NipCircum = cfgSHO.nip_circumference;
                  HMI.pg351_NipPulses = cfgSHO.pulses_per_rev;
                  break;
            }
            HMI.ChangePage = pg351_Calibrate_Edit;
            Edit_Calibration();
         }

/* action buttons for calibrate/edit calibration popup for weigh devices */
         if (HMI.Action_Input6 == gcActive)    /* calibrate weigh device */
         {
            HMI.Action_Input6 = gcInActive;
            visible(&HMI.PopupRuntime3,FALSE); /* hide the edit/calibrate popup */
            HMI.ChangePage = pg352_Calibrate_Weigh;
         }

         if (HMI.Action_Input7 == gcActive)    /* edit calibration weigh device */
         {
            HMI.Action_Input7 = gcInActive;
            visible(&HMI.PopupRuntime3,FALSE); /* hide the edit/calibrate popup */
            HMI.ChangePage = pg351_Calibrate_Edit;

            switch (HMI.CurrentDevice)
            {
               case 1:     /* batch weigh hopper */
                  HMI.pg351_ZeroFactor = cfgBWH.zerowt;
                  HMI.pg351_TestFactor = cfgBWH.testwt;
                  HMI.pg351_TestWeight = cfgBWH.test_wt;
                  break;
               case 0:     /* extruder hopper */
                  HMI.pg351_ZeroFactor = cfgEXT.zerowt;
                  HMI.pg351_TestFactor = cfgEXT.testwt;
                  HMI.pg351_TestWeight = cfgEXT.test_wt;
                  break;
               case 7:     /* gravifluff feeder */
                  HMI.pg351_ZeroFactor = cfgGFEEDER.zerowt;
                  HMI.pg351_TestFactor = cfgGFEEDER.testwt;
                  HMI.pg351_TestWeight = cfgGFEEDER.test_wt;
                  break;
               case 6:     /* gravifluff loader */
                  HMI.pg351_ZeroFactor = cfgGFLUFF.zerowt;
                  HMI.pg351_TestFactor = cfgGFLUFF.testwt;
                  HMI.pg351_TestWeight = cfgGFLUFF.test_wt;
                  break;
            }
            Edit_Calibration();
         }
         break;
      case pg351_Calibrate_Edit:
         if (HMI.Action_Input0 == gcActive)   /* the submit button was pressed */
         {
            HMI.Action_Input0 = gcInActive;
			HMI.pg350_BWH_calibration_flag = 1;
				
            switch (HMI.CurrentDevice)        /* the device is set via the button */
            {                                 /* press on panel pg350             */
               case 1:     /* batch weigh hopper */
                  cfgBWH.zerowt = HMI.pg351_ZeroFactor;
                  cfgBWH.testwt = HMI.pg351_TestFactor;
                  cfgBWH.test_wt = HMI.pg351_TestWeight;
                  Result = CalculateResolution();
                  break;
               case 5:     /* haul off device */
                  cfgHO.nip_circumference = HMI.pg351_NipCircum;
                  cfgHO.pulses_per_rev = HMI.pg351_NipPulses;
	               if ((cfgHO.nip_circumference != 0.0) && (cfgHO.pulses_per_rev != 0.0))
                  {
                     cfgHO.lenperbit = (cfgHO.nip_circumference) / cfgHO.pulses_per_rev;
                     cfgHO.cal_done = TRUE;
	                  cfgHO.rs_num = 0;
                  }
                  else
                  cfgHO.cal_done = FALSE;	      
                  break;
               case 0:     /* extruder hopper */
                  cfgEXT.zerowt =  HMI.pg351_ZeroFactor;
                  cfgEXT.testwt = HMI.pg351_TestFactor;
                  cfgEXT.test_wt = HMI.pg351_TestWeight;
                  Result = CalculateResolution();
                  break;
               case 3:     /* winder device */
                  cfgSHO.nip_circumference = HMI.pg351_NipCircum;
                  cfgSHO.pulses_per_rev = HMI.pg351_NipPulses;
                  break;
               case 7:     /* gravifluff feeder */
                  cfgGFEEDER.zerowt = HMI.pg351_ZeroFactor;
                  cfgGFEEDER.testwt = HMI.pg351_TestFactor;
                  cfgGFEEDER.test_wt = HMI.pg351_TestWeight;
                  break;
               case 6:     /* gravifluff loader */
                  cfgGFLUFF.zerowt = HMI.pg351_ZeroFactor;
                  cfgGFLUFF.testwt = HMI.pg351_TestFactor;
                  cfgGFLUFF.test_wt = HMI.pg351_TestWeight;
                  break;
            }
            Calibrate_Init();                 /* make sure all was reset       */
            HMI.ChangePage = pg350_Calibrate; /* return to main calibrate page */
         }

         if (HMI.Action_Input1 == gcActive)   /* the back button was pressed   */
         {
            HMI.Action_Input1 = gcInActive;
            Calibrate_Init();                 /* make sure all was reset       */
            HMI.ChangePage = pg350_Calibrate;
         }
         break;
      case pg352_Calibrate_Weigh:    /* calib process for weigh device         */
            cfgBWH.offsetbits = 0;    /* force to 0 */
            if (shr_global.Calib_ADError > 0)
            {
               if ((shr_global.Calib_ADError & ERR_NOTPAUSED) == ERR_NOTPAUSED)
               {
                     HMI.pg352_CalibErrorTextIndex = 1;
                     visible(&HMI.PopupRuntime4,TRUE);
               }
               else if ((shr_global.Calib_ADError & ERR_HARDWARE) == ERR_HARDWARE)
               {
                     HMI.pg352_CalibErrorTextIndex = 2;
                     visible(&HMI.PopupRuntime4,TRUE);
               }
               else if ((shr_global.Calib_ADError & ERR_UNSTABLEWEIGHT) == ERR_UNSTABLEWEIGHT)
               {
                     HMI.pg352_CalibErrorTextIndex = 3;
                     visible(&HMI.PopupRuntime4,TRUE);
               }
               else if ((shr_global.Calib_ADError & ERR_WEIGHTTOOSMALL) == ERR_WEIGHTTOOSMALL)
               {
                     HMI.pg352_CalibErrorTextIndex = 4;
                     visible(&HMI.PopupRuntime4,TRUE);
               }
               else if ((shr_global.Calib_ADError & ERR_AD_LOCKED) == ERR_AD_LOCKED)
               {
                     HMI.pg352_CalibErrorTextIndex = 5;
                     visible(&HMI.PopupRuntime4,TRUE);
               }
               else
               {
                  HMI.pg352_CalibErrorTextIndex = 0;
                  visible(&HMI.PopupRuntime4,TRUE);
               }
            }

            if (HMI.PopupRuntime4 == 0)  /* err popup is visible do not calibrate */
            {
               if (HMI.Action_Input4 == gcActive)
               {
                  HMI.Action_Input4 = gcInActive;  /* restart calibrate process */
                  Calibrate_Init();
                  visible(&HMI.PopupRuntime4,FALSE); /* hide err panel */
                  HMI.ChangePage = pg350_Calibrate;
               }
            }
			   else
			   {
				   Calibrate_Weigh();
				   HMI.pg350_BWH_calibration_flag = 1;
			   }
         break;
      default:
         break;
   }
}
/************************************************************************/
/*
   Function:  Edit_Calibration()

   Description:  controls the HMI for the editing of calibration values.
                                                                        */
/************************************************************************/
void Edit_Calibration()
{
   switch (HMI.CurrentDevice)   /* show layer needed for device */
   {
      case 1:     /* batch weigh hopper */
         visible(&HMI.PopupRuntime1,FALSE);  /* hide nip layer */
         visible(&HMI.PopupRuntime2,TRUE);  /* show weigh hopper layer */
         break;
      case 5:     /* haul off device */
         visible(&HMI.PopupRuntime1,TRUE);  /* show nip layer */
         visible(&HMI.PopupRuntime2,FALSE);  /* hide weigh hopper layer */
         break;
      case 0:     /* extruder hopper */
         visible(&HMI.PopupRuntime1,FALSE);  /* hide nip layer */
         visible(&HMI.PopupRuntime2,TRUE);  /* show weigh hopper layer */
         break;
      case 3:     /* winder device */
         visible(&HMI.PopupRuntime1,TRUE);  /* show nip layer */
         visible(&HMI.PopupRuntime2,FALSE);  /* hide weigh hopper layer */
         break;
      case 7:     /* gravifluff feeder */
         visible(&HMI.PopupRuntime1,FALSE);  /* hide nip layer */
         visible(&HMI.PopupRuntime2,TRUE);  /* show weigh hopper layer */
         break;
      case 6:     /* gravifluff loader */
         visible(&HMI.PopupRuntime1,FALSE);  /* hide nip layer */
         visible(&HMI.PopupRuntime2,TRUE);  /* show weigh hopper layer */
         break;
   }
}

/************************************************************************/
/*
   Function:  Calibrate_Weigh()

   Description:  controls the HMI and the calibration process.
                                                                        */
/************************************************************************/
void Calibrate_Weigh()
{
   int result = FAIL;

   switch (gCalibrateState)
   {
      case State_WaitForStart:
         HMI.pg351_CalibDeviceTextIndex = DisplayTextState_ZeroWtbits; /* show instruction text */
         visible(&HMI.SetupRuntime1, TRUE);   /* show the button to get zero weight bits */
         visible(&HMI.PopupRuntime1, TRUE);   /* show the popup for getting the zero weight */
         if (HMI.Action_Input0 == gcActive)   /* get the zero weight bits */
         {
            HMI.Action_Input0 = gcInActive;
			   shrBWH.BWH_self_check_flag = TRUE;
            visible(&HMI.SetupRuntime1, FALSE);   /* hide the button for getting zero weight */
                                                  /* this will show the zero weight results below */
            switch (HMI.CurrentDevice)            /* different devices have different routines */
            {
               case 1:  /* Batch Weigh Hopper */
                  if (BLN_OPER_MODE != PAUSE_MODE)
                  {
                     shr_global.Calib_ADError |= ERR_NOTPAUSED; /* error out, blender must be paused */
                     return;
                  }
                  else
                     shr_global.Calib_ADError &= ~ERR_NOTPAUSED;

                  cfgBWH.cal_done = FALSE;
                  if (cfgBWH.demo)
                  {
                     shrBWH.demo_cal_test_wt = 0.0;
                     shrBWH.demo_calibrating = TRUE;
                  }
                  break;
               case 0:
                  if (EXT_HO_OPER_MODE != PAUSE_MODE)
                  {
                     shr_global.Calib_ADError |= ERR_NOTPAUSED;
                     return;
                  }
                  else
                     shr_global.Calib_ADError &= ~ERR_NOTPAUSED;

                  cfgEXT.cal_done = FALSE;
                  if (cfgEXT.demo)
                  {
                     shrEXT.demo_cal_test_wt = 0.0;
                     shrEXT.demo_calibrating = TRUE;
                  }
                  break;
               default:
                  break;
            }
            gCalibrateState = State_CalibZeroBits;  /* move to the next state */
         }
         break;
      case State_CalibZeroBits:
         switch (HMI.CurrentDevice)
         {
            case 1:  /* Batch Weigh Hopper */
                  result=Get_Calib_Factor(WEIGH_HOP_LOOP,ZEROBITFACTOR);
               break;
            case 0:  /* Batch Weigh Hopper */
                  result=Get_Calib_Factor(EXT_LOOP,ZEROBITFACTOR);
               break;
            default:
               break;
         }

         if (result == SUCCESS)
         {
            visible(&HMI.PopupRuntime1,FALSE);  /* hide the popup for getting the zero weight bits */
            visible(&HMI.PopupRuntime2,TRUE);   /* show the popup for getting the test weight */
            HMI.pg351_CalibDeviceTextIndex = DisplayTextState_TestWt; /* change instruction text */
            HMI.pg351_TestWeight = 0;    /* force user to enter a valid value to pass onto next state */
            gCalibrateState = State_WaitForTestWt;  /* move to the next state */
         }
         else if (result == FAIL)          /* could not get factor, error out, prepare to start over */
         {
            Calibrate_Init();
            HMI.ChangePage = pg350_Calibrate;
         }
         break;
      case State_WaitForTestWt:
         /* wait for the text box to be edited */
         if (HMI.Action_Input0 == gcActive)    /* the test weight value has been entered */
         {
            visible(&HMI.PopupRuntime2, FALSE);   /* hide the popup for getting the test weight */
            HMI.Action_Input0 = gcInActive;
			   shrBWH.BWH_self_check_flag = FALSE;		
            gCalibrateState = State_WaitForStartTestBits;  /* move to the next state */
            switch (HMI.CurrentDevice)        /* store the test weight value for the correct device */
            {
               case 1:  /* Batch Weigh Hopper */
                  cfgBWH.test_wt = HMI.pg351_TestWeight; /* store the test weight value */
                  break;
               case 0:  /* Batch Weigh Hopper */
                  cfgEXT.test_wt = HMI.pg351_TestWeight; /* store the test weight value */
                  break;
               default:
                  break;
            }
         }
         break;
      case State_WaitForStartTestBits:
         HMI.pg351_CalibDeviceTextIndex = DisplayTextState_TestWtbits; /* change instruction text */
         visible(&HMI.PopupRuntime3, TRUE);   /* show the popup for get the test weight bits */
         visible(&HMI.SetupRuntime2, TRUE);   /* show the button for getting test weight bits */

         if (HMI.Action_Input0 == gcActive)    /* find the test weight bit value */
         {
            HMI.Action_Input0 = gcInActive;
            visible(&HMI.SetupRuntime2, FALSE);  /* hide the button for getting test weight bits*/
            gCalibrateState = State_CalibTestBits; /* move to the next state */
        }
         break;
      case State_CalibTestBits:
         switch (HMI.CurrentDevice)
         {
            case 1:  /* Batch Weigh Hopper */
               result=Get_Calib_Factor(WEIGH_HOP_LOOP, TestBitFactor);
               break;
            case 0:  /* extruder Hopper */
               result=Get_Calib_Factor(EXT_LOOP, TestBitFactor);
               break;
            default:
               break;
         }

         if (result == SUCCESS )
         {
            gCalibrateState = State_CalibDone;
         }
         else if (result == FAIL)
         {
            Calibrate_Init();
            HMI.ChangePage = pg350_Calibrate;
         }
         break;
      case State_CalibDone:    /* calibration is done go back to main calib page */
         Calibrate_Init();     /* init data for next calib process */
         HMI.ChangePage = pg350_Calibrate;
         break;
      default:
         break;
   }

   if (HMI.Action_Input3 == gcActive) /* back button has been pressed, calibrate cancelled */
   {
      Calibrate_Init();      /* init data for next calib process */
      HMI.ChangePage = pg350_Calibrate;
   }
}

/************************************************************************/
/*
   Function:  CalculateResolution()

   Description: This function will calculate the wtperbit and resolution
      of the weigh hopper.

   Returns: SUCCESS - If the weight factor is calculated.
            ERR_WEIGHTTOOSMALL - if the weight difference is to small
                                                                        */
/************************************************************************/
int CalculateResolution()
{
   float bitsperwt;
	
	DTStructure   dateTimeNow;
	DTGetTime_typ tempTimeNow;
  
	tempTimeNow.enable = (BOOL)1;
	DTGetTime(&tempTimeNow);

	DT_TO_DTStructure (tempTimeNow.DT1, (UDINT)&dateTimeNow);		
	
   switch (HMI.CurrentDevice)
   {
      case 1:
         bitsperwt = (float)(cfgBWH.testwt - cfgBWH.zerowt) / cfgBWH.test_wt;

         if (bitsperwt < 5000)
         {
            /* error bit wt too small for valid calibration */
            shr_global.Calib_ADError = ERR_WEIGHTTOOSMALL;
            return(ERR_WEIGHTTOOSMALL);
         }

         cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
#if 0
         //cfgBWH.resolution = ((float)MAX_AD_VALUE * cfgBWH.test_wt)/
         //             (10000.0 * ((float)(cfgBWH.testwt - cfgBWH.zerowt)));
#endif
         cfgBWH.resolution = (cfgBWH.wtperbit * MAX_BIT_RANGE)/2;
#if 0
         //cfgBWH.max_batch_size = (MAX_LC_AD_VALUE - cfgBWH.zerowt) * cfgBWH.wtperbit;
         //cfgBWH.min_batch_size = cfgBWH.max_batch_size * 0.3; /* make min be 30% of max */
#endif
         cfgBWH.cal_done = TRUE;
			
			// to add calibration time and day on diag page
			debugFlags.calib_minute = dateTimeNow.minute;
			debugFlags.calib_hour = dateTimeNow.hour;
			debugFlags.calib_day = dateTimeNow.day;
			debugFlags.calib_year = dateTimeNow.year;	
			debugFlags.calib_month = dateTimeNow.month;
			
			cfg_super.calib_minute = debugFlags.calib_minute;
			cfg_super.calib_hour = debugFlags.calib_hour;
			cfg_super.calib_day = debugFlags.calib_day;
			cfg_super.calib_year = debugFlags.calib_year;
			cfg_super.calib_month = debugFlags.calib_month;

         break;
   case 0:
      bitsperwt = (float)(cfgEXT.testwt - cfgEXT.zerowt) / cfgEXT.test_wt;

      if (bitsperwt < 50)
      {
         /* error bit wt too small for valid calibration */
         shr_global.Calib_ADError = ERR_WEIGHTTOOSMALL;
         return(ERR_WEIGHTTOOSMALL);
      }

      cfgEXT.wtperbit = cfgEXT.test_wt / (float)(cfgEXT.testwt - cfgEXT.zerowt);
#if 0
      cfgEXT.resolution = ((float)MAX_AD_VALUE * cfgEXT.test_wt)/
                   (10000.0 * ((float)(cfgEXT.testwt - cfgEXT.zerowt)));
#endif
      cfgEXT.resolution = (cfgEXT.wtperbit * MAX_BIT_RANGE)/2;
      cfgEXT.cal_done = TRUE;
      break;
   }
   return(SUCCESS);
}
/************************************************************************/
/*
   Function:  Get_Calib_Factor()

   Description: Task to get either the zero or test weight factor for the
      batch weigh hopper device.

   Returns: SUCCESS - If the weight factor is calculated.
                                                                        */
/************************************************************************/
int Get_Calib_Factor(int liw,int factor_type)
{
   int Result;

   shr_global.CalibrateDevice = liw;
   if (factor_type == ZEROBITFACTOR)
   {
      if (Signal.Calibrate == SIG_NONE)
         Signal.Calibrate = Sig_Calib_Zero_Bit;

      switch (Signal.Calibrate)
      {
         case Sig_Calib_Error:
            Signal.Calibrate = SIG_NONE;
            return(FAIL);
            break;
         case Sig_Calib_Done:
            Signal.Calibrate = SIG_NONE;
            return(SUCCESS);
            break;
         default:
            return(NOTDONE);
            break;
      }
   }

   if (factor_type == TestBitFactor)
   {
      if (Signal.Calibrate == SIG_NONE)
         Signal.Calibrate = Sig_Calib_Test_Bit;

      switch (Signal.Calibrate)
      {
         case Sig_Calib_Error:
            Signal.Calibrate = SIG_NONE;
            return(FAIL);
            break;
         case Sig_Calib_Done:
            Signal.Calibrate = SIG_NONE;
            Result = CalculateResolution();
            return(Result);
            break;
         default:
            return(NOTDONE);
            break;
      }
   }
   return(SUCCESS);
}




