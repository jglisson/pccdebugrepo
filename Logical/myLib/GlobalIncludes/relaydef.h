/************************************************************************/
/*
   File:  relaydef.h

   Description: This file defines the digital i/o positions available to
      the system.


      $Log:   F:\Software\BR_Guardian\relaydef.h_v  $
 *
 *    Rev 1.5   May 09 2008 14:01:46   FMK
 * Added input for remote off switch of buzzer.
 *
 *    Rev 1.4   May 01 2008 10:28:04   FMK
 * Changed definition for alarm out and added
 * alarm out audible. This moved the purge valve
 * relays down to the next output module.
 *
 *    Rev 1.3   Mar 27 2008 08:46:20   FMK
 *
 *
 *    Rev 1.2   Feb 27 2008 14:34:58   FMK
 * Changed the relay definitions for the extrusion
 * and ho modules.
 *
 *    Rev 1.1   Feb 04 2008 09:44:24   FMK
 * Just updated the comments to reflect which
 * module the relaydefs are associated with.
 *
 *    Rev 1.0   Jan 11 2008 10:33:38   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
/* relay definitions*/
/* Slot ST1 is for the Bus Receiver */
/* Slot ST2 is for the analog input module */

/* ST3 Digital input module 12 inputs, misc inputs for blender operation */
#define RESV_IN_1               1   /* PLCstruct_In[0], slot ST3 IN 0 */
#define RESV_IN_2               2   /* PLCstruct_In[0], slot ST3 IN 1 */
#define RESV_IN_3               3   /* PLCstruct_In[0], slot ST3 IN 2 */
#define MIXER_STRTR_AUX_CNTCT   4   /* PLCstruct_In[0], slot ST3 IN 3 */
/* was #define RESV_IN_4        4      PLCstruct_In[0], slot ST3 IN 3 */
#define RESV_IN_5               5   /* PLCstruct_In[0], slot ST3 IN 4 */ //not used in code
#define AUX_CONT                6   /* PLCstruct_In[0], slot ST3 IN 5 */
#define BUZZER_SILENCE          7   /* PLCstruct_In[0], slot ST3 IN 6 */
#define KEY_SWITCH_IN           8   /* PLCstruct_In[0], slot ST3 IN 7 */
#define SAFETY_SWITCH_IN        9   /* PLCstruct_In[0], slot ST3 IN 8 */
#define SAFETY_OR_KEYSWITCH_IN  10  /* PLCstruct_In[0], slot ST3 IN 9 */ //not used in code
#define MATERIAL_REQUEST_IN     11  /* PLCstruct_In[0], slot ST3 IN 10 */
#define MIXER_PROX_IN           12  /* PLCstruct_In[0], slot ST3 IN 11 */

/* ST4 Digital Output module 12 outputs, misc blender, self cleaning, extruder */
/* load signal, elements A to D gate control */
#define WEIGH_HOPPER_DUMP_OUT   13  /* PLCstruct_Out[0], slot ST4 OUT 0 */
#define MIXER_GATE_OUT          14  /* PLCstruct_Out[0], slot ST4 OUT 1 */
#define MIXER_MOTOR_OUT         15  /* PLCstruct_Out[0], slot ST4 OUT 2 */
#define ALARM_OUT               16  /* PLCstruct_Out[0], slot ST4 OUT 3 */
#define ALARM_OUT_AUDIBLE       17  /* PLCstruct_Out[0], slot ST4 OUT 4 */
#define EXT_LOAD_SIGNAL_OUT     18  /* PLCstruct_Out[0], slot ST4 OUT 5 */
//G2-224 2/1/17 drt
#define RUN_OUTPUT              19  /* PLCstruct_Out[0], slot ST4 OUT 6 */

#define NF                      20  /* PLCstruct_Out[0], slot ST4 OUT 7 */
#define GATE_A_OUT              21  /* PLCstruct_Out[0], slot ST4 OUT 8 */
#define GATE_B_OUT              22  /* PLCstruct_Out[0], slot ST4 OUT 9 */
#define GATE_C_OUT              23  /* PLCstruct_Out[0], slot ST4 OUT 10 */
#define GATE_D_OUT              24  /* PLCstruct_Out[0], slot ST4 OUT 11 */

/* Slot ST5 is reserved for the comm port */

/* ST6 Digital output module 12 outputs, elements E to L gate control, 4 spares */
#define GATE_E_OUT              25  /* PLCstruct_Out[1], slot ST6 OUT 0 */
#define GATE_F_OUT              26  /* PLCstruct_Out[1], slot ST6 OUT 1 */
#define GATE_G_OUT              27  /* PLCstruct_Out[1], slot ST6 OUT 2 */
#define GATE_H_OUT              28  /* PLCstruct_Out[1], slot ST6 OUT 3 */
#define GATE_I_OUT              29  /* PLCstruct_Out[1], slot ST6 OUT 4 */
#define GATE_J_OUT              30  /* PLCstruct_Out[1], slot ST6 OUT 5 */
#define GATE_K_OUT              31  /* PLCstruct_Out[1], slot ST6 OUT 6 */
#define GATE_L_OUT              32  /* PLCstruct_Out[1], slot ST6 OUT 7 */
#define PurgeValve_R            33  /* PLCstruct_Out[1], slot ST6 OUT 8 */
#define PurgeValve_L            34  /* PLCstruct_Out[1], slot ST6 OUT 9 */
#define PurgeValve_T            35  /* PLCstruct_Out[1], slot ST6 OUT 10 */
#define RESV_OUT_3              36  /* PLCstruct_Out[1], slot ST6 OUT 11 */

/* Self loading module */
/* ST7 Digital Input module 12 inputs, Used for signal to load elements A to L */
#define LOAD_A_IN               37  /* PLCstruct_In[1], slot ST7 IN 0 */
#define LOAD_B_IN               38  /* PLCstruct_In[1], slot ST7 IN 1 */
#define LOAD_C_IN               39  /* PLCstruct_In[1], slot ST7 IN 2 */
#define LOAD_D_IN               40  /* PLCstruct_In[1], slot ST7 IN 3 */
#define LOAD_E_IN               41  /* PLCstruct_In[1], slot ST7 IN 4 */
#define LOAD_F_IN               42  /* PLCstruct_In[1], slot ST7 IN 5 */
#define LOAD_G_IN               43  /* PLCstruct_In[1], slot ST7 IN 6 */
#define LOAD_H_IN               44  /* PLCstruct_In[1], slot ST7 IN 7 */
#define LOAD_I_IN               45  /* PLCstruct_In[1], slot ST7 IN 8 */
#define LOAD_J_IN               46  /* PLCstruct_In[1], slot ST7 IN 9 */
#define LOAD_K_IN               47  /* PLCstruct_In[1], slot ST7 IN 10 */
#define LOAD_L_IN               48  /* PLCstruct_In[1], slot ST7 IN 11 */

/* ST8 Digital Output module 12 outputs, Used for signal to load elements A to L */
/* also used for pump control, provides one spare output */
#define DUST_A_OUT              49  /*  PLCstruct_Out[2], slot ST8 OUT 0 */
#define DUST_B_OUT              50  /*  PLCstruct_Out[2], slot ST8 OUT 1 */
#define CONT_RUN_OUT            51  /*  PLCstruct_Out[2], slot ST8 OUT 2 */
#define PUMP_OUT                52  /*  PLCstruct_Out[2], slot ST8 OUT 3 */

//G2-224 2/1/17 drt (Moved ADDR 4 (14 & 24)
#define VFD_HIGH                53  /*  PLCstruct_Out[2], slot ST8 OUT 4 */
#define VFD_LOW                 54  /*  PLCstruct_Out[2], slot ST8 OUT 5 */

#define LOAD_A_OUT              55  /*  PLCstruct_Out[2], slot ST8 OUT 6 */
#define LOAD_B_OUT              56  /*  PLCstruct_Out[2], slot ST8 OUT 7 */
#define LOAD_C_OUT              57  /*  PLCstruct_Out[2], slot ST8 OUT 8 */
#define LOAD_D_OUT              58  /*  PLCstruct_Out[2], slot ST8 OUT 9 */
#define LOAD_E_OUT              59  /*  PLCstruct_Out[2], slot ST8 OUT 10 */
#define LOAD_F_OUT              60  /*  PLCstruct_Out[2], slot ST8 OUT 11 */

/* ST9 Digital output module 12 outputs, Used for signal to load elements G-L */
#define LOAD_G_OUT              61  /* PLCstruct_Out[3], slot ST9 OUT 0 */
#define LOAD_H_OUT              62  /* PLCstruct_Out[3], slot ST9 OUT 1 */
#define LOAD_I_OUT              63  /* PLCstruct_Out[3], slot ST9 OUT 2 */
#define LOAD_J_OUT              64  /* PLCstruct_Out[3], slot ST9 OUT 3 */
#define LOAD_K_OUT              65  /* PLCstruct_Out[3], slot ST9 OUT 4 */
#define LOAD_L_OUT              66  /* PLCstruct_Out[3], slot ST9 OUT 5 */
#define RESV_OUT_5              67  /* PLCstruct_Out[3], slot ST8 OUT 6 */
#define RESV_OUT_6              68  /* PLCstruct_Out[3], slot ST8 OUT 7 */
#define EXTSpeed_OUT_Up         69  /* PLCstruct_Out[3], slot ST8 OUT 8 */
#define EXTSpeed_OUT_Dn         70  /* PLCstruct_Out[3], slot ST8 OUT 9 */
#define HOSpeed_OUT_Up          71  /* PLCstruct_Out[3], slot ST8 OUT 10 */
#define HOSpeed_OUT_Dn          72  /* PLCstruct_Out[3], slot ST8 OUT 11 */

/* st10 weigh input for extrusion control (73-84) */
/* ST11 drive output voltage/current reference (85-96) */

#define ANALOG_OUT_EXTRUDER PLC.AnaOut.AnalogOutput1
#define ANALOG_OUT_HAULOFF  PLC.AnaOut.AnalogOutput2

/* ST12 auto/manual, drive inhibit 12x digital inputs (97-108) */
#define AutoMan_EXT             97   /* PLCstruct_In[2], slot ST14 in 0 */
#define AutoMan_HO              98   /* PLCstruct_In[2], slot ST14 in 1 */
#define DriveInh_Ext            99   /* PLCstruct_In[2], slot ST14 in 2 */
#define DriveInh_HO             100  /* PLCstruct_In[2], slot ST14 in 3 */
#define ScrewSync_EXT           101  /* PLCstruct_In[2], slot ST14 in 4 */
#define IntExtDrv_CTRL_EXT      102  /* PLCstruct_In[2], slot ST14 in 5 */
#define IntExtDrv_CTRL_HO       103  /* PLCstruct_In[2], slot ST14 in 6 */
#define RESV_IN_10              104  /* PLCstruct_In[2], slot ST14 in 7 */
#define EXTSpeed_IN_Up          105  /* PLCstruct_In[2], slot ST14 in 8 */
#define EXTSpeed_IN_Dn          106  /* PLCstruct_In[2], slot ST14 in 9 */
#define HOSpeed_IN_Up           107  /* PLCstruct_In[2], slot ST14 in 10 */
#define HOSpeed_IN_Dn           108  /* PLCstruct_In[2], slot ST14 in 11 */
/* ST13 Auto/Manual Relay outputs for HO/EXT pot. control (109-112)*/
#define EXTManual_Out_0         109
#define EXTManual_Out_1         110
#define HOManual_Out_0          111
#define HOManual_Out_1          112
/* ST14 Pulse counters plus 2 digital inputs (113-114) */
#define RESV_IN_15              113  /*  PLCstruct_PulseCounter.in[0], slot st13 in 0 */
#define RESV_IN_16              114  /*  PLCstruct_PulseCounter.in[1], slot st13 in 1 */
/* ST15 analog input for bumpless transfer, 2AI/2DI (97-108) */

/* ST16 digital input for Autobatch,  (115-126) */

#define Hopper_A_PROX_IN    115       /* PLCstruct_In[3], slot ST16 in 0 */
#define Hopper_B_PROX_IN    116       /* PLCstruct_In[3], slot ST16 in 1 */
#define Hopper_C_PROX_IN    117       /* PLCstruct_In[3], slot ST16 in 2 */
#define Hopper_D_PROX_IN    118       /* PLCstruct_In[3], slot ST16 in 3 */
#define Hopper_E_PROX_IN    119       /* PLCstruct_In[3], slot ST16 in 4 */
#define Hopper_F_PROX_IN    120       /* PLCstruct_In[3], slot ST16 in 5 */
#define Hopper_G_PROX_IN    121       /* PLCstruct_In[3], slot ST16 in 6 */
#define Hopper_H_PROX_IN    122       /* PLCstruct_In[3], slot ST16 in 7 */
#define Hopper_I_PROX_IN    123       /* PLCstruct_In[3], slot ST16 in 8 */
#define Hopper_J_PROX_IN    124       /* PLCstruct_In[3], slot ST16 in 9 */
#define Hopper_K_PROX_IN    125       /* PLCstruct_In[3], slot ST16 in 10 */
#define Hopper_L_PROX_IN    126       /* PLCstruct_In[3], slot ST16 in 11 */

//#define InputChannel         127      // hopper A magnetic switch for 1Kg blender only G3 G2-541
                                        // Use hopper prox inputs for hopper switch G3 
#define InputChannel           115      // hopper A magnetic switch for 1Kg blender only G3 G2-541
#define ALARM_OUT_GRAV         136 
#define Gravitrol_Hopper_Load  138

/* Haul Off Gear Number relay definitions */
#define HO_GEAR_0                0     /* gear 0 engaged */
#define HO_GEAR_1                1     /* gear 1 engaged */
#define HO_GEAR_2                2     /* gear 2 engaged */
#define HO_GEAR_3                3     /* gear 3 engaged */
#define HO_GEAR_4                4     /* gear 4 engaged */

/* PIO Relay Usage Definitions */
#define  NO_RELAY_USAGE    0
#define  STANDARD_RELAYS   1
#define  CUSTOM_RELAYS     2

/* These defines CANNOT be changed unless you change the order/position of  */
/* the text for the corresponding menu. It will not work if they do not     */
/* match RAC 3/6/98                                                         */
#define  AUX_ALARM_FUNC                1
#define  CLEAR_ALARMS_FUNC             2
#define  MANUAL_BACKUP_FUNC            3
#define  SELF_CLEANING_FUNC            4
#define  REMOTE_ON_OFF_FUNC            5
#define  POWDER_CLEAN_FUNC             6

/* MAY NO LONGER BE NECESSARY WITH b&r */
//#define AD_SYNC_OUT             21
//#define AD_RESET_OUT            22
//#define MONITOR_24V_IN          23
//#define AD_SYNC_OUT_1           67  /* PLCstruct_Out[1], slot ST7 OUT 13 */
//#define AD_RESET_OUT_1          68  /* PLCstruct_Out[1], slot ST7 OUT 14 */
//#define AD_RESET_OUT_2          69  /* PLCstruct_Out[1], slot ST7 OUT 15 */



