/************************************************************************/
/*  
   File:  diagnostics.h                                       
                                                                      
   Description: Defines the memory structures used in diagnostics.c.
      

 * 
 *    Rev 1.0   Dec 02 2009 10:33:36   FMK
 * Initial version

      Process Control Corp,   Atlanta, GA                    
                                                                        */
/************************************************************************/
#include <bur/plc.h>
#include <bur/plctypes.h>
#include <asstring.h>
#include <fileio.h>

#define STATE_INIT              0
#define STATE_WAITING           1
#define STATE_DISPENSE_WEIGHT   2 
#define STATE_DISPENSE_TIME     3
#define STATE_DISPENSE_WAIT     4
#define STATE_DELAY1            5
#define STATE_EMPTYHOPPER       6
#define STATE_DELAY2            7
#define STATE_PREPULSE          8
#define STATE_PAUSE             9
#define STATE_RAMPING          10
#define STATE_OPEN_RAMP_FILE   11
#define STATE_ADD_RAMP_LINE    12
#define STATE_CLOSE_RAMP_FILE  13
#define STATE_GET_FILE_NAME    14
#define STATE_RAMP_FILE_EXISTS 15

typedef struct         /* line data in the diagnostic ramp test */
{
   int step_time;       /* time of each step */
   int step_cnt;        /* # of non-zero entries */
   float wt[10];        /* weight of each gate fire */
} ramping_line;

void BuildLineOfDiag();
int open_ramp_file(int *);

_GLOBAL int   pg430_step_count;
_LOCAL  int   pg430_name_input_done;
_LOCAL  int   warning_answered;

/* if we assume that in converts to 5 ascii charecters (12345)
 * and float converts to 10 ascii charecters (123.12345 plus lead ",")
 * each line in final file will be 105 bytes
*    max entries per time = 10
*    min start time       = 25 mS
*    min step size        =  5 mS
*    max end time         = 10,000 mS
*    (10,000-25)/5        = 2000 lines of 
*    time, wt   , wt   , wt   , wt   , wt   , wt   , wt   , wt   , wt   , wt   
*     int, float, float, float, float, float, float, float, float, float, float
*    
*    assume float is 123.12345 = float length is 9+1 add lead ","
*    assume int is 12345 = int length is 5
*    line length is 105 bytes
*    file length is 2000*105 = 210,000 bytes
*/

#define FILE_STORE_NEXT      27  /* defined state for store writing the block */
#define FILE_STORE_WAIT      28  /* defined state for store writing the block */
#define FILE_STORE_DELETE    29  /* defined state for store writing the block */

_LOCAL ramping_line ramp_data;
_GLOBAL STRING rline[200];
_LOCAL unsigned char dStorePath[15];
_LOCAL UDINT dFileIDent;     /* the system id for the file currently opened */
_LOCAL USINT dErrorLevel;    /* indicates where the error occurred */
_LOCAL UINT dsysError;       
_LOCAL STRING istring[5];
_LOCAL STRING fstring[9];
_LOCAL int ramp_file_exists;
_GLOBAL int state_Store_Diag; /* static var for state function is in */

_LOCAL FileCreate_typ DIAGFile_TypCreate;      /* structure for creating file */
_LOCAL FileOpen_typ   DIAGFile_TypOpen;        /* structure for opening file */
_LOCAL FileWrite_typ  DIAGFile_TypWrite;       /* structure for writing data to file */
_LOCAL FileClose_typ  DIAGFile_TypClose;       /* structure for closing file */
_LOCAL FileDelete_typ DIAGDelete;              /* structure for deleting data file */

/************************************************************************/
/*  
   Function:  open_ramp_file()                                       
                                                                      
   Description: This function tries to open and save data to an existing
        configuration file. If the file does not exist, it is created. It
        will save data according to the set block size. If more than a 
        block is needed another block will be created until all of the 
        required data is saved. When finished the file will be closed.

        This routine must run cyclically until all the data is written.

   Returns: SUCCESS
            FAIL
            NOTDONE 
                                                                        */
/************************************************************************/
int open_ramp_file(int *pState)
{
   static unsigned int rampStatus;                              /* return status for writing to file */
   static unsigned int SC_Error;
   int file_stored;
   int result;
   
   result = NOTDONE;
   HMI.pg225_err_state = FALSE;
   HMI.pg225_err_code = FALSE;
   switch (state_Store_Diag)
   {
      case FILE_STORE_INIT:    /* initialize parameters for processing store state machine */
         if(Signal.FILEIO_INUSE)
         {
            break;
         }
         else
         {
            file_stored = FAIL;
            SC_Error = 0;
            state_Store_Diag = FILE_STORE_CREATE;
            Signal.FILEIO_INUSE = TRUE;
            DIAGFile_TypWrite.offset   = 0;
         }
         brsstrcpy((UDINT)dStorePath, (UDINT)SYSDrivePath);
         break;
      case FILE_STORE_CREATE:    /* initialize parameters for processing store state machine */
         /* Initialize file create structure */
         DIAGFile_TypCreate.enable  = 1;
         DIAGFile_TypCreate.pDevice = (unsigned long) &dStorePath;  
         DIAGFile_TypCreate.pFile   = (unsigned long) &SYSFilename;
         /* Call FUB */
         FileCreate(&DIAGFile_TypCreate);
         /* Get output information of FBK */
         dFileIDent = DIAGFile_TypCreate.ident;
         rampStatus = DIAGFile_TypCreate.status;
         /* Verify status */
         if (rampStatus == brNOERROR)             /* all is well */
         {
            state_Store_Diag = FILE_STORE_BUILD;     /* got to build record state */
            result = SUCCESS;
            *pState = SUCCESS;
            file_stored = SUCCESS;
         }
         else if (rampStatus == fiERR_EXIST)/* file is already there */
         {
            state_Store_Diag = FILE_STORE_WAIT;
            ramp_file_exists = TRUE;
            break;
         }
         else if (rampStatus != brBUSY)    /* if 65535 (busy), else error state */
         {
            *pState = FAIL;
            dErrorLevel = Def_ErrLvlCreate;
            if (rampStatus == brSystemError)
               dsysError = FileIoGetSysError();
            state_Store_Diag = FILE_STORE_ERROR;
         }
      break;
      case FILE_STORE_WAIT:
         *pState = rampStatus; 
      break;
      case FILE_STORE_DELETE:
         /* delete old data file GTF */
         DIAGDelete.enable    = 1;
         DIAGDelete.pDevice = (unsigned long) &dStorePath;
         DIAGDelete.pName   = (unsigned long) &SYSFilename;
         /* Call FBK */
         FileDelete(&DIAGDelete);
         /* Get status */
         rampStatus = DIAGDelete.status;
         if(rampStatus == brNOERROR)
         {
            /* delete successful go on GTF */
            state_Store_Diag = FILE_STORE_CREATE;
         }
         else if (rampStatus == brFILENOTFOUND)
         {
            /* no old backup file so no problem GTF */
            state_Store_Diag = FILE_STORE_CREATE;
         }
         else if (rampStatus != brBUSY)
            /* finished with some other error GTF */
            state_Store_Diag = FILE_STORE_ERROR;
      break;
      case FILE_STORE_OPEN:    /* initialize parameters for processing store state machine */
         /* Initialize file create structure */
         DIAGFile_TypOpen.enable  = 1;
         DIAGFile_TypOpen.pDevice = (unsigned long) &dStorePath;  
         DIAGFile_TypOpen.pFile   = (unsigned long) &SYSFilename;
         DIAGFile_TypOpen.mode    = FILE_RW;   /* Read and write access */

         /* Call FUB */
         FileOpen(&DIAGFile_TypOpen);
         /* Get output information of FBK */
         dFileIDent = DIAGFile_TypOpen.ident;
         rampStatus = DIAGFile_TypOpen.status;
         /* Verify status */
         if (rampStatus == brNOERROR)             /* all is well */
         {
            state_Store_Diag = FILE_STORE_BUILD;     /* got to build record state */
            result = SUCCESS;
            *pState = SUCCESS;
            file_stored = SUCCESS;
         }
         else if (rampStatus != brBUSY)    /* if 65535 (busy), else error state */
         {
            *pState = FAIL;
            dErrorLevel = Def_ErrLvlCreate;
            if (rampStatus == brSystemError)
               dsysError = FileIoGetSysError();
            state_Store_Diag = FILE_STORE_ERROR;
         }
      break;
      case FILE_STORE_BUILD:     /* build block to write */
         if(Signal.diag_line_ready)
         {
            state_Store_Diag = FILE_STORE_WRITE;    /* write data state */
         }
         else if(Signal.diag_file_done)
         {
            state_Store_Diag = FILE_STORE_DONE; /* done adding lines close the file */
            break;
         }
         else
            break;
      case FILE_STORE_WRITE:
         /* Initialize file write structure */
         DIAGFile_TypWrite.enable   = 1;
         DIAGFile_TypWrite.ident    = dFileIDent;
         /*  Point to mixer config structure */
         DIAGFile_TypWrite.pSrc     = (unsigned long)&rline;
         DIAGFile_TypWrite.len      = brsstrlen((UDINT)rline); /* sizeof(rline); */
         /* write Mixer to file  */
         FileWrite(&DIAGFile_TypWrite);
         /* Verify status */
         rampStatus = DIAGFile_TypWrite.status;
         if (rampStatus == 0)
         {
            state_Store_Diag = FILE_STORE_NEXT;
            /* add 1 just to make sure structures don't overlap */
            DIAGFile_TypWrite.offset   += DIAGFile_TypWrite.len +1; 
/*            Signal.diag_line_ready = FALSE; */
         }
         else if (rampStatus != brBUSY){
            HMI.pg225_err_state = state_Store_Diag;
            HMI.pg225_err_code = rampStatus;
            state_Store_Diag = FILE_STORE_ERROR;
         }
      break;
      case FILE_STORE_NEXT:
         state_Store_Diag = FILE_STORE_BUILD;
         file_stored = SUCCESS;
         *pState = SUCCESS;
         result = SUCCESS;
         break;
      case FILE_STORE_DONE:
            state_Store_Diag = FILE_STORE_CLOSE;
      case FILE_STORE_CLOSE:        /* close file */
         /* Initialize file close structure */
         DIAGFile_TypClose.enable   = 1;
         DIAGFile_TypClose.ident    = dFileIDent;
         /* Call FBK */
         FileClose(&DIAGFile_TypClose);
         /* Get status */
         rampStatus = DIAGFile_TypClose.status;
         /* Verify status */
         if (rampStatus == brNOERROR)
         {
            Signal.FILEIO_INUSE = FALSE;
            state_Store_Diag = FILE_STORE_INIT;   /* reset state variable */
            file_stored=SUCCESS;
            result = SUCCESS;
            return(result);                        /* file written and closed return SUCCESS */  
         }
         else if (rampStatus != brBUSY)
         {
            Signal.FILEIO_INUSE = FALSE;
            dErrorLevel = Def_ErrLvlClose;
            if (rampStatus == brSystemError)
               dsysError = FileIoGetSysError();
            HMI.pg225_err_state = state_Store_Diag;
            HMI.pg225_err_code = dsysError;
            state_Store_Diag = FILE_STORE_ERROR;
         }
      break;
      case FILE_STORE_ERROR:     /* error state */
         state_Store_Diag = FILE_STORE_INIT;
         Signal.FILEIO_INUSE = FALSE;
         result = FAIL;
         return(result);
      break;
      default:
         if(file_stored != SUCCESS)
            result = NOTDONE;
         return(result);   /* still storing current block or more blocks/files to store */
   }
   if(file_stored != SUCCESS)
      result = NOTDONE;
   return(result);   /* still storing current block or more blocks/files to store */
}
/*****************************************************************************************/
/*                                                                                       */
/*****************************************************************************************/
void BuildLineOfDiag()
{
   int i;
   
   brsstrcpy((UDINT)rline, (UDINT)" ");
   brsitoa((DINT)ramp_data.step_time, (UDINT)&istring);
   brsstrcpy((UDINT)rline, (UDINT)istring);
   brsstrcat((UDINT)rline, (UDINT)",");
   brsitoa((DINT)ramp_data.step_cnt, (UDINT)&istring);
   brsstrcat((UDINT)rline, (UDINT)istring);
   for(i=0; i<pg430_step_count; i++)
   {
      brsstrcat((UDINT)rline, (UDINT)",");
      brsftoa(ramp_data.wt[i], (UDINT)&fstring);
      brsstrcat((UDINT)rline, (UDINT)fstring);
   }
   brsstrcat((UDINT)rline, (UDINT)"\r\n");   
}

