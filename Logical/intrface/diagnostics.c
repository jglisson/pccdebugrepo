
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

/************************************************************************/
/*
   File:  diagnostics.c

   Description: This file controls the visualization as it pertains to the
      diagnostic pages.


      $Log:   F:\Software\BR_Guardian\diagnostics.c_v  $
 *    Rev 1.13   Jan 5, 2009 10:58:00   BMM
 * Weight and time dispense tests fixed.
 *
 *    Rev 1.12   Aug 07 2008 17:17:58   gtf
 * Added Enable/Disable all to set up debug data
 *
 *    Rev 1.11   Jul 17 2008 16:39:28   gtf
 * Added support for 4PP045.0571-K15 PCC specific panel version
 *
 *    Rev 1.11   July 17,  2008 15:05:34   GTF
 * Added support for 4PP045.0571-K15 PCC version of B&R Panel
 *
 *    Rev 1.10   May 28 2008 15:05:34   FMK
 * Got rid of compile warnings.
 *
 *    Rev 1.9   May 13 2008 10:04:20   Barry
 * Fixed some problems with the dispensing test by weight
 *
 *    Rev 1.8   Apr 29 2008 14:46:52   FMK
 * I believe the feed by weight is now ready for
 * testing.
 *
 *    Rev 1.7   Apr 22 2008 10:33:32   FMK
 *
 *
 *    Rev 1.6   Apr 16 2008 14:05:44   Barry
 * Force gate_done to true at start os dispensing test
 *
 *    Rev 1.5   Apr 16 2008 11:52:58   FMK
 * Added diagnostics pages for extruder and
 * haul off devices.
 *
 *    Rev 1.4   Apr 07 2008 14:32:12   FMK
 * Now showing device modes correctly in the
 * diagnostics modes.
 *
 *    Rev 1.3   Mar 24 2008 14:30:12   FMK
 *
 *
 *    Rev 1.2   Feb 01 2008 15:41:36   FMK
 * Added more io modules to the status of the
 * plc rack.
 *
 *    Rev 1.1   Jan 18 2008 09:51:10   FMK
 * Got the running of the dispensing test
 * for time working.
 *
 *    Rev 1.0   Jan 11 2008 10:33:32   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <standard.h>
#include "string.h"
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "plc.h"
#include "signal_pcc.h"
#include "relaydef.h"
#include "bwh_lib.h"
#include "timer.h"
#include "gate_pcc.h"
#include "libbgdbg.h"
#include "diagnostics.h"
#include "constants.h"
#include "recipe.h"

#define DISPENSE_BY_WEIGHT 0
#define DISPENSE_BY_TIME 1
#define DISPENSE_RAMPING 2

#define GATE_TEST_NOT_STARTED				      0
#define GATE_TEST_EMPTY_BWH				      1
#define GATE_TEST_FIRE_GATE					   2
#define GATE_TEST_WAIT_TO_CLOSE_AND_SETTLE	3
#define GATE_TEST_WAIT_PAUSE				      4

#define MAX_TIME_DISPLAY 6

extern void visible(unsigned char *Runtime, unsigned char state);
extern void UpDownDataPoint(int *, unsigned char *, unsigned char *);

unsigned int EmptyHopper();
void Diagnostics_Main();
void clear_cl_diagnostics(void);
void clear_net_diagnostics(void);
void clear_gate_calibration(void);
void output_debug_data(void);
void Diag_IORack();
unsigned int WaitingXMSecs(unsigned int DelayMSecs);                                       
int save_time_weight_table_to_csv(void);
int sprintf(char *out, const char *format, ...);

TON_typ TON_01;		/* Timer for delay between dispense tests */
TIME ElapsedTime;

_GLOBAL bgRingBuffer_typ g_RingBuffer1;

_GLOBAL int DispenseTestState;

unsigned char OutputDebugInit = 0;
unsigned char DispenseTestExecuting = FALSE; /* TRUE if dispense test running */
unsigned char CycleCount = 0;
unsigned char gState_InitDispenseTest;
void enableAllDebug(void);
void disableAllDebug(void);

int save_time_weight_table_to_csv();
	
_GLOBAL float pg430_start_wt;
_GLOBAL float pg430_display_wt;
_GLOBAL float pg430_sum_wt;
_GLOBAL int   pg430_num_pulse;
_GLOBAL int   pg430_pulse_ms;
_GLOBAL int   pg430_pause_ms;
_GLOBAL int   pg430_pulse_type;
_GLOBAL int   pg430_box_fade;
_GLOBAL int   pg430_stringvar;
_GLOBAL int   pg430_start_time;
_GLOBAL int   pg430_step_size;
_GLOBAL int   pg430_curr_time;
_GLOBAL int   pg430_curr_count;

USINT pg435_u8GateTestState = GATE_TEST_NOT_STARTED;
float fLastBWHwt;

#define PULSE_1ST 0
#define PULSE_ALL 1

extern int gPage;

#define DISP_TEST_MIN_WT (1.0 / 453.6) /* in grams */

_LOCAL float target_weight;
_LOCAL float target_tolerance;
_LOCAL int   Pg430_gate_type;
_LOCAL int   pg405color[MAX_GATES];

_LOCAL hmi_time_wt_str time_wt_display[MAX_TIME_DISPLAY];
_LOCAL int hmi_pg411_mgt_index_changed;
_LOCAL int file_result;
_LOCAL int file_state;

_GLOBAL char  file_data[1024];
_GLOBAL UINT  u16TimeToTest, u16PauseToTest;
_GLOBAL USINT u8GateToTest, u8MeasurementPoints;

char tmp_string[10];
_LOCAL FileCreate_typ TWFile_TypCreate;      /* structure for creating file */
_LOCAL FileOpen_typ   TWFile_TypOpen;        /* structure for opening file */
_LOCAL FileWrite_typ  TWFile_TypWrite;       /* structure for writing data to file */
_LOCAL FileClose_typ  TWFile_TypClose;       /* structure for closing file */
_LOCAL FileDelete_typ TWDelete;              /* structure for deleting data file */
_LOCAL UDINT dFileIDent1;

/************************************************************************/
/*
   Function:  Diagnostics_Main()

        This routine must run cyclically until all the data is written.

   Returns: SUCCESS
            FAIL
            NOTDONE 
                                                                        */
/************************************************************************/
int save_time_weight_table_to_csv(void)
{ 
	static unsigned int Current_State = FILE_STORE_INIT, largest_table_entry;
	static unsigned int Offset, row_num;
	int Status, gate_num, gn, rn;

   switch (Current_State)
   {
      case FILE_STORE_INIT:    /* initialize parameters for processing store state machine */
         if (!Signal.FILEIO_INUSE)
         {
            Current_State = FILE_STORE_DELETE;
            Signal.FILEIO_INUSE = TRUE;
         	largest_table_entry = 0;
         	brsmemset((UDINT)file_data, (UDINT)'\0', (UDINT)1024);
	      	row_num = 0;
	      	Offset = 0;	      	
	      	for (gn = 0; gn < cfg_super.total_gates; gn++)
	      	{
	   			for (rn = 0; rn < 200; rn++)
	   		 	{
	        		   if (TIME_WT[gn].entry[199 - rn].avg_wt > 0)  
	        		   {	
						   if ((int)largest_table_entry < 199 - rn)
							   largest_table_entry = 199 - rn;	        			
	        			   break;
					   }
			     }
			  }
		 }		
       return NOTDONE;
       break;

      case FILE_STORE_DELETE:
         TWDelete.enable    = 1;
         TWDelete.pDevice = (unsigned long) "SYSDISK";  
         TWDelete.pName   = (unsigned long) "TimeWeightTable.csv";         
         /* Call FBK */
         FileDelete(&TWDelete);
         /* Get status */
         Status = TWDelete.status;
         switch (Status)
         {
         	case brBUSY:
         		return NOTDONE;
         		break;
         	case brFILENOTFOUND:
         		Current_State = FILE_STORE_CREATE;    
         		return NOTDONE; 
         		break;	         	
         	case brNOERROR:
         		Current_State = FILE_STORE_CREATE;    
         		return NOTDONE; 
         		break;	
         	default:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;
         		return FAIL;          	
         		break;
         }
      	break;         
         
      case FILE_STORE_CREATE:    /* initialize parameters for processing store state machine */
         /* Initialize file create structure */
         TWFile_TypCreate.enable  = 1;
         TWFile_TypCreate.pDevice = (unsigned long) "SYSDISK";  
         TWFile_TypCreate.pFile   = (unsigned long) "TimeWeightTable.csv";         
         /* Call FUB */
         FileCreate(&TWFile_TypCreate);
         /* Get output information of FBK */
         dFileIDent1 = TWFile_TypCreate.ident;
         Status = TWFile_TypCreate.status;
         /* Verify status */
         switch (Status)
         {
         	case brBUSY:
         		return NOTDONE;
         		break;
         	case brNOERROR:
         		Current_State = FILE_STORE_OPEN;     
         		return NOTDONE;
         		break;	
         	default:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;
         		return FAIL;          	
         		break;
         }
      	break;
      
      case FILE_STORE_OPEN:    /* initialize parameters for processing store state machine */
         /* Initialize file create structure */
         TWFile_TypOpen.enable  = 1;
         TWFile_TypOpen.pDevice = (unsigned long) "SYSDISK";  
         TWFile_TypOpen.pFile   = (unsigned long) "TimeWeightTable.csv";
         TWFile_TypOpen.mode    = FILE_W;   /* Read and write access */

         /* Call FUB */
         FileOpen(&TWFile_TypOpen);
         /* Get output information of FBK */
         dFileIDent1 = TWFile_TypOpen.ident;
         Status = TWFile_TypOpen.status;
         /* Verify status */
         switch (Status)
         {
         	case brBUSY:
         		return NOTDONE;
         		break;
         	case brNOERROR:
         		Current_State = FILE_STORE_BUILD; 
         		return NOTDONE;
         		break;	
         	default:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;
         		return FAIL;          	
         		break;
         }
         break;
      
      case FILE_STORE_BUILD:     /* build block to write */ 
        while (row_num <= largest_table_entry)
        {
        	  for (gate_num = 0; gate_num < cfg_super.total_gates; gate_num++)
        	  {
        		  if (1/*BATCH_RECIPE_EDIT.parts.s[gate_num] > 0*/) /* AdBi: I do not care if the hopper is in the recipe; just give me the available TWT*/
        		  {
	        		  brsitoa((DINT)TIME_WT[gate_num].entry[row_num].avg_time, (UDINT)&tmp_string);
					  brsstrcat((UDINT)file_data, (UDINT)tmp_string);
					  brsstrcat((UDINT)file_data, (UDINT)",");
					  if (TIME_WT[gate_num].entry[row_num].avg_wt > 0) 	
        			  {
						  brsftoa(TIME_WT[gate_num].entry[row_num].avg_wt, (UDINT)&tmp_string);
	        			  brsstrcat((UDINT)file_data, (UDINT)tmp_string);
	        		  }
	        		  else
	        			  brsstrcat((UDINT)file_data, (UDINT)"0");
					  brsstrcat((UDINT)file_data, (UDINT)",");
					  brsitoa((DINT)TIME_WT[gate_num].entry[row_num].numpts, (UDINT)&tmp_string);
					  brsstrcat((UDINT)file_data, (UDINT)tmp_string);

	        	  }
	        	  else
	        		  brsstrcat((UDINT)file_data, (UDINT)"N/A,N/A");
	        		
	        	  if (gate_num < (cfg_super.total_gates - 1)) 
                 brsstrcat((UDINT)file_data, (UDINT)",");
        	  }
        	  brsstrcat((UDINT)file_data, (UDINT)"\r\n");
        	  if (((row_num++ + 1) % 10) == 0) 
              break; /* do a disk write every 5 rows */
        }
        Current_State = FILE_STORE_WRITE;     
        return NOTDONE;
        break;
            
      case FILE_STORE_WRITE:
         /* Initialize file write structure */
         TWFile_TypWrite.enable   = 1;
         TWFile_TypWrite.ident    = dFileIDent1;
         TWFile_TypWrite.offset   = Offset;
         /*  Point to mixer config structure */
         TWFile_TypWrite.pSrc     = (unsigned long) &file_data;
         TWFile_TypWrite.len      = brsstrlen((UDINT)file_data);
         /* write Mixer to file  */
         FileWrite(&TWFile_TypWrite);
         Status = TWFile_TypWrite.status;
         /* Verify status */
         switch (Status)
         {
         	case brBUSY:
         		return NOTDONE;
         		break;
         	case brNOERROR:
         		if (row_num > largest_table_entry)
         		{
         			Offset = 0;
         			Current_State = FILE_STORE_CLOSE;    
         		}
         		else
         		{
         			Offset += brsstrlen((UDINT)file_data);
         			brsmemset((UDINT)file_data, (USINT)'\0', (UDINT)1024);
         			Current_State = FILE_STORE_BUILD;
         		}    
         		return NOTDONE;
         		break;	
         	default:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;
         		return FAIL;          	
         		break;
         }
      	break;
      case FILE_STORE_CLOSE:        /* close file */
         /* Initialize file close structure */
         TWFile_TypClose.enable   = 1;
         TWFile_TypClose.ident    = dFileIDent1;
         /* Call FBK */
         FileClose(&TWFile_TypClose);
         /* Get status */
         Status = TWFile_TypClose.status;
         /* Verify status */
         switch (Status)
         {
         	case brBUSY:
         		return NOTDONE;
         		break;
         	case brNOERROR:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;         	
         		return SUCCESS;     
         		break;	
         	default:
         		Signal.FILEIO_INUSE = FALSE;
         		Current_State = FILE_STORE_INIT;
         		return FAIL;          	
         		break;
         }
         break;
      }
   return NOTDONE;
}

/************************************************************************/
/*  
   Function:  Diagnostics_Main()                                       
                                                                      
   Description:  main cyclic task for the diagnostics function            
                                                                        */
/************************************************************************/
void Diagnostics_Main()
{
   /* these should be used on pg 411 and unfortuately but field where needed on takes
      only fixed value.  Left here as a reminder if underlaying #defines ever change GTF*/
   //static int pg411_time_index_max = SIZE_TIME_WT_ARRAY -1; /* array is 0 based GTF */
   //static int pg411_gate_index_max = MAX_GATES -1; /* array is 0 based GTF */
	int dind;
	int gind;
	int index,i;
	//float slope;
	static int waszero = 0;
	
	shared_LIW_device *sext = &shrEXT;
	//config_LIW_device *cext = &cfgEXT;

	static int first_pulse_done = FALSE;
	static int u8EmptyHopperCount = 0, pg430_pulse_count;

	switch (HMI.CurrentPage)
	{
		case pg399_Diag:
			visible(&HMI.SetupRuntime1,TRUE);
			visible(&HMI.SetupRuntime2,TRUE);
			visible(&HMI.SetupRuntime3,TRUE);
			visible(&HMI.SetupRuntime4,TRUE);
			visible(&HMI.SetupRuntime5,TRUE);
			if (BLN_OPER_MODE == PAUSE_MODE) /* Show the dispense test only if blender paused */
				HMI.BlendPauseLock = 1;
			else
				HMI.BlendPauseLock = 0;

			if (HMI.Action_Input0 == gcActive)   /* disable service codes,  lol disabled!*/
			{
				cfg_super.current_security_level = cfg_super.minimum_security_level;
				HMI.Action_Input0 = gcInActive;
				shr_global.dbg_what = 0;
				HMI.CurrentPage = pg000_MainPage;
				HMI.ChangePage = pg000_MainPage;
			}
			if (HMI.Action_Input1 == gcActive)  /* clear control loop diagnostic data */
			{
				HMI.Action_Input1 = gcInActive;
				clear_cl_diagnostics();
			}
			if (HMI.Action_Input2 == gcActive)  /* clear net diagnostic data */
			{
				HMI.Action_Input2 = gcInActive;
				clear_net_diagnostics();
			}
			if (HMI.Action_Input4 == gcActive)  /* clear wm calibration */
			{
				HMI.Action_Input4 = gcInActive;
				clear_net_diagnostics();
			}
			if (HMI.Action_Input5 == gcActive)  /* clear gate calibration */
			{
				HMI.Action_Input5 = gcInActive;
				clear_gate_calibration();
			}

			if (HMI.Action_Input6 == gcActive)  /* clear gate calibration */
			{
				HMI.Action_Input6 = gcInActive;
				DispenseTestExecuting = FALSE;
				if (HMI.pg430_TargetTime < 25)
					HMI.pg430_TargetTime = 50;
				if ((HMI.pg430_DispenseMethod > 2)||(HMI.pg430_DispenseMethod < 0))
					HMI.pg430_DispenseMethod = 1;
				if (HMI.pg430_numtests < 1)
					HMI.pg430_numtests = 1;
				if (HMI.pg430_HopperToTest > (cfg_super.total_gates -1))
					HMI.pg430_HopperToTest = 0;
				DispenseTestState = STATE_INIT;
				HMI.pg430_dispensetestColor = gGreen;
				gState_InitDispenseTest = TRUE;
				visible(&HMI.PopupRuntime1,FALSE);
				visible(&HMI.PopupRuntime2,FALSE);

				/* configure menu to show only valid gates available */
				for (i=0; i<MAX_GATES; i++)
				{
					if (i < cfg_super.total_gates)
						HMI.SetupOptionPoint1[i] = 0;
					else
						HMI.SetupOptionPoint1[i] = 0xff;
				}
				HMI.ChangePage = pg430_Diag_DispenseTest;
			}
			break;

		case pg401_Diag_SelectDevice:
			break;
		case pg402_Diag_BWH:
			switch(shrBWH.oper_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Auto");
					break;
				case WAITING_FOR_NEW_FEED:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Wait");
					break;
				case FAST_AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"FastAuto");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Monitor");
					break;
				case CALIBRATE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Calib");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Purge");
					break;
				case DISPENSING_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"Dispense");
					break;
				case NEW_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"New");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg402_BWH.OperMode, (UDINT)"N/A");
					break;
			}
			switch(shrBWH.running_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Auto");
					break;
				case WAITING_FOR_NEW_FEED:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Wait");
					break;
				case FAST_AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"FastAuto");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Monitor");
					break;
				case CALIBRATE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Calib");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Purge");
					break;
				case DISPENSING_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"Dispense");
					break;
				case NEW_MODE:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"New");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg402_BWH.RunMode, (UDINT)"N/A");
					break;
			}
			
			if (HMI.Action_Input0 == gcActive)
			{
				HMI.Action_Input0 = gcInActive;
				shrBWH.MinAD = shrBWH.wt_bits;
				shrBWH.MaxAD = shrBWH.wt_bits;
			}
			break;
		case pg403_Diag_Mixer:  /* Mixer diagnostics */
			visible(&HMI.SetupRuntime2,TRUE);
			visible(&HMI.SetupRuntime3,TRUE);
			visible(&HMI.SetupRuntime4,TRUE);
			visible(&HMI.SetupRuntime5,TRUE);
			if (cfgMIXER.aux_starter_used)
				visible(&HMI.SetupRuntime1,TRUE);  /* show aux starter contact status */
			else
				visible(&HMI.SetupRuntime1,FALSE); /* hidden aux starter contact status */

			switch(shrMIXER.oper_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Auto");
					break;
				case SUSPEND:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Suspend");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Monitor");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Purge");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg402_Mix.OperMode, (UDINT)"Unknown");
					break;
			}
			switch(shrMIXER.mixer_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Auto");
					break;
				case MIXER_WAITING_EMPTY:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"WaitEmpty");
					break;
				case TIME_MIXER:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Timing");
					break;
				case MIXER_WAITING_FULL:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"WaitFull");
					break;
				case MIXER_DUMPING:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Dumping");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Purge");
					break;
				case MIXER_CHECK_MATERIAL:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Checking");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg402_Mix.RunMode, (UDINT)"Unknown");
					break;
			}
			break;
		case pg405_Diag_Gate:
			visible(&HMI.SetupRuntime1,TRUE);
			visible(&HMI.SetupRuntime2,TRUE);
			visible(&HMI.SetupRuntime3,TRUE);
			visible(&HMI.SetupRuntime4,TRUE);
			visible(&HMI.SetupRuntime5,TRUE);

			if (HMI.Action_Input0 == gcActive) // change layer
			{  
				UpDownDataPoint(&HMI.pg405change_layer, &HMI.pg405_LayerRuntime1, &HMI.pg405_LayerRuntime2);
				HMI.Action_Input0 = gcInActive;
			} 

			for(i=0; i<cfg_super.total_gates; i++)
			{
            if (BATCH_RECIPE_NOW->parts_pct.s[i] > 0.0) //G2-418
            {
				   if (shrGATE[i].learn_mode == 1)
					   pg405color[i] = 34; /* yellow */
			 	   else
					   pg405color[i] = 53; /* background */
            }
            else //G2-418
                pg405color[i] = 53; /* background */
			}
			break;
		case pg406_Diag_Gate:
			visible(&HMI.SetupRuntime1,TRUE);  
			visible(&HMI.SetupRuntime2,TRUE);  
			visible(&HMI.SetupRuntime3,TRUE);  
			visible(&HMI.SetupRuntime4,TRUE);  
			visible(&HMI.SetupRuntime5,TRUE);  

			if (HMI.Action_Input0 == gcActive) // change layer
			{  
				UpDownDataPoint(&HMI.pg406change_layer, &HMI.pg406_LayerRuntime1, &HMI.pg406_LayerRuntime2);
				HMI.Action_Input0 = gcInActive;
			} 
			
			for (i=0; i<=cfg_super.total_gates; i++)
			{
				switch(shrGATE[i].oper_mode)
				{
					case PAUSE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Paused");
						break;
					case MAN_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Manual");
						break;
					case AUTO_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Auto");
						break;
					case WAITING_FOR_NEW_FEED:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Wait");
						break;
					case FAST_AUTO_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"FastAuto");
						break;
					case MONITOR_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Monitor");
						break;
					case CALIBRATE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Calib");
						break;
					case PURGE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Purge");
						break;
					case DISPENSING_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"Dispense");
						break;
					case NEW_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"New");
						break;
					default:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].OperMode, (UDINT)"N/A");
						break;
				}
				switch(shrGATE[i].running_mode)
				{
					case PAUSE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Paused");
						break;
					case MAN_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Manual");
						break;
					case AUTO_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Auto");
						break;
					case WAITING_FOR_NEW_FEED:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Wait");
						break;
					case FAST_AUTO_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"FastAuto");
						break;
					case MONITOR_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Monitor");
						break;
					case CALIBRATE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Calib");
						break;
					case PURGE_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Purge");
						break;
					case DISPENSING_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"Dispense");
						break;
					case NEW_MODE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"New");
						break;
					default:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].RunMode, (UDINT)"N/A");
						break;
				}
				switch(shrGATE[i].feed_state)
				{
					case START:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Start");
						break;
					case PREPULSE_GATE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"PrePls");
						break;
					case CHECKING_PREPULSE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Chk Pre");
						break;
					case PULSE_GATE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Pulse");
						break;
					case WAITING_FOR_GATE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"GWait");
						break;
					case WAITING_FOR_SETTLING:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"SWait");
						break;
					case CHECK_WT:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Chk Wt");
						break;
					case DONE:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Done");
						break;
					default:
						brsstrcpy((UDINT)HMI.pg402_Gate[i].FeedMode, (UDINT)"Unknown");
						break;
				}
			}
			
			if (HMI.pg406_LayerRuntime1 == 1 && HMI.pg406_LayerRuntime2 == 1)
			{
				HMI.pg406_ClearButtonA_Visibilty = 0;
				HMI.pg406_ClearButtonB_Visibilty = 0;
				HMI.pg406_ClearButtonC_Visibilty = 0;
				HMI.pg406_ClearButtonD_Visibilty = 0;
			}
			else
			{
				HMI.pg406_ClearButtonA_Visibilty = 1;	
				HMI.pg406_ClearButtonB_Visibilty = 1;
				HMI.pg406_ClearButtonC_Visibilty = 1;
				HMI.pg406_ClearButtonD_Visibilty = 1;
			}
			
			if (HMI.pg406_clearA)
			{
				cfgGATE[0].gate_pulse_number = 0;
				index = 0;
				HMI.pg406_clearA = FALSE;
			}
			if (HMI.pg406_clearB)
			{
				cfgGATE[1].gate_pulse_number = 0;
				index = 1;
				HMI.pg406_clearB = FALSE;
			}
			if (HMI.pg406_clearC)
			{
				cfgGATE[2].gate_pulse_number = 0;
				index = 2;
				HMI.pg406_clearC = FALSE;
			}
			if (HMI.pg406_clearD)
			{
				cfgGATE[3].gate_pulse_number = 0;
				index = 3;
				HMI.pg406_clearD = FALSE;
			}
			if (HMI.pg406_clearE)
			{
				cfgGATE[4].gate_pulse_number = 0;
				index = 4;
				HMI.pg406_clearE = FALSE;
			}
			if (HMI.pg406_clearF)
			{
				cfgGATE[5].gate_pulse_number = 0;
				index = 5;
				HMI.pg406_clearF = FALSE;
			}
			if (HMI.pg406_clearG)
			{
				cfgGATE[6].gate_pulse_number = 0;
				index = 6;
				HMI.pg406_clearG = FALSE;
			}
			if (HMI.pg406_clearH)
			{
				cfgGATE[7].gate_pulse_number = 0;
				index = 7;
				HMI.pg406_clearH = FALSE;
			}
			if (HMI.pg406_clearI)
			{
				cfgGATE[8].gate_pulse_number = 0;
				index = 8;
				HMI.pg406_clearI = FALSE;
			}
			if (HMI.pg406_clearJ)
			{
				cfgGATE[9].gate_pulse_number = 0;
				index = 9;
				HMI.pg406_clearJ = FALSE;
			}
			if (HMI.pg406_clearK)
			{
				cfgGATE[10].gate_pulse_number = 0;
				index = 10;
				HMI.pg406_clearK = FALSE;
			}
			if (HMI.pg406_clearL)
			{
				cfgGATE[11].gate_pulse_number = 0;
				index = 11;
				HMI.pg406_clearL = FALSE;
			}			
			break;
		case pg407_Diag_SWD: 	
			if (HMI.Action_Input0 == 1)    /* Next Set button pressed */
			{
				HMI.ChangePage =  pg411_Diag_TimeWt;
				HMI.Action_Input0 = 0;
				break;
			}
			if (HMI.Action_Input1 == 1)    /* Prev Set button pressed */
			{
				HMI.ChangePage = pg406_Diag_Gate;
				HMI.Action_Input1 = 0;
				break;
			} 
			if (HMI.Action_Input2 == gcActive) // change layer
			{  
				UpDownDataPoint(&HMI.pg345change_layer, &HMI.pg345_LayerRuntime1, &HMI.pg345_LayerRuntime2);
				HMI.Action_Input2 = gcInActive;
			} 
			
			if (shr_global.material_request == TRUE)
				HMI.pg330_MaterialReqColor = gGreen;
			else
				HMI.pg330_MaterialReqColor = gBlack;
			/* if enabled make it visible */
			if (Signal.doMaterialRequestAlarm & (cfgMIXER.UncoveredMatReqSwitchAlarmTime >= 15.0))
				visible(&HMI.pg330_MaterialReqRuntime, TRUE);
			else
				visible(&HMI.pg330_MaterialReqRuntime, FALSE);

			/*
			index = 99;            
			if(HMI.pg406_clearA)
			{
				cfgGATE[0].gate_pulse_number = 0;
				index = 0;
				HMI.pg406_clearA = FALSE;
			}
			if(HMI.pg406_clearB)
			{
				index = 1;
				HMI.pg406_clearB = FALSE;
			}
			if(HMI.pg406_clearC)
			{
				index = 2;
				HMI.pg406_clearC = FALSE;
			}
			if(HMI.pg406_clearD)
			{
				index = 3;
				HMI.pg406_clearD = FALSE;
			}
			if(HMI.pg406_clearE)
			{
				index = 4;
				HMI.pg406_clearE = FALSE;
			}
			if(HMI.pg406_clearF)
			{
				index = 5;
				HMI.pg406_clearF = FALSE;
			}
			if(HMI.pg406_clearG)
			{
				index = 6;
				HMI.pg406_clearG = FALSE;
			}
			if(HMI.pg406_clearH)
			{
				index = 7;
				HMI.pg406_clearH = FALSE;
			}

			if(HMI.pg406_clearI)
			{
				index = 8;
				HMI.pg406_clearI = FALSE;
			}

			if(HMI.pg406_clearJ)
			{
				index = 9;
				HMI.pg406_clearJ = FALSE;
			}
			if(HMI.pg406_clearK)
			{
				index = 10;
				HMI.pg406_clearK = FALSE;
			}
			if(HMI.pg406_clearL)
			{
				index = 11;
				HMI.pg406_clearL = FALSE;
			}

			if ((index != 99) && (index < 12))
			{
				shrBWH.batch_stat[index].numOutOfSpec = 0;
				shrBWH.batch_stat[index].gate_avg_wt_deviation = 0;
			}
        
			*/
			if (HMI.pg345_proxtimerCHANGED)
			{
				if ((cfgMIXER.UncoveredMatReqSwitchAlarmTime > 0.0)&(cfgMIXER.UncoveredMatReqSwitchAlarmTime <15.0))
				{
					cfgMIXER.UncoveredMatReqSwitchAlarmTime = 15.0;
				}
				else if (cfgMIXER.UncoveredMatReqSwitchAlarmTime == 0.0)
				{
					cfgMIXER.UncoveredMatReqSwitchAlarmTime =0;
				}
				/* if alarm time is 0 disable it. GTF */
				if (!cfgMIXER.UncoveredMatReqSwitchAlarmTime)
					Signal.doMaterialRequestAlarm = FALSE;
				else
				{
					Signal.doMaterialRequestAlarm = TRUE;
				}
				if (waszero)
				{
					shrMIXER.MatReqSwitchUncoveredTimerStart = start_time();
					waszero = FALSE;
				}
				/* clear the current elapsed time when alarm time is turned off GTF */
				if (cfgMIXER.UncoveredMatReqSwitchAlarmTime == 0)
					shrMIXER.MatReqSwitchUncoveredTimerElap = 0;
				HMI.pg345_proxtimerCHANGED = 0;
			}
			if (cfgMIXER.UncoveredMatReqSwitchAlarmTime == 0.0)
				waszero = 1;
			break;
		
		case pg411_Diag_TimeWt:
			if (HMI.pg411_gate_index >= cfg_super.total_gates)
				HMI.pg411_gate_index = 0;
			gind = HMI.pg411_gate_index;
			HMI.pg411_current_gate = gind;
			if (hmi_pg411_mgt_index_changed)
			{
				hmi_pg411_mgt_index_changed = FALSE;
				cfgGATE[gind].min_gate_time_index = HMI.pg411_mgt_index;
				Signal.EnteredSetup = TRUE; /* need to save cfgGATE to preserve changes GTF */
			}
			HMI.pg411_mgt_index = cfgGATE[gind].min_gate_time_index;
			for (i=0; i<MAX_TIME_DISPLAY; i++)
			{
				dind = HMI.pg411_time_index + i;
				time_wt_display[i].index        = dind;
				time_wt_display[i].numpts       = TIME_WT[gind].entry[dind].numpts;
				time_wt_display[i].nominal_time = TIME_WT[gind].entry[dind].nominal_time;
				time_wt_display[i].avg_time     = TIME_WT[gind].entry[dind].avg_time;
				time_wt_display[i].avg_wt       = TIME_WT[gind].entry[dind].avg_wt;
				time_wt_display[i].expected_wt  = TIME_WT[gind].entry[dind].expected_wt;	/* expected_wt DF*/

				if (HMI.pg411_time_index > 4)
					time_wt_display[i].bg_color = 22;
				else if ((int)cfgGATE[gind].min_gate_time_index == i)
					time_wt_display[i].bg_color = 23;
				else
					time_wt_display[i].bg_color = 22;
			}
			break;
		case pg415_Diag_Ext:
         if (cfg_super.wtp_k != 0.0)
         {
            HMI.pg338_wtp_set = shrEXT.set_wtp / cfg_super.wtp_k;
            HMI.pg338_wtp_act = shrEXT.act_wtp / cfg_super.wtp_k;
         }
			
			HMI.pg225_err_code      = sext->delta_t/TICKSPERSEC;
			HMI.pg445_SupplyCurrent = (float)sext->newrate;
			
         if ((cfgEXT.accum_grav + cfgEXT.accum_coast) > 0.0)
			   HMI.pg415_PercentGravimetric = (cfgEXT.accum_grav/(cfgEXT.accum_grav + cfgEXT.accum_coast)) * 100.00;
         else
            HMI.pg415_PercentGravimetric = 0.0;
			switch (shrEXT.oper_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Auto");
					break;
				case WAITING_FOR_NEW_FEED:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Wait");
					break;
				case FAST_AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"FastAuto");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Monitor");
					break;
				case CALIBRATE_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Calib");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Purge");
					break;
				case DISPENSING_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"Dispense");
					break;
				case NEW_MODE:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"New");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg415_ExtMode.OperMode, (UDINT)"N/A");
					break;
			}
			break;
		case pg416_Diag_HO:
         if (cfg_super.ltp_k != 0.0)
         {
            HMI.pg339_ltp_set = shrHO.set_ltp / cfg_super.ltp_k;
            HMI.pg339_ltp_act = shrHO.act_ltp / cfg_super.ltp_k;
         }
			switch(shrHO.oper_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Auto");
					break;
				case WAITING_FOR_NEW_FEED:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Wait");
					break;
				case FAST_AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"FastAuto");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Monitor");
					break;
				case CALIBRATE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Calib");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Purge");
					break;
				case DISPENSING_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Dispense");
					break;
				case NEW_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"New");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"N/A");
					break;
			}
			break;

		case pg418_Diag_SHO:
         if (cfg_super.ltp_k != 0.0)
         {
            HMI.pg339_ltp_set = shrSHO.set_ltp / cfg_super.ltp_k;
            HMI.pg339_ltp_act = shrSHO.act_ltp / cfg_super.ltp_k;
         }
			switch(shrSHO.oper_mode)
			{
				case PAUSE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Paused");
					break;
				case MAN_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Manual");
					break;
				case AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Auto");
					break;
				case WAITING_FOR_NEW_FEED:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Wait");
					break;
				case FAST_AUTO_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"FastAuto");
					break;
				case MONITOR_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Monitor");
					break;
				case CALIBRATE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Calib");
					break;
				case PURGE_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Purge");
					break;
				case DISPENSING_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"Dispense");
					break;
				case NEW_MODE:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"New");
					break;
				default:
					brsstrcpy((UDINT)HMI.pg416_HOMode.OperMode, (UDINT)"N/A");
					break;
			}
			break;

		case pg417_Diag_Comm:
			if ((HMI.pg417_ToggleComm==0) && (cfg_super.mbenet_enabled == TRUE))
			{
            HMI.pg417_ulIndexMax = MAX_MODBUS_TCP_CLIENTS-1;
				brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Modbus TCP/IP");
				if ( HMI.pg417_ulIndex < MAX_MODBUS_TCP_CLIENTS )
				{
					HMI.pg417_ulSend = ulSendModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulSendError = ulSendErrorModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulRecv = ulRecvModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulRecvError = ulRecvErrorModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulServer = ulServerModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulServerError = ulServerErrorModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulOpen = ulOpenModbusTcpIp[HMI.pg417_ulIndex];
					HMI.pg417_ulOpenError = ulOpenErrorModbusTcpIp[HMI.pg417_ulIndex];
					HMI.TCPIP_State = Mbenet_State[HMI.pg417_ulIndex];
				}
			}
			else if ((HMI.pg417_ToggleComm==1) && (cfg_super.abtcp_enabled == TRUE))
			{
            HMI.pg417_ulIndexMax = MAX_ABTCP_CLIENTS-1;
				brsstrcpy((UDINT)HMI.pg225_sErrorText, (UDINT)"Allen Bradley TCP/IP");
            if ( HMI.pg417_ulIndex < MAX_ABTCP_CLIENTS )
            {
               HMI.pg417_ulSend = ulSendABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulSendError = ulSendErrorABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulRecv = ulRecvABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulRecvError = ulRecvErrorABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulServer = ulServerABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulServerError = ulServerErrorABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulOpen = ulOpenABTcpIp[HMI.pg417_ulIndex];
               HMI.pg417_ulOpenError = ulOpenErrorABTcpIp[HMI.pg417_ulIndex];
               HMI.TCPIP_State = Abtcp_State[HMI.pg417_ulIndex];
            }
			}
			if (HMI.Action_Input0 == 1)
			{
            HMI.Action_Input0=0;
            
            if(cfg_super.abtcp_enabled == TRUE)
            {
               for (i=0; i < MAX_ABTCP_CLIENTS; i++)
               {
                  Abtcp_State[i] = ST_TCPCLOSE;
                  ulSendABTcpIp[i]=0;
                  ulSendErrorABTcpIp[i]=0;
                  ulRecvABTcpIp[i]=0;
                  ulRecvErrorABTcpIp[i]=0;
                  ulServerABTcpIp[i]=0;
                  ulServerErrorABTcpIp[i]=0;
                  ulOpenABTcpIp[i]=0;
                  ulOpenErrorABTcpIp[i]=0;
               }
            }

            if (cfg_super.mbenet_enabled == TRUE)
            {
               for (i=0; i < MAX_MODBUS_TCP_CLIENTS; i++)
               {
                  Mbenet_State[i] = ST_TCPCLOSE;
                  ulSendModbusTcpIp[i]=0;
                  ulSendErrorModbusTcpIp[i]=0;
                  ulRecvModbusTcpIp[i]=0;
                  ulRecvErrorModbusTcpIp[i]=0;
                  ulServerModbusTcpIp[i]=0;
                  ulServerErrorModbusTcpIp[i]=0;
                  ulOpenModbusTcpIp[i]=0;
                  ulOpenErrorModbusTcpIp[i]=0;
               }
            }
            HMI.pg417_ulSend=0;
            HMI.pg417_ulSendError=0;
            HMI.pg417_ulRecv=0;
            HMI.pg417_ulRecvError=0;
            HMI.pg417_ulServer=0;
            HMI.pg417_ulServerError=0;
            HMI.pg417_ulOpen=0;
            HMI.pg417_ulOpenError=0;
         }
			break;
		case pg425_Diag_Output_Debug1:
		case pg426_Diag_Output_Debug2:
		case pg427_Diag_Output_Debug3:
		case pg428_Diag_Output_Debug4:
		case pg429_Diag_Output_Debug5:
			visible(&HMI.SetupRuntime1,TRUE);
			visible(&HMI.SetupRuntime2,TRUE);
			visible(&HMI.SetupRuntime3,TRUE);
			visible(&HMI.SetupRuntime4,TRUE);
			visible(&HMI.SetupRuntime5,TRUE);

			if (HMI.pg425_enable_all_debug)
			{
				enableAllDebug();
				HMI.pg425_enable_all_debug = FALSE;
			}

			if (HMI.pg425_disable_all_debug)
			{
				disableAllDebug();
				HMI.pg425_disable_all_debug = FALSE;
			}
			output_debug_data();
			break;
		case pg436_GateTestView:
			if (HMI.pg435_u8ButtonPressed)
			{
				if (HMI.pg435_u8ButtonPressed == 1) 
					HMI.ChangePage = pg435_GateDelayTest;
				else if (HMI.pg435_u8ButtonPressed == 2) 
				{
					HMI.CurrentPage = pg000_MainPage;
					HMI.ChangePage = pg000_MainPage;
				}
				HMI.pg435_u8ButtonPressed = 0;
				pg435_u8GateTestState = GATE_TEST_NOT_STARTED;		
			}
		case pg435_GateDelayTest:
			switch (pg435_u8GateTestState)
			{
				case GATE_TEST_NOT_STARTED:
					if (HMI.pg435_bGoPressed == TRUE)
					{
						HMI.pg435_bGoPressed = FALSE;
						u8GateToTest = 0;
						while ((HMI.pg435_bGatesToTest[u8GateToTest] == FALSE) && (u8GateToTest < 6))
							u8GateToTest++;
						if (u8GateToTest == 6) ;
							/* no gates selected */
						else
						{ 
							u16TimeToTest = HMI.pg435_u16FirstTime;
							u16PauseToTest = HMI.pg435_u16FirstDelay; 
							u8MeasurementPoints = 1;
							pg435_u8GateTestState = GATE_TEST_EMPTY_BWH;
							u8EmptyHopperCount = 0;
							shrBWH.cur_gate = u8GateToTest;
							shrBWH.wt_stable = FALSE;
							shrBWH.wt_stable_count = 0;
							HMI.ChangePage = pg436_GateTestView;
						}
					}
					break;
				case GATE_TEST_EMPTY_BWH:
					if (u8EmptyHopperCount > 40)
					{
						u8EmptyHopperCount = 0;
						IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
						pg435_u8GateTestState = GATE_TEST_WAIT_PAUSE;
					}
					else
					{
						IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = ON;
						u8EmptyHopperCount++;
					}
					break;
				case GATE_TEST_WAIT_PAUSE:
					if(WaitingXMSecs(u16PauseToTest) == TRUE)
						/*fallthrough*/;
					else
						break;
				case GATE_TEST_FIRE_GATE:
					if (shrBWH.wt_stable)
					{
						shr_global.opengate_time = u16TimeToTest; /* initializes open gate time in opengate.c */
						Signal.OpenGate = TRUE; /* set the open gate to execute */
						pg435_u8GateTestState = GATE_TEST_WAIT_TO_CLOSE_AND_SETTLE;
						shrBWH.wt_stable = FALSE;
						shrBWH.wt_stable_count = 0;
					}
					break;
				case GATE_TEST_WAIT_TO_CLOSE_AND_SETTLE:
					if (Signal.OpenGate == FALSE)
					{  /* gate has closed */ 
						if (u8MeasurementPoints == HMI.pg435_u8AveragePoints)
						{   
							if (shrBWH.wt_stable)
							{
								if(WaitingXMSecs(5000) == TRUE)
								{
									/* got enough measurements and weight is stable */
									bgDbgInfo(&g_RingBuffer1, "Gate test: ", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
										"Gate|%d|Time|%d|Delay|%d|%d Fires Total Weight|%f\r\n", 
										u8GateToTest, u16TimeToTest, u16PauseToTest, u8MeasurementPoints, shrBWH.hop_wt);
									u8MeasurementPoints = 1;
									u16PauseToTest += HMI.pg435_u8DelayStep;
									if (u16PauseToTest > HMI.pg435_u16LastDelay)
									{
										u16PauseToTest = HMI.pg435_u16FirstDelay;
										u16TimeToTest += HMI.pg435_u8TimeStep;
										if (u16TimeToTest > HMI.pg435_u16LastTime)
										{
											u16TimeToTest = HMI.pg435_u16FirstTime;
											u8GateToTest++;
											while ((HMI.pg435_bGatesToTest[u8GateToTest] == FALSE) && (u8GateToTest < 6))
												u8GateToTest++;
											if (u8GateToTest == 6)
												/* no more gates selected */
												pg435_u8GateTestState = GATE_TEST_NOT_STARTED;
											else
											{
												shrBWH.cur_gate = u8GateToTest;
												pg435_u8GateTestState = GATE_TEST_EMPTY_BWH;
												u8EmptyHopperCount = 0;
												bgDbgInfo(&g_RingBuffer1, "Gate test: ", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
													"New Gate %d\r\n", u8GateToTest);
											}
										}
										else
										{
											pg435_u8GateTestState = GATE_TEST_EMPTY_BWH;
											u8EmptyHopperCount = 0;
											bgDbgInfo(&g_RingBuffer1, "Gate test: ", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
												"New Time %d\r\n", u16TimeToTest);
										}
									}
									else
									{
										pg435_u8GateTestState = GATE_TEST_EMPTY_BWH;
										u8EmptyHopperCount = 0;	
									}
								}
								else
									break; /* wait for a few seconds for the user to catch the displayed values */
							}
						}
						else 
						{
							u8MeasurementPoints++;
							pg435_u8GateTestState = GATE_TEST_WAIT_PAUSE;
						}
					}
					break;				
			}
			break;
		case pg430_Diag_DispenseTest:
			target_weight = DISP_TEST_MIN_WT;
			target_tolerance = DISP_TEST_MIN_WT / 40.0;
			HMI.pg430_max_gates =  cfg_super.total_gates - 1;    /* max selctable gate number for pulldown GTF */
			Pg430_gate_type = cfgGATE[HMI.pg430_TargetGate].algorithm;
			if (pg430_pulse_ms < 25)
				pg430_pulse_ms = 50;

			switch (DispenseTestState)
			{
				case STATE_INIT:
					visible(&HMI.SetupRuntime1,TRUE);  
					visible(&HMI.SetupRuntime2,TRUE);
					visible(&HMI.SetupRuntime3,TRUE);
					visible(&HMI.SetupRuntime4,FALSE);  
					visible(&HMI.SetupRuntime5,FALSE); 
					DispenseTestState = STATE_WAITING;
					if (HMI.pg430_numtests < 1)
						HMI.pg430_numtests = 1;
					pg430_num_pulse = 0;
					pg430_pulse_ms = 0;
					pg430_pause_ms = 0;
					pg430_pulse_type = PULSE_1ST;
					pg430_box_fade = 15;
					if (pg430_start_time < 25)
						pg430_start_time = 25;
					if (pg430_step_size < 5)
						pg430_step_size = 5;
					brsstrcpy((UDINT)SYSFilename, (UDINT)"diagdata");
					break;
				case STATE_WAITING:
					HMI.pg430_dispensetestColor = gGreen;
					pg430_start_wt = shrBWH.hop_wt;
					CycleCount = HMI.pg430_numtests;
					HMI.SetupRuntime3 = FALSE; /* make it visible and unlocked GTF */
					pg430_box_fade = 15;
					/* force pg430_step_size to be a multiple of 5 GTF */
					pg430_step_size = pg430_step_size / 5;
					pg430_step_size = pg430_step_size * 5;
					if (HMI.SetupRuntime5 == 2)
						HMI.SetupRuntime5 = 1; /* unlock these in wait mode GTF */
					if (HMI.SetupRuntime7 == 2)
						HMI.SetupRuntime7 = 1; /* unlock these in wait mode GTF */
					visible(&HMI.SetupRuntime8, FALSE);  
					visible(&HMI.SetupRuntime9, FALSE);
					if (pg430_step_count == 0)
						pg430_step_count = 1;
					if (HMI.pg430_DispenseMethod == DISPENSE_RAMPING)
					{
						visible(&HMI.SetupRuntime5,TRUE); 
						visible(&HMI.SetupRuntime6,TRUE);
						visible(&HMI.SetupRuntime7,FALSE);
						pg430_stringvar = 125;
					}
					else
					{
						pg430_stringvar = 60;
						visible(&HMI.SetupRuntime5,FALSE); 
						visible(&HMI.SetupRuntime6,FALSE);
						visible(&HMI.SetupRuntime7,TRUE);
					}
					if (pg430_num_pulse > 0)
					{
						HMI.SetupRuntime4 = FALSE;
					}
					else
					{
						HMI.SetupRuntime4 = TRUE;
					}	
					if (HMI.pg430_DispenseMethod == DISPENSE_BY_WEIGHT)
					{
						visible(&HMI.SetupRuntime1, TRUE);  
						visible(&HMI.SetupRuntime2, FALSE);
					}
					else
					{
						visible(&HMI.SetupRuntime1, FALSE);  
						visible(&HMI.SetupRuntime2, TRUE);
					}	
	
					/* if user pressed Execute Test button */			
					if (HMI.Action_Input0 == gcActive)
					{
						pg430_sum_wt = 0.0;
						pg430_display_wt = shrBWH.hop_wt - pg430_start_wt;
						if (!cfgBWH.cal_done)
						{
							HMI.Action_Input0 = gcInActive; /* deactivate Execute Test button */   		
							gPage = pg352_Calibrate_Weigh;
							HMI.ChangePage = pg451_Recipe_Check;  /* change to calibration page */ 
							break;
						}
							/* blender must be paused */
						else if (BLN_OPER_MODE != PAUSE_MODE)
						{
							visible(&HMI.PopupRuntime1, TRUE);
							if (HMI.Action_Input6 == gcActive)
							{
								HMI.Action_Input6 = gcInActive;
								visible(&HMI.PopupRuntime1, FALSE);
								HMI.Action_Input0 = gcInActive;   
							}
							break;
						}
							/* door must be closed and not in manual backup and mixer not in demo mode GTF */
						else if (shr_global.bln_manual_backup || !(shr_global.interlock_closed || cfgMIXER.demo))
						{
							visible(&HMI.PopupRuntime2,TRUE);
							if (HMI.Action_Input6 == gcActive)
							{
								HMI.Action_Input6 = gcInActive;
								visible(&HMI.PopupRuntime2, FALSE);
								HMI.Action_Input0 = gcInActive;
							}
							break;
						}
							/* normal case */
						else
						{
							pg430_box_fade = 7;       /* make it look locked GTF */
							HMI.SetupRuntime3 = 2;    /* locked while test is running GTF */
							if (pg430_num_pulse > 0)
								HMI.SetupRuntime4 = 2; /* locked while test is running GTF */
							if (HMI.pg430_DispenseMethod == DISPENSE_RAMPING)
								HMI.SetupRuntime5 = 2; /* locked while test is running GTF */
							else
								HMI.SetupRuntime7 = 2; /* locked while test is running GTF */
							HMI.Action_Input0 = gcInActive;
							shrBWH.cur_gate = HMI.pg430_TargetGate;
							HMI.pg430_dispensetestColor = gRed;
							file_state = 0;
							Signal.OpenGate = FALSE;
							if (HMI.pg430_DispenseMethod == DISPENSE_RAMPING)
							{
								DispenseTestState = STATE_GET_FILE_NAME;
								pg430_curr_time = pg430_start_time;
								pg430_curr_count = 0;
								visible(&HMI.SetupRuntime8,TRUE);  
							}
							else if (pg430_num_pulse > 0)
							{
								DispenseTestState = STATE_PREPULSE;
								pg430_pulse_count = 0;
							}
							else if (HMI.pg430_DispenseMethod == DISPENSE_BY_TIME)
								DispenseTestState = STATE_DISPENSE_TIME;
							else
								DispenseTestState = STATE_DISPENSE_WEIGHT;
							shrGATE[HMI.pg430_TargetGate].clear_gate_cal = HMI.pg430_ClearGateCalib;
						}
					}
					break;
				case STATE_GET_FILE_NAME:
					if (pg430_name_input_done == TRUE)
					{
						visible(&HMI.SetupRuntime8,FALSE);  
						pg430_name_input_done = FALSE;
						brsstrcat((UDINT)SYSFilename, (UDINT)".csv");
						state_Store_Diag = FILE_STORE_INIT;
						DispenseTestState = STATE_OPEN_RAMP_FILE;
						Signal.FILEIO_INUSE = FALSE;
					}
					break;
				case STATE_PREPULSE:
					if (!first_pulse_done || 
						(pg430_pulse_type == PULSE_ALL) ||
						(pg430_pulse_count < pg430_num_pulse))
					{
						shr_global.opengate_time = pg430_pulse_ms; /* initializes open gate time in opengate.c */
						Signal.OpenGate = TRUE; /* set the open gate to execute */
						first_pulse_done = TRUE;
						pg430_pulse_count++;
						DispenseTestState = STATE_PAUSE;
					}
					else
						DispenseTestState = STATE_PAUSE;
					break;
				case STATE_PAUSE:
					if (Signal.OpenGate == FALSE)
					{
						if (WaitingXMSecs(pg430_pulse_ms) == TRUE)
						{
							if (pg430_pulse_count >= pg430_num_pulse)
							{
								first_pulse_done = FALSE;
								if (HMI.pg430_DispenseMethod == DISPENSE_BY_TIME)
									DispenseTestState = STATE_DISPENSE_TIME;
								else if (HMI.pg430_DispenseMethod == DISPENSE_RAMPING)
								{
									pg430_pulse_count = 0;
									DispenseTestState = STATE_RAMPING;
								}
								else
									DispenseTestState = STATE_DISPENSE_WEIGHT;
							}
							else
							{
								DispenseTestState = STATE_PREPULSE;
								/*                        pg430_pulse_count = 0; */
							}
						}
					}
					break;
				case STATE_DISPENSE_WEIGHT:
					pg430_display_wt = 0;			
					if (cfgGATE[HMI.pg430_TargetGate].demo) 
						shrBWH.cur_gate = HMI.pg430_TargetGate;
					shrGATE[HMI.pg430_TargetGate].gate_done = FALSE;
					gState_InitDispenseTest = FALSE;
					shrGATE[HMI.pg430_TargetGate].act_wt = 0;
					GateCommand[HMI.pg430_TargetGate].SetWeight = HMI.pg430_TargetWt;
					GateCommand[HMI.pg430_TargetGate].Tolerance = HMI.pg430_TargetTol;
					GateCommand[HMI.pg430_TargetGate].Command = AUTO_MODE;
					GateCommand[HMI.pg430_TargetGate].CommandPresent = TRUE;
					DispenseTestState = STATE_DISPENSE_WAIT;
					break;
				case STATE_DISPENSE_TIME:
					shr_global.opengate_time = HMI.pg430_TargetTime; /* initializes open gate time in opengate.c */
					Signal.OpenGate = TRUE; /* set the open gate to execute */
					DispenseTestState = STATE_DISPENSE_WAIT;
					shrBWH.wt_stable = FALSE;
					shrBWH.wt_stable_count = 0;
					break;
				case STATE_OPEN_RAMP_FILE:
					if (file_state == SUCCESS)
					{
						if (pg430_num_pulse == 0)
							DispenseTestState = STATE_RAMPING;
						else
							DispenseTestState = STATE_PREPULSE;
						break;
					}
					warning_answered = FALSE;
					file_result = open_ramp_file(&file_state);
					if (file_state == fiERR_EXIST)
					{
						DispenseTestState = STATE_RAMP_FILE_EXISTS; 
						break;
					}
					else if ((file_result == SUCCESS)||(file_state == SUCCESS))
					{
						if (pg430_num_pulse == 0)
							DispenseTestState = STATE_RAMPING;
						else
							DispenseTestState = STATE_PREPULSE;
					}
					else if(file_result == FAIL)
						DispenseTestState = STATE_WAITING;
					break;
				case STATE_RAMP_FILE_EXISTS:
					visible(&HMI.SetupRuntime9, TRUE);
					if (warning_answered)
					{
						if (warning_answered == 2)
						{
							file_result = open_ramp_file(&file_state);
							visible(&HMI.SetupRuntime9, FALSE);
							if (file_result == SUCCESS)
							{
								if (pg430_num_pulse == 0)
									DispenseTestState = STATE_RAMPING;
								else
									DispenseTestState = STATE_PREPULSE;
								warning_answered = 0;
							}
							else if(file_result == FAIL)
								DispenseTestState = STATE_WAITING;
							break;
						}
						else
						{
							visible(&HMI.SetupRuntime9, FALSE);
							DispenseTestState = STATE_WAITING;
							state_Store_Diag = FILE_STORE_INIT;
							Signal.FILEIO_INUSE = FALSE;
							warning_answered = 0;
						}
						visible(&HMI.SetupRuntime9, FALSE);
					}
					else
						state_Store_Diag = FILE_STORE_DELETE;
					break;
				case STATE_RAMPING:
					shr_global.opengate_time = pg430_curr_time; /* initializes open gate time in opengate.c */
					Signal.OpenGate = TRUE; /* set the open gate to execute */
					DispenseTestState = STATE_DISPENSE_WAIT;
					shrBWH.wt_stable = FALSE;
					shrBWH.wt_stable_count = 0;
					break;
				case STATE_DISPENSE_WAIT: /* waiting for time weight routines in other cyclic to complete */
					if ((HMI.pg430_DispenseMethod == DISPENSE_BY_TIME)||(HMI.pg430_DispenseMethod == DISPENSE_RAMPING))
					{
						if ((Signal.OpenGate == FALSE)&& shrBWH.wt_stable)
						{
							if (HMI.pg430_DispenseMethod != DISPENSE_RAMPING)
							{
								if (WaitingXMSecs(3000) == TRUE)
								{
									if (HMI.pg430_DispenseMethod == DISPENSE_BY_TIME)
										HMI.pg430_numtests--;
									DispenseTestState = STATE_DELAY1;
									pg430_display_wt = shrBWH.hop_wt - pg430_start_wt;
								}
							}
							else
							{
								if (WaitingXMSecs(500) == TRUE)
								{
									DispenseTestState = STATE_DELAY1;
									pg430_display_wt = shrBWH.hop_wt - pg430_start_wt;
								}
							}
						}
					}
					else
					{
						if (shrGATE[HMI.pg430_TargetGate].gate_done == TRUE) 
						{
							HMI.pg430_numtests--;
							DispenseTestState = STATE_DELAY1;
							pg430_display_wt = shrBWH.hop_wt - pg430_start_wt;
							/*pg430_display_wt = shrGATE[HMI.pg430_TargetGate].act_wt;*/
						}					
					}
					break;
				case STATE_DELAY1: /* wait for user to write down weight */
					if ((WaitingXMSecs(3000) == TRUE)||( HMI.pg430_DispenseMethod == DISPENSE_RAMPING))
					{
						DispenseTestState = STATE_EMPTYHOPPER;
						shrBWH.wt_stable = FALSE;
						shrBWH.wt_stable_count = 0;
					}
					break;
				case STATE_EMPTYHOPPER: /* dump the weight hopper */
					if (EmptyHopper() == TRUE)
						DispenseTestState = STATE_DELAY2;
					break;
				case STATE_DELAY2: /* wait 1/2 second for weight hopper gate to close */
					if (shrBWH.wt_stable)
					{
/*                  if(WaitingXMSecs(pg430_pause_ms) == TRUE)
                  { */
						pg430_start_wt = shrBWH.hop_wt; /* reset after every test */
						pg430_sum_wt += pg430_display_wt;
						if (HMI.pg430_DispenseMethod == DISPENSE_RAMPING)
						{
							ramp_data.step_time = pg430_curr_time;
							ramp_data.step_cnt = pg430_step_count;
							ramp_data.wt[pg430_curr_count] = pg430_display_wt;
							pg430_curr_count += 1;
							if (pg430_curr_count == pg430_step_count)
							{
								pg430_curr_count = 0;
								pg430_curr_time += pg430_step_size;
								DispenseTestState = STATE_ADD_RAMP_LINE;
							}
							else
							{
								if ((pg430_pulse_type == PULSE_ALL) && (pg430_num_pulse > 0))
								{
									pg430_pulse_count = 0;
									DispenseTestState = STATE_PREPULSE;
								}
								else
									DispenseTestState = STATE_RAMPING;
							}
						}
						else if ((HMI.pg430_numtests) > 0)
						{
							shrGATE[HMI.pg430_TargetGate].clear_gate_cal = FALSE; /* clear so only done once for repeat tests */ 
							if ((pg430_pulse_type)&&(pg430_num_pulse >0))
							{
								DispenseTestState = STATE_PREPULSE;
								pg430_pulse_count = 0;
							}
							else if (HMI.pg430_DispenseMethod == DISPENSE_BY_TIME)
								DispenseTestState = STATE_DISPENSE_TIME;
							else
								DispenseTestState = STATE_DISPENSE_WEIGHT;
						}
						else
						{
							HMI.pg430_numtests = CycleCount;
							DispenseTestState = STATE_WAITING;
						}
						pg430_start_wt = shrBWH.hop_wt;
						if (debugFlags.dispense_test)
						{
							bgDbgInfo(&g_RingBuffer1, "dispense.c", (char *)__func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
								"Gate %d,time %d,tgt_wt %f,tol_wt %f,wt %f, \r\n",
								HMI.pg430_TargetGate, HMI.pg430_TargetTime, HMI.pg430_TargetWt, target_tolerance, pg430_display_wt);
						}
						/*                  } */
					}
					break;
				case STATE_ADD_RAMP_LINE:
					if (!Signal.diag_line_ready)
					{
						BuildLineOfDiag();
						ramp_data.step_time = 0;
						ramp_data.step_cnt = 0;
						for (i=0; i<10; i++)
							ramp_data.wt[i] = 0;
						Signal.diag_line_ready = TRUE;
						state_Store_Diag = FILE_STORE_BUILD;
					}
					else
					{
						file_result = open_ramp_file(&file_state);
						if (file_result == SUCCESS)
						{
							Signal.diag_line_ready = FALSE;
							if (pg430_curr_time > HMI.pg430_TargetTime)
							{
								DispenseTestState = STATE_CLOSE_RAMP_FILE;
							}
							else
							{
								pg430_sum_wt = 0.0;
								if (pg430_num_pulse == 0)
									DispenseTestState = STATE_RAMPING;
								else
								{
									DispenseTestState = STATE_PREPULSE;
									pg430_pulse_count = 0;
									first_pulse_done = FALSE;
								}
							}
						}
						else if (file_result == FAIL)
						{
							Signal.diag_line_ready = FALSE;
							DispenseTestState = STATE_WAITING;
						}
					}
					break; /* DispenseTestState */
				case STATE_CLOSE_RAMP_FILE:
					Signal.diag_file_done = TRUE;
					file_result = open_ramp_file(&file_state);
					if (file_result == SUCCESS)
					{
						Signal.diag_file_done = TRUE;
						DispenseTestState = STATE_WAITING;
					}
					else if (file_result == FAIL)
					{
						Signal.diag_file_done = TRUE;
						DispenseTestState = STATE_WAITING;
					}
					break;
				default:
					break;
			}
			break; /* HMI.current_page = pg430 */
		case pg445_Diag_IORack:
			Diag_IORack();
			break;
		case pg446_Env_Temp:
			HMI.pg445_EnvTemperature = PLC.TouchPanel.TemperatureENV;
			break;
		default:
			break;
	}
}

void disableAllDebug (void)
{
  int i;

  debugFlags.openGate.init_opengate   = FALSE;
  debugFlags.openGate.cyclic_opengate = FALSE;
  debugFlags.extDrive                 = FALSE;
  debugFlags.exdDrive                 = FALSE;
  debugFlags.relay                    = FALSE;
  debugFlags.timer                    = FALSE;
  debugFlags.hoDrive                  = FALSE;
  debugFlags.adScan                   = FALSE;
  debugFlags.calAd                    = FALSE;
  debugFlags.calBwh                   = FALSE;
  debugFlags.abTcp                    = FALSE;
  debugFlags.abCom                    = FALSE;
  debugFlags.readFun                  = FALSE;
  debugFlags.readVar                  = FALSE;
  debugFlags.recFun                   = FALSE;
  debugFlags.remVars                  = FALSE;
  debugFlags.remLink                  = FALSE;
  debugFlags.writeFun                 = FALSE;
  debugFlags.writeVar                 = FALSE;
  debugFlags.extLoop                  = FALSE;
  debugFlags.plcExd                   = FALSE;
  debugFlags.hoPcc                    = FALSE;
  debugFlags.hoLoop                   = FALSE;
  debugFlags.shoLoop                  = FALSE;
  debugFlags.bwhPcc                   = FALSE;
  debugFlags.gatePcc                  = FALSE;
  debugFlags.gatePccFirst             = FALSE;
  debugFlags.gatePccCalcGateTime      = FALSE;
  debugFlags.gatePccDeltaWeight       = FALSE;
  debugFlags.mixer                    = FALSE;
  debugFlags.procMon                  = FALSE;
  debugFlags.defaults                 = FALSE;
  debugFlags.fileRw                   = FALSE;
  debugFlags.alarm                    = FALSE;
  debugFlags.super                    = FALSE;
  debugFlags.vars                     = FALSE;
  debugFlags.print                    = FALSE;
  debugFlags.modbus                   = FALSE;
  debugFlags.modCom                   = FALSE;
  debugFlags.mbeNet                   = FALSE;
  debugFlags.remFun                   = FALSE;
  debugFlags.alarms                   = FALSE;
  debugFlags.fileRonly                = FALSE;
  debugFlags.diagnostics              = FALSE;
  debugFlags.extMan                   = FALSE;
  debugFlags.recipeRw                 = FALSE;
  debugFlags.recCom                   = FALSE;
  debugFlags.runExt                   = FALSE;
  debugFlags.recipe                   = FALSE;
  debugFlags.security                 = FALSE;
  debugFlags.setup                    = FALSE;
  debugFlags.totals                   = FALSE;
  debugFlags.liwPcc                   = FALSE;
  debugFlags.time_weight_to_csv       = FALSE;
  for (i=0; i<MAX_GATES; i++)
     debugFlags.printFeedTable[i]     = FALSE;
}

void enableAllDebug (void)
{
  int i;

  debugFlags.openGate.init_opengate   = TRUE;
  debugFlags.openGate.cyclic_opengate = TRUE;
  debugFlags.extDrive                 = TRUE;
  debugFlags.exdDrive                 = TRUE;
  debugFlags.relay                    = TRUE;
  debugFlags.timer                    = TRUE;
  debugFlags.hoDrive                  = TRUE;
  debugFlags.adScan                   = TRUE;
  debugFlags.calAd                    = TRUE;
  debugFlags.calBwh                   = TRUE;
  debugFlags.abTcp                    = TRUE;
  debugFlags.abCom                    = TRUE;
  debugFlags.readFun                  = TRUE;
  debugFlags.readVar                  = TRUE;
  debugFlags.recFun                   = TRUE;
  debugFlags.remVars                  = TRUE;
  debugFlags.remLink                  = TRUE;
  debugFlags.writeFun                 = TRUE;
  debugFlags.writeVar                 = TRUE;
  debugFlags.extLoop                  = TRUE;
  debugFlags.plcExd                   = TRUE;
  debugFlags.hoPcc                    = TRUE;
  debugFlags.hoLoop                   = TRUE;
  debugFlags.shoLoop                  = TRUE;
  debugFlags.bwhPcc                   = TRUE;
  debugFlags.gatePcc                  = TRUE;
  debugFlags.gatePccFirst             = TRUE;
  debugFlags.gatePccCalcGateTime      = TRUE;
  debugFlags.gatePccDeltaWeight       = TRUE;
  debugFlags.mixer                    = TRUE;
  debugFlags.procMon                  = TRUE;
  debugFlags.defaults                 = TRUE;
  debugFlags.fileRw                   = TRUE;
  debugFlags.alarm                    = TRUE;
  debugFlags.super                    = TRUE;
  debugFlags.vars                     = TRUE;
  debugFlags.print                    = TRUE;
  debugFlags.modbus                   = TRUE;
  debugFlags.modCom                   = TRUE;
  debugFlags.mbeNet                   = TRUE;
  debugFlags.remFun                   = TRUE;
  debugFlags.alarms                   = TRUE;
  debugFlags.fileRonly                = TRUE;
  debugFlags.diagnostics              = TRUE;
  debugFlags.extMan                   = TRUE;
  debugFlags.recipeRw                 = TRUE;
  debugFlags.recCom                   = TRUE;
  debugFlags.runExt                   = TRUE;
  debugFlags.recipe                   = TRUE;
  debugFlags.security                 = TRUE;
  debugFlags.setup                    = TRUE;
  debugFlags.totals                   = TRUE;
  debugFlags.liwPcc                   = TRUE;
  debugFlags.time_weight_to_csv       = TRUE;
  for (i=0; i<MAX_GATES; i++)
     debugFlags.printFeedTable[i]     = TRUE;
}

/************************************************************************/
/*
   Function:  EmptyHopper()

   Description:  This function empties the hopper afetr a dispense test.

                                                                        */
/************************************************************************/
unsigned int EmptyHopper()
{
   static int dumped = FALSE;
   int result;
#if 0
	/* setup timer for 1 second */
	TON_01.IN = 1; /* enable timer */
	TON_01.PT = 5000; /* 1 second preset time */
	/* start the timer */
	TON(&TON_01);
	/* open hopper door */
	IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = ON;

	ElapsedTime = TON_01.ET;
	if (ElapsedTime >= TON_01.PT)
	{
		IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
		/* reset the timer */
		TON_01.IN = 0;
		TON(&TON_01);
		return TRUE;
	}
#endif
   result = FALSE;
   if (!dumped)
   {
      IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = ON;
      shrBWH.wt_stable = FALSE;
      shrBWH.wt_stable_count = 0;
      dumped = TRUE;
   }
   else
   	switch (HMI.CurrentPage)
	   {
	   	case pg435_GateDelayTest:
   			if (shrBWH.wt_stable)
   			{
   				IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
   				shrBWH.wt_stable = FALSE;
   				shrBWH.wt_stable_count = 0;
   				dumped = FALSE;
   				result = TRUE;
   			}
			   break;

		   default:
   		   if (WaitingXMSecs(pg430_pause_ms) == TRUE)
   		   {
   		      if (shrBWH.wt_stable)
   		      {
   		         IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] = OFF;
   		         shrBWH.wt_stable = FALSE;
   		         shrBWH.wt_stable_count = 0;
   		         dumped = FALSE;
   		         result = TRUE;
   		      }
   		   }
   		   break;
      }
   return result;
}

/************************************************************************/
/*
   Function:  DelayXMSecs(unsigned int DelayMSecs)

   Description:  Generic millisecond delay routine.

                                                                        */
/************************************************************************/
unsigned int WaitingXMSecs(unsigned int DelayMSecs)
{
	/* setup timer for 1 second */
	TON_01.IN = 1; /* enable timer */
	TON_01.PT = DelayMSecs;
	/* start the timer */
	TON(&TON_01);

	ElapsedTime = TON_01.ET;
	if (ElapsedTime >= TON_01.PT)
	{
		/* reset the timer */
		TON_01.IN = 0;
		TON(&TON_01);
		return TRUE;
	}
	else 
      return FALSE;
}

/************************************************************************/
/*
   Function:  Diag_IORack()

   Description:  task for handling the pages relevant to the x20 io rack
                                                                        */
/************************************************************************/
void Diag_IORack()
{
   unsigned int ModuleID;

   switch (HMI.CurrentDevice)
   {
      case 0:  /* touch screen itself */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.SetupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, TRUE);
         ModuleID = PLC.TouchPanel.ModuleID;
         HMI.pg445_SerialNum = PLC.TouchPanel.SerialNum;
         HMI.pg445_BatteryVoltage = PLC.TouchPanel.BatteryStatus;
         HMI.pg445_CpuTemperature = PLC.TouchPanel.TemperatureCPU;
         HMI.pg445_EnvTemperature = PLC.TouchPanel.TemperatureENV;
         break;
      case 1:  /* X20 Bus Receiver Module */
         visible(&HMI.PopupRuntime1, TRUE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.BusRecvr.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.BusRecvr.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.BusRecvr.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.BusRecvr.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.BusRecvr.ModuleOK;
         HMI.pg445_SupplyCurrent = PLC.BusRecvr.SupplyCurrent*.1;
         HMI.pg445_SupplyVoltage = PLC.BusRecvr.SupplyVoltage*.1;
         HMI.pg445_Status1 = PLC.BusRecvr.Status1;
         HMI.pg445_Status2 = PLC.BusRecvr.Status2;
         break;
      case 2:  /* X20 AI1744 Strain Gauge Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.StrainGauge[0].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.StrainGauge[0].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.StrainGauge[0].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.StrainGauge[0].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.StrainGauge[0].ModuleOK;
         break;
      case 3:  /* X20 DI9371 12x Input Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigIn[0].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigIn[0].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigIn[0].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigIn[0].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigIn[0].ModuleOK;
         break;
      case 4:  /* X20 DO9322 12x Output Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigOut[0].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigOut[0].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigOut[0].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigOut[0].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigOut[0].ModuleOK;
         break;
      case 5: /* X20 DO9322 12x Output Module */ /* X20 CS1030 RS422/RS485 Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigOut[1].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigOut[1].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigOut[1].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigOut[1].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigOut[1].ModuleOK;
         break;
      case 6: /* X20 CS1030 RS422/RS485 Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.Comm422Module.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.Comm422Module.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.Comm422Module.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.Comm422Module.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.Comm422Module.ModuleOK;
         break;
      case 7:  /* X20 DI9371 12x Input Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1 ,TRUE);
         ModuleID = PLC.DigIn[1].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigIn[1].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigIn[1].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigIn[1].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigIn[1].ModuleOK;
         break;
      case 8:  /* X20 DO9322 12x Output Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigOut[2].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigOut[2].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigOut[2].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigOut[2].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigOut[2].ModuleOK;
         break;
      case 9:  /* X20 DO9322 12x Output Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigOut[3].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigOut[3].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigOut[3].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigOut[3].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigOut[3].ModuleOK;
         break;
      case 10:  /* X20 AI1744 Strain Gauge Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.StrainGauge[1].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.StrainGauge[1].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.StrainGauge[1].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.StrainGauge[1].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.StrainGauge[1].ModuleOK;
         break;
      case 11:  /* X20 AO2632 Strain Gauge Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.AnaOut.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.AnaOut.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.AnaOut.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.AnaOut.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.AnaOut.ModuleOK;
         break;
      case 12:  /* X20 DI9371 12x Input Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.DigIn[2].Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.DigIn[2].Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.DigIn[2].Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.DigIn[2].Info.SerialNum;
         HMI.pg445_Module_Status = PLC.DigIn[2].ModuleOK;
         break;
      case 13:  /* X20 D04529 Dry Contact Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.RelayOut.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.RelayOut.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.RelayOut.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.RelayOut.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.RelayOut.ModuleOK;
         break;
      case 14:  /* X20 DI2377 2x Digita Input Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.PulseCounter.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.PulseCounter.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.PulseCounter.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.PulseCounter.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.PulseCounter.ModuleOK;
         break;
      case 15:  /* X20 AI2632 Strain Gauge Module */
         visible(&HMI.PopupRuntime1, FALSE);
         visible(&HMI.PopupRuntime2, FALSE);
         visible(&HMI.SetupRuntime1, TRUE);
         ModuleID = PLC.AnaIn.Info.ModuleID;
         HMI.pg445_HW_Vers = PLC.AnaIn.Info.HardwareVariant;
         HMI.pg445_FW_Vers = PLC.AnaIn.Info.FirmwareVersion;
         HMI.pg445_SerialNum = PLC.AnaIn.Info.SerialNum;
         HMI.pg445_Module_Status = PLC.AnaIn.ModuleOK;
         break;
	   case 16:  /* X20 DI9371 12x Digital Input Module */
	     visible(&HMI.PopupRuntime1, FALSE);
		  visible(&HMI.PopupRuntime2, FALSE);
		  visible(&HMI.SetupRuntime1, TRUE);
		  ModuleID = PLC.DigIn[3].Info.ModuleID;
		  HMI.pg445_HW_Vers = PLC.DigIn[3].Info.HardwareVariant;
		  HMI.pg445_FW_Vers = PLC.DigIn[3].Info.FirmwareVersion;
		  HMI.pg445_SerialNum = PLC.DigIn[3].Info.SerialNum;
		  HMI.pg445_Module_Status = PLC.DigIn[3].ModuleOK;
		  break;	
	   case 17:  /* X20 DO9322 12x Output Module */
		  visible(&HMI.PopupRuntime1, FALSE);
		  visible(&HMI.PopupRuntime2, FALSE);
		  visible(&HMI.SetupRuntime1, TRUE);
		  ModuleID = PLC.DigOut[4].Info.ModuleID;
		  HMI.pg445_HW_Vers = PLC.DigOut[4].Info.HardwareVariant;
		  HMI.pg445_FW_Vers = PLC.DigOut[4].Info.FirmwareVersion;
		  HMI.pg445_SerialNum = PLC.DigOut[4].Info.SerialNum;
		  HMI.pg445_Module_Status = PLC.DigOut[4].ModuleOK;
		  break;
      default:
          visible(&HMI.PopupRuntime1, FALSE);
          visible(&HMI.SetupRuntime1, FALSE);
          visible(&HMI.PopupRuntime2, FALSE);
          break;
   }

   switch (ModuleID)
   {
      case ModID_BR9300:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20BR9300");
         break;
      case ModID_4PP045:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"4PP045.0571-062");
         break;
	   // added DRT 01/12/16 Support for pp65
	   case ModID_4PP065:
	      brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"4PP065.0571-K61");
		   break;	
      case ModID_4PPC70:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"4PPC70.101G-23B");
		   break;
	   case 0:
		   brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"N/A");
		   break;			
      case ModID_4PP045PCC:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"4PP045.0571-K15");
         break;
      case ModID_CS1030:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20CS1030");
         break;
      case ModID_DO9321:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20DO9321");
         break;
      case ModID_D04529:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20DO4529");
         break;
      case ModID_DO9322:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20DO9322");
         break;
      case ModID_DI9371:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20DI9371");
         break;
      case ModID_AI1744:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20AI1744");
         break;
      case ModID_AI1744_3:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20AI1744-3");
         break;
      case ModID_AO2632:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20AO2632");
         break;
      case ModID_AI2632:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20AI2632");
         break;
      case ModID_DI2377:
         brsstrcpy((UDINT)HMI.pg445_ModuleID, (UDINT)"X20DI2377");
         break;
      default:
		   sprintf((char *)HMI.pg445_ModuleID, "? (%d)?? ", ModuleID);
         break;
   }
}
/************************************************************************/
/*
   Function:  output_debug_data()

   Description:
                                                                        */
/************************************************************************/
void output_debug_data(void)
{
   int i;
   int mask;
   unsigned int dbg_who;
   unsigned int dbg_what;

   dbg_who = shr_global.dbg_who;
   dbg_what = shr_global.dbg_what;

   if (OutputDebugInit == 0) /* This should be done 1 time to set up HMI */
   {
      for (i=0; i<32; i++ )
      {
         mask = 1<<i;
         if (shr_global.dbg_who & mask)
            HMI.pg425_DiagOutputWho[i] = 1;
         else
            HMI.pg425_DiagOutputWho[i] = 0;

         if (shr_global.dbg_what & mask)
            HMI.pg425_DiagOutputWhat[i] = 1;
         else
            HMI.pg425_DiagOutputWhat[i] = 0;
      }
      OutputDebugInit = 1;
   }

   for (i=0; i<32; i++ )
   {
      mask = 1<<i;
      if (HMI.pg425_DiagOutputWho[i] == 1)
         dbg_who |= mask;
      else
         dbg_who &= ~mask;

      if (HMI.pg425_DiagOutputWhat[i] == 1)
         dbg_what |= mask;
      else
         dbg_what &= ~mask;
   }
   shr_global.dbg_what = dbg_what;
   shr_global.dbg_who = dbg_who;
}
/************************************************************************/
/*
   Function:  clear_gate_calibration()

   Description:
                                                                        */
/************************************************************************/
void clear_gate_calibration(void)
{
   int i;

   for (i=0; i<MAX_GATES; i++)
   {
      shrGATE[i].clear_gate_cal = TRUE;
   }
}
/************************************************************************/
/*
   Function:  clear_cl_diagnostics()

   Description:
                                                                        */
/************************************************************************/
void clear_cl_diagnostics(void)
{
    /*cs->diagnostics_start = start_timer();*/
    cfgEXT.loads = 0;
    cfgEXT.coasts = 0;
    cfgEXT.avg_dump_size = 0.0;
    cfgEXT.accum_coast = 0.0;
    cfgEXT.accum_grav = 0.0;
    cfgGFLUFF.loads = 0;
    cfgGFLUFF.coasts = 0;
    cfgGFLUFF.avg_dump_size = 0.0;
    cfgGFLUFF.accum_coast = 0.0;
    cfgGFLUFF.accum_grav = 0.0;
    cfgGFEEDER.loads = 0;
    cfgGFEEDER.coasts = 0;
    cfgGFEEDER.avg_dump_size = 0.0;
    cfgGFEEDER.accum_coast = 0.0;
    cfgGFEEDER.accum_grav = 0.0;
    cfgHO.coasts = 0;
    cfgSHO.coasts = 0;
}
/************************************************************************/
/*
   Function:  clear_net_diagnostics()

   Description:
                                                                        */
/************************************************************************/
void clear_net_diagnostics(void)
{
   int i;

   for (i=0; i<MAX_ADDR; i++)
   {
      shr_global.stats[i].good_msgs = 0;
      shr_global.stats[i].timeouts  = 0;
   }

   shrEXT.cal_counter = 0;
   shrGFLUFF.cal_counter = 0;
   shrGFEEDER.cal_counter = 0;

   shrBWH.last_cal_reason = ' ';
   shrBWH.cal_counter = 0;
   shrBWH.queue_overflows = 0;
   shrBWH.queue_underflows = 0;

   shrWTH.good_msgs_in = 0;
   shrWTH.good_msgs_out = 0;
   shrWTH.timeouts = 0;
}



