/************************************************************************/
/*
   File:  version.h

   Description: Maintains the currently displayed version numbers. The
      file comments reflect changes that occurred from version to version.

      $Log:   F:\Software\B&R_Guardian\version.h_v  $
 *
 *    Rev 1.0   Jan 11 2008 13:34:58   FMK
 * initial version
 *     Process Control Corp,   Atlanta, GA
 **
WX7.7.5  06/05/18
         removing dNum from code (old OS9)  using #define 
         replacing gCurrentSpeed w/ gCurrentSpeed[3],  Ext=0, Hauloff=1, Winder=2
         replacing gFinalSpeed w/ gFinalSpeed[3],  Ext=0, Hauloff=1, Winder=2
         replacing 32767 with OVF_INT
WX7.7.4  05/17/18
         G2-704 Clean up pg990 c code.
         G2-703 Make all text same size on Startup 
WX7.7.3  05/15/2018
         Demo for Extruder, Haul-Off & Winder Winder stretch factor
         Test version for Warning updates
WX7.7.2  05/08/18
         All new filters are active for testing
WX7.7.1  04/30/18
         Filter Changes
WX7.7.0  04/26/18
         Fix Alarm processing func. to use correct visu handle
WX7.6.1  04/25/18
         Filter changes in drop down box.
WX7.6.0  04/24/18 C70 Initial release
         vis_fo reworked
WX7.5.0  04/24/18 C70 Initial release
         Main page Monitor/Recipe button does a straight change page now
WX7.4.0  04/23/18 C70 Initial release
         G2-692 fix for C70 to startup with blank retain vars.
WX7.3.0  04/03/18
         G2-691 Next button on recpie page shows when not needed.
WX7.2.2  04/03/18 
         G2-689 Dust Coll. not working in poper Seuuence.
WX7.2.1  03/29/18 Testing
         Fix for AB comm to update mix time.
WX7.2.0  03/26/18 Release
WX7.1.5  Release Testing 3/19/18
WX7.1.4  Pre release testing
WX7.1.3  03/13/18 G2-651
WX7.1.2  testing
WX7.1.1  03/06/18
         G2-675 Page 033, When Bypass mixer prox. sensor is enable to yes. The delay closing gate can not be edited.
         G2-673 Page 330, Usage rate does not show up on Modbus correctly. 
WX7.1.0  03/05/18 Release
WX7.0.5  03/02/18
WX7.0.4  03/01/18
         bug fix for density in recipe over comm's
WX7.0.3  03/01/18
         Setting Recipe over comm's bug fix.
WX7.0.2  02/27/18
         project for PP65 & C70
WX7.0.1  02/23/18
         fix config. errors.
			G2-668 Don't prevent extrusion or blender from going to AUTO if their respective portion in new 
					 recipe is validated. Invalidated recipe parts should not stop extrusion from going into 
					 AUTO, etc..
			G2-669 Stopping extrusion through Comms shouldn't also stop blender.
WX7.0.0  02/22/18
         pp65 & c70 config.
WX6.38.2 02/20/18
         Cleanup to remove warnings from SW build
         G2-666 Extrusion & HO hand control improvements
         Remove obsolete code (AB Drive)
         Add code to prepare for support of C70 HW
WX6.38.1 02/15/18
         G2-665 Do not have pop when going to auto from Comms.
         Extrusion, Recipie code cleanup.
WX6.38.0 02/14/18 release 
WX6.37.4 02/09/18
         G2-661 set loading for all hoppers in recipe
         G2-662 changes for Armacell.
			G2-663 Extrusion auto control
			G2-664 Redo unit scaling
         G2-542 change text on pg 018
			G2-646 Set speed not working correctly on Modbus.
			G2-649 Extrusion manual control.
WX6.37.3 01/25/18
         G2-646 Set speed not working correctly on Modbus.(start)
WX6.37.2 01/24/18
         G2-639 After reboot alarms are active
         G2-641 Disable manual button when in monitor mode
WX6.37.1 12/19/17 
         G2-638 Ext. Alarm Defaults
WX6.37.0 12/19/17 Release
WX6.36.4 11/30/17 Release test
WX6.36.2 12/13/17
         G2-614 EA_UNIT not set in defaults
         G2-612 ProfiBus not writing UCB #
         G2-613 Added OPC-UA framework for testing
WX6.36.1 12/4/17
         code for autobatch testing
         G2-610 Fixed comms for Totals (inv. and shift)
         G2-611 Totals page clear popup has wrong text in 64 bit mode.
WX6.36.0 11/30/17 Release
WX6.35.1 11/30/17 internal testing
         G2-608 Editing A's Density doesnt trigger the right action
         G2-599 Autobatch starts first batch without mixing mixer gate is open, and should stay closed durring mix time.
WX6.35.0 11/27/17 Release
WX6.34.2 11/22/17 internal testing
         G2-600 cfg_super.Flood_Feed_Timer was not initialized
         G2-588 stop after manuale will go to pause.
         G2-603 density in recipe if set, for any blender mode.  
WX6.34.1 11/21/17 internal testing
         G2-598 G2 EXTRUSION control, excessive coast alarm is excessive
         G2-597 If Alarm is configured as NONE for PAUSED, it would not go OLD.
         G2-593 extruder shutdow should not shut down blender.
WX6.34.0 release 11/17/17
WX6.33.1b 11/16/17 internal testing
         Coast.  Coming out of coast shortened.
         G2-596 save flood feed during a reset.      
WX6.33.1 11/15/17 internal testing
         G2-590 blank out loading setup when loading disabled
WX6.33.0 release 11/15/17
WX6.32.1 11/14/2017 internal testing
         Thk not calculating correctly
WX6.32.0 release 11/13/17
WX6.31.2 10/30/17 internal testing
         G2-589 Density in recipe allow .001 for low limit
         G2-581 Haul-Off updates for Visual
         G2-451 G3 compatible mode in wizzard. (revisited)
         G2-571 Allow auger to fill up in use auger mode befoe OOM alarm
         G2-581 Haul-off not calculating pulses for LTP ft/min.
WX6.31.1 10/11/17 internal testing
         G2-575 BWH unstable WT Alarm fixed. if unstable for 10 Sec. or more, Set alarm.
         G2-559 IP address cut off on Main pg
         G2-560 pg 405 align row,col of display boxes
         G2-558 HMI.pg990_HopperDoorSwitch not saved
WX6.31.0 10/09/17 Release
WX6.30.3 10/06/17 Internal test for WX6.31.0 release
         G2-539 Extrusion/Mixer bugs
         G2-524 German language Translations. end
WX6.30.2 9/18/17 internal testing
         G2-517 Internal testing filters
WX6.30.1 9/28/17 internal testing
         G2-541 G3 Hopper sw
         G2-524 German translation work with Holger. start
WX6.30.0 9/15/17 Release
         G2-517 add filters
         G2-513 BWH delay 
WX6.29.0 9/6/17 Release
         G2-513 adloop factor
         G2-518 Make pg430 WT add 1 extra decimal point
         G2-514 extrusion limit changes on HMI pages
WX6.28.0 8/24/17 Release
WX6.27.2 8/22/17 internal testing
         G2-500 - Version testing on restore
WX6.27.1 8/21/17 internal testing
         G2-500 - Version testing on restore
WX6.27.0 8/18/17 Release 
         G2-511 - Totals not updating
         G2-465 - Self Cleaning Lockup was fixed, now make it work.
WX6.26.1 8/16/17 internal test release 
WX6.26.0 8/16/17 Release 
         G2-401 - Textrude remote self loading (display arrows from inputs even if disabled)
WX6.25.0 8/11/17 Release
         G2-443 - Reset IP address after RESTORE (Binary mode)
WX6.24.0 8/4/17 Release
         G2-498 - Remove Metric Conv. in Comm's we are just lb only.
         G2-463 - Final corrections for Chinees from Germany
         G2-464 - skipping gate 1st when 6 element & dust col. on
         G2-456 - OS9 to pp65 Conversion of Manual Run switch
         G2-465 - pg 005 manual self cleaning button results in lockup also pg 201 needs to remove option not offered on G2
         G2-442 - pg 330 status square colors
         G2-401 - Loading Page small correction due to testing with real remote CTL pannel 
WX6.23.5 Internal release
         G2-3?? - Binary System Backup/Restore to compact flash
WX6.23.4 Internal release
         G2-442 - pg 330 status square colors
         G2-443 - ip address on save/restore not updating ?
WX6.23.0 Release 7/11/17
         G2-425 - Chinees font mapping
         G2-427 - modbus status var correction.
WX6.22.0 Release 7/6/17
         G2-418 - recovery from OOM alarm
   `     TCP/IP - updates mbenet recovery code
         G2-415 - DUMP_LOW alarm text need to be updated
         G2-410 - Clib. page lang correction
         G2-406 - Make pg 210 on G2 like C2
         G2-405 - bwh hngs on -wt no alarm
         G2-404 - Motor trip not shuddown  
         G2-401 - Loading Page
         G2-400 - pg 405 Tolerance 3 places not 2
         G2-399 - Update BWH default weights
         G2-394 - metric conversion for for MAX WTP 436001 MB reg.
WX6.21.0 Release 
         G2-349 - move bwh delay from diag to setup
         G2-364 - remove old opc tags & code from cpu (sw test)
         G2-360 - brsstrcpy(x, y) needs to be brsstrcpy((UDINT)x, (UDINT)y) (sw test)
         G2-362 - add file name edit to xml out
         G2-227 - USB now supportd in 4.x release AR runtime
         G2-348 - Saftey fix for mixer motor when interlock is out.
         G2-347 - Metric conv. in comms.
         G2-337 - Manulal mode not cleaing in comms when enter run mode.
         G2-343 - Selected lang. not preserved when a reboot is done.
         G2-334 - alarm descriptions do not match alarm and are confusing
WX6.20.0 Release
         Fix to allow system save/restore use J4.10 Runtime
WX6.19.0 Release
         G2-284 - low prox alarm corrections
WX6.18.1 
         G2-276 - Extruder monitor mode, Moving average not updating. 
WX6.18.0 release
WX6.17.4
         G2-268
         G2-267
         G2-258
         G2-257
         G2-254
         G2-253
         G2-251
         G2-225
         G2-224
WX6.17.1 
         G2-250 - Modbus 32/16, Allen Bradley 16/32 bits and UCB's.. Removing old hack.
         G2-243 - cleanup VAR retain 
         G2-242 - Allen Bradley, Alarm Bits N100:99 (A) reading is not the same when using 303 UCB (1,1) N500:00
WX6.17.0 release
*
WX6.16.5 G2-224 - RUN Ooutput Enable on pg201 (Moved digital out ADDR 4 (14 & 24) to addr08 (13 & 23)
*
WX6.16.4 G2-225 - Aux inputs not working Code and page 201 fixed
*
WX6.16.3 internal Release for recipe max at 400 Date 010417
WX6.16.0 
WX6.15.2 
1. Added buttons to page 214 to set default UCBs and clear UCBs
WX6.15.1 
1. Added buttons to diagnostics page 406 to clear # of gate fires
WX6.15.0 
1. BWH delay (delay gate to allow hopper to close before filling)
WX6.14.0 
1. UCBs defnitions can now be changed over remote comms 
2. Added register to read last batch weight
WX6.13.0 Release Version
1. fix edit ranges on aux_alarm_time var
2. fix logic on "MIXER_MOTOR_ALARM" alarm
WX6.12.0 Release version
1. Fix for mixer motor not turning off when using ext prox
2.
WX6.11.0 test version 3/28/16 drt
1. fixed flood feed bug drt
2. added OPC communication code drt
3. fixed integer types on profibus drt
4. fixed flood feed timming drt
WX6.8.0 ****** Customer Release *******************************************************
WX6.7.2 ****** Customer Release *******************************************************
1. Fixed bug related with saving the recipe.(G2)
2. Fixed bug related with Powder Receiver selection on pg.15(G2)
WX6.7.1 ****** Internal Release *******************************************************
1. Added Profibus input/output flags
WX6.7.0 ****** Customer Release *******************************************************
1. Monitor Extruder page for has info about WTP; Delta bits and Update Time. Actual WTP is a part of the moving average function now.  
WX6.6.0 ****** Customer Release *******************************************************
1. Improved recipe visualization new feature.
WX6.5.6 ****** Customer Release *******************************************************
1. Added new feature editable  Input Buffer Length and Output Buffer Lentgh for Profibus communication.(G2)
WX6.5.5 ****** Customer Release *******************************************************
1. Added Blender Calibration check for Profibus  communication.(G2)
2. BWH calibration time.(new feature)
WX6.5.1 ****** Customer Release *******************************************************
1. Added Blender Calibration bit for Profibus  communication.(G2)
2. Fixed bug related with current mode status bit when use Profibus communication.(G2)  
WX6.5.0 ****** Customer Release *******************************************************
WX6.4.2 ****** Internal Release **************************************************************
1. Fixed bug related with comms. when read/write ingredient names. (G2) 
WX6.4.1 ****** Customer Release Germany*******************************************************
1. Added the "stop after the end of batch" option with profibus.
2. Change the cyclic time for Profibus. 
WX6.4.0 ****** Customer Release *******************************************************
1. First dispense target is based on input batch size. Once it is dispensed and measured, the total batch size is recalculated; and the new set target for the rest of the gates is established. (G2)
2. Improved algorithm for gate dispense accuracy when target accuracy less than 0.05% on pg.18. (G2)
3. Fixed bug related with comms. Profibus(G2) 
4. Fixed bug related with comms. when change recipe remotely.(G2)
5. Auxiliary Alarm bug fixed. 
6. Auxiliary Alarm Time and MIx Time are now in synchronization.
7. Self Cleaning Relay bug fixed. Added Self Clean function on Manual page to keep the security rules in place.  
WX6.2.0 ****** Customer Release *******************************************************
1. Fixed bug related with Security Setup option.
2. Fixed bugs related with Recipe Number when use communication.
WX6.1.0 ****** Customer Release *******************************************************
1. Fixed bug that display the correct recipe name and number on main page and monitor page.(G2 & Autobatch) 
2. Fixed bug related with random "Mix Valve Failure" Alarms.(G2)
3. Fixed bug related with wizard page(pg.990) (G2 & Autobatch) 
4. Added a new option for Autobatch blenders on Mixer Control Loop Setup page, �Mixer Gate Mode Leave Open�: 
Yes, will keep the mixer gate open after dumping if External Prox switch is covered. No, this is the default option, and will close the mixer gate after dumping.
5. Change settings value for: Mix Time, Delay Closing Gate By, Dump Cycle Time and Low Level Time while the blender is running.
6. Fixed bug related with the corruption of one point in the moving average function which sometimes display strange value.(G2 & Autobatch) 
7. Added a new option to restart and save from main page when blender security is service.(G2 & Autobatch) 
WX2.27.0 ****** Customer Release *******************************************************
1. Allen Bradley Communications updates for PCC and PCC2 registers (G2)
2. Warning "Safety Interlock is Open" for Autobatch was removed. (Autobatch) 
3. Fixed "BWH calibration" restore on power up to correct display. (G2)
WX2.26.0 ****** Customer Release *******************************************************
1. I/O Configuration Output status information value & I/O mapping problems. (G2)
WX2.25.0 ****** Customer Release *******************************************************
1. Ability for customers to adjust saved recipes as they are running the blender. (G2)
2. If Regrind hopper was out of material, it would dump what little bit dosed into the mixing chamber without the remaining ingredients. (G2)
3. The LICENSE KEY set up has a simplified configuration, only last box of activation keys is needed. (G2 & Autobatch)
WX2.24.0 ****** Customer Release *******************************************************
WX2.23.2 ****** Internal Release *********************************************************
Operators in operator mode need to be allowed change/modify the recipe based on the Security Setup setting from page 185.
WX2.23.1 ****** Internal Release *********************************************************
Option to clear shift totals at the beginning of each job.
Fixed Recipe Total when using multiple recipes. 
WX2.23.0 ****** Customer Release *******************************************************
Single Gravitrol hopper bug fix.
WX2.22.0 ****** Customer Release *******************************************************
Single Gravitrol hopper option has been added.
WX2.21.1 ****** Internal Release *******************************************************
Default �Delay Closing Gate�  for Guardian blenders needs to be 0 sec.
There is no need to allow time for the mixer gate to stay open after they dump the batch like Autobatch blenders. 
If �Material Request = External Input�  the Mixer Prox Sensor does not need to be bypass, it is used as a backup sensor 
in case the mixer gets fill up above the limit.
B&R 001-161 Added print page button to support printing totals (from page 320) to a log file on USB.
Conversion from metric to imperial bug fixes 
Added new feature on diagnostic pg 411,a  self correcting mechanism for the time weight table.
It can be enable/disable on pg 179.  
Added new feature to accommodate the new 2.5 Kg Gravitol Blender, G3.
On pg. 411Added a new feature to display the last expected weight to diagnostic the blender while running.
Tim Payton request: the operator can change only the ingredient percentages of the recipe, but not the gate order.  
WX2.21.0 ****** Customer Release *******************************************************
Communication bug fixes related with reading one register for the extruder rate.
On pg. 32 When "Material Request Invert  Yes" (for customers using KBL type prox instead of KAL type prox), the blender was not working properly.
When "Material Request" is set to "External Input", on "Bypass Mixer Prox.Sensor" should be set for "NO" by default
On pg. 16 If disable gate loading on a particular gate when hopper is actually loading the receiver valve needs to close in order to maintain the proper vacuum inside the system. 
On pg. 22 added BWH jiggling hopper enable/disable setup option, default is disable.
WX2.20.0 ****** Customer Release ******************************************************
On pg 211 Serial Port page added Node Address input for system setup 
On pg 005 Manual Backup Blender page On/Off Button has a new  feature  
WX2.19.1 ****** Internal Release *******************************************************
If blender in Simulation display the word "Demo" on the Recipe, Monitor, Totals, Setup, Alarms, Security and Language pages. 
B&R 001-106 pg 205 - Job Weight in Recipe - When set to yes the Job shows on recipe page 453. 
If you change this value on 453 and goto main and back it's not saved.
B&R 001-107 pg 207 - pg 207 - Mix Time in Recipe - When set to yes the Mix Time shows on recipe page 453.
 If you change this value on 453 and goto main and back it's not saved.
B&R 001-107 pg.103 - Pump Restart Time change to max 30s, default 10s/per Germany
WX2.19.0 ****** Customer Release ******************************************************
System: Blender in Manual Backup alarm replaced by BWH: Hopper in Manual Backup alarm 
WX2.18.1 ****** Internal Release *******************************************************
B&R 001-33 Super password Timeout 
If Blender in operator mode disable Clear ALL from the Alarm page 
WX2.18.0 ****** Customer Release ******************************************************
Added code to be able to select hopper switch option from initial setup
B&R 001-85	Added code to support hopper lockout from a physical keyswitch
B&R 001-81 pg 335 - When using hopper D as regrind hopper, hopper weights on monitor page do not add up to 40lb batch size
WX2.17.0 ****** Customer Release *******************************************************
Added hopper switch failure feature to 1Kg Guardian2 blender
WX2.16.6 ****** Internal Release *******************************************************
Tensar - 3 prox system UPDATES.. enough ALREADY!!!!
WX2.16.5 ****** Internal Release *******************************************************
Tensar - 3 prox system UPDATES
WX2.16.4 ****** Internal Release *******************************************************
Tensar - 3 prox system UPDATES
WX2.16.3 ****** Internal Release *******************************************************
Tensar - 3 prox system changed low prox to adjust every batch and using HIGH HIGH step		
WX2.16.2 ****** Internal Release *******************************************************
B&R 001-64 - pg.453 add a popup window reminding operators to press Auto button once they modify recipe and press use recipe button
B&R 001-61 - pg.445 The "Status" field shows Good or Bad. This is incorrect. Status means is module present or not present 
             there or not there. Change "Status" to read "Module In Rack" and field to show "Yes" or "No
B&R 001-56 - pg.14 Default "Hopper Out of Spec" to 0.5% for 1 and 1.25 Kg, to 1.0% for all other sizes.
B&R 001-50 - pg.33 When Blender size 1Kg Delay Closing Gate needs to be adjusted to 3 ms instead of 5 ms.
B&R 001-49 - pg.21 When Blender size 1Kg Batch Size maximum value set up to 2.5lb
B&R 001-48 - pg.33 When Blender size 1Kg Bypass Mixer Prox.Setup default to YES
B&R 001-47 - pg.32 When Blender size 1Kg Material Request default to External Input
B&R 001-6 - pg.213  Clear out all fields ( Make Blank)
B&R 001-8 - pg.102  Hide "Continuous Run Time" edit field if "Continuous Run Option" is NO
WX2.16.1 ****** Internal Release *******************************************************
B&R 001-3 -   pg.338 - "Prev. Section" button is not working right, goes to Diagnostics Section
B&R 001-42 - pg 454 UseRecipe Button needs to save the recipe and return to Main Menu
B&R 001- 41- pg 454 Ext&HO Remove Save Recipe button, save when user changes values.  
B&R 001-37 - pg 12 Missing "button loading setup"
B&R 001-36 - pg 205 Ext&HO Can not change Densities in Receipe to no (always yes)
B&R 001-35 - pg 454 Ext&HO set field not wide enough to display anything > 1000.0
B&R 001-34 - pg 21 Maximum Coast Time Input range is 1.00 to 34.30 but can not see it to 34.30
B&R 001-33 - pg 190 Super password Timeout not display correctly
B&R 001-32 - pg 185, 186,187, 188, 190 Remove observer from drop down boxes on these pages
B&R 001-31 - pg17 The type drop down box needs to be wider, text is cut off 
WX2.16.0 ****** Customer Release *******************************************************
WX2.15.4 ****** Internal Release *******************************************************
B&R 001-13 - Fix Self Cleaning Option
WX2.15.3 ****** Internal Release *******************************************************
B&R 001-15 - pg300 - Hide serial port settings if non-serial port device selected
B&R 001-16 - Regrind goes "Out Of Material", blender does not continue running with other hoppers.
B&R 001-17 - pg408 - Add 3 PROX to Regrind Hopper . Main -> Setup -> 3 Proxy Regrind Setup
WX2.15.2 ****** Internal Release *******************************************************
Kundig Driver
3 prox hopper
WX2.15.1 ****** Internal Release *******************************************************
Leon Work...
WX2.15.0 ****** Customer Release *******************************************************
WX2.14.5 ****** Internal Release *******************************************************
B&R001-183	When REGRIND Hopper goes OutOfMaterial, we should set this hopper to "0.0" 
            and recalulate batch size and continue.
WX2.14.4 ****** Internal Release *******************************************************
B&R001-186	Going into AUTO when recipe not valid
B&R001-185	Grey out the settings on pg.32, 33, when blensder is on AUTO.
B&R001-181	When external prox enabled. and mixer prox bypassed, mixer gate never
            opens to dump batch
B&R001-179	pg 202 - Self Cleaning. Press PURGE when in auto, locked up screen
B&R001-178	Recipe Pg - Change title bar to show Recipe Edit % or Recipe Edit Parts
B&R001-176	Recipe Pg - Using parts, run A=1,B=1,C=1 and then run A=0,B=1,C=0, see notes
B&R001-175	Recipe Pg - Gate Order not always correct, see notes
B&R001-174	Pg 2 - There are two pages two's, press Next Page button
B&R001-173	Recipe Edit - When more than 4 hoppers and gate order enabled, 
            you cannot change gate order for hoppers 5 and up.
B&R001-172	Add 1kg blender size to wizard and pg 175 Blender size drop-down.
B&R001-170	Jumping to icon display
B&R001-169	Pg 207 - Gate Order in Recipe does not work when the recipe is running
B&R001-168	Pg 207 - Gate Order in Recipe starts at zero on recipe page, 
            confusing operators
B&R001-137	Pg 345 - When Using 12 Hoppers, The Button "Next Hopper" Not Working For 
            Gates I to L
B&R001-127	Pg 205 - Set everything to No except Resin Codes Available. On recipe page, 
            everything shown
B&R001-96	Add a weight threshold, dont start a new batch if there is material in BWH
B&R001-95	Alarm Beacon Flashing When No Alarms Active
B&R001-64	After Out of Material shutdown, going back into auto overfills weigh hopper
WX2.14.3 ****** Customer Release *******************************************************

WX2.14.2 ****** Customer Release *******************************************************
B&R001-173	Recipe Edit - When more than 4 hoppers and gate order enabled, 
            you cannot change gate order for hoppers 5 and up.
B&R001-140	Random False "Mix Valve Failure" Alarms
B&R001-95	Alarm Beacon Flashing When No Alarms Active
B&R001-78	Ralph Rone - remove air to bwh during batch, should alarm and stop.
B&R001-64	After Out of Material shutdown, going back into auto overfills weigh hopper
B&R001-36	Ralph Rone , set alarm to shutdown in Automatic if hoppers out of material.
WX2.14.0 ****** Customer Release *******************************************************
B&R001-115	Pg 195 - When weight unit set to oz, weight now on pg 330 shows lbs, and vice versa
B&R001-170	Jumping to icon display
B&R001-169	Pg 207 - Gate Order in Recipe does not work when the recipe is running
B&R001-161	Recipe Page - On reboot getting fields appearing that shouldn't
B&R001-160	Recipe Page - Job Weight and Mix Time showing even though they are not selected
B&R001-159	Recipe Page - Remove Next Hopper button if using 4 or less hoppers
B&R001-115	** CRITICAL ** Pg 195 - When weight unit set to oz, weight now on pg 330 shows lbs, and vice versa
B&R001-156	pg 33 - Hide Prev/Next Section buttons, there is no prev/next sections
B&R001-152	pg 17 - when setup as autobatch prev/next not working
B&R001-150	The alarm beacon flashes when in auto and no alarms present, clears when you goto alm pg
B&R001-148	When on recipe page and at operator security level, gray out receipe values for recipes 2 and up
B&R001-143	Control of displaying Densities on Recipe Page from pg 205 not correct
B&R001-141	Pg 451 - Remove text from this page so it does not appear to flash
B&R001-136	Pg 33 - Prev and Next Section Buttons Do Not Work
WX2.13.0 ****** Customer Release *******************************************************
B&R001-135	Pg 32 - Changing MIxer Motor Mode settings stops blender from going into AUTO
B&R001-133	Pg 19 - Remove Step Size for Z-Crossing and hard code this as 5ms
B&R001-130	Pg 19 - Not Accesible from pg 18, only from pg 17 using previous page button
B&R001-129	Pg 17 - Change text on algorithm drop down box to Attempt Single shot and Attempt Dual Shot
B&R001-126	Pg 012 - Remove network setup and all sub items from hoppers. No longer used.
B&R001-121	Pg 451 - Delete, doesn't do anything or have any controls on it.
B&R001-114	page 430, Prepulse not saved when you leave the page
B&R001-113	Pg 175 - What is Frame Size used for? Should we delete this? Seems redundant.
B&R001-112	Recipe Edit Page - Using 8 hoppers, next hopper button requires multiple key press.
B&R001-111	Pg 417 - The Prev/Next button should be removed. There is no next page.
B&R001-110	Interlock not working correctly if not on main page
B&R001-108	Page 32 - Material Request Not Being Saved When Changed And Then Reboot
B&R001-94	Page 320 - When security is operator level, they are allowed to clear totals but should not
B&R001-92	Output Debug Data - Remove Enable All and Disable All buttons
B&R001-89	Page 399 - There are buttons that are not needed or don't seem to do anything
B&R001-88	Page 213 - Change page description to SMTP/Email as shown on page 212
B&R001-87	Page 425 - Press Enable All causes unit to reboot in service mode
B&R001-83	Page 32 - Set Mixer Motor Mode to No Mixer, reboot and press AUTO but it does not run
B&R001-82	Can we remove the Invalid Touchscreen Message on boot?
B&R001-81	Page 32 - Mixer Motor Mode setting, when set to No Mixer, gets stuck there when in AUTO
B&R001-78	remove air to bwh during batch, should alarm and stop.
B&R001-76	Security levels not correct on items on 5 pages
B&R001-71	Page 181 - Select Device: Mixer -> Select Alarm: Material Reequest Uncovered Alarm Spelling
B&R001-68	UWeight unit set to Oz but on pag.21 batch size is in lbs
B&R001-64	After Out of Material shutdown, going back into auto overfills weigh hopper
B&R001-63	Monitor Page Showing Oz for Weight now/avg when system set to Lbs
B&R001-48	Why Is Setting Grayed Out?
B&R001-46	Low Battery Alarm Not Working
B&R001-36	set alarm to shutdown in Automatic if hoppers out of material.
WX2.12.3 ****** Customer Beta Release *******************************************************
Bug B&R001-110 : Interlock not working correctly if not on main page
WX2.12.2 ****** Customer Beta Release *******************************************************
B&R001-115	BWH weight, added 2 decimal places
WX2.12.1 ****** Customer Beta Release *******************************************************
B&R001-117	Recipe Page - Density showing even though not selected on recipe setup pg 205
B&R001-116	Recipe 1 is not working properly if the system is set up for more then 4 hoppers
B&R001-115	Pg 195 - When weight unit set to oz, weight now on pg 330 shows lbs, and vice versa
WX2.12.0 ****** Customer Release *******************************************************
B&R001-99 : page 190, Cannot change security passwords
B&R001-90	Print cyclic not working.. always on now, serial printer fixed.
B&R001-66	Page 14 - Can't enter a value for alarm factor
B&R001-44	Icons - Mixer Motor rotating backwards
WX2.11.6 ****** Customer Beta Release *******************************************************
B&R001-84 : clear gate calibration - using communications fails on system and hopper.
B&R001-70 : Page 21, Maximum AD Bit Range will not accept a value
B&R001-65 : General Issues with Recipes
B&R001-62 : Page 175, When extrusion turned off, prev/next page buttons hidden, 
            cant get to page 176
B&R001-61 : When extrusion and haul-off enabled, from main screen, all buttons take you 
            to screen 454
B&R001-55 : Batch Weight in Recipe not working as expected
B&R001-43 : Setting debug.AbTCP on cause reboot
B&R001-42 : System Status Register, bit 15, batch complete
B&R001-28 : AD Loop Factor Timer
WX2.11.5 ****** Customer Beta Release *******************************************************
B&R001-53 : Modbus TCP and Allen-Bradly TCP not working after version WX2.01.11.1
B&R001-44 : Icons - Mixer Motor rotating clockwise (wrong!), now correctly moving 
            counter-clockwise
WX2.11.4 ****** Customer Beta Release *******************************************************
Leon - Continious Run Value fixed - Dead-heading
WX2.11.3 ****** Customer Beta Release *******************************************************
B&R001-28 : AD Loop Factor Timer
B&R001-40 : Autobatch - pg 323, inventory/shift totals no decimal places for G & H
B&R001-43 : Setting debug.AbTCP on cause reboot
B&R001-47 : Icons - Main/Alarm button visibility
B&R001-50 : Monitor pages... Standard deviation page (pg345). now has next hop button, 
            hoppers e-h now a layer, not new page.
WX2.11.2 ****** Customer Beta Release *******************************************************
self loading - modified timing for continuous run value
WX2.11.1 ****** Customer Beta Release *******************************************************
Recipes Reworked / Overhauled
WX2.01.11.00 ****** Customer Release  *******************************************************
WX2.01.10.14 ****** Customer Beta Release  **************************************************
Alarm Pages updates
Icons - Monitor Beta
WX2.01.10.13 ****** Customer Beta Release  *****************************************************
Icons - Monitor Beta
Profibus UCB save added
WX2.01.10.12 ****** Customer Beta Release  *****************************************************
Setting Manual Mode remotely updates.
recipe hopper 6 min/max bug fix
WX2.01.10.11 ****** Customer Beta Release  *****************************************************
recipe save through communications bug fix.
WX2.01.10.10 ****** Customer Beta Release  *****************************************************
Icons updated, BETA
WX2.01.10.09 ****** Customer Beta Release  *****************************************************
Reboot bug fix with Ethernet gateway.
WX2.01.10.08 ****** Customer Beta Release  *****************************************************
Icons Beta
WX2.01.10.07 ****** Customer Beta Release  *****************************************************
calculate material feed page updated
Gateway added to ethernet page, subnet saves bug fixed also
WX2.01.10.06 ****** Customer Beta Release  *****************************************************
mixer prox invert option
recipe screen updates
AD loop factor timer
Reserve Input alarms, 1, 2 & 3.
Run Manual screen though Comms
WX2.01.10.05 ****** Customer Beta Release  *****************************************************
dump batch when prox switch is uncoverd and mixer motor off/ regardless of dump cycle timer
WX2.01.10.04 ****** Customer Beta Release  *****************************************************
panel contrast/brightness 1-100
3 reserve Alarms created
WX2.01.10.03 ****** Customer Beta Release  *****************************************************
diagnostics page 417 updates.
WX2.01.10.02 ****** Customer Beta Release  *****************************************************
Simulation bug fix
recipe communications fix for auto command
WX2.01.10.01 ****** Customer Beta Release  *****************************************************
fixed density's getting updated though communications.  communications uses BATCH_RECIPE_EDIT now
WX2.01.10.00 ****** Customer Release  *****************************************************
WX2.01.09.05 ****** Customer Beta Release  *****************************************************
Tim Peyton pg345-pg346 updates
WX2.01.09.04 ****** Customer Beta Release  *****************************************************
turn on debug flag to print print.log of batch statistics
recipe screens updates/fixes
WX2.01.09.03 ****** Customer Beta Release  *****************************************************
WX2.01.09.02 ****** Customer Beta Release  *****************************************************
recipe.csv: (number,name(no spaces), %1(a), %2(b), %3(c)... %12(l)), restore recipe special 
      sample line of file: 1,MyRecipe,45.0,35.0,10.0,10.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0
pg. 337 made all output 2 decimal places
WX2.01.09.00 ****** Customer Release  *****************************************************
WX2.01.08.16 ****** Customer Beta Release  *****************************************************
maintenance changes
extrusion control
WX2.01.08.10 ****** Customer Beta Release  *****************************************************
Allen Bradley Communications updates for PCC and PCC2 registers
debug logging expanded
WX2.01.08.09 ****** Customer Beta Release  *****************************************************
Allen Bradley Communications
WX2.01.08.08 ****** Customer Beta Release  *****************************************************
Allen Bradley Communications
WX2.01.08.07 ****** Customer Beta Release  *****************************************************
Allen Bradley Communications fixes for UCB's
WX2.01.08.06b ****** Customer Beta Release  *****************************************************
Allen Bradley Communications fixes for UCB's
WX2.01.08.05b ****** Customer Beta Release  *****************************************************
Allen Bradley Communications fixes for UCB's
WX2.01.08.04 ****** Customer Release  *****************************************************
Recipe - Mix time bug fix
Recipe - Job Weight bug fix
WX2.01.08.01 ****** Customer Release  *****************************************************
Alarm Text fix
WX2.01.08.00 ****** Customer Release  *****************************************************
smtp ability, added. Alarms can be emailed or texted.
profibus cyclic flag added.
English to German Translation complete.
added ability to log messeges, out to a file for debugging
WX2.01.07b.04 ****** Customer Beta Release  *****************************************************
Modbus comms stuff
WX2.01.07b.00 ****** Customer Release  *****************************************************
communication cyclics moved to 5ms cyclic
WX2.01.07.00 ****** Customer Release  *****************************************************
Allen Bradley TCP/IP debug reboot fix
recipe.c recipe gate order fixed
recipe setup allows only percent to be chosen
XML file. CRC check is unusable with float precision
modbus tcp/ip. allow closing of client without affecting current clients.
WX2.01.06.00 ****** Customer Release  *****************************************************
Allen Bradley TCP/IP fixed
Allen Bradley Serial Communication capability added 
Autobatch now works with setup Wizard
WX2.01.05.00 ****** Customer Release  *****************************************************
XML Configuration file, everthing except recipes.
time weight changes for a more accurate table.
print functionality, print to printer or file (usb,disk)
WX2.01.03.02 ****** Customer Release  *****************************************************
Self Loading Added
WX2.01.03.01 ****** Customer Release  *****************************************************
Customer Release
WX2.01.03.00 ****** Customer Release  *****************************************************
Customer Release
WX2.01.02.29 ****** Release candidate 29 *****************************************************
release --- recipes start at 1
gate_pcc.c, gate_pcc.h changed the time table structure and implemented a real moving average
WX2.01.02.28 ****** Release candidate 28 *****************************************************
release --- recipes start at 1
WX2.01.02.27 ****** Release candidate 27 *****************************************************
release ---
WX2.01.02.26 ****** Release candidate 26 *****************************************************
changes in gate_pcc.c
 - Guarded against interpolating outside the interpolation interval; 
 - Guarded against using the "learn the second consecutive unlearned point" before starting to build the table; 
 - Reset the point after 30 hits until a fix for the std_dev formula is implemented
WX2.01.02.23 ****** Release candidate 23 *****************************************************
Release... 
WX2.01.02.22 ****** Release candidate 22 *****************************************************
Pre-Release... pg000 - pg450 visual bug.
WX2.01.02.21 ****** Release candidate 21 *****************************************************
Pre-Release... pg000 - pg450 visual bug.
WX2.01.02.20 ****** Release candidate 20 *****************************************************
Pre-Release... recipe nextnum bug.
WX2.01.02.19 ********* Release candidate 19 *****************************************************
Pre-Release... recipe nextnum bug.
WX2.01.02.18 ********* Release candidate 18 *****************************************************
Pre-Release... recipe nextnum bug.
WX2.01.02.17 ********* Release candidate 17 *****************************************************
Pre-Release... 
WX2.01.02.16 ********* Release candidate 16 *****************************************************
recipe.c       bug fixes related to recipe interface 
interface.c    bug fixes related to recipe interface 
WX2.01.02.15 ********* Release candidate 15 *****************************************************
recipe.c       bug fixes related to recipe interface 
WX2.01.02.14 ********* Release candidate 14 *****************************************************
recipe.c       bug fixes related to recipe interface 
WX2.01.02.13 ********* Release candidate 13 *****************************************************
recipe.c       bug fixes related to recipe interface
WX2.01.02.12 ********* Release candidate 12 *****************************************************
recipe.c       bug fixes related to recipe interface delete/all button/security on save
WX2.01.02.11 ********* Release candidate 11 *****************************************************
recipe.c       B&R001-06 Recipe Hopper Order
interface.     B&R001-06 Recipe Hopper Order
visual.obj     B&R001-06 Recipe Hopper Order
WX2.01.02.10 ********* Release candidate 10 *****************************************************
recipe.c       B&R001-06 Recipe Hopper Order
interface.     B&R001-06 Recipe Hopper Order
visual.obj     B&R001-06 Recipe Hopper Order
WX2.01.02.09 ********* Release candidate 9 *****************************************************
recipe.c       B&R001-06 Recipe Hopper Order
interface.     B&R001-06 Recipe Hopper Order
visual.obj     B&R001-06 Recipe Hopper Order
WX2.01.02.08 ********* Release candidate 8 *****************************************************
recipe.c       B&R001-06 Recipe Hopper Order
interface.     B&R001-06 Recipe Hopper Order
visual.obj     B&R001-06 Recipe Hopper Order
WX2.01.02.07 ********* Release candidate 7 *****************************************************
recipe.c       B&R001-32 Recipe pg450 Next Changeable
WX2.01.02.06 ********* Release candidate 6 *****************************************************
recipe.c       B&R001-48 Old Recipe Interface
WX2.01.02.05 ********* Release candidate 5 *****************************************************
recipe.c       B&R001-48 Old Recipe Interface
interface.c    B&R001-48 Old Recipe Interface
WX2.01.02.04 ********* Release candidate 4 *****************************************************
recipe.c       B&R001-48 Old Recipe Interface
visual obj     B&R001-48 Old Recipe Interface
WX2.01.02.03 ********* Release candidate 3 *****************************************************
recipe.c       B&R001-48 Old Recipe Interface
visual obj     B&R001-48 Old Recipe Interface
WX2.01.02.02 ********* Release candidate 2 *****************************************************
interface.c    B&R001-20 Change Encryption From External File To User Entered Key. 
visual obj	   B&R001-21 Add Unit Serial Number and MAC Address To Page 404. 
hmi.h		   	B&R001-34 Test Security Levels Work Correctly. 
mem.h		   	B&R001-51 Misc. Visualization Inconsistencies. 
receipe.c		B&R001-70 Changing the batch size during AUTO_MODE has been enabled. Needs to be checked if it functions correctly. 
alarm.c			B&R001-72 Send Panel into Error Screen if reading MAC Address or Panel ID timeouts.
monitor.c	  	B&R001-62 Direct entry into page 005 Manual Mode Items are still checked.	   
totals.c
security.c
WX2.01.02.01 ********* Release candidate 1 *****************************************************
recipe.c       B&R001-48 Old Recipe Interface
interface.c    B&R001-48 Old Recipe Interface
WX2.01.03.00 ********* Release candidate 1 *****************************************************
recipe.c       B&R001-6 update page 450 so the recipe order is correct BEB
remote.h       removed redundant #defines TYPE_XXX. BEB
gate_pcc.c     B&R001-16 init_dev() moved to _INIT for proper initialization. BEB
recipe.c       B&R001-36 USE button now active when batch is 100%. BEB
recipe.c       B&R001-37 Gate order reboot fixed, subscripts were out of range. BEB
1.00.47 ********* Release candidate 3 *****************************************************
hmi.h          Added some additional color vars.
setup.c        Set time_wt load status to fail if file cannot be read.
proc_mon.c     Moved recipe file load to after system is loaded.
interface.c    Added loop to clear previous selections when man mode is pressed.
               Added color toggle in precess_leds to flash alarm button color.
gate_pcc.c     Added loop to set load status of time_wt to crc_Fail if file fails to
               load.
file_rw.c      Changed init loop in store_config
defaults.c     Changed default for safety interrupt pause mode
1.00.46 ********* Release candidate 2 *****************************************************
setup.c        Added load status page for time_wt of the 12 gates.
recipe.c       Fixed display of layers on page 450,  Fixed a couple of return
               page values.  Added Signal.store_recipe when "Use Recipe" pressed.
proc_mon.c     Fixed recipe restore on power up to correct display last recipe.
mixer.c        Added code to let batch finish when prox uncovered while in "Waiting
               full" mode.
gate_pcc.c     Fixed two bugs in calc_gate_time 1 occurred when index was 0 and the other
               occurred when index was 199.
interface.c    Fixed bug that sent user to recipe page after AUTO pressed.
file_rw.c      Added load status variables to restore_timewt.  Added temporary structure
               to load config into then after successful crc check, copy into regular
               structure.
bwh_pcc.c      Changed code setting minimum dump time to 10s is now 5s
1.00.45 ********* Release candidate 1 *****************************************************
 file_ronly.c  redid logic in case statement.  and moved set of global var to inside case
               statement (only 1 place).  This is now the ony place var is set to TRUE.
   recipe.h    #define MAXRECIPE_S     MAX_RECIPES
     mem.h     fix definition of MAX_RECIPES
1.00.44 ********* Release candidate 1 *****************************************************
      usb.c    reworked how result is returned and used

   Procmon.c   Reworked recipe store/restore recipes.

   Gate_pcc.c  Fixed debug print feed table.  Used cleanup_timewt to
               reset time_wt[0] when numpts corrupts avg vars.  Added
               clear_timewt() to actual reset the point requested.
   BWP_pcc.c:  Added counter to limit the number of times that Jiggle Gate is used in A batch

  Visualizaton Reworked store/restore pages and added recipe to load status.
  /Setup.c     PG225/226/227

   recipe      replaced recipe handling with new process that works with new
               recipe array.  Fixed "use recipe" on pg450.  Added function to
               cancel changes on pg453 by reloading saved recipe file.
   mixer       changed mix-time to mS
   interface   implement new recipe array & removing g, changed first store
               of time_wt to batch 1 and then still every 50 thereafter.  Added
               store/restore config to interface.  Added code to close gates if
               "stop" and "don't finish batch" are selcted.  Previously they were
               left "as-is"  (If open, left open).
   file_rw     reworked config store/restore to prevent increasing the offset
               var second or more times through each block.  Created new
               store/restore routine for new recipe structure.  Added counter to
               limit number of times control passes though each FILE_IO routine.
               Added flag var to limit access to file_IO FUB's to one at a time.
               Changed basic file_IO flow to allow flags and counters and copy
               any existing file to backup name and create new file every time.
   file_ronly  created new global var to hold value of crypt check routine.
               This is then checked instead of re-calling file_ronly.
   various     Implement new recipe array and move all recipe vars to this array.
1.00.43 *************************************************************************
   WxCode.pgp  Combined all 12 gate tasks into a single task called gatecntr
   Gate_pcc.c  Removed clear of wt_stable from prepulse any wt will be caught
               on pule_gate.
   BWP_pcc.c:  Changed shrBWH to include 10 pt MA for batch time, batch weight,
               average WTP and max WTP.  Created functions for clearing and
               adding points to these.  All time vars for this changed to mS.
               Changed loop to dnum for consistency.  Moved batch stat calcs
               to finish batch.
   mixer.c:    removed force of mixtime to 5S if mixer motor mode always on.
               Consolidated WTP calculations
   visualization:  Created a new unit group to properly display mS as Sec
   setup.c:    Changed config mixer demo to default to 2000 lbs/hr from 500.
1.00.42 *************************************************************************
     pg017      fixed a miss-direct on simulation setup button
     bwh_pcc.c  Verified John's changes to remove compile errors they
        caused.  Left John's WTP calc but rolled back to displaying
        previous one.  Will finalize once max_rate issue is settled.
     defaults.c Changed default of prepulse_enabled to TRUE
     gate_pcc.c Added variable to signal when should do prepulse.
        Removed some old code.  Remove Auger as a case for prepulse.
        Only modifed targer_wt on first shot of gate.  Added clear
        learn_mode on first gate shot and only reset it if wt found
        is less than 90% of target weight.  (normal mode offset is
        8% on first shot).  Changed calc_gate_time to have a single
        return point.
        intrface.c  Put back vars deleted by John that are needed to
           signal store time weight.
        mixer.c     Put previous WTP back until max rate question is answered.
        setup.c     Changed pg018 to only shade out prepulse enabled if
           user level too low.
        bwh_pcc.h   Moved John's #defines to H file
1.00.41   Added load status page227 to visualization
          Added restore from sys disk to system restore page226
          Added store to sys disk to system store page225
          Added Load status button to pg170
          added gate shots to batch stats telnet statement bwh_pcc.c
          Changed defaults.c to allow partial default on load failure
          Changed file_rw.c to allow partial load failure
          in gate_pcc.c converted num_pts > 29 to a moving average instead of reset
              to zero.
          monitor.c Tim Peyton Prox. alarm can now be disabled by entering 0 sec.
              And values are cleared when changed from 0 to ...
          proc_mon.c reset to defaults function altered to match new partial
              failure mode.
1.00.40   pg345:  Widen fields on average vars, added completion var to change of
              prox timer.  Added virtual keys to accommodate changes to SPD page
              for TIM Peyton.
          gate_pcc.c:  Expanded debug statement to show opengate count open time, and
              close time of gate.
          monitor.c:  Added clear button to SPD and SWD data on pg345 handled in case
              pg345 here.  Added code to handle clearing SPD and SWD and setting prox
              timer to/from 0 to limit range to 15-900 when above zero.
          Opengate.c:  Added call to get-time when gate opened and when closed.
          HMI.h:  Added vars to handle clearing entries.
          mem.h:  Made several vars _GLOBAL so I could see them in gate_pcc
1.00.39   Changed how learn mode is set.
          Moved check of zc time vs MIN_GATE_TIME_HI so that it will get checked.
          Recreated pre-pulse and set it to be used in learn mode to get more acccurate
               first entries in time_wt table.
          Added  a second way to trigger shift time wt if overshot and fired min_gate time.
          Moved FiveMSCounter & unsigned int GateOpenTime to mem.h and made global.
          Added code to do a MA of raw AD in Calc_ad
          Created code to predict weight at a new point on the timewt table and modify
          gate time accordingly.
1.00.38   Added code to signal load time_wt
          Added code to correct display of WTP as NaN
          Added debug display of batch data
          Added flag to signal use settle time on learn mode
          Converted Tim Peyton Alarm to display weight
1.00.37   Added activity check to Security timeout
          Resolve several warning regarding function declarations.  Changed round_gate_time
          to truncate.  Created shift_time_wt to move mingate time and table when needed.
          Changed to use waiting for settling during zero_cross.  Call shift_q   1time_wt if
          time_wt[0] num_pts >=10 and avg_wt greater than zc_min_wt.  Fixed calc mode Z to
          use saved_avg_wt instead of saved_avg_time.
1.00.36   Fixed cal mode Y in Calc_gate_time to correctly mod comparison weight
1.00.35   Fixed Problem with Security timeout getting stored as mS not Minutes
                                                                        */
/************************************************************************/

#define SW_VERSION_DATE "06/19/18"
#define SW_VERSION      "7.7.5"
#define FW_VERSION      "WX7.7.5"
#define WE_VERSION      "WX7.7.5"   /* Guardian w/ Extrusion Control      */
#define FW_VERSION_AB   "WX7.7.5"   /* AutoBatch firmware version         */	  

/* Current level of Gravilink supported, passed in id_vector from sremote */
#define GRAVILINK_VERSION  2
#define GRAVILINK_REVISION 0

/* Current level of Configuration Structure Revisions */
#define CFG_SUPER_VERSION  "2.0.0"
#define CFG_BWH_VERSION    "2.0.0"
#define CFG_HO_VERSION     "1.0.0"
#define CFG_EXT_VERSION    "1.0.0"
#define CFG_GATE_VERSION   "2.0.0"
#define CFG_MIXER_VERSION  "2.0.0"
#define SHR_SUPER_VERSION  "1.0.0"
#define SHR_BWH_VERSION    "1.0.0"
#define SHR_HO_VERSION     "1.0.0"
#define SHR_EXT_VERSION    "1.0.0"
#define SHR_GATE_VERSION   "1.0.0"
#define SHR_MIXER_VERSION  "1.0.0"
#define TIME_WT_VERSION    "1.0.0"
#define RECIPE_VERSION     "2.0.0"
/* note only change these when the structure definitions change GTF */


