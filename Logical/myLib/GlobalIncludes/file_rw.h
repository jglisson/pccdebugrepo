/************************************************************************/
/*
   File:  file_rw.h

   Description:   This file contains the defines needed to interface the
      system to the cf cards.


      $Log:   F:\Software\BR_Guardian\file_rw.h_v  $
 *
 *    Rev 1.3   Mar 26 2008 12:42:34   vidya
 * Defined CreateSaveRecipe and DeleteAllRecipe.
 *
 *
 *    Rev 1.2   Mar 13 2008 13:18:14   vidya
 * Added a write state for recipe store function.
 *
 *    Rev 1.1   Jan 15 2008 13:40:42   FMK
 * Changed block size to 2k.
 *
 *    Rev 1.0   Jan 11 2008 10:33:32   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#define Def_ErrLvlOpen     1  /* error occured opening file */
#define Def_ErrLvlCreate   2  /* error occured createing file */
#define Def_ErrLvlWrite    3  /* error occured writing file */
#define Def_ErrLvlRead     4  /* error occured reading file */
#define Def_ErrLvlClose    5  /* error occured closing file */

#define Def_MoreBlock      0  /* define value for more blocks to process */
#define Def_LastBlock      1  /* define value for last block to process */

#define Def_StateReadConfigInit     0  /* defined state for setting up to read configuration */
#define Def_StateReadConfigFindFile 1  /* find config file in filelist to read */

#define FILE_STORE_INIT       0  /* defined state for store initialization */
#define FILE_STORE_PATH       1  /* defined state for store initialization */
#define FILE_STORE_OPEN       2  /* defined state for store open a file */
#define FILE_STORE_CREATE     3  /* defined state for store creating a file */
#define FILE_STORE_BUILD      4  /* defined state for store build the record/block */
#define FILE_STORE_WRITE      5  /* defined state for store writing the block */
#define FILE_STORE_NEXT_GATE  6  /* defined state for storing TimeWt          */
#define FILE_STORE_MIX        6  /* defined state for store writing the block */
#define FILE_STORE_RECIPE     6  /* defined state for store writing the block */
#define FILE_STORE_BWH        7  /* defined state for store writing the block */
#define FILE_STORE_HO         8  /* defined state for store writing the block */
#define FILE_STORE_EXT        9  /* defined state for store writing the block */
#define FILE_STORE_SYS       10  /* defined state for store writing the block */
#define FILE_STORE_GATE_A    11  /* defined state for store writing the block */
#define FILE_STORE_GATE_B    12  /* defined state for store writing the block */
#define FILE_STORE_GATE_C    13  /* defined state for store writing the block */
#define FILE_STORE_GATE_D    14  /* defined state for store writing the block */
#define FILE_STORE_GATE_E    15  /* defined state for store writing the block */
#define FILE_STORE_GATE_F    16  /* defined state for store writing the block */
#define FILE_STORE_GATE_G    17  /* defined state for store writing the block */
#define FILE_STORE_GATE_H    18  /* defined state for store writing the block */
#define FILE_STORE_GATE_I    19  /* defined state for store writing the block */
#define FILE_STORE_GATE_J    20  /* defined state for store writing the block */
#define FILE_STORE_GATE_K    21  /* defined state for store writing the block */
#define FILE_STORE_GATE_L    22  /* defined state for store writing the block */
#define FILE_STORE_DONE      23  /* defined state for store writing the block */
#define FILE_DELETE_BACKUP   24  /* defined state for store writing the block */
#define FILE_DELETE_EXISTING 25  /* defined state for store writing the block */
#define FILE_STORE_COPY      26  /* defined state for store writing the block */
#define FILE_STORE_CLOSE     30  /* defined state for store closing the file */
#define FILE_STORE_ERROR     31  /* defined state for store error */
#define FILE_STORE_SEARCH    32  /* defined state for searcing the file */

#define FILE_READ_INIT        0  /* defined state for Read initialization */
#define FILE_READ_PATH        1  /* defined state for store initialization */
#define FILE_READ_SEARCH      2  /* defined state for searcing the file */
#define FILE_READ_OPEN        3  /* defined state for Read open a file */
#define FILE_READ_BLOCK       4  /* defined state for Read a file */
#define FILE_READ_MIX         5  /* defined state for store writing the block */
#define FILE_READ_BWH         6  /* defined state for store writing the block */
#define FILE_READ_HO          7  /* defined state for store writing the block */
#define FILE_READ_EXT         8  /* defined state for store writing the block */
#define FILE_READ_SYS         9  /* defined state for store writing the block */
#define FILE_READ_GATE_A     10  /* defined state for store writing the block */
#define FILE_READ_GATE_B     11  /* defined state for store writing the block */
#define FILE_READ_GATE_C     12  /* defined state for store writing the block */
#define FILE_READ_GATE_D     13  /* defined state for store writing the block */
#define FILE_READ_GATE_E     14  /* defined state for store writing the block */
#define FILE_READ_GATE_F     15  /* defined state for store writing the block */
#define FILE_READ_GATE_G     16  /* defined state for store writing the block */
#define FILE_READ_GATE_H     17  /* defined state for store writing the block */
#define FILE_READ_GATE_I     18  /* defined state for store writing the block */
#define FILE_READ_GATE_J     19  /* defined state for store writing the block */
#define FILE_READ_GATE_K     20  /* defined state for store writing the block */
#define FILE_READ_GATE_L     21  /* defined state for store writing the block */
#define FILE_READ_DONE       22  /* defined state for store writing the block */
#define FILE_READ_CLOSE      30  /* defined state for Read closing the file */
#define FILE_READ_NOTFOUND   31  /* defined state for Read error */
#define FILE_READ_ERROR      32  /* defined state for Read error */
/* old defines used in recipe_rw not 100% about values */
//#define FILE_READ_WRITE       4
//#define FILE_READ_PARSE       3

//#define FileMaxBlocks  2         /* Length of file block processed at one time */
//#define FileBlockSize  2048      /* Length of file block processed at one time */
//#define FileKeySize    50        /* max length of keys used in configuration files on cf card */

#define FileGateA    0           /* configuration file index for gate a */
#define FileGateB    1           /* configuration file index for gate b */
#define FileGateC    2           /* configuration file index for gate c */
#define FileGateD    3           /* configuration file index for gate d */
#define FileGateE    4           /* configuration file index for gate e */
#define FileGateF    5           /* configuration file index for gate f */
#define FileGateG    6           /* configuration file index for gate g */
#define FileGateH    7           /* configuration file index for gate h */
#define FileGateI    8           /* configuration file index for gate i */
#define FileGateJ    9           /* configuration file index for gate j */
#define FileGateK    10          /* configuration file index for gate k */
#define FileGateL    11          /* configuration file index for gate l */
#define FileMixer    12          /* configuration file index for gate mixer */
#define FileBatch    13          /* configuration file index for gate batch hopper */
#define FileHO       14          /* configuration file index for gate haul off */
#define FileExtruder 15          /* configuration file index for gate extruder */
#define FileSystem   16          /* configuration file index for gate system */
#define FileDynData  17          /* configuration file index for gate dynamic data */

#define CreateSaveRecipe  0
#define DeleteAllRecipe   1

#define NUM_STRUCTURES   45  /* Max number of structures in the sys store/restore */
#define MIX_STATUS       1   /* Load status of this structure */
#define BWH_STATUS       2   /* Load status of this structure */
#define HO_STATUS        3   /* Load status of this structure */
#define EXT_STATUS       4   /* Load status of this structure */
#define SYS_STATUS       5   /* Load status of this structure */
#define SHO_STATUS       6   /* Load status of this structure */
#define WTH_STATUS       7   /* Load status of this structure */
#define GRF_STATUS       8   /* Load status of this structure */
#define LIW_STATUS       9   /* Load status of this structure */
#define GATE_A_STATUS    10  /* Load status of this structure */
#define GATE_B_STATUS    11  /* Load status of this structure */
#define GATE_C_STATUS    12  /* Load status of this structure */
#define GATE_D_STATUS    13  /* Load status of this structure */
#define GATE_E_STATUS    14  /* Load status of this structure */
#define GATE_F_STATUS    15  /* Load status of this structure */
#define GATE_G_STATUS    16  /* Load status of this structure */
#define GATE_H_STATUS    17  /* Load status of this structure */
#define GATE_I_STATUS    18  /* Load status of this structure */
#define GATE_J_STATUS    19  /* Load status of this structure */
#define GATE_K_STATUS    20  /* Load status of this structure */
#define GATE_L_STATUS    21  /* Load status of this structure */

#define RECIPE_STATUS    29  /* Load status of this structure */

#define GATE_A_tw_STATUS 30  /* Load status of this structure */
#define GATE_B_tw_STATUS 31  /* Load status of this structure */
#define GATE_C_tw_STATUS 32  /* Load status of this structure */
#define GATE_D_tw_STATUS 33  /* Load status of this structure */
#define GATE_E_tw_STATUS 34  /* Load status of this structure */
#define GATE_F_tw_STATUS 35  /* Load status of this structure */
#define GATE_G_tw_STATUS 36  /* Load status of this structure */
#define GATE_H_tw_STATUS 37  /* Load status of this structure */
#define GATE_I_tw_STATUS 38  /* Load status of this structure */
#define GATE_J_tw_STATUS 39  /* Load status of this structure */
#define GATE_K_tw_STATUS 40  /* Load status of this structure */
#define GATE_L_tw_STATUS 41  /* Load status of this structure */

#define STRUCTURE_LOADED   1
#define STRUCTURE_DEFAULT  2
#define STRUCTURE_CRC_FAIL 3
#define FILEFAILpoint      200
/* file restore err codes GTF */
#define USBFAIL            10
