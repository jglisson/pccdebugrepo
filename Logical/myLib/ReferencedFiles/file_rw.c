 #ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  file_rw.c

   Description: This file controls all read/write funtionality to the
      external cf cards.


      $Log:   F:\Software\BR_Guardian\file_rw.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.fileRw to toggle the debug information in this file
 *
 *    Rev 1.21   Sep 15 2008 13:14:46   gtf
 * Fixed Gate order in recipe bug
 *
 *    Rev 1.20   Jul 01 2008 10:06:56   vidya
 * Added a function to store resins.
 *
 *    Rev 1.19   May 16 2008 11:21:52   vidya
 * Added control_mode variable in BuildSystemFile
 * and ProcessSystemFile.
 *
 *    Rev 1.18   May 15 2008 15:39:20   YZS
 * Added a serial port protocol option.
 *
 *    Rev 1.17   May 09 2008 15:46:10   FMK
 * Fixed problem with building of configuraiton files
 * for gate and system.
 *
 *    Rev 1.16   May 06 2008 15:07:30   FMK
 * Setup remote port settings in visualization as
 * well as config_super.
 *
 *    Rev 1.15   Apr 30 2008 14:18:16   vidya
 * Got the delete all recipes to work.
 *
 *    Rev 1.14   Apr 14 2008 13:37:58   FMK
 * Added ethernet settings to configuration store.
 * Fixed problem with system alarm parameters
 * setup.
 *
 *    Rev 1.13   Apr 03 2008 13:01:58   vidya
 * Corrected the recipe file creation function.
 *
 *    Rev 1.12   Apr 02 2008 15:06:06   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.11   Mar 28 2008 10:00:18   FMK
 * Got the simulation screens working on the
 * visualization.
 *
 *    Rev 1.10   Mar 26 2008 12:39:00   vidya
 * Modified file ident for Recipe Configuration file.
 * Also, initialized variable result to "FAIL".
 *
 *    Rev 1.9   Mar 13 2008 13:24:46   vidya
 * Added functions to store recipe file on to the CF card
 * as well as to read and parse the recipe file from
 * the CF card.
 *
 *    Rev 1.8   Mar 07 2008 10:28:52   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.7   Feb 06 2008 13:24:28   FMK
 * Added the saving/retrieving of the calibration data
 * for the extruder.
 *
 *
 *    Rev 1.6   Feb 01 2008 14:01:26   FMK
 * Fixed warning in compiles for implicit function
 * declaration.
 *
 *    Rev 1.5   Jan 30 2008 13:51:40   FMK
 * Fixed problem with opening files when reading
 * the configuration. The Path variable was somehow
 * messing up the open FBK. By making this variable
 * a global variable the problem is resolved.
 *
 *    Rev 1.4   Jan 25 2008 14:07:28   FMK
 * Made changes to the setup pages to allow the
 * user to store/restore configuration files to a thumb
 * drive installed in the USB ports.
 *
 *    Rev 1.3   Jan 18 2008 09:48:06   FMK
 * Changed global variables to new naming structure.
 * Fixed issues with storing and retrieving the configuration.
 * Problem existed with the Batch Weigh Hopper and
 * the system parameters. Added security to the system
 * parameters.
 *
 *    Rev 1.2   Jan 15 2008 15:20:18   FMK
 * When processing filelist to retrieve configuration, if
 * got to non-existent filename returned error rather than
 * FILENOTFOUND or graceful exit. Added test to look only
 * at valid files.
 *
 *    Rev 1.1   Jan 14 2008 15:50:32   FMK
 * Reconciled changes from Vidya. Added configuration
 * save for extruders, haul off and system. Yet to do alarms
 * and security.
 *
 *    Rev 1.0   Jan 11 2008 10:33:32   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <fileio.h>
#include "mem.h"
#include "sys.h"
#include "file_rw.h"
#include "signal_pcc.h"
#include "asusb.h"
#include "usb.h"
#include <math.h>
#include "vars.h"
#include "libbgdbg.h"
#include <string.h>
#include "version.h"
#include "timer.h"
#include "hmi.h"
#include "gate_pcc.h"
#include "recipe.h"

#define FloatValueType     0
#define IntegValueType     1
#define StringValueType    2
#define None_ValueType     3

#define USB_GETNODELIST    1

// All not used removed by DRT 3/10/17
//
//#define NumRecipes         500
//#define RecipeSize         2 * sizeof(recipe_struct)
//#define NumResins          MAX_RESINS
//#define ResinSize          60
//#define RecipeArraySize    20
//#define delim              "\n"
//char *gLine;
//char strFileBlock[FileMaxBlocks][FileBlockSize]; /* current files data block */
//char strRecipeFileBlock[NumRecipes][sizeof(recipe_struct)];
//char strResinFileBlock[NumResins][ResinSize];
//unsigned int gBlockCharPos;    /* used as a position indicator within the
//                                 currently scanned file block */
//unsigned int gReadFileIndex;   /* index into file directory table for read */
//int gRecipeNum, gRecipeNotExists;

/* for config file stuff GTF */
FileOpen_typ CFGFile_TypOpen;          /* structure for opening file */
FileClose_typ CFGFile_TypClose;        /* structure for closing file */
FileCreate_typ CFGFile_TypCreate;      /* structure for creating file */
FileWrite_typ CFGFile_TypWrite;        /* structure for writing data to file */
FileRead_typ CFGFile_TypRead;          /* structure for reading data from file */
/* For recipe stuff */
FileOpen_typ RecipeFile_TypOpen;       /* structure for opening file */
FileClose_typ RecipeFile_TypClose;     /* structure for closing file */
FileCreate_typ RecipeFile_TypCreate;   /* structure for creating file */
FileWrite_typ RecipeFile_TypWrite;     /* structure for writing data to file */
FileRead_typ RecipeFile_TypRead;       /* structure for reading data from file */
/* For resin stuff */
FileOpen_typ ResinFile_TypOpen;        /* structure for opening file */
FileClose_typ ResinFile_TypClose;      /* structure for closing file */
FileCreate_typ ResinFile_TypCreate;    /* structure for creating file */
FileWrite_typ ResinFile_TypWrite;      /* structure for writing data to file */
FileRead_typ ResinFile_TypRead;        /* structure for reading data from file */
/* for Time_wt file stuff GTF */
FileOpen_typ TIMEFile_TypOpen;         /* structure for opening file */
FileClose_typ TIMEFile_TypClose;       /* structure for closing file */
FileCreate_typ TIMEFile_TypCreate;     /* structure for creating file */
FileWrite_typ TIMEFile_TypWrite;       /* structure for writing data to file */
FileRead_typ TIMEFile_TypRead;         /* structure for reading data from file */

_GLOBAL UDINT TIMEFileIDent;           /* the system id for the file currently opened */
_GLOBAL unsigned long TIMEFile_offset;

_GLOBAL int COUNTconfig;

/* For recipe stuff */
FileOpen_typ   RECIPEFile_TypOpen;  /* structure for opening file */
FileClose_typ  RECIPEFile_TypClose; /* structure for closing file */
FileCreate_typ RECIPEFile_TypCreate;/* structure for creating file */
FileWrite_typ  RECIPEFile_TypWrite; /* structure for writing data to file */
FileRead_typ   RECIPEFile_TypRead;  /* structure for reading data from file */
_LOCAL FileDelete_typ FDelete;      /* structure for deleting data file */
_LOCAL FileCopy_typ FCopy;          /* structure for copying data file */

_GLOBAL UDINT RECIPEFileIDent;      /* the system id for the file currently opened */
_GLOBAL unsigned long RECIPEFile_offset;
_LOCAL  int USBresult;
_GLOBAL int COUNTrecipe;
_GLOBAL unsigned char gStorePath[15];
_GLOBAL int gstate_Store_Config;
_GLOBAL int gstate_Restore_Config;

UDINT gFileIDent;      /* the system id for the file currently opened */
UDINT gRecipeFileIDent;
UDINT gResinFileIDent;
USINT gErrorLevel;     /* indicates where the error occurred */
UINT  gsysError;       /* return from 'FileIoGetgsysError' */

int gStatus_ReadTime;  /* State of the cyclic task of reading a file */
int gFileCreated = 0;
int gUSBPath1Active;   /* Specifies which USB drive path is valid */

int RetrieveKey(unsigned char *strKey, unsigned char *strValue, char *bHeader);
unsigned int CRC16(unsigned char *buffer, unsigned int bufferLength);
int Store_TimeWt(void);
int Restore_TimeWt(void);
int Store_Recipes(void);

int Restore_Recipes(void);
int Store_Config  (char * pFname, char * pData, unsigned long size);
int Restore_Config(char * pFname, char * pData, unsigned long size);

extern unsigned int elap_time(TIME iTime);
extern unsigned short brsitoa(signed long value, unsigned long pString);

// global variable for debug information
// uncomment var below when debug output is desired
//_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/*************************************************************************
 * calculate a 16 bit CRC value for the contents in buffer               *
 *                                                                       *
 * INPUTS:                                                               *
 *   unsigned char *buffer                                               *
 *     pointer to the buffer to calculate the CRC for.  This buffer can  *
 *     contain anything (structure, string, int, etc) as long as the     *
 *     the size of the buffer is known.                                  *
 *   unsigned short bufferLength                                         *
 *     the number of bytes in the buffer                                 *
 *                                                                       *
 * OUTPUTS:                                                              *
 *   returns an unsigned short containing the calculated CRC16 value     *
 *                                                                       *
 * HISTORY:                                                              *
 *   2008-11-18 - JLR - intial version.  adapted from the Modbus spec    *
 *   document                                                            *
 *                                                                       *
 *                                                                       *
 *                                                                       *
 *************************************************************************/

/* Table of CRC values for high�order byte */
static unsigned char auchCRCHi[] = {
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
	0x00, 0xC1, 0x81, 0x40};

/* Table of CRC values for low�order byte */
static char auchCRCLo[] = {
	0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7,
	0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E,
	0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9,
	0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
	0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
	0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32,
	0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4, 0x3C, 0xFC, 0xFD, 0x3D,
	0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
	0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF,
	0x2D, 0xED, 0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
	0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0, 0xA0, 0x60, 0x61, 0xA1,
	0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
	0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB,
	0x69, 0xA9, 0xA8, 0x68, 0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA,
	0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
	0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
	0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97,
	0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E,
	0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89,
	0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
	0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83,
	0x41, 0x81, 0x80, 0x40};

unsigned int CRC16 (unsigned char *buffer, unsigned int bufferLength)
{
	unsigned char highByte = 0xFF;    /* high byte of CRC */
	unsigned char lowByte = 0xFF;     /* low byte of CRC */
	unsigned int index;               /* index into the CRC lookup table */

	/*  return bufferLength; */
	while (bufferLength--)            /* loop thru the buffer */
	{
		index = highByte ^ *buffer++;   /* calculate the CRC */
		highByte = lowByte ^ auchCRCHi[index];
		lowByte = auchCRCLo[index];
	}
	return (highByte << 8 | lowByte);
}

/************************************************************************/
/*
   Function:  Store_TimeWt()

   Description: This function tries to open and save data to an existing
        configuration file. If the file does not exist, it is created. It
        will save data according to the set block size. If more than a
        block is needed another block will be created until all of the
        required data is saved. When finished the file will be closed.

        This routine must run cyclically until all the data is written.

        It will be auto triggered off of Signal.Store_Timewt
        It may be user triggered off of Signal.UserBackupTimeWt

   Returns: SUCCESS
            FAIL
            NOTDONE
                                                                        */
/************************************************************************/
int Store_TimeWt(void)
{
	static int   state_Store_TimeWt = FILE_STORE_INIT; /* static var for state function is in */
	static unsigned int wStatus;                       /* return status for writing to file */
	static unsigned int ST_ErrorLevel;
	static unsigned int ST_Error;
	static int usbAction;
	int file_stored;
	int result;
	unsigned char gatenum;
	static int gnum;

	COUNTtimewt += 1;
	if (COUNTtimewt > FILEFAILpoint)
		state_Store_TimeWt = FILE_STORE_ERROR;

	result = NOTDONE;
	switch (state_Store_TimeWt){
		case FILE_STORE_INIT:    /* initialize parameters for processing store state machine */
			if(Signal.FILEIO_INUSE)
			{
				break;
			}
			else
			{
				COUNTtimewt = 0;
				file_stored = FAIL;
				gnum = 0;
				gUSBPath1Active = TRUE;
				ST_Error = 0;
				usbAction = USB_GETNODELIST;
				state_Store_TimeWt = FILE_STORE_PATH;
				Signal.FILEIO_INUSE = TRUE;
			}
		case FILE_STORE_PATH:    /* initialize parameters for processing store state machine */
			/* if user signaled for storage look for USB GTF*/
			if (Signal.USB_store)
				if (usbAction != USB_DONE)
					USBresult = usb_check(&usbAction);      /* if the first usb path fails the code needs to switch to the second usb path */
				else
					if (USBresult == SUCCESS)
					   state_Store_TimeWt = FILE_STORE_SEARCH;
				else
				{
					HMI.pg225_err_state = state_Store_TimeWt;
					HMI.pg225_err_code = USBresult;
					state_Store_TimeWt = FILE_STORE_ERROR;
				}
			else /* assume sys disk is present GTF*/
			{
				brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
				state_Store_TimeWt = FILE_STORE_SEARCH;
			}
			break;
		case FILE_STORE_SEARCH:
			state_Store_TimeWt = FILE_DELETE_BACKUP;
			if (Signal.USB_store)
			{
				if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"/bd0") == 0)
				{
					brsstrcpy((UDINT)gStorePath, (UDINT)USB1DrivePath);
					gUSBPath1Active = TRUE;
				}
				else if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"/bd0") == 0)
				{
					brsstrcpy((UDINT)gStorePath, (UDINT)USB2DrivePath);
					gUSBPath1Active = FALSE;
				}
				else
				{
					HMI.pg225_err_state = state_Store_TimeWt;
					HMI.pg225_err_code = 0;
					state_Store_TimeWt = FILE_STORE_ERROR;   /* no usb mass storage device found */
				}
			}
			break;
		case FILE_DELETE_BACKUP:
			/* delete old backup file GTF */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.old");
			FDelete.enable  = 1;
			FDelete.pDevice = (UDINT)&gStorePath;
			FDelete.pName   = (UDINT)&SYSFilename;
			/* Call FBK */
			FileDelete(&FDelete);
			/* Get status */
			wStatus = FDelete.status;
			if (wStatus == brNOERROR)
			{
				/* delete successful go on GTF */
				brsstrcpy((UDINT)SYSFilename,  (UDINT)"TimeWt.pcc");
				brsstrcpy((UDINT)SYSFilename2, (UDINT)"TimeWt.old");
				state_Store_TimeWt = FILE_STORE_COPY;
			}
			else if (wStatus != brBUSY)
			{
				if (wStatus == brFILENOTFOUND)
				{
					/* no old backup file so no problem GTF */
					brsstrcpy((UDINT)SYSFilename,  (UDINT)"TimeWt.pcc");
					brsstrcpy((UDINT)SYSFilename2, (UDINT)"TimeWt.old");
					state_Store_TimeWt = FILE_DELETE_EXISTING;
					//state_Store_TimeWt = FILE_STORE_COPY;
				}
				else
				{
					/* finished with some other error GTF */
					state_Store_TimeWt = FILE_STORE_ERROR;
				}
			}
			break;
		case FILE_STORE_COPY:
			/* copy existing file to backup name GTF */
			FCopy.enable   = 1;
			FCopy.pSrcDev  = (UDINT)&gStorePath;
			FCopy.pSrc     = (UDINT)&SYSFilename;
			FCopy.pDestDev = (UDINT)&gStorePath;
			FCopy.pDest    = (UDINT)&SYSFilename2;
			FCopy.option   = FILE_FILE;
			/* Call FUB */
			FileCopy(&FCopy);
			/* Get FBK output information */
			wStatus = FCopy.status;
			/* Verify status */
			if (wStatus == 0)
			{
				state_Store_TimeWt = FILE_DELETE_EXISTING;
			}
			else if (wStatus != brBUSY)
			{
				if (wStatus == brFILENOTFOUND)
				{
					/* no old backup file so no problem GTF */
					brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.pcc");
					state_Store_TimeWt = FILE_DELETE_EXISTING;
				}
				else
				{
					/* finished with some other error GTF */
					state_Store_TimeWt = FILE_STORE_ERROR;
				}
			}
			break;
		case FILE_DELETE_EXISTING:
			/* delete old backup file GTF */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.pcc");
			FDelete.enable    = 1;
			FDelete.pDevice = (UDINT)&gStorePath;
			FDelete.pName   = (UDINT)&SYSFilename;
			/* Call FBK */
			FileDelete(&FDelete);
			/* Get status */
			wStatus = FDelete.status;
			if (wStatus == brNOERROR)
			{
				/* delete successful go on GTF */
				state_Store_TimeWt = FILE_STORE_CREATE;
				brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.pcc");
			}
			else if (wStatus == brFILENOTFOUND)
			{
				/* no old backup file so no problem GTF */
				state_Store_TimeWt = FILE_STORE_CREATE;
			}
			else if (wStatus != brBUSY)
				/* finished with some other error GTF */
				state_Store_TimeWt = FILE_STORE_ERROR;
			break;
		case FILE_STORE_CREATE: /**** Create file ****/
			brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.pcc");
			/* Initialize file create structure */
			TIMEFile_TypCreate.enable  = 1;
			TIMEFile_TypCreate.pDevice = (UDINT)&gStorePath;
			TIMEFile_TypCreate.pFile   = (UDINT)&SYSFilename;
			/* Call FUB */
			FileCreate(&TIMEFile_TypCreate);
			/* Get output information of FBK */
			wStatus = TIMEFile_TypCreate.status;
			/* Verify status */
			if (wStatus == brNOERROR)             /* all is well */
			{
				TIMEFileIDent = TIMEFile_TypCreate.ident;
				TIMEFile_offset = 0;
				state_Store_TimeWt = FILE_STORE_BUILD;  /* go to build data state */
			}
			else if (wStatus != brBUSY)    /* if 65535 (busy), else error state */
			{
				gErrorLevel = Def_ErrLvlCreate;
				state_Store_TimeWt = FILE_STORE_ERROR;
				if (wStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_BUILD:     /* build block to write */
			/* Initialize file write structure */
			brsstrcpy((UDINT)TIME_WT[gnum].header.structureName, (UDINT)"TimeWt");
			gatenum = (gnum + 'A');
			brsstrcat((UDINT)TIME_WT[gnum].header.structureName, (UDINT)&gatenum);
			brsstrcpy((UDINT)TIME_WT[gnum].header.VersionString, (UDINT)TIME_WT_VERSION);
			/* calculate the CRC16 of the structure... note that we are NOT     */
			/* including the CRC in the structure in the calculation (hence the */
			/* subtraction of the sizeof an unsigned short                      */
			TIME_WT[gnum].CRC16 = CRC16 ((unsigned char *) &(TIME_WT[gnum]),
				(sizeof (time_wt_line) - sizeof (unsigned int)));
			/*set FILEWRITE structure info GTF */
			TIMEFile_TypWrite.enable  = 1;
			TIMEFile_TypWrite.ident   = TIMEFileIDent;
			TIMEFile_TypWrite.offset  = TIMEFile_offset;
			TIMEFile_TypWrite.pSrc    = (unsigned long) &TIME_WT[gnum].header;
			TIMEFile_TypWrite.len     = sizeof(time_wt_line);
			/* write data to file  GTF */
			FileWrite(&TIMEFile_TypWrite);
			/* Verify status */
			wStatus = TIMEFile_TypWrite.status;
			if (wStatus == 0)
         {
				TIMEFile_offset += (TIMEFile_TypWrite.len + 1);
				state_Store_TimeWt = FILE_STORE_NEXT_GATE;
			}
         else if (wStatus != brBUSY)
         {
				ST_ErrorLevel = state_Store_TimeWt;
				ST_Error = wStatus;
				state_Store_TimeWt = FILE_STORE_ERROR;
			}
			break;      
		case FILE_STORE_NEXT_GATE:
			if (gnum >= (cfg_super.total_gates-1)) /*last gate GTF */
				state_Store_TimeWt = FILE_STORE_DONE;
			else
			{
				state_Store_TimeWt = FILE_STORE_BUILD;   /* go to build record state */
				gnum += 1;
			}
			break;
		case FILE_STORE_DONE:
			state_Store_TimeWt = FILE_STORE_CLOSE;
		case FILE_STORE_CLOSE:        /* close file */
			/* Initialize file close structure */
			TIMEFile_TypClose.enable   = 1;
			TIMEFile_TypClose.ident    = TIMEFile_TypWrite.ident;
			/* Call FBK */
			FileClose(&TIMEFile_TypClose);
			/* Get status */
			wStatus = TIMEFile_TypClose.status;
			/* Verify status */
			if (wStatus == brNOERROR)
         {
				state_Store_TimeWt = FILE_STORE_INIT;   /* reset state variable */
				file_stored=SUCCESS;
				result = SUCCESS;
				return(result);                        /* file written and closed return SUCCESS */
			}
			else if (wStatus != brBUSY)
			{
				gErrorLevel = Def_ErrLvlClose;
				state_Store_TimeWt = FILE_STORE_ERROR;
				if (wStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_ERROR:     /* error state */
			COUNTtimewt = 0;
			if (Signal.USB_store)
			{
   			/* if path 1 is active set to inactive and try path 2 */
   			/* if path 1 is inactive and still failure then path or process is invalid/failed */
   			if (gUSBPath1Active == TRUE)
   			{
      			gUSBPath1Active = FALSE;
      			state_Store_TimeWt = FILE_STORE_INIT;
      			result =  NOTDONE;
      			return(result);
   			} 
            else
   			   state_Store_TimeWt = FILE_STORE_INIT;   /* reset state variable */
   			result = FAIL;
   			return(result);
			}
			else
			   state_Store_TimeWt = FILE_STORE_INIT;
			result = FAIL;
			return(result);
			break;
		default:
			if (file_stored != SUCCESS)
				result = NOTDONE;
			return(result);   /* still storing current block or more blocks/files to store */
	}
	if (file_stored != SUCCESS)
		result = NOTDONE;
	return(result);   /* still storing current block or more blocks/files to store */
}

/************************************************************************/
/*
   Function:  Restore_TimeWt()

   Description: This function tries to open and load data from an existing
        time_wt data file. If the file does not exist, this will set clear
        gate cal to initial the time_wt table for that gate.

        This routine must run cyclically until all the data is read.

        It will be auto triggered off of Signal.Restore_Timewt
        It may be user triggered off of Signal.UserBackupTimeWt

   Returns: SUCCESS
            FAIL
            NOTDONE
                                                                        */
/************************************************************************/
int Restore_TimeWt(void)
{
	static int state_Restore_TimeWt = FILE_READ_INIT; /* static var for state function is in */
	static unsigned int rStatus;                     /* return status for writing to file */
	static unsigned int RT_ErrorLevel;
	static unsigned int RT_Error;
	static unsigned int line = 0;
	static int usbAction;
	int file_restored;
	int result;
	static int gnum;
	unsigned int myCRC;

	result = NOTDONE;
	COUNTtimewt += 1;
	if (COUNTtimewt > FILEFAILpoint)
		state_Restore_TimeWt = FILE_STORE_ERROR;

	switch (state_Restore_TimeWt)
	{
		case FILE_READ_INIT:    /* initialize parameters for processing store state machine */
			if (Signal.FILEIO_INUSE)
			{
				break;
			}
			else
			{
				COUNTtimewt = 0;
				file_restored = FAIL;
				gUSBPath1Active = TRUE;
				RT_ErrorLevel = 0;
				RT_Error = 0;
				usbAction = USB_GETNODELIST;
				state_Restore_TimeWt = FILE_READ_PATH;
				gnum = 0;
				Signal.FILEIO_INUSE = TRUE;
			}
		case FILE_READ_PATH:    /* initialize parameters for processing store state machine */
			/* if user signaled for storage look for USB GTF*/
			if (Signal.USB_store)
				if (usbAction != USB_DONE)
					USBresult = usb_check(&usbAction);  /* if the first usb path fails the code needs to switch to the second usb path */
				else
					state_Restore_TimeWt = FILE_READ_SEARCH;
			else /* assume sys disk is present GTF*/
			{
				brsstrcpy((UDINT)gStorePath, (DINT)SYSDrivePath);
				if (gnum == 0)
					state_Restore_TimeWt = FILE_READ_SEARCH;
				else
					state_Restore_TimeWt = FILE_READ_BLOCK;
			}
			break;
		case FILE_READ_SEARCH:
			state_Restore_TimeWt = FILE_READ_OPEN;   /* remove function this case until user store is written GTF */
			if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"bd0") == 0)
         {
				brsstrcpy((UDINT)gStorePath, (UDINT)USB1DrivePath);
				gUSBPath1Active = TRUE;
				state_Restore_TimeWt = FILE_READ_OPEN;
			}
         else if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"bd0") == 0)
         {
				brsstrcpy((UDINT)gStorePath, (UDINT)USB2DrivePath);
				gUSBPath1Active = FALSE;
				state_Restore_TimeWt = FILE_READ_OPEN;
			}
         else if (Signal.USB_store)
				state_Restore_TimeWt = FILE_READ_ERROR;   /* no usb mass storage device found */
			break;
		case FILE_READ_OPEN:        /* try to open existing file */
			/* Initialize file open structrue */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"TimeWt.pcc");
			TIMEFile_TypOpen.enable    = 1;
			TIMEFile_TypOpen.pDevice   = (unsigned long)&gStorePath;
			TIMEFile_TypOpen.pFile     = (unsigned long)&SYSFilename;
			TIMEFile_TypOpen.mode      = FILE_RW;   /* Read and write access */
			/* Call FUB */
			FileOpen(&TIMEFile_TypOpen);
			/* Get FBK output information */
			rStatus = TIMEFile_TypOpen.status;
			if (rStatus == brNOERROR)
         {                 /* all is well brNOERROR = 0*/
				TIMEFileIDent   = TIMEFile_TypOpen.ident;
				TIMEFile_offset = 0;
				TIMEFile_TypRead.offset  = 0;

				state_Restore_TimeWt = FILE_READ_BLOCK;  /* go to read data state */
			}
         else if (rStatus == brFILENOTFOUND)       /* file does not exist brFILENOTFOUND=20708*/
			{
				state_Restore_TimeWt = FILE_READ_NOTFOUND; /* file does not exist set clear gate cal */
				TIMEFile_TypWrite.offset = 0;
				line = 0;
			}
			else if (rStatus == brPATHNOTFOUND)       /* invalid file path/device brPATHNOTFOUND = 20709*/
				state_Restore_TimeWt = FILE_READ_ERROR;  /* in error routine setup to process path 2 or fail */
			else if (rStatus != brBUSY)               /* if 65535 (brBUSY), else error state brBUSY = 65535 */
			{
				gErrorLevel = Def_ErrLvlOpen;
				state_Restore_TimeWt = FILE_READ_ERROR;
				if (rStatus == brSystemError)          /* brSystemError = 20799 */
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_READ_BLOCK: /**** Read file ****/
			/* Initialize file create structure */
			TIMEFile_TypRead.enable  = 1;
			TIMEFile_TypRead.ident   = TIMEFileIDent;
			/*         TIMEFile_TypRead.offset  = (gnum *sizeof (time_wt_line)+ gnum); */
			TIMEFile_TypRead.pDest   = (unsigned long) &(TIME_WT[gnum]);
			TIMEFile_TypRead.len     = sizeof (time_wt_line);
			/* read time_wt[gnum] ffrom file GTF */
			FileRead(&TIMEFile_TypRead);
			/* Verify status */
			rStatus = TIMEFile_TypRead.status;
			if (rStatus == brNOERROR)             /* all is well */
			{
				myCRC = CRC16((unsigned char *)&(TIME_WT[gnum]),
					(sizeof(time_wt_line) - sizeof(unsigned int)));
				if (myCRC != TIME_WT[gnum].CRC16)
				{
					shrGATE[gnum].clear_gate_cal = TRUE;
					HMI.Load_status[gnum + GATE_A_tw_STATUS] = STRUCTURE_CRC_FAIL;
				}
				else
				{
					cfgGATE[gnum].minimum_gate_time = TIME_WT[gnum].entry[0].nominal_time;
					shrGATE[gnum].clear_gate_cal = FALSE;
					shrGATE[gnum].zero_calc_done = TRUE;
					HMI.Load_status[gnum + GATE_A_tw_STATUS] = STRUCTURE_LOADED;
				}
				state_Restore_TimeWt = FILE_STORE_NEXT_GATE;
			}
			else if (rStatus != brBUSY)    /* if 65535 (busy), else error state */
			{
				gErrorLevel = Def_ErrLvlCreate;
				state_Restore_TimeWt = FILE_STORE_ERROR;
				HMI.Load_status[gnum + GATE_A_tw_STATUS] = STRUCTURE_DEFAULT;
				if (rStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_NEXT_GATE:
			if (gnum >= (cfg_super.total_gates-1)) /*last gate GTF */
				state_Restore_TimeWt = FILE_STORE_DONE;
			else
			{
				state_Restore_TimeWt = FILE_READ_BLOCK;   /* go to build record state */
				TIMEFile_TypRead.offset  += TIMEFile_TypRead.len + 1;
				gnum += 1;
			}
			break;
		case FILE_STORE_DONE:
			state_Restore_TimeWt = FILE_STORE_CLOSE;
		case FILE_STORE_CLOSE:        /* close file */
			/* Initialize file close structure */
			TIMEFile_TypClose.enable   = 1;
			TIMEFile_TypClose.ident    = TIMEFileIDent;
			/* Call FBK */
			FileClose(&TIMEFile_TypClose);
			/* Get status */
			rStatus = TIMEFile_TypClose.status;
			/* Verify status */
			if (rStatus == brNOERROR)
         {
				state_Restore_TimeWt = FILE_STORE_INIT;   /* reset state variable */
				file_restored=SUCCESS;
				result = SUCCESS;
				return(result);                        /* file written and closed return SUCCESS */
			}
			else if (rStatus != brBUSY)
			{
				gErrorLevel = Def_ErrLvlClose;
				state_Restore_TimeWt = FILE_STORE_ERROR;
				if (rStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_ERROR:     /* error state */
			if (Signal.USB_store)
			{
				/* if path 1 is active set to inactive and try path 2 */
				/* if path 1 is inactive and still failure then path or process is invalid/failed */
				if (gUSBPath1Active == TRUE)
				{
					gUSBPath1Active = FALSE;
					state_Restore_TimeWt = FILE_STORE_INIT;
					result =  NOTDONE;
					return(result);
				} 
            else
					state_Restore_TimeWt = FILE_STORE_INIT;   /* reset state variable */
				result = FAIL;
				return(result);
			}
			else
				state_Restore_TimeWt = FILE_STORE_INIT;
			result = FAIL;
			return(result);
			break;
		case FILE_READ_ERROR:
			state_Restore_TimeWt = FILE_STORE_INIT;
			result = FAIL;
			return(result);
			break;
		default:
			if (file_restored != SUCCESS)
				result = NOTDONE;
			return(result);   /* still storing current block or more blocks/files to store */
	}
	if (file_restored != SUCCESS)
		result = NOTDONE;
	return(result);   /* still storing current block or more blocks/files to store */
}
/************************************************************************/
/*
   Function:  Store_recipe()

   Description: This function tries to open and save data to an existing
        configuration file. If the file does not exist, it is created. It
        will save data according to the set block size. If more than a
        block is needed another block will be created until all of the
        required data is saved. When finished the file will be closed.

        This routine must run cyclically until all the data is written.

        It will be auto triggered off of Signal.Store_Timewt
        It may be user triggered off of Signal.UserBackupTimeWt

   Returns: SUCCESS
            FAIL
            NOTDONE
                                                                        */
/************************************************************************/
int Store_Recipes(void)
{
	static int   state_Store_Recipes = FILE_STORE_INIT; /* static var for state function is in */
	static unsigned int wStatus;                       /* return status for writing to file */
	static unsigned int SR_ErrorLevel;
	static unsigned int SR_Error;
	static int usbAction;
	int file_stored;
	int result;

	COUNTrecipe += 1;
	if (COUNTrecipe > FILEFAILpoint)
		state_Store_Recipes = FILE_STORE_ERROR;
	result = NOTDONE;
	switch (state_Store_Recipes)
   {
		case FILE_STORE_INIT:    /* initialize parameters for processing store state machine */
			if (Signal.FILEIO_INUSE)
			{
				break;
			}
			else
			{
				COUNTrecipe = 0;
				file_stored = FAIL;
				gUSBPath1Active = TRUE;
				SR_Error = 0;
				usbAction = USB_GETNODELIST;
				state_Store_Recipes = FILE_STORE_PATH;
				Signal.FILEIO_INUSE = TRUE;
			}
		case FILE_STORE_PATH:    /* initialize parameters for processing store state machine */
			/* if user signaled for storage look for USB GTF*/

			if (Signal.USB_store)
         {
            USBresult = usb_check(&usbAction); 
				if (usbAction != USB_DONE)
					return NOTDONE;   //wait for usb to complete
				else
            {
					if (USBresult == SUCCESS)
					   state_Store_Recipes = FILE_STORE_SEARCH;
				   else
				   {
					   HMI.pg225_err_state = state_Store_Recipes;
					   HMI.pg225_err_code  = USBresult;
					   state_Store_Recipes = FILE_STORE_ERROR;
				   }
            }
         }
			else /* assume sys disk is present GTF*/
			{
				brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
				state_Store_Recipes = FILE_STORE_SEARCH;
			}
			break;
		case FILE_STORE_SEARCH:
			state_Store_Recipes = FILE_DELETE_BACKUP;
			if (Signal.USB_store)
			{
				if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"/bd0") == 0)
				{
					brsstrcpy((UDINT)gStorePath, (UDINT)USB1DrivePath);
					gUSBPath1Active = TRUE;
				}
				else if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"/bd16") == 0)
				{
					brsstrcpy((UDINT)gStorePath, (UDINT)USB2DrivePath);
					gUSBPath1Active = FALSE;
				}
				else
				{
					HMI.pg225_err_state = state_Store_Recipes;
					HMI.pg225_err_code  = 0;
					state_Store_Recipes = FILE_STORE_ERROR;   /* no usb mass storage device found */
				}
			}
			break;
		case FILE_DELETE_BACKUP:
			/* delete old backup file GTF */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.old");
			FDelete.enable    = 1;
			FDelete.pDevice = (unsigned long)&gStorePath;
			FDelete.pName   = (unsigned long)&SYSFilename;
			/* Call FBK */
			FileDelete(&FDelete);
			/* Get status */
			wStatus = FDelete.status;
			if (wStatus == brNOERROR)
			{
				/* delete successful go on GTF */
				brsstrcpy((UDINT)SYSFilename,  (UDINT)"Recipes.pcc");
				brsstrcpy((UDINT)SYSFilename2, (UDINT)"Recipes.old");
				state_Store_Recipes = FILE_STORE_COPY;
			}
			else if (wStatus != brBUSY)
			{
				if (wStatus == brFILENOTFOUND)
				{
					/* no old backup file so no problem GTF */
					brsstrcpy((UDINT)SYSFilename,  (UDINT)"Recipes.pcc");
					brsstrcpy((UDINT)SYSFilename2, (UDINT)"Recipes.old");
					state_Store_Recipes = FILE_DELETE_EXISTING;
					//state_Store_Recipes = FILE_STORE_COPY;
				}
				else
				{
					/* finished with some other error GTF */
					state_Store_Recipes = FILE_STORE_ERROR;
				}
			}
			break;
		case FILE_STORE_COPY:
			/* copy existing file to backup name GTF */
			FCopy.enable   = 1;
			FCopy.pSrcDev  = (unsigned long)&gStorePath;
			FCopy.pSrc     = (unsigned long)&SYSFilename;
			FCopy.pDestDev = (unsigned long)&gStorePath;
			FCopy.pDest    = (unsigned long)&SYSFilename2;
			FCopy.option   = FILE_FILE;
			/* Call FUB */
			FileCopy(&FCopy);
			/* Get FBK output information */
			wStatus = FCopy.status;
			/* Verify status */
			if (wStatus == 0)
			{
				state_Store_Recipes = FILE_DELETE_EXISTING;
			}
			else if (wStatus != brBUSY)
			{
				if (wStatus == brFILENOTFOUND)
				{
					/* no old backup file so no problem GTF */
					brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.pcc");
					state_Store_Recipes = FILE_DELETE_EXISTING;
				}
				else
				{
					/* finished with some other error GTF */
					state_Store_Recipes = FILE_STORE_ERROR;
				}
			}
			break;
		case FILE_DELETE_EXISTING:
			/* delete old backup file GTF */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.pcc");
			FDelete.enable    = 1;
			FDelete.pDevice = (unsigned long)&gStorePath;
			FDelete.pName   = (unsigned long)&SYSFilename;
			/* Call FBK */
			FileDelete(&FDelete);
			/* Get status */
			wStatus = FDelete.status;
			if (wStatus == brNOERROR)
			{
				/* delete successful go on GTF */
				state_Store_Recipes = FILE_STORE_CREATE;
				brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.pcc");
			}
			else if (wStatus == brFILENOTFOUND)
			{
				/* no old backup file so no problem GTF */
				state_Store_Recipes = FILE_STORE_CREATE;
			}
			else if (wStatus != brBUSY)
				/* finished with some other error GTF */
				state_Store_Recipes = FILE_STORE_ERROR;
			break;
		case FILE_STORE_CREATE: /**** Create file ****/
			brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.pcc");
			/* Initialize file create structure */
			RECIPEFile_TypCreate.enable  = 1;
			RECIPEFile_TypCreate.pDevice = (unsigned long)&gStorePath;
			RECIPEFile_TypCreate.pFile   = (unsigned long)&SYSFilename;
			/* Call FUB */
			FileCreate(&RECIPEFile_TypCreate);
			/* Get output information of FBK */
			wStatus = RECIPEFile_TypCreate.status;
			/* Verify status */
			if (wStatus == brNOERROR)           /* all is well */
			{
				RECIPEFileIDent = RECIPEFile_TypCreate.ident;
				RECIPEFile_offset = 0;
				state_Store_Recipes = FILE_STORE_BUILD;  /* go to build data state */
			}
			else if (wStatus != brBUSY)    /* if 65535 (busy), else error state */
			{
				gErrorLevel = Def_ErrLvlCreate;
				state_Store_Recipes = FILE_STORE_ERROR;
				if (wStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_BUILD:     /* build block to write */
			/* Initialize file write structure */
			brsstrcpy((UDINT)RECIPES->header.structureName, (UDINT)"Recipes");
			brsstrcpy((UDINT)RECIPES->header.VersionString, (UDINT)RECIPE_VERSION);
			/* calculate the CRC16 of the structure... note that we are NOT     */
			/* including the CRC in the structure in the calculation (hence the */
			/* subtraction of the sizeof an unsigned short                      */
			RECIPES->CRC16 = CRC16((unsigned char *)&(RECIPES), (sizeof(recipe_book) - sizeof(unsigned int)));
			/*set FILEWRITE structure info GTF */
			RECIPEFile_TypWrite.enable   = 1;
			RECIPEFile_TypWrite.ident    = RECIPEFileIDent;
			RECIPEFile_TypWrite.offset   = RECIPEFile_offset;
			RECIPEFile_TypWrite.pSrc     = (unsigned long)RECIPES;
			RECIPEFile_TypWrite.len      = sizeof(recipe_book);
			/* write header to file  */
			FileWrite(&RECIPEFile_TypWrite);
			/* Verify status */
			wStatus = RECIPEFile_TypWrite.status;
			if (wStatus == 0)
         {
				state_Store_Recipes = FILE_STORE_DONE;
			} 
         else if (wStatus != brBUSY)
         {
				SR_ErrorLevel = state_Store_Recipes;
				SR_Error = wStatus;
				state_Store_Recipes = FILE_STORE_ERROR;
			}
			break;
		case FILE_STORE_DONE:
			state_Store_Recipes = FILE_STORE_CLOSE;
		case FILE_STORE_CLOSE:        /* close file */
			/* Initialize file close structure */
			RECIPEFile_TypClose.enable   = 1;
			RECIPEFile_TypClose.ident    = RECIPEFile_TypWrite.ident;
			/* Call FBK */
			FileClose(&RECIPEFile_TypClose);
			/* Get status */
			wStatus = RECIPEFile_TypClose.status;
			/* Verify status */
			if (wStatus == brNOERROR)
         {
				state_Store_Recipes = FILE_STORE_INIT;   /* reset state variable */
				file_stored = SUCCESS;
				result = SUCCESS;
				return(result);                        /* file written and closed return SUCCESS */
			}
			else if (wStatus != brBUSY)
			{
				gErrorLevel = Def_ErrLvlClose;
				state_Store_Recipes = FILE_STORE_ERROR;
				if (wStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_ERROR:     /* error state */
			if (Signal.UserBackupRecipe)
			{
				/* if path 1 is active set to inactive and try path 2 */
				/* if path 1 is inactive and still failure then path or process is invalid/failed */
				if (gUSBPath1Active == TRUE)
				{
					gUSBPath1Active = FALSE;
					state_Store_Recipes = FILE_STORE_INIT;
					result = NOTDONE;
					return(result);
				} 
            else
					state_Store_Recipes = FILE_STORE_INIT;   /* reset state variable */
				result = FAIL;
				return(result);
			}
			else
				state_Store_Recipes = FILE_STORE_INIT;
			result = FAIL;
			return(result);
			break;
		default:
			if (file_stored != SUCCESS)
				result = NOTDONE;
			return(result);   /* still storing current block or more blocks/files to store */
	}
	if (file_stored != SUCCESS)
		result = NOTDONE;
	return(result);   /* still storing current block or more blocks/files to store */
}


/************************************************************************/
/*
   Function:  Restore_Recipe()

   Description: This function tries to open and load data from an existing
        time_wt data file. If the file does not exist, this will set clear
        gate cal to initial the time_wt table for that gate.

        This routine must run cyclically until all the data is read.

        It will be auto triggered off of Signal.Restore_Timewt
        It may be user triggered off of Signal.UserBackupTimeWt

   Returns: SUCCESS
            FAIL
            NOTDONE
                                                                        */
/************************************************************************/
int Restore_Recipes(void)
{
	static int state_Restore_Recipes = FILE_READ_INIT; /* static var for state function is in */
	static unsigned int rStatus;                     /* return status for writing to file */
	static unsigned int RR_ErrorLevel;
	static unsigned int RR_Error;
	static unsigned int line = 0;
	static int usbAction;
	int file_restored;
	int result;
	static int gnum;
	unsigned int myCRC;

	result = NOTDONE;
	COUNTrecipe +=1;
	if (COUNTrecipe > FILEFAILpoint)
		state_Restore_Recipes = FILE_STORE_ERROR;
	switch (state_Restore_Recipes)
	{
		case FILE_READ_INIT:    /* initialize parameters for processing store state machine */
			if (Signal.FILEIO_INUSE)
			{
				break;
			}
			else
			{
				COUNTrecipe = 0;
				file_restored = FAIL;
				gUSBPath1Active = TRUE;
				RR_ErrorLevel = 0;
				RR_Error = 0;
				usbAction = USB_GETNODELIST;
				state_Restore_Recipes = FILE_READ_PATH;
				gnum = 0;
				Signal.FILEIO_INUSE = TRUE;
			}
		case FILE_READ_PATH:    /* initialize parameters for processing store state machine */
			/* if user signaled for storage look for USB GTF*/
			if (Signal.USB_store)
				if (usbAction != USB_DONE)
					USBresult = usb_check(&usbAction);  /* if the first usb path fails the code needs to switch to the second usb path */
				else
					state_Restore_Recipes = FILE_READ_SEARCH;
			else /* assume sys disk is present GTF*/
			{
				brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
				if (gnum == 0)
					state_Restore_Recipes = FILE_READ_OPEN;
				else
					state_Restore_Recipes = FILE_READ_BLOCK;
			}
			break;
		case FILE_READ_SEARCH:
			state_Restore_Recipes = FILE_READ_OPEN;   /* remove function this case until user store is written GTF */
			if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"bd0") == 0)
         {
				brsstrcpy((UDINT)gStorePath, (UDINT)USB1DrivePath);
				gUSBPath1Active = TRUE;
				state_Restore_Recipes = FILE_READ_OPEN;
			}
         else if (brsstrcmp((UDINT)usbDevice.ifName, (UDINT)"bd0") == 0)
         {
				brsstrcpy((UDINT)gStorePath, (UDINT)USB2DrivePath);
				gUSBPath1Active = FALSE;
				state_Restore_Recipes = FILE_READ_OPEN;
			}
         else 
			{
				HMI.pg225_err_state   = state_Restore_Recipes;
				HMI.pg225_err_code    = USBFAIL;
				state_Restore_Recipes = FILE_READ_ERROR;   /* no usb mass storage device found */
			}
			break;
		case FILE_READ_OPEN:        /* try to open existing file */
			/* Initialize file open structrue */
			brsstrcpy((UDINT)SYSFilename, (UDINT)"Recipes.pcc");
			RECIPEFile_TypOpen.enable    = 1;
			RECIPEFile_TypOpen.pDevice   = (unsigned long) &gStorePath;
			RECIPEFile_TypOpen.pFile     = (unsigned long) &SYSFilename;
			RECIPEFile_TypOpen.mode      = FILE_RW;   /* Read and write access */
			/* Call FUB */
			FileOpen(&RECIPEFile_TypOpen);
			/* Get FBK output information */
			rStatus = RECIPEFile_TypOpen.status;
			if (rStatus == brNOERROR)
         {                 /* all is well brNOERROR = 0*/
				RECIPEFileIDent   = RECIPEFile_TypOpen.ident;
				RECIPEFile_offset = 0;
				RECIPEFile_TypRead.offset  = 0;
				state_Restore_Recipes = FILE_READ_BLOCK;  /* go to read data state */
			}
         else if (rStatus == brFILENOTFOUND)       /* file does not exist brFILENOTFOUND=20708*/
			{
				state_Restore_Recipes = FILE_READ_NOTFOUND; /* file does not exist set clear gate cal */
				RECIPEFile_TypWrite.offset = 0;
				line = 0;
			}
			else if (rStatus == brPATHNOTFOUND)       /* invalid file path/device brPATHNOTFOUND = 20709*/
			{
				HMI.pg225_err_state = state_Restore_Recipes;
				HMI.pg225_err_code = rStatus;
				state_Restore_Recipes = FILE_READ_ERROR;  /* in error routine setup to process path 2 or fail */
			}
			else if (rStatus != brBUSY)               /* if 65535 (brBUSY), else error state brBUSY = 65535 */
			{
				gErrorLevel = Def_ErrLvlOpen;
				HMI.pg225_err_state = state_Restore_Recipes;
				HMI.pg225_err_code = rStatus;
				state_Restore_Recipes = FILE_READ_ERROR;
				if (rStatus == brSystemError)          /* brSystemError = 20799 */
					gsysError = FileIoGetSysError();
			}
			break;

		case FILE_READ_BLOCK: /**** Read file ****/
			/* Initialize file create structure */
			RECIPEFile_TypRead.enable  = 1;
			RECIPEFile_TypRead.ident   = RECIPEFileIDent;
			RECIPEFile_TypRead.pDest   = (unsigned long)RECIPES;
			RECIPEFile_TypRead.len     = sizeof(recipe_book);
			FileRead(&RECIPEFile_TypRead);
			/* Verify status */
			rStatus = RECIPEFile_TypRead.status;
			if (rStatus == brNOERROR)             /* all is well */
			{
				myCRC = CRC16((unsigned char *)&(RECIPES),
					(sizeof(recipe_book) - sizeof(unsigned int)));
				if (myCRC != RECIPES->CRC16)
				{
					state_Restore_Recipes = FILE_STORE_DONE;
					RR_ErrorLevel = state_Restore_Recipes;
					RR_Error = STRUCTURE_CRC_FAIL;
				}
				else
				{
					state_Restore_Recipes = FILE_STORE_DONE;
				}
			}
			else if (rStatus != brBUSY)    /* if 65535 (busy), else error state */
			{
				gErrorLevel = Def_ErrLvlCreate;
				state_Restore_Recipes = FILE_STORE_ERROR;
				if (rStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_DONE:
			state_Restore_Recipes = FILE_STORE_CLOSE;
		case FILE_STORE_CLOSE:        /* close file */
			/* Initialize file close structure */
			RECIPEFile_TypClose.enable   = 1;
			RECIPEFile_TypClose.ident    = RECIPEFileIDent;
			/* Call FBK */
			FileClose(&RECIPEFile_TypClose);
			/* Get status */
			rStatus = RECIPEFile_TypClose.status;
			/* Verify status */
			if (rStatus == brNOERROR)
         {
				state_Restore_Recipes = FILE_STORE_INIT;   /* reset state variable */
				file_restored=SUCCESS;
				result = SUCCESS;
            HMI.pg225_FailCode = 0;
				return(result);                        /* file written and closed return SUCCESS */
			}
			else if (rStatus != brBUSY)
			{
				gErrorLevel = Def_ErrLvlClose;
				state_Restore_Recipes = FILE_STORE_ERROR;
				if (rStatus == brSystemError)
					gsysError = FileIoGetSysError();
			}
			break;
		case FILE_STORE_ERROR:     /* error state */
			if (Signal.UserBackupRecipe)
			{
				/* if path 1 is active set to inactive and try path 2 */
				/* if path 1 is inactive and still failure then path or process is invalid/failed */
				if (gUSBPath1Active == TRUE)
				{
					gUSBPath1Active = FALSE;
					state_Restore_Recipes = FILE_STORE_INIT;
					result = NOTDONE;
					return(result);
				} 
            else
            {
					state_Restore_Recipes = FILE_STORE_INIT;   /* reset state variable */
               HMI.pg225_FailCode    = 1001;
            }
				result = FAIL;
				return(result);
			}
			else
         {
            HMI.pg225_FailCode    = 1002;
				state_Restore_Recipes = FILE_STORE_INIT;
         }
			result = FAIL;
			return(result);
			break;
		case FILE_READ_ERROR:
			state_Restore_Recipes = FILE_STORE_INIT;
         HMI.pg225_FailCode    = 1003;
			result = FAIL;
			return(result);
			break;
		default:
			if (file_restored != SUCCESS)
				result = NOTDONE;
			return(result);   /* still storing current block or more blocks/files to store */
	}
	if (file_restored != SUCCESS)
		result = NOTDONE;
	return(result);   /* still storing current block or more blocks/files to store */
}

/************************************************************************/
/*
   Function:  Restore_Config()

   Description: This function will retrieve the configuration from the cf
        card and parse it. The parsed variables will be updated in memory.

        Returns:  SUCCESS
                  FAIL
                  NOTDONE
                                                                        */
/************************************************************************/
int Restore_Config(char * pFname, char * pData, unsigned long size)
{
   static int state_Read_Config = FILE_READ_INIT; /* static var for state function is in */
   static unsigned int rStatus;                              /* return status for writing to file */
   static unsigned int RC_ErrorLevel;
   static unsigned int RC_Error;
   static int usbAction;
   int file_restored;
   int result;
   unsigned int myCRC;

   gstate_Restore_Config = state_Read_Config;
   result = NOTDONE;
   COUNTconfig += 1;
   if(COUNTconfig > FILEFAILpoint)
      state_Read_Config = FILE_STORE_ERROR;
   switch (state_Read_Config)
   {
      case FILE_READ_INIT:    /* initialize parameters for processing store state machine */
         if(Signal.FILEIO_INUSE)
         {
            break;
         }
         else
         {
            COUNTconfig = 0;
            file_restored = FAIL;
            gUSBPath1Active = TRUE;
            RC_ErrorLevel = 0;
            RC_Error = 0;
            HMI.pg225_err_state = 0;
            HMI.pg225_err_code = 0;
            usbAction = USB_GETNODELIST;
            state_Read_Config = FILE_READ_PATH;
            Signal.FILEIO_INUSE = TRUE;
         }

      case FILE_READ_PATH:    /* initialize parameters for processing read state machine */
         /* if user signaled for storage GTF*/
         if (Signal.USB_store)
         {
            if (usbAction != USB_DONE)
               USBresult = usb_check(&usbAction);      /* if the first usb path fails the code needs to switch to the second usb path */
            else if(USBresult == SUCCESS)
            {
               usbAction         = USB_DEVICELINK;
               state_Read_Config = FILE_READ_SEARCH;
            }
            else
            {
               HMI.pg225_err_state = state_Read_Config;
               HMI.pg225_err_code  = USBresult;
               state_Read_Config   = FILE_READ_ERROR;
            }
         }
         else /* assume sys disk is present GTF*/
         {
            brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
            state_Read_Config = FILE_READ_OPEN;
         }
         break;

      case FILE_READ_SEARCH:
         if (Signal.USB_store)
         {
            if (usbAction != USB_DONE)
               USBresult = usb_check (&usbAction);      /* if the first usb path fails the code needs to switch to the second usb path */
            else if (USBresult == SUCCESS)
            {
               //USB disk is present AND linked > use file_rw
               brsstrcpy((UDINT)gStorePath, (UDINT)MSDEVICE); //define device (usb) for the file mgr
               state_Read_Config = FILE_READ_OPEN; 
            }
            else
            {
               // USB has failed lets write to disk
               HMI.pg225_err_state = state_Read_Config;
               HMI.pg225_err_code  = USBresult;
               state_Read_Config   = FILE_READ_ERROR;
            }
         }
         else
            state_Read_Config = FILE_READ_OPEN;         
         break;

      case FILE_READ_OPEN:        /* try to open existing file */
         /* Initialize file open structrue */
         CFGFile_TypOpen.enable    = 1;
         CFGFile_TypOpen.pDevice   = (unsigned long) &gStorePath;
         CFGFile_TypOpen.pFile     = (unsigned long) pFname;
         CFGFile_TypOpen.mode      = FILE_RW;   /* Read and write access */
         /* Call FUB */
         FileOpen(&CFGFile_TypOpen);
         /* Get FBK output information */
         rStatus = CFGFile_TypOpen.status;
         if (rStatus == brNOERROR)                /* all is well brNOERROR = 0*/
         {  
            gFileIDent = CFGFile_TypOpen.ident;
            state_Read_Config = FILE_READ_BLOCK;            /* go to read the data */
         }
         else if (rStatus == brFILENOTFOUND)       /* file does not exist brFILENOTFOUND=20708*/
         {
            HMI.pg225_err_state = state_Read_Config;
            HMI.pg225_err_code = rStatus;
            state_Read_Config = FILE_READ_NOTFOUND;        /* cannot read from current device */
         }
         else if (rStatus == brPATHNOTFOUND)       /* invalid file path/device brPATHNOTFOUND = 20709*/
         {
            HMI.pg225_err_state = state_Read_Config;
            HMI.pg225_err_code = rStatus;
            state_Read_Config = FILE_READ_ERROR;           /* in error routine setup to process path 2 or fail */
         }
         else if (rStatus != brBUSY)               /* if 65535 (brBUSY), else error state brBUSY = 65535 */
         {
            HMI.pg225_err_state = state_Read_Config;
            HMI.pg225_err_code = rStatus;
            state_Read_Config = FILE_READ_ERROR;
            if (rStatus == brSystemError)          /* brSystemError = 20799 */
               gsysError = FileIoGetSysError();
         }
         break;
      case FILE_READ_BLOCK:
         /* Initialize file write structure */
         CFGFile_TypRead.enable   = 1;
         CFGFile_TypRead.ident    = gFileIDent;
         /* Point to System Config structure */
         CFGFile_TypRead.pDest     = (unsigned long) pData;
         CFGFile_TypRead.len      = size;
         /* write System Config to file  */
         FileRead(&CFGFile_TypRead);
         /* Verify status */
         rStatus = CFGFile_TypRead.status;
         if (rStatus == 0)
         {
            unsigned int   crc=   0;

            myCRC = CRC16 ((unsigned char *) pData, size - sizeof (unsigned int));
            crc   = *((unsigned int *)(pData + (size - sizeof(unsigned int))));
            if (myCRC != crc)
            {
               HMI.pg225_err_state = state_Read_Config;
               HMI.pg225_err_code = STRUCTURE_CRC_FAIL;
            }
            state_Read_Config = FILE_READ_DONE;
         }
         else if (rStatus != brBUSY)
         {
            HMI.pg225_err_state = state_Read_Config;
            HMI.pg225_err_code = rStatus;
            state_Read_Config = FILE_READ_DONE;
         }
         break;

      case FILE_READ_DONE:
         state_Read_Config = FILE_READ_CLOSE;

      case FILE_READ_CLOSE:        /* close file */
         /* Initialize file close structure */
         CFGFile_TypClose.enable   = 1;
         CFGFile_TypClose.ident    = gFileIDent;
         /* Call FBK */
         FileClose(&CFGFile_TypClose);
         /* Get status */
         rStatus = CFGFile_TypClose.status;
         /* Verify status */
         if (rStatus == brNOERROR){
            state_Read_Config = FILE_READ_INIT;   /* reset state variable */
            file_restored = SUCCESS;
            result = SUCCESS;
            Signal.FILEIO_INUSE = FALSE;
            state_Read_Config = FILE_READ_INIT;
            return(result);                        /* file written and closed return SUCCESS */
         }
         else if (rStatus != brBUSY)
         {
            gErrorLevel = Def_ErrLvlClose;
            HMI.pg225_err_state = state_Read_Config;
            HMI.pg225_err_code = rStatus;
            state_Read_Config = FILE_READ_ERROR;
            if (rStatus == brSystemError)
               gsysError = FileIoGetSysError();
         }
         break;
      case FILE_READ_NOTFOUND:     /* error state */
         result = FAIL;
         return(result);
         break;
      case FILE_READ_ERROR:     /* error state */
         if (Signal.UserRetrieveConfig)
         {
            /* if path 1 is active set to inactive and try path 2 */
            /* if path 1 is inactive and still failure then path or process is invalid/failed */
            if (gUSBPath1Active == TRUE)
            {
               gUSBPath1Active = FALSE;
               state_Read_Config = FILE_READ_OPEN;
               result = NOTDONE;
               return(result);
            }
            else
               state_Read_Config = FILE_READ_INIT;   /* reset state variable */
            result = FAIL;
            return(result);
         }
         else
         {
            state_Read_Config = FILE_READ_INIT;   /* reset state variable */
            result = FAIL;
            return(result);
         }
         break;
      default:
         if(file_restored != SUCCESS)
            result = NOTDONE;
         return(result);   /* still storing current block or more blocks/files to store */
   }
   if(file_restored != SUCCESS)
      result = NOTDONE;
   return(result);   /* still storing current block or more blocks/files to store */
}


/************************************************************************/
/*
   Function:  Store_Config()

   Description: This function tries to open and save data to an existing
        configuration file. If the file does not exist, it is created. It
        will save data according to the set block size. If more than a
        block is needed another block will be created until all of the
        required data is saved. When finished the file will be closed.

        This routine must run cyclically until all the data is written.

   Returns: SUCCESS
            FAIL
            NOTDONE
                                                                        */
/************************************************************************/
int Store_Config(char * pFname, char * pData, unsigned long size)
{   
   static int   state_Store_Config = FILE_STORE_INIT; /* static var for state function is in */
   static unsigned int wStatus;                              /* return status for writing to file */
   static unsigned int SC_Error;
   static int usbAction;
   int file_stored;
   int result;

   gstate_Store_Config = state_Store_Config;
   result = NOTDONE;
   COUNTconfig += 1;
   if(COUNTconfig > FILEFAILpoint)
      state_Store_Config = FILE_STORE_ERROR;
   HMI.pg225_err_state = FALSE;
   HMI.pg225_err_code = FALSE;
   switch (state_Store_Config)
   {
      case FILE_STORE_INIT:    /* initialize parameters for processing store state machine */
         if(Signal.FILEIO_INUSE)
         {
            break;
         }
         else
         {
            COUNTconfig = 0;
            file_stored = FAIL;
            gUSBPath1Active = TRUE;
            SC_Error = 0;
            usbAction = USB_GETNODELIST;
            state_Store_Config = FILE_STORE_PATH;
            Signal.FILEIO_INUSE = TRUE;
         }

      case FILE_STORE_PATH:    /* initialize parameters for processing store state machine */
         /* if user signaled for storage look for USB GTF*/
         if (Signal.USB_store)
         {
            if (usbAction != USB_DONE)
               USBresult = usb_check (&usbAction);      /* if the first usb path fails the code needs to switch to the second usb path */
            else if(USBresult == SUCCESS)
            {
               usbAction       = USB_DEVICELINK;
               state_Store_Config = FILE_STORE_SEARCH;
            }
            else
            {
               HMI.pg225_err_state = state_Store_Config;
               HMI.pg225_err_code = USBresult;
               state_Store_Config = FILE_STORE_ERROR;
            }
         }
         else /* assume sys disk is present GTF*/
         {
            brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
            state_Store_Config = FILE_STORE_SEARCH;
         }
         break;

      case FILE_STORE_SEARCH:
         if (Signal.USB_store)
         {
            if (usbAction != USB_DONE)
               USBresult = usb_check (&usbAction);      /* if the first usb path fails the code needs to switch to the second usb path */
            else if (USBresult == SUCCESS)
            {
               //USB disk is present AND linked > use file_rw
               brsstrcpy((UDINT)gStorePath, (UDINT)MSDEVICE); //define device (usb) for the file mgr
               state_Store_Config = FILE_DELETE_EXISTING; 
            }
            else
            {
               // USB has failed lets write to disk
               brsstrcpy((UDINT)gStorePath, (UDINT)SYSDrivePath);
               state_Store_Config = FILE_DELETE_EXISTING;
            }
         }
         else
            state_Store_Config = FILE_DELETE_EXISTING;         
         break;

      case FILE_DELETE_EXISTING:
         /* delete old backup file GTF */
         FDelete.enable    = 1;
         FDelete.pDevice = (unsigned long) &gStorePath;
         FDelete.pName   = (unsigned long) pFname;
         /* Call FBK */
         FileDelete(&FDelete);
         /* Get status */
         wStatus = FDelete.status;
         if(wStatus == brNOERROR)
         {
            /* delete successful go on GTF */
            state_Store_Config = FILE_STORE_CREATE;
         }
         else if (wStatus == brFILENOTFOUND)
         {
            /* no old backup file so no problem GTF */
            state_Store_Config = FILE_STORE_CREATE;
         }
         else if (wStatus != brBUSY)
            /* finished with some other error GTF */
            state_Store_Config = FILE_STORE_ERROR;
         break;

      case FILE_STORE_CREATE: /**** Create file ****/
         /* Initialize file create structure */
         CFGFile_TypCreate.enable  = 1;
         CFGFile_TypCreate.pDevice = (unsigned long) &gStorePath;
         CFGFile_TypCreate.pFile   = (unsigned long) pFname;
         /* Call FUB */
         FileCreate(&CFGFile_TypCreate);
         /* Get output information of FBK */
         gFileIDent = CFGFile_TypCreate.ident;
         wStatus = CFGFile_TypCreate.status;
         /* Verify status */
         if (wStatus == brNOERROR)             /* all is well */
            state_Store_Config = FILE_STORE_BUILD;     /* got to build record state */
         else if (wStatus != brBUSY)    /* if 65535 (busy), else error state */
         {
            gErrorLevel = Def_ErrLvlCreate;
            if (wStatus == brSystemError)
               gsysError = FileIoGetSysError();
            HMI.pg225_err_state = state_Store_Config;
            HMI.pg225_err_code = gsysError;
            state_Store_Config = FILE_STORE_ERROR;
         }
         break;

      case FILE_STORE_BUILD:     /* build block to write */
         /* Initialize file write structure */
         state_Store_Config = FILE_STORE_WRITE;    /* write data state */

      case FILE_STORE_WRITE:
         /* Initialize file write structure */
         CFGFile_TypWrite.enable   = 1;
         CFGFile_TypWrite.ident    = gFileIDent;
         CFGFile_TypWrite.offset   = 0;
         /*  Point to mixer config structure */
         CFGFile_TypWrite.pSrc     = (unsigned long) pData;
         CFGFile_TypWrite.len      = size;
         /* calculate the CRC16 of the structure... note that we are NOT
          * including the CRC in the structure in the calculation (hence the
          * subtraction of the sizeof an unsigned short
          */
         *((unsigned int *)(pData + (size - sizeof(unsigned int))))=             //  store CRC at the end of the buffer
            CRC16 ((unsigned char *) pData, size - sizeof (unsigned int));        //  make sure to leave space for an unsigned int at the end of the buffer!
         FileWrite(&CFGFile_TypWrite);
         /* Verify status */
         wStatus = CFGFile_TypWrite.status;
         if (wStatus == 0)
         {
            state_Store_Config = FILE_STORE_DONE;
         } 
         else if (wStatus != brBUSY)
         {
            HMI.pg225_err_state = state_Store_Config;
            HMI.pg225_err_code  = wStatus;
            state_Store_Config  = FILE_STORE_ERROR;
         }
         break;

      case FILE_STORE_DONE:
         state_Store_Config = FILE_STORE_CLOSE;

      case FILE_STORE_CLOSE:        /* close file */
         /* Initialize file close structure */
         CFGFile_TypClose.enable   = 1;
         CFGFile_TypClose.ident    = gFileIDent;
         /* Call FBK */
         FileClose(&CFGFile_TypClose);
         /* Get status */
         wStatus = CFGFile_TypClose.status;
         /* Verify status */
         if (wStatus == brNOERROR)
         {
            state_Store_Config = FILE_STORE_INIT;   /* reset state variable */
            Signal.FILEIO_INUSE = FALSE;
            file_stored=SUCCESS;
            result = SUCCESS;
            return(result);                        /* file written and closed return SUCCESS */
         }
         else if (wStatus != brBUSY)
         {
            gErrorLevel = Def_ErrLvlClose;

            if (wStatus == brSystemError)
               gsysError = FileIoGetSysError();
            HMI.pg225_err_state = state_Store_Config;
            HMI.pg225_err_code = gsysError;
            state_Store_Config = FILE_STORE_ERROR;
         }
         break;
      case FILE_STORE_ERROR:     /* error state */
         if (Signal.USB_store)
         {
            /* if path 1 is active set to inactive and try path 2 */
            /* if path 1 is inactive and still failure then path or process is invalid/failed */
            if (gUSBPath1Active == TRUE)
            {
               gUSBPath1Active = FALSE;
               state_Store_Config = FILE_STORE_OPEN;
               result =  NOTDONE;
               return(result);
            } 
            else
               state_Store_Config = FILE_STORE_INIT;   /* reset state variable */
            COUNTconfig = 0;
            result = FAIL;
            return(result);
         }
         else
            state_Store_Config = FILE_STORE_INIT;
         COUNTconfig = 0;
         result = FAIL;
         return(result);
         break;
      default:
         if (file_stored != SUCCESS)
            result = NOTDONE;
         return(result);   /* still storing current block or more blocks/files to store */
   }
   if (file_stored != SUCCESS)
      result = NOTDONE;
   return(result);   /* still storing current block or more blocks/files to store */
}
