#include <stdio.h>
#include "pcc_math.h"
#include "math.h"

float fpercent_error(float a,float b)
{
  float percent;

  if (a==b)
     percent = 0.0;   /* Both the same number */
  else
    {if ((b==0.0)||(a==0.0))
        percent = 1.0;  /* Trivial case */
     else
        percent = (a-b)/b;    /* DIVIDE */
    }

  return (percent);
}


