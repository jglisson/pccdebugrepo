#include <stdio.h>
#include "pcc_math.h"
#include "math.h"

signed short add_isample(isamples *s_data, signed short xdata, signed short ydata)
{
    ordered_ipair *d;

    d = s_data->pts;

    d[s_data->next].x = xdata;
    d[s_data->next].y = ydata;
    if(++s_data->next >= s_data->length) s_data->next = 0;
    ++s_data->num_taken;

    return(0);
 }


