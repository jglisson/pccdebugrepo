/************************************************************************/
/*
   File:  exd_drive.h

   Description: This file is the low level driver defintion file for the
         exd (c1200) type interfaces.

      $Log:   F:\Software\BR_Guardian\exd_drive.h_v  $
 *
 *    Rev 1.3   Jul 16 2008 09:15:04   vidya
 * Added SCP_GETWEIGHT.
 *
 *
 *    Rev 1.2   Mar 27 2008 08:46:00   FMK
 *
 *
 *    Rev 1.1   Mar 07 2008 10:28:52   FMK
 * Changed reference of wtp_mode and ltp_mode.
 *
 *
 *    Rev 1.0   Feb 22 2008 10:42:16   FMK
 * Initial version. These are the lower level driver
 * files for controlling the output to drive systems.

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#define EXD_DRIVE    0
#define HO_DRIVE     1
#define SHO_DRIVE    2

#define RAMPNONE     0
#define RAMPACCEL    1
#define RAMPDECEL    2

#define DRIVEINH_TIMEOUT 5

#define Bit_ExtDrv_Ctrl  0x01
#define Bit_IntDrv_Ctrl  0x02

/* drive specific status bits definition */
#define SCP_BIT_DriveRamp  0x10
#define SCP_BIT_DriveInh   0x20
#define SCP_BIT_DriveMan   0x40
#define SCP_BIT_DriveADC   0x80

/* smart card protocol command defintions */
#define SCP_GETWEIGHT    0x40
#define SCP_SETACCEL     0x45
#define SCP_SETDECEL     0x46
#define SCP_SETSPEED     0x43
#define SCP_GETSPEED     0x44
#define SCP_GETPULSES    0x49
#define SCP_EMERGENCY    0x24
#define SCP_ACK          0x06
#define SCP_NACK         0x15
#define SCP_RESET        0x20
#define SCP_ID           0x27



