#include <stdio.h>
#include "pcc_math.h"
#include "math.h"
#include <AsBrStr.h>

signed short remove_isample(isamples *s_data, signed short index)
{
    ordered_ipair *start;
    unsigned long len;

    if (index < s_data->length)
    {
        /*start = s_data->pts + index;*/

        len = (unsigned long)((s_data->length - index - 1) * sizeof(int));
        (void)brsmemmove((UDINT)start, (UDINT)(start+1), (UDINT)len);

        if (--s_data->next < 0) 
           s_data->next = 0;
        --s_data->num_taken;
    }
    return(0);

}


