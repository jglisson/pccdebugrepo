#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  security.c

   Description: This file is used to control user security with regards
      to the visualization.


      $Log:   F:\Software\BR_Guardian\security.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.security to toggle debugging information in this file
 *
 *    Rev 1.4   Sep 15 2008 13:21:00   gtf
 * Fixed debug layer visibility
 *
 *    Rev 1.3   Aug 11 2008 12:29:22   gtf
 * Added code to display debug layer only in service mode
 *
 *    Rev 1.2   Jan 28 2008 13:52:58   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.1   Jan 18 2008 09:06:44   FMK
 * Changed global variable names to account for
 * new naming convention.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <string.h>
#include "sys_lib.h"
#include "mem.h"
#include "sys.h"
#include "signal_pcc.h"
#include "hmi.h"
#include "libbgdbg.h"
#include "timer.h"
//#include "security.h"

#define BACKDOOR_CODE  "852456"       
#define LANG_BAR_CODE  "852457"
#define TWTABLE_CODE   "852458"
#define TPEYTON_CODE   "193750"       

extern void SetSecurity();  /* in interface.h */

void Security_Main();

RTCtime_typ gRTCStartTimeSecurity, gRTCNextTimeSecurity;  /* elapsed time variables for status popup */
extern void visible(unsigned char *Runtime,unsigned char state);

extern struct PopupActiveStruct Popup; /* make sure popups only for security are allowed */

/************************************************************************/
/*
   Function:  Security_Main()

   Description:  watches the HMI for the security page and appropriately calls
                 the correct page setup routines.

   Action_Input0 is for submitting a new passcode
   Action_Input1 is for resetting to minimum security level
   Action_Input2 is for manually clearing the change password status visibility

   Popup_Runtime1 is to show the password was accepted
   Popup_Runtime2 is to show the password was denied

************************************************************************/
void Security_Main()
{
   int  i;
   UINT status;
   static int language_toggle;
   
   config_super *cs = &cfg_super;
   
   if (Popup.Security == FALSE)
   {
      visible(&HMI.PopupRuntime1,FALSE);
      visible(&HMI.PopupRuntime2,FALSE);
      visible(&HMI.PopupRuntime3,FALSE);
      visible(&HMI.PopupRuntime4,FALSE);
      visible(&HMI.PopupRuntime5,FALSE);
   }

   if (HMI.Action_Input0 == gcActive) /* new passcode was submitted */
   {
       if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)LANG_BAR_CODE))
       {   /* Toggle the variable value */
          cs->current_security_level = SERVICE_LOCK;
          if(!language_toggle)
          {
             visible(&HMI.PopupRuntime1,TRUE);
             visible(&HMI.pg000_Lang_Layer_On,  TRUE); /* Turn on language layer GTF */
             visible(&HMI.pg000_Debug_Layer_On, TRUE); /* Turn on debug layer  GTF */
             language_toggle = TRUE;
          }
          else
          {
             visible(&HMI.PopupRuntime1,FALSE);
             visible(&HMI.pg000_Lang_Layer_On,  FALSE); /* Turn on language layer GTF */
             visible(&HMI.pg000_Debug_Layer_On, FALSE); /* Turn on debug layer  GTF */
             language_toggle = TRUE;
          }
       }
       else if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)BACKDOOR_CODE))
       {
          cfg_super.EnteredPassword = start_time();
          cs->current_security_level = SERVICE_LOCK;
          visible(&HMI.PopupRuntime1, TRUE);
       }
       else if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)TWTABLE_CODE))
       {  /* Toggle the variable value */
          Signal.display_timewt = !Signal.display_timewt;
       }
       else if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)cs->supervisor_code))
       {
          cfg_super.EnteredPassword = start_time();
          cs->current_security_level = SUPERVISOR_LOCK; 
          visible(&HMI.PopupRuntime1, TRUE);
       }
       else if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)cs->maintenance_code))
       {
          cs->current_security_level = MAINTENANCE_LOCK;
          visible(&HMI.PopupRuntime1,TRUE);
       }
       else if (!brsstrcmp((UDINT)HMI.input_password, (UDINT)cs->operator_code))
       {
          cs->current_security_level = OPERATOR_LOCK;
          visible(&HMI.PopupRuntime1, TRUE);
       }
       else    /* invalid password */
       {
          visible(&HMI.PopupRuntime2, TRUE);
       }
       HMI.Action_Input0 = gcInActive;     /* clear button press status */
       status = RTC_gettime(&gRTCStartTimeSecurity);        /* get current time and store it into time struct */
       Popup.Security = TRUE;
       SetSecurity();  /* Visibility might change as a result of security change */
       for (i=0; i<gPasswordLength; i++)   /* clear password on sucessful entry GTF */
          HMI.input_password[i] = '\0';
   }
   else
   {
      /* do nothing */
   }
   if (HMI.Action_Input1 == gcActive)  /* reset to mininum security level command */
   {
      for (i=0; i<gPasswordLength; i++)
         HMI.input_password[i] = '\0';

      cs->current_security_level = cs->minimum_security_level;
      HMI.Action_Input1 = gcInActive;  /* clear button press status */
      visible(&HMI.PopupRuntime1, TRUE);  /* password of course accepted show popup */
      Popup.Security = TRUE;
      status = RTC_gettime(&gRTCStartTimeSecurity);        /* get current time and store it into time struct */
      SetSecurity();  /* Visibility might change as a result of security change */
   }
   else
   {
      /* do nothing */
   }
   /* if popup is showing calculate for how long. */
   if ((HMI.PopupRuntime1 == 0) || (HMI.PopupRuntime2 == 0))  /* if popup is showing, check time to make invisible */
   {
      status = RTC_gettime(&gRTCNextTimeSecurity);        /* get current time and store it into time struct */
      HMI.PopupElapTime = 0;
      if (gRTCNextTimeSecurity.second >= gRTCStartTimeSecurity.second)  /* calculate elapsed seconds the popup has shown */
          HMI.PopupElapTime = gRTCNextTimeSecurity.second - gRTCStartTimeSecurity.second;
      else
          HMI.PopupElapTime = (gRTCNextTimeSecurity.second+60) - gRTCStartTimeSecurity.second;
   }
  
   if ((HMI.Action_Input2 == gcActive) || (HMI.PopupElapTime >=1))   /* clear password change status if done manually or by time */
   {
      HMI.PopupElapTime = 0;              /* reset elap time state to 0 */
      visible(&HMI.PopupRuntime1,FALSE);     /* hide the accepted password popup window */
      visible(&HMI.PopupRuntime2,FALSE);     /* hide the invalid password popup window */
      Popup.Security = FALSE;
      HMI.Action_Input2 = gcInActive;     /* clear button press status */
   }

   /* show current security level in box */
   switch (cs->current_security_level)
   {
      case SERVICE_LOCK:
         HMI.pg310_SecurityLvlIndex = 4;
         visible(&HMI.pg000_Debug_Layer_On, TRUE);    /* Turn on debug layer in Service Mode GTF */
      break;
      case SUPERVISOR_LOCK:
         HMI.pg310_SecurityLvlIndex = 3;
         visible(&HMI.pg000_Debug_Layer_On, FALSE);   /* Turn off debug layer GTF */
         visible(&HMI.pg000_Lang_Layer_On,  FALSE);   /* Turn off language layer GTF */
      break;
      case MAINTENANCE_LOCK:
         HMI.pg310_SecurityLvlIndex = 2;
         visible(&HMI.pg000_Debug_Layer_On, FALSE);   /* Turn off debug layer GTF */
         visible(&HMI.pg000_Lang_Layer_On,  FALSE);   /* Turn off language layer GTF */
      break;
      case OPERATOR_LOCK:
         HMI.pg310_SecurityLvlIndex = 1;
         visible(&HMI.pg000_Debug_Layer_On, FALSE);   /* Turn off debug layer GTF */
         visible(&HMI.pg000_Lang_Layer_On,  FALSE);   /* Turn off language layer GTF */
      break;
      default:
         HMI.pg310_SecurityLvlIndex = 0;
         visible(&HMI.pg000_Debug_Layer_On, FALSE);   /* Turn off debug layer GTF */
         visible(&HMI.pg000_Lang_Layer_On,  FALSE);   /* Turn off language layer GTF */
      break;
   }
}


