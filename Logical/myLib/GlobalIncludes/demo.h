/************************************************************************/
/* FILE:        register.h                                              */
/*                                                                      */
/* DESCRIPTION: defines various weights for use in the demo mode        */
/*                                                                      */
/*  $Log:   F:\Software\BR_Guardian\demo.h_v  $
 *
 *    Rev 1.1   Mar 12 2008 14:25:58   Barry
 * Added demo definitions
 *
 *    Rev 1.0   Mar 12 2008 09:37:50   Barry
 * Initial version
 *
 *    Rev 1.5   Jun 08 2004 11:35:10   ZBJ
 * Changed demo test wt bits from 7000000 to 5000000
 * in order to run bigger batch size like 12kg.
 *
 *    Rev 1.4   Jan 23 2004 15:44:46   ZBJ
 *
 *
 *    Rev 1.4   Apr 26 1999 12:23:30   HOTCH
 * Changed demo low weight for calibration
 *
 *    Rev 1.3   Jan 12 1999 15:12:48   JohnS
 * changed demo calibration stuff.
 *
 *    Rev 1.2   Dec 30 1998 16:26:24   HOTCH
 * fixed comment within comment lint error
 *
 *    Rev 1.1   Sep 03 1998 15:16:00   JohnS
 * Removed old Batch stuff.
 *
 *    Rev 1.0   02 Apr 1998 09:49:22   Brian
 *
 *
 *    Rev 1.6   02 Apr 1998 09:39:46   Brian
 * initial revision
 *
 *                                                                      */
/*                                                                      */
/*  Process Control Corp,   Atlanta, GA                                 */
/************************************************************************/
#define LIW_DEMO_TESTWT 2000000
#define LIW_DEMO_TEST_WT 5.0
#define LIW_DEMO_ZEROWT 1000000
#define LIW_DEMO_LOAD_WT_ON 10.0
#define LIW_DEMO_LOAD_WT_OFF 0.0
#define LIW_DEMO_ALARM_WT 7.0
#define LIW_DEMO_CRIT_WT 1.0
#define BWH_DEMO_TESTWT 3000000
#define BWH_DEMO_TEST_WT 10.0
#define BWH_DEMO_ZEROWT 1000000
#define BWH_DEMO_LOAD_WT_ON 30.0
#define BWH_DEMO_LOAD_WT_OFF 0.1
#define BWH_DEMO_ALARM_WT 7.0
#define BWH_DEMO_CRIT_WT 1.0

#define DEMO_BOTH_MODS  0xFF        /* demo drive and weigh module      */
#define DEMO_DRIVE_MOD  0xF0        /* demo drive module                */
#define DEMO_WEIGH_MOD  0x0F        /* demo weigh module                */
#define DEMO_PULSES     0x0F        /* demo pulse module                */


