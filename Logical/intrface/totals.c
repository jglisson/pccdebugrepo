#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  totals.c

   Description: Controls the visualization as it pertains to the totals
      pages.


      $Log:   F:\Software\BR_Guardian\totals.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.totals to toggle debugging information in this file
 *
 *    Rev 1.5   May 28 2008 15:07:12   FMK
 * Got rid of compile warnings.
 *
 *    Rev 1.4   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.3   Apr 29 2008 16:14:56   FMK
 * Added recipe/resin totals.
 *
 *    Rev 1.2   Apr 02 2008 15:06:08   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.1   Jan 28 2008 13:52:58   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.0   Jan 11 2008 10:33:40   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <string.h>
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "plc.h"
#include "libbgdbg.h"
#include "recipe.h"

stored_recipe *rec;
extern struct PopupActiveStruct Popup; /* make sure popups only for totals are allowed */

void Totals_Main();
void clear_inventory_totals(void);
void clear_shift_totals(void);
void clear_resin_totals(void);
void clear_resin_indv_totals(void);
void clear_recipe_totals(void);
void clear_recipe_indv_totals(void);
void ProcessSystemTotalPanel(void);
void ProcessSystemTotalPanel64(void);

extern void visible(unsigned char *Runtime,unsigned char state);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/************************************************************************/
/*
   Function:  Totals_Main()

   Description:  processes the totals pages
                                                                        */
/************************************************************************/
void Totals_Main()
{
   config_super *cs = &cfg_super;

   switch (HMI.CurrentPage)
   {
      case pg320_Totals_System:  /* System Panel */
		 if (Popup.Totals == FALSE)
         {
            visible(&HMI.PopupRuntime1,FALSE);
            visible(&HMI.PopupRuntime2,FALSE);
            visible(&HMI.PopupRuntime3,TRUE);
            visible(&HMI.PopupRuntime4,FALSE);
            visible(&HMI.PopupRuntime5,FALSE);
         }
         ProcessSystemTotalPanel();

         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
               HMI.ChangePage = pg324_Totals_ExtNip;   /* Next set is extruder/nip totals */
            else
            {
               if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
                  HMI.ChangePage = pg325_Totals_Resin;
               else
                  HMI.ChangePage = pg320_Totals_System;   /* Previous set is system totals */
            }
            HMI.ChangeDevice = 0;
            }
         break;
      case pg321_Totals_AtoF:          /* Now A-F GTF */
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
               HMI.ChangePage = pg324_Totals_ExtNip;   /* Next set is extruder/nip totals */
            else
            {
               if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
                  HMI.ChangePage = pg325_Totals_Resin;
               else
                  HMI.ChangePage = pg320_Totals_System;   /* Previous set is system totals */
            }
            HMI.ChangeDevice = 0;
         }
    	 break;
      case pg322_Totals_System64:  /* System Panel 64 bit */
         if (Popup.Totals == FALSE)
         {
            visible(&HMI.PopupRuntime1,FALSE);
            visible(&HMI.PopupRuntime2,FALSE);
            visible(&HMI.PopupRuntime3,TRUE);
            visible(&HMI.PopupRuntime4,FALSE);
            visible(&HMI.PopupRuntime5,FALSE);
         }
         ProcessSystemTotalPanel64();

         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
               HMI.ChangePage = pg329_Totals_ExtNip64;   /* Next set is extruder/nip totals */
            else
            {
               if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
                  HMI.ChangePage = pg328_Totals_Resin64;
               else
                  HMI.ChangePage = pg322_Totals_System64;   /* Previous set is system totals */
            }
            HMI.ChangeDevice = 0;
         }
         break;
      case pg324_Totals_ExtNip:
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
               HMI.ChangePage = pg325_Totals_Resin;
            else
               HMI.ChangePage = pg320_Totals_System;   /* Previous set is system totals */

            HMI.ChangeDevice = 0;
         }
         break;
      case pg325_Totals_Resin:
         if (Popup.Totals == FALSE)
         {
            visible(&HMI.PopupRuntime1, FALSE);
         }

         if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RESIN])
            visible(&HMI.SetupRuntime1, TRUE);    /* shown clear resins */
         else
            visible(&HMI.SetupRuntime1, FALSE);     /* hidden clear resins */

         if (HMI.ChangeDevice == 1)    /* Previous Set button pressed */
         {
            if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED))
               HMI.ChangePage = pg324_Totals_ExtNip;   /* previous set is extruder/nip totals */
            else
               HMI.ChangePage = pg321_Totals_AtoF;   /* Previous set is hopper totals */
            HMI.ChangeDevice = 0;
         }

         if (HMI.Action_Input0 == gcActive)   /* clear shift totals pressed */
         {
            HMI.pg320_PopupTextIndex = 0; /* show correct popup title */
            visible(&HMI.PopupRuntime1,TRUE);    /* show popup */
            HMI.Action_Input0 = gcInActive; /* Reset keypress */
            Popup.Totals = TRUE;
         }
         else if (HMI.Action_Input1 == gcActive)  /* clear inventory totals pressed */
         {
            HMI.pg320_PopupTextIndex = 1; /* show correct popup title */
            visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
            HMI.Action_Input1 = gcInActive; /* Reset keypress */
            Popup.Totals = TRUE;
         }
         
         if (HMI.Action_Input7 == gcActive)    /* NO was pressed on the popup */
         {
            visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
            Popup.Totals = FALSE;
            HMI.Action_Input7 = gcInActive;  /* Reset keypress */
         }

         if (HMI.Action_Input6 == gcActive)
         {
            switch (HMI.pg320_PopupTextIndex)
            {
               case 0:
                  clear_resin_indv_totals();
               break;
               case 1:
                  clear_resin_totals();
               break;
            }
            visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
            Popup.Totals = FALSE;
            HMI.Action_Input6 = gcInActive;  /* Reset keypress */
         }

         HMI.pg320_ResinInven = PMEM.resin_inven[shr_global.num_resin_inven];
         brsstrcpy((DINT)HMI.pg320_product_code, (DINT)cfg_super.resin_table[shr_global.num_resin_inven].resin_desc);
         break;

      case pg326_Totals_AtoF64:
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
               HMI.ChangePage = pg329_Totals_ExtNip64;   /* Next set is extruder/nip totals */
            else
            {
               if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
                  HMI.ChangePage = pg328_Totals_Resin64;
               else
                  HMI.ChangePage = pg322_Totals_System64;   /* Previous set is system totals */
            }
            HMI.ChangeDevice = 0;
         }
         break;

      case pg328_Totals_Resin64:
         if (Popup.Totals == FALSE)
         {
            visible(&HMI.PopupRuntime1, FALSE);
         }

         if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RESIN])
            visible(&HMI.SetupRuntime1, TRUE);    /* shown clear resins */
         else
            visible(&HMI.SetupRuntime1, FALSE);     /* hidden clear resins */

         if (HMI.ChangeDevice == 1)    /* Previous Set button pressed */
         {
            if ((cfg_super.wtp_mode != UNUSED) || (cfg_super.ltp_mode != UNUSED))
               HMI.ChangePage = pg329_Totals_ExtNip64;   /* previous set is extruder/nip totals */
            else
               HMI.ChangePage = pg326_Totals_AtoF64;   /* Previous set is hopper totals */
            HMI.ChangeDevice = 0;
         }

         if (HMI.Action_Input0 == gcActive)   /* clear shift totals pressed */
         {
            HMI.pg320_PopupTextIndex = 0; /* show correct popup title */
            visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
            HMI.Action_Input0 = gcInActive; /* Reset keypress */
            Popup.Totals = TRUE;
         }
         else if (HMI.Action_Input1 == gcActive)  /* clear inventory totals pressed */
         {
            HMI.pg320_PopupTextIndex = 1; /* show correct popup title */
            visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
            HMI.Action_Input1 = gcInActive; /* Reset keypress */
            Popup.Totals = TRUE;
         }
         
         if (HMI.Action_Input7 == gcActive)    /* NO was pressed on the popup */
         {
            visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
            Popup.Totals = FALSE;
            HMI.Action_Input7 = gcInActive;  /* Reset keypress */
         }

         if (HMI.Action_Input6 == gcActive)
         {
            switch (HMI.pg320_PopupTextIndex)
            {
               case 0:
                  clear_resin_indv_totals();
               break;
               case 1:
                  clear_resin_totals();
               break;
            }
            visible(&HMI.PopupRuntime1,FALSE);    /* hidden popup */
            Popup.Totals = FALSE;
            HMI.Action_Input6 = gcInActive;  /* Reset keypress */
         }

         HMI.pg320_ResinInven = PMEM.dresin_inven[shr_global.num_resin_inven];
         brsstrcpy((DINT)HMI.pg320_product_code, (DINT)cfg_super.resin_table[shr_global.num_resin_inven].resin_desc);
         break;

      case pg329_Totals_ExtNip64:
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if (cs->enable_resin_codes == TRUE)       /* Previous set is resin totals */
               HMI.ChangePage = pg328_Totals_Resin64;
            else
               HMI.ChangePage = pg322_Totals_System64;   /* Previous set is system totals */

            HMI.ChangeDevice = 0;
         }
         break;

      default:
         break;
   }
}
/************************************************************************/
/*
   Function:  ProcessSystemTotalPanel()

   Description:  Process the system total panel with all of the popups.
                                                                        */
/************************************************************************/
void ProcessSystemTotalPanel64()
{
   config_super *cs = &cfg_super;

   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_SHIFT])
      visible(&HMI.SetupRuntime1, TRUE);    /* shown clear shift button */
   else
      visible(&HMI.SetupRuntime1, FALSE);   /* hidden clear shift button */

   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_INVEN])
      visible(&HMI.SetupRuntime2, TRUE);    /* shown clear inventory button */
   else
      visible(&HMI.SetupRuntime2, FALSE);   /* hidden clear inventory button */

   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RECIPE])
      visible(&HMI.SetupRuntime3, TRUE);    /* shown clear recipes button */
   else
      visible(&HMI.SetupRuntime3, FALSE);   /* hidden clear recipes button */

   if (HMI.ChangeDevice == 1)    /* Previous Set button pressed */
   {
      if (cfg_super.enable_resin_codes == TRUE)       /* Previous set is resin totals */
      {
         HMI.ChangePage = pg325_Totals_Resin;
      }
      else
      {
         if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
         {
            HMI.ChangePage = pg324_Totals_ExtNip;   /* Previous set is extruder/nip totals */
         }
         else
         {
            HMI.ChangePage = pg321_Totals_AtoF;   /* Previous set is hopper totals */
         }
      }
      HMI.ChangeDevice = 0;
   }

   if (HMI.Action_Input0 == gcActive)   /* clear shift totals pressed */
   {
      HMI.pg320_PopupTextIndex = 0; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input0 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input1 == gcActive)  /* clear inventory totals pressed */
   {
      HMI.pg320_PopupTextIndex = 1; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input1 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input2  == gcActive) /* clear recipe total pressed   */
   {
      HMI.pg320_PopupTextIndex = 2; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input2 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input3 == gcActive) /* clear all recipe totals pressed */
   {
      HMI.pg320_PopupTextIndex = 3; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input3 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }

   if (HMI.Action_Input7 == gcActive)    /* NO was pressed on the popup */
   {
      visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
      Popup.Totals = FALSE;
      HMI.Action_Input7 = gcInActive;  /* Reset keypress */
   }

   if (HMI.Action_Input6 == gcActive)
   {
      switch (HMI.pg320_PopupTextIndex)
      {
         case 0:
            clear_shift_totals();
            break;
         case 1:
            clear_inventory_totals();
            break;
         case 2:
            clear_recipe_indv_totals();
            break;
         case 3:
            clear_recipe_totals();
            break;
      }
      visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
      Popup.Totals = FALSE;
      HMI.Action_Input6 = gcInActive;  /* Reset keypress */
   }

   if (shr_global.num_recipe_inven == 0)
      shr_global.num_recipe_inven = 1;

   HMI.pg320_RecipeInven = PMEM.drecipe_inven[shr_global.num_recipe_inven];
   
   brsstrcpy((DINT)HMI.pg320_product_code, (DINT)RECIPES->entry[shr_global.num_recipe_inven].product_code);
}   

/************************************************************************/
/*
   Function:  ProcessSystemTotalPanel()

   Description:  Process the system total panel with all of the popups.
                                                                        */
/************************************************************************/

void ProcessSystemTotalPanel()
{
   config_super *cs = &cfg_super;

   if (cs->ucIconInterface)
	  visible(&HMI.pg320_LayerRuntime1, TRUE);
   else
	  visible(&HMI.pg320_LayerRuntime1, FALSE);
	
   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_SHIFT])
      visible(&HMI.SetupRuntime1, TRUE);    /* shown clear shift button */
   else
      visible(&HMI.SetupRuntime1, FALSE);     /* hidden clear shift button */

   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_INVEN])
      visible(&HMI.SetupRuntime2, TRUE);    /* shown clear inventory button */
   else
      visible(&HMI.SetupRuntime2, FALSE);     /* hidden clear inventory button */

   if(cfg_super.current_security_level >= cfg_super.pe_needed[PE_CLEAR_RECIPE])
      visible(&HMI.SetupRuntime3, TRUE);    /* shown clear recipes button */
   else
      visible(&HMI.SetupRuntime3, FALSE);     /* hidden clear recipes button */

   if (HMI.ChangeDevice == 1)    /* Previous Set button pressed */
   {
      if (cfg_super.enable_resin_codes == TRUE)       /* Previous set is resin totals */
      {
         HMI.ChangePage = pg325_Totals_Resin;
      }
      else
      {
         if ((cs->wtp_mode != UNUSED) || (cs->ltp_mode != UNUSED))
         {
            HMI.ChangePage = pg324_Totals_ExtNip;   /* Previous set is extruder/nip totals */
         }
         else
         {
            HMI.ChangePage = pg321_Totals_AtoF;   /* Previous set is hopper totals */
         }
      }
      HMI.ChangeDevice = 0;
   }

   if (HMI.Action_Input0 == gcActive)   /* clear shift totals pressed */
   {
      HMI.pg320_PopupTextIndex = 0; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input0 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input1 == gcActive)  /* clear inventory totals pressed */
   {
      HMI.pg320_PopupTextIndex = 1; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input1 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input2  == gcActive) /* clear recipe total pressed   */
   {
      HMI.pg320_PopupTextIndex = 2; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input2 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }
   else if (HMI.Action_Input3 == gcActive) /* clear all recipe totals pressed */
   {
      HMI.pg320_PopupTextIndex = 3; /* show correct popup title */
      visible(&HMI.PopupRuntime1, TRUE);    /* show popup */
      HMI.Action_Input3 = gcInActive; /* Reset keypress */
      Popup.Totals = TRUE;
   }

   if (HMI.Action_Input4 == gcActive)    /* Next Section wsa pressed */
   {
 	   visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
      HMI.ChangePage = pg321_Totals_AtoF;		
      HMI.Action_Input4 = gcInActive;  /* Reset keypress */
   }

   if (HMI.Action_Input7 == gcActive)    /* NO was pressed on the popup */
   {
      visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
      Popup.Totals = FALSE;
      HMI.Action_Input7 = gcInActive;  /* Reset keypress */
   }

   if (HMI.Action_Input6 == gcActive)
   {
      switch (HMI.pg320_PopupTextIndex)
      {
         case 0:
            clear_shift_totals();
            break;
         case 1:
            clear_inventory_totals();
            break;
         case 2:
            clear_recipe_indv_totals();
            break;
         case 3:
            clear_recipe_totals();
            break;
      }
      visible(&HMI.PopupRuntime1, FALSE);    /* hidden popup */
      Popup.Totals = FALSE;
      HMI.Action_Input6 = gcInActive;  /* Reset keypress */
   }

   if (shr_global.num_recipe_inven == 0)
      shr_global.num_recipe_inven = 1;

   HMI.pg320_RecipeInven = PMEM.recipe_inven[shr_global.num_recipe_inven];
   
   brsstrcpy((DINT)HMI.pg320_product_code, (DINT)RECIPES->entry[shr_global.num_recipe_inven].product_code);
}   
/************************************************************************/
/*
   Function:  clear_inventory_totals()

   Description:   Clear the inventory totals.
                                                                        */
/************************************************************************/
void clear_inventory_totals(void)
{
    int i;

    for(i=0;i<MAX_GATES;i++)
    {
      PMEM.Gate[i].InvTotal = 0.0;
      PMEM.Gate[i].dInvTotal = 0.0;
    }

    PMEM.EXT.InvTotal = 0.0;
    PMEM.EXT.dInvTotal = 0.0;
    PMEM.HO.InvTotal = 0.0;
    PMEM.HO.dInvTotal = 0.0;
}

/************************************************************************/
/*
   Function:  clear_shift_totals()

   Description:   Clear the shift totals
                                                                        */
/************************************************************************/
void clear_shift_totals(void)
{
   int i;

   for(i=0;i<MAX_GATES;i++)
   {
      PMEM.Gate[i].ShftTotal = 0.0;
      PMEM.Gate[i].dShftTotal = 0.0;
   }    

    PMEM.EXT.ShftTotal = 0.0;
    PMEM.EXT.dShftTotal = 0.0;
    PMEM.HO.ShftTotal = 0.0;
    PMEM.HO.dShftTotal = 0.0;
}

/************************************************************************/
/*
   Function:  clear_resin_totals()

   Description:   Clear all resin totals
                                                                        */
/************************************************************************/
void clear_resin_totals(void)
{
   int i;

   for (i=0; i<MAX_RESINS; ++i)
   {
      PMEM.resin_inven[i] = 0.0;
      PMEM.dresin_inven[i] = 0.0;
   }
}

/************************************************************************/
/*
   Function:  clear_resin_indv_totals()

   Description:   Clear the selected resin total
                                                                        */
/************************************************************************/
void clear_resin_indv_totals(void)
{
   PMEM.resin_inven[shr_global.num_resin_inven] = 0.0;
   PMEM.dresin_inven[shr_global.num_resin_inven] = 0.0;
}

/************************************************************************/
/*
   Function:  clear_recipe_totals()

   Description:   Clear all the recipe totals
                                                                        */
/************************************************************************/
void clear_recipe_totals(void)
{
   int i;

   for (i=0; i<MAX_RECIPES; ++i)
   {
      PMEM.recipe_inven[i] = 0.0;
      PMEM.drecipe_inven[i] = 0.0;
   }
}

/************************************************************************/
/*
   Function:  Clear_recipe_indv_totals()

   Description:   Clear the selected resin total
                                                                        */
/************************************************************************/
void clear_recipe_indv_totals(void)
{
   PMEM.recipe_inven[shr_global.num_recipe_inven] = 0.0;
   PMEM.drecipe_inven[shr_global.num_recipe_inven] = 0.0;
}


