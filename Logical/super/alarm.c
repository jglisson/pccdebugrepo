#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  alarm.c

   Description: Process all alarm conditions and controls there severity,
      and state.


      $Log:   F:\Software\BR_Guardian\alarm.c_v  $
 *
 *    2008/12/02 JLR
 *     added/changed telnet debug stuff
 *     use debugFlags.alarm to toggle the debug information in this file
 *
 *    Rev 1.6   May 28 2008 15:05:50   BMM
 * Silence Old Alarms is now working.
 *
 *    Rev 1.5   May 28 2008 15:05:50   FMK
 * Got rid of compile warnings.
 *
 *    Rev 1.4   May 09 2008 14:01:22   FMK
 * Added control for buzzer.
 *
 *    Rev 1.3   Mar 21 2008 09:56:26   Barry
 * Added calc_demo_relay.
 *
 *    Rev 1.2   Feb 11 2008 14:59:52   FMK
 * Modified code to correctly acknowledge the alarms
 * and clear the alarm history.
 *
 *    Rev 1.1   Jan 30 2008 11:39:46   FMK
 * Changed reference for PVCS check in comment path.
 *
 *    Rev 1.0   Jan 11 2008 10:33:30   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <string.h>
#include "mem.h"
#include "plc.h"
#include "signal_pcc.h"
#include "sys.h"
#include "alarm.h"
#include "almask.h"
#include "relaydef.h"
#include "HMI.h"
#include "libbgdbg.h"

void add_smtp_queue_msg(char *, char *);
void check_alarms(void);
void ReviewAlarm(unsigned int alarm_index);
int  alarm_status_changed(void);
void parse_alarm(unsigned int alarm_index, unsigned char *alarm_type, unsigned char *device_type, unsigned int *loop);
int  calc_demo_relay(int relay);

_GLOBAL unsigned int previous_alarm_latch_bits[MAX_LOOP];
_GLOBAL unsigned int previous_alarm_bits[MAX_LOOP];
_GLOBAL char         previous_oper_mode[MAX_LOOP];
_GLOBAL unsigned int previous_sys_alarm_latch_bits;
_GLOBAL unsigned int previous_sys_alarm_bits;

char previous_bln_alm_mode;
char previous_ext_alm_mode;
char ack_alarms_during_reset;

_GLOBAL UDINT latched;
_GLOBAL UDINT current;
_GLOBAL USINT relay_action;
_GLOBAL USINT current_level;

_GLOBAL unsigned int alarmbit;
_GLOBAL unsigned int alarmmsg_tableoffset;

_LOCAL unsigned char current_action;
_LOCAL unsigned int  alarm_upgrade; /* flag for alarm action increased in severity */

/* devices to take action on */
_LOCAL char   blender_alarm;
_LOCAL char   extruder_alarm;
_GLOBAL UDINT SYS_ALM_MASK;

typedef struct 
{
   unsigned int  uiIndex;
   char *cAlarmDesc;
   unsigned char ucToggleOnOff;
} AlarmTextKey_struct;

// *** warning *** This needs updating if more text is added below.
// This structure holds the index and text for alarms from the HMI visual
#define AlarmTextKeyMaxRecords 256

//  used by Simple Mail Transfer Protocol See: add_smtp_queue_msg() - adds msg to queue            
AlarmTextKey_struct AlarmTextKey[AlarmTextKeyMaxRecords] = {
   {19,"Sys:Reserve Input 3 Alarm",0},
   {20,"Sys:Reserve Input 2 Alarm",0},
   {21,"Sys:Reserve Input 1 Alarm",0},
   {22,"Sys: PLC Low Battery Alarm",0},
   {23,"Sys: Auxiliary Alarm",0},
   {24,"Sys: Pump Failure Alarm",0},
   {25,"Sys: Battery Low Alarm",0},
   {26,"Sys: Hardware Failure Alarm",0},
   {27,"Sys: Power Supply Failure Alarm",0},
   {28,"Sys: Interlock Open-Alarm",0},
   {29,"Sys: Check Printer Alarm",0},
   {30,"Sys: Information Alarm",0},
   {31,"Sys: General Alarm",0},
   {32,"Sys: Shutdown Alarm",0},

   {41,"BWH: Weight Interface Failure",0},
   {45,"BWH: Hopper weight to high",0},
   {47,"BWH: Hopper in manual",0},
   {52,"BWH: Hopper weight to low",0},
   {53,"Reserved 53 ",0},
   {54,"BWH: Mix out of specification",0},
   {55,"BWH: Unstable Weigh system",0},
   {56,"Reserved 56 ",0},
   {57,"Reserved 57 ",0},
   {58,"Reserved 58 ",0},
   {59,"BWH: Hopper not dumping",0},

   {60,"SWD: Out of spec",0},
   {61,"SPD: Out of spec",0},
   {62,"Uncovered Prox Timer Expired",0},
   {63,"Reserved 63 ",0},
   {64,"Reserved 64 ",0},
   {65,"Reserved 65 ",0},
   {77,"Reserved 77 ",0},
   {88,"MIX: Starter Failure",0},
   {90,"MIX: Valve Failure",0},
   {92,"MIX: Interlock Door Open",0},
   {95,"Reserved 95",0},  // 36
	
	{104,"Hop A: Level Critical Low",0},
	{113,"Rcv A: Low Dump Size",0},
	{115,"Hop A: Out of Material",0},
	{118,"Hop A: Mix out of specification",0},
	{119,"Hop A: 3-Prox High Alarm",0},
	{120,"Hop A: 3-Prox Low Alarm",0},
	{121,"Hop A: Switch Failure",0},
	{127,"Hop A: Low Prox Level",0},

	{136,"Hop B: Level Critical Low",0},
	{145,"Rcv B: Low Dump Size",0},
	{147,"Hop B: Out of Material",0},
	{150,"Hop B: Mix out of specification",0},
	{151,"Hop B: 3-Prox High Alarm",0},
	{152,"Hop B: 3-Prox Low Alarm",0},
	{153,"Hop B: Switch Failure",0},
	{159,"Hop B: Low Prox Level",0},

	{168,"Hop C: Level Critical Low",0},
	{177,"Rcv C: Low Dump Size",0},
	{179,"Hop C: Out of Material",0},
	{182,"Hop C: Mix out of specification",0},
	{183,"Hop C: 3-Prox High Alarm",0},
	{184,"Hop C: 3-Prox Low Alarm",0},
	{185,"Hop C: Switch Failure",0},
	{191,"Hop C: Low Prox Level",0},

	{200,"Hop D: Level Critical Low",0},
	{209,"Rcv D: Low Dump Size",0},
	{211,"Hop D: Out of Material",0},
	{214,"Hop D: Mix out of specification",0},
	{215,"Hop D: 3-Prox High Alarm",0},
	{216,"Hop D: 3-Prox Low Alarm",0},
	{217,"Hop D: Switch Failure",0},
	{223,"Hop D: Low Prox Level",0},

	{232,"Hop E: Level Critical Low",0},
	{241,"Rcv E: Low Dump Size",0},
	{243,"Hop E: Out of Material",0},
	{246,"Hop E: Mix out of specification",0},
	{247,"Hop E: 3-Prox High Alarm",0},
	{248,"Hop E: 3-Prox Low Alarm",0},
	{249,"Hop E: Switch Failure",0},
	{255,"Hop E: Low Prox Level",0},

	{264,"Hop F: Level Critical Low",0},
	{273,"Rcv F: Low Dump Size",0},
	{275,"Hop F: Out of Material",0},
	{278,"Hop F: Mix out of specification",0},
	{279,"Hop F: 3-Prox High Alarm",0},
	{280,"Hop F: 3-Prox Low Alarm",0},
	{281,"Hop F: Switch Failure",0},
	{287,"Hop F: Low Prox Level",0},

	{296,"Hop G: Level Critical Low",0},
	{305,"Rcv G: Low Dump Size",0},
	{307,"Hop G: Out of Material",0},
	{310,"Hop G: Mix out of specification",0},
	{311,"Hop G: 3-Prox High Alarm",0},
	{312,"Hop G: 3-Prox Low Alarm",0},
	{313,"Hop G: Switch Failure",0},
	{319,"Hop G: Low Prox Level",0},

	{328,"Hop H: Level Critical Low",0},
	{337,"Rcv H: Low Dump Size",0},
	{339,"Hop H: Out of Material",0},
	{342,"Hop H: Mix out of specification",0},
	{343,"Hop H: 3-Prox High Alarm",0},
	{344,"Hop H: 3-Prox Low Alarm",0},
	{345,"Hop H: Switch Failure",0},
	{351,"Hop H: Low Prox Level",0},

	{360,"Hop I: Level Critical Low",0},
	{369,"Rcv I: Low Dump Size",0},
	{371,"Hop I: Out of Material",0},
	{374,"Hop I: Mix out of specification",0},
	{375,"Hop I: 3-Prox High Alarm",0},
	{376,"Hop I: 3-Prox Low Alarm",0},
	{377,"Hop I: Switch Failure",0},
	{383,"Hop I: Low Prox Level",0},

	{392,"Hop J: Level Critical Low",0},
	{401,"Rcv J: Low Dump Size",0},
	{403,"Hop J: Out of Material",0},
	{406,"Hop J: Mix out of specification",0},
	{407,"Hop J: 3-Prox High Alarm",0},
	{408,"Hop J: 3-Prox Low Alarm",0},
	{409,"Hop J: Switch Failure",0},
	{415,"Hop J: Low Prox Level",0},

	{424,"Hop K: Level Critical Low",0},
	{433,"Rcv K: Low Dump Size",0},
	{435,"Hop K: Out of Material",0},
	{438,"Hop K: Mix out of specification",0},
	{439,"Hop K: 3-Prox High Alarm",0},
	{440,"Hop K: 3-Prox Low Alarm",0},
	{441,"Hop K: Switch Failure",0},
	{447,"Hop K: Low Prox Level",0},

	{456,"Hop L: Level Critical Low",0},
	{465,"Rcv L: Low Dump Size",0},
	{467,"Hop L: Out of Material",0},
	{470,"Hop L: Mix out of specification",0},
	{471,"Hop L: 3-Prox High Alarm",0},
	{472,"Hop L: 3-Prox Low Alarm",0},
	{473,"Hop L: Switch Failure",0},
	{479,"Hop L: Low Prox Level",0},  // 96
   
   {481,"EXD: Drive hardware failure",0},
   {482,"EXD: Drive software failure",0},
   {483,"EXD: Drive software update",0},
   {484,"EXD: Drive network failure",0},
   {485,"EXD: Drive Input Overload",0},
   {486,"EXD: Drive inhibited",0},
   {487,"EXD: Drive in manual backup",0},

   {493,"LIW: Hopper Weight Too High",0},
   {496,"EXD: Loss of Syncronization",0},
   {497,"LIW: Last Refill Too Small",0},
   {498,"LIW: Almosat out of Material",0},
   {499,"LIW: Out of Material",0},
   {500,"LIW: Hopper Weight Too Low",0},
   {501,"EXD: Drive below speed limit",0},
   {502,"LIW: Mix out of specification",0},
   {503,"EXD: Unstable weigh system",0},
   {504,"EXD: Drive over speed limit",0},
   {505,"LIW: No weight loss detected",0},
   
   {545,"HO: Drive hardware failure",0},
   {546,"HO: Drive software failure",0},
   {547,"HO: Drive software update",0},
   {548,"HO: Drive network failure",0},
   {549,"HO: Drive Input Overload",0},
   {550,"HO: Drive inhibited",0},
   {551,"HO: Drive in manual backup",0},

   {560,"HO: Loss of Syncronization",0},
   {565,"HO: Drive below speed limit",0},
   {567,"HO: Unstable pulse pickup",0},
   {568,"HO: Drive over speed limit",0},
   {569,"HO: Drive system failure",0},

   {577,"SHO: Drive hardware failure",0},
   {578,"SHO: Drive software failure",0},
   {579,"SHO: Drive software update",0},
   {580,"SHO: Drive network failure",0},
   {581,"SHO: Drive Input Overload",0},
   {582,"SHO: Drive inhibited",0},
   {583,"SHO: Drive in manual backup",0},

   {592,"SHO: Loss of Syncronization",0},
   {597,"SHO: Drive below speed limit",0},
   {599,"SHO: Unstable pulse pickup",0},
   {600,"SHO: Drive over speed limit",0},
   {601,"SHO: Drive system failure",0},

   {609,"FLF: Hopper hardware failure",0},
   {610,"FLF: Hopper software failure",0},
   {611,"FLF: Hopper software update",0},
   {612,"FLF: Hopper network failure",0},
   {613,"FLF: Hopper in manual backup",0},
   {614,"FLF: Hopper weight too high",0},
   {615,"FLF: Hopper almost out of material",0},
   {616,"FLF: Out of material",0},
   {617,"FLF: Hopper weight too low",0},

   {641,"EXD: Drive hardware failure",0},
   {642,"EXD: Drive software failure",0},
   {643,"EXD: Drive software update",0},
   {644,"EXD: Drive network failure",0},
   {645,"EXD: Drive Input Overload",0},
   {646,"EXD: Drive inhibited",0},
   {647,"EXD: Drive in manual backup",0},
   
   {653,"LIW: Hopper Weight Too High",0},
   {656,"EXD: Loss of Syncronization",0},
   {657,"LIW: Last Refill Too Small",0},
   {658,"LIW: Almost out of material",0},
   {659,"LIW: Out of Material",0},
   {660,"LIW: Hopper Weight Too Low",0},
   {661,"EXD: Drive below speed limit",0},
   {662,"LIW: Mix out opf specification",0},
   {663,"EXD: Unstable weigh system",0},
   {664,"EXD: Drive over speed limit",0},
   {665,"LIW: No weight loss detected",0},

   {673,"WTH: Controller failure",0},
   {674,"WTH: Controller incompatible",0},
   {675,"WTH: Controller net failure",0},
   {676,"WTH: Controller in manual",0},
   {677,"WTH: Product break",0},
   {678,"WTH: Measurement Error",0},
   {679,"WTH: Over maximum",0},
   {680,"WTH: Under minimum",0},
   {681,"WTH: Mix out of specification",0},

   {705,"Extruder: Drive hardware failure",0},
   {706,"Extruder: Drive software failure",0},
   {707,"Extruder: Drive software update",0},
   {708,"Extruder: Drive network failure",0},
   {709,"Extruder: Drive Input Overload",0},
   {710,"Extruder: Drive inhibited",0},
   {711,"Extruder: Drive in manual backup",0},
   {717,"Extruder: Hopper Weight Too High",0},  
   {720,"Extruder: Loss of Syncronization",0},
   {721,"Extruder: Last Refill Too Small",0},
   {722,"Extruder: Almost out of material",0},
   {723,"Extruder: Out of Material",0},
   {724,"Extruder: Hopper Weight Too Low",0},
   {725,"Extruder: Drive below speed limit",0},
   {726,"Extruder: WTP out of specification",0},
   {727,"Extruder: Excessive Coasting, Can't Run in Gravimetric Mode",0},
   {728,"Extruder: Drive over speed limit",0},
   {729,"Extruder: No weight loss detected",0}
   };

// global variable for debug information JLR 
_GLOBAL bgRingBuffer_typ g_RingBuffer1;
_GLOBAL int              iAlarm;
_GLOBAL unsigned long    shift;

/************************************************************************/
/*
   Function:  check_alarms()

   Description:  This function checks to see if there has been a change
          to the condition or status of any alarm in the system. If a 
          change hsa been detected, the necessary actions are taken.
                                                                        */
/************************************************************************/
void check_alarms(void)
{
   unsigned char action          = NONE;
   unsigned char blender_action  = NONE;
   unsigned char extruder_action = NONE;

   int i=0, j;

   relay_action = NONE;
   
   g_RingBuffer1.dbgLevel  = 4;  /* ok to print debug information */
   g_RingBuffer1.dbgFormat = (BG_DEBUG_FORMAT_FILENAME |
                             BG_DEBUG_FORMAT_FUNCTION |
                             BG_DEBUG_FORMAT_TIMESTAMP |
                             BG_DEBUG_FORMAT_LEVEL |
                             BG_DEBUG_FORMAT_LINENUMBER);

   if (!shr_global.halt_alarm_processing)
   {
      /* the buzzer may be on. If the user presses the remote silence input
       * on the blender, the buzzer should be silenced.
       */
      if (IO_dig[X20].channel[BUZZER_SILENCE] == TRUE)
         IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;

      /* do quick scan of alarms for changes  */
      if (alarm_status_changed())
      {
         iAlarm = 0;

         while ((cfg_super.AlarmAction[iAlarm].alarm_type != NOT_USED) &&
                (!shr_global.halt_alarm_processing))
         {
            ReviewAlarm(iAlarm);
            current_level = cfg_super.AlarmAction[iAlarm].ack &
                    (SYSTEM_SHUTDOWN | GENERAL_ALARM | INFORMATION_ONLY);

            // this is used for the smtp alarm stuff
            if (((current) || (latched)) && (shift) && (smtp_info.active != 0))
            {
               for (j=0; j<AlarmTextKeyMaxRecords; j++)
               {                     
                  if (AlarmTextKey[j].uiIndex == alarmbit)
                  {
                     if ((current)&&(!AlarmTextKey[j].ucToggleOnOff)) // if current, turn on
                     {
                        AlarmTextKey[j].ucToggleOnOff=1;
                        add_smtp_queue_msg(AlarmTextKey[j].cAlarmDesc, " -ON-");
                     }
                     else if ((latched)&&(AlarmTextKey[j].ucToggleOnOff)) // if latched, turn off
                     {
                        AlarmTextKey[j].ucToggleOnOff=0;
                        add_smtp_queue_msg(AlarmTextKey[j].cAlarmDesc, " -OFF-");
                     }
                  }
               }
            }

            if (latched)
            {
                if ((debugFlags.alarmsys) && (iAlarm == 68))
                     bgDbgInfo(&g_RingBuffer1, "Check Alarms latched", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "current_action=%u current_level=%u \r\n", current_action, current_level);
               /* upgrade the ack bits to highest level */
               if (current_action != current_level)
               {
                  if ((debugFlags.alarmsys) && (iAlarm == 68))
                     bgDbgInfo(&g_RingBuffer1, "Check Alarms latched", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm.ack=%u \r\n", cfg_super.AlarmAction[iAlarm].ack);
                  
                  cfg_super.AlarmAction[iAlarm].ack = (cfg_super.AlarmAction[iAlarm].ack &
                     ~(SYSTEM_SHUTDOWN | GENERAL_ALARM | INFORMATION_ONLY)) | current_action;

                  if ((debugFlags.alarmsys) && (iAlarm == 68))
                     bgDbgInfo(&g_RingBuffer1, "Check Alarms latched", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm.ack=%u \r\n", cfg_super.AlarmAction[iAlarm].ack);
               }
               /* make alarm relays follow latched and unacknowledged alarms
                * (default) if action is system shutdown then don't silence
                * the alarm
                */
               if (!(cfg_super.AlarmAction[iAlarm].ack & ALARM_ACK))
               {
                  if (cfg_super.silence_old_alarms)
                  {
                     if (alarm_upgrade || (cfg_super.AlarmAction[iAlarm].ack & SYSTEM_SHUTDOWN))
                         relay_action |= cfg_super.AlarmAction[iAlarm].ack;
                  }
                  else
                      relay_action |= cfg_super.AlarmAction[iAlarm].ack;
               }
               else
               {
                  if (alarm_upgrade)
                      relay_action |= cfg_super.AlarmAction[iAlarm].ack;
               }
            }

            //G2-597
            if ((latched) && (cfg_super.AlarmAction[iAlarm].ack == 0))
            {
               Alarm.Gen[alarmbit]  = FALSE;
               Alarm.Info[alarmbit] = FALSE;
               Alarm.Shut[alarmbit] = FALSE;
            }
            
            /* This is where we set the alarm bits so they show up on
             * the B&R alarm screen
             */
            if ((debugFlags.alarmsys) && (iAlarm == 68))
                    bgDbgInfo(&g_RingBuffer1, "Alarm ACTION", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm.ack=%u \r\n", cfg_super.AlarmAction[iAlarm].ack);

            if (cfg_super.AlarmAction[iAlarm].ack & SYSTEM_SHUTDOWN)
            {
               Alarm.Gen[alarmbit]  = FALSE;
               Alarm.Info[alarmbit] = FALSE;
               if (current)
                  Alarm.Shut[alarmbit] = TRUE;
               else
                  Alarm.Shut[alarmbit] = FALSE;
                  if ((debugFlags.alarmsys) && (iAlarm == 68))
                    bgDbgInfo(&g_RingBuffer1, "SYSTEM_SHUTDOWN", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm=%d Alarmbit=%u \r\n", iAlarm, alarmbit);
            }

            
            if (cfg_super.AlarmAction[iAlarm].ack & GENERAL_ALARM)
            {
               Alarm.Shut[alarmbit] = FALSE;
               Alarm.Info[alarmbit] = FALSE;
               if (current)
                  Alarm.Gen[alarmbit] = TRUE;
               else
                  Alarm.Gen[alarmbit] = FALSE;
                  if ((debugFlags.alarmsys) && (iAlarm == 68))
                    bgDbgInfo(&g_RingBuffer1, "GENERAL_ALARM", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm=%d Alarmbit=%u \r\n", iAlarm, alarmbit);
            }

            if (cfg_super.AlarmAction[iAlarm].ack & INFORMATION_ONLY)
            {
               Alarm.Shut[alarmbit] = FALSE;
               Alarm.Gen[alarmbit]  = FALSE;
               if (current)
                  Alarm.Info[alarmbit] = TRUE;
               else
                  Alarm.Info[alarmbit] = FALSE;
                  if ((debugFlags.alarmsys) && (iAlarm == 68))
                    bgDbgInfo(&g_RingBuffer1, "INFORMATION_ONLY", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "iAlarm=%d Alarmbit=%u \r\n", iAlarm, alarmbit);
            }
            
            if (current)
            {
               /* only start & stop, etc off of current, not latched */
               action |= current_action;

               if (blender_alarm)
                  blender_action |= current_action;

               if (extruder_alarm)
                  extruder_action |= current_action;

               /* relay must follow current action if the alarm is not
                * acknowledged
                */
               if (!(cfg_super.AlarmAction[iAlarm].ack & ALARM_ACK))
                  relay_action |= cfg_super.AlarmAction[iAlarm].ack;
               else if (alarm_upgrade)
                  relay_action |= cfg_super.AlarmAction[iAlarm].ack;

               //if (((unsigned char)~cfg_super.alarms[iAlarm].ack & PRINT_ALARM) || alarm_upgrade)
               //{
                  //_sysdate(2,(int*)&time_stamp, (int*)&date, (short*)&day, (int*)&tick);
                  //log_alarm(iAlarm,current_action, time_stamp, date, TRUE);
                  //cfg_super.alarms[iAlarm].ack |= PRINT_ALARM;
                  //if (cfg_super.log_alarms && shr_global.printer_ok)
                  //   print_alarm_message(iAlarm, current_action, time_stamp, date, TRUE);
                  //put putty msg here
               //}

               /* relay must follow current action if the alarm is
                * not acknowledged
                */
               if (!(cfg_super.AlarmAction[iAlarm].ack & ALARM_ACK))
                  relay_action |= cfg_super.AlarmAction[iAlarm].ack;
            }
            
            iAlarm++;
         }

         /* if reset in middle of scan, force cleared */
         if (shr_global.halt_alarm_processing)
         {
              action = NONE;
              relay_action = NONE;
              blender_action = NONE;
              extruder_action = NONE;
         }

         if (blender_action & SYSTEM_SHUTDOWN)
         {
            shr_global.alarm_latch_bits |= SYS_SHUTDOWN;
            shr_global.alarm_bits |= SYS_SHUTDOWN;
		      Signal.ProcessShutdown = 1;
         }
         else
            shr_global.alarm_bits &= ~SYS_SHUTDOWN;

         if (blender_action & GENERAL_ALARM)
         {
            shr_global.alarm_latch_bits |= GEN_ALARM;
            shr_global.alarm_bits       |= GEN_ALARM;
         }
         else
            shr_global.alarm_bits &= ~GEN_ALARM;

         if (blender_action & INFORMATION_ONLY)
         {
            shr_global.alarm_latch_bits |= INFO_ALARM;
            shr_global.alarm_bits       |= INFO_ALARM;
         }
         else
            shr_global.alarm_bits &= ~INFO_ALARM;

         // G2-593
         if (extruder_action & SYSTEM_SHUTDOWN)
         {
            shrEXT.alarm_latch_bits |= SYS_SHUTDOWN;
            shrEXT.alarm_bits |= SYS_SHUTDOWN;
		      Signal.StopExtr = 1;
         }
         else
            shrEXT.alarm_bits &= ~SYS_SHUTDOWN;

         if (extruder_action & GENERAL_ALARM)
         {
            shrEXT.alarm_latch_bits |= GEN_ALARM;
            shrEXT.alarm_bits       |= GEN_ALARM;
         }
         else
            shrEXT.alarm_bits &= ~GEN_ALARM;

         if (extruder_action & INFORMATION_ONLY)
         {
            shrEXT.alarm_latch_bits |= INFO_ALARM;
            shrEXT.alarm_bits       |= INFO_ALARM;
         }
         else
            shrEXT.alarm_bits &= ~INFO_ALARM;

#if 0  /* this doesn't work.  it makes the assumption that when an alarm is
        * acked, the alarm_bits are set to 0.  That simply isn't true */
         /* If Silence Old Alarms == True AND There Are No Active Alarms
          * but there are old alarms then turn off beacon and buzzer BMM */
         if ((cfg_super.silence_old_alarms == TRUE) && (shr_global.alarm_bits == 0) && (shr_global.alarm_latch_bits > 0))
         {
            IO_dig[X20].channel[ALARM_OUT]         = OFF;
            IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
         }
#endif
         if ((cfg_super.silence_old_alarms == TRUE) && (gAllAlarmsAcked == TRUE))
         {
            IO_dig[X20].channel[ALARM_OUT]         = OFF;
            IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
            gAllAlarmsAcked                        = FALSE;
         }
         else
         {
           switch (cfg_super.set_alarm_relay_type)
           {
              case 1:   /* Turn relay on for Shutdown alarms only */
              if (relay_action & SYSTEM_SHUTDOWN)
              {
                 IO_dig[X20].channel[ALARM_OUT] = ON;
                 IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = ON;
              }
              /* only user of silence old should turn off GTF */
              else if (cfg_super.silence_old_alarms == TRUE)
              {
                 IO_dig[X20].channel[ALARM_OUT] = OFF;
                 IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
              }
              break;
              case 2:   /* Turn relay on for Shutdown and General alarms only */
              if (relay_action & (SYSTEM_SHUTDOWN|GENERAL_ALARM))
              {
                IO_dig[X20].channel[ALARM_OUT] = ON;
                IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = ON;
              }
              /* only user of silence old should turn off GTF */
              else if (cfg_super.silence_old_alarms == TRUE)
              {
                IO_dig[X20].channel[ALARM_OUT] = OFF;
                IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
              }
              break;
              case 3:   /* Turn relay on for All alarms */
              if (relay_action & (SYSTEM_SHUTDOWN | GENERAL_ALARM | INFORMATION_ONLY))
              {
                IO_dig[X20].channel[ALARM_OUT] = ON;
                IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = ON;
              }
              /* only user of silence old should turn off GTF */
              else if (cfg_super.silence_old_alarms == TRUE)
              {
                IO_dig[X20].channel[ALARM_OUT] = OFF;
                IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
              }
              break;
              default:  /* no relay output */
              /* only user of silence old should turn off GTF */
              if (cfg_super.silence_old_alarms == TRUE)
              {
                 IO_dig[X20].channel[ALARM_OUT] = OFF;
                 IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;
              }
              break;
           }
        }

        if (blender_action & SYSTEM_SHUTDOWN)
        {
          shrBWH.oper_mode    = PAUSE_MODE;
          shrBWH.running_mode = PAUSE_MODE;
        }

        if (extruder_action & SYSTEM_SHUTDOWN)
        {
          shrEXT.oper_mode = PAUSE_MODE;
	       LiwCommand[0].Command = PAUSE_MODE;
	       EXT_HO_OPER_MODE = PAUSE_MODE;
	       shrHO.oper_mode = PAUSE_MODE;
	       LiwCommand[1].Command = PAUSE_MODE;
        }
      }
   }

   /* clear all alarms - resetting */
   if (shr_global.halt_alarm_processing || shr_global.ack_all_alarms)
   {
      IO_dig[X20].channel[ALARM_OUT] = OFF;

      if ((shr_global.halt_alarm_processing && ack_alarms_during_reset) ||
           shr_global.ack_all_alarms)
      {
         for (i=0; i<MAX_ALARMS; i++)
            cfg_super.AlarmAction[i].ack = 0;
      }
      ack_alarms_during_reset = FALSE;
      shr_global.ack_all_alarms = FALSE;
   }
}

/************************************************************************/
/*
   Function:  alarm_status_changed()

   Description:  This function checks to see if there has been a change
          to the condition or status of any alarm in the system. It looks
          at the latched alarm state, current alarm state, and the oper.
          mode. If the status of any of these change then this function
          returns TRUE, else it returns FALSE;
                                                                        */
/************************************************************************/
int alarm_status_changed(void)
{
   config_super *cs = &cfg_super;
   int j;
   int alarm_change = FALSE;

   shift = 0;
   /* Batch Hopper Alarms */
   if (shrBWH.alarm_latch_bits != previous_alarm_latch_bits[WEIGH_HOP_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[WEIGH_HOP_LOOP] = shrBWH.alarm_latch_bits;
   }
   if (shrBWH.alarm_bits != previous_alarm_bits[WEIGH_HOP_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[WEIGH_HOP_LOOP] = shrBWH.alarm_bits;
   }
   if (shrBWH.oper_mode != previous_oper_mode[WEIGH_HOP_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[WEIGH_HOP_LOOP] = shrBWH.oper_mode;
   }

   /* Feeder Alarms */
   for (j=0; j<cs->total_gates; j++)
   {
      if (shrGATE[j].alarm_latch_bits != previous_alarm_latch_bits[j])
      {
         alarm_change = TRUE;
         previous_alarm_latch_bits[j] = shrGATE[j].alarm_latch_bits;
      }
      if (shrGATE[j].alarm_bits != previous_alarm_bits[j])
      {
         alarm_change = TRUE;
         previous_alarm_bits[j] = shrGATE[j].alarm_bits;
      }
      if (shrGATE[j].oper_mode != previous_oper_mode[j])
      {
         alarm_change = TRUE;
         previous_oper_mode[j] = shrGATE[j].oper_mode;
      }
   }

   /* Mixer Alarms */
   if (shrMIXER.alarm_latch_bits != previous_alarm_latch_bits[MIXER_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[MIXER_LOOP] = shrMIXER.alarm_latch_bits;
   }
   if (shrMIXER.alarm_bits != previous_alarm_bits[MIXER_LOOP])
   {
      alarm_change = TRUE;
      if ((shrMIXER.alarm_bits<<4) != (previous_alarm_bits[MIXER_LOOP]<<4))
      {
         shift += 1<<2;
      }
      previous_alarm_bits[MIXER_LOOP] = shrMIXER.alarm_bits;
   }

   /* Extruder Alarms */
   if (shrEXT.alarm_latch_bits != previous_alarm_latch_bits[EXT_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[EXT_LOOP] = shrEXT.alarm_latch_bits;
   }
   if (shrEXT.alarm_bits != previous_alarm_bits[EXT_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[EXT_LOOP] = shrEXT.alarm_bits;
   }
   if (shrEXT.oper_mode != previous_oper_mode[EXT_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[EXT_LOOP] = shrEXT.oper_mode;
   }

   /* Gravifluff Alarms */
   if (shrGFLUFF.alarm_latch_bits != previous_alarm_latch_bits[GRAVIFLUFF_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[GRAVIFLUFF_LOOP] = shrGFLUFF.alarm_latch_bits;
   }
   if (shrGFLUFF.alarm_bits != previous_alarm_bits[GRAVIFLUFF_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[GRAVIFLUFF_LOOP] = shrGFLUFF.alarm_bits;
   }
   if (shrGFLUFF.oper_mode != previous_oper_mode[GRAVIFLUFF_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[GRAVIFLUFF_LOOP] = shrGFLUFF.oper_mode;
   }

   if (shrGFLUFF.alarm_latch_bits != previous_alarm_latch_bits[GRAVIFLUFF_FDR_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[GRAVIFLUFF_FDR_LOOP] = shrGFLUFF.alarm_latch_bits;
   }
   if (shrGFLUFF.alarm_bits != previous_alarm_bits[GRAVIFLUFF_FDR_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[GRAVIFLUFF_FDR_LOOP] = shrGFLUFF.alarm_bits;
   }
   if (shrGFLUFF.oper_mode != previous_oper_mode[GRAVIFLUFF_FDR_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[GRAVIFLUFF_FDR_LOOP] = shrGFLUFF.oper_mode;
   }

   /* Haul Off Alarms */
   if (shrHO.alarm_latch_bits != previous_alarm_latch_bits[HO_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[HO_LOOP] = shrHO.alarm_latch_bits;
   }
   if (shrHO.alarm_bits != previous_alarm_bits[HO_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[HO_LOOP] = shrHO.alarm_bits;
   }
   if (shrHO.oper_mode != previous_oper_mode[HO_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[HO_LOOP] = shrHO.oper_mode;
   }

   /* Secondary Haul Off Alarms */
   if (shrSHO.alarm_latch_bits != previous_alarm_latch_bits[SHO_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[SHO_LOOP] = shrSHO.alarm_latch_bits;
   }
   if (shrSHO.alarm_bits != previous_alarm_bits[SHO_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[SHO_LOOP] = shrSHO.alarm_bits;
   }
   if (shrSHO.oper_mode != previous_oper_mode[SHO_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[SHO_LOOP] = shrSHO.oper_mode;
   }

   /* Width Alarms */
   if (shrWTH.alarm_latch_bits != previous_alarm_latch_bits[WTH_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_latch_bits[WTH_LOOP] = shrWTH.alarm_latch_bits;
   }
   if (shrWTH.alarm_bits != previous_alarm_bits[WTH_LOOP])
   {
      alarm_change = TRUE;
      previous_alarm_bits[WTH_LOOP] = shrWTH.alarm_bits;
   }
   if (shrWTH.oper_mode != previous_oper_mode[WTH_LOOP])
   {
      alarm_change = TRUE;
      previous_oper_mode[WTH_LOOP] = shrWTH.oper_mode;
   }

   /* System Alarms */
   if (shr_global.alarm_latch_bits != previous_sys_alarm_latch_bits)
   {
      alarm_change = TRUE;
      previous_sys_alarm_latch_bits = shr_global.alarm_latch_bits;
   }
   if (shr_global.alarm_bits != previous_sys_alarm_bits)
   {
      alarm_change = TRUE;
      if ((shr_global.alarm_bits<<4) != (previous_sys_alarm_bits<<4))
      {
         shift += 1<<1;
      }
      previous_sys_alarm_bits = shr_global.alarm_bits;
   }

   if (BLN_OPER_MODE != previous_bln_alm_mode)
   {
      alarm_change = TRUE;
      previous_bln_alm_mode = BLN_OPER_MODE;
   }

   if (EXT_HO_OPER_MODE != previous_ext_alm_mode)
   {
      alarm_change = TRUE;
      previous_ext_alm_mode = EXT_HO_OPER_MODE;
   }

/*
   the following code is put in place to stop the acknowledging
   of the alarms every pass of check alarms when halt_alarm_
   processing is true. This code will insure that the next time
   halt_alarm_procssing is TRUE the alarms will be acknowledged
*/

   if (!shr_global.halt_alarm_processing && !ack_alarms_during_reset)
   {
      alarm_change = TRUE;
      ack_alarms_during_reset = TRUE;
   }

   //if (shift)
   //   j=1; //will be gone aftter ret
   return(alarm_change);
}


/************************************************************************/
/*
   Function:  ReviewAlarm()

   Description:  This function looks at an alarm condition and sets global
        variables defining what actions should be taken if any and what the
        current alarm state is occuring.
                                                                        */
/************************************************************************/
void ReviewAlarm(unsigned int alarm_index)
{
   config_super *cs = &cfg_super;

   unsigned int  loop;
   unsigned int  tempalarm;
   unsigned char device_type;
   unsigned char alarm_type;
   char          oper_mode;

   current        = FALSE;
   latched        = FALSE;
   current_action = NONE;
   blender_alarm  = FALSE;
   extruder_alarm = FALSE;

   parse_alarm(alarm_index, &alarm_type, &device_type, &loop);

   if (alarm_type == NOT_USED) 
      return;

   if (loop == ILLEGAL_LOOP) 
      return;

   SYS_ALM_MASK = sys_alarm_mask[alarm_type];

   /* alarms are listed in order of probability of occurrance to speed */
   /* up searching                                                     */
   switch(device_type)
   {
      case TYPE_SYSTEM:
          oper_mode = BLN_OPER_MODE;
          latched   = (shr_global.alarm_latch_bits & sys_alarm_mask[alarm_type]);
          current   = (shr_global.alarm_bits & sys_alarm_mask[alarm_type]);
          blender_alarm  = TRUE;
          extruder_alarm = TRUE;
          SYS_ALM_MASK   = sys_alarm_mask[alarm_type];
          alarmmsg_tableoffset = SYSALARM_TABLEOFFSET;
          break;

      case TYPE_BWH:
          oper_mode = shrBWH.oper_mode;
          latched   = (shrBWH.alarm_latch_bits & bwh_alarm_mask[alarm_type]);
          current   = (shrBWH.alarm_bits & bwh_alarm_mask[alarm_type]);
          blender_alarm        = TRUE;
          alarmmsg_tableoffset = BWHALARM_TABLEOFFSET;
          break;

      case TYPE_GATE:
          oper_mode = shrGATE[loop].oper_mode;
          latched   = (shrGATE[loop].alarm_latch_bits & gate_alarm_mask[alarm_type]);
          current   = (shrGATE[loop].alarm_bits & gate_alarm_mask[alarm_type]);
          blender_alarm = TRUE;
          alarmmsg_tableoffset = HOPALARM_TABLEOFFSET + (loop * 32);
          break;

      case TYPE_MIX:
          oper_mode = shrMIXER.oper_mode;
          latched   = (shrMIXER.alarm_latch_bits & mix_alarm_mask[alarm_type]);
          current   = (shrMIXER.alarm_bits & mix_alarm_mask[alarm_type]);
          blender_alarm = TRUE;
          alarmmsg_tableoffset = MIXALARM_TABLEOFFSET;
          break;

      case TYPE_LIW:
          oper_mode = shrEXT.oper_mode;
          latched   = (shrEXT.alarm_latch_bits & liw_alarm_mask[alarm_type]);
          current   = (shrEXT.alarm_bits & liw_alarm_mask[alarm_type]);
          extruder_alarm = TRUE;
          alarmmsg_tableoffset = LIWALARM_TABLEOFFSET;
          break;

      case TYPE_HO:
          oper_mode = shrHO.oper_mode;
          latched   = (shrHO.alarm_latch_bits & ho_alarm_mask[alarm_type]);
          current   = (shrHO.alarm_bits & ho_alarm_mask[alarm_type]);
          extruder_alarm = TRUE;
          if (loop == 0)
            alarmmsg_tableoffset = HO_ALARM_TABLEOFFSET;
          else
            alarmmsg_tableoffset = SHOALARM_TABLEOFFSET;
          break;

      case TYPE_FLF:
          oper_mode = shrGFLUFF.oper_mode;
          latched   = (shrGFLUFF.alarm_latch_bits & flf_alarm_mask[alarm_type]);
          current   = (shrGFLUFF.alarm_bits & flf_alarm_mask[alarm_type]);
          extruder_alarm = TRUE;
          alarmmsg_tableoffset = GFLFALARM_TABLEOFFSET;
          break;

      case TYPE_WTH:
          oper_mode = shrWTH.oper_mode;
          latched   = (shrWTH.alarm_latch_bits & wth_alarm_mask[alarm_type]);
          current   = (shrWTH.alarm_bits & wth_alarm_mask[alarm_type]);
          extruder_alarm = TRUE;
          alarmmsg_tableoffset = WTHALARM_TABLEOFFSET;
          break;

      default:
          return;
   }

   switch(oper_mode)
   {
      case PAUSE_MODE:
      case MONITOR_MODE:
          current_action = cs->AlarmAction[alarm_index].pause_action;
          break;
      case MAN_MODE:
          current_action = cs->AlarmAction[alarm_index].man_action;
          break;
      case AUTO_MODE:
          current_action = cs->AlarmAction[alarm_index].auto_action;
          break;
      default:
          current_action = NONE;
   }

   if ((debugFlags.alarmsys) && (alarm_index == 68))
      bgDbgInfo(&g_RingBuffer1, "alarm.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "device_type=%u oper_mode=%c current_action=%u\r\n", 
         device_type, oper_mode, current_action);

   /* check to see if the alarm has increased in severity */
   /* signaled action code has gotten numerically bigger  */
   
   if (current_action > (cs->AlarmAction[alarm_index].ack & (SYSTEM_SHUTDOWN|GENERAL_ALARM|INFORMATION_ONLY)))
      alarm_upgrade = TRUE;
   else
      alarm_upgrade = FALSE;

   if ((debugFlags.alarmsys) && (alarm_index == 68))
      bgDbgInfo(&g_RingBuffer1, "alarm.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "current_action=%u > ack=%u alarm_updrade=%d\r\n", 
         current_action, cs->AlarmAction[alarm_index].ack, alarm_upgrade);

   /* if no alarm, make sure ack bits are cleared so that remote works   */
   if (!latched && !current)
   {
      cs->AlarmAction[alarm_index].ack = 0;
      if ((debugFlags.alarmsys) && (alarm_index == 68))
         bgDbgInfo(&g_RingBuffer1, "alarm.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "latched=%u  current=%u ack=%u \r\n", 
            latched, current, cs->AlarmAction[alarm_index].ack);
  }


   /* calculate bit position of alarm for alarm message */
   if (current || latched)
   {
      if ((debugFlags.alarmsys) && (alarm_index == 68))
         bgDbgInfo(&g_RingBuffer1, "Current || latched", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "latched=%u  current=%u ack=%u \r\n", 
            latched, current, cs->AlarmAction[alarm_index].ack);
      if (current)
      {
         alarmbit  = 1;
         tempalarm = current;
         while (tempalarm > 1)
         {
            alarmbit++;
            tempalarm = tempalarm >> 1;
         }
      }
      else if (latched)
      {
         alarmbit = 1;
         tempalarm = latched;
         while (tempalarm > 1)
         {
            alarmbit++;
            tempalarm = tempalarm >> 1;
         }
      }
      //if (debugFlags.alarmsys)
      //   bgDbgInfo(&g_RingBuffer1, "alarm.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "alarmbit=%08x offset=0x%8x\r\n", alarmbit, alarmmsg_tableoffset);
      alarmbit += alarmmsg_tableoffset;

   if ((debugFlags.alarmsys) && (alarm_index == 68))
         bgDbgInfo(&g_RingBuffer1, "alarm.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "alarmbit=%08x\r\n", alarmbit);
   }
}

/************************************************************************/
/*
   Function:  parse_alarm()

   Description:  This function pulls the information from the AlarmAction
           table into seperate variables

           alarm_index  index in alarms database
           alarm_type   pointer to alarm_type
           loop         pointer to loop number
                                                                        */
/************************************************************************/
void parse_alarm(unsigned int alarm_index, unsigned char *alarm_type, unsigned char *device_type, unsigned int *loop)
{
   config_super      *cs = &cfg_super;
   structAlarmAction *alm_ptr;
   unsigned char     dt;
   unsigned char     device;

   alm_ptr      = &cs->AlarmAction[alarm_index];
   dt           = alm_ptr->device_type;
   device       = alm_ptr->device;
   *alarm_type  = alm_ptr->alarm_type;
   *device_type = dt;

   /* alarms are listed in order of probability of occurrance to speed */
   /* up searching                                                     */

   switch(dt)
   {
      case TYPE_SYSTEM:
        *loop = 0;
        break;
      case TYPE_BWH:
        *loop = WEIGH_HOP_LOOP;
        break;
      case TYPE_GATE:
        *loop = device;
        break;
      case TYPE_MIX:
        *loop = MIXER_LOOP;
        break;
      case TYPE_LIW:
        if (!device)
          *loop = EXT_LOOP;
        else
          *loop = GRAVIFLUFF_FDR_LOOP;
        break;
      case TYPE_HO:
        if (!device)
           *loop = HO_LOOP;
        else
           *loop = SHO_LOOP;
        break;
      case TYPE_FLF:
        *loop = GRAVIFLUFF_LOOP;
        break;
      case TYPE_WTH:
        *loop = WTH_LOOP;
        break;
      default:
        *loop = ILLEGAL_LOOP;
        break;
   }
}
/************************************************************************/
/************************************************************************/
int calc_demo_relay(int relay)
{
   int retcode = FALSE;

   switch(relay)
   {
      default:
         break;
   }
   return (retcode);
}

/***********************************************************************/
/*  add_smtp_queue_msg() - adds msg to queue                           */
/***********************************************************************/
void add_smtp_queue_msg(char *MsgOut, char *toggle)
{
   int i;

   for (i=0; i < MAX_SMTP_QUEUE_MSGS; i++)
   {
      if (SmtpQueue[i].ucUsed == 0)
      {
         strncpy(SmtpQueue[i].cBuffer, MsgOut, sizeof(SmtpQueue[i].cBuffer));
         strncat(SmtpQueue[i].cBuffer, toggle,6);
         SmtpQueue[i].ucUsed = 1;
         break;
      }
   }
   return;
}

