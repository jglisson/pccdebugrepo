#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  ho_pcc.c

   Description:


      $Log:   F:\Software\BR_Guardian\ho_pcc.c_v  $
 *
 * 2008/12/01 JLR
 *  added/changed debug telnet stuff
 *  use debugFlags.hoPcc to toggle debug information in this file
 *
 *    Rev 1.4   Aug 25 2008 17:24:08   gtf
 * #if DEBUG_HO_PCC = 0
 *
 *    Rev 1.3   Jul 31 2008 10:57:12   jlr
 * fixed an oops where I'd put a DBGDBG line
 * above a declaration in fuction add_len
 *
 *    Rev 1.2   Jul 31 2008 10:36:56   jlr
 * added some debug information
 *
 *    Rev 1.1   Apr 02 2008 10:24:34   FMK
 * The HO loop now builds. The NetCommand function
 * and AB_PLC control is not yet finished though.
 *
 *    Rev 1.0   Apr 01 2008 09:58:12   FMK
 * Initial version

      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
/*#include "globalinc.h"*/
//
#include <bur/plctypes.h>
#include "pcc_math.h"
#include <astime.h>
#include <string.h>
#include <math.h>

//#include "signal_pcc.h"
//#include "relaydef.h"
//#include "plc.h"
//#include "alarm.h"
//#include "vars.h"
//#include <string.h>
//#include <math.h>
//#include "demo.h"
//#include "hmi.h"
//#include "file_rw.h"
//#include <fileio.h>
//#include "asusb.h"
//#include "usb.h"
//#include "version.h"
//#include "gate_pcc.h"
//#include "recipe.h"

#include "plc.h"
#include "relaydef.h"
#include "sys.h"
#include "exd_drive.h"
#include "mem.h"
#include "libbgdbg.h"
#include "signal_pcc.h"
#include "alarm.h"
#include "vars.h"
#include "net.h"
#include "demo.h"
#include "recipe.h"
#include "hmi.h"

#define PLC_5_READ   0x68
#define PLC_5_WRITE  0x67
#define TIME_BETWEEN_LOAD_OFF_ON  500
#define INT             0
#define FLOAT           1
#define LOAD_ON         258             /* turn loading relay on (wm)   */
#define LOAD_OFF        259             /* turn loading relay off (wm)  */
#define LOW_ON          260             /* turn low alarm on (wm)       */
#define LOW_OFF         261             /* turn low alarm off (wm)      */
#define STANDARD_TIMEBASE       1228800 /* standard wm,exd timebase     */
#define AB_PLC_TIMEBASE       100 /* standard wm,exd timebase     */
#define EXD_DEAD (s->alarm_bits & EXD_TIMEOUT)
#define Float_NEQ(A, B) (fabs((double)(A-B))>.0003)
#define Float_EQ(A, B) (fabs((double)(A-B)) < 0.0003)
#define WM_DEAD (s->alarm_bits & WM_TIMEOUT)
#define Connection_timed_out 27160
#define POINTS_IN_RS_TABLE  7
#define EXT_SPD_DIFF  0.05

#define DEBUG_EXT_PCC 0  /* define for debug data JLR */
#define Float_NEQ(A, B) (fabs((double)(A-B))>.0003)
#define OVF_INT         32767        /* overflow value for signed integer */
#define REFEED_ACT_WTP  g->actual.refeed_wtp

extern TIME start_time(void);
extern unsigned int elap_time(TIME iTime);
extern void SendUnitDataCommand(int chan, int FNC, short FileNum, short RegNum, short ElementSize, int DataType);
extern void RecvSendUnitDataResponse(int chan, int FNC);
extern void OpenRemoteSocketPLC(int PortVal, UDINT HostAddr, unsigned char unit_id, int chan);
extern void SendRegSessionCommand(int chan);
extern void RecvRegSessionResponse(int chan);
extern void SendRRData(int chan);
extern void RecvSendRRDataResponse(int chan);
extern void BuildPLCEthAddress();

//_LOCAL  unsigned int extruder_ramptime;
_LOCAL unsigned int extruder_ramptime_plc;	   /* in seconds */
_LOCAL unsigned int current_time;

void CLEAR_ALARM(unsigned int);
void SET_ALARM(unsigned int);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

#ifdef ALLEN_BRADLEY_DRIVE
void init_ethernet(void);
#endif
void reset_drive(void);
void init_dev();
void EXT_PCC(void);
void check_system();
void totals(void);
void auto_mode(void);
void manual_mode(void);
void pause_mode(void);
void handle_drive_inhibit(void);
void coast_totals(void);
void check_coast(void);
void set_update_flags(void);
int ready_to_update(void);
float percent_error(float a, float b);
void update(void);
void calc_new_initial_rate_speed(void);
void net_command(int cmd);
void set_initial_speed(void);
void monitor_mode(void);
void send_plc_alive_flag(void);
void reset_wm(void);
void check_weights(void);
int check_weight(int type, float wt_limit, unsigned int *timer, unsigned int time_limit);
void print_ext_data(void);
void add_wt(float wt);
void poke(void);
void set_exd_status(void);
void Check_ManualBackup_EXT();
REAL fGetSpeedFromTable(REAL fSpd);
void vStoreRS_Value(REAL fRate, REAL fSpeed);

extern int  SocketNew, ethnetpn;
extern char connectedPLC;
extern char ConnectedPLC_Flag;

extern unsigned char inbfr[256], outbfr[256];
extern int counter;
extern unsigned char session_handle[4];
extern int   PLC_WRITTEN_INT_DATA[10],   PLC_READ_INT_DATA[10];
extern float PLC_WRITTEN_FLOAT_DATA[10], PLC_READ_FLOAT_DATA[10];
unsigned char Encap_Header[24];
int recv_length;
//unsigned long HostAddr;
//unsigned char ip[4];

extern unsigned char gDnum;
extern unsigned char gDev_Init;
extern UINT EthError;

extern void ClosePLCRemoteSocket(void);

float ovf;                              /* overflow detector value      */
int dnum;                               /* 'dev' number for net         */
float last_set_wtp = 0.0;               /* last set wtp for rs clearing */
float last_set_spd = 0.0;               /* last set spd for rs clearing */

_LOCAL unsigned int ext_outofspec_counter;
unsigned int exd_timebase = 0;          /* timebase of exd              */
int local_wtp_mode;
_LOCAL int numbad;                             /* number of bad updates        */
unsigned int ramptime;                  /* ramptime in TICKS            */
_LOCAL unsigned int out_of_spec_count;        /* update # for when to check   */
_LOCAL char reset_rate_speed;          /* flag to restart rate speed   */
char process_command = FALSE;
unsigned int rampstart;
float min_per_tbit = 0.0;               /* mins per time bit factor     */

char request_for_update = FALSE;
unsigned char demo = FALSE;
char process_command;
float    last_grav_hop_wt = 0.0;        /* last gravimetric mode hop wt */
unsigned int max_ad_value;              /* maximum ad value for overwt  */
char cal_flag;                          /* weigh module in cal */
char low_dump_count = 0;                /* # of low dumps counter       */
/* timer to ensure there are TIME_BETWEEN_LOAD_OFF_ON  ticks between load_oof and next load_on */
_LOCAL unsigned int load_between_OFF_and_ON_timer;
_LOCAL float act_rsfact;
_LOCAL unsigned char rs_num;
_LOCAL float update_error;
//_LOCAL float test_read_float[10];
//_LOCAL unsigned int ethernet_reset_timer; 
//_LOCAL unsigned char good_msgs_snapshot; 
//_LOCAL unsigned char goodmsg_updater;

unsigned int wm_timebase = 0;           /* timebase of wm               */
float last_act_spd = 0.0;
unsigned int act_spd_bits;  /* Actual bits returned from last read of extruder speed */
char remote_calibrate_zero;
char remote_calibrate_test;
char remote_calibrate_clear;
int wtbits;
int testwt;
int zerowt;
float test_wt;
float wtperbit;
unsigned int timebits;
int addr, chan;
unsigned int timebase;
float bitsperwt;

char low = FALSE;                       /* flag to say we are low       */
char crit_low = FALSE;                  /* flag to say we are crit. low */
char try_recalibration = FALSE;         /* Indicate that recal needs to be tried */
unsigned int recal_timer = 0;           /* continue to try recal once per minute */

_GLOBAL unsigned int grav_timer;        /* timer for % grav             */
_GLOBAL unsigned int coast_timer;       /* timer for coast alarms       */
unsigned int no_update_timer;           /* timer for no updates         */
unsigned int sync_timer;                /* check for no sync            */
char sync_on = FALSE;                   /* flag for sync is on          */
char no_update_timer_on = FALSE;        /* flag for update timer active */
float    dump_size[5];                  /* last 5 good dumps            */
float    dump_sum = 0.0;                /* sum of dum_size[ ]           */
unsigned char dump_num = 0;             /* number of pts in dump_size   */
unsigned char dump_last = 0; 
unsigned int max_ad_value;              /* maximum ad value for overwt  */
float hr_per_tbit = 0.0;                /* hrs per time bit factor      */
unsigned int last_ramptime;
unsigned int crit_low_timer;            /* timer for critical low detect*/
unsigned int low_timer;                 /* timer for low detect         */
unsigned int overweight_timer;          /* timer for overweight detect  */
unsigned int underweight_timer;         /* timer for underweight detect */
unsigned int load_wt_on_timer;          /* timer for loading detect     */
unsigned int load_timer;                /* timer for loading time mode  */
//_LOCAL unsigned int ticks; 
//_LOCAL unsigned int passed_ticks; 
_LOCAL unsigned int interval_timer;
_LOCAL float wt;
_LOCAL float hours;
_LOCAL unsigned int tick1s; 
_LOCAL unsigned int passed_tick1s; 
//_LOCAL char to_read_from_plc;

/* global variable for debug information */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

//_LOCAL float expected;
_LOCAL int poke_update_timer;
config_super *cs_=&cfg_super;
//_LOCAL float last_set_spd_plc;
//_LOCAL int plc_alive_count;                /* send alive flag to plc every 30 times thru this loop */

shared_LIW_device *s = &shrEXT;
config_LIW_device *c = &cfgEXT;
global_struct *g = &shr_global;

//_LOCAL unsigned char connection_counter;

_GLOBAL wtp_data_str wtp_data;

_GLOBAL float fExtUpdateSeconds;


/************************************************************************/
/* EXT_PCC()                                                             */
/************************************************************************/
void EXT_PCC(void)
{         		
	if (gDev_Init == FALSE)
	{
		init_dev();
		gDev_Init = TRUE;
	}
         
   #ifdef ALLEN_BRADLEY_DRIVE
	if (c->drive_type == AB_PLC_DRIVE) 
	{
		send_plc_alive_flag();
	}
   #endif
      
	current_time = start_time();
	check_system();
	handle_drive_inhibit();
	
	if ((cfg_super.wtp_mode == CONTROL) && (cfgEXT.demo & DEMO_DRIVE_MOD))
		Check_ManualBackup_EXT();
	
	if (cfgEXT.demo & DEMO_DRIVE_MOD)
	{
		CLEAR_ALARM(EXD_IN_MANUAL|EXD_INHIBIT);
	}
   
	if (shrEXT.reset_wm || (wm_timebase == 0))
		reset_wm();

	if (shrEXT.reset_drive || ((cs_->wtp_mode == CONTROL) && (exd_timebase == 0)))
	{
		if (debugFlags.extLoop)
		{
			bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
				"about to reset drive, exd_timebase=%d\r\n", exd_timebase);
		}
		reset_drive();
	}
   
	if (shrEXT.clear_inven)
	{
		PMEM.EXT.InvTotal = 0.0;
		shrEXT.clear_inven = FALSE;
	}

	if (shrEXT.clear_shift)
	{
		PMEM.EXT.ShftTotal = 0.0;
		shrEXT.clear_shift = FALSE;
	}
   
	switch (shrEXT.oper_mode)
	{
		case AUTO_MODE:
			auto_mode();
			break;
		case MAN_MODE:
			manual_mode();
			break;
		case PAUSE_MODE:
			pause_mode();
			break;
		case MONITOR_MODE:
			monitor_mode();
			break;
	}
   
	/* #if ((MACH==WX_BATCH)||(MACH==WE_BATCH)) */

	if (remote_calibrate_zero)
	{
		shrEXT.remote_calibration_status = 0;
#if (MACH != GRAVITROL)
		if (g->bln_oper_mode != PAUSE_MODE)
#else
		/*if ((shr_LOOP[dnum].LIW->act_spd != 0.0) && (shr_LOOP[dnum].LIW->set_spd != 0.0))*/
		if (Float_NEQ(shrEXT.act_spd, 0.0) && Float_NEQ(shrEXT.set_spd, 0.0))
#endif
		{
			cfgEXT.cal_done = FALSE;
			shrEXT.remote_calibration_status |= REM_CAL_RUNNING;
		}
		else
		{
#if ((MACH==WX_BATCH)||(MACH==WE_BATCH))
        /* only for GUARDIAN PROFACE's new calibration */
        g->rem_calib_zero_scale_done = TRUE;
        g->rem_calib_auto_tare_done = TRUE;
#endif     
			addr = cfgEXT.weigh_mod.addr;
			chan = cfgEXT.weigh_mod.chan;

			shrEXT.remote_cal_zero_done = FALSE;
			shrEXT.remote_cal_all_done = FALSE;
			timebase = g->stats[addr].timebase;

			/* remote_cal_bits(addr, chan,timebase);*/
			if (!g->unable_to_talk_weigh_mod)
			{
				zerowt = wtbits;
				shrEXT.remote_cal_zero_done = TRUE;
			}
			else
				shrEXT.remote_calibration_status |= REM_CAL_NO_COM;
		}
		remote_calibrate_zero = FALSE;
	}

	if (shrEXT.remote_cal_zero_done && remote_calibrate_test)
	{
		shrEXT.remote_cal_zero_done = FALSE;
    
		test_wt = cfgEXT.test_wt;

		/* remote_cal_bits(addr, chan, timebase); */
		if (!g->unable_to_talk_weigh_mod)
		{
			testwt = wtbits;

			bitsperwt = (float)(testwt - zerowt) / test_wt;

			if (bitsperwt < 50.0)
			{
				cfgEXT.wtperbit = 0.0;
				cfgEXT.cal_done = FALSE;
				shrEXT.remote_calibration_status |= REM_CAL_NO_WEIGHT;
			}  
			else
			{
				wtperbit = test_wt/(float)(testwt - zerowt);
				cfgEXT.testwt = testwt;
				cfgEXT.zerowt = zerowt;  
				cfgEXT.wtperbit =  wtperbit;
				cfgEXT.cal_done = TRUE;
				cfgEXT.rs_num = 0;
				shrEXT.remote_cal_all_done = TRUE;
			}
		}
		else
			shrEXT.remote_calibration_status |= REM_CAL_NO_COM;

		remote_calibrate_test = FALSE;
	}

	if (remote_calibrate_clear)
	{
		shrEXT.remote_calibration_status = 0;
		remote_calibrate_clear = FALSE;
	}
}

/************************************************************************/
/* init_dev() - setup the device                                        */
/************************************************************************/
void init_dev()
{
	//unsigned long HostAddr; 
	int i;
  
	switch(gDnum)
	{
		case EXT_LOOP:
			local_wtp_mode = cs_->wtp_mode;
			s = &shrEXT;
			c = &cfgEXT;
			break;
		default:
			local_wtp_mode = cs_->wtp_mode;
			s = &shrEXT;
			c = &cfgEXT;
	}
   
	shrEXT.oper_mode = PAUSE_MODE;
	Ext_running_mode = NEW_MODE;
	DriveCommand[gDnum].CMDRdy = TRUE;
   
	low = FALSE;
	crit_low = FALSE;
	s->reset_wm = TRUE;
	s->reset_drive = TRUE;
	demo = c->demo;
	g->stats[c->weigh_mod.addr].cal_counter = 0;
   
   poke_update_timer = 300;
	max_ad_value = 0;     /* this is set below in check_sw_rev()          */
	numbad = 0;
	ramptime = 0;
	out_of_spec_count = 0;
	ext_outofspec_counter = 0;
	no_update_timer_on = FALSE;
	reset_rate_speed = FALSE;
		     
	if (c->avg_dump_size > 0.0)
	{
		for (i=0; i<5; i++)
			dump_size[i] = c->avg_dump_size;
		dump_num = 5;
		dump_sum = (float) dump_num * c->avg_dump_size;
	}
	else
	{
		for (i=0; i<5; i++)
			dump_size[i] = 0.0;
		dump_num = 0;
		dump_sum = 0.0;
	}
	dump_last = 0;
	s->set_spd = 0.0;
	s->act_spd = 0.0;
	last_act_spd = 0.0;
	s->set_wtp = 0.0;
	s->act_wtp = 0.0;
	s->have_new_act = FALSE;
	s->rate_uses_spd = FALSE;
	reset_drive();
	reset_wm();
	
	init_mvg_avg(&wtp_data.rs_avg, c->movepts, &wtp_data.rs_pts[0], c->rs_maxchg);

	// check_sw_rev(c->weigh_mod.addr,&s->alarm_bits,&s->alarm_latch_bits,WM_INCOMPATABILITY);
	// check_sw_rev(c->drive_mod.addr,&s->alarm_bits,&s->alarm_latch_bits,EXD_INCOMPATABILITY);

	shrEXT.rate_uses_spd = (c->rs_num > 0);
	reset_drive();                        /* reset drive functions        */

	//  if(c->drive_type==AB_PLC_DRIVE)
	//   {
	//      
	//      brsmemset(&HostAddr, 0, sizeof(HostAddr)); 
	//      brsmemcpy(&HostAddr, cfgEXT.drive_ip_address, sizeof(cfgEXT.drive_ip_address));
	//
	//      OpenRemoteSocketPLC(0xaf12, HostAddr, 1, c->drive_mod.chan);
	//
	//      if (connectedPLC)
	//      {
	//         SendRegSessionCommand(c->drive_mod.chan);
	//         RecvRegSessionResponse(c->drive_mod.chan);
	// /*        tsleep(3);*/
	//         SendRRData(c->drive_mod.chan);
	//         RecvSendRRDataResponse(c->drive_mod.chan);
	//      }
	//      exd_timebase = AB_PLC_TIMEBASE;
	//#if 0
	//   if (connectedPLC)
	//      printf("ext_pcc: chan = %d connected, counter = %d\n", c->drive_mod.chan, counter);
	//   else
	//      printf("ext_pcc: chan = %d NOT CONNECTED, counter = %d\n", c->drive_mod.chan, counter);
	//#endif
	//
	//   }
}

/************************************************************************/
/* poke() - gets the pulses & exd speed                                 */
/************************************************************************/
void poke(void)
{
   net_command(GET_WEIGHT);        
////   if (local_wtp_mode == CONTROL)
////   {
   net_command(GET_SPEED);
////   }
}
/************************************************************************/
/* auto_mode() - automatic lenght throughput control loop               */
/************************************************************************/
void auto_mode(void)
{
  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
       "in auto mode\r\n");
  }
  if (s->ramping)
  {
     no_update_timer_on = FALSE;
     if ((elap_time(rampstart) > ramptime * 1000))
     { 
/*      if (cfgEXT.drive_type != AB_PLC_DRIVE)*/
        s->ramping = FALSE;
        totals();
     }
  }
  else if (!no_update_timer_on)
  {
     no_update_timer_on = TRUE;
     no_update_timer = current_time;
  }

  switch (Ext_running_mode)
  {
    case AUTO_MODE:
      if (debugFlags.extLoop)
      {
        bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
            BG_DEBUG_LEVEL_DEBUG, "case AUTO_MODE\r\n");
      }
         
    if (!tick1s)
       tick1s = current_time;
    passed_tick1s = elap_time(tick1s);
   
    if (passed_tick1s > (unsigned int)poke_update_timer)	    /* add poke update timer here to adjust the frequency of poking */ 
    {	        
      poke();
      tick1s = 0;
       
      if (c->cal_done)
      {
         check_coast();
         if (!s->coast && !s->ramping)
         {
            if (ready_to_update())
               update();
         }
      } 
    } 
    break;
    
	 case NEW_MODE:
      if (debugFlags.extLoop)
      {
        bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
           BG_DEBUG_LEVEL_DEBUG, "case NEW_MODE\r\n");
      }
      if (s->coast)
        c->accum_coast = c->accum_coast + (float) elap_time(grav_timer);
      else
        c->accum_grav  = c->accum_grav  + (float) elap_time(grav_timer);
	
	
    case PAUSE_MODE:                                    /*lint -e616    */
    case MONITOR_MODE:
      if (debugFlags.extLoop)
      {
        bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
            BG_DEBUG_LEVEL_DEBUG, "case MONITOR_MODE\r\n");
      }
     case MAN_MODE:
      if (debugFlags.extLoop)
      {
        bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
            BG_DEBUG_LEVEL_DEBUG, "case MAN_MODE\r\n");
      }
      grav_timer = current_time;

      if (s->coast)
        coast_totals();
      else
        totals();
      set_initial_speed();
      net_command(SET_SPEED);
#if 0
      s->act_wtp = 0.0;
#endif
      Ext_running_mode = AUTO_MODE;
      s->accel = c->accel;      /* for correction, use device factors   */
      s->decel = c->decel;
      out_of_spec_count = 0;    /* restart count on any process change  */
      ext_outofspec_counter = 0;
      CLEAR_ALARM(DEV_OUT_OF_SPEC);
      break;                                            /*lint +e616    */
  }
}
/************************************************************************/
/* monitor_mode() - monitors the extruder speed only - no control           */
/************************************************************************/
void monitor_mode(void)
{
  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  if (fabs((double)percent_error(s->act_spd, last_act_spd)) > c->dead_band)
  {
    last_act_spd = s->act_spd;
    /*s->ramping = TRUE;*/
    rampstart = current_time;
  }
  
  if (s->ramping)
  {
    no_update_timer_on = FALSE;
    if (elap_time(rampstart) > 5*TICKSPERSEC)
    {
      s->ramping = FALSE;
      totals();
    }
  }
  else if (!no_update_timer_on)
  {
    no_update_timer_on = TRUE;
    no_update_timer = current_time;
  }
  
  switch (Ext_running_mode)
  {
     case MONITOR_MODE:
        poke();
        if (c->cal_done)
        {
           check_coast();
           if (!s->coast && !s->ramping)
           {
              //if (ready_to_updateMM())  //test
              if (ready_to_update())      //original
                 update();
           }
        }
     break;

     case PAUSE_MODE:
     case NEW_MODE:
     case MAN_MODE:
     case AUTO_MODE:
        Ext_running_mode = MONITOR_MODE;
        CLEAR_ALARM(DEV_OUT_OF_SPEC);
        totals();
     break;
  }
}

/************************************************************************/
/* manual_mode() - sets the motor speeds & monitors ltp                 */
/************************************************************************/
void manual_mode(void)
{
	if (debugFlags.extLoop)
	{
		bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
			BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	if (s->ext_manual_backup == AUTO_CONTROL)
	{
		if (s->ramping)
		{
			no_update_timer_on = FALSE;
			if ((elap_time(rampstart) > ramptime * 1000))
			{
				/*      if (cfgEXT.drive_type != AB_PLC_DRIVE) */	
				s->ramping = FALSE;
				totals();
			}
		}
		else if (!no_update_timer_on)
		{
			no_update_timer_on = TRUE;
			no_update_timer = current_time;
		}
	}
	else
		no_update_timer_on = FALSE;

	switch (Ext_running_mode)
	{
		case MAN_MODE:       
			poke();  
    
			if ((s->speed_ctrl == EXTERNAL_CTRL) && (s->ext_manual_backup == AUTO_CONTROL))
				s->set_spd =  (float) (PLC.AnaIn.AnaInp[0].Value / 32768);
			else if (s->ext_manual_backup == MANUAL_BACKUP)
			{
				IO_dig[0].channel[EXTManual_Out_0] = OFF;
				IO_dig[0].channel[EXTManual_Out_1] = OFF;
			}
			
			if ((s->set_spd == 0.0) && (c->zero_crossing >= 0) && (s->ext_manual_backup == AUTO_CONTROL))
				s->act_wtp = 0.0;
			else
			{
				if (c->cal_done)
				{
					check_coast();
					if (!s->coast && !s->ramping)
					{
						if (ready_to_update())
							update();
					}
				}
			}      
			break;
		case PAUSE_MODE:
			c->rs_num = 0;
		case MONITOR_MODE:                                    /*lint -e616    */
		case AUTO_MODE:
			s->coast = 0;
			s->diff_bits = 0;
		case NEW_MODE:
			if ((s->speed_ctrl == EXTERNAL_CTRL) && (s->ext_manual_backup == AUTO_CONTROL))
				s->set_spd =  (float) (PLC.AnaIn.AnaInp[0].Value / 32768);
			else if (s->ext_manual_backup == MANUAL_BACKUP)
			{
				IO_dig[0].channel[EXTManual_Out_0] = OFF;
				IO_dig[0].channel[EXTManual_Out_1] = OFF;
			}
#if 0
			s->act_wtp = 0.0;
#endif
			s->set_wtp = 0.0;
			totals();
			net_command(SET_SPEED);
			Ext_running_mode = MAN_MODE;
			CLEAR_ALARM(DEV_OUT_OF_SPEC);
			break;                                            /*lint +e616    */
	}
}
/************************************************************************/
/* pause_mode() - pauses the control loop and stops measuring ltp       */
/************************************************************************/
void pause_mode(void)
{
	if (debugFlags.extLoop)
	{
		bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	switch (Ext_running_mode)
	{
		case PAUSE_MODE:
			poke();
			
			if (s->ext_manual_backup == MANUAL_BACKUP)
			{
				no_update_timer_on = FALSE;
				IO_dig[0].channel[EXTManual_Out_0] = OFF;
				IO_dig[0].channel[EXTManual_Out_1] = OFF;
			
				if (c->cal_done)
				{
					check_coast();
					if (!s->coast && !s->ramping)
					{
						if (ready_to_update())      //original
							update();
					}
				}
			}
			else if (s->ramping)                           /* still slowing down   */
			{
				no_update_timer_on = FALSE;
				if (elap_time(rampstart)>ramptime * 1000)      /* done    */
				{
					s->ramping = FALSE;
					if (c->cal_done)                      /* don't worry w/coast  */
						totals();
				}
				else                                    /* not stopped yet      */
				{
					if (c->cal_done)
						check_coast();                      /* add in delta_b       */
				}
			}
			else if ((s->speed_ctrl == EXTERNAL_CTRL) && (s->ext_manual_backup == AUTO_CONTROL) &&
				!(shr_global.alarm_latch_bits & SYS_SHUTDOWN) && !shr_global.forced_pause )
			{
				s->oper_mode = MAN_MODE;			      
				Ext_running_mode = NEW_MODE;
				shrEXT.init_man_extspd = 1;
			}
			else
			{
				s->act_wtp = 0.0;   /* now not running      */
				s->rate_uses_spd = FALSE;
				CLEAR_ALARM(DEV_OUT_OF_SPEC|DEV_OVERSPEED|DEV_UNDERSPEED|DEV_EXCESSIVE_COAST|DEV_DRV_SYS_FAILURE);
				try_recalibration = FALSE;
				s->coast = 0;
				s->drive_fail_count = 0;
				no_update_timer_on = FALSE;
			}
			break;

		case MONITOR_MODE:                                  /*lint -e616    */
			s->set_wtp = s->act_wtp = s->set_spd = s->act_spd = 0.0;
			s->rate_uses_spd = FALSE;
			Ext_running_mode = PAUSE_MODE;
			last_act_spd = 0.0;
			break;

		case NEW_MODE:
		case MAN_MODE:
		case AUTO_MODE:
			s->set_wtp = 0.0;
			/* s->set_spd = 0.0; */
			net_command(SET_SPEED);
			CLEAR_ALARM(DEV_OUT_OF_SPEC|DEV_OVERSPEED|DEV_UNDERSPEED|DEV_EXCESSIVE_COAST|DEV_DRV_SYS_FAILURE);
			s->coast = 0;
			Ext_running_mode = PAUSE_MODE;
			break;                                            /*lint +e616    */
	}
}

/************************************************************************/
/* this handles going from drv inhibit to not w/ an actual speed > 0    */
/************************************************************************/
void handle_drive_inhibit(void)
{
 
  static unsigned int in_drive_inhibit = FALSE;
  float spd,f;
  unsigned int new_ramptime, new_rampstart;

  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  if (s->set_spd > s->act_spd)
    spd = s->set_spd;
  else
    spd = s->act_spd;
  
  if (in_drive_inhibit && !(s->alarm_bits & EXD_INHIBIT) && (spd > 0.0))
  {
    if ((f=c->spd_factor*c->accel) > 0.0)
      new_ramptime = (unsigned int)((float)((spd * (float)100.0 * FTICKSPERSEC)/f));
    else
      new_ramptime = 10*TICKSPERSEC;
    rampstart = current_time;
	
	 if (!s->ramping)
    {
      /*s->ramping = TRUE;*/
      rampstart = current_time;
      ramptime = new_ramptime;
    }
    else if (new_rampstart+new_ramptime > rampstart+ramptime)
    {
      ramptime = new_ramptime;
      rampstart = new_rampstart;
    }
	
    no_update_timer_on = FALSE;
  }
  
  in_drive_inhibit = s->alarm_bits & EXD_INHIBIT;
	
	if (cfgEXT.demo & DEMO_DRIVE_MOD)
		CLEAR_ALARM(EXD_INHIBIT);

	if ((cfg_super.wtp_mode == CONTROL) && (!cfgEXT.demo & DEMO_DRIVE_MOD))
	{
		if (c->drive_type != PCC)
		{
			if (c->drive_inhibit_invert)
			{
				if (!IO_dig[0].channel[DriveInh_Ext]) 
					CLEAR_ALARM(EXD_INHIBIT);
				else if (!(s->alarm_bits & EXD_INHIBIT))
					SET_ALARM(EXD_INHIBIT);
			}
			else
			{
				if (IO_dig[0].channel[DriveInh_Ext])
					CLEAR_ALARM(EXD_INHIBIT);
				else if (!(s->alarm_bits & EXD_INHIBIT))
					SET_ALARM(EXD_INHIBIT);
			}
		}
	}
}
/************************************************************************/
/* check_system() -                                                     */
/************************************************************************/
void check_system()
{
	char  oper_mode;
	float f1;

	if (debugFlags.extLoop)
	{
		bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	oper_mode = LiwCommand[0].Command;

	if (c->movepts < 10)
		c->algorithm = STANDARD;
	else
		c->algorithm = STATISTIC;
	  
	if (LiwCommand[0].CommandPresent)
	{
		switch(LiwCommand[0].Command)
		{
			case PAUSE_MODE:
				if (LiwCommand[0].SetAccelDecel < 0.0001)              /* no decel factor included    */
				{
					/* use device a/d factors       */
					s->decel = c->decel;
				}
				s->set_spd = 0;
				s->have_new_act = FALSE;
				shrEXT.oper_mode = PAUSE_MODE;
				//G2-539
				CLEAR_ALARM(DEV_OUT_OF_SPEC|DEV_OVERSPEED|DEV_UNDERSPEED|DEV_EXCESSIVE_COAST|DEV_DRV_SYS_FAILURE|WT_LOW|WT_CRIT);
				break;
			case AUTO_MODE:
				f1 = LiwCommand[0].SetWTP;
				if (f1 > 0.001)
				{
					if ((fabs((double)percent_error(s->set_wtp,f1)) > .005) || (Ext_running_mode != AUTO_MODE))
					{
						if (c->rs_resetchg > 0.0)
						{
							if (fabs((double)percent_error(f1,last_set_wtp)) > c->rs_resetchg)
								reset_rate_speed = TRUE;
						}
						if (BATCH_RECIPE_NOW->step_size > 0.0)
							reset_rate_speed = TRUE;
						
						s->set_wtp = f1;
						s->act_wtp = f1;
						init_mvg_avg(&wtp_data.rs_avg, c->movepts ,&wtp_data.rs_pts[0], c->rs_maxchg);
						ma_add_pt(&wtp_data.rs_avg, f1);
						
						if (Ext_running_mode == AUTO_MODE)
							Ext_running_mode = NEW_MODE;
					}
              
					reset_rate_speed = TRUE;
					if (LiwCommand[0].SetAccelDecel >= 0.0 )
						s->decel = LiwCommand[0].SetAccelDecel;
              
					if (LiwCommand[0].SetAccelDecel >= 0.0)
						s->accel = LiwCommand[0].SetAccelDecel;
				}
				s->have_new_act = FALSE;
				break;
			case MAN_MODE:
				if (LiwCommand[0].SetSpeed < 0.001)              /* no speed included    */
				{
					/* f1 = s->act_spd; */
					f1= 0;
					s->accel = c->accel;          /* use device a/d factors       */
					s->decel = c->decel;
				}
				else
				{
					f1 = (float)LiwCommand[0].SetSpeed;

					if (LiwCommand[0].SetAccelDecel >= 0.0)
						s->decel = LiwCommand[0].SetAccelDecel;
					else
						s->decel = c->decel;

					if (LiwCommand[0].SetAccelDecel >= 0.0)
						s->accel = LiwCommand[0].SetAccelDecel;
					else
						s->accel = c->accel;
				}

				if (Float_NEQ(f1,s->set_spd) || ((c->zero_crossing < 0) && (f1 == 0.0)))
				{
					if (c->rs_resetchg > 0.0)
					{
						if (fabs((double)percent_error(f1,last_set_spd)) > c->rs_resetchg)
							reset_rate_speed = TRUE;
					}
					s->set_spd = f1;
					if (Ext_running_mode == MAN_MODE)
						Ext_running_mode = NEW_MODE;
				}
				reset_rate_speed = TRUE;
				s->have_new_act  = FALSE;
				break;
			case MONITOR_MODE:
				s->have_new_act = FALSE;
				break;
			case REINIT:
				shrEXT.reset_drive = TRUE;
				break;
			default: /* bad command */
				return;
		}
		/* insure accel & decel not fried here                              */
		if ((shrEXT.accel < 0.0) || (shrEXT.accel > c->decel))
			shrEXT.accel = c->accel;
		if ((shrEXT.decel < 0.0) || (shrEXT.decel > c->decel))
			shrEXT.decel = c->decel;

		LiwCommand[0].CommandPresent = FALSE;
		switch(oper_mode)
		{
			case MAN_MODE:
				if ((shrEXT.set_spd == 0.0) && (shrEXT.act_spd == 0.0) && (c->zero_crossing >= 0.0))
				{
					oper_mode = PAUSE_MODE;
					LiwCommand[0].Command = PAUSE_MODE;
					LiwCommand[0].CommandPresent = TRUE;
				}
				break;
			case AUTO_MODE:
				if ((shrEXT.set_wtp == 0.0) && (shrEXT.set_spd == 0.0) && (shrEXT.act_spd == 0.0))
				{
					oper_mode = PAUSE_MODE;
					LiwCommand[0].Command = PAUSE_MODE;
					LiwCommand[0].CommandPresent = TRUE;
				}
				break;
			default:
				break;
		}
		//      if (LiwCommand[0].Command == AUTO_MODE)
		//      shrEXT.oper_mode = LiwCommand[0].Command;   /* must do here - keep alarms off       */
		shrEXT.oper_mode = oper_mode;
	}
	else
		process_command = FALSE;

	if (s->set_wtp > 0.0)
		last_set_wtp = s->set_wtp;
	if (s->set_spd > 0.0)
		last_set_spd = s->set_spd;
   
	shrEXT.alarm_bits |= shrEXT.alarm_force_bits;
	shrEXT.alarm_latch_bits |= shrEXT.alarm_force_bits;
   
	fExtUpdateSeconds = ((float)s->delta_t / FTICKSPERSEC );  //divide
}

/************************************************************************/
/* reset_drive() - resets various flags & gets set for when board       */
/*  comes back on line                                                  */
/************************************************************************/
void reset_drive(void)
{
  if (debugFlags.extLoop)
  {
     bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
     BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (local_wtp_mode == CONTROL)
   {
      switch (c->drive_type)
      {
         case PCC:
            net_command(SET_ACCEL);
            net_command(SET_DECEL);
            if (!EXD_DEAD)
            {
               if (s->speed_ctrl != EXTERNAL_CTRL)
               {
                  s->accel = 0.0;                 /* make sure done immediately   */
                  s->decel = 0.0;
                  net_command(SET_SPEED);         /* reset speed                  */
               }
            }
            if (!EXD_DEAD)
            {
               demo = c->demo & DEMO_DRIVE_MOD;
#if 0  /* fmk */
               read_info(c->drive_mod.addr,cs_->net_max_tries);
#endif
               exd_timebase = shr_global.stats[c->drive_mod.addr].timebase;
               if (exd_timebase != 0)
               {
                  hr_per_tbit = 1.0 / (60.0 * (float) exd_timebase);
                  s->reset_drive = FALSE;
               }
               if (debugFlags.extLoop)
               {
                 bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
                     BG_DEBUG_LEVEL_DEBUG, "timebase=%d hr_per_tbit=%f\r\n",
                     exd_timebase, hr_per_tbit);
               }
#if 0  /* fmk */
               check_exd_sw_rev();
#endif
            }
            break;
         case AB_PLC_DRIVE:
            s->reset_drive = FALSE;
            exd_timebase = 100;
            min_per_tbit = 1.0 / (60.0 * (float) 100);
            if (debugFlags.extLoop)
            {
               bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
               BG_DEBUG_LEVEL_DEBUG, "exd_timebase=%d\n", exd_timebase);
            }
            break;
      }
   }
   else
   {
      if(local_wtp_mode == MONITOR)
      {
         switch (c->drive_type)
         {
            case AB_PLC_DRIVE:
               s->reset_drive = FALSE;
               exd_timebase = 100;
               min_per_tbit = 1.0 / (60.0 * (float) 100);
               break;
            case PCC:
            default:
#if 0  /* fmk */
               read_info(c->drive_mod.addr,cs_->net_max_tries);
#endif
               exd_timebase = shr_global.stats[c->drive_mod.addr].timebase;
               if (exd_timebase == 0)
                  exd_timebase = 1228800;     /* have some default here   */
               if (exd_timebase != 0)
               {
                  min_per_tbit = 1.0 / (60.0 * (float) exd_timebase);
                  s->reset_drive = FALSE;
               }
               if (debugFlags.extLoop)
               {
                 bgDbgInfo (&g_RingBuffer1, "ho_pcc.c", __func__, __LINE__,
                   BG_DEBUG_LEVEL_DEBUG, "timebase=%d hr_per_tbit=%f\r\n",
                   exd_timebase,hr_per_tbit);
               }
               break;
         }
         net_command(GET_PULSES);            /* check for pulses             */
      }
   }
}
/************************************************************************/
/* totals() - adds in totals                                            */
/************************************************************************/
void totals(void)
{
  float wt;

  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if ((s->delta_b > 0))
   {
      /*if (local_wtp_mode == MONITOR || !(s->alarm_bits & EXD_INHIBIT))*/
      if (1)
      {
         wt = s->delta_b * c->wtperbit;
         add_wt(wt);
      }
   }
   s->delta_b = 0;
   s->delta_t = 0;
}

/************************************************************************/
/* add_wt(wt) - adds in wt to totals                                 */
/************************************************************************/
void add_wt(float wt)
{
   if (debugFlags.extLoop)
   {
      bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }

   if ((Ext_running_mode == MONITOR_MODE) && (s->newrate < c->min_wtp))
      return;

   PMEM.EXT.ShftTotal = PMEM.EXT.ShftTotal + wt;
   PMEM.EXT.InvTotal  = PMEM.EXT.InvTotal  + wt;
}
/************************************************************************/
/* percent_error(a,b) - computes the percent difference between a & b.         */
/************************************************************************/
float percent_error(float a, float b)
{
   float f;

   if (debugFlags.extLoop)
   {
      bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }

   if (b == 0.0)
      f = 0.0;
   else
      f = (a-b)/b;
   return (f);
}
/************************************************************************/
/* check_coast() - checks for coasting conditions and sets into coast   */
/*      depending upon current running mode                             */
/************************************************************************/
void check_coast(void)
{
   float dump;
   //int max_bits;
   int min_bits;
   int bad;
   int i;

   if (debugFlags.extLoop)
   {
     bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
   }
    
   if (s->curtime == 0)          /* if same reading, don't do anything   */
      return;

#if 0
   /* used to be check if in AUTO, but new monitor accepts set wtp  */
   if ((s->set_wtp > 0.0) && !s->ramping)
   {
      if (c->wtperbit > 0.0)
         expected = s->set_wtp * (float) s->curtime /3600000/ c->wtperbit;
      else
         expected = 0.0;
      max_bits = (int) ((float)((expected * c->coast_error) + (float)c->coast_tolerance));
      if (c->coast_error > 0)
         min_bits = (int) ((float)((expected / c->coast_error) - (float)c->coast_tolerance));
      else
         min_bits = 0;
      s->diff_bits = s->curbits - (int) expected;
      
      /* do not report coasting if in monitor mode and extruder not running */
      if ((Ext_running_mode == MONITOR_MODE) && (s->newrate <= c->min_wtp))
         bad = FALSE;
      else   
         bad = (s->curbits > max_bits) || (s->curbits < min_bits);
      if (debugFlags.extLoop)
      {
         bgDbgInfo (&g_RingBuffer1, "Auto", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, " |curbits=%d |minbits=%d |maxbits=%d | bad=%d| expected=%f\r\n", s->curbits, min_bits, max_bits, bad, expected);
      }
   }
   else
   {
      min_bits = (int) ((float) (-1.0 * c->coast_error) - (float)c->coast_tolerance);
      bad = s->curbits < min_bits;
      //bad = FALSE;
      if (debugFlags.extLoop)
      {
         bgDbgInfo (&g_RingBuffer1, "Manual", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, " |curbits=%d |minbits=%d |maxbits=%d | bad=%d| expected=%f\r\n", s->curbits, min_bits, max_bits, bad, expected);
      }
   }
#endif   
   // making it simple.  If you are gaining weight, go into coast
   min_bits = (int) ((float) (-1.0 * c->coast_error) - (float)c->coast_tolerance);
   bad = s->curbits < min_bits;
   
   if (bad)
   {
      if (s->coast)                       /* still bad                    */
      {
        s->coast++;
        if (s->loading)
         { 	 
            if (c->sync)
            {
               if (s->act_spd > c->max_spd*c->alarm_low_spd)
                  i = 2;
               else
                  i = 1;
            }
            else
               i = 3;

            if (s->coast > (i * c->initial_coast))
               s->coast = (unsigned char) (i * c->initial_coast);
         }
         /* check for too many coasts but not drive sys fail               */
         if ((elap_time(coast_timer) > c->max_coast_time * (unsigned int)TICKSPERSEC) &&
            !(s->alarm_bits & DEV_DRV_SYS_FAILURE))
         {
            SET_ALARM(DEV_EXCESSIVE_COAST);
            if (s->coast > 3*c->initial_coast)
               s->coast = (unsigned char)(3*c->initial_coast);  /* limit it  */
         }
         else if (s->alarm_bits & DEV_DRV_SYS_FAILURE)
            s->coast = (unsigned char) (2 * c->initial_coast); /*limit coast*/
      }
      else
      {
         s->coast = c->initial_coast;      /* was good, now bad            */
         coast_timer = current_time;       /* start timer for coast alarms */
         if (Ext_running_mode == AUTO_MODE)
         {
           c->accum_grav = c->accum_grav + (float) elap_time(grav_timer);
           grav_timer = current_time;
         }

         //if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)))
         //   printf("%c,CD(START),%08x\n",dnum+'A',s->time_bits);
      }
   }
   else                                  /* good reading                 */
   {
      if (s->coast)
      {
         if (s->coast > 10)
            s->coast = 3;
         s->coast--;
         if (!s->coast)                    /* coming out of coast          */
         {
           coast_totals();
           /* used to be check if in AUTO, but new monitor accepts set wtp  */
           if (s->set_wtp > 0.0)
           {
              c->accum_coast = c->accum_coast + (float) elap_time(grav_timer);
              grav_timer = current_time;
           }
           dump = s->hop_wt - last_grav_hop_wt;
           if (dump < 0.2)         /* bad dump                             */
           {
              c->coasts++;
              /*if ((s->act_wtp == 0.0) && (dump < 0.0) && (s->ramping))*/
              if (Float_EQ(s->act_wtp, 0.0) && (dump < 0.0) && (s->ramping))
                 add_wt(-1.0 * dump);
              //if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)))
              //   printf("%c,CD(END-B),%08x\n",dnum+'A',s->time_bits);
           }
           else                    /* good dump, check size                */
           {
              c->loads++;           /* dump diagnostics                     */
              if (c->avg_dump_size > 0.0)   /* check if have cleared diags  */
              {
                 dump_sum -= dump_size[dump_last];
                 dump_sum = dump_sum + dump;
                 dump_size[dump_last] = dump;
                 dump_last = (dump_last + 1) % 5;
                 dump_num++;
                 if (dump_num > 5)
                    dump_num = 5;
                 if (dump_num > 0)
                    c->avg_dump_size = dump_sum / dump_num;
              }
              else                  /* must have cleared diagnostics        */
              {
                 dump_size[0] = dump;
                 for (i=1; i<5; i++)
                    dump_size[i] = 0.0;
                 dump_sum = dump;
                 c->avg_dump_size = dump;
                 dump_num = 1;
                 dump_last = 1;
              }

              if (dump < c->min_dump_wt)
              {
                 if (++low_dump_count > 3)
                 {
                    SET_ALARM(DUMP_LOW);
                    low_dump_count = 3;               /* max is 3             */
                 }
              }
              else                                  /* good dump            */
              {
                 if (--low_dump_count < 0)
                    low_dump_count = 0;
                 if (s->alarm_bits & DUMP_LOW)       /* if have alarm, clear */
                    CLEAR_ALARM(DUMP_LOW);
              }
              //if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)))
              //   printf("%c,CD(END-G),%08x\n",dnum+'A',s->time_bits);
            }
         }
      }
      else                      /* not coasting, reading good           */
      {
         last_grav_hop_wt = s->hop_wt;
         s->delta_b += s->curbits;
      }
   }
   if (no_update_timer_on)
   {
      if (!(s->alarm_bits & DEV_DRV_SYS_FAILURE)) /* don't check w/drvfail*/
      {
         if (elap_time(no_update_timer) > ((unsigned int)15 * c->calctime))
         {
            SET_ALARM(DEV_EXCESSIVE_COAST);
	         shrEXT.adscan_reinitialize = TRUE;
         }
      }
      else
         no_update_timer = current_time;  /* only start w/no drive fail   */
   }

   /* check for drive system failure     */ /* all new for v220  */
   if ((s->act_spd > c->max_spd*c->alarm_low_spd) &&
       !s->ramping && (c->max_drive_fail > 0))
   {
      if ((s->curbits < c->drive_fail_error) && (s->curbits > -50))
         s->drive_fail_count++;
      else if (s->drive_fail_count > 0)
         s->drive_fail_count--;

      if (s->drive_fail_count > c->max_drive_fail)
      {
         s->drive_fail_count = c->max_drive_fail;
         SET_ALARM(DEV_DRV_SYS_FAILURE);
         s->rate_uses_spd = FALSE;
         s->act_wtp = 0.0;
         s->newrate = 0.0;
         try_recalibration = TRUE;

         if (s->coast)
         {
            s->coast = (unsigned char) (2 * c->initial_coast);/* limit coast*/
            coast_timer = current_time;            /* alarm stuff          */
         }
      }
   }
   else if ((s->curbits >= c->drive_fail_error) && (s->drive_fail_count > 0))
      s->drive_fail_count--;

   if (s->drive_fail_count == 0)
   {
      CLEAR_ALARM(DEV_DRV_SYS_FAILURE);
      try_recalibration = FALSE;
   }

   if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)))
   {
     //if (s->drive_fail_count > 0)
     //   printf("%c,DFC,%08x\n",dnum+'A',s->drive_fail_count);
   }

   if (try_recalibration)
   {
      if (recal_timer == 0)
      {
         recal_timer = current_time;
         net_command(FORCE_CAL);
      }

      if (current_time - recal_timer > 60*TICKSPERSEC)
      {
         net_command(FORCE_CAL);
         recal_timer = current_time;
      }
   }
   else
   {
      recal_timer = 0;
   }
   
   if (s->coast)
      s->coasts = 1;
   else 
      s->coasts = 0;
}
/************************************************************************/
/* coast_totals() - adds in estimated totals                            */
/************************************************************************/
void coast_totals(void)
{
  float wt;	
  
  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

  switch(Ext_running_mode)
  {
    case AUTO_MODE:
      wt = s->set_wtp * (float) s->delta_t/3600000;
      add_wt(wt);
      s->delta_b = 0;
      s->delta_t = 0;
      break;
    case MAN_MODE:
    case MONITOR_MODE:
      if (s->act_wtp == 0.0)
         totals();
      else
      {
        // look here with Bruce
        wt = s->act_wtp * (float) s->delta_t/3600000;
        add_wt(wt);
        s->delta_b = 0;
        s->delta_t = 0;
      }
      break;
    case PAUSE_MODE:
      totals();
      break;
  }
}
/************************************************************************/
/* ready_to_update() - returns true if time to update                   */
/************************************************************************/
int ready_to_update(void)
{
  int retcode = FALSE;
  char update_by_wt = FALSE;

  if (debugFlags.extLoop)
  {
    bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
        BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (c->update_wt > 0.0)
      {
        if ((float)((float)s->delta_b * c->wtperbit) > c->update_wt)
           update_by_wt = TRUE;
      }   

      if (update_by_wt || (s->delta_t >= c->calctime))
      {
         retcode = TRUE;
      }
      
   return retcode;
}

/************************************************************************/
/* set_initial_speed() - calculate initial set->speed when going into   */
/*  auto mode based upon history                                        */
/************************************************************************/
void set_initial_speed(void)
{
  if (debugFlags.extLoop)
  {
     bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
     BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (c->rs_num == 0)                           /* never in auto before */
   {
      if (s->act_wtp > 0.0)                       /* in man               */
      {
         if ((c->zero_crossing < 0) && (s->set_spd == 0))
            s->set_spd = (s->set_wtp * (float)fabs((double)c->zero_crossing) * (float)100.0) /
                         (s->act_wtp * (float)((float)100.0 + (float)fabs((double)c->zero_crossing)));
         else
            s->set_spd = (s->set_wtp / s->act_wtp) * s->set_spd;
      }
      else
         s->set_spd = LIW_NRS_SPD;
   }
   else
   {
      if (c->rs_ave > 0.0)
         s->set_spd = s->set_wtp / c->rs_ave;
      else
         s->set_spd = LIW_NRS_SPD;
   }
}
/************************************************************************/
/* update() - update wtp                                                */
/************************************************************************/
void update(void)
{
   BOOL  update_spd = FALSE;
   float rs_error;
   float additives_rate;
   //int   i, j;

   hours = ((float) s->delta_t)/3600000.0;    /* convert seconds to hours */
   wt    = (float) s->delta_b * c->wtperbit;
   
      
   /*if ((hours > 0.0) && !(s->alarm_bits & (WM_OVERWEIGHT|WM_UNDERWEIGHT|WT_CRIT))) */
   /*bmh 12/15/2004 - took out wt_crit */
   if ((hours > 0.0) && !(s->alarm_bits & (WM_OVERWEIGHT|WM_UNDERWEIGHT)))
   {
      s->newrate = wt / hours;
      if ((s->newrate < 0.1) && !(g->dbg_who & (1<<dnum)))
         s->newrate = 0.0;         
   }
   else          /* hours = 0, or over/underweight                       */
   {
      totals();
      return;
   }
  
   if (Ext_running_mode == MONITOR_MODE)
   {
      if (s->hop_wt > 0.001)     
         /* force a speed for monitor mode, so rate speed will work */         
			/* s->set_spd = s->demo_extruder_percent_speed = 50.0;    */;
      else
      {
         s->set_spd = s->demo_extruder_percent_speed = 0.0; 
         s->newrate = 0.0;
         s->act_wtp = s->newrate = 0.0;
         return;
      }
   }

   if ((Ext_running_mode == MONITOR_MODE) && (s->newrate <= c->min_wtp))
   {
      s->act_wtp = 0;
      reset_rate_speed = TRUE;
      s->delta_b = 0;
      s->delta_t = 0;
      no_update_timer = current_time;  /* don't give unstable alarm    */
      return;
   }

   // drive system failure detection    
   if (((Ext_running_mode == AUTO_MODE) && (s->newrate < (0.1 * s->set_wtp))) ||
     ((Ext_running_mode == MAN_MODE) && (s->act_spd > 0.0) && (s->newrate <= 0.0)))
   {
      SET_ALARM(DEV_DRV_SYS_FAILURE);
      s->drive_fail_count = c->max_drive_fail;
      try_recalibration = TRUE;

      s->rate_uses_spd = FALSE;
      s->act_wtp = s->newrate;
//    if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)) && (wm_timebase != 0))
//       printf(",%c,UTRS2,%8.3f,%8.3f,%8.3f,%8.3f,%8.4f\n",dnum+'A',(float)s->delta_t/(float)wm_timebase,s->set_spd,s->newrate,s->act_wtp,s->hop_wt);
      totals();
      return;
   }
   else
   {
      CLEAR_ALARM(DEV_DRV_SYS_FAILURE);
      s->drive_fail_count = 0;
      try_recalibration = FALSE;
   }

   if (Ext_running_mode == MONITOR_MODE)
   {
      /*if (s->set_spd == 0.0)*/
      if (Float_EQ(s->set_spd, 0.0))
         s->rate_uses_spd = FALSE;
      update_error = 0.0;
      if (c->rs_resetchg > 0.0)
         if ((float)fabs(percent_error(s->newrate,s->act_wtp)) > c->rs_resetchg)
            reset_rate_speed = TRUE;
   }
   else
   {
      s->rate_uses_spd = TRUE;
      update_error = (float)fabs((double)percent_error(s->newrate,s->set_wtp));
   }     

   if ((Ext_running_mode == MONITOR_MODE) ||
      ((cs_->wtp_mode == CONTROL) && (s->alarm_bits & (EXD_INHIBIT|EXD_IN_MANUAL|DEV_DRV_SYS_FAILURE)) ) ||
      (s->set_spd < c->max_spd * c->alarm_low_spd))  /* don't use alarms for speed   */
   {
      no_update_timer = current_time;  /* don't give unstable alarm    */

      if ((reset_rate_speed) || (wtp_data.rs_avg.num_pts == 0))
      {
         init_mvg_avg(&wtp_data.rs_avg, c->movepts ,&wtp_data.rs_pts[0], c->rs_maxchg);     
         clear_moving_avg(&wtp_data.rs_avg);
         if (s->newrate > 0.0)
            ma_add_pt(&wtp_data.rs_avg,s->newrate);
      }
      else
      {
         if (s->newrate > 0.0)
            ma_add_pt(&wtp_data.rs_avg,s->newrate);        
      }
      s->act_wtp = get_current_avg(&wtp_data.rs_avg);

      //s->act_wtp = s->newrate;
      s->have_new_act = TRUE;

      totals();
      reset_rate_speed = 0;
      s->alarm_bits &= ~DEV_EXCESSIVE_COAST;
      s->rate_uses_spd = FALSE;
      //if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)))
      //   printf(",%c,UTRS,%8.3f,%8.3f,%8.3f,%8.3f,%8.4f\n",dnum+'A',(float)s->delta_t/(float)wm_timebase,s->set_spd,s->newrate,s->act_wtp,s->hop_wt);
      return;
   }

   if (reset_rate_speed)
   {
      rs_num = 0;
      reset_rate_speed = FALSE;
   }
   else
      rs_num = c->rs_num;

   if ((update_error < c->max_error) || (++numbad > c->max_bad) || (rs_num == 0))
   {
      no_update_timer = current_time;
      additives_rate = 0.0;

//#if (MACH!=BATCH)
//      /* must add in additives into newrate to determine the rsfact       */
//      if (c->extruder_with_additives)
//      {
//#if (MACH==WE_BATCH)
//         if (cs->scrap)
//         {
//           if (cs->refeed == GRAVIFLUFF)
//           {
//              if ((shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->set_wtp > 0.0) &&
//                     (fabs((double)percent_error(shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->set_wtp,shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->act_wtp)) < 0.10))
//                additives_rate = additives_rate + shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->set_wtp;
//              else
//                additives_rate = additives_rate + shr_LOOP[GRAVIFLUFF_FDR_LOOP].LIW->act_wtp;
//           }
//           else if (cs->refeed == TRIM_ONLY)
//           {
//              if (cs->temp_recipe.refeed_wtp > 0.0)
//                additives_rate = additives_rate + cs->temp_recipe.refeed_wtp;
//           }
//         }
//#else /* Gravitrol */
//         i = c->ext;
//        // for (j=1; j<=cs_->num_add[i]; j++)
////         {
////            if ((HOP_SET_WTP(i,j) > 0.0) &&
////               (fabs((double)percent_error(HOP_SET_WTP(i,j),HOP_ACT_WTP(i,j))) < 0.10))
////               additives_rate = additives_rate + HOP_SET_WTP(i,j);
////            else
////               additives_rate = additives_rate + HOP_ACT_WTP(i,j);
////         }
////         if (cs_->scrap)
////         {
////            if (cs_->refeed == GRAVIFLUFF)
////            {
////              if ((HOP_SET_WTP(i,REFEED) > 0.0) &&
////                     (fabs((double)percent_error(HOP_SET_WTP(i,REFEED),HOP_ACT_WTP(i,REFEED))) < 0.10))
////                additives_rate = additives_rate + HOP_SET_WTP(i,REFEED);
////              else if (HOP_ACT_WTP(i,REFEED) > 0.0) /* in manual            */
////                additives_rate = additives_rate + HOP_ACT_WTP(i,REFEED);
////            }
////            else if (cs_->refeed == TRIM_ONLY)
////            {
////           //   if ((HOP_SET_WTP(i,REFEED) > 0.0) &&
//////                   (fabs((double)percent_error(HOP_SET_WTP(i,REFEED),HOP_ACT_WTP(i,REFEED))) < 0.10))
//////                additives_rate = additives_rate + HOP_SET_WTP(i,REFEED);
//////              else if (HOP_ACT_WTP(i,REFEED) > 0.0) /* in manual            */
//////                additives_rate = additives_rate + HOP_ACT_WTP(i,REFEED);
//////              else if (BATCH_RECIPE_NEW->layer[i].wtp_percent.s[REFEED] > 0.0)
//////                additives_rate = additives_rate + BATCH_RECIPE_NEW->layer[i].wtp.s[REFEED];
////            }
////         }
//#endif
//      }
//#endif

      if (s->set_spd != 0.0)
        act_rsfact = (s->newrate + additives_rate) / s->set_spd;
      else if (c->zero_crossing < 0.0)
        act_rsfact = ((s->newrate + additives_rate) / fabs((double)c->zero_crossing));
      else if (c->zero_crossing == 0.0)
         act_rsfact = s->newrate + additives_rate;
/*************************************************************************/
//    if ((g->dbg_what & UPDATE_RATE) && (g->dbg_who & (1<<dnum)))
//        printf(",%c,RS,%8.3f,%8.3f,%8.3f,%8.3f\n",dnum+'A',s->newrate,s->set_spd,act_rsfact,c->rs_ave);
/***************************************************************************/
      switch(c->algorithm)
      {
         case STANDARD:
            if (rs_num == 0)
            {
               rs_num = 1;
               c->rs_last = 0;
               c->rs_fact[c->rs_last] = act_rsfact;
               c->rs_sum = act_rsfact;
               c->rs_ave = act_rsfact;
            }
            else
            {
               if ((Ext_running_mode == AUTO_MODE) && (c->rs_clip))
               {
                 rs_error = percent_error(act_rsfact,c->rs_ave);
                 if (fabs((double)rs_error) > c->rs_maxchg)
                 {
                    if (rs_error < 0.0)
                       rs_error = -(c->rs_maxchg);
                    else
                       rs_error = c->rs_maxchg;
                    act_rsfact = (1 + rs_error) * c->rs_ave;
                 }
               }

               c->rs_last = (c->rs_last + 1) % c->movepts;
               if (++(rs_num) > c->movepts)
               {
                  rs_num = c->movepts;
                  c->rs_sum -= c->rs_fact[c->rs_last];
               }
               c->rs_sum = c->rs_sum + act_rsfact;
               c->rs_fact[c->rs_last] = act_rsfact;
               if (rs_num > 0)
                   c->rs_ave = c->rs_sum / rs_num;
            }
            update_spd = TRUE;
            break;
         case STATISTIC:
            if (rs_num == 0)                    /* first pass through   */
            {
                rs_num = 1;
                c->rs_last = 0;
                c->rs_sum = 0.0;
            }

            if (rs_num == 1)
            {
               c->rs_last++;
               c->rs_sum = c->rs_sum + act_rsfact;
               act_rsfact = c->rs_sum / (float) c->rs_last;
               if ((Ext_running_mode == AUTO_MODE) && (c->rs_clip))
               {
                  rs_error = percent_error(act_rsfact,c->rs_ave);
                  if (fabs((double)rs_error) > c->rs_maxchg)
                  {
                     if (rs_error < 0.0)
                        rs_error = -(c->rs_maxchg);
                     else
                        rs_error = c->rs_maxchg;
                     act_rsfact = (1 + rs_error) * c->rs_ave;
                  }
               }
               c->rs_ave = act_rsfact;
               update_spd = TRUE;
               if (c->rs_last >= c->movepts)
               {
                  rs_num = 2;
                  c->rs_last = 0;
                  c->rs_sum = 0.0;
               }
            }
            else if (rs_num >= 2)
            {
               c->rs_last++;
               c->rs_sum = c->rs_sum + act_rsfact;
               if (c->rs_last >= c->movepts)
               {
                  act_rsfact = c->rs_sum / (float) c->rs_last;
                  if ((Ext_running_mode == AUTO_MODE) && (c->rs_clip))
                  {
                     rs_error = percent_error(act_rsfact,c->rs_ave);
                     if (fabs((double)rs_error) > c->rs_maxchg)
                     {
                       if (rs_error < 0.0)
                         rs_error = -(c->rs_maxchg);
                       else
                         rs_error = c->rs_maxchg;
                       act_rsfact = (1 + rs_error) * c->rs_ave;
                     }
                  }
                  c->rs_ave = act_rsfact;
                  c->rs_last = 0;
                  c->rs_sum = 0.0;
                  update_spd = TRUE;
               }
            }
            break;
      }/*end switch*/

      c->rs_num = rs_num;

      if (c->extruder_with_additives)
        s->act_wtp = (c->rs_ave * s->set_spd) - additives_rate;
      else
      {
        /*s->act_wtp = c->rs_ave * s->set_spd;*/
        s->act_wtp = c->rs_ave * s->act_spd;	  /* use act_spd is more accurate */
        if (c->enable_multi_rs_factor)
        vStoreRS_Value(s->act_wtp,s->act_spd*100);   
         
      }
      /*if ((s->set_spd == 0.0) && (c->zero_crossing < 0.0))*/
	   if (cfg_super.wtp_mode == MONITOR)  
         s->act_wtp = s->newrate;
  
      s->have_new_act = TRUE;
//
//      if ((g->dbg_what & UPDATE_COAST) && (g->dbg_who & (1<<dnum)) && (wm_timebase != 0))
//         printf(",%c,UTRS3,%8.3f,%8.3f,%8.3f,%8.3f,%8.4f\n",dnum+'A',(float)s->delta_t/(float)wm_timebase,s->set_spd,s->newrate,s->act_wtp,s->hop_wt);
        
        
      if ((fabs((double)percent_error(s->act_wtp,s->set_wtp)) > c->dead_band) && (Ext_running_mode == AUTO_MODE) && update_spd)
      {
        /*if (c->rs_ave != 0.0)*/
        if (Float_NEQ(c->rs_ave, 0.0))
        {
           if (c->extruder_with_additives)
              s->set_spd = ((s->set_wtp) + additives_rate) / c->rs_ave;
           else
              s->set_spd = (s->set_wtp) / c->rs_ave;
	        if (c->enable_multi_rs_factor && (fabs(s->set_spd - s->act_spd) > EXT_SPD_DIFF))
	           s->set_spd = fGetSpeedFromTable(s->set_wtp)/100;
        }
        else
           s->set_spd = 0;

        net_command(SET_SPEED);
      }
      
      if (debugFlags.ext_wtp_enable)
         print_ext_data();	
      
      numbad = 0;
      if ((Ext_running_mode == AUTO_MODE) && (s->set_wtp > 0.0))
      {
        if (out_of_spec_count++ > (unsigned int)2*c->movepts)       /* time to check*/
        {
           out_of_spec_count = 2*c->movepts + 1;
	 
           if ((fabs((double)(s->act_wtp-s->set_wtp)/s->set_wtp)) > c->out_of_spec_limit)
	        {
              SET_ALARM(DEV_OUT_OF_SPEC);
	           ext_outofspec_counter++;
	        }
           else
	        {
              /*      out_of_spec_count = 0;*/
	           CLEAR_ALARM(DEV_OUT_OF_SPEC);
	        }     
         }
      }
      CLEAR_ALARM(DEV_EXCESSIVE_COAST);
   }
   else
   {
      numbad++;
   }
   totals();
}
/************************************************************************/
/************************************************************************/
void calc_new_initial_rate_speed(void)
{
  if (cs_->spd_in_percent)
    c->rs_num = 0;
  else if (!c->cal_done)
    c->rs_num = 0;
  else   /* have a speed factor, and this means rate speed is = 1)   */
  {
    c->rs_num = 1;
    c->rs_last = 0;
    c->rs_fact[0] = 1.0;
    c->rs_sum = 1.0;
    c->rs_ave = 1.0;
  }
}
/************************************************************************/
/* net_command(cmd) - sends out cmd on the network, parses the response,*/
/*   and sets the alarm_bits                                            */
/************************************************************************/
void net_command(int cmd)
{
	//unsigned int ramping_timer_1s;
	//register unsigned int timeperbit;
	float accel_decel, ftmmp = 0.0;
	//unsigned int last_time, last_bits, pulses_per_min, rpm;
	unsigned int max_ramptime = 0;
	//unsigned int bit_difference;
	//unsigned int expected_ramptime;
	//float lastsetspd4plc;
  
	if (debugFlags.extLoop)
	{
		bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG, "\r\n");
	}

	switch(cmd)
	{  
		case GET_WEIGHT:
			demo = c->demo & DEMO_WEIGH_MOD;
			/*send_net(c->weigh_mod.addr,GET_WEIGHT,c->weigh_mod.chan,0);*/
			/*set_wm_status();*/
			if (!WM_DEAD)
			{
				/* s->last_time_bits = s->time_bits;*/	
				/* s->wt_bits = (int) ipread((char *)&packet.data[CMD+3],3);*/
				/* s->time_bits = ipread((char *)&packet.data[CMD+6],4);*/
				/* s->curtime = s->time_bits - s->last_time_bits;*/
       
				s->curtime = elap_time(interval_timer);     /* in ms */
				//if (s->last_poke_bits >= s->wt_bits)
				s->curbits = s->last_poke_bits - s->wt_bits;  
			
				// s->delta_b += s->curbits;
				s->delta_t += s->curtime;
				s->last_poke_bits = s->wt_bits;		 /* jump out of check_coast loop temporarily */
				interval_timer = current_time;
       
				if (c->cal_done)
				{
					s->last_hop_wt = s->hop_wt;
					s->hop_wt = ((float) (s->wt_bits - c->zerowt)) * c->wtperbit;
					check_weights();
				}
			}
			break;
		case SET_SPEED:
			demo = c->demo & DEMO_DRIVE_MOD;
			if (demo)
				shrEXT.set_wtp = LiwCommand[0].SetWTP;  
			
			if (s->set_spd > c->max_spd * c->alarm_high_spd)
			{
				SET_ALARM(DEV_OVERSPEED);
				if (s->set_spd > c->max_spd)
					s->set_spd = c->max_spd;
			}
			else
				CLEAR_ALARM(DEV_OVERSPEED);

			if ((s->set_spd < c->max_spd * c->alarm_low_spd) && (s->oper_mode == AUTO_MODE))
			{
				SET_ALARM(DEV_UNDERSPEED);
				if (s->set_spd < 0.0)
					s->set_spd = 0.0;
			}
			else
				CLEAR_ALARM(DEV_UNDERSPEED);

			//if (((float)fabs((double)(s->set_spd - s->act_spd)) < (float)(0.0003*c->spd_factor)) ||     /*same spd   */
			//   (s->alarm_bits & EXD_IN_MANUAL))      /* in manual backup     */
			//accel_decel = 0.0;
			if (s->set_spd < s->act_spd)         /* decel        */
				accel_decel = s->decel;
			else                                      /* accel        */
				accel_decel = s->accel;   
#if 1
			/* took this out to calculate the ramptime differently */
			if (c->spd_factor > 0.0)
			{
				if (accel_decel > 0.0)
				{
					ovf = ((float)fabs((s->set_spd-s->act_spd)/(accel_decel*c->spd_factor)));
					max_ramptime = (float) (1/ accel_decel);
				}
				else
					ovf = 0.0;
				if (ovf > OVF_INT)  /* overflow */
				{
					accel_decel = 0.0;
					ovf = 0;
				}

				if (ovf > max_ramptime)
					ovf = max_ramptime;

				ramptime = (unsigned int) ovf;
			}
			else
			{
				ramptime = 0;
				accel_decel = 0.0;
			}

			extruder_ramptime_plc = ramptime;	      /* in ms */
#endif

			if (c->spd_factor > 0.0)
			{
				ovf = s->set_spd * 32767.0;       /* 16bits resolution */
				/* adjust speed bits for zero crossing of motor speed */
        
				if (ovf == 0)
					s->spd_bits = 0;
        
				if ((ovf > 0.0) && (c->zero_crossing > 0.0))
				{
					/* Scale down for bandwidth */
					ovf = ovf * (((float)100.0 - c->zero_crossing) / (float)100.0);
					/* add in offset adjustment */
					ovf = ovf + ((float)327.67 * c->zero_crossing);
				}

				if (ovf > 32767)
					s->spd_bits = 32767;
				else
					s->spd_bits = ovf;
	
			}
			else
				s->spd_bits = 0;
		
			/*ipwrite((char *)&packet.data[CMD+2],(unsigned int)s->spd_bits,2);*/
	  
			DriveCommand[gDnum].CMDRdy = TRUE;
			DriveCommand[gDnum].CMDReturn = FALSE;
			DriveCommand[gDnum].SetSpeed = (int) s->spd_bits;
			if (cfgEXT.drive_type != AB_PLC_DRIVE)
			{
#if 0
				/* calculate the timeperbit and the extruder ramptime */
				if ((exd_timebase != 0) && (accel_decel > 0.0))
				{
					ovf = ((float) exd_timebase)/(accel_decel * 327.67);
					if (ovf > OVF_INT)
						timeperbit = 0;
					else
						timeperbit = (unsigned int) ovf;
					/*   ipwrite((char *)&packet.data[CMD+4],(unsigned int)timeperbit,4);*/

					/* now calculate the extruder ramptime */
					/* calculate the max_ramptime as a sanity check */
					max_ramptime = (unsigned int)((100.0 / accel_decel) * (float)TICKSPERSEC);        /* DIVIDE */
					bit_difference = (unsigned int)abs((int)(act_spd_bits) - s->spd_bits);
					if (bit_difference > 0)
					{
						/* calculate extruder_ramptime in exd ticks */
						extruder_ramptime = timeperbit * bit_difference;

						/* change extruder_ramptime to seconds */
						/* 1 is added to account for the maximum round-off error due to */
						/* the integer math usedunsigned.                                       */
						extruder_ramptime = extruder_ramptime + 1;     /* DIVIDE */
						if(extruder_ramptime < 3)
							extruder_ramptime = 5;
						else
							extruder_ramptime = extruder_ramptime + 2;
						if(extruder_ramptime > max_ramptime)
							extruder_ramptime = max_ramptime;

						extruder_ramptime = extruder_ramptime;
						last_ramptime = extruder_ramptime;
						s->ramping = TRUE;
					}
					else
					{
						extruder_ramptime = last_ramptime; /* Jason */
						s->ramping = FALSE;
					}
					ramptime = extruder_ramptime;
				}
				else          /* zero accel/decel value       */
				{
					//ipwrite((char *)&packet.data[CMD+4], (unsigned int)0, 4);
					ramptime = 0;
				}
#endif
			}   
			DriveCommand[gDnum].AccelDecelValue =  accel_decel;		 
			//	 expected_ramptime = (unsigned int) bit_difference/ DriveCommand[gDnum].AccelDecelValue;
			//  DriveCommand[gDnum].RampTime = expected_ramptime;
	      #ifdef ALLEN_BRADLEY_DRIVE  
			if (c->drive_type==AB_PLC_DRIVE)
			{
				if (!connectedPLC) 
					init_ethernet();

				if (connectedPLC) 
				{	 
					PLC_WRITTEN_FLOAT_DATA[0] = s->set_spd *100;
					if (c->spd_factor != 0.0) 
						PLC_WRITTEN_FLOAT_DATA[0] = (float)(s->set_spd * (float)100.0)/c->spd_factor;
	        
					PLC_WRITTEN_FLOAT_DATA[1] = extruder_ramptime_plc;
					SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 20, 2 * c->drive_mod.chan, 2, FLOAT);  
					RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
					/*   ClosePLCRemoteSocket();*/
				}
				set_exd_status();
			}
			else
         #endif
			{
				/* If speed is being controlled externally, do not send speed to drive */
				if (s->speed_ctrl == INTERNAL_CTRL)
				{
					// send_net(c->drive_mod.addr,SET_SPEED,c->drive_mod.chan,6);
					set_exd_status();
				}
			}
			if (s->ramping)
				rampstart = current_time;

			break;
	  
		case LOAD_ON:
			//      demo = c->demo & DEMO_WEIGH_MOD;
			//      if (c->sync)
			//        net_command(SYNC_OFF);
			//#if (MACH==WE_BATCH) 
			g->material_request = TRUE;
			if (!c->use_internal_loading_signal_only)
			{
				IO_dig[0].channel[cfgEXT.load_relay.chan] = ON;
			}
			//#else
			//      if (c->load_relay_type == NORMALLY_OPEN)
			//        send_net(c->load_relay.addr,SET_DO,c->load_relay.chan,0);
			//      else
			//        send_net(c->load_relay.addr,RESET_DO,c->load_relay.chan,0);
			//#endif
			//         
			//      set_wm_status();
			if (!WM_DEAD)
				s->loading = TRUE;	
			break;

		case LOAD_OFF:
			//     demo = c->demo & DEMO_WEIGH_MOD;
			//         
			//#if (MACH==WE_BATCH)
			g->material_request = FALSE;  
			if (!c->use_internal_loading_signal_only)
			{
			IO_dig[0].channel[cfgEXT.load_relay.chan] = OFF;
			}
			//         
			//#else
			//      if (c->load_relay_type == NORMALLY_OPEN)
			//        send_net(c->load_relay.addr,RESET_DO,c->load_relay.chan,0);
			//      else
			//        send_net(c->load_relay.addr,SET_DO,c->load_relay.chan,0);
			//#endif
			//      set_wm_status();
			if (!WM_DEAD)
				s->loading = FALSE;

			//     if ((c->hop == REFEED) && (s->oper_mode != PAUSE_MODE))
			//      {
			//        sprintf(bfr,"/pipe/dev%d",GRAVIFLUFF_LOOP);
			//        fluff_cmdpn = open(bfr,(short)(S_IWRITE));
			//        sprintf(bfr,"%c\n",RESUME);
			//        write(fluff_cmdpn,bfr,brsstrlen(bfr));
			//        close(fluff_cmdpn);
			//      }
			break;
      
		case LOW_ON:
      
			IO_dig[X20].channel[cfgEXT.alarm_relay.chan] = ON;
       
			break;
       
		case LOW_OFF:
      
			IO_dig[X20].channel[cfgEXT.alarm_relay.chan] = OFF;
       
			break; 
       
		case GET_SPEED:
			demo = c->demo & DEMO_DRIVE_MOD;
         #ifdef ALLEN_BRADLEY_DRIVE
			if (c->drive_type == AB_PLC_DRIVE)
			{     
				if (!connectedPLC) 
					init_ethernet();
                   
				if (connectedPLC) 
				{  
					SendUnitDataCommand(c->drive_mod.chan, PLC_5_READ, 21,  c->drive_mod.chan, 1, FLOAT);  
					RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_READ);
					/* ClosePLCRemoteSocket(); */
				}
				set_exd_status();
				s->act_spd = (PLC_READ_FLOAT_DATA[0] * c->spd_factor)/100.0;
        
				if (s->ramping)
				{
					if (abs (s->act_spd * 100 - s->set_spd * 100) < 1)
						s->ramping = FALSE;
					last_act_spd = s->act_spd;
				}
			}
			else
         #endif
			{
				//send_net(c->drive_mod.addr,GET_SPEED,c->drive_mod.chan,0);
				set_exd_status();
         
				if (1)
				{
					//act_spd_bits = ipread((char *)&packet.data[CMD+3],2);
					act_spd_bits = PLC.AnaOut.AnalogOutput1;
					ftmmp = (float)act_spd_bits;
             
					//
					//          if ((ftmmp >= 0.0) && (c->zero_crossing >= 0.0))
					//          {
					//            ftmmp = (float)(ftmmp - (327 * c->zero_crossing));  /* Take out offset */
					//            ftmmp = (float)((ftmmp / (32767 - (327 * c->zero_crossing))));  /* DIVIDE */
					//          } 
				}
				else
					ftmmp = 0.0;
              
				if (ftmmp > 32767)
					ftmmp = 32767;
				else if (ftmmp < 0.0)
					ftmmp = 0.0;

				last_act_spd = s->act_spd;
				s->act_spd = ftmmp / (float)32767;
			}
         
			if (s->rate_uses_spd)
			{
				ftmmp = 0.0;
				if (cs_->scrap)
					/*ftmmp = ftmmp + REFEED_ACT_WTP;*/
					s->act_wtp = (c->rs_ave * s->act_spd) - ftmmp;
			}
			break; 
	}
}

#ifdef ALLEN_BRADLEY_DRIVE
/************************************************************************/
/* set_plc_alive_flag - sends alive flag to plc every periodically     */
/************************************************************************/
void send_plc_alive_flag(void)
{
  static unsigned char toggle_bit; 
   
   if (connectedPLC)
   { 
      if ((!g->stats[32 + c->drive_mod.chan].alive) || (EthError == Connection_timed_out))
      {
         if (!EXD_DEAD)
         {
            SET_ALARM(EXD_TIMEOUT);
            // ClosePLCRemoteSocket();
         }
         return;
      }
  //  else if ((s->alarm_bits & EXD_TIMEOUT) && g->stats[32 + c->drive_mod.chan].alive)
//         /*CLEAR_ALARM(EXD_TIMEOUT)*/
  }
  
  if (EXD_DEAD)
     shrEXT.act_spd = 0;       /* lost communication to plc, act_spd is reset to zero */ 
  
  if (debugFlags.extLoop)
  {
      bgDbgInfo (&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__,
         BG_DEBUG_LEVEL_DEBUG, "\r\n");
  }

   if (plc_alive_count++ > 10)
   {
      plc_alive_count = 0;
	   
      if (!connectedPLC) 
         init_ethernet();
      
      if (connectedPLC) 
      {
      PLC_WRITTEN_INT_DATA[0] = 0x0001;
      SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 7, 1, 1, INT);
      RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
      toggle_bit = 1;
      /* ClosePLCRemoteSocket(); */
      }
   }
   else if (toggle_bit)
   {
      toggle_bit = 0;
      if (!connectedPLC) 
         init_ethernet();
      if (connectedPLC) 
      {
      PLC_WRITTEN_INT_DATA[0] = 0x0000;
      SendUnitDataCommand(c->drive_mod.chan, PLC_5_WRITE, 7, 1, 1, INT);
      RecvSendUnitDataResponse(c->drive_mod.chan, PLC_5_WRITE);
      /*  ClosePLCRemoteSocket(); */
      }
   }
}
/************************************************************************/
/* init_ethernet) -                                                     */
/************************************************************************/
void init_ethernet(void)
{
   unsigned long HostAddr;

   brsmemset((UDINT)&HostAddr, 0, sizeof(HostAddr)); 
   brsmemcpy((UDINT)&HostAddr, (UDINT)cfgHO.drive_ip_address, sizeof(cfgHO.drive_ip_address));
			  
   OpenRemoteSocketPLC(0xaf12, (UDINT)HostAddr, 1, c->drive_mod.chan);

   if (connectedPLC) 
   {
      SendRegSessionCommand(c->drive_mod.chan);
      RecvRegSessionResponse(c->drive_mod.chan);
      SendRRData(c->drive_mod.chan);
      RecvSendRRDataResponse(c->drive_mod.chan);
     
    //  if (connection_counter > 0)
//      connection_counter--;
      //if (EXD_DEAD)
//      CLEAR_ALARM(EXD_TIMEOUT);
       ConnectedPLC_Flag = connectedPLC;
   }
   else 
   {
     // connection_counter++; 
//     if (connection_counter> 0) 
     if (EthError == Connection_timed_out)
     {
        if (!EXD_DEAD)
        {
           SET_ALARM(EXD_TIMEOUT);
/*         ClosePLCRemoteSocket();*/
        }
        return;
     }	   
   }
}
#endif

void reset_wm(void)
{
	
}

#define UNDER   0
#define OVER    1
/************************************************************************/
/* check_weights() - checks hop_wt against alarm/loading conditions     */
/*      assuming that calibration is done                               */
/************************************************************************/
void check_weights(void)
{
  //int fluff_cmdpn;
  unsigned int ElapTime;
 
  if (s->alarm_bits & WM_IN_MANUAL)     /* no alarms, etc when in backup*/
  {
    CLEAR_ALARM(WT_CRIT|WT_LOW|DUMP_LOW);
    if (s->loading)
    {
      net_command(LOAD_OFF);
      load_between_OFF_and_ON_timer = current_time;
    }
//    if (low)
//      net_command(LOW_OFF);
//    return;
  }

  /* check if need to turn on loading */
  if (!(s->alarm_bits & WM_OVERWEIGHT))
  {
    if ((!s->loading) && (c->load_wt_on > 0.0) && (!c->load_disabled) &&
       check_weight(UNDER,c->load_wt_on,&load_wt_on_timer,(unsigned int)c->detect_time)) 
    {
      if (elap_time(load_between_OFF_and_ON_timer) >= TIME_BETWEEN_LOAD_OFF_ON)
      {
        net_command(LOAD_ON);
        load_timer = current_time;
      }
    }
    else
    {   /* note - no time delay in turning off loading  */
      if (s->loading)
      {
        if (c->load_time > 0)
        {
          ElapTime = elap_time(load_timer);
          if ((ElapTime + 1) >= (unsigned int) c->load_time)
          {
            net_command(LOAD_OFF);
            load_between_OFF_and_ON_timer = current_time;
          } 
        }
        else
        {
          if ((((s->hop_wt > c->load_wt_off) && (s->hop_wt > c->load_wt_on)) ||
             ((c->load_wt_on <= 0.0) && (c->load_wt_off <= 0.0))))
            net_command(LOAD_OFF);
          load_between_OFF_and_ON_timer = current_time;
        }
      }
    }
  }
  else
  {
    if (s->loading)
    {
      net_command(LOAD_OFF);
      load_between_OFF_and_ON_timer = current_time;
    }
  }

  /* check if critical low                                              */
  if ((c->crit_wt > 0.0) && check_weight(UNDER,c->crit_wt,&crit_low_timer,(unsigned int)c->detect_time))
  {
    if (!(s->alarm_bits & WT_CRIT))
       SET_ALARM(WT_CRIT);
    CLEAR_ALARM(WT_LOW);         /* don't have low also          */
    if ((c->hop == REFEED) && (cs_->refeed == GRAVIFLUFF))
    {
      if (!crit_low)
      {
        /* tell fluff loader we do not have enough scrap */
   //     sprintf(bfr,"/pipe/dev%d",GRAVIFLUFF_LOOP);
//        fluff_cmdpn = open(bfr,(short)(S_IWRITE));
//        sprintf(bfr,"%c\n",MAX_VIRGIN);
//        write(fluff_cmdpn,bfr,brsstrlen(bfr));
//        close(fluff_cmdpn);
      }
    }
    crit_low = TRUE;
  }
  else
  {
    if (crit_low)
    {
      crit_low = FALSE;
      if ((c->hop == REFEED) && (cs_->refeed == GRAVIFLUFF))
      {
         /* tell fluff loader we now have enough scrap */
         //sprintf(bfr,"/pipe/dev%d",GRAVIFLUFF_LOOP);
         //fluff_cmdpn = open(bfr,(short)(S_IWRITE));
         //sprintf(bfr,"%c\n",MAX_SCRAP);
         //write(fluff_cmdpn,bfr,brsstrlen(bfr));
         //close(fluff_cmdpn);
      }
    }
    CLEAR_ALARM(WT_CRIT);
  }

  /* check if below low weight  */
  if ((c->alarm_wt > 0.0) && check_weight(UNDER, c->alarm_wt, &low_timer, (unsigned int)c->detect_time) &&
      ((s->hop_wt > c->crit_wt) || (c->crit_wt <= 0.0 )))
  {
     if (!(s->alarm_bits & WT_LOW))	
        SET_ALARM(WT_LOW);
  }
  else
     CLEAR_ALARM(WT_LOW);
  
  if ((s->alarm_bits & (WT_LOW|WT_CRIT)) && !low)
     net_command(LOW_ON);
  else if (!(s->alarm_bits & (WT_LOW|WT_CRIT)) && low)
     net_command(LOW_OFF);

  /* check for under weight hopper      */
  if (s->wt_bits == 0)
  {
     if (check_weight(UNDER, (float)99999.0, &underweight_timer, (unsigned int)c->detect_time))
        SET_ALARM(WM_UNDERWEIGHT);
  }
  else
     CLEAR_ALARM(WM_UNDERWEIGHT);
  
  /* check for overweight hopper */
  if (check_weight(OVER, cfgEXT.high_alarm_wt, &overweight_timer, (unsigned int)c->detect_time))
     SET_ALARM(WM_OVERWEIGHT);
  else
     CLEAR_ALARM(WM_OVERWEIGHT);
  
  /* check if low dump size weight has changed  */
  if (c->avg_dump_size > c->min_dump_wt)
     CLEAR_ALARM(DUMP_LOW);
}

/************************************************************************/
/* check_weight() - checks hop_wt against passed parameter              */
/*      assuming that calibration is done                               */
/************************************************************************/
int check_weight(int type,float wt_limit,unsigned int *timer,unsigned int time_limit)
/*
    type
    wt_limit
    *timer                  location of timer
    time_limit
*/
{
  if (((s->hop_wt < wt_limit) && (type == UNDER)) ||
      ((s->hop_wt > wt_limit) && (type == OVER)))
  {
    if (((s->last_hop_wt > wt_limit) && (type == UNDER)) ||
        ((s->last_hop_wt < wt_limit) && (type == OVER)))
    {
      *timer = current_time;
      return(FALSE);
    }
    else
    {
      if (elap_time(*timer) > time_limit)
        return(TRUE);
      else
        return(FALSE);
    }
  }
  else
    return(FALSE);
}

void print_ext_data(void)
{
   bgDbgInfo(&g_RingBuffer1, "ext_pcc.c", __func__, __LINE__, BG_DEBUG_LEVEL_DEBUG,
             "Act_Wtp = %f, Act_Spd = %f  \r\n", s->act_wtp, s->act_spd);        	
}

/************************************************************************/
/* set_exd_status() - sets the exd status bits in alarm_bits            */
/************************************************************************/
void set_exd_status(void)
{
   static int last_ctrl = INTERNAL_CTRL;
  
	if ((cfg_super.wtp_mode == CONTROL) && (!cfgEXT.demo & DEMO_DRIVE_MOD))
	{
		if (IO_dig[0].channel[AutoMan_EXT])
			CLEAR_ALARM(EXD_IN_MANUAL);
		else if (!(s->alarm_bits & EXD_IN_MANUAL))
			SET_ALARM(EXD_IN_MANUAL);
	}
	
	if (cfgEXT.demo & DEMO_DRIVE_MOD)
       CLEAR_ALARM(EXD_IN_MANUAL);
	
   #ifdef ALLEN_BRADLEY_DRIVE
   if (c->drive_type==AB_PLC_DRIVE)
   {
      if (!connectedPLC) 
         init_ethernet();
     
    if ((!g->stats[32 + c->drive_mod.chan].alive) || (EthError == Connection_timed_out))
    {
      if (!EXD_DEAD)
      {
         SET_ALARM(EXD_TIMEOUT);
         /* ClosePLCRemoteSocket();*/
      }
      return;
    }
    else
    {
       if ((s->alarm_bits & EXD_TIMEOUT) && g->stats[32 + c->drive_mod.chan].alive)
       {
          /*s->reset_drive = TRUE;*/
          /*CLEAR_ALARM(EXD_TIMEOUT)*/
       }
    } 
  }
   else
  #endif
   {
      if (s->speed_ctrl == INTERNAL_CTRL)
      {	
         if (last_ctrl == EXTERNAL_CTRL)
         {
            reset_rate_speed = TRUE;
            if (!cs_->line_spd_is_master)
               g->liw_spd[gDnum] = s->act_spd * shr_global.man_ratio / (float)100.0;
         }
      }
      last_ctrl = s->speed_ctrl;
   }
}

void Check_ManualBackup_EXT()
{  
   if (IO_dig[0].channel[IntExtDrv_CTRL_EXT])
      shrEXT.speed_ctrl = EXTERNAL_CTRL;
   else
      shrEXT.speed_ctrl = INTERNAL_CTRL; 
     
   if (!IO_dig[0].channel[AutoMan_EXT])
   {
      shrEXT.ext_manual_backup = MANUAL_BACKUP;
      IO_dig[0].channel[EXTManual_Out_0] = OFF;
      IO_dig[0].channel[EXTManual_Out_1] = OFF;
   }
   else
   {
      shrEXT.ext_manual_backup = AUTO_CONTROL; 
      IO_dig[0].channel[EXTManual_Out_0] = ON;
      IO_dig[0].channel[EXTManual_Out_1] = ON;
   }
       
   if (IO_dig[0].channel[AutoMan_EXT])
      CLEAR_ALARM(EXD_IN_MANUAL);
   else if (!(s->alarm_bits & EXD_IN_MANUAL))
      SET_ALARM(EXD_IN_MANUAL);     
}

REAL fGetSpeedFromTable(REAL fRate)
{
	USINT u8Idx  = 0;
	BOOL  bFound = FALSE;
	REAL  fRetVal;
	
	while ((u8Idx < POINTS_IN_RS_TABLE) && (bFound == FALSE))
	{
		if (fRate > shrEXT.stSpeed_vs_Rate[u8Idx].fWt)
			u8Idx++;
		else
			bFound = TRUE;
	}
	
	if (bFound == TRUE)
	{
		if (u8Idx == 0)
			/* before first stored avg rate extrapolate towards 0 */
			if (shrEXT.stSpeed_vs_Rate[u8Idx].fWt > 0)
				fRetVal = shrEXT.stSpeed_vs_Rate[0].fAverageMotorSpeed * fRate / shrEXT.stSpeed_vs_Rate[u8Idx].fWt;
			else
				fRetVal = shrEXT.set_spd*100;
		else
			if ((shrEXT.stSpeed_vs_Rate[u8Idx].fWt - shrEXT.stSpeed_vs_Rate[u8Idx-1].fWt) != 0)
				/* liniar interpolation with the previous point */
				fRetVal = shrEXT.stSpeed_vs_Rate[u8Idx-1].fAverageMotorSpeed +
					(shrEXT.stSpeed_vs_Rate[u8Idx].fAverageMotorSpeed - shrEXT.stSpeed_vs_Rate[u8Idx-1].fAverageMotorSpeed) * 
					(fRate - shrEXT.stSpeed_vs_Rate[u8Idx-1].fWt) /
					(shrEXT.stSpeed_vs_Rate[u8Idx].fWt - shrEXT.stSpeed_vs_Rate[u8Idx-1].fWt);
			else
				fRetVal = shrEXT.set_spd*100;
	}
	else
		if (shrEXT.stSpeed_vs_Rate[u8Idx-1].fWt > 0) 
			/* beyond last storred average speed extrapolate liniar */
			fRetVal = shrEXT.stSpeed_vs_Rate[u8Idx-1].fAverageMotorSpeed * fRate / shrEXT.stSpeed_vs_Rate[u8Idx-1].fWt;
		else
			fRetVal = shrEXT.set_spd*100;
		
	return fRetVal;
}

void vStoreRS_Value(REAL fRate, REAL fSpeed)
{
	/* find store index and average rate and avg speed with the new values */

	USINT u8Idx  = 0;
	BOOL  bFound = FALSE;
	
	while ((u8Idx < POINTS_IN_RS_TABLE) && (bFound == FALSE))
	{
		if (fSpeed > shrEXT.stSpeed_vs_Rate[u8Idx].fNominalMotorSpeed)
			u8Idx++;
		else
			bFound = TRUE;
	}
	
	if (bFound == TRUE)
	{
		if (u8Idx == 0) /* should never be the case, but...*/
			/* before first stored avg speed store in first point */;
		else
		{	
			if (fSpeed < ((shrEXT.stSpeed_vs_Rate[u8Idx].fNominalMotorSpeed + shrEXT.stSpeed_vs_Rate[u8Idx-1].fNominalMotorSpeed)/2))
				u8Idx--; /* store in left point */
			else
				;/* store in the right point */
		}
	}
	else
		/* beyond last storred speed so store in last; this also should not be the case */
		u8Idx--;
		
	shrEXT.stSpeed_vs_Rate[u8Idx].fWt = (shrEXT.stSpeed_vs_Rate[u8Idx].fWt + fRate)/2;
	shrEXT.stSpeed_vs_Rate[u8Idx].fAverageMotorSpeed =  (shrEXT.stSpeed_vs_Rate[u8Idx].fAverageMotorSpeed + fSpeed) / 2;
}

void SET_ALARM(unsigned int Alarm)   
{  
   if (G_firstdmpctr < STARTUPDELAY) //delay SETALARMS for 10 sec on startup G2-639
      return;

   s->alarm_bits       |= ((Alarm) | s->alarm_force_bits);
   s->alarm_latch_bits |= ((Alarm) | s->alarm_force_bits); 
}

void CLEAR_ALARM(unsigned int Alarm)
{ 
   s->alarm_bits &= ((~(Alarm)) | s->alarm_force_bits); 
}
