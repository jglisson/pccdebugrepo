#include <string.h>
#include <stdio.h>
#include <math.h>
#include "pcc_math.h"

/***********************************************************************/
/*   tenpt_ma_fclear (int, int) - clears a 10 pt MA to zero's          */
/*     written by:   Greg Fisher                                       */
/*     notes:        count must be at least 1                          */
/***********************************************************************/
unsigned char tenpt_ma_fclear (ma_tp_f_struct *array)
{
   int i;
   float sum;

   sum = 0;
   array->count = 0;

   for(i=0; i < 10; i++)
   {
      array->data[i] = 0.0;
   }
   array->avg = 0.0;
   return 1;
}

