/************************************************************************/
/*
   File:  aldeflt..h

   Description: Defines the action to be take for each mode of the blender
      for all alarm conditions.


      $Log:   F:\Software\BR_Guardian\aldeflt.h_v  $
 *
 *    Rev 1.1   Jan 30 2008 11:39:46   FMK
 * Changed reference for PVCS check in comment path.
 *
 *    Rev 1.0   Jan 11 2008 10:33:30   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/

/* this table defines what the severity level of the alarm is based on the */
/* operating mode of the blender. The masks are defined in alarm.h */
static char alarm_defaults[][3] = {
  {0,           0,             1},             /* alarm #0  NO_ALARM                  */
  {0,           RUNNING_MASK,  0},             /* alarm #1  DRIVE_OVERSPEED           */
  {0,           RUNNING_MASK,  0},             /* alarm #2  EXCESSIVE_COASTING        */
  {AUTO_MASK,   0,             0},             /* alarm #3  OUT_OF_SPEC               */
  {0,           RUNNING_MASK,  0},             /* alarm #4  DRIVE_UNDERSPEED          */
  {0,           RUNNING_MASK,  0},             /* alarm #5  DRIVE_SYSTEM_FAILURE      */
  {0,           0,             0},             /* alarm #6  */
  {0,           0,             0},             /* alarm #7  */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #8  HOPPER_LEVEL_CRIT for AutoBatch    */
  {0,           0,             0},             /* alarm #9  */
  {0,           0,             0},             /* alarm #10 */
  {0,           0,             0},             /* alarm #11 */
  {0,           0,             0},             /* alarm #12 */
  {0,           0,             0},             /* alarm #13 */
  {0,           0,             0},             /* alarm #14 */
  {0,           0,             0},             /* alarm #15 */
  {0,           0,             0},             /* alarm #16 */
  {0,           0,             0},             /* alarm #17 */
  {0,           0,             0},             /* alarm #18 */
  {0,           0,             0},             /* alarm #19 */
  {0,           0,             0},             /* alarm #20 SYSTEM_SHUTDOWN_TYPE      */
  {0,           0,             0},             /* alarm #21 GENERAL_ALARM_TYPE        */
  {0,           0,             0},             /* alarm #22 INFORMATION_ONLY_TYPE     */
  {ALL_MASK,    0,             0},             /* alarm #23 CHECK_PRINTER             */
  {PAUSE_MASK,  0,             RUNNING_MASK},  /* alarm #24 INTERLOCK_OPEN            */
  {0,           0,             ALL_MASK},      /* alarm #25 POWER_SUPPLY_FAILURE      */
  {0,           0,             ALL_MASK},      /* alarm #26 HARDWARE_FAILURE          */
  {ALL_MASK,    0,             0},             /* alarm #27 LOW_BATTERY               */
  {PAUSE_MASK,  0,             RUNNING_MASK},  /* alarm #28 MIXER_MOTOR_FAILURE       */ //G2-404
  {ALL_MASK,    0,             0},             /* alarm #29 AUXILIARY                 */
  {ALL_MASK,    0,             0},             /* alarm #30 PUMP_FAILURE              */
  {ALL_MASK,    0,             0},             /* alarm #31 EXD_PLC_LOW_BATTERY       */
  {ALL_MASK,    0,             0},             /* alarm #32 RESV_INPUT1_ALARM         */
  {ALL_MASK,    0,             0},             /* alarm #33 RESV_INPUT2_ALARM         */
  {ALL_MASK,    0,             0},             /* alarm #34 RESV_INPUT2_ALARM         */
  {0,           0,             0},             /* alarm #35         */
  {0,           0,             0},             /* alarm #36 */
  {ALL_MASK,    0,             0},             /* alarm #37 HOPPER_SWITCH_FAILURE_ALARM    */
  {ALL_MASK,    0,             0},             /* alarm #38 HOPPER_PROX_HIGH_LEVEL    */
  {ALL_MASK,    0,             0},             /* alarm #39 HOPPER_PROX_LOW_LEVEL     */
  {0,           PAUSE_MASK,    RUNNING_MASK},  /* alarm #40 HOPPER_FAILURE            */
  {ALL_MASK,    0,             0},             /* alarm #41 HOPPER_RESET              */
  {ALL_MASK,    0,             0},             /* alarm #42 HOPPER_SW_INCOMPATABLE    */
  {0,           PAUSE_MASK,    RUNNING_MASK},  /* alarm #43 HOPPER_NOT_RESPONDING     */
  {ALL_MASK,    0,             0},             /* alarm #44 HOPPER_IN_MANUAL          */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #45 HOPPER_OVERWEIGHT         */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #46 HOPPER_LEVEL_LOW          */
  {RUNNING_MASK,0,             0},             /* alarm #47 LOW_DUMP_SIZE             */
  {0,           RUNNING_MASK,  0},             /* alarm #48 HOPPER_LOW                */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #49 HOPPER_CRITICAL_LOW       */
  {0,           RUNNING_MASK,  0},             /* alarm #50 LOSS_OF_SYNC              */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #51 HOPPER_UNDERWEIGHT        */
  {0,           0,             0},             /* alarm #52 */
  {0,           RUNNING_MASK,  0},             /* alarm #53 HOPPER_NOT_DUMPING        */
  {0,           RUNNING_MASK,  0},             /* alarm #54 VALVE_FAILURE             */
  {0,           RUNNING_MASK,  RUNNING_MASK},  /* alarm #55 SWD OUT OF SPEC*/
  {0,           RUNNING_MASK,  RUNNING_MASK},  /* alarm #56 SPD OUT OF SPEC*/
  {0,           0,             0},             /* alarm #57 */
  {0,           0,             0},             /* alarm #58 */
  {0,           0,             0},             /* alarm #59 */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #60 WIDTH_FAILURE             */
  {ALL_MASK,    0,             0},             /* alarm #61 WIDTH_SW_INCOMPATIBLE     */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #62 WIDTH_NOT_RESPONDING      */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #63 WIDTH_IN_MANUAL           */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #64 WIDTH_PRODUCT_BREAK       */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #65 WIDTH_MEASUREMENT_ERROR   */
  {0,           RUNNING_MASK,  0},             /* alarm #66 WIDTH_OVER_MAXIMUM        */
  {0,           RUNNING_MASK,  0},             /* alarm #67 WIDTH_UNDER_MINIMUM       */
  {0,           0,             0},             /* alarm #68 */
  {0,           0,             0},             /* alarm #69 */
  {0,           PAUSE_MASK,    RUNNING_MASK},  /* alarm #70 DRIVE_FAILURE             */
  {ALL_MASK,    0,             0},             /* alarm #71 DRIVE_RESET               */
  {ALL_MASK,    0,             0},             /* alarm #72 DRIVE_SW_INCOMPATABLE     */
  {0,           PAUSE_MASK,    RUNNING_MASK},  /* alarm #73 DRIVE_NOT_RESPONDING      */
  {PAUSE_MASK,  RUNNING_MASK,  0},             /* alarm #74 DRIVE_IN_MANUAL           */
  {PAUSE_MASK,  0,             RUNNING_MASK},  /* alarm #75 DRIVE_INHIBIT             */
  {ALL_MASK,    0,             0},             /* alarm #76 DRIVE_AD_OVERLOAD         */
  {0,           RUNNING_MASK,  RUNNING_MASK},  /* alarm #77 MAT_REQ_UNCOVERED */
  {0,           0,             0},             /* alarm #78 */
  {0,           0,             0},             /* alarm #79 */
  {0,           0,             0},             /* alarm #80 */
  {0,           0,             0},             /* alarm #81 */
  {0,           0,             0},             /* alarm #82 */
  {0,           0,             0},             /* alarm #83 */
  {0,           0,             0},             /* alarm #84 */
  {0,           0,             0},             /* alarm #85 */
  {0,           0,             0},             /* alarm #86 */
  {0,           0,             0},             /* alarm #87 */
  {0,           0,             0},             /* alarm #88 */
  {0,           0,             0},             /* alarm #89 */
  {0,           0,             0},             /* alarm #90 RECIPE_CHANGED            */
  {0,           0,             0},             /* alarm #91 DEVICE_IN_AUTOMATIC       */
  {0,           0,             0},             /* alarm #92 DEVICE_IN_MANUAL          */
  {0,           0,             0},             /* alarm #93 DEVICE_STOPPED            */
  {0,           0,             0},             /* alarm #94 POWER_ON                  */
  {0,           0,             0},             /* alarm #95 POWER_OFF                 */
  {0,           0,             0},             /* alarm #96 MONITORING                */
  {0,           0,             0},             /* alarm #97 DEVICE_BATCH_DONE         */
  {0,           0,             0},             /* alarm #98 DEV_UNABLE_TO_RUN         */
  {0,           0,             0}              /* alarm #99 */
  };


