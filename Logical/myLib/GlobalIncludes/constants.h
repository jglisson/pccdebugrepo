#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#define ST_INIT			0

#define ST_SENDPAR1		1
#define ST_SENDPAR2		2
#define ST_ROBUF		   3
#define ST_WAITPAR1		4
#define ST_WAITPAR2 	   5
#define ST_WAITPAR3 	   6

#define ST_WAIT			10

#define ST_DIAL0		   100
#define ST_DIAL1		   101
#define ST_DIALDELAY	   102
#define ST_WAITCONNECT0	110
#define ST_WAITCONNECT1	111
#define ST_WAITCONNECT2	112

#define ST_SLIPSTART	   1
#define ST_SLIPSTOP		2

#define ST_TCPOPEN		100
#define ST_TCPSERVER	   101
#define ST_TCPIOCTL  	102
#define ST_TCPIOCTL2  	103
#define ST_TCPSEND		200
#define ST_TCPRECV		300
#define ST_TCPCLOSE		900
#define ST_TCPCLOSEALL  901
#define ST_TCPCLOSESRV  902

#define ST_ERROR	      999

#endif


