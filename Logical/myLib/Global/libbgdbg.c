#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
#include <stdarg.h>
#include <astime.h>
#include <asstring.h>
#include "libbgdbg.h"
#include "string_pcc.h"

/*
File:
bgRingBuffer.c

About:
------
- ring buffer implementation

B&R System Dependencies:
------------------------
- B&R system library: none
- B&R system header files: stdarg.h


History:
--------
03/11/2005
-first version

*/

static const char *bgRingBuffGetPtrToFileName(const char *pFileNameWithPath)
{
/*
Input Example:
*pFileNameWithPath = "d:/data/temp/cobble/2005-9-1/new_ng.pgp/pgm/new_ng/cpu/bgstatemachine.c"
                                                                             |
Output value is ptr to: bgstatemachine.c ------------------------------------+
*/
int i;
int len = brsstrlen((UDINT)pFileNameWithPath);
char* pStart = (char*)(pFileNameWithPath + len);
	for (i=0; i<len; i++, pStart--)
   {
		if ( (*pStart == '/') || (*pStart == '\\') )
      {
			pStart++;
			return pStart;
		}
	}
	return pFileNameWithPath;
}

/*
----------------------------------------------------------
*/
static void bgRingBuffIncDataInPtr(bgRingBuffer_typ *rb)
{
   rb->ptrDataIn++;
   if (rb->ptrDataIn >= rb->ptrCircularBufferEnd)
   {
      rb->ptrDataIn = rb->ptrCircularBufferBegin;
#if 0
      /* mark wrap arround for testing*/
      *rb->ptrDataIn = '@';
      rb->ptrDataIn++;
#endif
   }
}

/*
----------------------------------------------------------
*/
/* not used
static void bgRingBuffIncDataOutPtr(bgRingBuffer_typ *rb)
{
   rb->ptrDataOut++;

   if (rb->ptrDataOut >= rb->ptrCircularBufferEnd)
      rb->ptrDataOut = rb->ptrCircularBufferBegin;
}
*/

/*
----------------------------------------------------------
*/
static void bgRingBuffPutString(bgRingBuffer_typ *rb, char *sourceString)
{
   char *src = sourceString;

	if (!rb->init)
		return;

   /*input is string! look for zero termination*/
   while( *src )
   {
      /*store char*/
      *rb->ptrDataIn = *src;

      /*inc ptrs*/
		src++;

      bgRingBuffIncDataInPtr(rb);

#if 1 // just start from scratch
      if (rb->ptrDataIn == rb->ptrDataOut)
         break;
#else
      /*check if ring-buffer full*/ // this causes reboot, if no telenet attached
      if (rb->ptrDataIn == rb->ptrDataOut)
      {
	      bgRingBuffIncDataOutPtr(rb);

			/*mark ring buffer overrun*/
			if (!rb->overrun)
         {
		      char text[3]={'<','?','>'};
		      int i;
				
            rb->overrun = 1;
	        	for (i=0; i<3; i++)
            {
			      *rb->ptrDataIn = text[i];
			      bgRingBuffIncDataInPtr(rb);
			      bgRingBuffIncDataOutPtr(rb);
		      }
		   }
      }
#endif
   }
}
/*
----------------------------------------------------------
*/
void bgDbgInfo( 
                  bgRingBuffer_typ *rb, 
                  const char *file, 
                  const char *function,
                  unsigned int lineNumber, 
                  unsigned int level, 
                  const char *format,
                  ...
                  )
{
  char txt[256];
  va_list args;
  DTStructure dateTimeNow;
  DTGetTime_typ tempTimeNow;


  /*if dbg entry level > then currently set dbg level -> exit*/
  if (level > rb->dbgLevel)
    return;

  /* timestap needed? default=no */
  if (rb->dbgFormat & BG_DEBUG_FORMAT_TIMESTAMP)
  {
    tempTimeNow.enable = (BOOL)1;
    DTGetTime(&tempTimeNow);

    DT_TO_DTStructure(tempTimeNow.DT1, (UDINT)&dateTimeNow);

    sprintf(txt, "%d/%2d/%2d %2d:%2d:%2d.%2d|",
        dateTimeNow.year, dateTimeNow.month, dateTimeNow.day,
        dateTimeNow.hour, dateTimeNow.minute, dateTimeNow.second, dateTimeNow.millisec);
    bgRingBuffPutString(rb, txt);
  }

  /* debug level needed? default=no*/
  if (rb->dbgFormat & BG_DEBUG_FORMAT_LEVEL)
  {
    switch(level)
    {
      case 0:
        bgRingBuffPutString(rb, "CRT|");
      break;

      case 1:
        bgRingBuffPutString(rb, "ERR|");
      break;

      case 2:
        bgRingBuffPutString(rb, "WAR|");
      break;

      case 3:
        bgRingBuffPutString(rb, "INF|");
      break;

      case 4:
        bgRingBuffPutString(rb, "DBG|");
      break;

      default:
        bgRingBuffPutString(rb, "?  |");
      break;
    }
  }

  /* filename needed? default=no */
  if (rb->dbgFormat & BG_DEBUG_FORMAT_FILENAME)
  {
    sprintf(txt, "%s|", bgRingBuffGetPtrToFileName(file));
    bgRingBuffPutString(rb, txt);
  }

  /* function name needed? default=no */
  if (rb->dbgFormat & BG_DEBUG_FORMAT_FUNCTION)
  {
    sprintf(txt, "%s()|", function);
    bgRingBuffPutString(rb, txt);
  }

  /* line number needed? default=no */
  if (rb->dbgFormat & BG_DEBUG_FORMAT_LINENUMBER)
  {
    sprintf(txt, "%d|", lineNumber);
    bgRingBuffPutString(rb, txt);
  }

  va_start(args, format);
  vsprintf(txt, format, args);
  bgRingBuffPutString(rb, txt);
}


/*
----------------------------------------------------------
*/
void bgDbgPrintf(bgRingBuffer_typ *rb, char *format, ...)
{
  char txt[256];
  va_list args;

  va_start(args, format);
  vsprintf(txt, format, args);
  bgRingBuffPutString(rb, txt);
}

/*
----------------------------------------------------------
*/
void add_dbg_queue_msg(char *MsgOut)
{
   static int counter=0;
   int i;

   for (i=0; i<MAX_DBG_QUEUE_MSGS; i++)
   {
      if (DbgQueue[i].ucUsed == 0)
      {
         sprintf((char *)DbgQueue[i].ucBuffer, "%5d,", counter++);
         brsstrcat((UDINT)DbgQueue[i].ucBuffer, (UDINT)MsgOut);
         DbgQueue[i].ucUsed = 1;
         break;
      }
   }
   return;
}

#if 0
/*
----------------------------------------------------------
*/
static void bgRingBuffInit(bgRingBuffer_typ *rb, char *buffer, int bufSize)
{
   if (!buffer || !bufSize)
      return;

    rb->init = 1;
    rb->overrun = 0;
    rb->bufSize = bufSize;
    rb->ptrCircularBufferBegin = buffer;
    rb->ptrCircularBufferEnd = buffer + rb->bufSize;
    rb->ptrDataIn = rb->ptrCircularBufferBegin;
    rb->ptrDataOut = rb->ptrCircularBufferBegin;
}

/*
----------------------------------------------------------
*/
static int bgRingBuffGet(bgRingBuffer_typ *rb, char *destination, int destSize)
{
   char *dst    = destination;
   char *dstMax = dst + destSize;
   int  length  = 0;

   if (!rb->init)
      return 0;

    if (rb->ptrDataIn == rb->ptrDataOut)
       return 0;

    while(1)
    {
      *dst = *rb->ptrDataOut;
      length++;
      dst++;
      /*check if max size riched*/
      if (dst >= dstMax)
         break;

      bgRingBuffIncDataOutPtr(rb);

      /*check if ring-buffer empty*/
      if (rb->ptrDataIn == rb->ptrDataOut)
      {
         /*mark end of ring buffer overrun*/
         if (rb->overrun)
         {
            char text[3] = {'<', '!', '>'};
            int  i;

            rb->overrun = 0;
              for (i=0; i<3; i++)
              {
                 *dst = text[i];
                 dst++;
                 length++;
                 if (dst >= dstMax)
                    break;
              }
          }
          break;
        }
    }
    return length;
}
#endif
/*EOF*/


