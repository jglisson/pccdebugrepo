#include <stdarg.h>
#include <string.h>

#define PAD_RIGHT 1
#define PAD_ZERO  2

#define PRINT_BUF_LEN 12 /* should be enough for 32 bit int */

/* there's a lot more functions in string_pcc.c, but they're all staticly
 * delcared, meaning they can only be used from within the string_pcc.c
 * file.  They're pretty much all support routines anyway.
 * function prototypes for those functions are contained within the
 * string_pcc.c file */

/* function prototypes */
int sprintf(char *out, const char *format, ...);
int vsprintf(char *out, const char *format, va_list args);


