#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  relay.c

   Description: This function processes relay requests and will set reset
      the digital i/o based upon those requests.


      $Log:   F:\Software\BR_Guardian\relay.c_v  $
 * Dec 1, 2008 JLR
 *   added/changed telnet Debug stuff
 *   use debugFlags.relay for turning on/off debug information in this file
 *
 *    Rev 1.3   Mar 27 2008 08:46:20   FMK
 * The relays for the extrusion control process
 * changed.
 *
 *    Rev 1.2   Mar 21 2008 09:55:24   Barry
 * Added call to calc_demo_relay
 *
 *    Rev 1.1   Feb 27 2008 14:33:28   FMK
 * Changed relay functions for those relays used
 * on extrusion and ho control.
 *
 *    Rev 1.0   Jan 11 2008 10:33:38   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include "plc.h"
#include "sys.h"
#include "libbgdbg.h"

void set_relay(unsigned short relay);
void reset_relay(unsigned short relay);
unsigned short read_relay(unsigned short relay);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

/************************************************************************/
/*
   Function:  set_relay()

   Description:  turn on output relay defined by 'relay' and mapped to
             relaydef.h
                                                                        */
/************************************************************************/
void set_relay(unsigned short relay)
{
   IO_dig[X20].channel[relay] = ON;
}
/************************************************************************/
/*
   Function:  reset_relay()

   Description:  turn off output relay defined by 'relay' and mapped to
             relaydef.h
                                                                        */
/************************************************************************/
void reset_relay(unsigned short relay)
{
   IO_dig[X20].channel[relay] = OFF;
}
/************************************************************************/
/*
   Function:  read_relay()

   Description:  read the status of the relay defined by 'relay' and mapped to
             relaydef.h
                                                                        */
/************************************************************************/
unsigned short read_relay(unsigned short relay)
{
   return IO_dig[X20].channel[relay];
}


