#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  monitor.c

   Description: This file controls the visualization as it pertains to the
      monitor pages.


      $Log:   F:\Software\BR_Guardian\monitor.c_v  $
 *
 *    Rev 1.6   May 09 2008 15:46:54   FMK
 * Added monitor panels for gates.
 *
 *    Rev 1.5   Apr 09 2008 15:26:20   FMK
 * Fixed next/prev issue with haul off device.
 *
 *    Rev 1.4   Mar 18 2008 14:44:18   FMK
 * Made seperate function for processing the
 * system mode buttons. Now this processing
 * can be common among several pages.
 *
 *    Rev 1.3   Mar 07 2008 10:35:24   FMK
 * Added graphic to main system page of monitor.
 * This eliminated page 2 and 3 of monitor system.
 *
 *    Rev 1.2   Jan 28 2008 13:52:58   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.1   Jan 28 2008 09:44:50   FMK
 * Found issues with page swapping. Fixed this along
 * with correcting the interface for use with a keyboard.
 *
 *    Rev 1.0   Jan 11 2008 10:33:36   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <string.h>
#include "mem.h"
#include "plc.h"
#include "relaydef.h"
#include "sys.h"
#include "signal_pcc.h"
#include "hmi.h"
#include "astime.h"
#include "recipe.h"
#include "bwh_pcc.h"

extern void visible(unsigned char *Runtime, unsigned char state);

#define TRUE 1 
#define FALSE 0

void Monitor_Main();
void MonitorHistogram();
void MonitorVisibility();
void sys_first_recipe_set_act();
extern void Hopper_Visibility460(int);

extern struct PopupActiveStruct Popup; /* make sure popups only for monitor are allowed */

extern DATE_AND_TIME start_time(void);

void MonitorVisibilityIcons(void);

extern void copy_recipe(recipe_struct *, recipe_struct *, int);

static unsigned char ucHopperState=0;
static unsigned char ucBWHState=0;
static unsigned char ucMixerState=0;
static unsigned char ucBoxState=0;

// for Page 025 Histogram
_LOCAL DINT C, W, D; 
_LOCAL DINT range;
/************************************************************************/
/*
   Function:  Monitor_Main()

   Description:  main cyclic task for the Monitor function
                                                                        */
/************************************************************************/
void Monitor_Main()
{
   //shared_LIW_device *sext = &shrEXT;
   //config_LIW_device *cext = &cfgEXT;
	
   config_super *cs = &cfg_super;
   shared_BWH_device *sbwh = ((shared_loop_ptr*)(&shr_LOOP[WEIGH_HOP_LOOP]))->BWH;

   int i;  

   switch (HMI.CurrentPage)
   {
      case pg075_select_config_set:
         if (HMI.pg_075_config_set == 1) //5kg
         {
            cfgHO.nip_circumference = .178;
            cfgHO.pulses_per_rev    = 1000;
            cfgHO.cal_done = TRUE;
            cfgHO.lenperbit = (cfgHO.nip_circumference) / cfgHO.pulses_per_rev;

            cfgBWH.zerowt  = 2004352;
            cfgBWH.test_wt = 5.0;
            cfgBWH.testwt  = 2956334;
            cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
            cfgBWH.resolution = (cfgBWH.wtperbit * MAX_BIT_RANGE)/2;
            cfgBWH.cal_done = TRUE;

            cfgEXT.zerowt  = 808882;
            cfgEXT.test_wt = 5.0;
            cfgEXT.testwt  = 934448;
            cfgEXT.wtperbit = cfgEXT.test_wt / (float)(cfgEXT.testwt - cfgEXT.zerowt);
            cfgEXT.resolution = (cfgEXT.wtperbit * MAX_BIT_RANGE)/2;
            cfgEXT.cal_done = TRUE;
            cfgEXT.min_dump_wt = 1.0;

            for (i=0; i<MAX_GATES; i++)
            {
               BATCH_RECIPE_EDIT.density.s[i] = 1.0;
               BATCH_RECIPE_NOW->density.s[i] = 1.0;
            }

            HMI.pg_075_config_set = 0;
         }
         else if (HMI.pg_075_config_set == 2) //2.5lg
         {
            cfgHO.nip_circumference = .178;
            cfgHO.pulses_per_rev    = 1000;
            cfgHO.cal_done = TRUE;
            cfgHO.lenperbit = (cfgHO.nip_circumference) / cfgHO.pulses_per_rev;

            cfgBWH.zerowt  = 1466129;
            cfgBWH.test_wt = 2.0;
            cfgBWH.testwt  = 2077187;
            cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
            cfgBWH.resolution = (cfgBWH.wtperbit * MAX_BIT_RANGE)/2;
            cfgBWH.cal_done = TRUE;

            cfgEXT.zerowt  = 899268;
            cfgEXT.test_wt = 5.0;
            cfgEXT.testwt  = 1211480;
            cfgEXT.wtperbit = cfgEXT.test_wt / (float)(cfgEXT.testwt - cfgEXT.zerowt);
            cfgEXT.resolution = (cfgEXT.wtperbit * MAX_BIT_RANGE)/2;
            cfgEXT.cal_done = TRUE;
            cfgEXT.min_dump_wt = 1.0;

            cfg_super.blend_control = 1;
            cfg_super.total_receivers = 4;
            
            for (i=0; i<MAX_GATES; i++)
            {
               BATCH_RECIPE_EDIT.density.s[i] = 1.0;
               BATCH_RECIPE_NOW->density.s[i] = 1.0;
            }
            
            HMI.pg_075_config_set = 0;
         }
         else if (HMI.pg_075_config_set == 3) //1kg
         {
            //cfgHO.nip_circumference = .178;
            //cfgHO.pulses_per_rev    = 1000;
            //cfgHO.cal_done = TRUE;
            //cfgHO.lenperbit = (cfgHO.nip_circumference) / cfgHO.pulses_per_rev;

            cfgBWH.zerowt  = 1567215;
            cfgBWH.test_wt = 1.0;
            cfgBWH.testwt  = 2003263;
            cfgBWH.wtperbit = cfgBWH.test_wt / (float)(cfgBWH.testwt - cfgBWH.zerowt);
            cfgBWH.resolution = (cfgBWH.wtperbit * MAX_BIT_RANGE)/2;
            cfgBWH.cal_done = TRUE;

            //cfgEXT.zerowt  = 808882;
            //cfgEXT.test_wt = 5.0;
            //cfgEXT.testwt  = 934448;
            //cfgEXT.wtperbit = cfgEXT.test_wt / (float)(cfgEXT.testwt - cfgEXT.zerowt);
            //cfgEXT.resolution = (cfgEXT.wtperbit * MAX_BIT_RANGE)/2;
            //cfgEXT.cal_done = TRUE;
            //cfgEXT.min_dump_wt = 1.0;

            for (i=0; i<MAX_GATES; i++)
            {
               BATCH_RECIPE_EDIT.density.s[i] = 1.0;
               BATCH_RECIPE_NOW->density.s[i] = 1.0;
            }
            HMI.pg_075_config_set = 0;
         }
         break;

      case pg330_Monitor_Sys:  /* System Monitor Main panel 1 */
		 if (Popup.Monitor == FALSE)
		 {
		    visible(&HMI.PopupRuntime1, FALSE);
		    visible(&HMI.PopupRuntime2, FALSE);
		    visible(&HMI.PopupRuntime3, FALSE);
		    visible(&HMI.PopupRuntime4, FALSE);
		    visible(&HMI.PopupRuntime5, FALSE);
		 }			
         /* show current security level in box */
         switch (cfg_super.current_security_level )
         {
            case SERVICE_LOCK:
               HMI.pg310_SecurityLvlIndex = 4;
            break;
            case SUPERVISOR_LOCK:
               HMI.pg310_SecurityLvlIndex = 3;
            break;
            case MAINTENANCE_LOCK:
               HMI.pg310_SecurityLvlIndex = 2;
            break;
            case OPERATOR_LOCK:
               HMI.pg310_SecurityLvlIndex = 1;
            break;
            default:
               HMI.pg310_SecurityLvlIndex = 0;
            break;
         }

         // hopper control
         //***********************************************************************
         for (i=0; i<MAX_GATES; i++)
         {
            if ((i == sbwh->cur_gate) && (sbwh->cur_gate <= cfg_super.total_gates))
            {
               HMI.pg330_HopperAnimTextIndex = i;
               if (shrGATE[i].alarm_bits)
               {
                  HMI.pg330_GateFeedingColor[i] = gRed;
                  ucHopperState = 5;            
               }
               else if (shrGATE[i].learn_mode)
               {
                  HMI.pg330_GateFeedingColor[i] = gYellow;
                  ucHopperState = 1;
               } 
               else
               {
                  HMI.pg330_GateFeedingColor[i] = gGreen;
                  ucHopperState = 1;
               }
            }
         }
   
         if (sbwh->cur_gate == -1)
         {
            //HMI.pg330_GateFeedingColor[i] = gBlack;
            ucHopperState = 0;
         }

         if (shrBWH.alarm_bits > 0)
         {
            HMI.pg330_BatchStsColor = gRed;
            ucBWHState = 0;
         }
         else
         {
            if (IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] == ON)
            {
               HMI.pg330_BatchStsColor = gGreen;
               ucBWHState = 1;
            }
            else
            {
               HMI.pg330_BatchStsColor = gBlack;
               ucBWHState = 0;
            }
         }

         if (shrMIXER.alarm_bits > 0)
         {
            HMI.pg330_MixerStsColor = gRed;     /* show mixer running */
            HMI.pg330_MixerGateStsColor = gRed;
            ucMixerState = 0;
         }
         else
         {
            if (IO_dig[X20].channel[MIXER_MOTOR_OUT] == ON)
               HMI.pg330_MixerStsColor = gGreen;     /* show mixer running */
            else
               HMI.pg330_MixerStsColor = gBlack;  /* hidden mixer stopped */

            if (IO_dig[X20].channel[MIXER_GATE_OUT] == TRUE)
               HMI.pg330_MixerGateStsColor = gGreen;
            else
               HMI.pg330_MixerGateStsColor = gBlack;
            
            if ((HMI.pg330_MixerStsColor == gGreen) &&  //both are green, mix and dump;
               (HMI.pg330_MixerGateStsColor == gGreen) )
               {
                  ucMixerState = 4;
               } 
            else if( (HMI.pg330_MixerStsColor == gGreen) &&  //mixer motor green, just mix
                     (HMI.pg330_MixerGateStsColor == gBlack) )
               {
                  ucMixerState = 1;
               }
            else if (IO_dig[X20].channel[WEIGH_HOPPER_DUMP_OUT] == ON)  //just receive material
               {
                  ucMixerState = 7;
               }
            else
               {
                  ucMixerState = 0;
               }
         }
         
         if(shrMIXER.mixer_prox_switch == TRUE)
            HMI.pg330_MixerProxStsColor = gGreen;
         else
            HMI.pg330_MixerProxStsColor = gBlack;

         if (HMI.pg330_MixerGateStsColor == gGreen)
            ucBoxState = 2;
         
			if(shr_global.material_request == TRUE)
         {
            HMI.pg330_MaterialReqColor = gGreen;
            ucBoxState = 1;
         }
         else
         {
            HMI.pg330_MaterialReqColor = gBlack;
            ucBoxState = 3;            
         }

         if (cs->ucIconInterface)
            MonitorVisibilityIcons();
         else
            MonitorVisibility();
         
         if (HMI.Action_Input0 == gcActive)    /* Next page button pressed */
         {
			HMI.ChangePage = pg334_Monitor_Hop_AtoH;   /* Next set is hopper monitor */
			HMI.Action_Input0 = gcInActive;
         }

         if (HMI.Action_Input1 == gcActive)    /* Previous set button pressed */
         {
			   if ((cfg_super.ltp_mode != UNUSED))
               HMI.ChangePage = pg339_Monitor_Line;   /* Next set is nip monitor */
            else
            {
               if ((cfg_super.wtp_mode != UNUSED))
                  HMI.ChangePage = pg338_Monitor_Ext;   /* Next set is extruder monitor */
               else
                  HMI.ChangePage = pg334_Monitor_Hop_AtoH;   /* Next set is hopper monitor */
            }
            HMI.Action_Input1 = gcInActive;
         }
         break;
      case pg334_Monitor_Hop_AtoH:
      	brsstrcpy((UDINT)HMI.pg334_product_code, (UDINT)BATCH_RECIPE_NOW->product_code);
      	
         copy_recipe(&HMI_RECIPE1, BATCH_RECIPE_NOW, BLENDER_RECIPE);
         copy_recipe(&HMI_RECIPE2, ACTUAL_RECIPE,    BLENDER_RECIPE);
         copy_recipe(&HMI_RECIPE3, BATCH_RECIPE_NEW, BLENDER_RECIPE);
       
         if (NEW_Recipe_after_batch)
            copy_recipe(&HMI_RECIPE3, BATCH_RECIPE_NEW, BLENDER_RECIPE);

         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
			   if ((cfg_super.wtp_mode == UNUSED))
			      HMI.ChangePage = pg330_Monitor_Sys;   /* Standard Deviation */
			   else
			      HMI.ChangePage = pg338_Monitor_Ext;
            HMI.ChangeDevice = 0;
         }
         break;
      case pg335_Monitor_Hop_AtoH2:
		   brsstrcpy((UDINT)HMI.pg334_product_code, (UDINT)BATCH_RECIPE_NOW->product_code);

		   if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
			   HMI.ChangePage = pg330_Monitor_Sys;   /* Next set is nip monitor */
            HMI.ChangeDevice = 0;
         }
         break;
      case pg338_Monitor_Ext:
         if (cs->wtp_k != 0.0)
         {
            HMI.pg338_wtp_set = shrEXT.set_wtp / cs->wtp_k;
            HMI.pg338_wtp_act = shrEXT.act_wtp / cs->wtp_k;
         }

         if (cs->scrap && (cs->refeed == TRIM_ONLY)) 
            visible(&HMI.pg454_recipe_refeed_visible_1, TRUE);
         else
            visible(&HMI.pg454_recipe_refeed_visible_1, FALSE);

         if (cs->wtp_k != 0.0)
            HMI.pg339_refeed_wtp_set = BATCH_RECIPE_NOW->refeed_wtp / cs->wtp_k;

			/* Test for Alpine			
			HMI.pg225_err_code = sext->delta_t/TICKSPERSEC;
			HMI.pg445_SupplyVoltage = (float) sext->delta_b * cext->wtperbit;
			HMI.pg445_SupplyCurrent = (float) sext->newrate;
			
			if(sext->coasts)
				HMI.pg330_MixerProxStsColor = gGreen;
			else
				HMI.pg330_MixerProxStsColor = gBlack;

			if(sext->loading)
				HMI.pg330_MaterialReqColor = gGreen;
			else
				HMI.pg330_MaterialReqColor = gBlack;
			*/
			
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cfg_super.ltp_mode != UNUSED))
               HMI.ChangePage = pg339_Monitor_Line;   /* Next set is nip monitor */
            else
               HMI.ChangePage = pg330_Monitor_Sys;   /* Next set is system monitor */
            HMI.ChangeDevice = 0;
         }
         
         if (!cfg_super.spd_in_percent)
         {
            shrEXT.act_spd_rpm = shrEXT.act_spd * cfgEXT.spd_factor;
            shrEXT.set_spd_rpm = shrEXT.set_spd * cfgEXT.spd_factor;
            visible((unsigned char *)&shrEXT.spd_not_percent_visible, TRUE);
         }
         else 
            visible((unsigned char *)&shrEXT.spd_not_percent_visible, FALSE);
         break;

      case pg339_Monitor_Line:
         if (cs->ltp_k != 0.0)
         {
            HMI.pg339_ltp_set  = shrHO.set_ltp / cs->ltp_k;
            HMI.pg339_ltp_act  = shrHO.act_ltp / cs->ltp_k;
            HMI.pg339_sltp_act = shrSHO.act_ltp / cs->ltp_k;
            if (cs->sltp_mode == MONITOR)
            {
               if ((shrHO.act_ltp > 0.0) && (shrSHO.act_ltp > 0.0) && (cs->ltp_k > 0.0))
               {
                  HMI.pg339_stretch_factor = (shrSHO.act_ltp / shrHO.act_ltp) / cs->ltp_k;
               }
               else
                  HMI.pg339_stretch_factor = 0.0;
            }
         }

         sys_first_recipe_set_act();
         if (HMI.ChangeDevice == 1)    /* Next Set button pressed */
         {
            if ((cfg_super.wtp_mode != UNUSED))
               HMI.ChangePage = pg338_Monitor_Ext;   /* Previous set is extruder monitor */
            else
               HMI.ChangePage = pg330_Monitor_Sys;   /* Next set is nip monitor */
            HMI.ChangeDevice = 0;
         }
			if (!cfg_super.spd_in_percent)
			{
				shrHO.act_spd_rpm = shrHO.act_spd * cfgHO.spd_factor[shrHO.GearNum];
				shrHO.set_spd_rpm = shrHO.set_spd * cfgHO.spd_factor[shrHO.GearNum];
				visible((unsigned char *)&shrHO.spd_not_percent_visible, TRUE);
			}
			else 
				visible((unsigned char *)&shrHO.spd_not_percent_visible, FALSE);
         break;
      
      default:
         /* what to do here? ZZZ */
         break;
   }
	
	if (ucHopperState == 1)
		visible((unsigned char *)&HMI.pg991_TWTLearningRuntime, FALSE); //drt?? DWR
	else
		visible((unsigned char *)&HMI.pg991_TWTLearningRuntime, TRUE);  //drt?? DWR
}

void MonitorVisibilityIcons()
{
   static unsigned char ucCounter=0;
   
   visible(&HMI.pg330_IconsRuntime, TRUE);   
   // statemachines : Hopper + BWH + Mixer + Box
   if (++ucCounter > 1)
   {
      switch (ucHopperState)
      {
         case 0:
            HMI.pg330_HopperAnimIndex = 0;
            break;
         case 1:
            if (HMI.pg330_HopperAnimIndex >= 4)
               HMI.pg330_HopperAnimIndex = 0;
            HMI.pg330_HopperAnimIndex++;
            break;
         case 5:
            HMI.pg330_HopperAnimIndex = 5;
            break; 
      }

      switch (ucBWHState)
      {
         case 0:
            HMI.pg330_BWHAnimIndex = 0;
            break;
         case 1:
            if (HMI.pg330_BWHAnimIndex >= 4)
               HMI.pg330_BWHAnimIndex = 0;
            HMI.pg330_BWHAnimIndex++;
            break;
         case 5:
            HMI.pg330_BWHAnimIndex = 5;
            break;
      }

      switch (ucMixerState)
      {
         case 0:
            HMI.pg330_MixerAnimIndex = 0;
            break;
         case 1: // Mixing.. 1-3
            if (HMI.pg330_MixerAnimIndex >= 3)
               HMI.pg330_MixerAnimIndex = 0;
            HMI.pg330_MixerAnimIndex++;
            break;
         case 4: // Dumping.. 4-6
            if (HMI.pg330_MixerAnimIndex >= 6)
               HMI.pg330_MixerAnimIndex = 3;
            HMI.pg330_MixerAnimIndex++;
            break;
         case 7:
            HMI.pg330_MixerAnimIndex = 7;
            break;
      }
      
      switch (ucBoxState)
      {
         case 0: // empty box
            HMI.pg330_BoxAnimIndex = 0;
            break;

         case 1: // Box recieviing
            if (HMI.pg330_BoxAnimIndex >= 2)
               HMI.pg330_BoxAnimIndex = 0;
            HMI.pg330_BoxAnimIndex++;
            break;
         case 3: // Full box, inactive
            if (HMI.pg330_BoxAnimIndex >= 2)
               HMI.pg330_BoxAnimIndex = 0;
            HMI.pg330_BoxAnimIndex++;
            break;
      }
      ucCounter = 0;
   }
}

void MonitorVisibility()
{
   int   i;
   float f;
   shared_BWH_device *sbwh = ((shared_loop_ptr *)(&shr_LOOP[WEIGH_HOP_LOOP]))->BWH;

   visible(&HMI.pg330_IconsRuntime, FALSE);
   visible(&HMI.pg330_JobNameRuntime, TRUE);     /* show job name */

   if (cfgMIXER.material_request_source == MATERIAL_REQUEST_MIXER_PROX)
      visible(&HMI.pg330_MaterialReqRuntime, FALSE);
   else
      visible(&HMI.pg330_MaterialReqRuntime, TRUE);

   if (cfg_super.job_wt_in_recipe)
      visible(&HMI.pg330_JobWeighInRecipe, TRUE);
   else
      visible(&HMI.pg330_JobWeighInRecipe, FALSE);

   if ((cfg_super.job_wt_complete == TRUE) && (cfg_super.job_wt_in_recipe == TRUE))
   {
      visible(&HMI.pg330_JobCompleteRuntime, TRUE);  /* show job complete text */
      visible(&HMI.pg330_JobPercentRuntime, FALSE);
   }
   else
   {
      if ((f=cfg_super.job_wt) > .003)
         HMI.pg330_job_percent_done = (float)(cfg_super.job_wt_subtotal / f  * 100.0);
      else
         HMI.pg330_job_percent_done = 0.0;

      visible(&HMI.pg330_JobCompleteRuntime, FALSE);  /* hidden job complete text */

      if (cfg_super.job_wt_in_recipe)
         visible(&HMI.pg330_JobPercentRuntime, TRUE);
      else
         visible(&HMI.pg330_JobPercentRuntime, FALSE);
   }

   for (i=0; i<MAX_GATES; i++)
   {
      if (HMI.CurrentPage == pg330_Monitor_Sys) //G2-418
      {
         visible(&HMI.pg330_GateFeedingRuntime[i], FALSE);

         if ((i == sbwh->cur_gate) && (sbwh->cur_gate < cfg_super.total_gates)) /* IT   */
         {
            //G2-418
            if (BATCH_RECIPE_NOW->parts_pct.s[i] == 0.0)
            {
               visible(&HMI.pg330_GateFeedingRuntime[i], FALSE);   
            }
            else if (shrGATE[i].alarm_bits)
            {
               HMI.pg330_GateFeedingColor2[i] = gRed;
               visible(&HMI.pg330_GateFeedingRuntime[i], TRUE);    /* show hopper code */
            }
            else if (shrGATE[i].learn_mode)
            {
               /* learn mode indications GTF */
               if (shrGATE[i].sreceiver.receiving_on == TRUE)
                  HMI.pg330_GateFeedingColor2[i] = gDarkYellow;
               else
                  HMI.pg330_GateFeedingColor2[i] = gYellow;
               visible(&HMI.pg330_GateFeedingRuntime[i], TRUE);    /* show hopper code */
            }
            else
            {
               if (shrGATE[i].sreceiver.receiving_on == TRUE)
                  HMI.pg330_GateFeedingColor2[i] = 113; //dark green
               else
                  HMI.pg330_GateFeedingColor2[i] = gGreen;
               visible(&HMI.pg330_GateFeedingRuntime[i], TRUE);    /* show hopper code */
            }
         }
         else
         {
            if (shrGATE[i].alarm_bits) //show alarm color even if not in recipe
            {
               HMI.pg330_GateFeedingColor2[i] = gRed;
               visible(&HMI.pg330_GateFeedingRuntime[i], TRUE);    /* show hopper feeding */ 
            }
            else
				   visible(&HMI.pg330_GateFeedingRuntime[i], FALSE);   /* hidden hopper feeding */
         }
      }
      //G2-418
   }
}

void MonitorHistogram()
{
   int i;
   static int j;

   if (HMI.pg025_Clear)
   {
      for (i=0; i<10; i++)
      {
         HMI.pg025_hist[i]   = 0;   
         HMI.pg025_Counts[i] = 0;
      }
      HMI.pg025_Count = 0;

      if (j == 0)
         HMI.pg025_Center = 0;   
    
      if (HMI.pg025_chan == 0)
         HMI.pg025_Center += shrBWH.wt_bits;       // weight
      else
         HMI.pg025_Center += shrEXT.wt_bits;       // weight
      HMI.pg025_Center /= 2;

      if (++j >= 100)
      {
         HMI.pg025_Clear  = 0;
         range = HMI.pg025_Range;     // was 50 for initial test
         if (range < 2)
             range = 2;               // for sanity

         for (i=0; i<5; i++)
         {
            HMI.pg025_label[i+5] = range*(i+1);
            HMI.pg025_label[4-i] = -range*(i+1); 
         }
      }
   }
   else
   {
      j = 0;
   
      HMI.pg025_Count++;
   
      C = HMI.pg025_Center;        // center base
      if (HMI.pg025_chan == 0)
         W = shrBWH.wt_bits;       // weight
      else
         W = shrEXT.wt_bits;       // weight
      D = W-C;                     // difference
      
      if (D<0) //Negative
      {
         if (D<0 && D>(DINT)(-range))
         { 
            HMI.pg025_Counts[4]++;
         }
         if (D<(-range) && D>(-range*2))
         {
            HMI.pg025_Counts[3]++;
         }
         if (D<(-range*2) && D>(-range*3))
         {
            HMI.pg025_Counts[2]++;
         }
         if (D<(-range*3) && D>(-range*4))
         {
            HMI.pg025_Counts[1]++;
         }
         if (D<(-range*4) && D>(-range*5))
         {
            HMI.pg025_Counts[0]++;
         }
      }
      else //Positive
      {
         if ((D>=0) && (D<(range)))
         {
             HMI.pg025_Counts[5]++;
         }
         if ((D>(range*1)) && (D<(range*2)))
         {
            HMI.pg025_Counts[6]++;
         }
         if ((D>(range*2)) && (D<(range*3)))
         {
            HMI.pg025_Counts[7]++;
         }
         if ((D>(range*3)) && (D<(range*4)))
         {
            HMI.pg025_Counts[8]++;
         }
         if ((D>(range*4)) && (D<(range*5)))
         {
            HMI.pg025_Counts[9]++;
         }
      }
   
      HMI.pg025_hist[4] = (int) ((float)HMI.pg025_Counts[4])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[5] = (int) ((float)HMI.pg025_Counts[5])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[3] = (int) ((float)HMI.pg025_Counts[3])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[6] = (int) ((float)HMI.pg025_Counts[6])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[2] = (int) ((float)HMI.pg025_Counts[2])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[7] = (int) ((float)HMI.pg025_Counts[7])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[1] = (int) ((float)HMI.pg025_Counts[1])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[8] = (int) ((float)HMI.pg025_Counts[8])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[0] = (int) ((float)HMI.pg025_Counts[0])/((float)HMI.pg025_Count)*100.0;
      HMI.pg025_hist[9] = (int) ((float)HMI.pg025_Counts[9])/((float)HMI.pg025_Count)*100.0;
   }
}

void sys_first_recipe_set_act()
{
  config_super *cs = &cfg_super;
      
  switch(cs->recipe_mode[0])
  {
    case WPL:
       visible(&HMI.pg339_wpl_display, TRUE);
       visible(&HMI.pg339_wpa_display, FALSE);
       visible(&HMI.pg339_thk_display, FALSE);
      break;

    case WPA:
       visible(&HMI.pg339_wpl_display, FALSE);
       visible(&HMI.pg339_wpa_display, TRUE);
       visible(&HMI.pg339_thk_display, FALSE);
      break;

    case THICKNESS:
       visible(&HMI.pg339_wpl_display, FALSE);
       visible(&HMI.pg339_wpa_display, FALSE);
       visible(&HMI.pg339_thk_display, TRUE);
      break;

    default:   /* WTP */
	    visible(&HMI.pg339_wpl_display, FALSE);
	    visible(&HMI.pg339_wpa_display, FALSE);
	    visible(&HMI.pg339_thk_display, FALSE);
      break;
  }
}


