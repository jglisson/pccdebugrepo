/************************************************************************/
/*
   File:  vars.h

   Description: This file contains definitions for process variables.


      $Log:   F:\Software\BR_Guardian\vars.h_v  $
 *
 *    Rev 1.2   Apr 04 2008 11:20:08   vidya
 * Changed cs->temp_recipe to PMEM.RECIPE->
 *
 *    Rev 1.1   Apr 02 2008 15:06:08   FMK
 * Moved the inventory and shift totals for the
 * devices into battery backed ram. Now all references
 * point to this new structure.
 *
 *    Rev 1.0   Jan 11 2008 10:33:42   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
/* replaced these with new var names ones left here are not used in project */

#define GATE_TEMP_SET_PARTS(H)   BATCH_RECIPE_EDIT.parts.s[H]
#define LAST_GATE_ACT_PARTS(H)   LAST_ACTUAL_RECIPE->parts.s[H]

#define LAST_GATE_SET_DENSITY(H) BATCH_RECIPE_LAST->density.s[H]
#define GATE_TEMP_SET_DENSITY(H) BATCH_RECIPE_EDIT.density.s[H]

#define LAST_EXT_SET_PARTS       BATCH_RECIPE_LAST->parts.ext
#define EXT_TEMP_SET_PARTS       BATCH_RECIPE_EDIT.parts.ext

#define LAST_EXT_SET_WTP         BATCH_RECIPE_LAST->ext_wtp
#define EXT_TEMP_SET_WTP         BATCH_RECIPE_EDIT.ext_wtp
#define LAST_EXT_ACT_WTP         LAST_ACTUAL_RECIPE->ext_wtp
              
#define LAST_EXT_SET_WPL         BATCH_RECIPE_LAST->wpl
#define EXT_SET_WPL              BATCH_RECIPE_NOW->wpl
#define EXT_TEMP_SET_WPL         BATCH_RECIPE_EDIT.wpl
#define EXT_ACT_WPL              ACTUAL_RECIPE->wpl
#define LAST_EXT_ACT_WPL         LAST_ACTUAL_RECIPE->wpl

#define LAST_EXT_SET_DENSITY     BATCH_RECIPE_LAST->density.t
#define EXT_SET_DENSITY          BATCH_RECIPE_NOW->density.t
#define EXT_TEMP_SET_DENSITY     BATCH_RECIPE_EDIT.density.t

#define LAST_REFEED_SET_PARTS    BATCH_RECIPE_LAST->parts.s[REFEED]
#define REFEED_TEMP_SET_PARTS    BATCH_RECIPE_EDIT.parts.s[REFEED]
#define LAST_REFEED_ACT_PARTS    LAST_ACTUAL_RECIPE->parts.s[REFEED]

#define LAST_REFEED_SET_WTP      BATCH_RECIPE_LAST->refeed_wtp
#define REFEED_TEMP_SET_WTP      BATCH_RECIPE_EDIT.refeed_wtp

#define LAST_REFEED_SET_DENSITY  BATCH_RECIPE_LAST->density.t
#define REFEED_SET_DENSITY       BATCH_RECIPE_NOW->density.t
#define REFEED_TEMP_SET_DENSITY  BATCH_RECIPE_EDIT.density.t

#define LAST_HO_SET_LTP          BATCH_RECIPE_LAST->ltp
#define HO_TEMP_SET_LTP          BATCH_RECIPE_EDIT.ltp
#define LAST_HO_ACT_LTP          LAST_ACTUAL_RECIPE->ltp

#define LAST_SYS_SET_WPL         BATCH_RECIPE_LAST->wpl
#define SYS_TEMP_SET_WPL         BATCH_RECIPE_EDIT.wpl
#define LAST_SYS_ACT_WPL         LAST_ACTUAL_RECIPE->wpl

#define LAST_SYS_SET_THK         BATCH_RECIPE_LAST->thk
#define SYS_TEMP_SET_THK         BATCH_RECIPE_EDIT.thk
#define LAST_SYS_ACT_THK         LAST_ACTUAL_RECIPE->thk

#define LAST_SYS_SET_WPA         BATCH_RECIPE_LAST->wpa
#define SYS_TEMP_SET_WPA         BATCH_RECIPE_EDIT.wpa
#define EXT_ACT_WPA              ACTUAL_RECIPE->wpa  /* this exsists to make calc match gravitrol */
#define LAST_SYS_ACT_WPA         LAST_ACTUAL_RECIPE->wpa


#define LAST_SYS_SET_WTP         BATCH_RECIPE_LAST->total_wtp
#define SYS_TEMP_SET_WTP         BATCH_RECIPE_EDIT.total_wtp
#define LAST_SYS_ACT_WTP         LAST_ACTUAL_RECIPE->total_wtp

#define LAST_SYS_SET_WIDTH       BATCH_RECIPE_LAST->width
#define SYS_TEMP_SET_WIDTH       BATCH_RECIPE_EDIT.width
#define LAST_SYS_ACT_WIDTH       LAST_ACTUAL_RECIPE->width

#define LAST_SYS_SET_OD          BATCH_RECIPE_LAST->od
#define SYS_TEMP_SET_OD          BATCH_RECIPE_EDIT.od
#define LAST_SYS_ACT_OD          LAST_ACTUAL_RECIPE->od

#define LAST_SYS_SET_ID          BATCH_RECIPE_LAST->id
#define SYS_TEMP_SET_ID          BATCH_RECIPE_EDIT.id
#define LAST_SYS_ACT_ID          LAST_ACTUAL_RECIPE->id

#define LAST_SYS_SET_TOTAL_PARTS BATCH_RECIPE_LAST->total_parts
#define SYS_TEMP_TOTAL_PARTS     BATCH_RECIPE_EDIT.total_parts

#define LAST_SYS_SET_EA          BATCH_RECIPE_LAST->ea
#define SYS_SET_EA               BATCH_RECIPE_NOW->ea

#define LAST_SYS_SET_DENSITY     BATCH_RECIPE_LAST->avg_density
#define SYS_TEMP_SET_DENSITY     BATCH_RECIPE_EDIT.avg_density

#define SYS_TEMP_STRETCH_FACTOR  BATCH_RECIPE_EDIT.stretch_factor

#define SYS_TEMP_JOB_WT          BATCH_RECIPE_EDIT.job_wt
#define SYS_JOB_WT               cs->job_wt
#define SYS_JOB_WT_DONE          cs->job_wt_subtotal

#define SYS_TEMP_STEP_SIZE       BATCH_RECIPE_EDIT.step_size
#define SYS_STEP_SIZE            cs->step_size

#define SYS_INVEN_WT             shr_global.sys_inven_wt
#define SYS_INVEN_WT64           PMEM.dshr_global_sys_inven_wt
#define SYS_SHIFT_WT             shr_global.sys_shift_wt
#define SYS_SHIFT_WT64           PMEM.dshr_global_sys_shift_wt

#define SYS_LAST_INVEN_WT        shr_global.last_sys_inven_wt
#define SYS_LAST_INVEN_WT64      PMEM.dshr_global_last_sys_inven_wt
#define SYS_LAST_SHIFT_WT        shr_global.last_sys_shift_wt
#define SYS_LAST_SHIFT_WT64      PMEM.dshr_global_last_sys_shift_wt

/* The following HOP_ macros are defined for LIW type loops only due to shared code requirements */
#define HOP_WT(H)               shr_LOOP[H].LIW->hop_wt
#define HOP_SET_SPD(H)          shr_LOOP[H].LIW->set_spd
#define HOP_ACT_SPD(H)          shr_LOOP[H].LIW->act_spd
#define HOP_SHIFT_WT(H)         cnf_LOOP[H].LIW->shift_wt
#define HOP_INVEN_WT(H)         cnf_LOOP[H].LIW->inven_wt

#define EXT_SET_SPD             shrEXT.set_spd
#define EXT_ACT_SPD             shrEXT.act_spd
#define EXT_HOP_WT              shrEXT.hop_wt
#define EXT_SHIFT_WT            PMEM.EXT.ShftTotal
#define EXT_INVEN_WT            PMEM.EXT.InvTotal
#define EXT_SHIFT_WT64          PMEM.EXT.dShftTotal
#define EXT_INVEN_WT64          PMEM.EXT.dInvTotal

#define EXT_LAST_SHIFT_WT       PMEM.dshrEXT_last_shift_wt
#define EXT_LAST_INVEN_WT       PMEM.dshrEXT_last_inven_wt

#define REFEED_SET_SPD          cfgGFEEDER.set_spd
#define REFEED_ACT_SPD          cfgGFEEDER.act_spd

#define BATCH_HOP_WT            shrBWH.hop_wt
#define BATCH_HOP_ACT_WTP       shrBWH.last_wtp
#define BATCH_HOP_BATCH_SIZE    cfgBWH.set_batch_size

/* use this definition of the set wt since it changes to correct for the initial dump */
#define GATE_SET_WT(A)          shrBWH.batch_stat[A].gate_set_wt
#define GATE_ACT_WT(A)          shrGATE[A].act_wt

#define GATE_REMAINING_WT(A)    shrGATE[A].rem_wt
#define GATE_SHIFT_WT(A)        PMEM.Gate[A].ShftTotal
#define GATE_INVEN_WT(A)        PMEM.Gate[A].InvTotal
#define GATE_SHIFT_WT64(A)      PMEM.Gate[A].dShftTotal
#define GATE_INVEN_WT64(A)      PMEM.Gate[A].dInvTotal

#define GATE_LAST_SHIFT_WT64(A)    PMEM.dshrGATE[A].last_shift_wt
#define GATE_LAST_INVEN_WT64(A)    PMEM.dshrGATE[A].last_inven_wt

#define HO_SET_SPD              shrHO.set_spd
#define HO_ACT_SPD              shrHO.act_spd
#define HO_SHIFT_LEN            PMEM.HO.ShftTotal
#define HO_INVEN_LEN            PMEM.HO.InvTotal
#define HO_SHIFT_AREA           PMEM.HO.ShftArea

#define HO_INVEN_TOTAL64        PMEM.HO.dInvTotal
#define HO_SHIFT_TOTAL64        PMEM.HO.dShftTotal
#define HO_INVEN_AREA64         PMEM.HO.dInvArea
#define HO_SHIFT_AREA64         PMEM.HO.dShftArea

#define HO_LAST_INVEN_LEN64     PMEM.dshrHO_last_inven_len
#define HO_LAST_SHIFT_LEN64     PMEM.dshrHO_last_shift_len

#define EXT_LAST_INVEN_WT64     PMEM.dshrEXT_last_inven_wt
#define EXT_LAST_SHIFT_WT64     PMEM.dshrEXT_last_shift_wt

#define EXT_INVEN_TOTAL64       PMEM.EXT.dInvTotal
#define EXT_SHIFT_TOTAL64       PMEM.EXT.dShftTotal

#define SHO_ACT_LTP             shrSHO.act_ltp

#define FLF_HOP_WT              shrGFLUFF.hop_wt
#define FLF_ROLL_FEEDER         shrGFLUFF.loading

#define RECIPE_INVEN_WT(A)      PMEM.recipe_inven[A]
#define RECIPE_INVEN_WT64(A)    PMEM.drecipe_inven[A]

#define RESIN_INVEN_WT(A)       PMEM.resin_inven[A]
#define RESIN_INVEN_WT64(A)     PMEM.dresin_inven[A]

