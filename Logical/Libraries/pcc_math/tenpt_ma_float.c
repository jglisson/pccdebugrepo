#include <string.h>
#include <stdio.h>
#include <math.h>
#include "pcc_math.h"

/***********************************************************************/
/*   tenpt_ma_float (int, int) - adds a point to a 10 pt MA of floats  */
/*     written by:   Greg Fisher                                       */
/*     notes:        count must be at least 1                          */
/***********************************************************************/
unsigned char tenpt_ma_float (ma_tp_f_struct *array, float new_point)
{
   int   i;
   float sum;

   array->count += 1; //moved inside function DRT

   sum = 0;
   if (array->count == 0)
      array->count = 1;

   if (array->count < 10)
   {
      array->data[array->count - 1] = new_point;
      for (i=0; i<array->count; i++)
         sum += array->data[i];
      array->avg = sum / array->count;
      /* do I need to float these?  ZZZ GTF */
   }
   else
   {
      array->count = 10;
      /* move the data down one place */

		array->data[array->count - 1] = new_point;	
		
      for (i=0; i<array->count-1; i++)
         array->data[i] = array->data[i+1];
		
      for (i=0; i<array->count; i++)
         sum += array->data[i];
      array->avg = sum / array->count;
      /* do I need to float these?  ZZZ GTF */
   }
   return 1;
}




