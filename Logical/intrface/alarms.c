#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  alarms.c

   Description:


      $Log:   F:\Software\BR_Guardian\alarms.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.alarms to toggle the debug information in this file
 *
 *    Rev 1.6   May 02 2008 15:10:56   FMK
 * Needed to add references to plc rack and relay
 * definitions for controlling alarm outputs.
 *
 *    Rev 1.5   May 01 2008 14:17:56   FMK
 * Changed relay action of alarm outputs.
 *
 *    Rev 1.4   Feb 27 2008 14:35:22   FMK
 * Removed annoying compile warning.
 *
 *    Rev 1.3   Feb 11 2008 14:59:52   FMK
 * Modified code to correctly acknowledge the alarms
 * and clear the alarm history.
 *
 *    Rev 1.2   Jan 30 2008 11:39:46   FMK
 * Changed reference for PVCS check in comment path.
 *
 *    Rev 1.1   Jan 28 2008 13:52:56   FMK
 * Added a 'Popup' structure which informs the system
 * if the popup window is being displayed. Problem existed
 * when using an external keyboard that page switching
 * while showing could interfere with showing a popup on
 * the page switched to. This insures that the popup will
 * be visible only when it is supposed to be.
 *
 *    Rev 1.0   Jan 11 2008 10:33:30   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <sys_lib.h>
#include <visapi.h>
#include "mem.h"
#include "sys.h"
#include "hmi.h"
#include "plc.h"
#include "relaydef.h"
#include "libbgdbg.h"

void Alarm_Main();

extern void visible(unsigned char *Runtime,unsigned char state);

extern struct PopupActiveStruct Popup; /* make sure popups only for alarms are allowed */
extern unsigned char gFlag_AckAlarms;
extern unsigned char gFlag_ClearAlarmHistory;

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;

//_GLOBAL int pg306_Input0;

/************************************************************************/
/*
   Function:  Alarm_Main()

   Description:  main cyclic task for the alarm panel function
                                                                        */
/************************************************************************/
void Alarm_Main()
{
   config_super *cs = &cfg_super;

   if (Popup.Alarms == FALSE)
   {
      visible(&HMI.PopupRuntime1,FALSE);
      visible(&HMI.PopupRuntime2,FALSE);
      visible(&HMI.PopupRuntime3,FALSE);
      visible(&HMI.PopupRuntime4,FALSE);
      visible(&HMI.PopupRuntime5,FALSE);
   }

   if (HMI.CurrentPage == pg306_ActiveAlarms)
   {
      if (HMI.Action_Input0 == gcActive)
      {
         gFlag_AckAlarms = TRUE;
         HMI.Action_Input0 = gcInActive;     /* Reset keypress */
         gAllAlarmsAcked = TRUE;
      }
      IO_dig[X20].channel[ALARM_OUT] = OFF;           /* turn off the beacon */
      IO_dig[X20].channel[ALARM_OUT_AUDIBLE] = OFF;           /* turn off the audible */
   }
   if (HMI.CurrentPage == pg307_AlarmHistory)
   {
      if (cs->current_security_level >= cfg_super.pe_needed[PE_ALARM_CONTROL])
         visible(&HMI.SetupRuntime1,TRUE);    /* shown clear alarm log button */
      else
         visible(&HMI.SetupRuntime1,FALSE);   /* hidden clear alarm log button */

      if (HMI.Action_Input0 == gcActive)
      {
         visible(&HMI.PopupRuntime1,TRUE);    /* show popup */
         Popup.Alarms = TRUE;
         HMI.Action_Input0 = gcInActive;     /* Reset keypress */
      }

      if (HMI.Action_Input7 == gcActive)
      {
         visible(&HMI.PopupRuntime1,FALSE);
         Popup.Alarms = FALSE;
         HMI.Action_Input7 = gcInActive;     /* Reset keypress */
      }

      if (HMI.Action_Input6 == gcActive)
      {
         HMI.Action_Input6 = gcInActive;
         visible(&HMI.PopupRuntime1,FALSE);
         Popup.Alarms = FALSE;
         gFlag_ClearAlarmHistory = TRUE;
      }
   }
}


