#ifdef _DEFAULT_INCLUDES
 #include <AsDefault.h>
#endif
/************************************************************************/
/*
   File:  vars.c

   Description:   functions which return variables used for udata
                  and monitor


      $Log:   F:\Software\BR_Guardian\vars.c_v  $
 *
 * 2008/12/02 JLR
 *  added/changed telnet debug stuff
 *  use debugFlags.vars to toggle debug information in this file
 *
 *    Rev 1.3   May 28 2008 15:06:12   FMK
 * Got rid of compile warnings.
 *
 *    Rev 1.2   May 20 2008 10:34:12   FMK
 * Corrected issues with inventory totals with
 * recipes and resins.
 *
 *    Rev 1.1   Apr 04 2008 11:38:04   vidya
 * Changed cs->temp_recipe to PMEM.RECIPE->
 * Also, changed add_recipe_wt().
 *
 *    Rev 1.0   Jan 11 2008 10:33:42   FMK
 * Initial version


      Process Control Corp,   Atlanta, GA
                                                                        */
/************************************************************************/
#include <bur/plctypes.h>
#include <math.h>
#include "sys.h"
#include "mem.h"
#include "vars.h"
#include "libbgdbg.h"
#include "recipe.h"

void add_recipe_wt(void);
void calc_vars(void);

/* global variable for debug information JLR */
_GLOBAL bgRingBuffer_typ g_RingBuffer1;


/************************************************************************/
/*
   Function:  calc_vars()

   Description: calculates all system actuals for monitor, remote, udata
                                                                        */
/************************************************************************/
void calc_vars(void)
{
	int    i,j, density_valid;
	float  f, f1, f2, f9;
	double d, d1, d2;
	config_super *cs = &cfg_super;
	char rec_mode[3];

   for (i=0; i<3; ++i)
      rec_mode[i] = cfg_super.recipe_mode[i];

   ACTUAL_RECIPE->ext_wtp = shrEXT.act_wtp;
   
   // We need a width to use or no calculations are made
   if ((rec_mode[1] == WIDTH) && (shrEXT.oper_mode == MAN_MODE) && (BATCH_RECIPE_EDIT.width != 0.0))
      ACTUAL_RECIPE->width = BATCH_RECIPE_EDIT.width;
   if (ACTUAL_RECIPE->width == 0.0)
      ACTUAL_RECIPE->width = 24.0;
   
   if (cs->scrap)
   {
      if (cs->refeed == TRIM_ONLY)
      {
         if (EXT_HO_OPER_MODE == AUTO_MODE)
            ACTUAL_RECIPE->refeed_wtp = BATCH_RECIPE_NOW->refeed_wtp;
         else
         {
            f = ACTUAL_RECIPE->ext_wtp;
            ACTUAL_RECIPE->refeed_wtp = (float)(REFEED_TEMP_SET_PARTS * f / 100.0);
         }
      }
      else
         ACTUAL_RECIPE->refeed_wtp = shrGFLUFF.act_wtp;
   }

   ACTUAL_RECIPE->ltp = shrHO.act_ltp;

   /* calculate the current stretch factor */
   if (cs->sltp_mode != UNUSED)
   {
      if ((shrHO.updated_flag) && (shrSHO.updated_flag))
      {
         if ((shrHO.act_ltp > 0.001) && (shrSHO.act_ltp > 0.001))
         {
            ACTUAL_RECIPE->stretch_factor = shrSHO.act_ltp / shrHO.act_ltp;
            shrHO.updated_flag = shrSHO.updated_flag = FALSE;
         }
         else
            ACTUAL_RECIPE->stretch_factor = 0.0;         
      }
   }
   else
      ACTUAL_RECIPE->stretch_factor = 0.0;

   if (cs->wth_mode == UNUSED)
   {
      if (EXT_HO_OPER_MODE == AUTO_MODE)
         ACTUAL_RECIPE->width = BATCH_RECIPE_NOW->width;
      else if (EXT_HO_OPER_MODE != PAUSE_MODE)
         ACTUAL_RECIPE->width = BATCH_RECIPE_NEW->width;
      else   
         ACTUAL_RECIPE->width = 0.0;
   }
   else
      ACTUAL_RECIPE->width = shrWTH.act_wth;

   if (cs->scrap)
      ACTUAL_RECIPE->total_wtp = ACTUAL_RECIPE->ext_wtp + ACTUAL_RECIPE->refeed_wtp;
   else
      ACTUAL_RECIPE->total_wtp = ACTUAL_RECIPE->ext_wtp;

     
	/* do wpl & wpa  */
   f = ACTUAL_RECIPE->total_wtp;
	if ((f9=ACTUAL_RECIPE->ltp) > 0.0)
	{
      if ((cs->sltp_mode != UNUSED) && (ACTUAL_RECIPE->stretch_factor > 0.0))
      { 
         ACTUAL_RECIPE->wpl = (f * cs->wpl_k) / (f9 * ACTUAL_RECIPE->stretch_factor); // DIVIDE;    /* DIVIDE */
         if (ACTUAL_RECIPE->width > 0.001)
		      ACTUAL_RECIPE->wpa = (f * cs->wpa_k) / (ACTUAL_RECIPE->width * cs->width_k * f9 * ACTUAL_RECIPE->stretch_factor);
      }
      else
      {
         ACTUAL_RECIPE->wpl = (f * cs->wpl_k) / f9; // DIVIDE;    /* DIVIDE */
         if (ACTUAL_RECIPE->width > 0.001)
		      ACTUAL_RECIPE->wpa = (f * cs->wpa_k) / (ACTUAL_RECIPE->width * cs->width_k * f9);
      }
	}
	else
	{
		ACTUAL_RECIPE->wpl = 0.0;
		ACTUAL_RECIPE->wpa = 0.0;
	}
	
	shrHO.act_wpa = ACTUAL_RECIPE->wpa;
	shrHO.act_wpl = ACTUAL_RECIPE->wpl;

   /* do inventory calculations  */
   f1 = 0.0;
   f2 = 0.0;
   for (j=0; j<cs->total_gates; j++)
   {
      f = GATE_INVEN_WT(j);
      f1 = f1 + f;
      f = GATE_SHIFT_WT(j);
      f2 = f2 + f;
   }
   SYS_INVEN_WT = f1;
   SYS_SHIFT_WT = f2;

   /* do inventory calculations 64 bit */
   d1 = 0.0;
   d2 = 0.0;
   for (j=0; j<cs->total_gates; j++)
   {
      d = GATE_INVEN_WT64(j);
      d1 = d1 + d;
      d = GATE_SHIFT_WT64(j);
      d2 = d2 + d;
   }
   SYS_INVEN_WT64 = d1;
   SYS_SHIFT_WT64 = d2;

#if 0
   f3 = 0.0;
   f4 = 0.0;

   /* blender total shift/inven weights */
   EXT_INVEN_WT(0) = f3;
   EXT_SHIFT_WT(0) = f4;

   /* extruder shift/inven weight */
   EXT_INVEN_WT(EXT_LOOP) = HOP_INVEN_WT(EXT_LOOP);
   EXT_SHIFT_WT(EXT_LOOP) = HOP_SHIFT_WT(EXT_LOOP);
#endif

   if (cs->enable_product_code || (cs->current_temp_recipe_num > 0))   /* add accumulated weight to the current recipe */
      add_recipe_wt();

	/* do parts & subparts        */
	if (cs->density_in_recipe)    /* thickness modes      */
	{
		/* first calculate actual density's                 */
		/* do system first so can do refeed                 */
		f  = 0.0;
		f1 = 0.0;
      	
		
   	density_valid = FALSE;
		for (j=0; j<cs->total_gates; j++)
		{
			f  = f  + BATCH_RECIPE_NOW->parts.s[j] * BATCH_RECIPE_NOW->density.s[j];
			f1 = f1 + BATCH_RECIPE_NOW->parts.s[j];
			if (BATCH_RECIPE_NOW->density.s[j])
			   density_valid = TRUE;
		}

      /* divide by total parts to get average density */
      if (f1 > 0.0)
         ACTUAL_RECIPE->avg_density = (float)(f / f1);
      else
         ACTUAL_RECIPE->avg_density = (float)0.0;

		ACTUAL_RECIPE->density.t = ACTUAL_RECIPE->avg_density/100.0;
      
		if (ACTUAL_RECIPE->density.t == 0)
			ACTUAL_RECIPE->density.t = 1;

		/* now calculate effective areas    */
		f = ACTUAL_RECIPE->ltp * ACTUAL_RECIPE->density.t * cs->t_k;
		if (f > 0.0)
			f = ACTUAL_RECIPE->ext_wtp / f;
      
	   if ((cs->sltp_mode != UNUSED) && (ACTUAL_RECIPE->stretch_factor > 0.0))
         ACTUAL_RECIPE->ea = (float)f / ACTUAL_RECIPE->stretch_factor;
      else
         ACTUAL_RECIPE->ea = (float)f;
  
		switch(cs->application)
		{
			case BLOWN_FILM:
				f = ACTUAL_RECIPE->width * cs->width_k * cs->ea_k * 2.0;
				if (f > 0.0)
					ACTUAL_RECIPE->thk = ACTUAL_RECIPE->ea / f ;
				else
					ACTUAL_RECIPE->thk = 0.0;
				break;

         case SHEET:
            f = ACTUAL_RECIPE->width * cs->width_k * cs->ea_k;
            if (f > 0.0)
               ACTUAL_RECIPE->thk = ACTUAL_RECIPE->ea / f;
            else
               ACTUAL_RECIPE->thk = 0.0;
            break;

         case WIRE_AND_CABLE:
         case PIPE_AND_TUBING:
            switch(cs->recipe_mode[1])      /* look at which is fixed       */
            {
               case OD:
                  ACTUAL_RECIPE->od = BATCH_RECIPE_NOW->od;
                  f = ACTUAL_RECIPE->od * cs->od_k;
                  f = f * f - (4.0 * ACTUAL_RECIPE->ea / PI);
                  if (cs->id_k > 0.0)
                  {
                     if (f > 0.0)
                        ACTUAL_RECIPE->id = (float)(sqrt(f)/cs->id_k);
                     else
                        ACTUAL_RECIPE->id = ACTUAL_RECIPE->od*cs->od_k/cs->id_k;
                  }
                  else
                     ACTUAL_RECIPE->id = 0.0;
                  break;

               case ID:
                  ACTUAL_RECIPE->id = BATCH_RECIPE_NOW->id;
                  f = ACTUAL_RECIPE->id * cs->id_k;
                  f = f * f + (4.0 * ACTUAL_RECIPE->ea / PI);
                  if (cs->od_k > 0.0)
                  {
                     if (f > 0.0)
                        ACTUAL_RECIPE->od = (float)(sqrt(f)/cs->od_k);
                     else
                        ACTUAL_RECIPE->od = ACTUAL_RECIPE->id*cs->id_k/cs->od_k;
                  }
                  else
                     ACTUAL_RECIPE->od = 0.0;
                  break;

               default:
                  break;
            }

            if (cs->ea_k > 0.0)
               ACTUAL_RECIPE->thk = (ACTUAL_RECIPE->od * cs->od_k - ACTUAL_RECIPE->id * cs->id_k)/(2.0 * cs->ea_k);
            else
               ACTUAL_RECIPE->thk = 0.0;
            break;

         default:
            break;
      }
		if (density_valid)
   		shrHO.act_thk = ACTUAL_RECIPE->thk/100.0;
	   else
		   shrHO.act_thk = ACTUAL_RECIPE->thk;
   }
   else if (cs->force_density)
   {
      /* first calculate actual density's                 */
      /* do system first so can do refeed                 */
      f  = 0.0;
      f1 = 0.0;

      for (j=0; j<cs->total_gates; j++)
      {
         f  = f  + ACTUAL_RECIPE->parts.s[j] * BATCH_RECIPE_NOW->density.s[j];
         f1 = f1 + ACTUAL_RECIPE->parts.s[j];
      }

      /* divide by total parts to get average density */
      if (f1 > 0.0)
         ACTUAL_RECIPE->avg_density = (float)(f / f1);
      else
         ACTUAL_RECIPE->avg_density = (float)0.0;

      ACTUAL_RECIPE->density.t = ACTUAL_RECIPE->avg_density;

   }
   if ((f9=ACTUAL_RECIPE->total_wtp) > 0.0001)
   {
      if (cs->scrap)
      {
         if (EXT_HO_OPER_MODE == AUTO_MODE)
         {
            ACTUAL_RECIPE->parts.ext = (float)((ACTUAL_RECIPE->ext_wtp / f9) * BATCH_RECIPE_NOW->total_parts);
            ACTUAL_RECIPE->parts.s[REFEED] = (float)((ACTUAL_RECIPE->refeed_wtp / f9) * BATCH_RECIPE_NOW->total_parts);
         }
         else
         {
            ACTUAL_RECIPE->parts.ext = (float)((ACTUAL_RECIPE->ext_wtp / f9) * (double)100.0);
            ACTUAL_RECIPE->parts.s[REFEED] = (float)((ACTUAL_RECIPE->refeed_wtp / f9) * (double)100.0);
         }
      }
      else
      {
         ACTUAL_RECIPE->parts.ext = 100.0;
         ACTUAL_RECIPE->parts.s[REFEED] = 0.0;
      }
   }
   else
   {
      ACTUAL_RECIPE->parts.ext = 0.0;
      ACTUAL_RECIPE->parts.s[REFEED] = 0.0;
   }
}


/************************************************************************/
/*
   Function:  add_recipe_wt()

   Description:
                                                                        */
/************************************************************************/
void add_recipe_wt(void)
{
    int rnum;

    //rnum = shr_global.current_recipe_num;
	 rnum = PMEM.recipe_num;

    /* ensure that recipe number is valid */
    if (rnum >= MAX_RECIPES)
       rnum = MAX_RECIPES-1;

    PMEM.recipe_inven[rnum] = PMEM.recipe_inven[rnum] + shr_global.recipe_inven_wt;
    shr_global.recipe_inven_wt = 0.0;

    PMEM.drecipe_inven[rnum] = PMEM.drecipe_inven[rnum] + PMEM.dshr_global_recipe_inven_wt;
    PMEM.dshr_global_recipe_inven_wt = 0.0;
}


