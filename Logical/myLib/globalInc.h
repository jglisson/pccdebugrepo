/********************************************************************
 * COPYRIGHT -- Piedmont Automation
 ********************************************************************
 * Package: Global
 * File: GlobalInc.h
 * Created: September 15, 2009
 *******************************************************************/
#ifndef GLOBAL_INC_H
#define GLOBAL_INC_H

#include <bur/plctypes.h>
#include "mem.h"
#include "sys.h"
#include <Global/timer.c>
#include <Global/libbgdbg.c>
#include <Global/string_pcc.c>

#endif
