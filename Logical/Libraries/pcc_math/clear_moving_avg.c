#include <stdio.h>
#include "pcc_math.h"

signed short clear_moving_avg(register struct moving_avg_str *ma_ptr)
{
    register int i;

    ma_ptr->curr_sum = 0.0;
    ma_ptr->curr_avg = 0.0;
    ma_ptr->num_valid = 0;
    ma_ptr->idx = 0;

    for(i=0;i<ma_ptr->num_pts;++i)
    {
        ma_ptr->data[i] = 0.0;
    }

    return(0);
}


