#include <math.h>
#include "pcc_math.h"

float calc_rms_error_p(samples *error_data,signed short length)
{
    float error;
    int i;
    ordered_pair *data;
    int idx;

    if(length >= error_data->length) length = error_data->length;
    data = error_data->pts;

    idx = error_data->next - length;
    if(idx < 0)
        idx += error_data->length;

    error = 0.0;

    for(i=0; i<length; ++i)
    {
        error = error + data[idx].y * data[idx].y;

        if(++idx >= error_data->length)
            idx -= error_data->length;
    }
    error = error / length;
    error = (REAL)sqrt((REAL)error);

    return error;
}


