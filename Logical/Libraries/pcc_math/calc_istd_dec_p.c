#include <stdio.h>
#include "math.h"
#include "pcc_math.h"

signed short calc_istd_dev_p(isamples *sample_data, signed short *mean, signed short num)
{
    int std_dev;
    int var;

    var = calc_ivariance_p(sample_data,mean,num);

    std_dev = (int)(sqrt((float)var)+.5);
    return std_dev;
}


