#include <stdio.h>
#include "pcc_math.h"


signed short init_mvg_avg(struct moving_avg_str *ma_ptr,signed short size,float *data,float change_limit)
{
   if ((ma_ptr == NULL) || (size <= 0) || (data == NULL) || (change_limit < 0.0))
      return (-1);

    ma_ptr->num_pts = size;
    ma_ptr->data = (REAL *)data;
    ma_ptr->limit_change = change_limit;

    ma_ptr->idx = 0;
    ma_ptr->num_valid = 0;
    ma_ptr->curr_sum = 0.0;
    ma_ptr->curr_avg = 0.0;
    ma_ptr->bad_value = 0.0;
    ma_ptr->num_clipped = 0;

    clear_moving_avg(ma_ptr);

    return(0);
}


